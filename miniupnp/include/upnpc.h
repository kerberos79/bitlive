/* $Id: upnpc.c,v 1.101 2014/01/31 13:18:25 nanard Exp $ */
/* Project : miniupnp
 * Author : Thomas Bernard
 * Copyright (c) 2005-2013 Thomas Bernard
 * This software is subject to the conditions detailed in the
 * LICENCE file provided in this distribution. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#ifdef _WIN32
#include <winsock2.h>
#define snprintf _snprintf
#else
/* for IPPROTO_TCP / IPPROTO_UDP */
#include <netinet/in.h>
#endif
#include "miniwget.h"
#include "miniupnpc.h"
#include "upnpcommands.h"
#include "upnperrors.h"

/* protofix() checks if protocol is "UDP" or "TCP"
 * returns NULL if not */
const char * protofix(const char * proto);
void DisplayInfos(struct UPNPUrls * urls,
                         struct IGDdatas * data);

void GetConnectionStatus(struct UPNPUrls * urls,
                               struct IGDdatas * data);

void ListRedirections(struct UPNPUrls * urls,
                             struct IGDdatas * data);

void NewListRedirections(struct UPNPUrls * urls,
                                struct IGDdatas * data);
/* Test function
 * 1 - get connection type
 * 2 - get extenal ip address
 * 3 - Add port mapping
 * 4 - get this port mapping from the IGD */
int SetRedirectAndTest(struct UPNPUrls * urls,
                               struct IGDdatas * data,
							   const char * iaddr,
							   const char * iport,
							   const char * eport,
                               const char * proto,
                               const char * leaseDuration,
                               const char * description,
							   const char * eaddr);

void
RemoveRedirect(struct UPNPUrls * urls,
               struct IGDdatas * data,
			   const char * eport,
			   const char * proto);

/* IGD:2, functions for service WANIPv6FirewallControl:1 */
void GetFirewallStatus(struct UPNPUrls * urls, struct IGDdatas * data);

/* Test function
 * 1 - Add pinhole
 * 2 - Check if pinhole is working from the IGD side */
int SetPinholeAndTest(struct UPNPUrls * urls, struct IGDdatas * data,
					const char * remoteaddr, const char * eport,
					const char * intaddr, const char * iport,
					const char * proto, const char * lease_time);

/* Test function
 * 1 - Check if pinhole is working from the IGD side
 * 2 - Update pinhole */
void GetPinholeAndUpdate(struct UPNPUrls * urls, struct IGDdatas * data,
					const char * uniqueID, const char * lease_time);
/* Test function
 * Get pinhole timeout
 */
void GetPinholeOutboundTimeout(struct UPNPUrls * urls, struct IGDdatas * data,
					const char * remoteaddr, const char * eport,
					const char * intaddr, const char * iport,
					const char * proto);
void
GetPinholePackets(struct UPNPUrls * urls,
               struct IGDdatas * data, const char * uniqueID);
void
CheckPinhole(struct UPNPUrls * urls,
               struct IGDdatas * data, const char * uniqueID);
void
RemovePinhole(struct UPNPUrls * urls,
               struct IGDdatas * data, const char * uniqueID);
