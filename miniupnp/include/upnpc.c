/* $Id: upnpc.c,v 1.101 2014/01/31 13:18:25 nanard Exp $ */
/* Project : miniupnp
 * Author : Thomas Bernard
 * Copyright (c) 2005-2013 Thomas Bernard
 * This software is subject to the conditions detailed in the
 * LICENCE file provided in this distribution. */

#include "upnpc.h"
#include <jni.h>
#include <android/log.h>

#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, "upnp", __VA_ARGS__)
/* protofix() checks if protocol is "UDP" or "TCP"
 * returns NULL if not */
const char * protofix(const char * proto)
{
	const char proto_tcp[4] = { 'T', 'C', 'P', 0};
	const char proto_udp[4] = { 'U', 'D', 'P', 0};
	int i, b;
	for(i=0, b=1; i<4; i++)
		b = b && (   (proto[i] == proto_tcp[i])
		          || (proto[i] == (proto_tcp[i] | 32)) );
	if(b)
		return proto_tcp;
	for(i=0, b=1; i<4; i++)
		b = b && (   (proto[i] == proto_udp[i])
		          || (proto[i] == (proto_udp[i] | 32)) );
	if(b)
		return proto_udp;
	return 0;
}

void DisplayInfos(struct UPNPUrls * urls,
                         struct IGDdatas * data)
{
	char externalIPAddress[40];
	char connectionType[64];
	char status[64];
	char lastconnerr[64];
	unsigned int uptime;
	unsigned int brUp, brDown;
	time_t timenow, timestarted;
	int r;
	if(UPNP_GetConnectionTypeInfo(urls->controlURL,
	                              data->first.servicetype,
	                              connectionType) != UPNPCOMMAND_SUCCESS)
		LOGI("GetConnectionTypeInfo failed.");
	else
		LOGI("Connection Type : %s", connectionType);
	if(UPNP_GetStatusInfo(urls->controlURL, data->first.servicetype,
	                      status, &uptime, lastconnerr) != UPNPCOMMAND_SUCCESS)
		LOGI("GetStatusInfo failed.");
	else
		LOGI("Status : %s, uptime=%us, LastConnectionError : %s",
		       status, uptime, lastconnerr);
	timenow = time(NULL);
	timestarted = timenow - uptime;
	LOGI("  Time started : %s", ctime(&timestarted));
	if(UPNP_GetLinkLayerMaxBitRates(urls->controlURL_CIF, data->CIF.servicetype,
	                                &brDown, &brUp) != UPNPCOMMAND_SUCCESS) {
		LOGI("GetLinkLayerMaxBitRates failed.");
	} else {
		LOGI("MaxBitRateDown : %u bps", brDown);
		if(brDown >= 1000000) {
			LOGI(" (%u.%u Mbps)", brDown / 1000000, (brDown / 100000) % 10);
		} else if(brDown >= 1000) {
			LOGI(" (%u Kbps)", brDown / 1000);
		}
		LOGI("   MaxBitRateUp %u bps", brUp);
		if(brUp >= 1000000) {
			LOGI(" (%u.%u Mbps)", brUp / 1000000, (brUp / 100000) % 10);
		} else if(brUp >= 1000) {
			LOGI(" (%u Kbps)", brUp / 1000);
		}
		LOGI("");
	}
	r = UPNP_GetExternalIPAddress(urls->controlURL,
	                          data->first.servicetype,
							  externalIPAddress);
	if(r != UPNPCOMMAND_SUCCESS) {
		LOGI("GetExternalIPAddress failed. (errorcode=%d)", r);
	} else {
		LOGI("ExternalIPAddress = %s", externalIPAddress);
	}
}

void GetConnectionStatus(struct UPNPUrls * urls,
                               struct IGDdatas * data)
{
	unsigned int bytessent, bytesreceived, packetsreceived, packetssent;
	DisplayInfos(urls, data);
	bytessent = UPNP_GetTotalBytesSent(urls->controlURL_CIF, data->CIF.servicetype);
	bytesreceived = UPNP_GetTotalBytesReceived(urls->controlURL_CIF, data->CIF.servicetype);
	packetssent = UPNP_GetTotalPacketsSent(urls->controlURL_CIF, data->CIF.servicetype);
	packetsreceived = UPNP_GetTotalPacketsReceived(urls->controlURL_CIF, data->CIF.servicetype);
	LOGI("Bytes:   Sent: %8u\tRecv: %8u", bytessent, bytesreceived);
	LOGI("Packets: Sent: %8u\tRecv: %8u", packetssent, packetsreceived);
}

void ListRedirections(struct UPNPUrls * urls,
                             struct IGDdatas * data)
{
	int r;
	int i = 0;
	char index[6];
	char intClient[40];
	char intPort[6];
	char extPort[6];
	char protocol[4];
	char desc[80];
	char enabled[6];
	char rHost[64];
	char duration[16];
	/*unsigned int num=0;
	UPNP_GetPortMappingNumberOfEntries(urls->controlURL, data->servicetype, &num);
	LOGI("PortMappingNumberOfEntries : %u", num);*/
	LOGI(" i protocol exPort->inAddr:inPort description remoteHost leaseTime");
	do {
		snprintf(index, 6, "%d", i);
		rHost[0] = '\0'; enabled[0] = '\0';
		duration[0] = '\0'; desc[0] = '\0';
		extPort[0] = '\0'; intPort[0] = '\0'; intClient[0] = '\0';
		r = UPNP_GetGenericPortMappingEntry(urls->controlURL,
		                               data->first.servicetype,
		                               index,
		                               extPort, intClient, intPort,
									   protocol, desc, enabled,
									   rHost, duration);
		if(r==0)
		/*
			LOGI("%02d - %s %s->%s:%s\tenabled=%s leaseDuration=%s"
			       "     desc='%s' rHost='%s'",
			       i, protocol, extPort, intClient, intPort,
				   enabled, duration,
				   desc, rHost);
				   */
			LOGI("%2d %s %5s->%s:%-5s '%s' '%s' %s",
			       i, protocol, extPort, intClient, intPort,
			       desc, rHost, duration);
		else
			LOGI("GetGenericPortMappingEntry() returned %d (%s)",
			       r, strupnperror(r));
		i++;
	} while(r==0);
}

void NewListRedirections(struct UPNPUrls * urls,
                                struct IGDdatas * data)
{
	int r;
	int i = 0;
	struct PortMappingParserData pdata;
	struct PortMapping * pm;

	memset(&pdata, 0, sizeof(struct PortMappingParserData));
	r = UPNP_GetListOfPortMappings(urls->controlURL,
                                   data->first.servicetype,
	                               "0",
	                               "65535",
	                               "TCP",
	                               "1000",
	                               &pdata);
	if(r == UPNPCOMMAND_SUCCESS)
	{
		LOGI(" i protocol exPort->inAddr:inPort description remoteHost leaseTime");
		for(pm = pdata.head.lh_first; pm != NULL; pm = pm->entries.le_next)
		{
			LOGI("%2d %s %5hu->%s:%-5hu '%s' '%s' %u",
			       i, pm->protocol, pm->externalPort, pm->internalClient,
			       pm->internalPort,
			       pm->description, pm->remoteHost,
			       (unsigned)pm->leaseTime);
			i++;
		}
		FreePortListing(&pdata);
	}
	else
	{
		LOGI("GetListOfPortMappings() returned %d (%s)",
		       r, strupnperror(r));
	}
	r = UPNP_GetListOfPortMappings(urls->controlURL,
                                   data->first.servicetype,
	                               "0",
	                               "65535",
	                               "UDP",
	                               "1000",
	                               &pdata);
	if(r == UPNPCOMMAND_SUCCESS)
	{
		for(pm = pdata.head.lh_first; pm != NULL; pm = pm->entries.le_next)
		{
			LOGI("%2d %s %5hu->%s:%-5hu '%s' '%s' %u",
			       i, pm->protocol, pm->externalPort, pm->internalClient,
			       pm->internalPort,
			       pm->description, pm->remoteHost,
			       (unsigned)pm->leaseTime);
			i++;
		}
		FreePortListing(&pdata);
	}
	else
	{
		LOGI("GetListOfPortMappings() returned %d (%s)",
		       r, strupnperror(r));
	}
}

/* Test function
 * 1 - get connection type
 * 2 - get extenal ip address
 * 3 - Add port mapping
 * 4 - get this port mapping from the IGD */
int SetRedirectAndTest(struct UPNPUrls * urls,
                               struct IGDdatas * data,
							   const char * iaddr,
							   const char * iport,
							   const char * eport,
                               const char * proto,
                               const char * leaseDuration,
                               const char * description,
							   const char * eaddr)
{
	char intClient[40];
	char intPort[6];
	char duration[16];
	int r;

	if(!iaddr || !iport || !eport || !proto)
	{
		LOGI("Wrong arguments");
		return;
	}
	proto = protofix(proto);
	if(!proto)
	{
		LOGI("invalid protocol");
		return;
	}

	UPNP_GetExternalIPAddress(urls->controlURL,
	                          data->first.servicetype,
							  eaddr);
	if(eaddr[0])
		LOGI("ExternalIPAddress = %s", eaddr);
	else
		LOGI("GetExternalIPAddress failed.");

	r = UPNP_AddPortMapping(urls->controlURL, data->first.servicetype,
	                        eport, iport, iaddr, description,
	                        proto, 0, leaseDuration);
	if(r!=UPNPCOMMAND_SUCCESS) {
		LOGI("AddPortMapping(%s, %s, %s) failed with code %d (%s)",
		       eport, iport, iaddr, r, strupnperror(r));
		return r;
	}
	r = UPNP_GetSpecificPortMappingEntry(urls->controlURL,
	                                 data->first.servicetype,
    	                             eport, proto, NULL/*remoteHost*/,
									 intClient, intPort, NULL/*desc*/,
	                                 NULL/*enabled*/, duration);
	if(r!=UPNPCOMMAND_SUCCESS)
		LOGI("GetSpecificPortMappingEntry() failed with code %d (%s)",
		       r, strupnperror(r));

	if(intClient[0]) {
		LOGI("InternalIP:Port = %s:%s", intClient, intPort);
		LOGI("external %s:%s %s is redirected to internal %s:%s (duration=%s)",
		       eaddr, eport, proto, intClient, intPort, duration);
	}
	return r;
}

void
RemoveRedirect(struct UPNPUrls * urls,
               struct IGDdatas * data,
			   const char * eport,
			   const char * proto)
{
	int r;
	if(!proto || !eport)
	{
		LOGI("invalid arguments");
		return;
	}
	proto = protofix(proto);
	if(!proto)
	{
		LOGI("protocol invalid");
		return;
	}
	r = UPNP_DeletePortMapping(urls->controlURL, data->first.servicetype, eport, proto, 0);
	LOGI("UPNP_DeletePortMapping() returned : %d", r);
}

/* IGD:2, functions for service WANIPv6FirewallControl:1 */
void GetFirewallStatus(struct UPNPUrls * urls, struct IGDdatas * data)
{
	unsigned int bytessent, bytesreceived, packetsreceived, packetssent;
	int firewallEnabled = 0, inboundPinholeAllowed = 0;

	UPNP_GetFirewallStatus(urls->controlURL_6FC, data->IPv6FC.servicetype, &firewallEnabled, &inboundPinholeAllowed);
	LOGI("FirewallEnabled: %d & Inbound Pinhole Allowed: %d", firewallEnabled, inboundPinholeAllowed);
	LOGI("GetFirewallStatus:   Firewall Enabled: %s   Inbound Pinhole Allowed: %s", (firewallEnabled)? "Yes":"No", (inboundPinholeAllowed)? "Yes":"No");

	bytessent = UPNP_GetTotalBytesSent(urls->controlURL_CIF, data->CIF.servicetype);
	bytesreceived = UPNP_GetTotalBytesReceived(urls->controlURL_CIF, data->CIF.servicetype);
	packetssent = UPNP_GetTotalPacketsSent(urls->controlURL_CIF, data->CIF.servicetype);
	packetsreceived = UPNP_GetTotalPacketsReceived(urls->controlURL_CIF, data->CIF.servicetype);
	LOGI("Bytes:   Sent: %8u\tRecv: %8u", bytessent, bytesreceived);
	LOGI("Packets: Sent: %8u\tRecv: %8u", packetssent, packetsreceived);
}

/* Test function
 * 1 - Add pinhole
 * 2 - Check if pinhole is working from the IGD side */
int SetPinholeAndTest(struct UPNPUrls * urls, struct IGDdatas * data,
					const char * remoteaddr, const char * eport,
					const char * intaddr, const char * iport,
					const char * proto, const char * lease_time)
{
	char uniqueID[8];
	/*int isWorking = 0;*/
	int r;
	char proto_tmp[8];

	if(atoi(proto) == 0)
	{
		const char * protocol;
		protocol = protofix(proto);
		if(protocol && (strcmp("TCP", protocol) == 0))
		{
			snprintf(proto_tmp, sizeof(proto_tmp), "%d", IPPROTO_TCP);
			proto = proto_tmp;
		}
		else if(protocol && (strcmp("UDP", protocol) == 0))
		{
			snprintf(proto_tmp, sizeof(proto_tmp), "%d", IPPROTO_UDP);
			proto = proto_tmp;
		}
		else
		{
			LOGI("invalid protocol");
			return UPNPCOMMAND_INVALID_ARGS;
		}
	}
	r = UPNP_AddPinhole(urls->controlURL_6FC, data->IPv6FC.servicetype, remoteaddr, eport, intaddr, iport, proto, lease_time, uniqueID);
	if(r!=UPNPCOMMAND_SUCCESS) {
		LOGI("AddPinhole([%s]:%s -> [%s]:%s) failed with code %d (%s)",
		       remoteaddr, eport, intaddr, iport, r, strupnperror(r));
	}
	else
	{
		LOGI("AddPinhole: ([%s]:%s -> [%s]:%s) / Pinhole ID = %s",
		       remoteaddr, eport, intaddr, iport, uniqueID);
		/*r = UPNP_CheckPinholeWorking(urls->controlURL_6FC, data->servicetype_6FC, uniqueID, &isWorking);
		if(r!=UPNPCOMMAND_SUCCESS)
			LOGI("CheckPinholeWorking() failed with code %d (%s)", r, strupnperror(r));
		LOGI("CheckPinholeWorking: Pinhole ID = %s / IsWorking = %s", uniqueID, (isWorking)? "Yes":"No");*/
	}
	return r;
}

/* Test function
 * 1 - Check if pinhole is working from the IGD side
 * 2 - Update pinhole */
void GetPinholeAndUpdate(struct UPNPUrls * urls, struct IGDdatas * data,
					const char * uniqueID, const char * lease_time)
{
	int isWorking = 0;
	int r;

	if(!uniqueID || !lease_time)
	{
		LOGI("Wrong arguments");
		return;
	}
	r = UPNP_CheckPinholeWorking(urls->controlURL_6FC, data->IPv6FC.servicetype, uniqueID, &isWorking);
	LOGI("CheckPinholeWorking: Pinhole ID = %s / IsWorking = %s", uniqueID, (isWorking)? "Yes":"No");
	if(r!=UPNPCOMMAND_SUCCESS)
		LOGI("CheckPinholeWorking() failed with code %d (%s)", r, strupnperror(r));
	if(isWorking || r==709)
	{
		r = UPNP_UpdatePinhole(urls->controlURL_6FC, data->IPv6FC.servicetype, uniqueID, lease_time);
		LOGI("UpdatePinhole: Pinhole ID = %s with Lease Time: %s", uniqueID, lease_time);
		if(r!=UPNPCOMMAND_SUCCESS)
			LOGI("UpdatePinhole: ID (%s) failed with code %d (%s)", uniqueID, r, strupnperror(r));
	}
}

/* Test function
 * Get pinhole timeout
 */
void GetPinholeOutboundTimeout(struct UPNPUrls * urls, struct IGDdatas * data,
					const char * remoteaddr, const char * eport,
					const char * intaddr, const char * iport,
					const char * proto)
{
	int timeout = 0;
	int r;

	if(!intaddr || !remoteaddr || !iport || !eport || !proto)
	{
		LOGI("Wrong arguments");
		return;
	}

	r = UPNP_GetOutboundPinholeTimeout(urls->controlURL_6FC, data->IPv6FC.servicetype, remoteaddr, eport, intaddr, iport, proto, &timeout);
	if(r!=UPNPCOMMAND_SUCCESS)
		LOGI("GetOutboundPinholeTimeout([%s]:%s -> [%s]:%s) failed with code %d (%s)",
		       intaddr, iport, remoteaddr, eport, r, strupnperror(r));
	else
		LOGI("GetOutboundPinholeTimeout: ([%s]:%s -> [%s]:%s) / Timeout = %d", intaddr, iport, remoteaddr, eport, timeout);
}

void
GetPinholePackets(struct UPNPUrls * urls,
               struct IGDdatas * data, const char * uniqueID)
{
	int r, pinholePackets = 0;
	if(!uniqueID)
	{
		LOGI("invalid arguments");
		return;
	}
	r = UPNP_GetPinholePackets(urls->controlURL_6FC, data->IPv6FC.servicetype, uniqueID, &pinholePackets);
	if(r!=UPNPCOMMAND_SUCCESS)
		LOGI("GetPinholePackets() failed with code %d (%s)", r, strupnperror(r));
	else
		LOGI("GetPinholePackets: Pinhole ID = %s / PinholePackets = %d", uniqueID, pinholePackets);
}

void
CheckPinhole(struct UPNPUrls * urls,
               struct IGDdatas * data, const char * uniqueID)
{
	int r, isWorking = 0;
	if(!uniqueID)
	{
		LOGI("invalid arguments");
		return;
	}
	r = UPNP_CheckPinholeWorking(urls->controlURL_6FC, data->IPv6FC.servicetype, uniqueID, &isWorking);
	if(r!=UPNPCOMMAND_SUCCESS)
		LOGI("CheckPinholeWorking() failed with code %d (%s)", r, strupnperror(r));
	else
		LOGI("CheckPinholeWorking: Pinhole ID = %s / IsWorking = %s", uniqueID, (isWorking)? "Yes":"No");
}

void
RemovePinhole(struct UPNPUrls * urls,
               struct IGDdatas * data, const char * uniqueID)
{
	int r;
	if(!uniqueID)
	{
		LOGI("invalid arguments");
		return;
	}
	r = UPNP_DeletePinhole(urls->controlURL_6FC, data->IPv6FC.servicetype, uniqueID);
	LOGI("UPNP_DeletePinhole() returned : %d", r);
}
