LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := yuv
LOCAL_SRC_FILES := libyuv/lib/$(TARGET_ARCH_ABI)/libyuv.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/libyuv/include
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := crypto
LOCAL_SRC_FILES := openssl/lib/armeabi-v7a/libcrypto.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/openssl/include
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := ssl
LOCAL_SRC_FILES := openssl/lib/armeabi-v7a/libssl.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/openssl/include
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := expat
LOCAL_SRC_FILES := expat-2.1.0/lib/armeabi-v7a/libexpat.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/expat-2.1.0/include
include $(PREBUILT_STATIC_LIBRARY)

#include $(CLEAR_VARS)
#LOCAL_MODULE := jingle
#LOCAL_SRC_FILES := libjingle/lib/armv7/libjingle.a
#LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/libjingle
#include $(PREBUILT_STATIC_LIBRARY)
include $(LOCAL_PATH)/jingle.mk

include $(CLEAR_VARS)
LOCAL_MODULE := miniupnp
LOCAL_SRC_FILES := miniupnp/lib/$(TARGET_ARCH_ABI)/libminiupnp.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/miniupnp/include
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := opus
LOCAL_SRC_FILES := opus/lib/$(TARGET_ARCH_ABI)/libopus.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/opus/include
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := vpx
LOCAL_SRC_FILES := vpx/lib/armeabi-v7a/libvpx.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/vpx/include
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp
LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_store_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_profile_static

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a) 
LOCAL_ARM_NEON := true
endif

LOCAL_CPPFLAGS += -DHAMMER_TIME=1 -DLOGGING=1 -DFEATURE_ENABLE_SSL  -DUSE_OPENSSL=1 -DSSL_USE_OPENSSL\
-DWEBRTC_POSIX -D_REENTRANT -DWEBRTC_LINUX -D_DEBUG \
-DXML_STATIC -DEXPAT_RELATIVE_PATH \
-DWEBRTC_ANDROID -D_STLP_NO_ANACHRONISMS=1 -D_STLP_USE_DYNAMIC_LIB=1 -DHAVE_OPENSSL_SSL_H \
-DEXPAT_RELATIVE_PATH -DFEATURE_VISTA -DCOCOS2D_DEBUG

LOCAL_SRC_FILES := main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/stream/Stream.cpp \
                   ../../Classes/stream/StreamPacket.cpp \
                   ../../Classes/stream/GFrameRecvHelper.cpp \
                   ../../Classes/stream/GFrameSendHelper.cpp \
                   ../../Classes/stream/P2PStreamEngine.cpp \
                   ../../Classes/codec/VPDecoder.cpp \
                   ../../Classes/codec/VPEncoder.cpp \
                   ../../Classes/codec/VoiceDecoder.cpp \
                   ../../Classes/codec/VoiceEncoder.cpp \
                   ../../Classes/controller/CustomXmppTask.cpp \
                   ../../Classes/controller/XmppMainHandler.cpp \
                   ../../Classes/controller/XmppLoginThread.cpp \
                   ../../Classes/controller/CallStateHandler.cpp \
                   ../../Classes/controller/UpnpPortHandler.cpp \
                   ../../Classes/controller/HttpRequestHandler.cpp \
                   ../../Classes/controller/P2PChannelAllocator.cpp \
                   ../../Classes/controller/P2PStreamer.cpp \
                   ../../Classes/datatype/SignupInfo.cpp \
                   ../../Classes/datatype/InviteEventData.cpp \
                   ../../Classes/view/MainScene.cpp \
                   ../../Classes/view/PreviewScreen.cpp \
                   ../../Classes/view/DecodeScreen.cpp \
                   ../../Classes/view/YUVSprite.cpp\
                   ../../Classes/view/ToggleButton.cpp \
                   ../../Classes/view/EmoticonMenuButton.cpp\
                   ../../Classes/view/SliderEx.cpp\
                   ../../Classes/view/DialogEx.cpp\
                   ../../Classes/view/PageViewEx.cpp\
                   ../../Classes/view/UntouchablePageView.cpp\
                   ../../Classes/view/ContactDialog.cpp\
                   ../../Classes/view/InviteDialog.cpp\
                   ../../Classes/view/InstallPage.cpp\
                   ../../Classes/view/WarningDialog.cpp\
                   ../../Classes/view/Alert.cpp\
                   ../../Classes/view/LicenseDialog.cpp\
                   ../../Classes/view/WebBrowser.cpp\
                   ../../Classes/view/Toast.cpp\
                   ../../Classes/view/TimerSprite.cpp\
                   ../../Classes/view/ActionQueue.cpp\
                   ../../Classes/view/MainMenu.cpp \
                   ../../Classes/view/EffectMenu.cpp \
                   ../../Classes/view/EmoticonMenu.cpp \
                   ../../Classes/view/SettingMenu.cpp \
                   ../../Classes/view/RecordSettingMenu.cpp \
                   ../../Classes/view/CaptureMenu.cpp \
                   ../../Classes/view/ExitMenu.cpp \
                   ../../Classes/view/HangupMenu.cpp \
                   ../../Classes/view/InfiniteProgressBar.cpp \
                   ../../Classes/view/ViewFinder.cpp \
                   ../../Classes/extra/Strings.cpp \
                   ../../Classes/util/IdentityManager.cpp \
                   ../../Classes/util/EmoticonManager.cpp \
                   ../../Classes/util/StickerManager.cpp \
                   ../../Classes/util/DecorationManager.cpp \
                   ../../Classes/util/Tokenizer.cpp \
                   ../../Classes/util/HexString.cpp \
                   ../../Classes/util/BitrateMonitor.cpp \
                   ../../Classes/util/GLProgramManager.cpp \
                   ../../Classes/util/FaceDetectHelper.cpp \
                   ../../Classes/util/AndroidMP3Decoder.cpp \
                   ../../Classes/gif/Bitmap.cpp \
                   ../../Classes/gif/CacheGif.cpp \
                   ../../Classes/gif/GifBase.cpp \
                   ../../Classes/gif/GifMovie.cpp \
                   ../../Classes/gif/InstantGif.cpp \
                   ../../Classes/gif/Movie.cpp \
                   ../../Classes/gif/GifEncoder.cpp \
                   ../../Classes/gif/gif_lib/dgif_lib.c \
                   ../../Classes/gif/gif_lib/gif_font.c \
                   ../../Classes/gif/gif_lib/gif_hash.c \
                   ../../Classes/gif/gif_lib/gifalloc.c \
                   ../../Classes/gif/gif_lib/quantize.c \
                   ../../Classes/gif/gif_lib/gif_encode.cpp                

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes \
					$(LOCAL_PATH)/../../Classes/util \
                    $(LOCAL_PATH)/../../cocos2d/extensions \
                    $(LOCAL_PATH)/../../cocos2d/cocos/network \
                    $(LOCAL_PATH)/../../cocos2d/cocos/platform/android \
                    $(LOCAL_PATH)/miniupnp/include \
                    $(LOCAL_PATH)/opus/include \
                    $(LOCAL_PATH)/openssl/include \
                    $(LOCAL_PATH)/vpx/include \
                    $(LOCAL_PATH)/libyuv/include \
                    $(LOCAL_PATH)/libjingle/expat-2.1.0/lib \
                    $(LOCAL_PATH)



LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/../../Classes \
                           $(LOCAL_PATH)/../../Classes/util \
                           $(LOCAL_PATH)/../../cocos2d/extensions \
                           $(LOCAL_PATH)/../../cocos2d/cocos/network \
                    	   $(LOCAL_PATH)/../../cocos2d/cocos/platform/android \
                           $(LOCAL_PATH)/miniupnp/include \
                           $(LOCAL_PATH)/opus/include \
                    	   $(LOCAL_PATH)/openssl/include \
                           $(LOCAL_PATH)/vpx/include \
                           $(LOCAL_PATH)/libyuv/include \
                           $(LOCAL_PATH)/libjingle/expat-2.1.0/lib \
                           $(LOCAL_PATH)


LOCAL_STATIC_LIBRARIES := cocos2dx_static jingle miniupnp opus vpx openal yuv crypto ssl
	

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
$(call import-module,extensions/cocos2dx-store)
$(call import-module,extensions/cocos2dx-profile)