#include "AppDelegate.h"
#include "cocos2d.h"
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <android/log.h>
#include "libyuv.h"
#include "util/AndroidMP3Decoder.h"

#define  LOG_TAG    "main"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

using namespace cocos2d;
AppDelegate *pAppDelegate;
void cocos_android_app_init (JNIEnv* env, jobject thiz) {
    LOGD("cocos_android_app_init");
    pAppDelegate = new AppDelegate();
}


extern "C" {

	JNIEXPORT void JNICALL Java_com_BitLive_AppActivity_onOfflineMessage(JNIEnv*  env, jobject thiz, jstring message) {

	    const char *str = env->GetStringUTFChars(message, NULL);
        Device::setOfflineMessage(str);
    	env->ReleaseStringUTFChars(message, str);
	}
	JNIEXPORT jint JNICALL Java_org_cocos2dx_lib_Cocos2dxCameraPreview_updatePreviewFrame(JNIEnv*  env, jobject thiz,
			jobject outputBuffer, jobject inputBuffer, jint src_width, jint src_height, jint dst_width, jint dst_height, jint rotation) {
		int bytesPiexel = src_width*src_height;
		int halfSrcWidth = src_width/2;
		int halfDstWidth = dst_width/2;
		unsigned char* src_y = (unsigned char*)env->GetDirectBufferAddress(inputBuffer);
		unsigned char* src_u = src_y + bytesPiexel;
		unsigned char* src_v = src_u + bytesPiexel/4;
		unsigned char* dst_y = (unsigned char*)env->GetDirectBufferAddress(outputBuffer);
		unsigned char* dst_u = dst_y + bytesPiexel;
		unsigned char* dst_v = dst_u + bytesPiexel/4;

	    libyuv::I420Rotate(src_y, src_width,
	                     src_u, halfSrcWidth,
	                     src_v, halfSrcWidth,
	                     dst_y, dst_width,
						 dst_u, halfDstWidth,
						 dst_v, halfDstWidth,
						 src_width, src_height, (libyuv::RotationMode)rotation);
		return Device::updateFrame(dst_y, dst_u, dst_v);

	}
	JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxCameraPreview_getVideoFrame(JNIEnv*  env, jobject thiz,
			jobject outputBuffer, jint dst_width, jint dst_height) {
		unsigned char* dst_y = (unsigned char*)env->GetDirectBufferAddress(outputBuffer);
		unsigned char* dst_uv = dst_y + dst_width*dst_height;
		Device::getVideoFrame(dst_y, dst_uv);
	}

	JNIEXPORT jint JNICALL Java_org_cocos2dx_lib_Cocos2dxFaceDetector_convertI420ToABGR(JNIEnv*  env, jobject thiz,
			jintArray outputArray, jobject inputBuffer, jint width, jint height) {
		int bytesPiexel = width*height;
		int halfWidth = width/2;
		unsigned char* src_y = (unsigned char*)env->GetDirectBufferAddress(inputBuffer);
		unsigned char* src_u = src_y + bytesPiexel;
		unsigned char* src_v = src_u + bytesPiexel/4;
		jint* intArray = (jint*)env->GetIntArrayElements(outputArray, 0 );

		int ret = libyuv::I420ToABGR(src_y, width,
		                 	 	 	 src_u, halfWidth,
									 src_v, halfWidth,
									 (uint8*)intArray, width*4,
									 width, height);
		env->ReleaseIntArrayElements(outputArray, intArray, 0);
		return ret;
	}

	JNIEXPORT jint JNICALL Java_org_cocos2dx_lib_VoiceRecorder_updatePCM(JNIEnv*  env, jobject thiz, jobject pcmBuffer) {
		return Device::updatePCM((short*) env->GetDirectBufferAddress(pcmBuffer));
	}

	JNIEXPORT void JNICALL Java_org_cocos2dx_lib_VoiceRecorder_mixPCM(JNIEnv*  env, jobject thiz, jobject pcmBuffer, jint size) {
		AndroidMP3Decoder::getInstance()->mixPcmBuffer((short*) env->GetDirectBufferAddress(pcmBuffer), size);
	}

	JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxFaceDetector_updateFaceDetect(JNIEnv*  env, jobject thiz,
			jfloat leftX, jfloat leftY, jfloat rightX, jfloat rightY) {

		Device::updateFace(cocos2d::Vec2(leftX, leftY), cocos2d::Vec2(rightX, rightY));
	}

	JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxCameraPreview_completeRecord(JNIEnv*  env, jobject thiz, jboolean result) {

		Device::complateRecord(result);
	}

	JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxCameraPreview_onRelease(JNIEnv*  env, jobject thiz) {

		if(pAppDelegate==NULL) {

			LOGD("pAppDelegate is NULL");
			return;
		}

		pAppDelegate->exit();
	}

	JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxHelper_nativeResultContactList(JNIEnv*  env, jobject thiz, jobject contactList,
			jbyteArray photoArray, jint arrayLength) {
		// retrieve the java.util.List interface class
		jclass cList = env->FindClass("java/util/List");

		// retrieve the toArray method and invoke it
		jmethodID mToArray = env->GetMethodID(cList, "toArray", "()[Ljava/lang/Object;");
		if(mToArray == NULL)
		{
		    return;
		}
		jobjectArray array = (jobjectArray)env->CallObjectMethod(contactList, mToArray);

		// now create the string array
		std::string* sArray = new std::string[env->GetArrayLength(array)];
		for(int i=0;i<env->GetArrayLength(array);i++) {
		    // retrieve the chars of the entry strings and assign them to the array!
		    jstring strObj = (jstring)env->GetObjectArrayElement(array, i);
		    const char * chr = env->GetStringUTFChars(strObj, JNI_FALSE);
		    sArray[i].append(chr);
		    env->ReleaseStringUTFChars(strObj, chr);
		}

		std::vector<std::string>* list = new std::vector<std::string>();
		// just print the array to std::cout
		for(int i=0;i<env->GetArrayLength(array);i++) {
			list->push_back(sArray[i].c_str());
			LOGD("result : %s", sArray[i].c_str());
		}

		if(!Device::onContactListCallback)
		{
			CCLOG("Device::onContractListCallback is nullptr!!");
			return;
		}

		if(photoArray)
		{
			jbyte *byteBuffer = (jbyte *)env->GetByteArrayElements(photoArray, NULL);
			unsigned char* pictureBuffer = (unsigned char*)malloc(arrayLength);
			int pictureSize = arrayLength;
			memcpy(pictureBuffer, (unsigned char*)byteBuffer, arrayLength);
			Device::onContactListCallback(Device::Res::CheckContactList, list, pictureBuffer, arrayLength);
			env->ReleaseByteArrayElements(photoArray, byteBuffer, 0 );
		}
		else
		{
			Device::onContactListCallback(Device::Res::CheckContactList, list, nullptr, 0);
		}
	}

	JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxHelper_nativeSearchContactResult(JNIEnv*  env, jobject thiz, jstring name,
			jbyteArray photoArray, jint arrayLength) {
	    const char *str = env->GetStringUTFChars(name, NULL);
		std::vector<std::string>* list = new std::vector<std::string>();
		// just print the array to std::cout
		list->push_back(str);

		if(!Device::onContactListCallback)
		{
			CCLOG("Device::onContractListCallback is nullptr!!");
			return;
		}

		if(photoArray)
		{
			jbyte *byteBuffer = (jbyte *)env->GetByteArrayElements(photoArray, NULL);
			unsigned char* pictureBuffer = (unsigned char*)malloc(arrayLength);
			int pictureSize = arrayLength;
			memcpy(pictureBuffer, (unsigned char*)byteBuffer, arrayLength);
			Device::onContactListCallback(Device::Res::SearchContact, list, pictureBuffer, arrayLength);
			env->ReleaseByteArrayElements(photoArray, byteBuffer, 0 );
		}
		else
		{
			Device::onContactListCallback(Device::Res::SearchContact, list, nullptr, 0);
		}

    	env->ReleaseStringUTFChars(name, str);
	}
}
