
                  
###########################################################
# build libjingle itself
#    
include $(CLEAR_VARS)        
LOCAL_MODULE := jingle
WEBRTC_SRC := ../../libjingle-ios

LOCAL_CPPFLAGS := -DHAMMER_TIME=1 -DFEATURE_ENABLE_SSL -DUSE_OPENSSL=1 -DSSL_USE_OPENSSL\
				  -DWEBRTC_POSIX -D_REENTRANT -DWEBRTC_LINUX -D_DEBUG \
                    -DXML_STATIC -DEXPAT_RELATIVE_PATH \
				  -DWEBRTC_ANDROID -D_STLP_NO_ANACHRONISMS=1 -D_STLP_USE_DYNAMIC_LIB=1 -DHAVE_OPENSSL_SSL_H \
				  -DFEATURE_VISTA


LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/expat-2.1.0/include \
	$(LOCAL_PATH)/openssl/include \
	$(JNI_H_INCLUDE) \
	$(WEBRTC_SRC) \
	$(WEBRTC_SRC)/webrtc/jsoncpp/include \

	
LOCAL_EXPORT_C_INCLUDES :=  $(LOCAL_PATH)/openssl/include \
	$(WEBRTC_SRC)

LOCAL_CPP_EXTENSION := .cc             
      
LOCAL_STATIC_LIBRARIES := \
	cpufeatures \
	expat \
    ssl \
    crypto

LOCAL_SHARED_LIBRARIES :=

#LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog

#including source files    
LOCAL_SRC_FILES := \
	$(WEBRTC_SRC)/webrtc/base/asyncfile.cc \
	$(WEBRTC_SRC)/webrtc/base/asynchttprequest.cc \
	$(WEBRTC_SRC)/webrtc/base/asyncinvoker.cc \
	$(WEBRTC_SRC)/webrtc/base/asyncpacketsocket.cc \
	$(WEBRTC_SRC)/webrtc/base/asyncresolverinterface.cc \
	$(WEBRTC_SRC)/webrtc/base/asyncsocket.cc \
	$(WEBRTC_SRC)/webrtc/base/asynctcpsocket.cc \
	$(WEBRTC_SRC)/webrtc/base/asyncudpsocket.cc \
	$(WEBRTC_SRC)/webrtc/base/autodetectproxy.cc \
	$(WEBRTC_SRC)/webrtc/base/bandwidthsmoother.cc \
	$(WEBRTC_SRC)/webrtc/base/base64.cc \
	$(WEBRTC_SRC)/webrtc/base/bitbuffer.cc \
	$(WEBRTC_SRC)/webrtc/base/buffer.cc \
	$(WEBRTC_SRC)/webrtc/base/bytebuffer.cc \
    $(WEBRTC_SRC)/webrtc/base/checks.cc \
    $(WEBRTC_SRC)/webrtc/base/common.cc \
    $(WEBRTC_SRC)/webrtc/base/cpumonitor.cc \
    $(WEBRTC_SRC)/webrtc/base/crc32.cc \
    $(WEBRTC_SRC)/webrtc/base/criticalsection.cc \
    $(WEBRTC_SRC)/webrtc/base/cryptstring.cc \
    $(WEBRTC_SRC)/webrtc/base/dbus.cc \
    $(WEBRTC_SRC)/webrtc/base/diskcache.cc \
    $(WEBRTC_SRC)/webrtc/base/event_tracer.cc \
    $(WEBRTC_SRC)/webrtc/base/event.cc \
    $(WEBRTC_SRC)/webrtc/base/exp_filter.cc \
    $(WEBRTC_SRC)/webrtc/base/filelock.cc \
    $(WEBRTC_SRC)/webrtc/base/fileutils.cc \
    $(WEBRTC_SRC)/webrtc/base/firewallsocketserver.cc \
    $(WEBRTC_SRC)/webrtc/base/flags.cc \
    $(WEBRTC_SRC)/webrtc/base/helpers.cc \
    $(WEBRTC_SRC)/webrtc/base/httpbase.cc \
    $(WEBRTC_SRC)/webrtc/base/httpclient.cc \
    $(WEBRTC_SRC)/webrtc/base/httpcommon.cc \
    $(WEBRTC_SRC)/webrtc/base/httprequest.cc \
    $(WEBRTC_SRC)/webrtc/base/httpserver.cc \
    $(WEBRTC_SRC)/webrtc/base/ifaddrs-android.cc \
    $(WEBRTC_SRC)/webrtc/base/ipaddress.cc \
    $(WEBRTC_SRC)/webrtc/base/latebindingsymboltable.cc \
    $(WEBRTC_SRC)/webrtc/base/libdbusglibsymboltable.cc \
	$(WEBRTC_SRC)/webrtc/base/linux.cc \
	$(WEBRTC_SRC)/webrtc/base/linuxfdwalk.c \
	$(WEBRTC_SRC)/webrtc/base/logging.cc \
    $(WEBRTC_SRC)/webrtc/base/md5.cc \
    $(WEBRTC_SRC)/webrtc/base/messagedigest.cc \
    $(WEBRTC_SRC)/webrtc/base/messagehandler.cc \
    $(WEBRTC_SRC)/webrtc/base/messagequeue.cc \
    $(WEBRTC_SRC)/webrtc/base/multipart.cc \
    $(WEBRTC_SRC)/webrtc/base/natserver.cc \
    $(WEBRTC_SRC)/webrtc/base/natsocketfactory.cc \
    $(WEBRTC_SRC)/webrtc/base/nattypes.cc \
    $(WEBRTC_SRC)/webrtc/base/nethelpers.cc \
    $(WEBRTC_SRC)/webrtc/base/network.cc \
    $(WEBRTC_SRC)/webrtc/base/nssidentity.cc \
    $(WEBRTC_SRC)/webrtc/base/nssstreamadapter.cc \
    $(WEBRTC_SRC)/webrtc/base/openssldigest.cc \
    $(WEBRTC_SRC)/webrtc/base/openssladapter.cc \
    $(WEBRTC_SRC)/webrtc/base/opensslidentity.cc \
    $(WEBRTC_SRC)/webrtc/base/opensslstreamadapter.cc \
    $(WEBRTC_SRC)/webrtc/base/optionsfile.cc \
    $(WEBRTC_SRC)/webrtc/base/pathutils.cc \
    $(WEBRTC_SRC)/webrtc/base/physicalsocketserver.cc \
    $(WEBRTC_SRC)/webrtc/base/platform_file.cc \
    $(WEBRTC_SRC)/webrtc/base/posix.cc \
    $(WEBRTC_SRC)/webrtc/base/profiler.cc \
    $(WEBRTC_SRC)/webrtc/base/proxydetect.cc \
    $(WEBRTC_SRC)/webrtc/base/proxyinfo.cc \
    $(WEBRTC_SRC)/webrtc/base/proxyserver.cc \
    $(WEBRTC_SRC)/webrtc/base/ratelimiter.cc \
    $(WEBRTC_SRC)/webrtc/base/ratetracker.cc \
   	$(WEBRTC_SRC)/webrtc/base/sha1.cc \
   	$(WEBRTC_SRC)/webrtc/base/sha1digest.cc \
   	$(WEBRTC_SRC)/webrtc/base/sharedexclusivelock.cc \
   	$(WEBRTC_SRC)/webrtc/base/signalthread.cc \
   	$(WEBRTC_SRC)/webrtc/base/sigslot.cc \
	$(WEBRTC_SRC)/webrtc/base/socketadapters.cc \
   	$(WEBRTC_SRC)/webrtc/base/socketaddress.cc \
    $(WEBRTC_SRC)/webrtc/base/socketaddresspair.cc \
    $(WEBRTC_SRC)/webrtc/base/socketpool.cc \
    $(WEBRTC_SRC)/webrtc/base/socketstream.cc \
	$(WEBRTC_SRC)/webrtc/base/ssladapter.cc \
    $(WEBRTC_SRC)/webrtc/base/sslfingerprint.cc \
    $(WEBRTC_SRC)/webrtc/base/sslidentity.cc \
	$(WEBRTC_SRC)/webrtc/base/sslsocketfactory.cc \
	$(WEBRTC_SRC)/webrtc/base/sslstreamadapter.cc \
	$(WEBRTC_SRC)/webrtc/base/sslstreamadapterhelper.cc \
	$(WEBRTC_SRC)/webrtc/base/stream.cc \
	$(WEBRTC_SRC)/webrtc/base/stringencode.cc \
   	$(WEBRTC_SRC)/webrtc/base/stringutils.cc \
	$(WEBRTC_SRC)/webrtc/base/systeminfo.cc \
   	$(WEBRTC_SRC)/webrtc/base/task.cc \
   	$(WEBRTC_SRC)/webrtc/base/taskparent.cc \
   	$(WEBRTC_SRC)/webrtc/base/taskrunner.cc \
   	$(WEBRTC_SRC)/webrtc/base/testclient.cc \
   	$(WEBRTC_SRC)/webrtc/base/thread_checker_impl.cc \
   	$(WEBRTC_SRC)/webrtc/base/thread.cc \
   	$(WEBRTC_SRC)/webrtc/base/timeutils.cc \
   	$(WEBRTC_SRC)/webrtc/base/timing.cc \
   	$(WEBRTC_SRC)/webrtc/base/transformadapter.cc \
	$(WEBRTC_SRC)/webrtc/base/unixfilesystem.cc \
   	$(WEBRTC_SRC)/webrtc/base/urlencode.cc \
   	$(WEBRTC_SRC)/webrtc/base/versionparsing.cc \
   	$(WEBRTC_SRC)/webrtc/base/virtualsocketserver.cc \
   	$(WEBRTC_SRC)/webrtc/base/worker.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/asyncstuntcpsocket.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/basicpacketsocketfactory.cc \
   	$(WEBRTC_SRC)/webrtc/p2p/base/constants.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/dtlstransportchannel.cc \
   	$(WEBRTC_SRC)/webrtc/p2p/base/p2ptransport.cc \
   	$(WEBRTC_SRC)/webrtc/p2p/base/p2ptransportchannel.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/port.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/portallocator.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/rawtransport.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/rawtransportchannel.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/relayport.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/session.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/sessiondescription.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/stun.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/stunport.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/stunrequest.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/tcpport.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/transport.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/transportchannel.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/transportchannelproxy.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/transportdescription.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/transportdescriptionfactory.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/turnport.cc \
	$(WEBRTC_SRC)/webrtc/p2p/base/udpport.cc \
	$(WEBRTC_SRC)/webrtc/p2p/client/basicportallocator.cc \
	$(WEBRTC_SRC)/webrtc/p2p/client/connectivitychecker.cc \
	$(WEBRTC_SRC)/webrtc/p2p/client/httpportallocator.cc \
	$(WEBRTC_SRC)/webrtc/p2p/client/socketmonitor.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmllite/qname.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmllite/xmlbuilder.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmllite/xmlconstants.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmllite/xmlelement.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmllite/xmlnsstack.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmllite/xmlparser.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmllite/xmlprinter.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/constants.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/hangoutpubsubclient.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/iqtask.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/jid.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/moduleimpl.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/mucroomconfigtask.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/mucroomdiscoverytask.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/mucroomlookuptask.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/mucroomuniquehangoutidtask.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/pingtask.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/presenceouttask.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/presencereceivetask.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/presencestatus.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/pubsub_task.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/pubsubclient.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/pubsubstateclient.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/pubsubtasks.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/receivetask.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/rostermoduleimpl.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/saslmechanism.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/xmppclient.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/xmppengineimpl.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/xmppengineimpl_iq.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/xmpplogintask.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/xmppstanzaparser.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/xmpp/xmpptask.cc \
    $(WEBRTC_SRC)/webrtc/libjingle/xmpp/xmppauth.cc \
    $(WEBRTC_SRC)/webrtc/libjingle/xmpp/xmpppump.cc \
    $(WEBRTC_SRC)/webrtc/libjingle/xmpp/xmppsocket.cc \
    $(WEBRTC_SRC)/webrtc/libjingle/xmpp/xmppthread.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/session/constants.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/session/p2ptransportparser.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/session/parsing.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/session/rawtransportparser.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/session/sessionmessages.cc \
	$(WEBRTC_SRC)/webrtc/libjingle/session/transportparser.cc
include $(BUILD_STATIC_LIBRARY)
