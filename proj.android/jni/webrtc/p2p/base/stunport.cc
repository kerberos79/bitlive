/*
 *  Copyright 2004 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#include "webrtc/p2p/base/stunport.h"

#include "webrtc/p2p/base/common.h"
#include "webrtc/p2p/base/portallocator.h"
#include "webrtc/p2p/base/stun.h"
#include "webrtc/base/common.h"
#include "webrtc/base/helpers.h"
#include "webrtc/base/logging.h"
#include "webrtc/base/nethelpers.h"

namespace cricket {
    
    StunPort* StunPort::Create(rtc::Thread* thread,
                            rtc::PacketSocketFactory* factory,
                            rtc::Network* network,
                            const rtc::IPAddress& ip,
                            uint16 min_port, uint16 max_port,
                            const std::string& username,
                            const std::string& password,
                            const ServerAddresses& servers,
                            const std::string& origin) {
        StunPort* port = new StunPort(thread, factory, network,
                                      ip, min_port, max_port,
                                      username, password, servers,
                                      origin);
        if (!port->Init()) {
            delete port;
            port = NULL;
        }
        return port;
    }
    void StunPort::PrepareAddress() {
        SendStunBindingRequests();
    }
    StunPort::StunPort(rtc::Thread* thread,
             rtc::PacketSocketFactory* factory,
             rtc::Network* network,
             const rtc::IPAddress& ip,
             uint16 min_port,
             uint16 max_port,
             const std::string& username,
             const std::string& password,
             const ServerAddresses& servers,
             const std::string& origin)
    : UDPPort(thread, factory, network, ip, min_port, max_port, username,
              password, origin) {
        // UDPPort will set these to local udp, updating these to STUN.
        set_type(STUN_PORT_TYPE);
        set_server_addresses(servers);
    }

}  // namespace cricket
