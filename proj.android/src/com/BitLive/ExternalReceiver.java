package com.BitLive;

import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;

public class ExternalReceiver  extends BroadcastReceiver {
	private final static String TAG = "ExternalReceiver";
	public static final String  XMPP_SERVER_ADDRESS = "http://52.68.201.252:9090";
	public static final String  XMPP_PUSH_SECRET = "2lW5ul4Q";
	private SharedPreferences setting = null;
	private SharedPreferences.Editor editor;
    public void onReceive(Context context, Intent intent) {
    	
    	try {

	        if(intent!=null){
	            String action = intent.getAction();
	            if (action.equals("com.google.android.c2dm.intent.REGISTRATION"))
	            {
	            }
	            else if (action.equals("com.google.android.c2dm.intent.RECEIVE"))
	            {
		            String message = "";
		            Bundle extras = intent.getExtras();
		            
		            if(extras!=null){
			            message = extras.getString("msg");
			            if(message==null || message.startsWith("error"))
			            	throw new Exception();
						if(message.startsWith("I:"))
						{
							if(getOfflineMessage(context).length()>0)
							{
								storeHiddenMessage(context, message);
							}
							else
							{
								storeOfflineMessage(context, message);
								showAlertDialog(context, message);
							}
						}
						else
						{
							storeHiddenMessage(context, message);
						}
		            }
	            }
	        }
    	} catch (Exception e) {
    		
    	} finally {
            setResultCode(Activity.RESULT_OK);
    	}
    }
    
    private String getOfflineMessage(Context context) throws Exception
    {
    	if(setting==null)
    	{
    		setting = context.getSharedPreferences("Cocos2dxPrefsFile", 0);
    		editor = setting.edit();
    	}
    	return setting.getString("offlinemessage", "");
    }

    private String getLoginId(Context context) throws Exception
    {
    	if(setting==null)
    	{
    		setting = context.getSharedPreferences("Cocos2dxPrefsFile", 0);
    		editor = setting.edit();
    	}
    	return setting.getString("loginid", "");
    }
    
    private void storeOfflineMessage(Context context, String message) throws Exception
    {
    	if(setting==null)
    	{
    		setting = context.getSharedPreferences("Cocos2dxPrefsFile", 0);
    		editor = setting.edit();
    	}
    	editor.putString("offlinemessage", message);
    	editor.commit();
    }
    
    private String resetOfflineMessage(Context context) throws Exception
    {
    	String offlinemessage;
    	if(setting==null)
    	{
    		setting = context.getSharedPreferences("Cocos2dxPrefsFile", 0);
    		editor = setting.edit();
    	}

    	offlinemessage = setting.getString("offlinemessage", "");
    	editor.putString("offlinemessage", "");
    	editor.commit();
    	
    	return offlinemessage;
    }
    
    private void storeHiddenMessage(Context context, String message) throws Exception
    {
    	if(setting==null)
    	{
    		setting = context.getSharedPreferences("Cocos2dxPrefsFile", 0);
    		editor = setting.edit();
    	}
    	int num = setting.getInt("hiddenmessagenum", 0) + 1;
    	editor.putString("hiddenmessage"+num, message);
    	editor.commit();
    }
    
    private void showAlertDialog(final Context context, String message)
    {
    	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
    	alertDialogBuilder.setIcon(R.drawable.icon);
    	
    	alertDialogBuilder.setTitle(context.getString(R.string.popup_title)+makeUserId(message));
    	alertDialogBuilder.setCancelable(false);
    	alertDialogBuilder.setPositiveButton(context.getString(R.string.popup_launch), new DialogInterface.OnClickListener() {

    	    @Override
    	    public void onClick(DialogInterface dialog, int which) {

    	        Intent intent = new Intent(context.getApplicationContext(), AppActivity.class);
    	        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	        context.startActivity(intent);
    	    }
    	});
    	 
    	alertDialogBuilder.setNegativeButton(context.getString(R.string.popup_cancel), new DialogInterface.OnClickListener() {
    	 
    	    @Override
    	    public void onClick(DialogInterface dialog, int which) {
    	    	try {
    	    		String msg = resetOfflineMessage(context);
    	    		/*
    	    		 * 아직 서버에서 처리가 안 됨.
    	    		String loginId = getLoginId(context);
    	    		StringTokenizer token = new StringTokenizer(msg, ":");
    	    		token.nextToken();
    	    		String remote_user = token.nextToken();
    	    		msg = "C:"+loginId+":"+token.nextToken();
					sendMessageViaHttp(context, remote_user, msg);
					*/
				} catch (Exception e) {
					e.printStackTrace();
				}
    	    }
    	});
    	 
    	AlertDialog alertDialog = alertDialogBuilder.create();
    	alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL|WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
    	if(((PowerManager)context.getSystemService(Context.POWER_SERVICE)).isScreenOn())
    		alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        WindowManager.LayoutParams wmlp = alertDialog.getWindow().getAttributes();

        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        alertDialog.getWindow().setAttributes(wmlp);
    	alertDialog.show();
    	try{
			Vibrator vib = (Vibrator) context.getSystemService (Service.VIBRATOR_SERVICE);
			vib.vibrate(2000);
    	}catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    
    private String makeUserId(String message)
    {
    	String result = "";
		StringTokenizer token = new StringTokenizer(message, ":");
		token.nextToken();
		String remote_user = token.nextToken();
		
		if(remote_user.endsWith(".gmail"))
		{
			result = remote_user.replace(".gmail", "@gmail.com");
		}
		else
		{
			result = "+"+ remote_user;
		}
		
		return result;
    }
    
    private void showNotification(Context context, String message)
    {
	    PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
	            new Intent(context, AppActivity.class), 0);
		NotificationCompat.Builder mBuilder =
	            new NotificationCompat.Builder(context)
				.setSmallIcon(R.drawable.icon)	
	            .setContentTitle("Bitlive")
	            .setContentText(message)
	            .setContentIntent(contentIntent)
	            .setAutoCancel(true)
	            .setDefaults(Notification.DEFAULT_SOUND);

	        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	        mNotificationManager.notify(1, mBuilder.build());
    }
    
    private void launchActivity(Context context, String message)
    {
        Intent intent2 = new Intent(context.getApplicationContext(), AppActivity.class);
        intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent2.putExtra("msg", message);
        context.startActivity(intent2);
    }
    
	public void sendMessageViaHttp(final Context context, final String id, final String msg) {
		try {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if(id!=null) {
						String result = HttpUtil.DownloadHtml(XMPP_SERVER_ADDRESS+
						"/plugins/push/pushservice?type=msg&secret="+XMPP_PUSH_SECRET+"&username="+id+"&param="+msg);
						
						if(result==null) {
							Log.i(TAG, "http result is null");
						}
						else {
							Log.i(TAG, result);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
		} catch(Exception e) {
			Log.i(TAG, e.getMessage());
		}
	}
}
