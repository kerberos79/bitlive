/****************************************************************************
 Copyright (c) 2014 Chukong Technologies Inc.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "UIWebViewImpl-ios.h"
#include "renderer/CCRenderer.h"
#include "base/CCDirector.h"
#include "platform/CCGLView.h"
#include "platform/ios/CCEAGLView-ios.h"
#include "platform/CCFileUtils.h"
#include "ui/UIWebView.h"

@interface UIWebViewWrapper : NSObject
@property (nonatomic) std::function<bool(std::string url)> shouldStartLoading;
@property (nonatomic) std::function<void(std::string url)> didFinishLoading;
@property (nonatomic) std::function<void(std::string url)> didFailLoading;
@property (nonatomic) std::function<void(std::string url)> onJsCallback;

@property(nonatomic, readonly, getter=canGoBack) BOOL canGoBack;
@property(nonatomic, readonly, getter=canGoForward) BOOL canGoForward;

+ (instancetype)webViewWrapper;

- (void)setVisible:(bool)visible;

- (void)setFrameWithX:(float)x y:(float)y width:(float)width height:(float)height;

- (void)setJavascriptInterfaceScheme:(const std::string &)scheme;

- (void)loadData:(const std::string &)data MIMEType:(const std::string &)MIMEType textEncodingName:(const std::string &)encodingName baseURL:(const std::string &)baseURL;

- (void)loadHTMLString:(const std::string &)string baseURL:(const std::string &)baseURL;

- (void)loadUrl:(const std::string &)urlString;

- (void)loadFile:(const std::string &)filePath;

- (void)stopLoading;

- (void)reload;

- (void)evaluateJS:(const std::string &)js;

- (void)goBack;

- (void)goForward;

- (void)setScalesPageToFit:(const bool)scalesPageToFit;
@end


@interface UIWebViewWrapper () <UIWebViewDelegate>
{
    CCEAGLView *mGLView;
    UIView *mPanel;
    CGRect mFrame;
    std::function<void(std::string url)> OnSaveHomepageListener;
    std::function<void(std::string url)> OnSendLinkListener;
    std::function<void()> OnCloseListener;
}
@property(nonatomic, retain) UIWebView *uiWebView;
@property(nonatomic, copy) NSString *jsScheme;
- (void) homepageAction:(UIButton *)sender;
- (void) sendUrlAction:(UIButton *)sender;
- (void) closeAction:(UIButton *)sender;
- (void) sendClipboardAction:(UIButton *)sender;
- (void) showMenu:(BOOL) onoff;

- (void)setOnSaveHomepageListener:(const std::function<void(std::string url)>&) callback;
- (void)setOnSendLinkListener:(const std::function<void(std::string url)>&) callback;
- (void)setOnCloseListener:(const std::function<void()>&) callback;
@end

@implementation UIWebViewWrapper {
    
}

+ (instancetype)webViewWrapper {
    return [[[self alloc] init] autorelease];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.uiWebView = nil;
        self.shouldStartLoading = nullptr;
        self.didFinishLoading = nullptr;
        self.didFailLoading = nullptr;
    }
    return self;
}

- (void)dealloc {
    self.uiWebView.delegate = nil;
    [self.uiWebView removeFromSuperview];
    self.jsScheme = nil;
    [super dealloc];
}

- (void)setupWebView {
    if (!self.uiWebView) {
        self.uiWebView = [[[UIWebView alloc] init] autorelease];
        self.uiWebView.delegate = self;
    }
    if (!self.uiWebView.superview) {
        auto view = cocos2d::Director::getInstance()->getOpenGLView();
        mGLView = (CCEAGLView *) view->getEAGLView();
        [mGLView addSubview:self.uiWebView];
    }
}

- (void)setVisible:(bool)visible {
    self.uiWebView.hidden = !visible;
}

- (void)setFrameWithX:(float)x y:(float)y width:(float)width height:(float)height {
    if (!self.uiWebView) {[self setupWebView];}
    CGRect newFrame = CGRectMake(x, y, width, height);
    if (!CGRectEqualToRect(self.uiWebView.frame, newFrame)) {
        self.uiWebView.frame = CGRectMake(x, y, width, height);
        float scale = width / 1080.0f;
        float cell_width = 180;
        float  cell_height = 45;
        NSBundle *bundle = [NSBundle mainBundle];
        CGPoint position = CGPointMake(0, 0);
        
        
        NSString *text = [bundle localizedStringForKey:@"homepage" value:@"" table:nil];
        NSInteger _stringLength=[text length];
        NSMutableAttributedString *homepageText = [[NSMutableAttributedString alloc] initWithString:text];
        // Normal font for the rest of the text
        [homepageText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14] forKey:NSFontAttributeName] range:NSMakeRange(0, _stringLength)];
        
        UIButton *homepagebutton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        homepagebutton.frame = CGRectMake(position.x, position.y, cell_width, cell_height);
        [homepagebutton setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]];
        [homepagebutton setAttributedTitle:homepageText
                        forState:UIControlStateNormal];
        [homepagebutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [homepagebutton addTarget:self action:@selector(homepageAction:) forControlEvents:UIControlEventTouchUpInside];
        
        position.y += cell_height ;
        
        text = [bundle localizedStringForKey:@"sendlink" value:@"" table:nil];
        _stringLength=[text length];
        NSMutableAttributedString *urlText = [[NSMutableAttributedString alloc] initWithString:text];
        // Normal font for the rest of the text
        [urlText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14] forKey:NSFontAttributeName] range:NSMakeRange(0, _stringLength)];
        
        UIButton *urlbutton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        urlbutton.frame = CGRectMake(position.x, position.y, cell_width, cell_height);
        [urlbutton setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]];
        [urlbutton setAttributedTitle:urlText
                   forState:UIControlStateNormal];
        [urlbutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [urlbutton addTarget:self action:@selector(sendUrlAction:) forControlEvents:UIControlEventTouchUpInside];
        
        position.y += cell_height ;
        
        text = [bundle localizedStringForKey:@"clipboard" value:@"" table:nil];
        _stringLength=[text length];
        NSMutableAttributedString *clipboardText = [[NSMutableAttributedString alloc] initWithString:text];
        // Normal font for the rest of the text
        [clipboardText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14] forKey:NSFontAttributeName] range:NSMakeRange(0, _stringLength)];
        
        UIButton *clipboardbutton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        clipboardbutton.frame = CGRectMake(position.x, position.y, cell_width, cell_height);
        [clipboardbutton setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]];
        [clipboardbutton setAttributedTitle:clipboardText
                   forState:UIControlStateNormal];
        [clipboardbutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [clipboardbutton addTarget:self action:@selector(sendUrlAction:) forControlEvents:UIControlEventTouchUpInside];
        
        position.y += cell_height ;
        
        text = [bundle localizedStringForKey:@"close" value:@"" table:nil];
        _stringLength=[text length];
        
        NSMutableAttributedString *closeText = [[NSMutableAttributedString alloc] initWithString:text];
        // Normal font for the rest of the text
        [closeText addAttributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14] forKey:NSFontAttributeName] range:NSMakeRange(0, _stringLength)];
        
        UIButton *closebutton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        closebutton.frame = CGRectMake(position.x, position.y, cell_width, cell_height);
        [closebutton setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]];
        [closebutton setAttributedTitle:closeText
                     forState:UIControlStateNormal];
        [closebutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [closebutton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
        position.y += cell_height ;
        
        mFrame = CGRectMake(width - cell_width , y, cell_width, position.y);
        mPanel = [[UIView alloc] initWithFrame:mFrame];
        mPanel.hidden = true;
        [mPanel setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]];
        
        [mPanel addSubview:homepagebutton];
        [mPanel addSubview:urlbutton];
        [mPanel addSubview:clipboardbutton];
        [mPanel addSubview:closebutton];
        [homepagebutton release];
        [urlbutton release];
        [closebutton release];
        [mGLView addSubview:mPanel];
    }
}

- (void) homepageAction:(UIButton *)sender {
    NSString *addr = [[self.uiWebView.request URL] absoluteString];
    OnSaveHomepageListener([addr UTF8String]);
    mPanel.hidden = true;
    
}
- (void) sendUrlAction:(UIButton *)sender {
    NSString *addr = [[self.uiWebView.request URL] absoluteString];
    OnSendLinkListener([addr UTF8String]);
    mPanel.hidden = true;
    
}
- (void) sendClipboardAction:(UIButton *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *string = pasteboard.string;
    if (string) {
        if(string.length < 1380)
        {
            OnSendLinkListener([string UTF8String]);
            mPanel.hidden = true;
        }
        else
        {
            CCLOG("String Size Overflow : %lu",(unsigned long)string.length);
        }
    }
    
}
- (void) closeAction:(UIButton *)sender {
    OnCloseListener();
    mPanel.hidden = true;
}
- (void) showMenu:(BOOL) onoff{
    mPanel.hidden = !onoff;
}

- (void)setJavascriptInterfaceScheme:(const std::string &)scheme {
    self.jsScheme = @(scheme.c_str());
}

- (void)loadData:(const std::string &)data MIMEType:(const std::string &)MIMEType textEncodingName:(const std::string &)encodingName baseURL:(const std::string &)baseURL {
    [self.uiWebView loadData:[NSData dataWithBytes:data.c_str() length:data.length()]
                    MIMEType:@(MIMEType.c_str())
            textEncodingName:@(encodingName.c_str())
                     baseURL:[NSURL URLWithString:@(baseURL.c_str())]];
}

- (void)loadHTMLString:(const std::string &)string baseURL:(const std::string &)baseURL {
    [self.uiWebView loadHTMLString:@(string.c_str()) baseURL:[NSURL URLWithString:@(baseURL.c_str())]];
}

- (void)loadUrl:(const std::string &)urlString {
    if (!self.uiWebView) {[self setupWebView];}
    NSURL *url = [NSURL URLWithString:@(urlString.c_str())];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.uiWebView loadRequest:request];
}

- (void)loadFile:(const std::string &)filePath {
    if (!self.uiWebView) {[self setupWebView];}
    NSURL *url = [NSURL fileURLWithPath:@(filePath.c_str())];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.uiWebView loadRequest:request];
}

- (void)stopLoading {
    [self.uiWebView stopLoading];
}

- (void)reload {
    [self.uiWebView reload];
}

- (BOOL)canGoForward {
    return self.uiWebView.canGoForward;
}

- (BOOL)canGoBack {
    return self.uiWebView.canGoBack;
}

- (void)goBack {
    [self.uiWebView goBack];
}

- (void)goForward {
    [self.uiWebView goForward];
}

- (void)evaluateJS:(const std::string &)js {
    if (!self.uiWebView) {[self setupWebView];}
    [self.uiWebView stringByEvaluatingJavaScriptFromString:@(js.c_str())];
}

- (void)setScalesPageToFit:(const bool)scalesPageToFit {
    self.uiWebView.scalesPageToFit = scalesPageToFit;
}


- (void)setOnSaveHomepageListener:(const std::function<void(std::string url)>&) callback {
    OnSaveHomepageListener = callback;
}
- (void)setOnSendLinkListener:(const std::function<void(std::string url)>&) callback {
    OnSendLinkListener = callback;
}
- (void)setOnCloseListener:(const std::function<void()>&) callback {
    OnCloseListener = callback;
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *url = [[request URL] absoluteString];
    if ([[[request URL] scheme] isEqualToString:self.jsScheme]) {
        self.onJsCallback([url UTF8String]);
        return NO;
    }
    if (self.shouldStartLoading && url) {
        return self.shouldStartLoading([url UTF8String]);
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    if (self.didFinishLoading) {
        NSString *url = [[webView.request URL] absoluteString];
        self.didFinishLoading([url UTF8String]);
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if (self.didFailLoading) {
        NSString *url = error.userInfo[NSURLErrorFailingURLStringErrorKey];
        if (url) {
            self.didFailLoading([url UTF8String]);
        }
    }
}

@end



namespace cocos2d {
namespace experimental {
    namespace ui{

WebViewImpl::WebViewImpl(WebView *webView)
        : _uiWebViewWrapper([UIWebViewWrapper webViewWrapper]),
        _webView(webView) {
    [_uiWebViewWrapper retain];
            
    _uiWebViewWrapper.shouldStartLoading = [this](std::string url) {
        if (this->_webView->_onShouldStartLoading) {
            return this->_webView->_onShouldStartLoading(this->_webView, url);
        }
        return true;
    };
    _uiWebViewWrapper.didFinishLoading = [this](std::string url) {
        if (this->_webView->_onDidFinishLoading) {
            this->_webView->_onDidFinishLoading(this->_webView, url);
        }
    };
    _uiWebViewWrapper.didFailLoading = [this](std::string url) {
        if (this->_webView->_onDidFailLoading) {
            this->_webView->_onDidFailLoading(this->_webView, url);
        }
    };
    _uiWebViewWrapper.onJsCallback = [this](std::string url) {
        if (this->_webView->_onJSCallback) {
            this->_webView->_onJSCallback(this->_webView, url);
        }
    };
}

WebViewImpl::~WebViewImpl(){
    [_uiWebViewWrapper release];
    _uiWebViewWrapper = nullptr;
}

void WebViewImpl::setJavascriptInterfaceScheme(const std::string &scheme) {
    [_uiWebViewWrapper setJavascriptInterfaceScheme:scheme];
}

void WebViewImpl::loadData(const Data &data,
                           const std::string &MIMEType,
                           const std::string &encoding,
                           const std::string &baseURL) {
    
    std::string dataString(reinterpret_cast<char *>(data.getBytes()), static_cast<unsigned int>(data.getSize()));
    [_uiWebViewWrapper loadData:dataString MIMEType:MIMEType textEncodingName:encoding baseURL:baseURL];
}

void WebViewImpl::loadHTMLString(const std::string &string, const std::string &baseURL) {
    [_uiWebViewWrapper loadHTMLString:string baseURL:baseURL];
}

void WebViewImpl::loadURL(const std::string &url) {
    [_uiWebViewWrapper loadUrl:url];
}

void WebViewImpl::loadFile(const std::string &fileName) {
    auto fullPath = cocos2d::FileUtils::getInstance()->fullPathForFilename(fileName);
    [_uiWebViewWrapper loadFile:fullPath];
}

void WebViewImpl::stopLoading() {
    [_uiWebViewWrapper stopLoading];
}

void WebViewImpl::reload() {
    [_uiWebViewWrapper reload];
}

bool WebViewImpl::canGoBack() {
    return _uiWebViewWrapper.canGoBack;
}

bool WebViewImpl::canGoForward() {
    return _uiWebViewWrapper.canGoForward;
}

void WebViewImpl::goBack() {
    [_uiWebViewWrapper goBack];
}

void WebViewImpl::goForward() {
    [_uiWebViewWrapper goForward];
}

void WebViewImpl::evaluateJS(const std::string &js) {
    [_uiWebViewWrapper evaluateJS:js];
}

void WebViewImpl::setScalesPageToFit(const bool scalesPageToFit) {
    [_uiWebViewWrapper setScalesPageToFit:scalesPageToFit];
}
        
void WebViewImpl::setOnSendLinkListener(const std::function<void(std::string url)>& callback) {
    [_uiWebViewWrapper setOnSendLinkListener:callback];
}
void WebViewImpl::setOnSaveHomepageListener(const std::function<void(std::string url)>& callback) {
    [_uiWebViewWrapper setOnSaveHomepageListener:callback];
}
void WebViewImpl::setOnCloseListener(const std::function<void()>& callback) {
    [_uiWebViewWrapper setOnCloseListener:callback];
}

void WebViewImpl::draw(cocos2d::Renderer *renderer, cocos2d::Mat4 const &transform, uint32_t flags) {
    if (flags & cocos2d::Node::FLAGS_TRANSFORM_DIRTY) {
        
        auto direcrot = cocos2d::Director::getInstance();
        auto glView = direcrot->getOpenGLView();
        auto frameSize = glView->getFrameSize();
        
        auto scaleFactor = [static_cast<CCEAGLView *>(glView->getEAGLView()) contentScaleFactor];

        auto winSize = direcrot->getWinSize();

        auto leftBottom = this->_webView->convertToWorldSpace(cocos2d::Vec2::ZERO);
        auto rightTop = this->_webView->convertToWorldSpace(cocos2d::Vec2(this->_webView->getContentSize().width, this->_webView->getContentSize().height));

        auto x = (frameSize.width / 2 + (leftBottom.x - winSize.width / 2) * glView->getScaleX()) / scaleFactor;
        auto y = (frameSize.height / 2 - (rightTop.y - winSize.height / 2) * glView->getScaleY()) / scaleFactor;
        auto width = (rightTop.x - leftBottom.x) * glView->getScaleX() / scaleFactor;
        auto height = (rightTop.y - leftBottom.y) * glView->getScaleY() / scaleFactor;

        [_uiWebViewWrapper setFrameWithX:x
                                      y:y
                                  width:width
                                 height:height];
    }
}

void WebViewImpl::setVisible(bool visible){
    [_uiWebViewWrapper setVisible:visible];
}
        
void WebViewImpl::showMenu(bool visible){
    [_uiWebViewWrapper showMenu:visible];
}
        
    } // namespace ui
} // namespace experimental
} //namespace cocos2d

#endif // CC_TARGET_PLATFORM == CC_PLATFORM_IOS
