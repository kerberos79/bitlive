//
//  CCCameraPreview.cpp
//  cocos2d_libs
//
//  Created by kerberos on 11/3/14.
//
//

#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC
#include "platform/CCCameraPreview.h"
#include <Foundation/Foundation.h>
#include <Cocoa/Cocoa.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include "base/ccTypes.h"
NS_CC_BEGIN

CameraPreview* CameraPreview::s_cameraPreview = nullptr;

CvCapture *(*ld_cvCreateCameraCapture)(int index);
double (*ld_cvGetCaptureProperty)( CvCapture* capture, int property_id );
int (*ld_cvSetCaptureProperty)( CvCapture* capture, int property_id, double value );
void (*ld_cvReleaseCapture)( CvCapture** capture );
IplImage *(*ld_cvQueryFrame)( CvCapture* capture );

CameraPreview* CameraPreview::getInstance()
{
    if(s_cameraPreview==nullptr)
    {
        s_cameraPreview = new CameraPreview();
    }
    return s_cameraPreview;
}

void CameraPreview::destroyInstance()
{
    delete s_cameraPreview;
    s_cameraPreview = nullptr;
}


CameraPreview::CameraPreview()
{
}

CameraPreview::~CameraPreview()
{
    
}

bool CameraPreview::init()
{
    void* libopencv_core_handle = dlopen("libopencv_core.2.4.10.dylib", RTLD_LOCAL|RTLD_LAZY);
    void* libopencv_highgui_handle = dlopen("libopencv_highgui.2.4.10.dylib", RTLD_LOCAL|RTLD_LAZY);
    
    ld_cvCreateCameraCapture = (CvCapture *(*)(int index))dlsym(libopencv_highgui_handle, "cvCreateCameraCapture");
    ld_cvGetCaptureProperty = (double (*)( CvCapture* capture, int property_id ))dlsym(libopencv_highgui_handle, "cvGetCaptureProperty");
    ld_cvSetCaptureProperty = (int (*)( CvCapture* capture, int property_id, double value ))dlsym(libopencv_highgui_handle, "cvSetCaptureProperty");
    ld_cvReleaseCapture = (void (*)( CvCapture** capture ))dlsym(libopencv_highgui_handle, "cvReleaseCapture");
    
    
    ld_cvQueryFrame = (IplImage *(*)(CvCapture* capture))dlsym(libopencv_highgui_handle, "cvQueryFrame");
    
    _camera = ld_cvCreateCameraCapture(CV_CAP_ANY);
    
    if (!_camera)
        return false;
    
    return true;
}

void CameraPreview::start()
{
    _current_frame = ld_cvQueryFrame (_camera);
    
    ld_cvReleaseCapture(&_camera);

}

NS_CC_END

#endif // CC_TARGET_PLATFORM == CC_PLATFORM_MAC

