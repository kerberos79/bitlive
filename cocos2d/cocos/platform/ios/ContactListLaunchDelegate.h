//
//  ContactListLaunchDelegate.h
//  cocos2d_libs
//
//  Created by kerberos on 1/20/15.
//
//

#ifndef cocos2d_libs_ContactListLaunchDelegate_h
#define cocos2d_libs_ContactListLaunchDelegate_h

#import <Foundation/Foundation.h>

@ protocol ContactListLaunchDelegate <NSObject>;
- (void) launch;
@ end

#endif
