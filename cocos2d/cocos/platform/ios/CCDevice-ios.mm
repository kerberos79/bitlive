/****************************************************************************
 Copyright (c) 2010-2012 cocos2d-x.org
 Copyright (c) 2013-2014 Chukong Technologies Inc.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/


#include "platform/CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "CCDevice.h"
#include "base/ccTypes.h"
#include "base/CCEventDispatcher.h"
#include "base/CCEventAcceleration.h"
#include "base/CCDirector.h"
#import <UIKit/UIKit.h>

#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <AVFoundation/AVFoundation.h>
#import<AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
// Accelerometer
#import<CoreMotion/CoreMotion.h>
#import <sys/utsname.h>
#import<CoreFoundation/CoreFoundation.h>
#import"platform/ios/CCAudioEncoder.h"
#import"platform/ios/CCCameraPreview.h"
#import"platform/ios/CCAudioIODevice.h"
#import"platform/ios/CCAudioIODeviceEx.h"
#import"platform/ios/CCAddressBook.h"
#import"platform/ios/CCVibrator.h"
#import<DigitsKit/Digits.h>
#import<DigitsKit/DGTSession.h>


@interface CCAccelerometerDispatcher : NSObject<UIAccelerometerDelegate>
{
    cocos2d::Acceleration *_acceleration;
    CMMotionManager *_motionManager;
}

+ (id) sharedAccelerometerDispather;
- (id) init;
- (void) setAccelerometerEnabled: (bool) isEnabled;
- (void) setAccelerometerInterval:(float) interval;

@end

@implementation CCAccelerometerDispatcher

static CCAccelerometerDispatcher* s_pAccelerometerDispatcher;

+ (id) sharedAccelerometerDispather
{
    if (s_pAccelerometerDispatcher == nil) {
        s_pAccelerometerDispatcher = [[self alloc] init];
    }
    
    return s_pAccelerometerDispatcher;
}

- (id) init
{
    if( (self = [super init]) ) {
        _acceleration = new cocos2d::Acceleration();
        _motionManager = [[CMMotionManager alloc] init];
    }
    return self;
}

- (void) dealloc
{
    s_pAccelerometerDispatcher = nullptr;
    delete _acceleration;
    [_motionManager release];
    [super dealloc];
}

- (void) setAccelerometerEnabled: (bool) isEnabled
{
    if (isEnabled)
    {
        [_motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
            [self accelerometer:accelerometerData];
        }];
    }
    else
    {
        [_motionManager stopAccelerometerUpdates];
    }
}

-(void) setAccelerometerInterval:(float)interval
{
    _motionManager.accelerometerUpdateInterval = interval;
}

- (void)accelerometer:(CMAccelerometerData *)accelerometerData
{
    _acceleration->x = accelerometerData.acceleration.x;
    _acceleration->y = accelerometerData.acceleration.y;
    _acceleration->z = accelerometerData.acceleration.z;
    _acceleration->timestamp = accelerometerData.timestamp;
    
    double tmp = _acceleration->x;
    
    switch ([[UIApplication sharedApplication] statusBarOrientation])
    {
        case UIInterfaceOrientationLandscapeRight:
            _acceleration->x = -_acceleration->y;
            _acceleration->y = tmp;
            break;
            
        case UIInterfaceOrientationLandscapeLeft:
            _acceleration->x = _acceleration->y;
            _acceleration->y = -tmp;
            break;
            
        case UIInterfaceOrientationPortraitUpsideDown:
            _acceleration->x = -_acceleration->y;
            _acceleration->y = -tmp;
            break;
            
        case UIInterfaceOrientationPortrait:
            break;
        default:
            NSAssert(false, @"unknow orientation");
    }

    cocos2d::EventAcceleration event(*_acceleration);
    auto dispatcher = cocos2d::Director::getInstance()->getEventDispatcher();
    dispatcher->dispatchEvent(&event);
}

@end

//

NS_CC_BEGIN

std::function<void(Device::Res, void*, unsigned char*, int)> Device::onContactListCallback = nullptr;
int Device::getDPI()
{
    static int dpi = -1;

    if (dpi == -1)
    {
        float scale = 1.0f;
        
        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
            scale = [[UIScreen mainScreen] scale];
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            dpi = 132 * scale;
        } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            dpi = 163 * scale;
        } else {
            dpi = 160 * scale;
        }
    }
    return dpi;
}




void Device::setAccelerometerEnabled(bool isEnabled)
{
    [[CCAccelerometerDispatcher sharedAccelerometerDispather] setAccelerometerEnabled:isEnabled];
}

void Device::setAccelerometerInterval(float interval)
{
    [[CCAccelerometerDispatcher sharedAccelerometerDispather] setAccelerometerInterval:interval];
}

typedef struct
{
    unsigned int height;
    unsigned int width;
    bool         isPremultipliedAlpha;
    bool         hasShadow;
    CGSize       shadowOffset;
    float        shadowBlur;
    float        shadowOpacity;
    bool         hasStroke;
    float        strokeColorR;
    float        strokeColorG;
    float        strokeColorB;
    float        strokeSize;
    float        tintColorR;
    float        tintColorG;
    float        tintColorB;
    
    unsigned char*  data;
    
} tImageInfo;

static bool s_isIOS7OrHigher = false;

static inline void lazyCheckIOS7()
{
    static bool isInited = false;
    if (!isInited)
    {
        s_isIOS7OrHigher = [[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending;
        isInited = true;
    }
}

static CGSize _calculateStringSize(NSString *str, id font, CGSize *constrainSize)
{
    CGSize textRect = CGSizeZero;
    textRect.width = constrainSize->width > 0 ? constrainSize->width
    : 0x7fffffff;
    textRect.height = constrainSize->height > 0 ? constrainSize->height
    : 0x7fffffff;
    
    CGSize dim = [str sizeWithFont:font constrainedToSize:textRect];

    dim.width = ceilf(dim.width);
    dim.height = ceilf(dim.height);
    
    return dim;
}

// refer Image::ETextAlign
#define ALIGN_TOP    1
#define ALIGN_CENTER 3
#define ALIGN_BOTTOM 2


static bool _initWithString(const char * text, cocos2d::Device::TextAlign align, const char * fontName, int size, tImageInfo* info)
{
    // lazy check whether it is iOS7 device
    lazyCheckIOS7();
    
    bool bRet = false;
    do
    {
        CC_BREAK_IF(! text || ! info);
        
        NSString * str          = [NSString stringWithUTF8String:text];
        NSString * fntName      = [NSString stringWithUTF8String:fontName];
        
        CGSize dim, constrainSize;
        
        constrainSize.width     = info->width;
        constrainSize.height    = info->height;
        
        // On iOS custom fonts must be listed beforehand in the App info.plist (in order to be usable) and referenced only the by the font family name itself when
        // calling [UIFont fontWithName]. Therefore even if the developer adds 'SomeFont.ttf' or 'fonts/SomeFont.ttf' to the App .plist, the font must
        // be referenced as 'SomeFont' when calling [UIFont fontWithName]. Hence we strip out the folder path components and the extension here in order to get just
        // the font family name itself. This stripping step is required especially for references to user fonts stored in CCB files; CCB files appear to store
        // the '.ttf' extensions when referring to custom fonts.
        fntName = [[fntName lastPathComponent] stringByDeletingPathExtension];
        
        // create the font
        id font = [UIFont fontWithName:fntName size:size];
        
        if (font)
        {
            dim = _calculateStringSize(str, font, &constrainSize);
        }
        else
        {
            if (!font)
            {
                font = [UIFont systemFontOfSize:size];
            }
            
            if (font)
            {
                dim = _calculateStringSize(str, font, &constrainSize);
            }
        }
        
        CC_BREAK_IF(! font);
        
        // compute start point
        int startH = 0;
        if (constrainSize.height > dim.height)
        {
            // vertical alignment
            unsigned int vAlignment = ((int)align >> 4) & 0x0F;
            if (vAlignment == ALIGN_TOP)
            {
                startH = 0;
            }
            else if (vAlignment == ALIGN_CENTER)
            {
                startH = (constrainSize.height - dim.height) / 2;
            }
            else
            {
                startH = constrainSize.height - dim.height;
            }
        }
        
        // adjust text rect
        if (constrainSize.width > 0 && constrainSize.width > dim.width)
        {
            dim.width = constrainSize.width;
        }
        if (constrainSize.height > 0 && constrainSize.height > dim.height)
        {
            dim.height = constrainSize.height;
        }
        
        
        // compute the padding needed by shadow and stroke
        float shadowStrokePaddingX = 0.0f;
        float shadowStrokePaddingY = 0.0f;
        
        if ( info->hasStroke )
        {
            shadowStrokePaddingX = ceilf(info->strokeSize);
            shadowStrokePaddingY = ceilf(info->strokeSize);
        }
        
        // add the padding (this could be 0 if no shadow and no stroke)
        dim.width  += shadowStrokePaddingX*2;
        dim.height += shadowStrokePaddingY*2;
        
        
        unsigned char* data = (unsigned char*)malloc(sizeof(unsigned char) * (int)(dim.width * dim.height * 4));
        memset(data, 0, (int)(dim.width * dim.height * 4));
        
        // draw text
        CGColorSpaceRef colorSpace  = CGColorSpaceCreateDeviceRGB();
        CGContextRef context        = CGBitmapContextCreate(data,
                                                            dim.width,
                                                            dim.height,
                                                            8,
                                                            (int)(dim.width) * 4,
                                                            colorSpace,
                                                            kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
        if (!context)
        {
            CGColorSpaceRelease(colorSpace);
            CC_SAFE_FREE(data);
            break;
        }
        
        // text color
        CGContextSetRGBFillColor(context, info->tintColorR, info->tintColorG, info->tintColorB, 1);
        // move Y rendering to the top of the image
        CGContextTranslateCTM(context, 0.0f, (dim.height - shadowStrokePaddingY) );
        CGContextScaleCTM(context, 1.0f, -1.0f); //NOTE: NSString draws in UIKit referential i.e. renders upside-down compared to CGBitmapContext referential
        
        // store the current context
        UIGraphicsPushContext(context);
        
        // measure text size with specified font and determine the rectangle to draw text in
        unsigned uHoriFlag = (int)align & 0x0f;
        NSTextAlignment nsAlign = (2 == uHoriFlag) ? NSTextAlignmentRight
                                                  : (3 == uHoriFlag) ? NSTextAlignmentCenter
                                                  : NSTextAlignmentLeft;
         
        
        CGColorSpaceRelease(colorSpace);
        
        // compute the rect used for rendering the text
        // based on wether shadows or stroke are enabled
        
        float textOriginX  = 0;
        float textOrigingY = startH;
        
        float textWidth    = dim.width;
        float textHeight   = dim.height;
        
        CGRect rect = CGRectMake(textOriginX, textOrigingY, textWidth, textHeight);
        
        CGContextSetShouldSubpixelQuantizeFonts(context, false);
        
        CGContextBeginTransparencyLayerWithRect(context, rect, NULL);
        
        if ( info->hasStroke )
        {
            CGContextSetTextDrawingMode(context, kCGTextStroke);
            
            if(s_isIOS7OrHigher)
            {
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                paragraphStyle.alignment = nsAlign;
                paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
                [str drawInRect:rect withAttributes:@{
                                                      NSFontAttributeName: font,
                                                      NSStrokeWidthAttributeName: [NSNumber numberWithFloat: info->strokeSize / size * 100 ],
                                                      NSForegroundColorAttributeName:[UIColor colorWithRed:info->tintColorR
                                                                                                     green:info->tintColorG
                                                                                                      blue:info->tintColorB
                                                                                                     alpha:1.0f],
                                                      NSParagraphStyleAttributeName:paragraphStyle,
                                                      NSStrokeColorAttributeName: [UIColor colorWithRed:info->strokeColorR
                                                                                                  green:info->strokeColorG
                                                                                                   blue:info->strokeColorB
                                                                                                  alpha:1.0f]
                                                      }
                 ];
                
                [paragraphStyle release];
            }
            else
            {
                CGContextSetRGBStrokeColor(context, info->strokeColorR, info->strokeColorG, info->strokeColorB, 1);
                CGContextSetLineWidth(context, info->strokeSize);
                
                //original code that was not working in iOS 7
                [str drawInRect: rect withFont:font lineBreakMode:NSLineBreakByWordWrapping alignment:nsAlign];
            }
        }
        
        CGContextSetTextDrawingMode(context, kCGTextFill);
        
        // actually draw the text in the context
        [str drawInRect: rect withFont:font lineBreakMode:NSLineBreakByWordWrapping alignment:nsAlign];
        
        CGContextEndTransparencyLayer(context);
        
        // pop the context
        UIGraphicsPopContext();
        
        // release the context
        CGContextRelease(context);
        
        // output params
        info->data                 = data;
        info->isPremultipliedAlpha = true;
        info->width                = dim.width;
        info->height               = dim.height;
        bRet                        = true;
        
    } while (0);
    
    return bRet;
}


Data Device::getTextureDataForText(const char * text, const FontDefinition& textDefinition, TextAlign align, int &width, int &height, bool& hasPremultipliedAlpha)
{
    Data ret;
    
    do {
        tImageInfo info = {0};
        info.width                  = textDefinition._dimensions.width;
        info.height                 = textDefinition._dimensions.height;
        info.hasShadow              = textDefinition._shadow._shadowEnabled;
        info.shadowOffset.width     = textDefinition._shadow._shadowOffset.width;
        info.shadowOffset.height    = textDefinition._shadow._shadowOffset.height;
        info.shadowBlur             = textDefinition._shadow._shadowBlur;
        info.shadowOpacity          = textDefinition._shadow._shadowOpacity;
        info.hasStroke              = textDefinition._stroke._strokeEnabled;
        info.strokeColorR           = textDefinition._stroke._strokeColor.r / 255.0f;
        info.strokeColorG           = textDefinition._stroke._strokeColor.g / 255.0f;
        info.strokeColorB           = textDefinition._stroke._strokeColor.b / 255.0f;
        info.strokeSize             = textDefinition._stroke._strokeSize;
        info.tintColorR             = textDefinition._fontFillColor.r / 255.0f;
        info.tintColorG             = textDefinition._fontFillColor.g / 255.0f;
        info.tintColorB             = textDefinition._fontFillColor.b / 255.0f;
        
        if (! _initWithString(text, align, textDefinition._fontName.c_str(), textDefinition._fontSize, &info))
        {
            break;
        }
        height = (short)info.height;
        width = (short)info.width;
		ret.fastSet(info.data,width * height * 4);
		hasPremultipliedAlpha = true;
    } while (0);
    
    return ret;
}

void Device::setKeepScreenOn(bool value)
{
    [[UIApplication sharedApplication] setIdleTimerDisabled:(BOOL)value];
}

static CCCameraPreview *s_pCameraPreview = nil;

void Device::startCameraPreview(VideoInputDeletegate *delegate, int previewSize)
{
    if(s_pCameraPreview == nil)
    {
        s_pCameraPreview = [[CCCameraPreview alloc] initWithSize:previewSize];
        [s_pCameraPreview setVideoInputDeletegate:delegate];
    }
    [s_pCameraPreview startCameraCapture];
}

void Device::releaseCameraPreview()
{
    [s_pCameraPreview dealloc];
}

void Device::flipCameraPreview()
{
    [s_pCameraPreview rotateCamera];
}

/* value (1~100) */
void Device::setFrameRateCamera(int value)
{
    [s_pCameraPreview setFrameRate:value];
}

/* value (1~100) */
void Device::setExposureCamera(int value)
{
    [s_pCameraPreview setExposure:(float)value];
}

int Device::getExposureCamera()
{
    if(s_pCameraPreview==nullptr)
        return 50;
    return [s_pCameraPreview getExposure];
}

bool Device::turnCameraFlashOnOff()
{
    return [s_pCameraPreview turnFlashOnOff];
}

void Device::enableFaceDetect(bool enable)
{
    [s_pCameraPreview enableFaceDetect:(enable)? TRUE: FALSE];
}

static CCAudioIODevice *s_pAudioIODevice = nil;
static CCAudioIODeviceEx *s_pAudioIODeviceEx = nil;
static CCAudioEncoder *s_pAudioEncoder = nil;
static AVAudioPlayer *s_PreviewSoundPlayer = nil;
static AVAudioPlayer *s_DecodeSoundPlayer = nil;

bool Device::createAudioIO(int samplerate, int channels, double duration)
{
    if(s_pAudioIODevice == nil)
    {
        s_pAudioIODevice = [[CCAudioIODevice alloc] initWithSamplerate:samplerate channel:channels duration:duration];
        return true;
    }
    
    return false;
}

void  Device::startVoiceRecorder(AudioInputDeletegate *delegate)
{
    if(s_pAudioIODevice)
    {
        [s_pAudioIODevice setAudioInputDeletegate:delegate];
        [s_pAudioIODevice startVoiceCapture];
    }
}

void  Device::startVoiceRecorderEx(AudioInputDeletegate *delegate)
{
    if(s_pAudioIODeviceEx==nil)
        s_pAudioIODeviceEx = [[CCAudioIODeviceEx alloc] initWithSamplerate:16000 channel:1 duration:0.06];
    [s_pAudioIODeviceEx setAudioInputDeletegate:delegate];
}

void Device::pauseVoiceRecorder()
{
    if(s_pAudioIODevice)
        [s_pAudioIODevice stopVoiceCapture];
}

void Device::resumeVoiceRecorder()
{
    [s_pAudioIODevice startVoiceCapture];
}

void Device::stopVoiceRecorder()
{
    [s_pAudioIODevice stopVoiceCapture];
}

void Device::releaseVoiceRecorder()
{
    [s_pAudioIODevice release];
}
static AVAssetWriter* s_assetWriter = nil;
void Device::startVideoRecord(const char* filename)
{
    if(s_assetWriter !=nil)
    {
        [s_assetWriter release];
        s_assetWriter = nil;
    }
    NSString *newName = [NSString stringWithUTF8String:filename];
    NSURL *fullPath = [NSURL fileURLWithPath:newName];
    NSError *error = nil;
    s_assetWriter = [[AVAssetWriter alloc] initWithURL:fullPath fileType:(NSString *)kUTTypeQuickTimeMovie error:&error];
    
    if (error != nil)
    {
        CCLOG("fail to startVideoRecord");
        return;
    }
    
    [s_pAudioIODeviceEx startVoiceCapture:s_assetWriter];
    [s_pCameraPreview startRecording:s_assetWriter filename:newName];
    [s_assetWriter startWriting];
}

bool Device::startAudioTrack(int samplerate, int channels)
{
    return (s_pAudioIODevice != nil);
}

short* Device::getAudioTrackBuffer()
{
    return [s_pAudioIODevice getOutputAudioBuffer];
}

void Device::writeAudioTrack(short* buffer, int size)
{
    [s_pAudioIODevice queueDecodeAudioBuffer:buffer];
}

void Device::lockAudioTrack()
{
    [s_pAudioIODevice lockOutputAudioBuffer];
}

void Device::unlockAudioTrack()
{
    [s_pAudioIODevice unlockOutputAudioBuffer];
}

void Device::stopAudioTrack()
{
    
}

int Device::getRingerMode()
{
    return 2;
}

static CCVibrator *s_pVibrator = nil;

void Device::vibrate(bool onoff)
{
    if(s_pVibrator == nil)
    {
        s_pVibrator = [[CCVibrator alloc] init];
    }
    if(onoff)
    {
        [s_pVibrator start];
    }
    else
    {
        [s_pVibrator cancel];
    }
}

std::string Device::getExternalStorageDirectory()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSLog(@"%@", documentsDirectory);
    return [documentsDirectory UTF8String];
}

std::string Device::getPhotoStorageDirectory()
{
    return FileUtils::getInstance()->getWritablePath();
}

bool Device::savePhoto(const char* filepath)
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    NSData *data = [NSData dataWithContentsOfFile:[NSString stringWithUTF8String:filepath]]; // Your GIF file path which you might have saved in NSDocumentDir or NSTempDir
    
    [library writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
        if (error) {
            NSLog(@"Error Saving Photo to Photo Album: %@", error);
        } else {
            // TODO: success handling
            NSLog(@"Photo Saved to %@", assetURL);
        }
    }];
    
    return true;
}

bool Device::saveGif(const char* filepath)
{
    NSString *newName = [NSString stringWithUTF8String:filepath];
    NSURL *fullPath = [NSURL fileURLWithPath:newName];
    NSError *error = nil;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        
        NSData *data = [NSData dataWithContentsOfURL:fullPath];
        
        [library writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:^(NSURL *assetURL, NSError *error)
         {
             if (error) {
                 NSLog(@"Error Saving Photo to Photo Album: %@", error);
             } else {
                 // TODO: success handling
                 NSLog(@"Photo Saved to %@", assetURL);
             }
         }];
     });
}

void Device::saveMovie()
{
    if(s_assetWriter==nil)
    {
        CCLOG("fail to saveMovie because s_assetWriter is nil");
        return ;
    }
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:s_assetWriter.outputURL])
    {
        [library writeVideoAtPathToSavedPhotosAlbum:s_assetWriter.outputURL completionBlock:^(NSURL *assetURL, NSError *error)
         {
             if (error) {
                 NSLog(@"error: %@", [error localizedDescription]);
             } else {
                 NSLog(@"saved");
             }
         }
         ];
    }
    [library release];
}

void Device::scanMediaNewFile(const char *filename)
{
    CCLOG("not supported yet..");
}

bool Device::sharePhoto(const char* filepath)
{
    NSString *newName = [NSString stringWithUTF8String:filepath];
    NSURL *fullPath = [NSURL fileURLWithPath:newName];
    NSData *imageData = [NSData dataWithContentsOfURL:fullPath];
    NSArray *objectsToShare = @[imageData];
    
    UIApplication* clientApp = [UIApplication sharedApplication];
    UIWindow* topWindow = [clientApp keyWindow];
    if (!topWindow)
    {
        topWindow = [[clientApp windows] objectAtIndex:0];
    }
    UIViewController *rootController=(UIViewController *)topWindow.rootViewController;
    UIActivityViewController *activityView = [[UIActivityViewController alloc]
                                              initWithActivityItems:objectsToShare
                                              applicationActivities:nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [rootController presentViewController:activityView
                                     animated:YES
                                   completion:nil];
    }
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityView];
        [popup presentPopoverFromRect:CGRectMake(rootController.view.frame.size.width/2,
                                                 rootController.view.frame.size.height/4, 0, 0)inView:rootController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    return true;
}

bool Device::shareMovie(const char* filepath)
{
    NSString *message = @"Bitlive capture video";
    NSString *videoToShare = [NSString stringWithUTF8String: filepath];
    NSURL *videoPath = [NSURL fileURLWithPath:videoToShare];
    NSArray *objectsToShare = [NSArray arrayWithObjects:message, videoPath, nil];
    
    UIApplication* clientApp = [UIApplication sharedApplication];
    UIWindow* topWindow = [clientApp keyWindow];
    if (!topWindow)
    {
        topWindow = [[clientApp windows] objectAtIndex:0];
    }
    UIViewController *rootController=(UIViewController *)topWindow.rootViewController;
    UIActivityViewController *activityView = [[UIActivityViewController alloc]
                                              initWithActivityItems:objectsToShare
                                              applicationActivities:nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [rootController presentViewController:activityView
                                     animated:YES
                                   completion:nil];
    }
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityView];
        [popup presentPopoverFromRect:CGRectMake(rootController.view.frame.size.width/2,
                                                 rootController.view.frame.size.height/4, 0, 0)inView:rootController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    return true;
}

void Device::requestContactList()
{
    UIApplication* clientApp = [UIApplication sharedApplication];
    UIWindow* topWindow = [clientApp keyWindow];
    if (!topWindow)
    {
        topWindow = [[clientApp windows] objectAtIndex:0];
    }
    UIViewController *rootController=(UIViewController *)topWindow.rootViewController;
    CCAddressBook* addressBook = [[CCAddressBook alloc] initWithController:rootController];
    [addressBook launch];
}

void Device::requestContactAccess()
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
            } else {
                // User denied access
                // Display an alert telling user the contact could not be added
            }
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
    }
    else {
        // The user has previously denied access
        // Send an alert telling user to change privacy setting in settings app
    }
}
void Device::requestMicAccess()
{
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            // Microphone enabled code
        }
        else {
            // Microphone disabled code
        }
    }];
}

void Device::searchContact(const char* key)
{
    NSString *result;
    std::vector<std::string>* list = new std::vector<std::string>();
    int len = 0;
    unsigned char* pictureBuffer = nullptr;
    
    ABAddressBookRef addressBook = ABAddressBookCreate();
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (granted) {
            } else {
                // User denied access
                // Display an alert telling user the contact could not be added
            }
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
    }
    else {
        // The user has previously denied access
        // Send an alert telling user to change privacy setting in settings app
    }
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    NSString *target;
    
    NSString *searchKey = [NSString stringWithUTF8String:key];
    if([searchKey hasSuffix:@"@gmail.com"]) // email
    {
        for (int i=0;i < nPeople;i++) {
            ABRecordRef ref = CFArrayGetValueAtIndex(allPeople,i);
            
            //For Email ids
            ABMutableMultiValueRef eMail  = ABRecordCopyValue(ref, kABPersonEmailProperty);
            if(ABMultiValueGetCount(eMail) > 0) {
                target = (__bridge NSString *)ABMultiValueCopyValueAtIndex(eMail, 0);
                if ([target isEqualToString:searchKey]) {
                    NSString *firstName = (NSString*)ABRecordCopyValue(ref, kABPersonFirstNameProperty);
                    NSString *lastName  = (NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty);
                    if(firstName==nil && lastName==nil)
                        result = target;
                    else if(firstName==nil)
                        result = lastName;
                    else if(lastName==nil)
                        result = firstName;
                    else
                        result = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
                    
                    list->push_back([result UTF8String]);
                    UIImage *photo;
                    if (ABPersonHasImageData(ref)) {
                        if ( &ABPersonCopyImageDataWithFormat != nil ) {
                            // iOS >= 4.1
                            photo = [UIImage imageWithData:(NSData *)ABPersonCopyImageDataWithFormat(ref, kABPersonImageFormatThumbnail)];
                        } else {
                            // iOS < 4.1
                            photo = [UIImage imageWithData:(NSData *)ABPersonCopyImageData(ref)];
                        }
                        NSData *imgData = UIImagePNGRepresentation(photo);
                        len = (int)[imgData length];
                        pictureBuffer = (unsigned char*)malloc(len);
                        memcpy(pictureBuffer, [imgData bytes], len);
                    }
                    cocos2d::Device::onContactListCallback(cocos2d::Device::Res::SearchContact, list, pictureBuffer, len);
                    return;
                 }
            }
        }
    }
    else
    {
        NSCharacterSet *toExclude = [NSCharacterSet characterSetWithCharactersInString:@"/.()- "];
        
        for (int i=0;i < nPeople;i++) {
            ABRecordRef ref = CFArrayGetValueAtIndex(allPeople,i);
            
            //For username and surname
            ABMultiValueRef phones =(__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonPhoneProperty));
            //For Phone number
            NSString* mobileLabel;
            
            for(CFIndex j = 0; j < ABMultiValueGetCount(phones); j++) {
                mobileLabel = (__bridge NSString*)ABMultiValueCopyLabelAtIndex(phones, j);

                if([mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel])
                {
                    target = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, j);
                    target = [[target componentsSeparatedByCharactersInSet:toExclude] componentsJoinedByString: @""];
                    if([target hasPrefix:@"0"])
                        target = [target substringFromIndex:1];
                    if ([searchKey hasSuffix:target]) {
                        NSString *firstName = (NSString*)ABRecordCopyValue(ref, kABPersonFirstNameProperty);
                        NSString *lastName  = (NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty);
                        if(firstName==nil && lastName==nil)
                            result = target;
                        else if(firstName==nil)
                            result = lastName;
                        else if(lastName==nil)
                            result = firstName;
                        else
                            result = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
                        
                        list->push_back([result UTF8String]);
                        // Check for contact picture
                        UIImage *photo;
                        if (ABPersonHasImageData(ref)) {
                            if ( &ABPersonCopyImageDataWithFormat != nil ) {
                                // iOS >= 4.1
                                photo = [UIImage imageWithData:(NSData *)ABPersonCopyImageDataWithFormat(ref, kABPersonImageFormatThumbnail)];
                            } else {
                                // iOS < 4.1
                                photo = [UIImage imageWithData:(NSData *)ABPersonCopyImageData(ref)];
                            }
                            NSData *imgData = UIImagePNGRepresentation(photo);
                            len = (int)[imgData length];
                            pictureBuffer = (unsigned char*)malloc(len);
                            memcpy(pictureBuffer, [imgData bytes], len);
                        }
                        cocos2d::Device::onContactListCallback(cocos2d::Device::Res::SearchContact, list, pictureBuffer, len);
                        return;
                    }
                }
                else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel])
                {
                    target = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, j);
                    target = [[target componentsSeparatedByCharactersInSet:toExclude] componentsJoinedByString: @""];
                    if([target hasPrefix:@"0"])
                        target = [target substringFromIndex:1];
                    if ([searchKey hasSuffix:target]) {
                        NSString *firstName = (NSString*)ABRecordCopyValue(ref, kABPersonFirstNameProperty);
                        NSString *lastName  = (NSString*)ABRecordCopyValue(ref, kABPersonLastNameProperty);
                        if(firstName==nil && lastName==nil)
                            result = target;
                        else if(firstName==nil)
                            result = lastName;
                        else if(lastName==nil)
                            result = firstName;
                        else
                            result = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
                        
                        list->push_back([result UTF8String]);
                        UIImage *photo;
                        if (ABPersonHasImageData(ref)) {
                            if ( &ABPersonCopyImageDataWithFormat != nil ) {
                                // iOS >= 4.1
                                photo = [UIImage imageWithData:(NSData *)ABPersonCopyImageDataWithFormat(ref, kABPersonImageFormatThumbnail)];
                            } else {
                                // iOS < 4.1
                                photo = [UIImage imageWithData:(NSData *)ABPersonCopyImageData(ref)];
                            }
                            NSData *imgData = UIImagePNGRepresentation(photo);
                            len = (int)[imgData length];
                            pictureBuffer = (unsigned char*)malloc(len);
                            memcpy(pictureBuffer, [imgData bytes], len);
                        }
                        cocos2d::Device::onContactListCallback(cocos2d::Device::Res::SearchContact, list, pictureBuffer, len);
                        return;
                    }
                }
            }
        }
    }
    
    list->push_back(key);
    cocos2d::Device::onContactListCallback(cocos2d::Device::Res::SearchContact, list, pictureBuffer, 0);
}

void Device::closeWebBrowser()
{
    
}

#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"

NSDictionary* getIPAddresses()
{
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // retrieve the current interfaces - returns 0 on success
    struct ifaddrs *interfaces;
    NSString *addrString;
    NSString *private_prefix1=@"fe80::";
    NSString *private_prefix2=@"::1";
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET6) {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                       type = IP_ADDR_IPv6;
                       addrString = [NSString stringWithUTF8String:addrBuf];
                        if(!([addrString hasPrefix:private_prefix1] || [addrString hasPrefix:private_prefix2])) {
                           addresses[name] = addrString;
                           continue;
                       }
                    }
                }
                else if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv4;
                        addresses[name] = [NSString stringWithUTF8String:addrBuf];
                    }
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}

bool Device::getMyIPAddress(std::string& ipaddress, std::string& network_type)
{
    
    NSDictionary *addresses = getIPAddresses();
    NSLog(@"addresses: %@", addresses);
    if(addresses[IOS_WIFI]!=nil)
    {
        ipaddress = [addresses[IOS_WIFI] UTF8String];
        network_type = "wifi";
    }
    else if(addresses[IOS_CELLULAR]!=nil)
    {
        ipaddress = [addresses[IOS_CELLULAR] UTF8String];
        network_type = "mobile";
    }
    else
    {
        ipaddress = "0.0.0.0";
        network_type = "none";
        return false;
    }
    return true;
}

std::string Device::getNetworkProvider()
{
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    if(carrier == nil)
        return "";
    
    NSLog(@"Carrier Name : %@", [carrier carrierName]);
    [networkInfo release];
    
    return [[carrier carrierName] UTF8String];
}

std::string Device::getPhoneNumber()
{
    return "";
}

void Device::setRegisterID(const char* deviceToken)
{
    UserDefault::getInstance()->setStringForKey("registerID", deviceToken);
}

std::string Device::getRegisterID()
{
    return UserDefault::getInstance()->getStringForKey("registerID");
}

void Device::setOfflineMessage(const char* message)
{
    UserDefault::getInstance()->setStringForKey("offlinemessage", message);
}

std::string Device::getOfflineMessage()
{
    std::string newMessage = UserDefault::getInstance()->getStringForKey("offlinemessage");
    if(newMessage.length()>0)
        UserDefault::getInstance()->setStringForKey("offlinemessage", "");
    return newMessage;
}

void Device::setHiddenMessage(const char* message)
{
    int hiddenMessageNum = UserDefault::getInstance()->getIntegerForKey("hiddenmessagenum") + 1;
    std::ostringstream s;
    s << "hiddenmessage" << hiddenMessageNum;
    std::string query(s.str());
    UserDefault::getInstance()->setIntegerForKey("hiddenmessagenum", hiddenMessageNum);
    UserDefault::getInstance()->setStringForKey(query.c_str(), message);
}

std::list<std::string>* Device::getHiddenMessageList()
{
    int hiddenMessageNum = UserDefault::getInstance()->getIntegerForKey("hiddenmessagenum");
    if(hiddenMessageNum==0)
        return nullptr;
    
    std::list<std::string>* result = new std::list<std::string>();
    std::ostringstream s;
    std::string newMessage;
    for(int i=0 ; i<hiddenMessageNum ; i++)
    {
        s << "hiddenmessage" << (i + 1);
        std::string query(s.str());
        newMessage = UserDefault::getInstance()->getStringForKey(query.c_str());
        result->push_back(newMessage);
    }
    UserDefault::getInstance()->setIntegerForKey("hiddenmessagenum", 0);
    return result;
}

void Device::setCountryCode(const char* code)
{
    UserDefault::getInstance()->setStringForKey("countrycode", code);
}

std::string Device::getCountryCode()
{
    return UserDefault::getInstance()->getStringForKey("countrycode");
}

std::string Device::getCountryCodeLocale()
{
    NSLocale *locale = [NSLocale currentLocale];
    NSString *key = [locale objectForKey: NSLocaleCountryCode];
    std::string fullPath = FileUtils::getInstance()->fullPathForFilename("DiallingCodes.plist");
    NSString* path = [NSString stringWithUTF8String:fullPath.c_str()];
    NSDictionary* dict = [NSDictionary dictionaryWithContentsOfFile:path];
    if(dict==nil)
    {
        CCLOG("Fail to create NSDictionary from %s", fullPath.c_str());
        return nullptr;
    }
    NSString *str = [dict objectForKey:[key lowercaseString]];
    
    if(str==nil)
    {
        CCLOG("Device::getCountryCode() is null");
        return "";
    }
    return [str UTF8String];
}

void Device::getPhoneNumberWithFablic(std::string event_name)
{
    /*
    ./Fabric.framework/run 64bbdb70be67f9ada419ec6c309d30ff5625cec4 21fe72832264227c01032e77b2d0e7b50bc0f552834a2063bde8cbfe4124d509
    */
    @try {
        [[Digits sharedInstance] authenticateWithCompletion:^
         (DGTSession* session, NSError *error) {
             if (session) {
                 EventCustom event(event_name);
                 event.setUserData((void*)[session.phoneNumber UTF8String]);
                 Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
             }
         }];
    }
    @catch(NSException *e)
    {
        EventCustom event(event_name);
        event.setUserData((void*)nullptr);
        Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
    }
}

int Device::getDeviceLevel()
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    if([code hasPrefix:@"iPhone3"] || [code hasPrefix:@"iPhone4"])
        return 0;
    
    return 1;
}

void Device::playSound(std::string filepath)
{
    if(s_DecodeSoundPlayer!=nil)
        [s_DecodeSoundPlayer release];
    std::string fullPath = FileUtils::getInstance()->fullPathForFilename(filepath);
    NSString *soundFilePath = [NSString stringWithUTF8String:fullPath.c_str()];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
    s_DecodeSoundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    s_DecodeSoundPlayer.numberOfLoops = 0; //infinite loop
    [s_DecodeSoundPlayer play];
    [s_DecodeSoundPlayer setVolume:0.5];
}

void Device::playSoundWithMixing(std::string filepath)
{
    std::string fullPath = FileUtils::getInstance()->fullPathForFilename(filepath);
    if(s_pAudioIODeviceEx!=nil && s_pAudioIODeviceEx.isRecording==TRUE)
    {
        [s_pAudioIODeviceEx setMixSoundPath:fullPath.c_str()];
    }
    else
    {
        if(s_PreviewSoundPlayer!=nil)
            [s_PreviewSoundPlayer release];
        NSString *soundFilePath = [NSString stringWithUTF8String:fullPath.c_str()];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
        s_PreviewSoundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
        s_PreviewSoundPlayer.numberOfLoops = 0; //infinite loop
        [s_PreviewSoundPlayer play];
        [s_PreviewSoundPlayer setVolume:0.5];
    }
}
NS_CC_END

#endif // CC_PLATFORM_IOS

