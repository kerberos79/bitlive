//
//  CCAddressBook.h
//  Vista
//
//  Created by kerberos on 1/20/15.
//
//


#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface CCAddressBook : NSObject <ABPeoplePickerNavigationControllerDelegate> {
    UIViewController* _controller;
};

@ property (nonatomic, assign) UIViewController* _controller;
- (id) initWithController:(UIViewController*) controller;
- (id) dealloc;
- (void)launch;
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController*)peoplePicker;
/*
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier NS_AVAILABLE_IOS(8_0);
*/ 
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person NS_AVAILABLE_IOS(8_0);
@end
