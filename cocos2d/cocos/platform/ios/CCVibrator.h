//
//  CCVibrator.h
//  cocos2d_libs
//
//  Created by keros on 5/30/15.
//
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface CCVibrator : NSObject
{
    NSThread *thread;
}
- (void)start;
- (void)play;
- (void)cancel;
@end
