//
//  CCCameraPreview.h
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//

#ifndef Anicamera_CCAudioDecodeBufferQueue_h
#define Anicamera_CCAudioDecodeBufferQueue_h

#include"CCDevice.h"
#include<functional>
#include<vector>
#include<list>
#import <AVFoundation/AVFoundation.h>

#define QUEUE_SIZE  6
@interface CCAudioDecodeBufferQueue : NSObject
{
    pthread_cond_t condition;
    pthread_mutex_t lock1;
    pthread_mutex_t lock2;
    std::list<unsigned char *>       decodeBufferQueue;
    std::list<unsigned char *>     readyBufferQueue;
}

- (CCAudioDecodeBufferQueue*)initWithBytesPerFrame:(int) bytes;
- (void)dealloc;
- (unsigned char *)getReadyBuffer;
- (void)pushReadyBuffer:(unsigned char *)buffer;
- (unsigned char *)getDecodeBuffer;
- (int)getDecodeBufferSize;
- (void)pushDecodeBuffer:(unsigned char *)buffer;
- (void)initBufferList;
@end
#endif
