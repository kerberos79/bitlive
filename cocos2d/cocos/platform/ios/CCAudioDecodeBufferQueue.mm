//
//  CCCameraPreview.mm
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//
#include <vector>
#include <list>
#include <pthread.h>
#import "CCAudioDecodeBufferQueue.h"

@implementation CCAudioDecodeBufferQueue

- (CCAudioDecodeBufferQueue*)initWithBytesPerFrame:(int) bytes
{
    [super init];
    pthread_mutex_init(&lock1, NULL);
    pthread_mutex_init(&lock2, NULL);
    pthread_cond_init(&condition, NULL);
    
    for(int i=0;i<QUEUE_SIZE;i++)
    {
        unsigned char *outputData = (unsigned char*)malloc(bytes);
        memset(outputData, 0, bytes);
        readyBufferQueue.push_back(outputData);
    }
    return self;
}

- (void)dealloc
{
    pthread_cond_signal(&condition);
    pthread_cond_destroy(&condition);
    pthread_mutex_destroy (&lock1);
    pthread_mutex_destroy (&lock2);
    
    while(!readyBufferQueue.empty())
    {
        unsigned char *outputData = readyBufferQueue.back();
        readyBufferQueue.pop_back();
        delete outputData;
    }
    while(!decodeBufferQueue.empty())
    {
        unsigned char *outputData = decodeBufferQueue.front();
        decodeBufferQueue.pop_front();
        delete outputData;
    }
    [self release];
    [super dealloc];
}

- (void)initBufferList
{
    while(!decodeBufferQueue.empty())
    {
        readyBufferQueue.push_back(decodeBufferQueue.back());
        decodeBufferQueue.pop_back();
    }
}

-(unsigned char *)getReadyBuffer
{
    unsigned char *outputData = NULL;
    pthread_mutex_lock(&lock1);
    if(!readyBufferQueue.empty())
    {
        outputData = readyBufferQueue.front();
        readyBufferQueue.pop_front();
    }
    pthread_mutex_unlock(&lock1);
    return outputData;
}

-(void)pushReadyBuffer:(unsigned char *)buffer
{
    pthread_mutex_lock(&lock1);
    readyBufferQueue.push_back(buffer);
    pthread_mutex_unlock(&lock1);
}

- (unsigned char *)getDecodeBuffer
{
    unsigned char *outputData = NULL;
    pthread_mutex_lock(&lock2);

    outputData = decodeBufferQueue.front();
    decodeBufferQueue.pop_front();
    
    pthread_mutex_unlock(&lock2);
    return outputData;
}

- (int)getDecodeBufferSize
{
    int size;
    pthread_mutex_lock(&lock2);
    size = decodeBufferQueue.size();
    pthread_mutex_unlock(&lock2);
    return size;
}

-(void)pushDecodeBuffer:(unsigned char *)buffer
{
    pthread_mutex_lock(&lock2);
    decodeBufferQueue.push_back(buffer);
    pthread_mutex_unlock(&lock2);
}
@end
