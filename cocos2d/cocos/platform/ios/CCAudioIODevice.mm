//
//  CCCameraPreview.mm
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//

#import "CCAudioIODevice.h"

#include <sys/time.h>
static OSStatus recordingCallback(void *inRefCon,
                                  AudioUnitRenderActionFlags *ioActionFlags,
                                  const AudioTimeStamp *inTimeStamp,
                                  UInt32 inBusNumber,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *ioData) {
    
    OSStatus status;
    CCAudioIODevice *inputDevice = (__bridge CCAudioIODevice*) inRefCon;
    AudioUnit ioUnit = inputDevice.ioUnit;
    ioData = inputDevice.inputData;
    if((status = AudioUnitRender(ioUnit, ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, ioData))>0)
    {
        NSLog(@"error : AudioUnitRender %d", (int)status);
    }
    else
    {
        [inputDevice processInputBuffer:ioData];
    }
    return noErr;
}

static OSStatus playbackCallback(void *inRefCon,
                                 AudioUnitRenderActionFlags *ioActionFlags,
                                 const AudioTimeStamp *inTimeStamp,
                                 UInt32 inBusNumber,
                                 UInt32 inNumberFrames,
                                 AudioBufferList *ioData) {
    CCAudioIODevice *outputDevice = (CCAudioIODevice*) inRefCon;
    [outputDevice writeOutputAudioBuffer:ioData];
    return noErr;
}

#pragma mark -
#pragma mark Private methods and instance variables


@implementation CCAudioIODevice

@synthesize currentOutputBuffer, gain;

- (void)setAudioInputDeletegate:(AudioInputDeletegate*) delegate
{
    _audioInputDeletegate = delegate;
}

- (CCAudioIODevice*)initWithSamplerate:(int)samplerate channel:(int)channels duration:(double)duration;
{
    [super init];
    
    currentBufferIndex = 0;
    currentOutputBuffer = 0;
    [self setupAudioSession];
    [self setupAudioIOGraph];
    
    decodeBufferQueue = [[CCAudioDecodeBufferQueue alloc] initWithBytesPerFrame:OPUS_FRAME_SIZE];
    return self;
}

- (void)dealloc
{
    if(self.inputData!=nil)
    {
        delete self.inputData;
    }
    if(playCallbackBuffer!=nil)
        delete playCallbackBuffer;
    if(startEncPos)
        delete startEncPos;
    [self release];
    [super dealloc];
}

- (void)setupAudioSession
{
    NSError *error;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setPreferredSampleRate:16000 error:&error];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [audioSession setPreferredIOBufferDuration:0.064  error:&error];
    [audioSession setActive:YES error:&error];
    double sampleRate = audioSession.sampleRate;
    double ioBufferDuration = audioSession.IOBufferDuration;
    int samplesPerFrame = (int)(ioBufferDuration * sampleRate);
    int bytesPerSample = sizeof(AudioSampleType);
    int bytesPerFrame = samplesPerFrame * bytesPerSample;
    
    self.sampleRate = sampleRate;
    self.frameDuration = ioBufferDuration;
    self.samplesPerFrame = samplesPerFrame;
    self.bytesPerSample = bytesPerSample;
    self.bytesPerFrame = bytesPerFrame;
}

- (void)setupAudioIOGraph
{
    OSStatus status = noErr;
    
    AUGraph audioGraph;
    status = NewAUGraph(&audioGraph);
    if(status != noErr) { NSLog(@"Failed to create audio graph"); return; }
    self.audioGraph = audioGraph;
    
    AudioComponentDescription ioUnitDesc;
    ioUnitDesc.componentType = kAudioUnitType_Output;
    ioUnitDesc.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
    ioUnitDesc.componentManufacturer = kAudioUnitManufacturer_Apple;
    ioUnitDesc.componentFlags = 0;
    ioUnitDesc.componentFlagsMask = 0;
    self.ioUnitDesc = ioUnitDesc;
    
    AUNode ioNode;
    status = AUGraphAddNode(audioGraph, &ioUnitDesc, &ioNode);
    if(status != noErr) { NSLog(@"Failed to add mic to audio graph"); return; }
    status = AUGraphOpen(audioGraph);
    if(status != noErr) { NSLog(@"Failed to open audio graph"); return; }
    self.ioNode = ioNode;
    
    AudioUnit ioUnit;
    status = AUGraphNodeInfo(audioGraph, ioNode, &ioUnitDesc, &ioUnit);
    if(status != noErr) { NSLog(@"Failed to get mic handle from audio graph"); return; }
    self.ioUnit = ioUnit;
    
    UInt32 ioEnabled = 1;
    status = AudioUnitSetProperty(ioUnit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Input, kInputBusNumber, &ioEnabled, sizeof(ioEnabled));
    if(status != noErr) { NSLog(@"Failed to set IO enabled on mic"); return; }
    
    size_t bytesPerSample = self.bytesPerSample;
    
    AudioStreamBasicDescription monoStreamFormat = {0};
    monoStreamFormat.mFormatID          = kAudioFormatLinearPCM;
    monoStreamFormat.mFormatFlags       = kAudioFormatFlagsCanonical;
    monoStreamFormat.mBytesPerPacket    = bytesPerSample;
    monoStreamFormat.mFramesPerPacket   = 1;
    monoStreamFormat.mBytesPerFrame     = bytesPerSample;
    monoStreamFormat.mChannelsPerFrame  = 1;
    monoStreamFormat.mBitsPerChannel    = 8 * bytesPerSample;
    monoStreamFormat.mSampleRate        = self.sampleRate;
    
    status = AudioUnitSetProperty(ioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, kInputBusNumber, &monoStreamFormat, sizeof(monoStreamFormat));
    if(status != noErr) { NSLog(@"Failed to set stream format on mic"); return; }
    
    AURenderCallbackStruct inputCallbackStruct;
    inputCallbackStruct.inputProc        = &recordingCallback;
    inputCallbackStruct.inputProcRefCon  = (__bridge void *)(self);
    status = AudioUnitSetProperty(ioUnit, kAudioOutputUnitProperty_SetInputCallback, kAudioUnitScope_Global, kInputBusNumber, &inputCallbackStruct, sizeof(inputCallbackStruct));
    if(status != noErr) { NSLog(@"Failed to set input callback on mic"); return; }
    
    status = AudioUnitSetProperty(ioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, kOutputBusNumber, &monoStreamFormat, sizeof(monoStreamFormat));
    if(status != noErr) { NSLog(@"Failed to set stream format on speaker"); return; }

    AURenderCallbackStruct outputCallbackStruct;
    outputCallbackStruct.inputProc        = &playbackCallback;
    outputCallbackStruct.inputProcRefCon  = (__bridge void *)(self);
    status = AudioUnitSetProperty(ioUnit, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Output, kOutputBusNumber, &outputCallbackStruct, sizeof(outputCallbackStruct));
    if(status != noErr) { NSLog(@"Failed to set render callback on speaker"); return; }
    
    @try {
        status = AUGraphInitialize(audioGraph);
        if(status != noErr) {
            NSLog(@"Failed to initialize audio graph");
        }
    }
    @catch(...) {
        NSLog(@"Failed to initialize audio graph");
    }

    startEncPos = curInputPos = curEncodePos = (unsigned char*)malloc(OPUS_FRAME_SIZE * 15);
    AudioBufferList *inputData = (AudioBufferList*)malloc(sizeof(AudioBufferList));
    inputData->mNumberBuffers = 1;
    inputData->mBuffers[0].mNumberChannels = 1;
    inputData->mBuffers[0].mDataByteSize = self.bytesPerFrame;
    inputData->mBuffers[0].mData = startEncPos;
    self.inputData = inputData;
    
    playCallbackBuffer = (unsigned char*)malloc(AUDIOUNIT_FRAME_SIZE);
}

-(void)startVoiceCapture;
{
    self.inputData->mBuffers[0].mData = startEncPos;
    curEncodePos = curInputPos = startEncPos;
    [decodeBufferQueue initBufferList];
    if(skipDecodeSize>0)
        [decodeBufferQueue pushReadyBuffer:remainDecodeBuffer];
    skipDecodeSize = 0;
    OSStatus status = AUGraphStart(self.audioGraph);
}

- (void)stopVoiceCapture;
{
    OSStatus status = AUGraphStop(self.audioGraph);
}

-(void)processInputBuffer: (AudioBufferList*) audioBufferList
{
    if( _audioInputDeletegate->func((short*)curEncodePos)>0)
    {
        [self stopVoiceCapture];
    }
    
    int size = (curInputPos - curEncodePos);
    if( size >= OPUS_FRAME_SIZE)
    {
        curEncodePos += OPUS_FRAME_SIZE;
        if( _audioInputDeletegate->func((short*)curEncodePos)>0)
        {
            [self stopVoiceCapture];
        }
        
        curEncodePos = startEncPos;
        curInputPos = startEncPos;
    }
    else
    {
        curEncodePos += OPUS_FRAME_SIZE;
        curInputPos += audioBufferList->mBuffers[0].mDataByteSize;
    }
    self.inputData->mBuffers[0].mData = curInputPos;
}

- (short*)getOutputAudioBuffer
{
    return (short*)[decodeBufferQueue getReadyBuffer];
}

- (void)queueDecodeAudioBuffer:(short*) buffer
{
    [decodeBufferQueue pushDecodeBuffer:(unsigned char *)buffer];
}

- (BOOL)writeOutputAudioBuffer:(AudioBufferList *)ioData
{
    if(skipDecodeSize==0)
    {
        if([decodeBufferQueue getDecodeBufferSize]>1)
        {
            remainDecodeBuffer = [decodeBufferQueue getDecodeBuffer];
            memcpy(playCallbackBuffer, remainDecodeBuffer, OPUS_FRAME_SIZE);
            [decodeBufferQueue pushReadyBuffer:remainDecodeBuffer];
            
            skipDecodeSize = AUDIOUNIT_FRAME_SIZE - OPUS_FRAME_SIZE;
            
            remainDecodeBuffer = [decodeBufferQueue getDecodeBuffer];
            memcpy(playCallbackBuffer + OPUS_FRAME_SIZE, remainDecodeBuffer, skipDecodeSize);
        }
        else
        {
            memset(playCallbackBuffer, 0, AUDIOUNIT_FRAME_SIZE);
        }
    }
    else
    {
        if([decodeBufferQueue getDecodeBufferSize]>0)
        {
            int buffer_size = OPUS_FRAME_SIZE - skipDecodeSize;
            memcpy(playCallbackBuffer, remainDecodeBuffer + skipDecodeSize, buffer_size);
            [decodeBufferQueue pushReadyBuffer:remainDecodeBuffer];
            
            skipDecodeSize = AUDIOUNIT_FRAME_SIZE - buffer_size;
            remainDecodeBuffer = [decodeBufferQueue getDecodeBuffer];
            memcpy(playCallbackBuffer + buffer_size, remainDecodeBuffer, skipDecodeSize);
            if(skipDecodeSize >= OPUS_FRAME_SIZE)
            {
                skipDecodeSize = 0;
                [decodeBufferQueue pushReadyBuffer:remainDecodeBuffer];
            }
        }
        else
        {
            memset(playCallbackBuffer, 0, AUDIOUNIT_FRAME_SIZE);
        }
    }
    ioData->mBuffers[0].mData = playCallbackBuffer;
    
    return NO;
}

@end
