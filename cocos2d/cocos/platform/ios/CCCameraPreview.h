//
//  CCCameraPreview.h
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//

#ifndef Anicamera_CCCameraPreview_h
#define Anicamera_CCCameraPreview_h

#include"CCDevice.h"
#include<functional>
#include<vector>
#include<list>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import"platform/ios/CCVideoEncoder.h"
#import"platform/ios/CCAudioEncoder.h"
//Delegate Protocal for Face Detection.
@protocol GPUImageVideoCameraDelegate <NSObject>

@optional
- (void)willOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer;
@end

@interface CCCameraPreview  : NSObject<AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate>
{
    NSUInteger numberOfFramesCaptured;
    CGFloat totalFrameTimeDuringCapture;
    
    AVCaptureSession *_captureSession;
    AVCaptureDevice *_inputCamera;
    AVCaptureDeviceInput *videoInput;
    AVCaptureVideoDataOutput *videoOutput;
    AVCaptureDevice *frontCamera;
    AVCaptureDevice *backCamera;
    AVCaptureDevice *_microphone;
    AVCaptureDeviceInput *audioInput;
    AVCaptureAudioDataOutput *audioOutput;
    AudioBufferList audioBufferList;
    UIImage *mStoreUIImage;
    CGImage *mStoreCGImage;
    CCVideoEncoder *videoEncoder;
    CCAudioEncoder *audioEncoder;
    AVAssetWriter* _assetWriter;
    int _audioFrameCount;
    
    NSInteger _frameRate;
    
    dispatch_queue_t cameraProcessingQueue;
    dispatch_queue_t audioProcessingQueue;
    
    int cameraWidth, cameraHeight;
    int imageWidth, imageHeight;
    BOOL captureAsYUV;
    GLuint luminanceTexture, chrominanceTexture;
    pthread_mutex_t lock;
    BOOL readyToFaceDetect, enableFaceDetect;
    CIDetector *_detector;
    unsigned char *dst_y[3];
    unsigned char *dst_u[3];
    unsigned char *dst_v[3];
    unsigned char *rgba;
    int bufferSelect;
    CVPixelBufferRef pixelBuffer;
    BOOL isFirstFrame;
    VideoInputDeletegate *_videoInputDeletegate;
}

/// The AVCaptureSession used to capture from the camera
@property(readonly, retain, nonatomic) AVCaptureSession *captureSession;

/// This enables the capture session preset to be changed on the fly
@property (readwrite, nonatomic, copy) NSString *captureSessionPreset;

/// This sets the frame rate of the camera (iOS 5 and above only)
/**
 Setting this to 0 or below will set the frame rate back to the default setting for a particular preset.
 */
@property (readwrite) NSInteger frameRate;

/// Easy way to tell if front-facing camera is present on device
@property (readonly, getter = isFrontFacingCameraPresent) BOOL frontFacingCameraPresent;

/// This enables the benchmarking mode, which logs out instantaneous and average frame times to the console
@property(readwrite, nonatomic) BOOL runBenchmark;

/// Use this property to manage camera settings. Focus point, exposure point, etc.
@property(readonly) AVCaptureDevice *inputCamera;

/// These properties determine whether or not the two camera orientations should be mirrored. By default, both are NO.
@property(readwrite, nonatomic) BOOL horizontallyMirrorFrontFacingCamera, horizontallyMirrorRearFacingCamera;

@property(nonatomic, assign) id<GPUImageVideoCameraDelegate> delegate;

/// @name Initialization and teardown

+ (NSArray *)connectedCameraDevices;

/** Begin a capture session
 
 See AVCaptureSession for acceptable values
 
 @param sessionPreset Session preset to use
 @param cameraPosition Camera to capture from
 */
- (id)initWithSize:(int) previewSize;
- (id)initWithDeviceUniqueID:(NSString *)deviceUniqueID;
- (id)initWithSessionPreset:(NSString *)sessionPreset deviceUniqueID:(NSString *)deviceUniqueID;
- (id)initWithSessionPreset:(NSString *)sessionPreset cameraDevice:(AVCaptureDevice *)cameraDevice;
/** Tear down the capture session
 */
- (void)removeInputsAndOutputs;

/// @name Manage the camera video stream

/** Start camera capturing
 */
- (void)startCameraCapture;

/** Stop camera capturing
 */
- (void)stopCameraCapture;
/** Get the position (front, rear) of the source camera
 */
- (AVCaptureDevicePosition)cameraPosition;

/** Get the AVCaptureConnection of the source camera
 */
- (AVCaptureConnection *)videoCaptureConnection;

/** This flips between the front and rear cameras
 */
- (void)rotateCamera;

- (void)setVideoInputDeletegate:(VideoInputDeletegate*) delegate;

- (bool)savePhoto:(unsigned char*)rawData width:(int)width height:(int)height;
- (bool)storePhoto:(const char *)filename;
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;

- (void)setExposure:(float)value;
- (float)getExposure;
- (BOOL)turnFlashOnOff;
- (void)enableFaceDetect:(BOOL) enable;
- (short*)getOutputAudioBuffer;
- (void)encodingLoop;
- (void)pushInputBuffer:(short*)buffer;
- (void)startRecording:(AVAssetWriter*) assetWriter filename:(NSString*)filename;
- (void)addAudioInput;
- (void)removeAudioInput;
@end
#endif
