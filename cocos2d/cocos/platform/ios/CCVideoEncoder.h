//
//  CCCameraPreview.h
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//

#ifndef Anicamera_CCVideoEncoder_h
#define Anicamera_CCVideoEncoder_h

#include"CCDevice.h"
#include<functional>
#include<vector>
#include<list>
#import <AVFoundation/AVFoundation.h>

@interface CCVideoEncoder : NSObject
{
    AVAssetWriter* _assetWriter;
    AVAssetWriterInput* _assetWriterVideoInput;
    AVAssetWriterInputPixelBufferAdaptor* _assetWriterPixelBufferInput;
    
    NSString* captureFilename;
    CVPixelBufferRef pixel_buffer;
    int _width;
    int _height;
    VideoInputDeletegate* _videoInputDeletegate;
}

-(CCVideoEncoder*)init:(AVAssetWriter*) assetWriter width:(int)width height:(int)height;
-(void)setPixelBuffer:(CVPixelBufferRef) pixelBuffer;
-(void)setVideoDelegate:(VideoInputDeletegate *)videoInputDeletegate;
-(void)setCaptureFilename:(NSString*)filename;
-(void)encode:(CMTime) presentationTimeStamp;
-(void)stopRecording;
@end
#endif
