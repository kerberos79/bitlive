//
//  CCCameraPreview.mm
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//
#include <vector>
#include <list>
#include <pthread.h>
#import "CCVideoEncoder.h"

@implementation CCVideoEncoder

-(CCVideoEncoder*)init:(AVAssetWriter*) assetWriter width:(int)width height:(int)height
{
    _assetWriter = assetWriter;
    _width = width;
    _height = height;
    NSMutableDictionary *settings = [[NSMutableDictionary alloc] init];
    [settings setObject:AVVideoCodecH264 forKey:AVVideoCodecKey];
    [settings setObject:[NSNumber numberWithInt:width] forKey:AVVideoWidthKey];
    [settings setObject:[NSNumber numberWithInt:height] forKey:AVVideoHeightKey];
    NSMutableDictionary * compressionProperties
    = [[NSMutableDictionary alloc] init];
    [compressionProperties setObject: [NSNumber numberWithInt: 1000000]
                              forKey: AVVideoAverageBitRateKey];
    [compressionProperties setObject: [NSNumber numberWithInt: 16]
                              forKey: AVVideoMaxKeyFrameIntervalKey];
    [compressionProperties setObject: AVVideoProfileLevelH264Main31
                              forKey: AVVideoProfileLevelKey];
    [settings setObject: compressionProperties
                       forKey: AVVideoCompressionPropertiesKey];
    _assetWriterVideoInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:settings];
    _assetWriterVideoInput.expectsMediaDataInRealTime = YES;
    
    // You need to use BGRA for the video in order to get realtime encoding. I use a color-swizzling shader to line up glReadPixels' normal RGBA output with the movie input's BGRA.
    NSDictionary *sourcePixelBufferAttributesDictionary =
    [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA],  kCVPixelBufferPixelFormatTypeKey,
                                                kCFBooleanTrue,kCVPixelBufferCGBitmapContextCompatibilityKey,
                                                [NSNumber numberWithInt:width], kCVPixelBufferWidthKey,
                                                [NSNumber numberWithInt:height], kCVPixelBufferHeightKey,
                                                nil];
    
    _assetWriterPixelBufferInput = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:_assetWriterVideoInput sourcePixelBufferAttributes:sourcePixelBufferAttributesDictionary];
    [_assetWriterPixelBufferInput retain];
    
    [_assetWriter addInput:_assetWriterVideoInput];
    
    [compressionProperties release];
    [settings release];
    return self;
}
-(void)setCaptureFilename:(NSString*)filename
{
    captureFilename = [filename copy];
}

-(void)setPixelBuffer:(CVPixelBufferRef) pixelBuffer
{
    pixel_buffer = pixelBuffer;
}

-(void)setVideoDelegate:(VideoInputDeletegate *) videoInputDeletegate
{
    _videoInputDeletegate = videoInputDeletegate;
}

-(void)encode:(CMTime) presentationTimeStamp
{
    BOOL append_ok = NO;
    CVPixelBufferLockBaseAddress(pixel_buffer, 0);
    
    unsigned char* pixelBufferData = (unsigned char*)CVPixelBufferGetBaseAddress(pixel_buffer);
    
    _videoInputDeletegate->getVideoFrameRGBA(pixelBufferData);
    
    if (!_assetWriterVideoInput.readyForMoreMediaData)
    {
        NSLog(@"!readyForMoreMediaData");
        return;
    }
    append_ok = [_assetWriterPixelBufferInput appendPixelBuffer:pixel_buffer  withPresentationTime:presentationTimeStamp];
    CVPixelBufferUnlockBaseAddress(pixel_buffer, 0);
}

-(void)stopRecording
{
    [_assetWriterVideoInput markAsFinished];
    [_assetWriter finishWritingWithCompletionHandler:^{
        if (_assetWriter.status != AVAssetWriterStatusFailed && _assetWriter.status == AVAssetWriterStatusCompleted) {
            _videoInputDeletegate->completeRecordCallback(true);
        } else {
            _videoInputDeletegate->completeRecordCallback(false);
        }
        [self release];
    }];
}
@end
