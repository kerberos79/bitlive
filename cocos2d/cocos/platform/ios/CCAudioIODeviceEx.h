//
//  CCCameraPreview.h
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//

#ifndef Anicamera_CCAudioIODeviceEx_h
#define Anicamera_CCAudioIODeviceEx_h

#include"CCDevice.h"
#include<functional>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "CCAudioDecodeBufferQueue.h"
#import"platform/ios/CCAudioEncoder.h"
#include <mach/mach_time.h>


const AudioUnitElement kInputBusNumberEx = 1;
const AudioUnitElement kOutputBusNumberEx = 0;
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define PCM_READ_BYTESIZE       2048
#define PCM_BUFFER_SHORTSIZE    1024
#ifndef SHORT_MAX
#define SHORT_MAX 32767
#endif
#ifndef SHORT_MIN
#define SHORT_MIN -32768
#endif
@interface CCAudioIODeviceEx : NSObject
{
    // gain
    float gain;
    CMFormatDescriptionRef _format;
    CCAudioEncoder *_audioEncoder;
    AudioInputDeletegate *_audioInputDeletegate;
    mach_timebase_info_data_t tinfo;
    CMSampleBufferRef _sampleBuffer;
    
    unsigned char *_emptyPCMBuffer;
    unsigned char *_mixPCMBuffer;
    pthread_mutex_t lock;
    FILE *_sound;
}

@property (assign) BOOL isRecording;
@property (nonatomic) float gain;
@property (assign) double sampleRate;
@property (assign) double frameDuration;
@property (assign) int channels;
@property (assign) int samplesPerFrame;
@property (assign) int bytesPerSample;
@property (assign) int bytesPerFrame;
@property (assign) AudioBufferList *inputData;

@property (assign) AUGraph audioGraph;
@property (assign) AudioComponentDescription ioUnitDesc;
@property (assign) AUNode ioNode;
@property (assign) AudioUnit ioUnit;

- (CCAudioIODeviceEx*)initWithSamplerate:(int)samplerate channel:(int)channels duration:(double)duration;
- (void)dealloc;
- (void)setupAudioSession;
- (void)setupAudioIOGraph;
- (void)setAudioInputDeletegate:(AudioInputDeletegate*) delegate;
- (void)startVoiceCapture:(AVAssetWriter*) assetWriter;
- (void)stopVoiceCapture;
- (void)processInputBuffer:(AudioBufferList*) samples numSamples:(UInt32)inNumberFrames timstamp:(const AudioTimeStamp *)inTimeStamp;
- (BOOL)writeOutputAudioBuffer:(AudioBufferList *)ioData;
- (void)mixPCMBuffer:(short*) buffer size:(int)byteSize;
- (void)setMixSoundPath:(const char*) filepath;
@end
#endif
