//
//  CCAddressBook.m
//  Vista
//
//  Created by kerberos on 1/20/15.
//
//


#import "CCAddressBook.h"
#include "cocos2d.h"

@implementation CCAddressBook

- (id) initWithController:(UIViewController*) controller{
    self = [super init];
    _controller = controller;
    return self;
}
- (id) dealloc
{
}

- (void)launch{
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    
    picker.peoplePickerDelegate = self;
    
    [_controller presentModalViewController:picker animated:YES];
    
    [picker release];
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController*)peoplePicker;
{
    [_controller dismissModalViewControllerAnimated:YES];
}
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person NS_AVAILABLE_IOS(8_0) {
    NSLog(@"shouldContinueAfterSelectingPerson");
    
    std::vector<std::string>* list = new std::vector<std::string>();
    NSString *firstname = (NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *lastname = (NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
    if(firstname!=nil && lastname!=nil)
    {
        NSString *contactFirstLast = [NSString stringWithFormat:@"%@ %@", firstname, lastname];
         list->push_back([contactFirstLast UTF8String]);
    }
    else if(firstname==nil)
    {
        list->push_back([lastname UTF8String]);
    }
    else if(lastname==nil)
    {
        list->push_back([firstname UTF8String]);
    }
    //Get mobile phone number
    
    ABMultiValueRef phones =(NSString*)ABRecordCopyValue(person, kABPersonPhoneProperty);
    ABMultiValueRef emails =(NSString*)ABRecordCopyValue(person, kABPersonEmailProperty);
    
    NSString *mobile=@"";
    NSString *email=@"";
    NSString* mobileLabel;
    
    for(CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
        mobileLabel = (NSString*)ABMultiValueCopyLabelAtIndex(phones, i);
        if(mobileLabel != nil) {
            mobile = (NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            if(mobile!=nil) {
                list->push_back([mobile UTF8String]);
            }
        }
    }
    for(CFIndex i = 0; i < ABMultiValueGetCount(emails); i++) {
        email = (NSString*)ABMultiValueCopyValueAtIndex(emails, i);
        if(email!=nil) {
            list->push_back([email UTF8String]);
        }
    }
    UIImage *photo;
    // Check for contact picture
    if (ABPersonHasImageData(person)) {
        if ( &ABPersonCopyImageDataWithFormat != nil ) {
            // iOS >= 4.1
            photo = [UIImage imageWithData:(NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail)];
        } else {
            // iOS < 4.1
            photo = [UIImage imageWithData:(NSData *)ABPersonCopyImageData(person)];
        }
    } else {
        photo = nil;
    }
    
    if(list->size()>0 && cocos2d::Device::onContactListCallback)
    {
        if(photo!=nil)
        {
            NSData *imgData = UIImagePNGRepresentation(photo);
            NSUInteger len = [imgData length];
            
            // pictureBuffer will be deleted by InviteDialog.
            unsigned char* pictureBuffer = (unsigned char *)malloc(len);
            memcpy(pictureBuffer, [imgData bytes], len);
            cocos2d::Device::onContactListCallback(cocos2d::Device::Res::CheckContactList, list, pictureBuffer, len);
        }
        else
        {
            cocos2d::Device::onContactListCallback(cocos2d::Device::Res::CheckContactList, list, nullptr, 0);
        }
    }
    else
    {
        CCLOG("Device::onContactListCallback is nullptr!!");
        delete list;
    }

    [_controller dismissModalViewControllerAnimated:YES];
}
/*
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier NS_AVAILABLE_IOS(8_0) {
    
}
 */
@end
