//
//  CCCameraPreview.h
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//

#ifndef Anicamera_CCAudioEncoder_h
#define Anicamera_CCAudioEncoder_h

#include"CCDevice.h"
#include<functional>
#include<vector>
#include<list>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import"platform/ios/CCAudioEncoder.h"

@interface CCAudioEncoder : NSObject
{
    // gain
    float gain;
    
    unsigned char *readyData;
    AudioStreamBasicDescription monoStreamFormat;
    CMFormatDescriptionRef mCMAudioFormat;
    AVAssetWriter* _assetWriter;
    AVAssetWriterInput* _assetWriterAudioInput;
}

- (CCAudioEncoder*)init:(AVAssetWriter*) assetWriter samplerate:(float)samplerate channels:(int)channels;
- (void)dealloc;
-(void)encode:(CMSampleBufferRef)sampleBuffer;
-(void)stopRecording;
@end
#endif
