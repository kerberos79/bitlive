//
//  CCVibrator.m
//  cocos2d_libs
//
//  Created by keros on 5/30/15.
//
//

#import "CCVibrator.h"

@implementation CCVibrator

- (void)start
{
    thread = [[NSThread alloc] initWithTarget:self selector:@selector(play) object:nil];
    [thread start];
}

- (void)play
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    while([[NSThread currentThread] isCancelled] == NO)
    {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        [NSThread sleepForTimeInterval:1.5];
    }
    [pool release];
}

- (void)cancel
{
    [thread cancel];
}
@end
