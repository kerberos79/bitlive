//
//  CCCameraPreview.mm
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//

#import "CCAudioIODeviceEx.h"
#include <sys/time.h>
static OSStatus recordingCallback(void *inRefCon,
                                  AudioUnitRenderActionFlags *ioActionFlags,
                                  const AudioTimeStamp *inTimeStamp,
                                  UInt32 inBusNumber,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *ioData) {
    
    OSStatus status;
    CCAudioIODeviceEx *inputDevice = (__bridge CCAudioIODeviceEx*) inRefCon;
    AudioUnit ioUnit = inputDevice.ioUnit;
    ioData = inputDevice.inputData;
    if((status = AudioUnitRender(ioUnit, ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, ioData))>0)
    {
        NSLog(@"error : AudioUnitRender %d", (int)status);
    }
    else
    {
        [inputDevice processInputBuffer:ioData numSamples:inNumberFrames timstamp:inTimeStamp];
    }
    return noErr;
}

static OSStatus playbackCallback(void *inRefCon,
                                 AudioUnitRenderActionFlags *ioActionFlags,
                                 const AudioTimeStamp *inTimeStamp,
                                 UInt32 inBusNumber,
                                 UInt32 inNumberFrames,
                                 AudioBufferList *ioData) {
    CCAudioIODeviceEx *outputDevice = (CCAudioIODeviceEx*) inRefCon;
    [outputDevice writeOutputAudioBuffer:ioData];
    return noErr;
}

#pragma mark -
#pragma mark Private methods and instance variables


@implementation CCAudioIODeviceEx

@synthesize gain;

- (void)setAudioInputDeletegate:(AudioInputDeletegate*) delegate
{
    _audioInputDeletegate = delegate;
}

- (CCAudioIODeviceEx*)initWithSamplerate:(int)samplerate channel:(int)channels duration:(double)duration;
{
    self.sampleRate = samplerate;
    self.channels = channels;
    self.frameDuration = duration;
    [super init];
    
    [self setupAudioSession];
    [self setupAudioIOGraph];
    
    return self;
}

- (void)dealloc
{
    if(self.inputData!=nil)
    {
        delete self.inputData;
    }
    if(_emptyPCMBuffer!=nullptr)
        delete _emptyPCMBuffer;
    if(_mixPCMBuffer!=nullptr)
        delete _mixPCMBuffer;
    [self release];
    [super dealloc];
}

- (void)setupAudioSession
{
    NSError *error;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setPreferredSampleRate:self.sampleRate error:&error];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [audioSession setPreferredIOBufferDuration:self.frameDuration  error:&error];
    [audioSession setActive:YES error:&error];
    double sampleRate = audioSession.sampleRate;
    double ioBufferDuration = audioSession.IOBufferDuration;
    int samplesPerFrame = (int)(ioBufferDuration * sampleRate);
    int bytesPerSample = sizeof(AudioSampleType);
    int bytesPerFrame = samplesPerFrame * bytesPerSample;
    
    self.sampleRate = sampleRate;
    self.frameDuration = ioBufferDuration;
    self.samplesPerFrame = samplesPerFrame;
    self.bytesPerSample = bytesPerSample;
    self.bytesPerFrame = bytesPerFrame;
}

- (void)setupAudioIOGraph
{
    OSStatus status = noErr;
    
    AUGraph audioGraph;
    status = NewAUGraph(&audioGraph);
    if(status != noErr) { NSLog(@"Failed to create audio graph"); return; }
    self.audioGraph = audioGraph;
    
    AudioComponentDescription ioUnitDesc;
    ioUnitDesc.componentType = kAudioUnitType_Output;
    ioUnitDesc.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
    ioUnitDesc.componentManufacturer = kAudioUnitManufacturer_Apple;
    ioUnitDesc.componentFlags = 0;
    ioUnitDesc.componentFlagsMask = 0;
    self.ioUnitDesc = ioUnitDesc;
    
    AUNode ioNode;
    status = AUGraphAddNode(audioGraph, &ioUnitDesc, &ioNode);
    if(status != noErr) { NSLog(@"Failed to add mic to audio graph"); return; }
    status = AUGraphOpen(audioGraph);
    if(status != noErr) { NSLog(@"Failed to open audio graph"); return; }
    self.ioNode = ioNode;
    
    AudioUnit ioUnit;
    status = AUGraphNodeInfo(audioGraph, ioNode, &ioUnitDesc, &ioUnit);
    if(status != noErr) { NSLog(@"Failed to get mic handle from audio graph"); return; }
    self.ioUnit = ioUnit;
    
    UInt32 ioEnabled = 1;
    status = AudioUnitSetProperty(ioUnit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Input, kInputBusNumberEx, &ioEnabled, sizeof(ioEnabled));
    if(status != noErr) { NSLog(@"Failed to set IO enabled on mic"); return; }
    
    size_t bytesPerSample = self.bytesPerSample;
    
    AudioStreamBasicDescription monoStreamFormat = {0};
    monoStreamFormat.mFormatID          = kAudioFormatLinearPCM;
    monoStreamFormat.mFormatFlags       = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked|kAudioFormatFlagIsNonInterleaved;
    monoStreamFormat.mBytesPerPacket    = bytesPerSample;
    monoStreamFormat.mFramesPerPacket   = 1;
    monoStreamFormat.mBytesPerFrame     = bytesPerSample;
    monoStreamFormat.mChannelsPerFrame  = 1;
    monoStreamFormat.mBitsPerChannel    = 8 * bytesPerSample;
    monoStreamFormat.mSampleRate        = self.sampleRate;
    
    status = AudioUnitSetProperty(ioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, kInputBusNumberEx, &monoStreamFormat, sizeof(monoStreamFormat));
    if(status != noErr) { NSLog(@"Failed to set stream format on mic"); return; }
    
    AURenderCallbackStruct inputCallbackStruct;
    inputCallbackStruct.inputProc        = &recordingCallback;
    inputCallbackStruct.inputProcRefCon  = (__bridge void *)(self);
    status = AudioUnitSetProperty(ioUnit, kAudioOutputUnitProperty_SetInputCallback, kAudioUnitScope_Global, kInputBusNumberEx, &inputCallbackStruct, sizeof(inputCallbackStruct));
    if(status != noErr) { NSLog(@"Failed to set input callback on mic"); return; }
    
    status = AudioUnitSetProperty(ioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, kOutputBusNumberEx, &monoStreamFormat, sizeof(monoStreamFormat));
    if(status != noErr) { NSLog(@"Failed to set stream format on speaker"); return; }

    AURenderCallbackStruct outputCallbackStruct;
    outputCallbackStruct.inputProc        = &playbackCallback;
    outputCallbackStruct.inputProcRefCon  = (__bridge void *)(self);
    status = AudioUnitSetProperty(ioUnit, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Output, kOutputBusNumberEx, &outputCallbackStruct, sizeof(outputCallbackStruct));
    if(status != noErr) { NSLog(@"Failed to set render callback on speaker"); return; }

    @try {
        status = AUGraphInitialize(audioGraph);
        if(status != noErr) {
            NSLog(@"Failed to initialize audio graph");
        }
    }
    @catch(...) {
        NSLog(@"Failed to initialize audio graph");
    }
    AudioBufferList *inputData = (AudioBufferList*)malloc(sizeof(AudioBufferList));
    inputData->mNumberBuffers = 1;
    inputData->mBuffers[0].mNumberChannels = self.channels;
    inputData->mBuffers[0].mDataByteSize = self.bytesPerFrame;
    inputData->mBuffers[0].mData = (unsigned char*)malloc(self.bytesPerFrame);
    self.inputData = inputData;
    
    _emptyPCMBuffer = (unsigned char*)malloc(self.bytesPerFrame);
    memset(_emptyPCMBuffer, 0, self.bytesPerFrame);
    _mixPCMBuffer = (unsigned char*)malloc(self.bytesPerFrame);
    _format = NULL;
    _sound = NULL;
    self.isRecording = FALSE;
    status = CMAudioFormatDescriptionCreate(kCFAllocatorDefault, &monoStreamFormat, 0, NULL, 0, NULL, NULL, &_format);
    if (status != noErr) {
        // really shouldn't happen
        return;
    }
    kern_return_t kerror;
    kerror = mach_timebase_info(&tinfo);
    if (kerror != KERN_SUCCESS) {
        NSLog(@"Failed to mach_timebase_info");
    }
    pthread_mutex_init(&lock, NULL);
}

-(void)startVoiceCapture:(AVAssetWriter*) assetWriter;
{
    _audioEncoder = [[CCAudioEncoder alloc] init:assetWriter samplerate:self.sampleRate channels:self.channels];
    OSStatus status = AUGraphStart(self.audioGraph);
    self.isRecording = TRUE;
}

- (void)stopVoiceCapture;
{
    OSStatus status = AUGraphStop(self.audioGraph);
    [_audioEncoder stopRecording];
    CFRelease(_sampleBuffer);
    _audioEncoder = nullptr;
    self.isRecording = FALSE;
}

-(void)processInputBuffer: (AudioBufferList*) samples numSamples:(UInt32)inNumberFrames timstamp:(const AudioTimeStamp *)inTimeStamp
{
    int result = _audioInputDeletegate->func((short*)samples->mBuffers[0].mData);
    switch(result)
    {
        case 100:
        {
            [self mixPCMBuffer:(short*)samples->mBuffers[0].mData size:PCM_READ_BYTESIZE];
            OSStatus status;
            uint64_t time = inTimeStamp->mHostTime;
            time *= tinfo.numer;
            time /= tinfo.denom;
            CMTime presentationTime = CMTimeMake(time, 1000000000);
            CMSampleTimingInfo timing = { CMTimeMake(1, 16000), presentationTime, kCMTimeInvalid };
            
            _sampleBuffer = (CMSampleBufferRef)malloc(sizeof(CMSampleBufferRef));
            status = CMSampleBufferCreate(kCFAllocatorDefault,
                                          NULL,
                                          false,
                                          NULL,
                                          NULL,
                                          _format,
                                          inNumberFrames,
                                          1,
                                          &timing, 0, NULL, &_sampleBuffer);
            if (status != noErr) {
                // couldn't create the sample buffer
                NSLog(@"Failed to create sample buffer");
                CFRelease(_format);
                return;
            }
            status = CMSampleBufferSetDataBufferFromAudioBufferList(_sampleBuffer,
                                                                    kCFAllocatorDefault,
                                                                    kCFAllocatorDefault,
                                                                    0,
                                                                    samples);
            
            if (status != noErr) {
                NSLog(@"Failed to add samples to sample buffer");
                CFRelease(_sampleBuffer);
                CFRelease(_format);
                break;
            }
            [_audioEncoder encode:_sampleBuffer];
            break;
        }
        case 101:
        {
            [self stopVoiceCapture];
            break;
        }
        default:
            break;
    }
}

- (BOOL)writeOutputAudioBuffer:(AudioBufferList *)ioData
{
    pthread_mutex_lock(&lock);
    if(_sound==NULL)
    {
        pthread_mutex_unlock(&lock);
        memcpy((unsigned char *)ioData->mBuffers[0].mData, (unsigned char *)_emptyPCMBuffer, ioData->mBuffers[0].mDataByteSize);
        return TRUE;
    }
    int byteSize;
    if( (byteSize=fread(_mixPCMBuffer, 1, ioData->mBuffers[0].mDataByteSize, _sound))<PCM_READ_BYTESIZE)
    {
        fclose(_sound);
        _sound = NULL;
    }
    pthread_mutex_unlock(&lock);
    memcpy((unsigned char *)(ioData->mBuffers[0].mData), (unsigned char *)_mixPCMBuffer, byteSize);
    return TRUE;
}

- (void)mixPCMBuffer:(short*) buffer size:(int)byteSize
{
    pthread_mutex_lock(&lock);
    if(_sound==NULL)
    {
        pthread_mutex_unlock(&lock);
        return;
    }
    pthread_mutex_unlock(&lock);
    int mixed;
    short* src_ch = buffer;
    short* mix_ch = (short*)_mixPCMBuffer;
    
    for(int i=0;i<PCM_BUFFER_SHORTSIZE;i++)
    {
        mixed = *src_ch + *mix_ch++;
        if (mixed > SHORT_MAX) mixed = SHORT_MAX;
        else if (mixed < SHORT_MIN) mixed = SHORT_MIN;
        *src_ch++ = (short)mixed;
    }
}

- (void)setMixSoundPath:(const char*) filepath
{
    pthread_mutex_lock(&lock);
    if(_sound!=NULL)
        fclose(_sound);
    
    _sound = fopen(filepath, "rb");
    
    if (_sound == NULL) {
        NSLog(@"Open of testfile for reading failed");
    }
    else
    {
        fseek(_sound, 146, SEEK_SET);
    }
    pthread_mutex_unlock(&lock);
}
@end
