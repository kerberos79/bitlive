//
//  CCCameraPreview.h
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//

#ifndef Anicamera_CCAudioIODevice_h
#define Anicamera_CCAudioIODevice_h

#include"CCDevice.h"
#include<functional>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "CCAudioDecodeBufferQueue.h"


const AudioUnitElement kInputBusNumber = 1;
const AudioUnitElement kOutputBusNumber = 0;
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define OPUS_FRAME_SIZE          1920
#define AUDIOUNIT_FRAME_SIZE     2048

@interface CCAudioIODevice : NSObject
{
    int         currentBufferIndex;
    // gain
    float gain;
    
    AudioInputDeletegate *_audioInputDeletegate;
    CCAudioDecodeBufferQueue *decodeBufferQueue;
    unsigned char *readyData;
    
    unsigned char *startEncPos;
    unsigned char *curEncodePos;
    unsigned char *curInputPos;
    unsigned char *playCallbackBuffer;
    
    int skipDecodeSize;
    unsigned char *remainDecodeBuffer;
}

@property (readonly) int currentOutputBuffer;
@property (nonatomic) float gain;
@property (assign) double sampleRate;
@property (assign) double frameDuration;
@property (assign) int samplesPerFrame;
@property (assign) int bytesPerSample;
@property (assign) int bytesPerFrame;
@property (assign) AudioBufferList *inputData;

@property (assign) AUGraph audioGraph;
@property (assign) AudioComponentDescription ioUnitDesc;
@property (assign) AUNode ioNode;
@property (assign) AudioUnit ioUnit;

- (CCAudioIODevice*)initWithSamplerate:(int)samplerate channel:(int)channels duration:(double)duration;
- (void)dealloc;
- (void)setupAudioSession;
- (void)setupAudioIOGraph;
- (void)setAudioInputDeletegate:(AudioInputDeletegate*) delegate;
- (void)startVoiceCapture;
- (void)stopVoiceCapture;
- (void)processInputBuffer: (AudioBufferList*) audioBufferList;
- (short*)getOutputAudioBuffer;
- (BOOL)writeOutputAudioBuffer:(AudioBufferList *)ioData;
- (void)queueDecodeAudioBuffer:(short*) buffer;
@end
#endif
