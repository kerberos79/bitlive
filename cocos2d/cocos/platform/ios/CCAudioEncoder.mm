//
//  CCCameraPreview.mm
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//
#include <vector>
#include <list>
#include <pthread.h>
#import "CCAudioEncoder.h"

@implementation CCAudioEncoder

-(CCAudioEncoder*)init:(AVAssetWriter*) assetWriter samplerate:(float)samplerate channels:(int)channels
{
    _assetWriter = assetWriter;
    
    AudioChannelLayout acl;
    bzero( &acl, sizeof(acl));
    acl.mChannelLayoutTag = (channels==1)?kAudioChannelLayoutTag_Mono:kAudioChannelLayoutTag_Stereo;
    
    NSDictionary* audioOutputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                           [ NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                           [ NSNumber numberWithInt: channels ], AVNumberOfChannelsKey,
                           [ NSNumber numberWithFloat: samplerate ], AVSampleRateKey,
                           [ NSData dataWithBytes: &acl length: sizeof( acl ) ], AVChannelLayoutKey,
                           [ NSNumber numberWithInt: 16000 ], AVEncoderBitRateKey,
                           nil];
    
    _assetWriterAudioInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:audioOutputSettings];
    _assetWriterAudioInput.expectsMediaDataInRealTime = YES;
    [_assetWriter addInput:_assetWriterAudioInput];
    
    return self;
}

- (void)encode:(CMSampleBufferRef)sampleBuffer
{
    if (!_assetWriterAudioInput.readyForMoreMediaData)
    {
        NSLog(@"!readyForMoreMediaData");
        return;
    }
    
    if(![_assetWriterAudioInput appendSampleBuffer:sampleBuffer])
    {
        NSLog(@"fail to Audio input appendSampleBuffer");
    }    
}

-(void)stopRecording
{
    [_assetWriterAudioInput markAsFinished];
    [self release];
}
@end
