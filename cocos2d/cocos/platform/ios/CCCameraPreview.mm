//
//  CCCameraPreview.mm
//  Anicamera
//
//  Created by kerberos on 10/25/14.
//
//

#import "CCCameraPreview.h"
#import <AssetsLibrary/AssetsLibrary.h>
#include "libyuv.h"

#pragma mark -
#pragma mark Private methods and instance variables

@interface CCCameraPreview()
{
}

- (void)updateOrientationSendToTargets;
- (void)convertYUVToRGBOutput;

@end

@implementation CCCameraPreview

@synthesize captureSessionPreset = _captureSessionPreset;
@synthesize captureSession = _captureSession;
@synthesize inputCamera = _inputCamera;
@synthesize runBenchmark = _runBenchmark;
@synthesize horizontallyMirrorFrontFacingCamera = _horizontallyMirrorFrontFacingCamera, horizontallyMirrorRearFacingCamera = _horizontallyMirrorRearFacingCamera;

#pragma mark -
#pragma mark Initialization and teardown

+ (NSArray *)connectedCameraDevices;
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    return devices;
}

- (id)initWithSize:(int) previewSize;
{
    if(previewSize == 480)
    {
        if (!(self = [self initWithSessionPreset:AVCaptureSessionPreset640x480 cameraDevice:nil]))
        {
            return nil;
        }
        
        cameraWidth = 640;
        cameraHeight = 480;
        imageWidth = 480;
        imageHeight = 640;
    }
    else
    {
        if (!(self = [self initWithSessionPreset:AVCaptureSessionPreset1280x720 cameraDevice:nil]))
        {
            return nil;
        }
        
        cameraWidth = 1280;
        cameraHeight = 720;
        imageWidth = 720;
        imageHeight = 1280;
    }
    for(int i=0;i<3;i++)
    {
        dst_y[i] = new unsigned char[imageWidth*imageHeight*3/2];
        dst_u[i] = dst_y[i] + imageWidth*imageHeight;
        dst_v[i] = dst_u[i] + imageWidth*imageHeight/4;
    }
    rgba = new unsigned char[imageWidth*imageHeight*4];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, imageWidth,
                                          imageHeight, kCVPixelFormatType_32BGRA, (CFDictionaryRef) options,
                                          &pixelBuffer);
    if (pixelBuffer == NULL || (status != kCVReturnSuccess))
    {
        CVPixelBufferRelease(pixelBuffer);
        return nil;
    }
    bufferSelect = 0;
    
    return self;
}

- (id)initWithDeviceUniqueID:(NSString *)deviceUniqueID;
{
    if (!(self = [self initWithSessionPreset:AVCaptureSessionPreset640x480 deviceUniqueID:deviceUniqueID]))
    {
        return nil;
    }
    
    return self;
}

- (id)initWithSessionPreset:(NSString *)sessionPreset deviceUniqueID:(NSString *)deviceUniqueID;
{
    if (!(self = [self initWithSessionPreset:sessionPreset cameraDevice:[AVCaptureDevice deviceWithUniqueID:deviceUniqueID]]))
    {
        return nil;
    }
    
    return self;
}

- (id)initWithSessionPreset:(NSString *)sessionPreset cameraDevice:(AVCaptureDevice *)cameraDevice;
{
    if (!(self = [super init]))
    {
        return nil;
    }
#if 1
    mStoreCGImage = nil;
#else
    mStoreUIImage = nil;
#endif
    cameraProcessingQueue = dispatch_queue_create("cameraProcessingQueue", NULL);
    audioProcessingQueue = dispatch_queue_create("audioProcessingQueue", NULL);
    
    _frameRate = 0; // This will not set frame rate unless this value gets set to 1 or above
    _runBenchmark = NO;
    //    captureAsYUV = YES;
    captureAsYUV = YES;
    
    NSArray *devices = [AVCaptureDevice devices];
    
    for (AVCaptureDevice *device in devices) {
        
        //NSLog(@"Device name: %@", [device localizedName]);
        
        if ([device hasMediaType:AVMediaTypeVideo]) {
            
            if ([device position] == AVCaptureDevicePositionBack) {
                //NSLog(@"Device position : back");
                backCamera = device;
            }
            else {
                //NSLog(@"Device position : front");
                frontCamera = device;
            }
        }
    }
    
    // Grab the back-facing or front-facing camera
    _inputCamera = nil;
    
    if (cameraDevice == nil)
    {
        _inputCamera = frontCamera;
    }
    else
    {
        _inputCamera = cameraDevice;
    }
    
    if (!_inputCamera) {
        return nil;
    }
    
    // Create the capture session
    _captureSession = [[AVCaptureSession alloc] init];
    
    [_captureSession beginConfiguration];
    
    // Add the video input
    NSError *error = nil;
    videoInput = [[AVCaptureDeviceInput alloc] initWithDevice:_inputCamera error:&error];
    if ([_captureSession canAddInput:videoInput])
    {
        [_captureSession addInput:videoInput];
    }
    
    // Add the video frame output
    videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    [videoOutput setAlwaysDiscardsLateVideoFrames:NO];
    
    
    if (captureAsYUV)
    {
        BOOL supportsFullYUVRange = NO;
        
        NSArray *supportedPixelFormats = videoOutput.availableVideoCVPixelFormatTypes;
        for (NSNumber *currentPixelFormat in supportedPixelFormats)
        {
            if ([currentPixelFormat intValue] == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)
            {
                supportsFullYUVRange = YES;
            }
        }
        
        if (supportsFullYUVRange)
        {
            [videoOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
        }
        else
        {
            [videoOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
        }
    }
    else
    {
        // Despite returning a longer list of supported pixel formats, only RGB, RGBA, BGRA, and the YUV 4:2:2 variants seem to return cleanly
        [videoOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
        //        [videoOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_422YpCbCr8_yuvs] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
    }
    
    [videoOutput setSampleBufferDelegate:self queue:cameraProcessingQueue];
    //    [videoOutput setSampleBufferDelegate:self queue:[GPUImageContext sharedContextQueue]];
    if ([_captureSession canAddOutput:videoOutput])
    {
        [_captureSession addOutput:videoOutput];
    }
    else
    {
        NSLog(@"Couldn't add video output");
        return nil;
    }
    
    _captureSessionPreset = sessionPreset;
    [_captureSession setSessionPreset:_captureSessionPreset];
    
    // This will let you get 60 FPS video from the 720p preset on an iPhone 4S, but only that device and that preset
    //    AVCaptureConnection *conn = [videoOutput connectionWithMediaType:AVMediaTypeVideo];
    //
    //    if (conn.supportsVideoMinFrameDuration)
    //        conn.videoMinFrameDuration = CMTimeMake(1,60);
    //    if (conn.supportsVideoMaxFrameDuration)
    //        conn.videoMaxFrameDuration = CMTimeMake(1,60);
    
    [_captureSession commitConfiguration];
    
    [self setFrameRate:20];
    pthread_mutex_init(&lock, NULL);
    readyToFaceDetect = FALSE;
    enableFaceDetect = FALSE;
    return self;
}

- (void)dealloc
{
    [self stopCameraCapture];
    [videoOutput setSampleBufferDelegate:nil queue:dispatch_get_main_queue()];
//    [audioOutput setSampleBufferDelegate:nil queue:dispatch_get_main_queue()];
    
    [self removeInputsAndOutputs];
    
    // ARC forbids explicit message send of 'release'; since iOS 6 even for dispatch_release() calls: stripping it out in that case is required.
#if ( (__IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0) || (!defined(__IPHONE_6_0)) )
    if (cameraProcessingQueue != NULL)
    {
        dispatch_release(cameraProcessingQueue);
    }
#endif
    [super dealloc];
    for(int i=0;i<3;i++)
    {
        if(dst_y[i])
            delete dst_y[i];
        if(dst_u[i])
            delete dst_u[i];
        if(dst_v[i])
            delete dst_v[i];
    }
    if(rgba)
        delete rgba;
    if(pixelBuffer!=nil)
        CVPixelBufferRelease(pixelBuffer);
}

- (void)removeInputsAndOutputs;
{
    [_captureSession removeInput:videoInput];
    [_captureSession removeOutput:videoOutput];
}


#pragma mark -
#pragma mark Manage the camera video stream

- (void)startCameraCapture;
{
    if (![_captureSession isRunning])
    {
        [_captureSession startRunning];
    };
}

- (void)stopCameraCapture;
{
    if ([_captureSession isRunning])
    {
        [_captureSession stopRunning];
    }
}

- (void)rotateCamera
{
    if (self.frontFacingCameraPresent == NO)
        return;
    
    NSError *error;
    AVCaptureDeviceInput *newVideoInput;
    AVCaptureDevicePosition currentCameraPosition = [[videoInput device] position];
    
    if (currentCameraPosition == AVCaptureDevicePositionBack)
    {
        currentCameraPosition = AVCaptureDevicePositionFront;
    }
    else
    {
        currentCameraPosition = AVCaptureDevicePositionBack;
    }
    
    AVCaptureDevice *backFacingCamera = nil;
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == currentCameraPosition)
        {
            backFacingCamera = device;
        }
    }
    newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:backFacingCamera error:&error];
    
    if (newVideoInput != nil)
    {
        [_captureSession beginConfiguration];
        
        [_captureSession removeInput:videoInput];
        if ([_captureSession canAddInput:newVideoInput])
        {
            [_captureSession addInput:newVideoInput];
            videoInput = newVideoInput;
        }
        else
        {
            [_captureSession addInput:videoInput];
        }
        //captureSession.sessionPreset = oriPreset;
        [_captureSession commitConfiguration];
    }
    
    _inputCamera = backFacingCamera;
    [self setFrameRate:_frameRate];
}

- (AVCaptureDevicePosition)cameraPosition
{
    return [[videoInput device] position];
}

- (BOOL)isFrontFacingCameraPresent;
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == AVCaptureDevicePositionFront)
            return YES;
    }
    
    return NO;
}

- (void)setCaptureSessionPreset:(NSString *)captureSessionPreset;
{
    [_captureSession beginConfiguration];
    
    _captureSessionPreset = captureSessionPreset;
    [_captureSession setSessionPreset:_captureSessionPreset];
    
    [_captureSession commitConfiguration];
}

- (void)setFrameRate:(NSInteger)frameRate;
{
    _frameRate = frameRate;
    
    if ([_inputCamera respondsToSelector:@selector(setActiveVideoMinFrameDuration:)] &&
        [_inputCamera respondsToSelector:@selector(setActiveVideoMaxFrameDuration:)]) {
        

        if ([_inputCamera lockForConfiguration:nil]) {
            [_inputCamera setActiveVideoMinFrameDuration:CMTimeMake(1, _frameRate)];
            [_inputCamera setActiveVideoMaxFrameDuration:CMTimeMake(1, _frameRate)];
            [_inputCamera unlockForConfiguration];
        }
        
    } else {
        
        for (AVCaptureConnection *connection in videoOutput.connections)
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
            if ([connection respondsToSelector:@selector(setVideoMinFrameDuration:)])
                connection.videoMinFrameDuration = CMTimeMake(1, _frameRate);
            
            if ([connection respondsToSelector:@selector(setVideoMaxFrameDuration:)])
                connection.videoMaxFrameDuration = CMTimeMake(1, _frameRate);
#pragma clang diagnostic pop
        }
    }
}

- (NSInteger)frameRate;
{
    return _frameRate;
}

- (AVCaptureConnection *)videoCaptureConnection {
    for (AVCaptureConnection *connection in [videoOutput connections] ) {
        for ( AVCaptureInputPort *port in [connection inputPorts] ) {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] ) {
                return connection;
            }
        }
    }
    
    return nil;
}


- (void)setVideoInputDeletegate:(VideoInputDeletegate*) delegate
{
    _videoInputDeletegate = delegate;
}

#pragma mark -
#pragma mark AVCaptureVideoDataOutputSampleBufferDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    if(captureOutput == videoOutput)
    {
        CVImageBufferRef cvImageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        CVPixelBufferLockBaseAddress(cvImageBuffer, 0);
        unsigned char *src_y = (unsigned char *)CVPixelBufferGetBaseAddressOfPlane(cvImageBuffer, 0);
        unsigned char *src_uv = (unsigned char *)CVPixelBufferGetBaseAddressOfPlane(cvImageBuffer, 1);
        
        int ret = libyuv::NV12ToI420Rotate(src_y, cameraWidth,
                               src_uv, cameraWidth,
                               dst_y[bufferSelect], imageWidth,
                               dst_v[bufferSelect], imageWidth/2,
                               dst_u[bufferSelect], imageWidth/2,
                               cameraWidth, cameraHeight, libyuv::RotationMode::kRotate90);

        switch(_videoInputDeletegate->func(dst_y[bufferSelect], dst_u[bufferSelect], dst_v[bufferSelect]))
        {
            case 0:
                break;
            case 1:
                [self stopCameraCapture];
                break;
            case 2:
                break;
            case 100: // record
            {
                if(_assetWriter.status == AVAssetWriterStatusWriting)
                {
                    CMTime currentSampleTime;
                    currentSampleTime = CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer);
                    if(isFirstFrame)
                    {
                        [_assetWriter startSessionAtSourceTime:currentSampleTime];
                        isFirstFrame = FALSE;
                    }
                    [videoEncoder encode:currentSampleTime];
                }
                break;
            }
            case 101: // stop record
            {
                [self stopRecording];
                break;
            }
            case 102: // start record gif
            case 103: // stop record gif
            {
                break;
            }

        }
        pthread_mutex_lock(&lock);
        if(readyToFaceDetect)
        {
            readyToFaceDetect = FALSE;
            [self performSelectorInBackground:@selector(markFaces:) withObject:[NSNumber numberWithInt:bufferSelect]];
        }
        pthread_mutex_unlock(&lock);

        bufferSelect++;
        if(bufferSelect>=3)
            bufferSelect = 0;
    }
    else
    {
        if(_assetWriter.status == AVAssetWriterStatusWriting)
        {
            if(isFirstFrame)
            {
                CMTime currentSampleTime;
                currentSampleTime = CMSampleBufferGetOutputPresentationTimeStamp(sampleBuffer);
                [_assetWriter startSessionAtSourceTime:currentSampleTime];
                isFirstFrame = FALSE;
            }
            if(audioEncoder)
            {
                [audioEncoder encode:sampleBuffer];
            }
        }
    }
}
-(void)markFaces:(NSNumber*)arg
{
    int ret;
    int index = [arg intValue];
    libyuv::I420ToRGBA(dst_y[index], imageWidth,
                       dst_u[index], imageWidth/2,
                       dst_v[index], imageWidth/2,
                       rgba, imageWidth*4, imageWidth, imageHeight);
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault | kCGImageAlphaPremultipliedLast;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, rgba, (imageWidth * imageHeight * 4), NULL);
    CGImageRef imageRef = CGImageCreate(imageWidth,
                                        imageHeight,
                                        8, 32, imageWidth * 4,
                                        colorSpaceRef,
                                        bitmapInfo,
                                        provider,
                                        NULL,
                                        NO,
                                        renderingIntent);
    CIImage *image = [[CIImage alloc] initWithCGImage:imageRef];
    
    CGImageRelease(imageRef);
    CGColorSpaceRelease(colorSpaceRef);
    CGDataProviderRelease(provider);
    
    NSDictionary *opts = @{ CIDetectorAccuracy: CIDetectorAccuracyHigh,
                            CIDetectorTracking: @YES,
                            CIDetectorMinFeatureSize: @0.15
                            };
    _detector = [CIDetector detectorOfType:CIDetectorTypeFace
                                              context:nil options:opts];
    NSArray* features = [_detector featuresInImage:image];
    
    if([features count]>0)
    {
        CIFaceFeature* faceFeature = [features objectAtIndex:0];
        cocos2d::Vec2 left, right;
        if(faceFeature.hasLeftEyePosition)
        {
            right.x = faceFeature.leftEyePosition.x * 0.966f;
            right.y =  faceFeature.leftEyePosition.y;
            left.x = faceFeature.rightEyePosition.x * 1.03f;
            left.y =  faceFeature.rightEyePosition.y;
            
            _videoInputDeletegate->faceDetect(left, right);
        }
    }
    [image release];
    pthread_mutex_lock(&lock);
    if(enableFaceDetect)
        readyToFaceDetect = TRUE;
    pthread_mutex_unlock(&lock);
}

- (bool)savePhoto:(unsigned char*)rawData width:(int)width height:(int)height
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
        CGDataProviderRef dataProviderRef = CGDataProviderCreateWithData(NULL, rawData, width*height*4, NULL);
        CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
        CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
        CGImageRef imageRef = CGImageCreate(width,
                                        height,
                                        8,
                                        32,
                                        width*4,
                                        colorSpaceRef,
                                        bitmapInfo,
                                        dataProviderRef,
                                        NULL,
                                        true,
                                        renderingIntent);
    
        UIImage *image = [UIImage imageWithCGImage:imageRef];
        [image retain];
        UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
        CGDataProviderRelease(dataProviderRef);
        CGColorSpaceRelease(colorSpaceRef);
        CGImageRelease(imageRef);
    });
    return true;
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo: (void *)contextInfo
{
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    } else {
        NSLog(@"saved");
    }
}

- (bool)storePhoto:(const char *)filename
{
    return true;
}

- (void)setExposure:(float)value
{
    if([_inputCamera lockForConfiguration:nil])
    {
        float minISO = _inputCamera.activeFormat.minISO;
        float maxISO = _inputCamera.activeFormat.maxISO;
        float clampedISO = _inputCamera.ISO + (value - 50.0f)*4;
        
        if(clampedISO <minISO)
            clampedISO = minISO;
        else if(clampedISO >maxISO)
            clampedISO = maxISO;
        
        [_inputCamera setExposureModeCustomWithDuration:AVCaptureExposureDurationCurrent ISO:clampedISO completionHandler:nil];
        [_inputCamera unlockForConfiguration];
    }
}

- (float)getExposure
{
    return 50.0f;
}

- (BOOL)turnFlashOnOff
{
    if([_inputCamera lockForConfiguration:nil])
    {
        if ([_inputCamera hasFlash]){
            if(_inputCamera.flashMode == AVCaptureFlashModeOff)
                [_inputCamera setFlashMode:AVCaptureFlashModeOn];
            else
                [_inputCamera setFlashMode:AVCaptureFlashModeOff];
            
            [_inputCamera unlockForConfiguration];
            return true;
        }
        else
        {
            [_inputCamera unlockForConfiguration];
            return false;
        }
    }
    return true;
}

- (void)enableFaceDetect:(BOOL) enable
{
    if(enable)
    {
        pthread_mutex_lock(&lock);
        if(!enableFaceDetect)
        {
            readyToFaceDetect = TRUE;
            enableFaceDetect = TRUE;
        }
        pthread_mutex_unlock(&lock);
    }
    else
    {
        pthread_mutex_lock(&lock);
        if(enableFaceDetect)
        {
            enableFaceDetect = FALSE;
        }
        pthread_mutex_unlock(&lock);
    }
}

- (void)startRecording:(AVAssetWriter*) assetWriter filename:(NSString*)filename
{
    _assetWriter = assetWriter;
    _audioFrameCount = 0;
    isFirstFrame = TRUE;
    videoEncoder = [[CCVideoEncoder alloc] init:assetWriter width:imageWidth height:imageHeight];
    [videoEncoder setPixelBuffer:pixelBuffer];
    [videoEncoder setCaptureFilename:filename];
    [videoEncoder setVideoDelegate:_videoInputDeletegate];
}

- (void)stopRecording
{
    [videoEncoder stopRecording];
    _assetWriter = nullptr;
}

- (void)addAudioInput
{
    if(_microphone==nil)
    {
        _microphone = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
        audioInput = [AVCaptureDeviceInput deviceInputWithDevice:_microphone error:nil];
        audioOutput = [[AVCaptureAudioDataOutput alloc] init];
    }
    if ([_captureSession canAddInput:audioInput])
    {
        [_captureSession addInput:audioInput];
    }
    
    if ([_captureSession canAddOutput:audioOutput])
    {
        [_captureSession addOutput:audioOutput];
    }
    else
    {
        NSLog(@"Couldn't add audio output");
    }
    [audioOutput setSampleBufferDelegate:self queue:audioProcessingQueue];
}

- (void)removeAudioInput
{
    [_captureSession removeInput:audioInput];
    [_captureSession removeOutput:audioOutput];
    audioInput = nil;
    audioOutput = nil;
    _microphone = nil;
}

@end
