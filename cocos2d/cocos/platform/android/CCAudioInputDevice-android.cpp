#include "platform/CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "jni/JniHelper.h"
#include "jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#include "CCAudioInputDevice-android.h"
#include "base/CCDirector.h"
#include <android/log.h>
#include <jni.h>
#include <cstring>

NS_CC_BEGIN

static short recorderBuffer[RECORDER_FRAMES];

AudioInputDevice* AudioInputDevice::mAudioInputDevice = nullptr;

AudioInputDevice* AudioInputDevice::getInstance()
{
	if(mAudioInputDevice==nullptr)
	{
		mAudioInputDevice = new AudioInputDevice();
		mAudioInputDevice->createEngine();
	}

	return mAudioInputDevice;
}

AudioInputDevice::AudioInputDevice()
{
}

AudioInputDevice::~AudioInputDevice()
{
}

void AudioInputDevice::createEngine()
{
    SLresult result;

    // create engine
    result = slCreateEngine(&engineObject, 0, NULL, 0, NULL, NULL);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the engine
    result = (*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the engine interface, which is needed in order to create other objects
    result = (*engineObject)->GetInterface(engineObject, SL_IID_ENGINE, &engineEngine);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
    CCLOG("createEngine %x", engineObject);
}

void AudioInputDevice::createAudioRecorder(AudioInputDeletegate *delegate, unsigned int newSamplerate, unsigned int newChannels)
{
	_audioDelegate = delegate;

    SLresult result;

    // configure audio source
    SLDataLocator_IODevice loc_dev = {SL_DATALOCATOR_IODEVICE, SL_IODEVICE_AUDIOINPUT,
            SL_DEFAULTDEVICEID_AUDIOINPUT, NULL};
    SLDataSource audioSrc = {&loc_dev, NULL};

    // configure audio sink
    SLDataLocator_AndroidSimpleBufferQueue loc_bq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
    SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, 1, (SLuint32)newSamplerate*1000,
        SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
        SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN};
    SLDataSink audioSnk = {&loc_bq, &format_pcm};

    // create audio recorder
    // (requires the RECORD_AUDIO permission)
    const SLInterfaceID id[1] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE};
    const SLboolean req[1] = {SL_BOOLEAN_TRUE};
    result = (*engineEngine)->CreateAudioRecorder(engineEngine, &recorderObject, &audioSrc,
            &audioSnk, 1, id, req);
    if (SL_RESULT_SUCCESS != result) {
    	CCLOG("error %d", __LINE__);
        return ;
    }

    // realize the audio recorder
    result = (*recorderObject)->Realize(recorderObject, SL_BOOLEAN_FALSE);
    if (SL_RESULT_SUCCESS != result) {
    	CCLOG("error %d", __LINE__);
        return ;
    }

    // get the record interface
    result = (*recorderObject)->GetInterface(recorderObject, SL_IID_RECORD, &recorderRecord);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the buffer queue interface
    result = (*recorderObject)->GetInterface(recorderObject, SL_IID_ANDROIDSIMPLEBUFFERQUEUE,
            &recorderBufferQueue);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // register callback on the buffer queue
    result = (*recorderBufferQueue)->RegisterCallback(recorderBufferQueue, recordCallback,
            this);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // in case already recording, stop recording and clear buffer queue
    result = (*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_STOPPED);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
    result = (*recorderBufferQueue)->Clear(recorderBufferQueue);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
    // enqueue an empty buffer to be filled by the recorder
    // (for streaming recording, we would enqueue at least 2 empty buffers to start things off)
    result = (*recorderBufferQueue)->Enqueue(recorderBufferQueue, recorderBuffer,
            RECORDER_FRAMES * sizeof(short));
    // the most likely other result is SL_RESULT_BUFFER_INSUFFICIENT,
    // which for this code example would indicate a programming error
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
}

void AudioInputDevice::record() {

    SLresult result;

    // start recording
    result = (*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_RECORDING);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
}

void AudioInputDevice::pause() {
    SLresult result;
	result = (*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_STOPPED);
    assert(SL_RESULT_SUCCESS == result);
}

void AudioInputDevice::resume() {
    SLresult result;
	result = (*recorderRecord)->SetRecordState(recorderRecord,SL_RECORDSTATE_RECORDING);
    assert(SL_RESULT_SUCCESS == result);
}

void AudioInputDevice::stop() {
    SLresult result;
	result = (*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_STOPPED);
    assert(SL_RESULT_SUCCESS == result);
}

void AudioInputDevice::release() {

    // destroy audio recorder object, and invalidate all associated interfaces
    if (recorderObject != NULL) {
        (*recorderObject)->Destroy(recorderObject);
        recorderObject = NULL;
        recorderRecord = NULL;
        recorderBufferQueue = NULL;
    }

    // destroy engine object, and invalidate all associated interfaces
    if (engineObject != NULL) {
        (*engineObject)->Destroy(engineObject);
        engineObject = NULL;
        engineEngine = NULL;
    }

}

void AudioInputDevice::setVolume(int level) {
}
// this callback handler is called every time a buffer finishes playing
void AudioInputDevice::recordCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
    size_t buffer_size;
    size_t done;
    AudioInputDevice* audioOutputDevice = (AudioInputDevice*)context;
	(*bq)->Enqueue (bq, audioOutputDevice->buffer, PCM_SIZE);
	if(audioOutputDevice->_audioDelegate->func((short*)audioOutputDevice->buffer)>0)
	{
		audioOutputDevice->stop();
	}
}
NS_CC_END

#endif
