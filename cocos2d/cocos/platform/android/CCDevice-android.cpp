/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/

#include "platform/CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "platform/CCDevice.h"
#include <string.h>
#include <android/log.h>
#include <jni.h>
#include "base/ccTypes.h"
#include "base/CCUserDefault.h"
#include "jni/DPIJni.h"
#include "jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#include "jni/JniHelper.h"
#include "platform/CCFileUtils.h"
#include "CCAudioInputDevice-android.h"
#include "CCAudioOutputDevice-android.h"

NS_CC_BEGIN

std::function<void(Device::Res, void*, unsigned char*, int)> Device::onContactListCallback = nullptr;

int Device::getDPI()
{
    static int dpi = -1;
    if (dpi == -1)
    {
        dpi = (int)getDPIJNI();
    }
    return dpi;
}

void Device::setAccelerometerEnabled(bool isEnabled)
{
    if (isEnabled)
    {
        enableAccelerometerJni();
    }
    else
    {
        disableAccelerometerJni();
    }
}

void Device::setAccelerometerInterval(float interval)
{
	setAccelerometerIntervalJni(interval);
}

class BitmapDC
{
public:

    BitmapDC()
    : _data(nullptr)
    , _width(0)
    , _height(0)
    {
    }

    ~BitmapDC(void)
    {
    }

    bool getBitmapFromJavaShadowStroke(	const char *text,
    									int nWidth,
    									int nHeight,
    									Device::TextAlign eAlignMask,
    									const char * pFontName,
    									float fontSize,
    									float textTintR 		= 1.0,
    									float textTintG 		= 1.0,
    									float textTintB 		= 1.0,
    									bool shadow 			= false,
    									float shadowDeltaX 		= 0.0,
    									float shadowDeltaY 		= 0.0,
    									float shadowBlur 		= 0.0,
    									float shadowOpacity 	= 0.0,
    									bool stroke 			= false,
    									float strokeColorR 		= 0.0,
    									float strokeColorG 		= 0.0,
    									float strokeColorB 		= 0.0,
    									float strokeSize 		= 0.0 )
    {
           JniMethodInfo methodInfo;
           if (! JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxBitmap", "createTextBitmapShadowStroke",
               "(Ljava/lang/String;Ljava/lang/String;IFFFIIIZFFFFZFFFF)Z"))
           {
               CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
               return false;
           }

           // Do a full lookup for the font path using FileUtils in case the given font name is a relative path to a font file asset,
           // or the path has been mapped to a different location in the app package:
           std::string fullPathOrFontName = FileUtils::getInstance()->fullPathForFilename(pFontName);
            
           // If the path name returned includes the 'assets' dir then that needs to be removed, because the android.content.Context
           // requires this portion of the path to be omitted for assets inside the app package.
           if (fullPathOrFontName.find("assets/") == 0)
           {
               fullPathOrFontName = fullPathOrFontName.substr(strlen("assets/"));	// Chop out the 'assets/' portion of the path.
           }

           /**create bitmap
            * this method call Cococs2dx.createBitmap()(java code) to create the bitmap, the java code
            * will call Java_org_cocos2dx_lib_Cocos2dxBitmap_nativeInitBitmapDC() to init the width, height
            * and data.
            * use this approach to decrease the jni call number
           */
           jstring jstrText = methodInfo.env->NewStringUTF(text);
           jstring jstrFont = methodInfo.env->NewStringUTF(fullPathOrFontName.c_str());

           if(!shadow)
           {
               shadowDeltaX = 0.0f;
               shadowDeltaY = 0.0f;
               shadowBlur = 0.0f;
               shadowOpacity = 0.0f;
           }
           if (!stroke)
           {
               strokeColorR = 0.0f;
               strokeColorG = 0.0f;
               strokeColorB = 0.0f;
               strokeSize = 0.0f;
           }
           if(!methodInfo.env->CallStaticBooleanMethod(methodInfo.classID, methodInfo.methodID, jstrText,
               jstrFont, (int)fontSize, textTintR, textTintG, textTintB, eAlignMask, nWidth, nHeight, shadow, shadowDeltaX, -shadowDeltaY, shadowBlur, shadowOpacity, stroke, strokeColorR, strokeColorG, strokeColorB, strokeSize))
           {
                return false;
           }

           methodInfo.env->DeleteLocalRef(jstrText);
           methodInfo.env->DeleteLocalRef(jstrFont);
           methodInfo.env->DeleteLocalRef(methodInfo.classID);

           return true;
    }

public:
    int _width;
    int _height;
    unsigned char *_data;
};

static BitmapDC& sharedBitmapDC()
{
    static BitmapDC s_BmpDC;
    return s_BmpDC;
}

Data Device::getTextureDataForText(const char * text, const FontDefinition& textDefinition, TextAlign align, int &width, int &height, bool& hasPremultipliedAlpha)
{
    Data ret;
    do 
    {
        BitmapDC &dc = sharedBitmapDC();

        if(! dc.getBitmapFromJavaShadowStroke(text, 
            (int)textDefinition._dimensions.width, 
            (int)textDefinition._dimensions.height, 
            align, textDefinition._fontName.c_str(),
            textDefinition._fontSize,
            textDefinition._fontFillColor.r / 255.0f, 
            textDefinition._fontFillColor.g / 255.0f, 
            textDefinition._fontFillColor.b / 255.0f, 
            textDefinition._shadow._shadowEnabled,
            textDefinition._shadow._shadowOffset.width, 
            textDefinition._shadow._shadowOffset.height, 
            textDefinition._shadow._shadowBlur, 
            textDefinition._shadow._shadowOpacity,
            textDefinition._stroke._strokeEnabled, 
            textDefinition._stroke._strokeColor.r / 255.0f, 
            textDefinition._stroke._strokeColor.g / 255.0f, 
            textDefinition._stroke._strokeColor.b / 255.0f, 
            textDefinition._stroke._strokeSize )) { break;};

        width = dc._width;
        height = dc._height;
        ret.fastSet(dc._data,width * height * 4);
        hasPremultipliedAlpha = true;
    } while (0);

    return ret;
}


void Device::setKeepScreenOn(bool value)
{
    setKeepScreenOnJni(value);
}

VideoInputDeletegate* Device::_videoDelegate;
AudioInputDeletegate* Device::_audioDelegate;


void Device::startCameraPreview(VideoInputDeletegate *delegate, int previewSize)
{
	_videoDelegate = delegate;
	startCameraPreviewJni(previewSize);
}
void Device::flipCameraPreview()
{
	CCLOG("Device::flipCameraPreview()");
	flipCameraPreviewJni();
}

void Device::setExposureCamera(int value)
{
	CCLOG("Device::setExposureCamera()");
	setExposureCameraJni(value);
}

int Device::getExposureCamera()
{
	CCLOG("Device::getExposureCamera()");
	return getExposureCameraJni();
}

bool Device::turnCameraFlashOnOff()
{
	CCLOG("Device::turnCameraFlashOnOff()");
	return turnCameraFlashOnOffJni();
}

void Device::enableFaceDetect(bool enable)
{
	enableFaceDetectJni(enable);
}

int Device::updateFrame(unsigned char *dst_y, unsigned char *dst_u, unsigned char *dst_v)
{
	return _videoDelegate->func(dst_y, dst_u, dst_v);
}

void Device::getVideoFrame(unsigned char *dst_y, unsigned char *dst_uv)
{
	_videoDelegate->getVideoFrameNV21(dst_y, dst_uv);
}

void Device::updateFace(Vec2 left, Vec2 right)
{
	_videoDelegate->faceDetect(left, right);
}

void Device::complateRecord(bool result)
{
	_videoDelegate->completeRecordCallback(result);
}

void Device::releaseCameraPreview()
{
	CCLOG("Device::releaseCameraPreview()");
	releaseCameraJni();
}

bool Device::createAudioIO(int samplerate, int channels, double duration)
{
	createVoiceRecorderJni(samplerate, channels);

	AudioOutputDevice::getInstance()->createAudioPlayer(samplerate, channels);
	return true;
}

void Device::startVoiceRecorder(AudioInputDeletegate *delegate)
{
	_audioDelegate = delegate;
	startVoiceRecorderJni();
}

void Device::pauseVoiceRecorder()
{
	pauseVoiceRecorderJni();
}

void Device::resumeVoiceRecorder()
{
	resumeVoiceRecorderJni();
}

void Device::stopVoiceRecorder()
{
	stopVoiceRecorderJni();
}

void Device::releaseVoiceRecorder()
{
	releaseVoiceRecorderJni();
}

void Device::startVideoRecord(const char* filename)
{
	startVideoRecordJni(filename);
}

int Device::updatePCM(short *buffer)
{
	return _audioDelegate->func(buffer);
}

bool Device::startAudioTrack(int samplerate, int channels)
{
	AudioOutputDevice::getInstance()->play();
	return true;
}

short* Device::getAudioTrackBuffer()
{
	return (short*)AudioOutputDevice::getInstance()->getOutputAudioBuffer();
}

void Device::writeAudioTrack(short* buffer, int size)
{
	AudioOutputDevice::getInstance()->queueDecodeAudioBuffer(buffer);
}

void Device::stopAudioTrack()
{
	AudioOutputDevice::getInstance()->stop();
}

int Device::getRingerMode()
{
    return getRingerModeJNI(0);
}

void Device::vibrate(bool onoff)
{
	vibrateJNI(onoff);
}

std::string Device::getExternalStorageDirectory()
{
	return getExternalStorageDirectoryJNI();
}

std::string Device::getPhotoStorageDirectory()
{
    return getExternalStorageDirectoryJNI();
}

void Device::scanMediaNewFile(const char *filename)
{
	CCLOG("Device::scanMediaNewFile()");
	scanMediaNewFileJNI(filename);
}

void Device::sendActionShareImage(const char *filename)
{
	sendActionShareImageJNI(filename);
}

void Device::sendActionShareMovie(const char *filename)
{
	sendActionShareMovieJNI(filename);
}

void Device::requestContactList()
{
	requestContactListJNI();
}

void Device::searchContact(const char* key)
{
	searchContactJNI(key);
}

bool Device::getMyIPAddress(std::string& ipaddress, std::string& network_type)
{
	ipaddress = getMyIPAddressJNI();
	network_type = getNetworkTypeJNI();
	CCLOG("getMyIPAddress %s %s", ipaddress.c_str(), network_type.c_str());
	return (network_type.compare("none"))? true : false;
}

std::string Device::getPhoneNumber()
{
    return getPhoneNumberJNI();
}

void Device::setCountryCode(const char* code)
{
    UserDefault::getInstance()->setStringForKey("countrycode", code);
}

std::string Device::getCountryCode()
{
    return UserDefault::getInstance()->getStringForKey("countrycode");
}

std::string Device::getCountryCodeLocale()
{
	return getCountryCodeJNI();
}

std::string Device::getNetworkProvider()
{
	return getNetworkProviderJNI();
}

void Device::setRegisterID(const char* deviceToken)
{
	CCLOG("Not supported..");
}

std::string Device::getRegisterID()
{
	return getRegisterIDJNI();
}

void Device::setOfflineMessage(const char* message)
{
    UserDefault::getInstance()->setStringForKey("offlinemessage", message);
}

std::string Device::getOfflineMessage()
{
    std::string newMessage = UserDefault::getInstance()->getStringForKey("offlinemessage");
    if(newMessage.length()>0)
        UserDefault::getInstance()->setStringForKey("offlinemessage", "");
    return newMessage;
}

void Device::setHiddenMessage(const char* message)
{
    UserDefault::getInstance()->setStringForKey("hiddenmessage", message);
}

std::list<std::string>* Device::getHiddenMessageList()
{
    int hiddenMessageNum = UserDefault::getInstance()->getIntegerForKey("hiddenmessagenum");
    if(hiddenMessageNum==0)
        return nullptr;

    std::list<std::string>* result = new std::list<std::string>();
    std::ostringstream s;
    std::string newMessage;
    for(int i=0 ; i<hiddenMessageNum ; i++)
    {
        s << "hiddenmessage" << (i + 1);
        std::string query(s.str());
        newMessage = UserDefault::getInstance()->getStringForKey(query.c_str());
        result->push_back(newMessage);
    }
    UserDefault::getInstance()->setIntegerForKey("hiddenmessagenum", 0);
    return result;
}

int Device::getDeviceLevel()
{
	return getDeviceLevelJNI();
}

std::string Device::getInternalStoragePath()
{
	return getInternalStoragePathJNI();
}
NS_CC_END

#endif // CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

// this method is called by Cocos2dxBitmap
extern "C"
{
    /**
    * this method is called by java code to init width, height and pixels data
    */
    JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxBitmap_nativeInitBitmapDC(JNIEnv*  env, jobject thiz, int width, int height, jbyteArray pixels)
    {
        int size = width * height * 4;
        cocos2d::BitmapDC& bitmapDC = cocos2d::sharedBitmapDC();
        bitmapDC._width = width;
        bitmapDC._height = height;
        bitmapDC._data = (unsigned char*)malloc(sizeof(unsigned char) * size);
        env->GetByteArrayRegion(pixels, 0, size, (jbyte*)bitmapDC._data);
    }
};
