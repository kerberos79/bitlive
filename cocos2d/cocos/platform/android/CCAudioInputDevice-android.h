#ifndef __CC_AUDIOINPUTDEVICE_ANDROID_H__
#define __CC_AUDIOINPUTDEVICE_ANDROID_H__

#include "platform/CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "platform/CCCommon.h"
#include "platform/CCApplicationProtocol.h"
#include "CCAudioDecodeBufferQueue-android.h"
#include "CCDevice.h"
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
NS_CC_BEGIN

#define PCM_SIZE	1920
#define RECORDER_FRAMES (1920 * 6)
class CC_DLL AudioInputDevice
{
public:
	AudioInputDevice();
	~AudioInputDevice();
	static AudioInputDevice* getInstance();
	void createEngine();
	void createAudioRecorder(AudioInputDeletegate *delegate, unsigned int newSamplerate, unsigned int newChannels);
	void record();
	void pause();
	void resume();
	void stop();
	void release();
	void setVolume(int level);
	static void recordCallback(SLAndroidSimpleBufferQueueItf bq, void *context);

	unsigned char buffer[1920];
    AudioInputDeletegate *_audioDelegate;
private:
	static AudioInputDevice* mAudioInputDevice;
    SLObjectItf engineObject = NULL;
    SLEngineItf engineEngine = NULL;

    // buffer queue player interfaces
    SLObjectItf recorderObject = NULL;
    SLRecordItf recorderRecord;
    SLAndroidSimpleBufferQueueItf recorderBufferQueue = NULL;
    SLEffectSendItf bqPlayerEffectSend = NULL;
    SLMuteSoloItf bqPlayerMuteSolo = NULL;
    SLVolumeItf bqPlayerVolume = NULL;
    int volume;
};

NS_CC_END

#endif // CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#endif
