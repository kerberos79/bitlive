#include "platform/CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "jni/JniHelper.h"
#include "jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#include "CCAudioDecodeBufferQueue-android.h"
#include "base/CCDirector.h"
#include <android/log.h>
#include <jni.h>
#include <cstring>

#define  LOG_TAG    "main"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
NS_CC_BEGIN
AudioDecodeBufferQueue::AudioDecodeBufferQueue(int bytes)
{
    pthread_mutexattr_t mAttr;
    pthread_mutexattr_settype(&mAttr, PTHREAD_MUTEX_RECURSIVE_NP);
    pthread_mutex_init(&lock1, &mAttr);
    pthread_mutex_init(&lock2, &mAttr);
    pthread_cond_init(&condition, &mAttr);

    for(int i=0;i<QUEUE_SIZE;i++)
    {
        unsigned char *outputData = (unsigned char*)malloc(bytes);
        memset(outputData, 0, bytes);
        readyBufferQueue.push_back(outputData);
    }
}

AudioDecodeBufferQueue::~AudioDecodeBufferQueue()
{
    pthread_cond_signal(&condition);
    pthread_cond_destroy(&condition);
	pthread_mutex_destroy (&lock1);
	pthread_mutex_destroy (&lock2);

    while(!readyBufferQueue.empty())
    {
        unsigned char *outputData = readyBufferQueue.back();
        readyBufferQueue.pop_back();
        delete outputData;
    }
    while(!decodeBufferQueue.empty())
    {
        unsigned char *outputData = decodeBufferQueue.front();
        decodeBufferQueue.pop_front();
        delete outputData;
    }
}

void AudioDecodeBufferQueue::initBufferList()
{
    while(!decodeBufferQueue.empty())
    {
        readyBufferQueue.push_back(decodeBufferQueue.back());
        decodeBufferQueue.pop_back();
    }
}

unsigned char* AudioDecodeBufferQueue::getReadyBuffer()
{
    unsigned char *outputData = NULL;
    pthread_mutex_lock(&lock1);
    if(!readyBufferQueue.empty())
    {
        outputData = readyBufferQueue.front();
        readyBufferQueue.pop_front();
    }
    pthread_mutex_unlock(&lock1);
    return outputData;
}

void AudioDecodeBufferQueue::pushReadyBuffer(unsigned char *buffer)
{
    pthread_mutex_lock(&lock1);
    readyBufferQueue.push_back(buffer);
    pthread_mutex_unlock(&lock1);
}

unsigned char* AudioDecodeBufferQueue::getDecodeBuffer()
{
    unsigned char *outputData = NULL;
    pthread_mutex_lock(&lock2);

    if(decodeBufferQueue.empty())
    {
    	LOGD("empty");
        pthread_cond_wait(&condition, &lock2);
    }

    outputData = decodeBufferQueue.front();
    decodeBufferQueue.pop_front();

    pthread_mutex_unlock(&lock2);
    return outputData;
}

void AudioDecodeBufferQueue::pushDecodeBuffer(unsigned char *buffer)
{
    pthread_mutex_lock(&lock2);
    decodeBufferQueue.push_back(buffer);
    if(decodeBufferQueue.size()>3)
    {
        pthread_cond_signal(&condition);
    }
    pthread_mutex_unlock(&lock2);
}
NS_CC_END

#endif
