#ifndef __CC_AUDIODECODEBUFFERQUEUE_ANDROID_H__
#define __CC_AUDIODECODEBUFFERQUEUE_ANDROID_H__

#include "platform/CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "platform/CCCommon.h"
#include "platform/CCApplicationProtocol.h"
#include <pthread.h>
#include <vector>
#include <list>

NS_CC_BEGIN
#define QUEUE_SIZE  10
class CC_DLL AudioDecodeBufferQueue
{
public:
	AudioDecodeBufferQueue(int bytes);
	~AudioDecodeBufferQueue();

	unsigned char* getReadyBuffer();
	void pushReadyBuffer(unsigned char *buffer);
	unsigned char* getDecodeBuffer();
	void pushDecodeBuffer(unsigned char *buffer);
	void initBufferList();
private:
    pthread_cond_t condition;
    pthread_mutex_t lock1;
    pthread_mutex_t lock2;
    std::list<unsigned char *>       decodeBufferQueue;
    std::list<unsigned char *>     readyBufferQueue;
};

NS_CC_END

#endif // CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#endif
