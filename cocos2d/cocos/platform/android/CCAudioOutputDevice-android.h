#ifndef __CC_AUDIOOUTPUTDEVICE_ANDROID_H__
#define __CC_AUDIOOUTPUTDEVICE_ANDROID_H__

#include "platform/CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "platform/CCCommon.h"
#include "platform/CCApplicationProtocol.h"
#include "CCAudioDecodeBufferQueue-android.h"
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
NS_CC_BEGIN
static const SLEnvironmentalReverbSettings reverbSettings =
        				SL_I3DL2_ENVIRONMENT_PRESET_STONECORRIDOR;
class CC_DLL AudioOutputDevice
{
public:
	AudioOutputDevice();
	~AudioOutputDevice();

	static AudioOutputDevice* getInstance();
	void createEngine();
	void createAudioPlayer(unsigned int newSamplerate, int newChannels);
	void play();
	void pause();
	void resume();
	void stop();
	void relaseStream();
	void setVolume(int level);
	void process();
	unsigned char* getOutputAudioBuffer();
	void queueDecodeAudioBuffer(short* buffer);
	static void playCallback(SLAndroidSimpleBufferQueueItf bq, void *context);

private:
    static AudioOutputDevice *mAudioOutputDevice;
    AudioDecodeBufferQueue* decodeBufferQueue;
    SLObjectItf engineObject = NULL;
    SLEngineItf engineEngine = NULL;

    // output mix interfaces
    SLObjectItf outputMixObject = NULL;
    SLEnvironmentalReverbItf outputMixEnvironmentalReverb = NULL;

    // buffer queue player interfaces
    SLObjectItf bqPlayerObject = NULL;
    SLPlayItf bqPlayerPlay = NULL;
    SLAndroidSimpleBufferQueueItf bqPlayerBufferQueue = NULL;
    SLEffectSendItf bqPlayerEffectSend = NULL;
    SLMuteSoloItf bqPlayerMuteSolo = NULL;
    SLVolumeItf bqPlayerVolume = NULL;

    int volume;
    int enqueueCount;
};

NS_CC_END

#endif // CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#endif
