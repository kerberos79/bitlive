package org.cocos2dx.lib;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;


public class Cocos2dxWebViewHelper {
    private static final String TAG = Cocos2dxWebViewHelper.class.getSimpleName();
    private static Handler handler;
    private static Cocos2dxActivity cocos2dxActivity;
    private static FrameLayout layout;

    private static SparseArray<Cocos2dxWebView> webViews;
    private static int viewTag = 0;

    public Cocos2dxWebViewHelper(FrameLayout layout) {
        Cocos2dxWebViewHelper.layout = layout;
        Cocos2dxWebViewHelper.handler = new Handler(Looper.myLooper());

        Cocos2dxWebViewHelper.cocos2dxActivity = (Cocos2dxActivity) Cocos2dxActivity.getContext();
        Cocos2dxWebViewHelper.webViews = new SparseArray<Cocos2dxWebView>();
    }

    private static native boolean shouldStartLoading(int index, String message);

    public static boolean _shouldStartLoading(int index, String message) {
        return !shouldStartLoading(index, message);
    }

    private static native void didFinishLoading(int index, String message);

    public static void _didFinishLoading(int index, String message) {
        didFinishLoading(index, message);
    }

    private static native void didFailLoading(int index, String message);

    public static void _didFailLoading(int index, String message) {
        didFailLoading(index, message);
    }

    private static native void onJsCallback(int index, String message);

    public static void _onJsCallback(int index, String message) {
        onJsCallback(index, message);
    }

    private static native void onSetHomepageCallback(int index, String url);

    public static void _onSetHomepageCallback(int index, String url) {
    	onSetHomepageCallback(index, url);
    }

    private static native void onSendLinkCallback(int index, String url);

    public static void _onSendLinkCallback(int index, String url) {
    	onSendLinkCallback(index, url);
    }
    
    private static native void onCloseCallback(int index);

    public static void _onCloseCallback(int index) {
    	onCloseCallback(index);
    }
    @SuppressWarnings("unused")
    public static int createWebView() {
        final int index = viewTag;
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Cocos2dxWebView webView = new Cocos2dxWebView(cocos2dxActivity, index);
                FrameLayout.LayoutParams lParams = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT);
                layout.addView(webView, lParams);

                webViews.put(index, webView);

                final View child = cocos2dxActivity.getLayoutInflater().inflate(R.layout.popup, null);
                Button homepageButton = (Button)child.findViewById(R.id.HomepageButton);
                homepageButton.setOnClickListener( new View.OnClickListener() {
            		public void onClick(View v) {
            	        cocos2dxActivity.runOnUiThread(new Runnable() {
            	            @Override
            	            public void run() {
            	            	String webUrl = webView.getUrl();
            	                Cocos2dxWebViewHelper._onSetHomepageCallback(index, webUrl);
            	                child.setVisibility(View.INVISIBLE);
            	            }
            	        });
            		}
            	});
                Button urlButton = (Button)child.findViewById(R.id.UrlButton);
                urlButton.setOnClickListener( new View.OnClickListener() {
            		public void onClick(View v) {
            	        cocos2dxActivity.runOnUiThread(new Runnable() {
            	            @Override
            	            public void run() {
            	            	String webUrl = webView.getUrl();
            	                Cocos2dxWebViewHelper._onSendLinkCallback(index, webUrl);
            	                child.setVisibility(View.INVISIBLE);
            	            }
            	        });
            		}
            	});
                Button clipboardButton = (Button)child.findViewById(R.id.ClipboardButton);
                clipboardButton.setOnClickListener( new View.OnClickListener() {
            		public void onClick(View v) {
            	        cocos2dxActivity.runOnUiThread(new Runnable() {
            	            @Override
            	            public void run() {
            	            	String pasteData = "";
            	            	ClipboardManager clipboard = (ClipboardManager) cocos2dxActivity.getSystemService(Context.CLIPBOARD_SERVICE);
            	            	if ((clipboard.hasPrimaryClip())) {
            	            		ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
            	            		pasteData = (String) item.getText().toString();
            	            		if (pasteData != null) {
            	            			if(pasteData.length() < 1380)
            	            			{
	                    	                Cocos2dxWebViewHelper._onSendLinkCallback(index, pasteData);
	                    	                child.setVisibility(View.INVISIBLE);
            	            			} 
            	            			else 
            	            			{
            	            				Log.i("WebView", "String Size Overflow : "+pasteData.length());
                	            		}
            	            		}
            	            	}
            	            }
            	        });
            		}
            	});
                Button closeButton = (Button)child.findViewById(R.id.CloseButton);
                closeButton.setOnClickListener( new View.OnClickListener() {
            		public void onClick(View v) {
            	        cocos2dxActivity.runOnUiThread(new Runnable() {
            	            @Override
            	            public void run() {
            	                Cocos2dxWebViewHelper._onCloseCallback(index);
            	                child.setVisibility(View.INVISIBLE);
            	            }
            	        });
            		}
            	});
                layout.addView(child, lParams);
                webView.setPopupMenu(child);
            }
        });
        return viewTag++;
    }

    @SuppressWarnings("unused")
    public static void removeWebView(final int index) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webViews.remove(index);
                    layout.removeView(webView);
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void setVisible(final int index, final boolean visible) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.setVisibility(visible ? View.VISIBLE : View.GONE); 
                    if(visible)
                    	webView.getPopupMenu().setVisibility(View.VISIBLE);
                    else
                    	webView.getPopupMenu().setVisibility(View.INVISIBLE);
                    webView.getPopupMenu().setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void setWebViewRect(final int index, final int left, final int top, final int maxWidth, final int maxHeight) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.setWebViewRect(left, top, maxWidth, maxHeight);
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void setJavascriptInterfaceScheme(final int index, final String scheme) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.setJavascriptInterfaceScheme(scheme);
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void loadData(final int index, final String data, final String mimeType, final String encoding, final String baseURL) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.loadDataWithBaseURL(baseURL, data, mimeType, encoding, null);
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void loadHTMLString(final int index, final String htmlString, final String mimeType, final String encoding) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.loadData(htmlString, mimeType, encoding);
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void loadUrl(final int index, final String url) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.loadUrl(url);
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void loadFile(final int index, final String filePath) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.loadUrl(filePath);
                }
            }
        });
    }

    public static void stopLoading(final int index) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.stopLoading();
                }
            }
        });

    }

    public static void reload(final int index) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.reload();
                }
            }
        });
    }

    public static <T> T callInMainThread(Callable<T> call) throws ExecutionException, InterruptedException {
        FutureTask<T> task = new FutureTask<T>(call);
        handler.post(task);
        return task.get();
    }

    @SuppressWarnings("unused")
    public static boolean canGoBack(final int index) {
        Callable<Boolean> callable = new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Cocos2dxWebView webView = webViews.get(index);
                return webView != null && webView.canGoBack();
            }
        };
        try {
            return callInMainThread(callable);
        } catch (ExecutionException e) {
            return false;
        } catch (InterruptedException e) {
            return false;
        }
    }

    @SuppressWarnings("unused")
    public static boolean canGoForward(final int index) {
        Callable<Boolean> callable = new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Cocos2dxWebView webView = webViews.get(index);
                return webView != null && webView.canGoForward();
            }
        };
        try {
            return callInMainThread(callable);
        } catch (ExecutionException e) {
            return false;
        } catch (InterruptedException e) {
            return false;
        }
    }

    @SuppressWarnings("unused")
    public static void goBack(final int index) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.goBack();
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void goForward(final int index) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.goForward();
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void evaluateJS(final int index, final String js) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.loadUrl("javascript:" + js);
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void setScalesPageToFit(final int index, final boolean scalesPageToFit) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView != null) {
                    webView.setScalesPageToFit(scalesPageToFit);
                }
            }
        });
    }

    @SuppressWarnings("unused")
    public static void showMenu(final int index, final boolean onoff) {
        cocos2dxActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxWebView webView = webViews.get(index);
                if (webView.getPopupMenu() != null) {
                	if(onoff)
                		webView.getPopupMenu().setVisibility(View.VISIBLE);
                	else
                		webView.getPopupMenu().setVisibility(View.INVISIBLE);
                }
            }
        });
    }
}
