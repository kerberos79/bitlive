package org.cocos2dx.lib;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.cocos2dx.lib.Cocos2dxFaceDetector.BasicFaceDetecor;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusMoveCallback;
import android.hardware.Camera.PreviewCallback;
import android.media.MediaMuxer;
import android.opengl.GLSurfaceView;
import android.util.Log;

public class Cocos2dxCameraPreview implements PreviewCallback{
	private static final String TAG = "CameraPreview";

	public final static int PREVIEW_WIDTH =  640;
	public final static int PREVIEW_HEIGHT =  480;
	public final static int FRAME_DELAY_NORMAL =  1000/20;
	
	public int camID =  Camera.CameraInfo.CAMERA_FACING_FRONT;	
	
    private Camera mCamera;
    boolean created = false;
    int cameraWidth, cameraHeight;
    int previewWidth, previewHeight;
    int minExposure, maxExposure;
    float stepExposure;

	private ByteBuffer[] mConverBuffer;
	private ByteBuffer mPreviewBuffer;
	private ByteBuffer mVideoBuffer = null;
	private int selector;
	private byte[] convertBuffer[];
	private byte[] previewBuffer;
	private int rest_delay;
	private AtomicBoolean isFrontCamera;
	private List<String> focus, colorEffects;
	private Camera.Parameters params;
	private boolean isFlashFlag = false;
    private int faceFrameRotation;
    private Cocos2dxFaceDetector faceDetectorFactory;
    private BasicFaceDetecor faceDetector;
    
    private Cocos2dxVideoEncoder videoEncoder;
	private MediaMuxer muxer;
    
    private int[] mTextureDummy = new int[1];
	private SurfaceTexture mSurfaceTexture = new SurfaceTexture(mTextureDummy[0]);
	private GLSurfaceView mGLSurfaceView;
	public Cocos2dxCameraPreview(final Context pContext, int previewSize) {
		if(previewSize == 480)
		{
	    	cameraWidth = 640;
	    	cameraHeight = 480;
	    	previewWidth = 480;
	    	previewHeight = 640;
		}
		else
		{
	    	cameraWidth = 1280;
	    	cameraHeight = 720;
	    	previewWidth = 1280;
	    	previewHeight = 720;

		}
		mConverBuffer = new ByteBuffer[2];

		mPreviewBuffer = ByteBuffer.allocateDirect(cameraWidth*cameraHeight * 3/2);
		mConverBuffer[0] = ByteBuffer.allocateDirect(previewWidth*previewHeight * 3 / 2);
		mConverBuffer[1] = ByteBuffer.allocateDirect(previewWidth*previewHeight * 3 / 2);
		convertBuffer = new byte[2][];
		previewBuffer = mPreviewBuffer.array();
		convertBuffer[0] = mConverBuffer[0].array();
		convertBuffer[1] = mConverBuffer[1].array();
		isFrontCamera = new AtomicBoolean(true);
		faceFrameRotation = 270;
		selector = 0;

        init();

		faceDetectorFactory = new Cocos2dxFaceDetector(previewWidth, previewHeight);

        faceDetector = faceDetectorFactory.createGmsFaceDetector(pContext);
        if (faceDetector == null)
	    {
        	faceDetector = faceDetectorFactory.createOrigianFaceDetector(pContext);
	    }
	}
	
	public void init() {
    	mCamera = Camera.open(camID);
    	params = mCamera.getParameters();
    	/*
    	List<Size> sizes;
    	sizes = params.getSupportedPreviewSizes();
    	Log.i(TAG, "==Cam supported SizeList of camera ==");
    	for(Size temp:sizes)
    		Log.i(TAG, temp.width+", "+temp.height);
    	List<Integer> format= params.getSupportedPreviewFormats();
    	Log.i(TAG, "==support preview format ");
    	for(int i:format) {
        	Log.i(TAG, "v "+i);
    	}
    	*/

    	if(params.isAutoWhiteBalanceLockSupported())
    		params.setAutoWhiteBalanceLock(false);
    	/*
    	Log.i(TAG, "==getSupportedWhiteBalance ");
    	List<String> whiteBalanace = params.getSupportedWhiteBalance();
    	if(whiteBalanace!=null) {
	    	for(String data:whiteBalanace) {
	        	Log.i(TAG, "w "+data);
	    	}
	    	if(whiteBalanace.contains("auto")) {
	    		params.setWhiteBalance("auto");
	        	Log.i(TAG, "==setSupportedWhiteBalance auto");
	    	}
    	}
    	Log.i(TAG, "==getSupportedFocusModes ");
    	*/
    	focus = params.getSupportedFocusModes();
    	if(focus!=null) {
    		/*
	    	for(String data:focus) {
	        	Log.i(TAG, "f "+data);
	    	}
	    	*/
	    	if(focus.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
	    		params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

				mCamera.setAutoFocusMoveCallback(new AutoFocusMoveCallback() {
					@Override
					public void onAutoFocusMoving(boolean start, Camera camera) {
					}
				});
	        	Log.i(TAG, "==setFocusMode FOCUS_MODE_CONTINUOUS_PICTURE");
	    	}
	    	else if(focus.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
	    		params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
	        	Log.i(TAG, "==setFocusMode FOCUS_MODE_AUTO");
	    	}
    	}
    	/*
    	Log.i(TAG, "==getColorEffects ");
    	List<String> colorEffects = params.getSupportedColorEffects();
    	if(colorEffects!=null) {
	    	for(String data:colorEffects) {
	        	Log.i(TAG, "c "+data);
	    	}
    	}

    	Log.i(TAG, "==getSupportedSceneModes ");
    	*/
    	List<String> scene = params.getSupportedSceneModes();
    	if(scene!=null) {
    		/*
	    	for(String data:scene) {
	        	Log.i(TAG, "s "+data);
	    	}
    		*/
	    	if(scene.contains("auto")) {
	    		params.setSceneMode("auto");
	        	Log.i(TAG, "==setSupportedSceneModes auto");
	    	}
	    	else if(scene.contains("portrait")) {
	    		params.setSceneMode("portrait");
	        	Log.i(TAG, "==setSupportedSceneModes portrait");
	    	}
    	}

    	if(params.isAutoExposureLockSupported())
    		params.setAutoExposureLock(false);
    	minExposure = params.getMinExposureCompensation();
    	maxExposure = params.getMaxExposureCompensation();
    	stepExposure = (float)(maxExposure-minExposure)/100.0f;//params.getExposureCompensationStep();
    	Log.i(TAG, "==getExposureCompensation min="+ minExposure +", max="+ maxExposure
    	    	+", step="+stepExposure);
    	params.setPreviewFormat(ImageFormat.YV12);
    	params.setPreviewSize(cameraWidth, cameraHeight);
    	mCamera.setParameters(params);
    }

    public void startPreview() {
    	if(mCamera!=null) {
			setPreviewCallback(previewBuffer, this);

    	   	mCamera.startPreview();
	    	if(!focus.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)
	    		&&focus.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
		    	mCamera.autoFocus(new Camera.AutoFocusCallback(){
					@Override
					public void onAutoFocus(boolean success, Camera camera) {
					}
		    	});
	    	}

	    	faceDetector.startProcess();
    	}  		
    	
    	Log.i(TAG, "start()");
    }

	protected void stopPreview() {
    	if(mCamera!=null)
    		mCamera.stopPreview();
    }

	public void setPreviewCallback(byte[] callbackBuffer, PreviewCallback callback) {
		try {
			if(mCamera != null) {
//	    		mSurfaceTexture.setOnFrameAvailableListener();
				mCamera.setPreviewTexture(mSurfaceTexture);
				mCamera.addCallbackBuffer(callbackBuffer);
				mCamera.setPreviewCallbackWithBuffer(callback);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setGLSurfaceView(Cocos2dxActivity activity) {

		this.mGLSurfaceView = activity.getGLSurfaceView();
	}
	
	public ByteBuffer getConvertBuffer() {
		return mConverBuffer[selector];
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {

		switch(updatePreviewFrame(getConvertBuffer(), mPreviewBuffer, cameraWidth, cameraHeight, previewWidth, previewHeight, faceFrameRotation)){
		case 0: // update
		{
			break;
		}

		case 1: // release
		{
			release();
			break;
		}

		case 100: // start record
		{
			getVideoFrame(mVideoBuffer, previewWidth, previewHeight);
			if(!videoEncoder.encode(mVideoBuffer))
			{
			}
			break;
		}
		case 101: // stop record
		{
			try{
				muxer.stop();
				videoEncoder.stop();
				muxer.release();
				muxer = null;
				completeRecord(true);
			} catch (Exception e)
			{
				completeRecord(false);
			}
			break;
		}
		}
		faceDetector.update(getConvertBuffer());
		selector = selector^1;
		mCamera.addCallbackBuffer(previewBuffer);
	}
	
	public native int updatePreviewFrame(ByteBuffer output, ByteBuffer input, int src_width, int src_height, int dst_width, int dst_height, int rotation);
	public native void getVideoFrame(ByteBuffer output, int dst_width, int dst_height);
	public native void completeRecord(boolean result);
	public native void onRelease();
	
	public void release() {
		try {
			faceDetector.release();
			if(mCamera==null)
				return;
			mCamera.stopPreview();
			mCamera.release();	
			mCamera = null;
			Cocos2dxHelper.getActivity().finish();
			
			Thread.sleep(300);
			Cocos2dxHelper.terminateProcess();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getcameraWidth() {
		return cameraWidth;
	}

	public int getcameraHeight() {
		return cameraHeight;
	}
	
	public void changeEffect(String effect) {

    	Camera.Parameters params = mCamera.getParameters();
    	params.setColorEffect(effect);
    	mCamera.setParameters(params);
    }
	
	public void changeScene(String scene) {

    	Camera.Parameters params = mCamera.getParameters();

    	params.setSceneMode(scene);
    	mCamera.setParameters(params);
    }
	
	public void changeExposure(int dgree) {

    	Camera.Parameters params = mCamera.getParameters();
    	int value = (int)((stepExposure * dgree) + minExposure);
    	Log.i(TAG, "changeExposure , step="+dgree+", value = "+value);
    	params.setExposureCompensation(value);
    	mCamera.setParameters(params);
    }
	
	public int getExposure() {
    	Camera.Parameters params = mCamera.getParameters();
    	return (stepExposure==0.0f)? 0 : (int)((params.getExposureCompensation() - minExposure )/stepExposure);
	}
	
	public void releaseCamera() {
	}

	public void flipCamera() {
		try {
			mCamera.release();
	
			if(camID == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				camID = Camera.CameraInfo.CAMERA_FACING_BACK;
				faceFrameRotation = 90;
				isFrontCamera.set(false);
			}
			else {
				camID = Camera.CameraInfo.CAMERA_FACING_FRONT;
				faceFrameRotation = 270;
				isFrontCamera.set(true);
			}
			init();
			startPreview();
		} catch(Exception e) {
			e.printStackTrace();
			
		}
	}
	
	public int getDegree() {
		return (camID == Camera.CameraInfo.CAMERA_FACING_FRONT)?0:180;
	}

	public int getCamID() {
		return camID;
	}

	public void setExposure(int value) {
    	int result = minExposure + (int)(value * stepExposure);
    	params.setExposureCompensation(result);
    	mCamera.setParameters(params);
	}

	public int getCurrentExposure() {
    	int exposure = mCamera.getParameters().getExposureCompensation();
    	float value = (float)(exposure - minExposure) / (float)(maxExposure-minExposure) * 100.0f;
    	return (int)value;
	}

	public void setColorEffect(int value) {
    	params.setColorEffect(colorEffects.get(value));
    	mCamera.setParameters(params);
	}

	public List<String> getColorEffect() {
    	return colorEffects;
	}

	public boolean turnFlashOnOff() {

		try {
	    	Camera.Parameters params = mCamera.getParameters();
	
	    	if(isFlashFlag) {
	    		params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
	    	}
	    	else {
	    		params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
	    	}

    		isFlashFlag = !isFlashFlag;
	    	mCamera.setParameters(params);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
    	return isFlashFlag;
    }
	
	public void enableFaceDetect(boolean enable)
	{
		faceDetector.enableFaceDetect(enable);				
	}
	
	public void startEncoding(MediaMuxer muxer, AtomicBoolean readyAudioTrack)
	{
		if(mVideoBuffer==null)
		{
			mVideoBuffer = ByteBuffer.allocateDirect(previewWidth*previewHeight * 3 / 2);
		}
		this.muxer = muxer;
		videoEncoder = new Cocos2dxVideoEncoder(muxer, previewWidth, previewHeight, readyAudioTrack);
	}
}
