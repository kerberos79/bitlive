package org.cocos2dx.lib;

import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.SystemClock;
import android.util.Log;

public class Cocos2dxAudioEncoder {
	private static final String TAG = "Cocos2dxAudioEncoder";

	private MediaCodec mediaCodec;
	private MediaFormat format;
	private MediaMuxer muxer;

	private ByteBuffer[] codecInputBuffers, codecOutputBuffers;
	private int samplingRate, channels;
	private int samplingRateKey;
	private long oldtime = 0, starttime = 0, delta = 10000, newtime = 0, delay = 0;
	private int audioTrackIndex;
	private AtomicBoolean readyAudioTrack;
	
	public Cocos2dxAudioEncoder(MediaMuxer muxer, int samplingRate, int channels, AtomicBoolean readyAudioTrack)
	{
		this.muxer = muxer;
		this.samplingRate = samplingRate;
		this.channels = channels;
		this.readyAudioTrack = readyAudioTrack;
		samplingRateKey = determineSamplingRateKey(samplingRate);
		mediaCodec = MediaCodec.createByCodecName("OMX.google.aac.encoder");
		format = new MediaFormat();
		format.setString(MediaFormat.KEY_MIME, "audio/mp4a-latm");
		format.setInteger(MediaFormat.KEY_AAC_PROFILE,
				MediaCodecInfo.CodecProfileLevel.AACObjectLC); // AAC LC
		format.setInteger(MediaFormat.KEY_SAMPLE_RATE, samplingRate);
		format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, channels);
		format.setInteger(MediaFormat.KEY_BIT_RATE, 64000);

		mediaCodec.configure(format, null /* surface */, null /* crypto */,
				MediaCodec.CONFIGURE_FLAG_ENCODE);
		mediaCodec.start();

		starttime = System.currentTimeMillis() * 1000;
	}

	private int determineSamplingRateKey(int samplingRate)
	{
		switch (samplingRate)
		{
		case 96000:
			return 0;
		case 88200:
			return 1;
		case 64000:
			return 2;
		case 48000:
			return 3;
		case 44100:
			return 4;
		case 32000:
			return 5;
		case 24000:
			return 6;
		case 22050:
			return 7;
		case 16000:
			return 8;
		case 12000:
			return 9;
		case 11025:
			return 10;
		case 8000:
			return 11;
		case 7350:
			return 12;
		default:
			return 4;
		}
	}
	public int getChannels() 
	{
		return channels;
	}
	public int getSamplingRate()
	{
		return samplingRate;
	}
	
	public boolean encode(byte[] input, int size) {
		try {
			ByteBuffer[] inputBuffers = mediaCodec.getInputBuffers();
			ByteBuffer[] outputBuffers = mediaCodec.getOutputBuffers();
			int inputBufferIndex = mediaCodec.dequeueInputBuffer(-1);
			if (inputBufferIndex >= 0) {
				ByteBuffer inputBuffer = inputBuffers[inputBufferIndex];
				inputBuffer.clear();
				synchronized(this) {
				inputBuffer.put(input, 0, size);
				}
				newtime = System.currentTimeMillis() * 1000;
				mediaCodec.queueInputBuffer(inputBufferIndex, 0, size, newtime - starttime, 0);
			}
 
			MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
			int outputBufferIndex = mediaCodec.dequeueOutputBuffer(bufferInfo, 100000);
			if (outputBufferIndex >= 0) {
				ByteBuffer outputBuffer = outputBuffers[outputBufferIndex];

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) == 0) 
                {
					outputBuffer.position(bufferInfo.offset);
					outputBuffer.limit(bufferInfo.offset + bufferInfo.size);
					muxer.writeSampleData(audioTrackIndex, outputBuffer, bufferInfo);
                }
    			mediaCodec.releaseOutputBuffer(outputBufferIndex, false);
			}
			else if (outputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) 
			{
				Log.i(TAG, "INFO_TRY_AGAIN_LATER");
			}
			else if (outputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) 
			{
				outputBuffers = mediaCodec.getOutputBuffers();
			} 
			else if (outputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                MediaFormat newFormat = mediaCodec.getOutputFormat();
                Log.d(TAG, "audio encoder output format changed: " + newFormat);
                audioTrackIndex = muxer.addTrack(newFormat);
                readyAudioTrack.set(true);
            }
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public void stop() {
		try {
			mediaCodec.stop();
			mediaCodec.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
