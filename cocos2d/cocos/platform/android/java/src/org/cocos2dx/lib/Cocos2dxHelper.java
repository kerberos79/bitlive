/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package org.cocos2dx.lib;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import android.app.Activity;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMuxer;
import android.media.MediaMuxer.OutputFormat;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.preference.PreferenceManager.OnActivityResultListener;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class Cocos2dxHelper {
	// ===========================================================
	// Constants
	// ===========================================================
	private static final String PREFS_NAME = "Cocos2dxPrefsFile";
	private static final int RUNNABLES_PER_FRAME = 5;
	private static final int REQEUST_CONTACT = 0x01;

	// ===========================================================
	// Fields
	// ===========================================================

	private static Cocos2dxMusic sCocos2dMusic;
	private static Cocos2dxSound sCocos2dSound;
	private static AssetManager sAssetManager;
	private static Cocos2dxAccelerometer sCocos2dxAccelerometer;
	private static boolean sAccelerometerEnabled;
	private static boolean sActivityVisible;
	private static String sPackageName;
	private static String sFileDirectory;
	private static VoiceRecorder sVoiceRecorder = null;
	private static String country_Number = null;
	private static String sColumnIDString = null;
	private static Uri    sContactUri = null;
	private static Activity sActivity = null;
	private static Cocos2dxHelperListener sCocos2dxHelperListener;
	private static Set<OnActivityResultListener> onActivityResultListeners = new LinkedHashSet<OnActivityResultListener>();


	// ===========================================================
	// Constructors
	// ===========================================================

	public static void runOnGLThread(final Runnable r) {
		((Cocos2dxActivity)sActivity).runOnGLThread(r);
	}

	private static boolean sInited = false;
	public static void init(final Activity activity) {
	    if (!sInited) {
    		final ApplicationInfo applicationInfo = activity.getApplicationInfo();
    		
            Cocos2dxHelper.sCocos2dxHelperListener = (Cocos2dxHelperListener)activity;
                    
    		Cocos2dxHelper.sPackageName = applicationInfo.packageName;
    		Cocos2dxHelper.sFileDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
            Cocos2dxHelper.nativeSetApkPath(applicationInfo.sourceDir);
    
    		Cocos2dxHelper.sCocos2dxAccelerometer = new Cocos2dxAccelerometer(activity);
    		
    		Cocos2dxHelper.sCocos2dMusic = new Cocos2dxMusic(activity);
    		Cocos2dxHelper.sCocos2dSound = new Cocos2dxSound(activity);
    		Cocos2dxHelper.sAssetManager = activity.getAssets();
    		Cocos2dxHelper.nativeSetContext((Context)activity, Cocos2dxHelper.sAssetManager);
    
            Cocos2dxBitmap.setContext(activity);
            sActivity = activity;

            sInited = true;
            
            onActivityResultListeners.add(activityResultListener);
	    }
	}
	
    public static Activity getActivity() {
        return sActivity;
    }
    
    public static void initCameraPreview() {
    }
    
    public static void addOnActivityResultListener(OnActivityResultListener listener) {
        onActivityResultListeners.add(listener);
    }
    
    public static Set<OnActivityResultListener> getOnActivityResultListeners() {
        return onActivityResultListeners;
    }
    
    public static boolean isActivityVisible(){
    	return sActivityVisible;
    }

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	// ===========================================================
	// Methods
	// ===========================================================

	private static native void nativeSetApkPath(final String pApkPath);

	private static native void nativeSetEditTextDialogResult(final byte[] pBytes);

	private static native void nativeSetContext(final Context pContext, final AssetManager pAssetManager);

	public static void startCameraPreview(int previewSize) {
		Cocos2dxGLSurfaceView.getInstance().startCameraPreview(previewSize);
	}
	
	public static void flipCameraPreview() {
		Cocos2dxGLSurfaceView.getInstance().flipCamera();
	}

	public static void setExposureCamera(int value) {
		Cocos2dxGLSurfaceView.getInstance().setExposureCamera(value);
	}

	public static int getExposureCamera() {
		return Cocos2dxGLSurfaceView.getInstance().getExposureCamera();
	}
	
	public static boolean turnCameraFlashOnOff() {
		return Cocos2dxGLSurfaceView.getInstance().turnCameraFlashOnOff();
	}

	public static void enableFaceDetect(boolean enable) {
		Cocos2dxGLSurfaceView.getInstance().enableFaceDetect(enable);
	}
	public static void releaseCamera() {
		Cocos2dxGLSurfaceView.getInstance().releaseCameraPreview();
	}

	public static void createVoiceRecorder(int samplerate, int channels) {
		VoiceRecorder.getInstance(samplerate, channels);
	}

	public static void startVoiceRecorder() {
		VoiceRecorder.getInstance().startEngine();
	}

	public static void pauseVoiceRecorder() {
	}

	public static void resumeVoiceRecorder() {
	}

	public static void stopVoiceRecorder() {
	}

	public static void releaseVoiceRecorder() {
		VoiceRecorder.getInstance().release();
	}

	public static void startVideoRecord(String filename) {
		try {
			MediaMuxer muxer = new MediaMuxer(filename, OutputFormat.MUXER_OUTPUT_MPEG_4);
			AtomicBoolean readyAudioTrack = new AtomicBoolean(false);
			VoiceRecorder.getInstance().startEncoding(muxer, readyAudioTrack);
			
			Cocos2dxGLSurfaceView.getInstance().startVideoEncoding(muxer, readyAudioTrack);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static int getRingerMode() {
		try {
			AudioManager audio = (AudioManager) ((Cocos2dxActivity)sActivity).getSystemService(Context.AUDIO_SERVICE);
			return audio.getRingerMode(); // SILENT : 0, VIBRATOR : 1, NORMAL : 2
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	static long[] pattern = {1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000}; 
	public static void vibrate(boolean onoff)
	{
		try {
			Vibrator vib = (Vibrator) sActivity.getSystemService (Service.VIBRATOR_SERVICE);
			
			if(onoff)
			{
				vib.vibrate (pattern, 29);
			}
			else
			{
				vib.cancel();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getCocos2dxPackageName() {
		return Cocos2dxHelper.sPackageName;
	}

	public static String getCocos2dxWritablePath() {
		return Cocos2dxHelper.sFileDirectory;
	}

	public static String getCocos2dxInternalStoragePath() {
		return sActivity.getFilesDir().getPath();
	}

	public static String getCurrentLanguage() {
		return Locale.getDefault().getLanguage();
	}
	
	public static String getDeviceModel(){
		return Build.MODEL;
    }

	public static AssetManager getAssetManager() {
		return Cocos2dxHelper.sAssetManager;
	}

	public static void enableAccelerometer() {
		Cocos2dxHelper.sAccelerometerEnabled = true;
		Cocos2dxHelper.sCocos2dxAccelerometer.enable();
	}


	public static void setAccelerometerInterval(float interval) {
		Cocos2dxHelper.sCocos2dxAccelerometer.setInterval(interval);
	}

	public static void disableAccelerometer() {
		Cocos2dxHelper.sAccelerometerEnabled = false;
		Cocos2dxHelper.sCocos2dxAccelerometer.disable();
	}
	
	public static void setKeepScreenOn(boolean value) {
		((Cocos2dxActivity)sActivity).setKeepScreenOn(value);
	}

	public static void preloadBackgroundMusic(final String pPath) {
		Cocos2dxHelper.sCocos2dMusic.preloadBackgroundMusic(pPath);
	}

	public static void playBackgroundMusic(final String pPath, final boolean isLoop) {
		Cocos2dxHelper.sCocos2dMusic.playBackgroundMusic(pPath, isLoop);
	}

	public static void resumeBackgroundMusic() {
		Cocos2dxHelper.sCocos2dMusic.resumeBackgroundMusic();
	}

	public static void pauseBackgroundMusic() {
		Cocos2dxHelper.sCocos2dMusic.pauseBackgroundMusic();
	}

	public static void stopBackgroundMusic() {
		Cocos2dxHelper.sCocos2dMusic.stopBackgroundMusic();
	}

	public static void rewindBackgroundMusic() {
		Cocos2dxHelper.sCocos2dMusic.rewindBackgroundMusic();
	}

	public static boolean isBackgroundMusicPlaying() {
		return Cocos2dxHelper.sCocos2dMusic.isBackgroundMusicPlaying();
	}

	public static float getBackgroundMusicVolume() {
		return Cocos2dxHelper.sCocos2dMusic.getBackgroundVolume();
	}

	public static void setBackgroundMusicVolume(final float volume) {
		Cocos2dxHelper.sCocos2dMusic.setBackgroundVolume(volume);
	}

	public static void preloadEffect(final String path) {
		Cocos2dxHelper.sCocos2dSound.preloadEffect(path);
	}

	public static int playEffect(final String path, final boolean isLoop, final float pitch, final float pan, final float gain) {
		return Cocos2dxHelper.sCocos2dSound.playEffect(path, isLoop, pitch, pan, gain);
	}

	public static void resumeEffect(final int soundId) {
		Cocos2dxHelper.sCocos2dSound.resumeEffect(soundId);
	}

	public static void pauseEffect(final int soundId) {
		Cocos2dxHelper.sCocos2dSound.pauseEffect(soundId);
	}

	public static void stopEffect(final int soundId) {
		Cocos2dxHelper.sCocos2dSound.stopEffect(soundId);
	}

	public static float getEffectsVolume() {
		return Cocos2dxHelper.sCocos2dSound.getEffectsVolume();
	}

	public static void setEffectsVolume(final float volume) {
		Cocos2dxHelper.sCocos2dSound.setEffectsVolume(volume);
	}

	public static void unloadEffect(final String path) {
		Cocos2dxHelper.sCocos2dSound.unloadEffect(path);
	}

	public static void pauseAllEffects() {
		Cocos2dxHelper.sCocos2dSound.pauseAllEffects();
	}

	public static void resumeAllEffects() {
		Cocos2dxHelper.sCocos2dSound.resumeAllEffects();
	}

	public static void stopAllEffects() {
		Cocos2dxHelper.sCocos2dSound.stopAllEffects();
	}

	public static void end() {
		Cocos2dxHelper.sCocos2dMusic.end();
		Cocos2dxHelper.sCocos2dSound.end();
	}

	public static void onResume() {
		sActivityVisible = true;
		if (Cocos2dxHelper.sAccelerometerEnabled) {
			Cocos2dxHelper.sCocos2dxAccelerometer.enable();
		}
	}

	public static void onPause() {
		sActivityVisible = false;
		if (Cocos2dxHelper.sAccelerometerEnabled) {
			Cocos2dxHelper.sCocos2dxAccelerometer.disable();
		}
	}

	public static void onEnterBackground() {
		sCocos2dSound.onEnterBackground();
		sCocos2dMusic.onEnterBackground();
	}
	
	public static void onEnterForeground() {
		sCocos2dSound.onEnterForeground();
		sCocos2dMusic.onEnterForeground();
	}
	
	public static void terminateProcess() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	private static void showDialog(final String pTitle, final String pMessage) {
		Cocos2dxHelper.sCocos2dxHelperListener.showDialog(pTitle, pMessage);
	}

	private static void showEditTextDialog(final String pTitle, final String pMessage, final int pInputMode, final int pInputFlag, final int pReturnType, final int pMaxLength) {
		Cocos2dxHelper.sCocos2dxHelperListener.showEditTextDialog(pTitle, pMessage, pInputMode, pInputFlag, pReturnType, pMaxLength);
	}

	public static void setEditTextDialogResult(final String pResult) {
		try {
			final byte[] bytesUTF8 = pResult.getBytes("UTF8");

			Cocos2dxHelper.sCocos2dxHelperListener.runOnGLThread(new Runnable() {
				@Override
				public void run() {
					Cocos2dxHelper.nativeSetEditTextDialogResult(bytesUTF8);
				}
			});
		} catch (UnsupportedEncodingException pUnsupportedEncodingException) {
			/* Nothing. */
		}
	}

    public static int getDPI()
    {
		if (sActivity != null)
		{
			DisplayMetrics metrics = new DisplayMetrics();
			WindowManager wm = sActivity.getWindowManager();
			if (wm != null)
			{
				Display d = wm.getDefaultDisplay();
				if (d != null)
				{
					d.getMetrics(metrics);
					return (int)(metrics.density*160.0f);
				}
			}
		}
		return -1;
    }
    
    // ===========================================================
 	// Functions for CCUserDefault
 	// ===========================================================
    
    public static boolean getBoolForKey(String key, boolean defaultValue) {
    	SharedPreferences settings = sActivity.getSharedPreferences(Cocos2dxHelper.PREFS_NAME, 0);
    	return settings.getBoolean(key, defaultValue);
    }
    
    public static int getIntegerForKey(String key, int defaultValue) {
    	SharedPreferences settings = sActivity.getSharedPreferences(Cocos2dxHelper.PREFS_NAME, 0);
    	return settings.getInt(key, defaultValue);
    }
    
    public static float getFloatForKey(String key, float defaultValue) {
    	SharedPreferences settings = sActivity.getSharedPreferences(Cocos2dxHelper.PREFS_NAME, 0);
    	return settings.getFloat(key, defaultValue);
    }
    
    public static double getDoubleForKey(String key, double defaultValue) {
    	// SharedPreferences doesn't support saving double value
    	SharedPreferences settings = sActivity.getSharedPreferences(Cocos2dxHelper.PREFS_NAME, 0);
    	return settings.getFloat(key, (float)defaultValue);
    }
    
    public static String getStringForKey(String key, String defaultValue) {
    	SharedPreferences settings = sActivity.getSharedPreferences(Cocos2dxHelper.PREFS_NAME, 0);
    	return settings.getString(key, defaultValue);
    }
    
    public static void setBoolForKey(String key, boolean value) {
    	SharedPreferences settings = sActivity.getSharedPreferences(Cocos2dxHelper.PREFS_NAME, 0);
    	SharedPreferences.Editor editor = settings.edit();
    	editor.putBoolean(key, value);
    	editor.commit();
    }
    
    public static void setIntegerForKey(String key, int value) {
    	SharedPreferences settings = sActivity.getSharedPreferences(Cocos2dxHelper.PREFS_NAME, 0);
    	SharedPreferences.Editor editor = settings.edit();
    	editor.putInt(key, value);
    	editor.commit();
    }
    
    public static void setFloatForKey(String key, float value) {
    	SharedPreferences settings = sActivity.getSharedPreferences(Cocos2dxHelper.PREFS_NAME, 0);
    	SharedPreferences.Editor editor = settings.edit();
    	editor.putFloat(key, value);
    	editor.commit();
    }
    
    public static void setDoubleForKey(String key, double value) {
    	// SharedPreferences doesn't support recording double value
    	SharedPreferences settings = sActivity.getSharedPreferences(Cocos2dxHelper.PREFS_NAME, 0);
    	SharedPreferences.Editor editor = settings.edit();
    	editor.putFloat(key, (float)value);
    	editor.commit();
    }
    
    public static void setStringForKey(String key, String value) {
    	SharedPreferences settings = sActivity.getSharedPreferences(Cocos2dxHelper.PREFS_NAME, 0);
    	SharedPreferences.Editor editor = settings.edit();
    	editor.putString(key, value);
    	editor.commit();
    }
	
	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	public static interface Cocos2dxHelperListener {
		public void showDialog(final String pTitle, final String pMessage);
		public void showEditTextDialog(final String pTitle, final String pMessage, final int pInputMode, final int pInputFlag, final int pReturnType, final int pMaxLength);

		public void runOnGLThread(final Runnable pRunnable);
	}

    public static MediaScannerConnection _conn;
    public static File _file;

	public static String getExternalStorageDirectory() {
		return Environment.getExternalStorageDirectory().getAbsolutePath()+"/Pictures/";
	}
	
	public static void scanMediaNewFile(final String filename) {

		new Thread(new Runnable() {

			@Override
			public void run() {
				_file = new File(filename);
				_conn  = new MediaScannerConnection( getActivity() ,
					     new MediaScannerConnection.MediaScannerConnectionClient() {
							@Override
					         public void onScanCompleted(String path, Uri uri) {

								Log.i("Cocos3dxHelper", "onScanCompleted : "+path);
					         }
							@Override
					         public void onMediaScannerConnected() {
								_conn.scanFile( _file.getAbsolutePath(), null);

					         }
					     });
				_conn.connect();
			}
			
		}).start();
	}
	
	public static void sendActionShareImage(final String filename) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("image/jpg");
		i.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+filename));
		getActivity().startActivity(Intent.createChooser(i, "Share Image"));
	}
	
	public static void sendActionShareMovie(final String filename) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("video/mp4");
		i.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+filename));
		getActivity().startActivity(Intent.createChooser(i, "Share Image"));
	}

    // ===========================================================
 	// Functions for Request Contract
 	// ===========================================================
	public static void requestContactList() {
		Intent intent = new Intent (Intent.ACTION_PICK);
		intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
		sActivity.startActivityForResult(intent,REQEUST_CONTACT);
	}
	
	public static void searchContact(final String key) {

		sActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {

				String number = key;
				String mName = "";
				byte[] photo = null;
				
				if(key.endsWith("@gmail.com")) {
					sContactUri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI, Uri.encode(number));
				}
				else
				{
					sContactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
				}
				ContentResolver mContentResolver = sActivity.getContentResolver();
				Cursor mContactCursor = mContentResolver.query(sContactUri,
						new String[]{BaseColumns._ID, ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.Contacts.Photo.PHOTO_ID}, 
						null, 
						null, 
						null);
				try {
					if( mContactCursor != null && mContactCursor.getCount()>0 ) {
						mContactCursor.moveToNext();
						sColumnIDString = mContactCursor.getString(mContactCursor.getColumnIndex(BaseColumns._ID));
						mName = mContactCursor.getString(mContactCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
						String photoId = mContactCursor.getString(mContactCursor.getColumnIndex(ContactsContract.Contacts.Photo.PHOTO_ID));

		                if(photoId!=null)
		                {	
		                	Cursor c = getActivity().getContentResolver().query(ContactsContract.Data.CONTENT_URI, 
		                				new String[] {
		                				ContactsContract.CommonDataKinds.Photo.PHOTO}, 
		                				ContactsContract.Data._ID + "=?", 
		                				new String[] { photoId }, 
		                				null);
		                    byte[] imageBytes = null;
		                    if (c != null) {
		                        if (c.moveToFirst()) {
		                            imageBytes = c.getBlob(0);
		                        }
		                        c.close();
		                    }
		                    if (imageBytes != null) {
		                        Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
		            			ByteArrayOutputStream baos = new ByteArrayOutputStream();

		            			bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
		            			photo = baos.toByteArray();
		            			baos.close();
		            		}
		                }
					}
					else {
						mName = number;
					}
					if(photo!=null)
						nativeSearchContactResult(mName, photo, photo.length);
					else
						nativeSearchContactResult(mName, null, 0);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				finally {
					if(mContactCursor!=null)
						mContactCursor.close();
				}
			}
		});
	}
	
	public static byte[] searchPhoto()
	{
		Bitmap photo = null;
		byte[] raw_data = null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getActivity().getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(sColumnIDString)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
            }
    		if(photo!=null)
    		{
    			ByteArrayOutputStream baos = new ByteArrayOutputStream();

    			photo.compress(Bitmap.CompressFormat.PNG, 100, baos);
    			raw_data = baos.toByteArray();
    			baos.close();
    		}
        } catch (Exception e) {
            e.printStackTrace();
            raw_data = null;
        }
		
		return  raw_data;
	}
	
	public static OnActivityResultListener activityResultListener = new OnActivityResultListener() {

		@Override
		public boolean onActivityResult(int requestCode,
				int resultCode, Intent data) {
			try{
				switch(requestCode) {
				case REQEUST_CONTACT:
				{
					if(data!=null)
					{
						List<String> resultList = contactPick(data.getData());
						Bitmap photo = getContactPhoto();
						if(photo!=null)
						{
							byte[] raw_data = null;
							ByteArrayOutputStream baos = new ByteArrayOutputStream();

							photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
							raw_data = baos.toByteArray();
							nativeResultContactList(resultList, raw_data, raw_data.length);
							baos.close();
						}
						else
						{
							nativeResultContactList(resultList, null, 0);
						}
					}
					break;
				}
				default:
					break;
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			return false;
		}
    	
    };
	public static native void nativeResultContactList(List<String> resultList, byte[] raw_data, int length);
	public static native void nativeSearchContactResult(String name, byte[] raw_data, int length);

	public static List<String> contactPick(Uri dataUri) {
		Cursor cursor = getActivity().managedQuery(dataUri, null, null, null, null);
		String people_Number = null, emailid = null, account_Name = null;

		List<String> accountList = new ArrayList<String>();
		while(cursor.moveToNext()) {
			int columnID = cursor.getColumnIndex(ContactsContract.Contacts._ID);
			sColumnIDString = cursor.getString(columnID);
			account_Name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
			accountList.add(account_Name);
			String hasPhoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
			if(hasPhoneNumber.equalsIgnoreCase("1")) {
				hasPhoneNumber = "true";
			}
			else {
				hasPhoneNumber = "false";
			}
			if(Boolean.parseBoolean(hasPhoneNumber)) {
				Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID+" = "+sColumnIDString, null, null);
				while(phones.moveToNext()) {
					people_Number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					accountList.add(people_Number);
				}
				phones.close();
			}
			
			Cursor email = getActivity().getContentResolver().query( 
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{sColumnIDString}, null); 

			while (email.moveToNext()) { 
	            emailid = email.getString(email.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
				accountList.add(emailid);
			}
			email.close();
			
		}
		return accountList;
	}
	
	public static Bitmap getContactPhoto()
	{
		Bitmap photo = null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getActivity().getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(sColumnIDString)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return photo;
	}
	
	public InputStream openDisplayPhoto(long contactId) {
	     Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId);
	     Uri displayPhotoUri = Uri.withAppendedPath(contactUri, Contacts.Photo.DISPLAY_PHOTO);
	     try {
	         AssetFileDescriptor fd =
	        		 getActivity().getContentResolver().openAssetFileDescriptor(displayPhotoUri, "r");
	         return fd.createInputStream();
	     } catch (IOException e) {
	         return null;
	     }
	 }

	public static String getMyIPAddress() {

    	return NetworkUtility.getLocalIpAddress();
	}
	public static String getPhoneNumber() {

    	return NetworkUtility.getPhoneNumber(sActivity);
	}
	public static String getNetworkType() {

    	return NetworkUtility.getNetworkType(sActivity);
	}
	public static String getNetworkProvider() {

    	return NetworkUtility.getNetworkProvider(sActivity);
	}
	public static String getCountryCode() {

    	return NetworkUtility.getSimCountryNumber(sActivity);
	}

	private static String project_number = "1067915222659";
	public static String getRegisterID() {
		String id = null;
		try {
			id = GoogleCloudMessaging.getInstance(sActivity).register(project_number);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}
	public static int getDeviceLevel() {
		return (Build.VERSION.SDK_INT > 20)? 1 : 0;
	}
}
