package org.cocos2dx.lib;


import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.PointF;
import android.graphics.Rect;
import android.media.FaceDetector;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;
public class Cocos2dxFaceDetector {
	private static final String TAG = "Cocos2dxFaceDetector";
    private BasicFaceDetecor basicFaceDetecor;
    int previewWidth, previewHeight;
    
    public Cocos2dxFaceDetector(int width, int heigth)
    {  	
    	previewWidth = width;
    	previewHeight = heigth;
    }
    
    public BasicFaceDetecor createGmsFaceDetector(final Context pContext)
    {
    	BasicFaceDetecor gmsFaceDetecor = new GmsFaceDetectThread();
    	if(gmsFaceDetecor.init(pContext))
    	{
    		return gmsFaceDetecor;
    	}
    	
    	return null;
    }
    
    public BasicFaceDetecor createOrigianFaceDetector(final Context pContext)
    {
    	BasicFaceDetecor basicFaceDetecor = new OriginFaceDetectThread();
    	if(basicFaceDetecor.init(pContext))
    	{
    		return basicFaceDetecor;
    	}
    	return null;
    }	
    
    public native void updateFaceDetect(float leftX, float leftY, float rightX, float rightY);
    private native int convertI420ToABGR(int[] output, ByteBuffer input, int width, int height);
    
    public interface BasicFaceDetecor
    {
    	public boolean init(final Context pContext);
		public void startProcess();
		public void resumeProcess();
		public void release();
		public void enableFaceDetect(boolean enable);
		public void update(ByteBuffer imageBuffer);
    }
    
	public class GmsFaceDetectThread extends Thread implements BasicFaceDetecor{

	    private com.google.android.gms.vision.face.FaceDetector mFaceDetector;
		private com.google.android.gms.vision.Frame.Builder  builder = new com.google.android.gms.vision.Frame.Builder();
		private com.google.android.gms.vision.Frame faceFrame;
	    private float leftX = 0, leftY = 0, rightX = 0, rightY = 0;
		private AtomicBoolean readyToFaceDetect = new AtomicBoolean(false);
		private AtomicBoolean enableFaceDetect = new AtomicBoolean(false);
		private AtomicBoolean isFrontCamera = new AtomicBoolean(true);
		public boolean init(final Context pContext)
		{
	        mFaceDetector = new com.google.android.gms.vision.face.FaceDetector.Builder(pContext)
			.setLandmarkType(com.google.android.gms.vision.face.FaceDetector.ALL_LANDMARKS)
			.setProminentFaceOnly(true)
			.setTrackingEnabled(false)
			.build();

	        if (!mFaceDetector.isOperational()) {
	        	Log.i(TAG, "NOT supported");
	        	
	        	return false;
	        }
	        
	        return true;
		}
		public void startProcess()
		{
			start();
		}

		public void resumeProcess()
		{
			synchronized(this)
			{
				notify();
			}
		}
		public void release()
		{
			this.interrupt();
		}
		
		public void enableFaceDetect(boolean enable)
		{
			if(enable)
			{
				if(!enableFaceDetect.get())
				{
					enableFaceDetect.set(true);
					readyToFaceDetect.set(true);
				}
			}
			else
			{
				if(enableFaceDetect.get())
				{
					enableFaceDetect.set(false);				
				}			
			}				
		}

	    public void update(ByteBuffer imageBuffer)
	    {
			if(readyToFaceDetect.get())
			{
				readyToFaceDetect.set(false);
				faceFrame = builder.setImageData(imageBuffer, previewWidth, previewHeight, ImageFormat.YV12).build();
				resumeProcess();
			}
	    }
		
		@Override
		public void run() {
			try {
			while(true){
				synchronized(this)
				{
					wait();
				}
				SparseArray<Face> faces = mFaceDetector.detect(faceFrame);
	
				if(faces.size()>0)
				{
		            Face thisFace = faces.valueAt(0);
		            for (Landmark landmark : thisFace.getLandmarks()) {
		            	if(landmark.getType() == Landmark.LEFT_EYE)
		            	{
		            		leftX = landmark.getPosition().x;
		            		leftY = previewHeight - landmark.getPosition().y;
		            	}
		            	else if(landmark.getType() == Landmark.RIGHT_EYE)
		            	{
		            		rightX = landmark.getPosition().x;
		            		rightY = previewHeight - landmark.getPosition().y;
		            	}
	            	}	
		            updateFaceDetect(leftX, leftY, rightX, rightY);
				}
				if(enableFaceDetect.get())
					readyToFaceDetect.set(true);
            }
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				mFaceDetector.release();
			}
		}
	};

	public class OriginFaceDetectThread extends Thread implements BasicFaceDetecor{
	    private Rect faceRect = new Rect();
		private android.media.FaceDetector mFaceDetect;
		private android.media.FaceDetector.Face[] mFace;
		int[] bitmapBytes;
		Bitmap bitmap;
		PointF midEyes = new PointF();
		float distance;
		private AtomicBoolean readyToFaceDetect = new AtomicBoolean(false);
		private AtomicBoolean enableFaceDetect = new AtomicBoolean(false);
		private AtomicBoolean isFrontCamera = new AtomicBoolean(true);
	    private float leftX = 0, leftY = 0, rightX = 0, rightY = 0;
		public boolean init(final Context pContext)
		{
			bitmapBytes = new int[previewWidth * previewHeight];
			bitmap = Bitmap.createBitmap(previewWidth, previewHeight, Bitmap.Config.RGB_565);
			mFace = new android.media.FaceDetector.Face[1];
			mFaceDetect = new FaceDetector(previewWidth, previewHeight,1);
	        return true;
		}

		public void startProcess()
		{
			start();
		}
		
		public void resumeProcess()
		{
			synchronized(this)
			{
				notify();
			}
		}
		public void release()
		{
			this.interrupt();
		}

		public void enableFaceDetect(boolean enable)
		{
			if(enable)
			{
				if(!enableFaceDetect.get())
				{
					enableFaceDetect.set(true);
					readyToFaceDetect.set(true);
				}
			}
			else
			{
				if(enableFaceDetect.get())
				{
					enableFaceDetect.set(false);				
				}			
			}				
		}

	    public void update(ByteBuffer imageBuffer)
	    {
			if(readyToFaceDetect.get())
			{
				readyToFaceDetect.set(false);
				convertI420ToABGR(bitmapBytes, imageBuffer, previewWidth, previewHeight);
				
				resumeProcess();
			}
	    }
		
		@Override
		public void run() {
			try {
			while(true){

				synchronized(this)
				{
					wait();
				}
				bitmap.setPixels(bitmapBytes, 0, previewWidth, 0, 0, previewWidth, previewHeight);
				int numOfFace=mFaceDetect.findFaces(bitmap, mFace);
				if(numOfFace>0)
				{
					mFace[0].getMidPoint(midEyes);
					distance = mFace[0].eyesDistance()/2;
            		leftX = midEyes.x + distance;
            		rightX = midEyes.x - distance;
            		leftY = rightY = previewHeight - midEyes.y;
					updateFaceDetect(leftX, leftY, rightX, rightY);
				}
				
				if(enableFaceDetect.get())
					readyToFaceDetect.set(true);
            }
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
			}
		}
	};
    
}
