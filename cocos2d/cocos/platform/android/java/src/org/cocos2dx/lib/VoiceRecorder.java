package org.cocos2dx.lib;

import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaMuxer;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.AudioEffect;
import android.util.Log;


public class VoiceRecorder extends Thread {
	private final static String TAG = "StreamVoiceEncoder";
	public final static int BITRATE = 128000;
	private final static int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
	private final static int OPUS_FRAME_SIZE = 960;
	private final static int OPUS_FRAME_BYTESIZE = 960 * 2;

	private final int bufferSize = 2048;
	private AudioRecord audioRecorder = null;
	private AcousticEchoCanceler canceler = null;
	private ByteBuffer pcmBuffer = ByteBuffer.allocateDirect(bufferSize<<1);
	private MediaMuxer muxer;
    private Cocos2dxAudioEncoder audioEncoder;

	private boolean isStarted = false;
	private boolean isQuit = false;
	
	private static VoiceRecorder sVoiceRecorder = null;
	private static int sSamplerate;
	private static int sChannels;
	
	public static VoiceRecorder getInstance() {
		if(sVoiceRecorder == null)
		{
			sVoiceRecorder = new VoiceRecorder();
		}
		return sVoiceRecorder;
	}

	public static VoiceRecorder getInstance(int samplerate, int channels) {
		if(sVoiceRecorder == null)
		{
			sVoiceRecorder = new VoiceRecorder(samplerate, channels);
		}
		return sVoiceRecorder;
	}

	public VoiceRecorder() {
		sSamplerate = 16000;
		sChannels = 1;
		audioRecorder = new AudioRecord(MediaRecorder.AudioSource.VOICE_COMMUNICATION, 
										sSamplerate, 
										(sChannels==1)?AudioFormat.CHANNEL_IN_MONO : AudioFormat.CHANNEL_IN_STEREO, 
										RECORDER_AUDIO_ENCODING,
										bufferSize);
		
		if (AcousticEchoCanceler.isAvailable()) {
			canceler =  AcousticEchoCanceler.create(audioRecorder.getAudioSessionId());
	        int ret = canceler.setEnabled(true);
	        if (ret != AudioEffect.SUCCESS) {
	            Log.e(TAG, "setEnabled error: " + ret);
	        }
		}
		
	}

	public VoiceRecorder(int samplerate, int channels) {
		sSamplerate = samplerate;
		sChannels = channels;
		audioRecorder = new AudioRecord(MediaRecorder.AudioSource.VOICE_COMMUNICATION, 
										samplerate, 
										(channels==1)?AudioFormat.CHANNEL_IN_MONO : AudioFormat.CHANNEL_IN_STEREO, 
										RECORDER_AUDIO_ENCODING,
										bufferSize);
		
		if (AcousticEchoCanceler.isAvailable()) {
			canceler =  AcousticEchoCanceler.create(audioRecorder.getAudioSessionId());
	        int ret = canceler.setEnabled(true);
	        if (ret != AudioEffect.SUCCESS) {
	            Log.e(TAG, "setEnabled error: " + ret);
	        }
		}
		
	}

	@Override
	public void run() {
		Log.i(TAG, "start VoiceEncoder");
		int result;
		try {
			if(audioRecorder!=null)
				audioRecorder.startRecording();
			while(true) {
				if( audioRecorder.read(pcmBuffer.array(),  0, OPUS_FRAME_BYTESIZE)>0 ){
					result = updatePCM(pcmBuffer);
					switch(result)
					{
					case 1: // stop
					{
						throw new Exception();
					}
					case 100: // start record
					{
						mixPCM(pcmBuffer, OPUS_FRAME_SIZE);
						audioEncoder.encode(pcmBuffer.array(), OPUS_FRAME_BYTESIZE);
						break;
					}
					case 101: // stop record
					{
						audioEncoder.stop();
						audioEncoder = null;
						throw new Exception();
					}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			audioRecorder.stop();
			release();
			sVoiceRecorder = null;
			Log.i(TAG, "release VoiceEncoder");
		}
	}

	public void startEngine() {
		Log.i("test", "startEngine");
		start();
	}

	public void pauseEngine() {
	}

	public void resumeEngine() {
	}

	public void stopEngine() {
		if(audioRecorder!=null)
		{
			audioRecorder.stop();
		}
	}

	public void release() {
		if(canceler!=null)
		{
			canceler.release();
			canceler = null;
		}
		audioRecorder.release();
	}

	public void startEncoding(MediaMuxer muxer, AtomicBoolean readyAudioTrack)
	{
		this.muxer = muxer;
		audioEncoder = new Cocos2dxAudioEncoder(muxer, sSamplerate, sChannels, readyAudioTrack);
	}
	
	private native int updatePCM(ByteBuffer pcm);
	private native void mixPCM(ByteBuffer pcm, int size);
}
