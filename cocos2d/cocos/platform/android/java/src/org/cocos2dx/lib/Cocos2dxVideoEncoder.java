package org.cocos2dx.lib;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecInfo.CodecCapabilities;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.SystemClock;
import android.util.Log;

public class Cocos2dxVideoEncoder {
	private final static String TAG = "MediaEncoder";
	private static final int COLOR_FORMAT21 = MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar;
	private MediaCodec mediaCodec;
	private MediaMuxer muxer;
	private int src_width, src_height;
	private long oldtime = 0, starttime = 0, delta = 10000, newtime = 0, delay = 0;
	private byte[] sps = new byte[12];
	private byte[] pps;
	private int videoTrackIndex;
	private AtomicBoolean readyAudioTrack;
	
	Cocos2dxVideoEncoder(MediaMuxer muxer, int width, int height, AtomicBoolean readyAudioTrack)
	{
		this.muxer = muxer;
		src_width = width;
		src_height = height;
		this.readyAudioTrack = readyAudioTrack;
		mediaCodec = MediaCodec.createEncoderByType("video/avc");

		MediaFormat mediaFormat = MediaFormat.createVideoFormat("video/avc", width, height);

//		getCapabilities();
		mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, 1200000);
		mediaFormat.setInteger(MediaFormat.KEY_FRAME_RATE, 20);
		mediaFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar);
		mediaFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 5 );
		mediaFormat.setInteger(MediaFormat.KEY_WIDTH, src_width);
		mediaFormat.setInteger(MediaFormat.KEY_HEIGHT, src_height);
		mediaCodec.configure(mediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
		mediaCodec.start();
		starttime = System.currentTimeMillis() * 1000;
	}

    public void getCapabilities() {
        Log.d(TAG, "testGetCapabilities");

        int codecCount = MediaCodecList.getCodecCount();
        for (int i = 0; i < codecCount; ++i) {
            MediaCodecInfo info = MediaCodecList.getCodecInfoAt(i);

            Log.d(TAG, (i + 1) + ": " + info.getName());
            Log.d(TAG, "  isEncoder = " + info.isEncoder());

            if (!info.getName().startsWith("OMX.")) {
                // Unfortunately for legacy reasons, "AACEncoder", a
                // non OMX component had to be in this list for the video
                // editor code to work... but it cannot actually be instantiated
                // using MediaCodec.
                Log.d(TAG, "  skipping...");
                continue;
            }

            String[] types = info.getSupportedTypes();
            for (int j = 0; j < types.length; ++j) {
                Log.d(TAG, "calling getCapabilitiesForType " + types[j]);
                CodecCapabilities capabilities = info.getCapabilitiesForType(types[j]);
            	for (int k = 0; k < capabilities.colorFormats.length; k++) {
            		int colorFormat = capabilities.colorFormats[k];
            		if (isRecognizedFormat(colorFormat)) {
            			Log.d("ColorFormat", "Find a good color format for " + info.getName()+" : "+colorFormat);
            		}
            	}
            }
        }
    }
    private boolean isRecognizedFormat(int colorFormat) {
    	switch (colorFormat) {
    	// these are the formats we know how to handle for this test
    	case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
    	case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedPlanar:
    	case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
    	case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedSemiPlanar:
    	case MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar:
    		return true;
    	default:
    		return false;
    	}
    }

	public boolean encode(ByteBuffer yuv) {
		try {
			ByteBuffer[] inputBuffers = mediaCodec.getInputBuffers();
			ByteBuffer[] outputBuffers = mediaCodec.getOutputBuffers();
			int inputBufferIndex = mediaCodec.dequeueInputBuffer(-1);
			if (inputBufferIndex >= 0) {
				ByteBuffer inputBuffer = inputBuffers[inputBufferIndex];
				inputBuffer.clear();
				yuv.position(0);
				yuv.limit(yuv.capacity());
				synchronized(this) {
				inputBuffer.put(yuv);
				}
				newtime = System.currentTimeMillis() * 1000;
				mediaCodec.queueInputBuffer(inputBufferIndex, 0, yuv.capacity(), newtime - starttime, 0);
			}
 
			MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
			int outputBufferIndex = mediaCodec.dequeueOutputBuffer(bufferInfo, 10000);
			if (outputBufferIndex >= 0) {
				ByteBuffer outputBuffer = outputBuffers[outputBufferIndex];

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) == 0) 
                {
					outputBuffer.position(bufferInfo.offset);
					outputBuffer.limit(bufferInfo.offset + bufferInfo.size);
					muxer.writeSampleData(videoTrackIndex, outputBuffer, bufferInfo);
                }
    			mediaCodec.releaseOutputBuffer(outputBufferIndex, false);
			}
			else if (outputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) 
			{
				Log.i(TAG, "INFO_TRY_AGAIN_LATER");
			}
			else if (outputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) 
			{
				outputBuffers = mediaCodec.getOutputBuffers();
			} 
			else if (outputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                MediaFormat newFormat = mediaCodec.getOutputFormat();
                Log.d(TAG, "video encoder output format changed: " + newFormat);
                videoTrackIndex = muxer.addTrack(newFormat);
                
                while(readyAudioTrack.get()==false)
                {
                	
                }
                muxer.start();
            }
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public void stop() {
		try {
			mediaCodec.stop();
			mediaCodec.release();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void close() throws IOException {
		mediaCodec.release();
	}

}
