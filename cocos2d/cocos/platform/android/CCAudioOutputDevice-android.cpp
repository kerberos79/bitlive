#include "platform/CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "jni/JniHelper.h"
#include "jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#include "CCAudioOutputDevice-android.h"
#include "base/CCDirector.h"
#include <android/log.h>
#include <jni.h>
#include <cstring>

NS_CC_BEGIN

AudioOutputDevice* AudioOutputDevice::mAudioOutputDevice = nullptr;

AudioOutputDevice* AudioOutputDevice::getInstance()
{
	if(mAudioOutputDevice==nullptr)
	{
		mAudioOutputDevice = new AudioOutputDevice();
		mAudioOutputDevice->createEngine();
	}

	return mAudioOutputDevice;
}

AudioOutputDevice::AudioOutputDevice()
:decodeBufferQueue(nullptr),
 enqueueCount(0)
{
}

AudioOutputDevice::~AudioOutputDevice()
{
	if(decodeBufferQueue)
		delete decodeBufferQueue;
}

void AudioOutputDevice::createEngine()
{
    SLresult result;

    // create engine
    result = slCreateEngine(&engineObject, 0, NULL, 0, NULL, NULL);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the engine
    result = (*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the engine interface, which is needed in order to create other objects
    result = (*engineObject)->GetInterface(engineObject, SL_IID_ENGINE, &engineEngine);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // create output mix, with environmental reverb specified as a non-required interface
    const SLInterfaceID ids[1] = {SL_IID_ENVIRONMENTALREVERB};
    const SLboolean req[1] = {SL_BOOLEAN_FALSE};
    result = (*engineEngine)->CreateOutputMix(engineEngine, &outputMixObject, 1, ids, req);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the output mix
    result = (*outputMixObject)->Realize(outputMixObject, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the environmental reverb interface
    // this could fail if the environmental reverb effect is not available,
    // either because the feature is not present, excessive CPU load, or
    // the required MODIFY_AUDIO_SETTINGS permission was not requested and granted
    result = (*outputMixObject)->GetInterface(outputMixObject, SL_IID_ENVIRONMENTALREVERB,
            &outputMixEnvironmentalReverb);
    if (SL_RESULT_SUCCESS == result) {
        result = (*outputMixEnvironmentalReverb)->SetEnvironmentalReverbProperties(
                outputMixEnvironmentalReverb, &reverbSettings);
        (void)result;
    }
}

void AudioOutputDevice::createAudioPlayer(unsigned int newSamplerate, int newChannels)
{
    SLresult result;
    SLuint32 channelMask;
    if(newChannels==1) {
    	channelMask = SL_SPEAKER_FRONT_CENTER;
    }
    else if(newChannels==2) {
    	channelMask = SL_SPEAKER_FRONT_LEFT|SL_SPEAKER_FRONT_RIGHT;
    }

    // configure audio source
    SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
    SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, (unsigned int)newChannels, (SLuint32)newSamplerate*1000/*SL_SAMPLINGRATE_44_1*/,
        SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
        channelMask, SL_BYTEORDER_LITTLEENDIAN};
    SLDataSource audioSrc = {&loc_bufq, &format_pcm};

    // configure audio sink
    SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, outputMixObject};
    SLDataSink audioSnk = {&loc_outmix, NULL};

    // create audio player
    const SLInterfaceID ids[3] = {SL_IID_BUFFERQUEUE, SL_IID_EFFECTSEND,
            /*SL_IID_MUTESOLO,*/ SL_IID_VOLUME};
    const SLboolean req[3] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE,
            /*SL_BOOLEAN_TRUE,*/ SL_BOOLEAN_TRUE};
    result = (*engineEngine)->CreateAudioPlayer(engineEngine, &bqPlayerObject, &audioSrc, &audioSnk,
            3, ids, req);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the player
    result = (*bqPlayerObject)->Realize(bqPlayerObject, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the play interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_PLAY, &bqPlayerPlay);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the buffer queue interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_BUFFERQUEUE,
            &bqPlayerBufferQueue);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

	// register callback on the buffer queue
	result = (*bqPlayerBufferQueue)->RegisterCallback(bqPlayerBufferQueue, playCallback, this);
	assert(SL_RESULT_SUCCESS == result);
	(void)result;

    // get the effect send interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_EFFECTSEND,
            &bqPlayerEffectSend);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the volume interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_VOLUME, &bqPlayerVolume);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

	decodeBufferQueue = new AudioDecodeBufferQueue(newSamplerate*0.06*2);
}

void AudioOutputDevice::play() {
	decodeBufferQueue->initBufferList();
    SLresult result;
    result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
}

void AudioOutputDevice::pause() {
    SLresult result;
    result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PAUSED);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
}

void AudioOutputDevice::resume() {
    SLresult result;
    result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
}

void AudioOutputDevice::stop() {
    SLresult result;
    result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PAUSED);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

	if(decodeBufferQueue)
		decodeBufferQueue->initBufferList();
	enqueueCount = 0;

}

void AudioOutputDevice::relaseStream() {
    SLresult result;
    result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_STOPPED);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    result = (*bqPlayerBufferQueue)->Clear(bqPlayerBufferQueue);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

}

void AudioOutputDevice::setVolume(int level) {
	volume = -1 * (level - 100) * (level - 100) /2;
    SLresult result;
    result = (*bqPlayerVolume)->SetVolumeLevel(bqPlayerVolume, volume);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
}

unsigned char* AudioOutputDevice::getOutputAudioBuffer()
{
	return decodeBufferQueue->getReadyBuffer();
}

void AudioOutputDevice::queueDecodeAudioBuffer(short* buffer)
{
	if(enqueueCount<2)
	{
		SLresult result;
		result = (*bqPlayerBufferQueue)->Enqueue(bqPlayerBufferQueue, buffer, 1920);
		decodeBufferQueue->pushReadyBuffer((unsigned char*)buffer);
		enqueueCount++;
	}
	else
	{
		decodeBufferQueue->pushDecodeBuffer((unsigned char *)buffer);
	}
}

// this callback handler is called every time a buffer finishes playing
void AudioOutputDevice::playCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
    AudioOutputDevice* audioOutputDevice = (AudioOutputDevice*)context;
    unsigned char* pcm = audioOutputDevice->decodeBufferQueue->getDecodeBuffer();
	if(pcm)
	{
		(*bq)->Enqueue(bq, pcm, 1920);
		audioOutputDevice->decodeBufferQueue->pushReadyBuffer(pcm);
	}
}
NS_CC_END

#endif
