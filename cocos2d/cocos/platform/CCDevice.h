/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/

#ifndef __CCDEVICE_H__
#define __CCDEVICE_H__

#include "platform/CCPlatformMacros.h"
#include "base/ccMacros.h"
#include "base/CCData.h"
#include "cocos2d.h"
#include "math/Vec2.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <jni.h>
#include "jni/JniHelper.h"
#endif
class VideoInputDeletegate
{
public:
    VideoInputDeletegate(){};
    std::function<int(unsigned char*, unsigned char*, unsigned char*)> func;
    std::function<void(cocos2d::Vec2& left, cocos2d::Vec2& right)> faceDetect;
    std::function<void(unsigned char*)> getVideoFrameRGBA;
    std::function<void(unsigned char*)> getVideoFrameBGRA;
    std::function<void(unsigned char*, unsigned char*)> getVideoFrameNV21;
    std::function<void(bool)> completeRecordCallback;
};

class AudioInputDeletegate
{
public:
	AudioInputDeletegate(){};
    std::function<int(short*)> func;
};

class ContactInfo
{
public:
    ContactInfo(std::string name, unsigned char* image, int size)
    {
        _name = name;
        _image = image;
        _size = size;
    }
    ~ContactInfo()
    {
        
    }
    std::string _name;
    unsigned char* _image;
    int _size;
};

NS_CC_BEGIN

struct FontDefinition;

class CC_DLL Device
{
public:
    enum class Res
    {
    	CheckContactList = 0,
        SearchContact
    };
    enum class TextAlign
    {
        CENTER        = 0x33, ///< Horizontal center and vertical center.
        TOP           = 0x13, ///< Horizontal center and vertical top.
        TOP_RIGHT     = 0x12, ///< Horizontal right and vertical top.
        RIGHT         = 0x32, ///< Horizontal right and vertical center.
        BOTTOM_RIGHT = 0x22, ///< Horizontal right and vertical bottom.
        BOTTOM        = 0x23, ///< Horizontal center and vertical bottom.
        BOTTOM_LEFT  = 0x21, ///< Horizontal left and vertical bottom.
        LEFT          = 0x31, ///< Horizontal left and vertical center.
        TOP_LEFT      = 0x11, ///< Horizontal left and vertical top.
    };
    /**
     *  Gets the DPI of device
     *  @return The DPI of device.
     */
    static int getDPI();
    
    /**
     * To enable or disable accelerometer.
     */
    static void setAccelerometerEnabled(bool isEnabled);
    /**
     *  Sets the interval of accelerometer.
     */
    static void setAccelerometerInterval(float interval);

    static Data getTextureDataForText(const char * text, const FontDefinition& textDefinition, TextAlign align, int &width, int &height, bool& hasPremultipliedAlpha);
    
    static void setKeepScreenOn(bool value);
    
    static void startCameraPreview(VideoInputDeletegate *delegate, int previewSize);
    
    static void releaseCameraPreview();
    
    static void flipCameraPreview();
    
    static void setExposureCamera(int value);

    static int getExposureCamera();

    static bool turnCameraFlashOnOff();
    
    static void enableFaceDetect(bool enable);
    
    static bool createAudioIO(int samplerate, int channels, double duration);
    
    static void startVoiceRecorder(AudioInputDeletegate *delegate);

    static void pauseVoiceRecorder();

    static void resumeVoiceRecorder();

    static void stopVoiceRecorder();
    
    static void releaseVoiceRecorder();
    
    static void startVideoRecord(const char* filename);

    static bool startAudioTrack(int samplerate, int channels);
    
    static short* getAudioTrackBuffer();
    
    static void writeAudioTrack(short* buffer, int size);

    static void stopAudioTrack();
    
    static int getRingerMode();
    
    static void vibrate(bool onoff);

    static std::string getExternalStorageDirectory();
    
    static std::string getPhotoStorageDirectory();

    static void scanMediaNewFile(const char *filename);
    
    static void requestContactList();
    
    static std::function<void(Device::Res, void*, unsigned char*, int)> onContactListCallback;

    static void searchContact(const char* key);

    static bool getMyIPAddress(std::string& ipaddress, std::string& network_type);
    
    static std::string getPhoneNumber();
    
    static void setCountryCode(const char* code);
    
    static std::string getCountryCode();
    
    static std::string getCountryCodeLocale();
    
    static std::string getNetworkProvider();
    
    static void setRegisterID(const char* deviceToken);
    
    static std::string getRegisterID();
    
    static void setOfflineMessage(const char* message);
    
    static std::string getOfflineMessage();
    
    static void setHiddenMessage(const char* message);
    static std::list<std::string>* getHiddenMessageList();
    
    static int getDeviceLevel();
    
    static void playSound(std::string filepath);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    static void startVoiceRecorderEx(AudioInputDeletegate *delegate);
    static void setFrameRateCamera(int value);
    static bool sharePhoto(const char* filepath);
    static bool shareMovie(const char* filepath);
    static bool savePhoto(const char* filepath);
    static bool saveGif(const char* filepath);
    static void playSoundWithMixing(std::string filepath);
    static void saveMovie();
    static void lockAudioTrack();
    static void unlockAudioTrack();
    static void getPhoneNumberWithFablic(std::string event_name);
    static void requestContactAccess();
    static void requestMicAccess();
    static void closeWebBrowser();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    static void sendActionShareImage(const char *filename);
    static void sendActionShareMovie(const char *filename);
    static int updateFrame(unsigned char *dst_y, unsigned char *dst_u, unsigned char *dst_v);
    static void getVideoFrame(unsigned char *dst_y, unsigned char *dst_uv);
    static void updateFace(cocos2d::Vec2 left, cocos2d::Vec2 right);
    static void complateRecord(bool result);
    static int updatePCM(short* buffer);
    static std::string getInternalStoragePath();
    static VideoInputDeletegate *_videoDelegate;
    static AudioInputDeletegate *_audioDelegate;
    static jobject audioTrack;
    static jbyteArray audioByteArray;
    static cocos2d::JniMethodInfo t;
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	static void setFrameRateCamera(int value);
#endif

private:
    CC_DISALLOW_IMPLICIT_CONSTRUCTORS(Device);
};


NS_CC_END

#endif /* __CCDEVICE_H__ */
