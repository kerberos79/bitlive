//
//  Strings.cpp
//  BitLive
//
//  Created by keros on 5/16/15.
//
//

#include "Strings.h"

std::string Strings::systemFont;
std::string Strings::brushFont;
bool Strings::loadResource(LanguageType type)
{
    switch(type)
    {
        case cocos2d::LanguageType::KOREAN:
        {
            systemFont = "font/nanum.ttf";
            brushFont = "font/brush.ttf";
            break;
        }
        default:
        {
            systemFont = "font/nanum.ttf";
            brushFont = "font/brush.ttf";
            break;
        }
    }
    
    return true;
}

const char* Strings::getBrushFontType()
{
    return brushFont.c_str();
}

const char* Strings::getFontType()
{
    return systemFont.c_str();
}

