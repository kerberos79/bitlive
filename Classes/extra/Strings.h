//
//  Strings.h
//  BitLive
//
//  Created by keros on 5/16/15.
//
//

#ifndef __BitLive__Strings__
#define __BitLive__Strings__

#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/DictionaryHelper.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocostudio;
#define MACRO_INSERT_HASH(x, y) \
    _instance->getHash().insert(std::pair<std::string, std::string>(y, DICTOOL->getStringValue_json(x, y)))
class Strings
{
public:
    static bool loadResource(LanguageType type);
    static const char* getFontType();
    static const char* getBrushFontType();
private:
    static std::string systemFont;
    static std::string brushFont;
};
#endif /* defined(__BitLive__Strings__) */
