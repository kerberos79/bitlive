#include "AppDelegate.h"
#include "view/MainScene.h"
#include "extra/Strings.h"
#include "util/GLProgramManager.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include <signal.h>
#endif

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::create("My Game");
        director->setOpenGLView(glview);
    }

    // turn on display FPS
//    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 30);
    
    auto screenSize = glview->getFrameSize();

    std::vector<std::string> searchPaths;
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (screenSize.width == 1536 && screenSize.height == 2048)
    {
        auto resourceSize = Size(1536, 2048);
        searchPaths.push_back("res-1080p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
    else if (screenSize.width == 768 && screenSize.height == 1024)
    {
        auto resourceSize = Size(768, 1024);
        searchPaths.push_back("res-720p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
    else if (screenSize.width > 750)
    {
        auto resourceSize = Size(1242, 2208);
        searchPaths.push_back("res-1080p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
    else if (screenSize.width > 720)
    {
        auto resourceSize = Size(750, 1334);
        searchPaths.push_back("res-750p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
    else if (screenSize.width > 640)
    {
        auto resourceSize = Size(720, 1280);
        searchPaths.push_back("res-720p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
    else
    {
        auto resourceSize = Size(640, 960);
        searchPaths.push_back("res-640p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if (screenSize.width > 1080)
    {
        auto resourceSize = Size(1440, 2560);
        searchPaths.push_back("res-1440p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
    else if (screenSize.width > 720)
    {
        auto resourceSize = Size(1080, 1920);
        searchPaths.push_back("res-1080p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
    else if (screenSize.width > 640)
    {
        auto resourceSize = Size(720, 1280);
        searchPaths.push_back("res-720p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
    else if (screenSize.width > 480)
    {
        auto resourceSize = Size(640, 960);
        searchPaths.push_back("res-640p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
    else
    {
        auto resourceSize = Size(480, 800);
        searchPaths.push_back("res-480p");
        glview->setDesignResolutionSize(resourceSize.width,resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
    }
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		auto resourceSize = Size(720, 1280);
		searchPaths.push_back("res-720p");
		glview->setDesignResolutionSize(resourceSize.width, resourceSize.height, ResolutionPolicy::FIXED_WIDTH);
#endif
    searchPaths.push_back("emoticon");
    searchPaths.push_back("data");
    searchPaths.push_back("sample");
    searchPaths.push_back("sticker");
    searchPaths.push_back("decoration");
    GLProgramManager::initializeDevice();
    
    LanguageType type = getCurrentLanguage();
    
    switch(type)
    {
        case cocos2d::LanguageType::KOREAN:
        {
            searchPaths.push_back("string/ko");
            break;
        }
        default:
        {
            searchPaths.push_back("string/en");
            break;
        }
    }
    Strings::loadResource(type);
    FileUtils::getInstance()->setSearchPaths(searchPaths);
    // create a scene. it's an autorelease object
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
    auto layer = MainScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // run
    director->runWithScene(scene);
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}

void AppDelegate::exit()
{
    Director::getInstance()->end();
}
