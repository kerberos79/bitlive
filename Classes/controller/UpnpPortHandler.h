//
//  UpnpPortHandler.h
//  Vista
//
//  Created by kerberos on 2/2/15.
//
//

#ifndef __Vista__UpnpPortHandler__
#define __Vista__UpnpPortHandler__

#include <stdio.h>
#include <string>
#include <sstream>

#include "cocos2d.h"
#include "util/Tokenizer.h"
#include "webrtc/base/messagehandler.h"
#include "webrtc/base/sigslot.h"
#include "webrtc/base/thread.h"
#include "webrtc/base/criticalsection.h"

extern"C" {
#include "upnpc.h"
}

USING_NS_CC;
using namespace rtc;

class UpnpPortHandler : public MessageHandler, public sigslot::has_slots<> {
public:
    
    enum class Cmd : unsigned int
    {
        Start=1,
        Release
    };
        
    UpnpPortHandler();
    ~UpnpPortHandler();
	static UpnpPortHandler* getInstance();
	rtc::SocketAddress& GetExternalAddress(int internal_port);
    void Request();
    virtual void OnMessage(Message *msg);
    void RequestMapping();
    void RemoveMapping();
    void Release();
    bool IsSupport();
    SocketAddress& GetAddress();
    bool GetUpnpInfo(std::string& ip, std::string& port);
        
    bool Compare(std::string& ip);
    
private:
    static UpnpPortHandler* mUpnpPortHandler;
    Thread* _workerThread;
    struct UPNPUrls urls;
    struct IGDdatas data;
    CriticalSection criticalsection;
        
    const char *internalPort;
    const char *requestPort;
    std::string protocolType;
    SocketAddress externalAddress;
    char localAddr[64];
    char mappedAddr[64];
	char externalPort[40];
	char lanaddr[64];
    int mappedPort;
    bool supportUpnp;
	std::string result;
};
#endif /* defined(__Vista__UpnpPortHandler__) */
