#include "P2PChannelAllocator.h"

P2PChannelAllocator::P2PChannelAllocator()
:_workerThread(nullptr),
network_manager_(nullptr),
isIpv6Address(false),
isPublicAddress(true),
connection_successed(false),
_disconnected(false),
tcpServerPort_(nullptr)
{
    stun_servers.insert(StunAddr);
    _workerThread = new Thread();
    _workerThread->Start();
}

P2PChannelAllocator::~P2PChannelAllocator()
{
    if(network_manager_)
        delete network_manager_;
    if(_workerThread)
        delete _workerThread;
}

P2PChannelAllocator* P2PChannelAllocator::mP2PChannelAllocator = nullptr;

P2PChannelAllocator* P2PChannelAllocator::getInstance()
{
    if(mP2PChannelAllocator==nullptr)
    {
        mP2PChannelAllocator = new P2PChannelAllocator();
    }
    return mP2PChannelAllocator;
}

void P2PChannelAllocator::OnMessage(rtc::Message *msg)
{
    switch ((P2PChannelAllocator::Cmd)msg->message_id)
    {
        case Cmd::GetNetwork:
        {
            network_manager_ = new rtc::BasicNetworkManager();
            network_manager_->SignalNetworksChanged.connect(this, &P2PChannelAllocator::OnNetworksChanged);
            network_manager_->StartUpdating();
            break;
        }
        case Cmd::Allocate:
        {
            OnAllocate();
            break;
        }
        case Cmd::Reallocate:
        {
            if(_disconnected)
                break;
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                OnReallocate(msgData->data());
            }
            break;
        }
        case Cmd::Candidate:
        {
            if(_disconnected)
                break;
            TypedMessageData<cricket::Candidate*> *msgData = (TypedMessageData<cricket::Candidate*> *)msg->pdata;
            if(msgData)
            {
                if(ports_.empty())
                {
                    UIMessage* message = new UIMessage(UIMessage::Cmd::Invite);
                    message->arg1 = (int)InviteEventData::Type::Connecting;
                    CallStateHandler::getInstance()->UIMessageCallback(message);
                }
                OnCandidate(msgData->data());
                delete msgData;
            }
            break;
        }
        case Cmd::RelayResponse:
        {
            if(_disconnected)
                break;
            TypedMessageData<cricket::Candidate*> *msgData = (TypedMessageData<cricket::Candidate*> *)msg->pdata;
            if(msgData)
            {
                OnCandidateRelayResponse(msgData->data());
                delete msgData;
            }
            break;
        }
        case Cmd::Transport:
        {
            if(_disconnected)
                break;
            TypedMessageData<cricket::Connection*> *msgData = (TypedMessageData<cricket::Connection*> *)msg->pdata;
            if(msgData)
            {
                if(connection_successed)
                {
                    
                }
                else
                {
                    connection_successed = true;
                    P2PStreamer::getInstance()->SignalConnectionCompleted.connect(this, &P2PChannelAllocator::OnConnectionCompleted);
                    P2PStreamer::getInstance()->Start(msgData->data());
                    SendEmptyMessage(Cmd::SendConnection);
                }
                delete msgData;
            }
            break;
        }
        case Cmd::Timeout:
        {
            if(_disconnected)
                break;
            OnTimeout();
            break;
        }
        case Cmd::Ping:
        {
            if(_disconnected)
                break;
            if(connection_successed)
                break;
            TypedMessageData<cricket::Connection*> *msgData = (TypedMessageData<cricket::Connection*> *)msg->pdata;
            if(msgData->data()!=nullptr)
            {
                cricket::Connection* conn = msgData->data();
                msgData->data()->Ping(rtc::Time());
                SendMessageDelayed(Cmd::Ping, msgData->data(), 200);
                delete msgData;
            }
            break;
        }
        case Cmd::TCPPing:
        {
            TypedMessageData<cricket::Connection*> *msgData = (TypedMessageData<cricket::Connection*> *)msg->pdata;
            if(msgData)
            {
                rtc::PacketOptions options;
                test_buffer[0] = '0' + (test_count/100)%10;
                test_buffer[1] = '0' + (test_count/10)%10;
                test_buffer[2] = '0' + test_count%10;
                test_count++;
                if(test_count==1000)
                    test_count=0;
                msgData->data()->Send(test_buffer, sizeof(test_buffer), options);
                delete msgData;
            }
            break;
        }
        case Cmd::SendConnection:
        {
            P2PStreamer::getInstance()->SendConnectCommand();
            SendEmptyMessageDelayed(Cmd::SendConnection, 200);
            break;
        }
        case Cmd::Destroy:
        {
            DestroyChannels();
            break;
        }
        case Cmd::PortRemove:
        {
            TypedMessageData<cricket::Port*> *msgData = (TypedMessageData<cricket::Port*> *)msg->pdata;
            if(msgData)
            {
                DestroyPort(msgData->data());
                delete msgData;
            }
            break;
        }
        case Cmd::SendInvitation:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                OnSendInvitation(msgData->data());
                delete msgData;
            }
            break;
        }
        case Cmd::RecvInvitation:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                OnRecvInvitation(msgData->data());
                delete msgData;
            }
            break;
        }
        case Cmd::SendCancel:
        {
            OnSendCancel();
            break;
        }
        case Cmd::SendRefusal:
        {
            OnSendRefusal();
            break;
        }
        case Cmd::SendBusy:
        {
            OnSendBusy();
            break;
        }
    }
}

void P2PChannelAllocator::Init(std::string &localIp, std::string &network_type, std::string &provider)
{
    
}

void P2PChannelAllocator::SetUserID(std::string userid)
{
    _userId = userid;
}

void P2PChannelAllocator::DestroyChannels()
{
    CCLOG("DestroyChannels");
    _disconnected = true;
    _remoteUserId = "";
    _workerThread->Clear(this, (int)Cmd::SendConnection);
    _workerThread->Clear(this, (int)Cmd::Timeout);
    _workerThread->Clear(this, (int)Cmd::Ping);
    
    std::vector<cricket::Connection*>::iterator iterEnd = connections_.end();
    for(std::vector<cricket::Connection*>::iterator iterPos = connections_.begin();
        iterPos != iterEnd;
        ++iterPos )
    {
        if(*iterPos != nullptr)
            (*iterPos)->Destroy();
    }
    connections_.clear();
    if(tcpServerPort_)
    {
        std::list<cricket::Port*>::iterator iterEnd = ports_.end();
        for(std::list<cricket::Port*>::iterator iterPos = ports_.begin();
            iterPos != iterEnd;
            ++iterPos )
        {
            if(*iterPos == tcpServerPort_)
            {
                ports_.erase(iterPos);
                break;
            }
        }
        delete tcpServerPort_;
        tcpServerPort_ = nullptr;
    }
    connection_successed = false;
    
    UpnpPortHandler::getInstance()->RemoveMapping();
}

void P2PChannelAllocator::DestroyPort(cricket::Port* port)
{
    std::list<cricket::Port*>::iterator iterEnd = ports_.end();
    for(std::list<cricket::Port*>::iterator iterPos = ports_.begin();
        iterPos != iterEnd;
        ++iterPos )
    {
        if(*iterPos == port)
        {
            ports_.erase(iterPos);
            if(port->connections().empty())
            {
                port->Destroy();
            }
            else
            {
                delete port;
            }
            break;
        }
    }
}

cricket::Port* P2PChannelAllocator::SearchPortFromCandidate(cricket::Candidate& candidate)
{
    
    if(!candidate.protocol().compare("udp"))
    {
        std::list<cricket::Port*>::iterator iterEnd = ports_.end();
        for(std::list<Port*>::iterator iterPos = ports_.begin();
            iterPos != iterEnd;
            ++iterPos )
        {
            std::vector<cricket::Candidate> c = (*iterPos)->Candidates();
            for( int i=0; i<c.size();i++)
            {
                if(c[i].protocol() == candidate.protocol() && c[i].type() == candidate.type())
                    return (*iterPos);
            }
        }
    }
    
    return nullptr;
}
void P2PChannelAllocator::OnAllocate()
{
    _disconnected = false;
    cricket::Port* port;
    
    if(isPublicAddress || UpnpPortHandler::getInstance()->IsSupport())
    {
        port = CreateTCPPorts(true);
        ports_.push_back(port);
        tcpServerPort_ = port;
    }
    else
    {
        XmppMainHandler::getInstance()->RequestReallocation(_remoteUserId);
    }

    port = CreateUDPPorts();
    ports_.push_back(port);
    port = CreateStunPorts();
    ports_.push_back(port);
     
    std::list<cricket::Port*>::iterator iterEnd = ports_.end();
    for(std::list<Port*>::iterator iterPos = ports_.begin();
        iterPos != iterEnd;
        ++iterPos )
    {
        (*iterPos)->SignalConnectionCreated.connect(this, &P2PChannelAllocator::OnConnectionCreated);
        (*iterPos)->SignalCandidateReady.connect(this, &P2PChannelAllocator::OnCandidateReady);
        (*iterPos)->SignalPortComplete.connect(this, &P2PChannelAllocator::OnPortComplete);
        (*iterPos)->SignalDestroyed.connect(this, &P2PChannelAllocator::OnPortDestroyed);
        (*iterPos)->SignalPortError.connect(this, &P2PChannelAllocator::OnPortError);
        
        (*iterPos)->set_generation(1);
        (*iterPos)->PrepareAddress();
    }
    SendEmptyMessageDelayed(Cmd::Timeout, P2P_STUN_TIMEOUT);
}

void P2PChannelAllocator::OnReallocate(std::string& remoteUser)
{
    cricket::Port* port;
    
    if(isIpv6Address || isPublicAddress || UpnpPortHandler::getInstance()->IsSupport())
    {
        port = CreateTCPPorts(true);
        ports_.push_back(port);
        tcpServerPort_ = port;
    }
    
    std::list<cricket::Port*>::iterator iterEnd = ports_.end();
    for(std::list<Port*>::iterator iterPos = ports_.begin();
        iterPos != iterEnd;
        ++iterPos )
    {
        (*iterPos)->SignalConnectionCreated.connect(this, &P2PChannelAllocator::OnConnectionCreated);
        (*iterPos)->SignalCandidateReady.connect(this, &P2PChannelAllocator::OnCandidateReady);
        (*iterPos)->SignalPortComplete.connect(this, &P2PChannelAllocator::OnPortComplete);
        (*iterPos)->SignalDestroyed.connect(this, &P2PChannelAllocator::OnPortDestroyed);
        (*iterPos)->SignalPortError.connect(this, &P2PChannelAllocator::OnPortError);
        
        (*iterPos)->set_generation(1);
        (*iterPos)->PrepareAddress();
    }
}

void P2PChannelAllocator::OnCandidate(cricket::Candidate* candidate)
{
    CCLOG("[OnCandidate] %s", candidate->ToString().c_str());
    cricket::Connection* connection;
    cricket::Port* port;
    port = SearchPortFromCandidate(*candidate);
    
    if(port==nullptr)
    {
        if(!candidate->protocol().compare("tcp"))
        {
            port = CreateTCPPorts(false);
            ports_.push_back(port);
        }
        else if(!candidate->type().compare("stun"))
        {
            port = CreateStunPorts();
            ports_.push_back(port);
        }
        else
        {
            port = CreateUDPPorts();
            ports_.push_back(port);
        }
        
        port->SignalConnectionCreated.connect(this, &P2PChannelAllocator::OnConnectionCreated);
        port->SignalCandidateReady.connect(this, &P2PChannelAllocator::OnCandidateReady);
        port->SignalPortComplete.connect(this, &P2PChannelAllocator::OnPortComplete);
        port->SignalDestroyed.connect(this, &P2PChannelAllocator::OnPortDestroyed);
        port->SignalPortError.connect(this, &P2PChannelAllocator::OnPortError);
        port->set_generation(1);
        port->PrepareAddress();
        
        
        if(!candidate->protocol().compare("tcp"))
        {
            connection = port->CreateConnection(*candidate, Port::CandidateOrigin::ORIGIN_THIS_PORT);
        }
        else
        {
            connection = port->CreateConnection(*candidate, Port::CandidateOrigin::ORIGIN_THIS_PORT);
        }
    }
    else if(candidate->protocol().compare("tcp"))
    {
        connection = port->GetConnection(candidate->address());
        if(connection==nullptr)
        {
            connection = port->CreateConnection(*candidate, Port::CandidateOrigin::ORIGIN_THIS_PORT);
        }
    }
    delete candidate;
}

void P2PChannelAllocator::OnCandidateRelay(cricket::Candidate* candidate)
{
    CCLOG("OnCandidateRelay");
    
    cricket::Connection* connection;
    cricket::Port* port;
    
    _workerThread->Clear(this, (int)Cmd::Ping);
    
    if(!candidate->protocol().compare("tcp"))
    {
        port = CreateTCPPorts(false);
        ports_.push_back(port);
        
        port->SignalConnectionCreated.connect(this, &P2PChannelAllocator::OnConnectionCreated);
        port->SignalCandidateReady.connect(this, &P2PChannelAllocator::OnCandidateReady);
        port->SignalPortComplete.connect(this, &P2PChannelAllocator::OnPortComplete);
        port->SignalDestroyed.connect(this, &P2PChannelAllocator::OnPortDestroyed);
        port->SignalPortError.connect(this, &P2PChannelAllocator::OnPortError);
        port->set_generation(1);
        port->PrepareAddress();
        
        connection = port->CreateConnection(*candidate, Port::CandidateOrigin::ORIGIN_THIS_PORT);
    }
    delete candidate;
}

void P2PChannelAllocator::OnCandidateRelayResponse(cricket::Candidate* candidate)
{
    CCLOG("OnCandidateRelayResponse");
    
    cricket::Connection* connection;
    cricket::Port* port;
    
    if(candidate->address().port()==0)
    {
        CCLOG("fail to allocate a relay port");
        delete candidate;
        return;
    }
    
    if(!candidate->protocol().compare("tcp"))
    {
        port = CreateTCPPorts(false);
        ports_.push_back(port);
        
        port->SignalConnectionCreated.connect(this, &P2PChannelAllocator::OnConnectionCreated);
        port->SignalCandidateReady.connect(this, &P2PChannelAllocator::OnCandidateReady);
        port->SignalPortComplete.connect(this, &P2PChannelAllocator::OnPortComplete);
        port->SignalDestroyed.connect(this, &P2PChannelAllocator::OnPortDestroyed);
        port->SignalPortError.connect(this, &P2PChannelAllocator::OnPortError);
        port->set_generation(1);
        port->PrepareAddress();
        
        connection = port->CreateConnection(*candidate, Port::CandidateOrigin::ORIGIN_THIS_PORT);
        XmppMainHandler::getInstance()->OnCandidate(_remoteUserId, candidate);
    }
    
}

void P2PChannelAllocator::OnTimeout()
{
    CCLOG("OnTimeout");
    
    _workerThread->Clear(this, (int)Cmd::Ping);
    XmppMainHandler::getInstance()->RequestRelayPort();
}

void P2PChannelAllocator::Release()
{
    if(network_manager_)
        delete network_manager_;
}

cricket::Port* P2PChannelAllocator::CreateTCPPorts(bool allow_listen) {
    cricket::Port* port = nullptr;
    port = TCPPort::Create(rtc::Thread::Current(),
                          socket_factory_,
                          network_, network_->ip(),
                          MIN_PORT,
                          MAX_PORT,
                          CreateRandomString(16),
                          CreateRandomString(16),
                           allow_listen);
#ifdef WEBRTC_IOS
    port->SetOption(rtc::Socket::Option::OPT_SIGPIPE, 1);
#endif
    return port;
}

cricket::Port* P2PChannelAllocator::CreateUDPPorts() {
    
    cricket::Port* port = UDPPort::Create(rtc::Thread::Current(),
                                          socket_factory_,
                                          network_, network_->ip(),
                                          MIN_PORT,
                                          MAX_PORT,
                                          CreateRandomString(16),
                                          CreateRandomString(16),
                                          "origin");
    return port;
}

cricket::Port* P2PChannelAllocator::CreateStunPorts() {
    
    cricket::Port* port = StunPort::Create(rtc::Thread::Current(),
                                           socket_factory_,
                                           network_, network_->ip(),
                                           MIN_PORT,
                                           MAX_PORT,
                                           CreateRandomString(16),
                                           CreateRandomString(16),
                                           stun_servers,
                                           "origin");
    return port;
}

cricket::Port* P2PChannelAllocator::CreateTurnPorts() {
    
    RelayServerConfig config(RELAY_TURN);
    
    cricket::RelayCredentials credentials(CallStateHandler::getInstance()->GetLoginID(), CallStateHandler::getInstance()->GetLoginPassword());
    cricket::TurnPort* port = TurnPort::Create(rtc::Thread::Current(),
                                               socket_factory_,
                                               network_, network_->ip(),
                                               MIN_PORT,
                                               MAX_PORT,
                                               CreateRandomString(16),
                                               CreateRandomString(16),
                                               RelayProtoAddr,
                                               credentials,
                                               config.priority,
                                               "");
    _turnport = port;
    return port;
}


void P2PChannelAllocator::OnNetworksChanged() {
    std::vector<rtc::Network*> networks;
    network_manager_->GetNetworks(&networks);
    if(networks.size()>0)
    {
        network_ = networks[0];
        socket_factory_ = new rtc::BasicPacketSocketFactory(rtc::Thread::Current());
        test_count = 0;
        thisipaddress = network_->ip().ToString();
        CCLOG("This machine ip is %s, %d", thisipaddress.c_str(), network_->ip().family());
        isIpv6Address = false;
        isPublicAddress = true;
        if(network_->ip().family() == AF_INET6)
        {
            if(thisipaddress.substr(0, 6).compare("fe80::"))
            {
                isIpv6Address = true;
            }
        }
        else
        {
            if(!thisipaddress.substr(0, 7).compare("192.168"))
            {
                isPublicAddress = false;
            }
            else if(!thisipaddress.substr(0, 3).compare("172"))
            {
                std::string temp = thisipaddress.substr(4, 6);
                if(temp[2]=='.')
                {
                    int value = (temp[0] - '0')* 10 + (temp[1] - '0');
                    if(value>=16 || value<=31)
                        isPublicAddress = false;
                }
            }
            else if(!thisipaddress.substr(0, 3).compare("10."))
            {
                isPublicAddress = false;
            }
        }
        CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Start);
    }
    else
    {
        CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::NotFoundNetwork);
    }
    
    network_manager_->StopUpdating();
}

void P2PChannelAllocator::OnCandidateReady(cricket::Port* port, const cricket::Candidate& candidate)
{
    CCLOG("[OnCandidateReady] type : %s [%s]", candidate.address().ToString().c_str(), candidate.protocol().c_str());
    cricket::Candidate newCandidate = candidate;
    if(!candidate.protocol().compare("tcp"))
    {
        if(candidate.address().port()!=0)
        {
            if(UpnpPortHandler::getInstance()->IsSupport())
            {
                SocketAddress& address = UpnpPortHandler::getInstance()->GetExternalAddress(candidate.address().port());
                newCandidate.set_address(address);
                SendNewCandidate(newCandidate);
            }
            else
            {
                CCLOG("[OnCandidateReady] type : %s [%d]", network_->ip().ToString().c_str(), candidate.address().port());
                newCandidate.set_address(rtc::SocketAddress(network_->ip(), candidate.address().port()));
            
                SendNewCandidate(newCandidate);
            }
        }
    }
    else
    {
        SendNewCandidate(newCandidate);
    }
    
}


void P2PChannelAllocator::SendNewCandidate(cricket::Candidate& candidate)
{
    cricket::Candidate* newCandidate = new cricket::Candidate();
    newCandidate->set_username(candidate.username());
    newCandidate->set_password(candidate.password());
    newCandidate->set_protocol(candidate.protocol());
    newCandidate->set_type(candidate.type());
    newCandidate->set_generation(candidate.generation());
    newCandidate->set_address(candidate.address());
    
    XmppMainHandler::getInstance()->OnCandidate(_remoteUserId, newCandidate);
}

void P2PChannelAllocator::OnPortError(cricket::Port* port)
{
    CCLOG("OnPortError");
    SendMessage(P2PChannelAllocator::Cmd::PortRemove, port);
}

void P2PChannelAllocator::OnPortDestroyed(PortInterface* port)
{
    CCLOG("OnPortDestroyed");
}

void P2PChannelAllocator::OnPortComplete(cricket::Port* port)
{
}

void P2PChannelAllocator::OnConnectionCreated(cricket::Port* port, cricket::Connection *conn)
{
    conn->SignalDestroyed.connect(this, &P2PChannelAllocator::OnConnectionDestroyed);
    conn->SignalStateChange.connect(this, &P2PChannelAllocator::OnConnectionStateChange);
    connections_.push_back(conn);
    
    if(port->Candidates().size()>0)
    {
        if(port->Candidates()[0].protocol().compare("tcp"))
        {
            SendMessageDelayed(Cmd::Ping, conn, 500);
            if(tcpServerPort_ == port)
            {
                tcpServerPort_ = nullptr;
            }
            CCLOG("OnConnectionCreated udp %s", port->Candidates()[0].address().ToString().c_str());
        }
        else
        {
            CCLOG("OnConnectionCreated tcp %s", port->Candidates()[0].address().ToString().c_str());
        }
    }
    else
    {
        SendMessageDelayed(Cmd::Ping, conn, 500);
    }
     
}

void P2PChannelAllocator::OnReadPacket(cricket::Connection *connection,
                               const char *data, size_t len, const rtc::PacketTime& time) {
    
    CCLOG("OnReadPacket ");
}

void P2PChannelAllocator::OnConnectionDestroyed(cricket::Connection *conn)
{
    CCLOG("OnConnectionDestroyed %x", conn);
    
    std::vector<cricket::Connection*>::iterator iterEnd = connections_.end();
    for(std::vector<cricket::Connection*>::iterator iterPos = connections_.begin();
        iterPos != iterEnd;
        ++iterPos )
    {
        if(*iterPos == conn)
        {
            connections_.erase(iterPos);
            break;
        }
    }
    cricket::Port* port = conn->port();
    if(port)
    {
        SendMessage(P2PChannelAllocator::Cmd::PortRemove, port);
     }
}

void P2PChannelAllocator::OnWaitTcpPing(cricket::Connection *connection,
                                       const char *data, size_t len,
                                       const rtc::PacketTime& time) {
    CCLOG("OnWaitTcpPing[%d] %s ", (int)len, data);
}

void P2PChannelAllocator::OnConnectionStateChange(cricket::Connection* conn)
{
    CCLOG("OnConnectionStateChange[r:%d, w:%d]",conn->read_state(), conn->write_state());
    if(conn->write_state() == Connection::STATE_WRITABLE &&
       conn->read_state() == Connection::STATE_READABLE)
    {
        CCLOG("OnConnectionStateChange[RW]");
        _workerThread->Clear(this, (int)Cmd::Timeout);
        _workerThread->Clear(this, (int)Cmd::Ping);
        SendMessage(Cmd::Transport, conn);
    }
}

void P2PChannelAllocator::OnConnectionCompleted()
{
    _workerThread->Clear(this, (int)Cmd::SendConnection);
}

void P2PChannelAllocator::SendEmptyMessage(P2PChannelAllocator::Cmd cmd)
{
    _workerThread->Post(this, (unsigned int)cmd);
}

void P2PChannelAllocator::SendEmptyMessageDelayed(P2PChannelAllocator::Cmd cmd, int delay)
{
    _workerThread->PostDelayed(delay, this, (unsigned int)cmd);
}

void P2PChannelAllocator::SendMessage(P2PChannelAllocator::Cmd cmd, std::string userData)
{
    TypedMessageData<std::string> *msgData = new TypedMessageData<std::string>(userData);
    _workerThread->Post(this, (unsigned int)cmd, msgData);
}

void P2PChannelAllocator::SendMessage(P2PChannelAllocator::Cmd cmd, cricket::Connection* conn)
{
    TypedMessageData<cricket::Connection*> *msgData = new TypedMessageData<cricket::Connection*>(conn);
    _workerThread->Post(this, (unsigned int)cmd, msgData);
}

void P2PChannelAllocator::SendMessage(P2PChannelAllocator::Cmd cmd, cricket::Port* port)
{
    TypedMessageData<cricket::Port*> *msgData = new TypedMessageData<cricket::Port*>(port);
    _workerThread->Post(this, (unsigned int)cmd, msgData);
}

void P2PChannelAllocator::SendMessageDelayed(P2PChannelAllocator::Cmd cmd, cricket::Connection* conn, int delay)
{
    TypedMessageData<cricket::Connection*> *msgData = new TypedMessageData<cricket::Connection*>(conn);
    _workerThread->PostDelayed(delay, this, (unsigned int)cmd, msgData);
}

void P2PChannelAllocator::SendRemoteCandidate(cricket::Candidate* candidate)
{
    TypedMessageData<cricket::Candidate*> *msgData = new TypedMessageData<cricket::Candidate*>(candidate);
    _workerThread->Post(this, (unsigned int)Cmd::Candidate, msgData);
}

void P2PChannelAllocator::SendRelayPortResponse(cricket::Candidate* candidate)
{
    TypedMessageData<cricket::Candidate*> *msgData = new TypedMessageData<cricket::Candidate*>(candidate);
    _workerThread->Post(this, (unsigned int)Cmd::RelayResponse, msgData);
}

void P2PChannelAllocator::OnSendInvitation(std::string remote)
{
    if(_remoteUserId.length()>0)
        return;
    _disconnected = false;
    _remoteUserId = remote;
    uint32 currenttime = rtc::Time();
    _timestamp = std::to_string(currenttime);
    
    std::string message = "I:"+_userId+":"+_timestamp;
    XmppMainHandler::getInstance()->SendText(remote, message);
}

void P2PChannelAllocator::OnSendCancel()
{
    std::string message = "C:"+_userId+":"+_timestamp;
    XmppMainHandler::getInstance()->SendText(_remoteUserId, message);
    SendEmptyMessage(P2PChannelAllocator::Cmd::Destroy);
}

void P2PChannelAllocator::OnSendRefusal()
{
    std::string message = "R:"+_userId+":"+_timestamp;
    XmppMainHandler::getInstance()->SendText(_remoteUserId, message);
    SendEmptyMessage(P2PChannelAllocator::Cmd::Destroy);
}

void P2PChannelAllocator::OnSendBusy()
{
    std::string message = "B:"+_userId+":"+_timestamp;
    XmppMainHandler::getInstance()->SendText(_remoteUserId, message);
    SendEmptyMessage(P2PChannelAllocator::Cmd::Destroy);
}

void P2PChannelAllocator::OnRecvInvitation(std::string message)
{
    std::vector<std::string> tokens;
    const std::string delimiters = ":";
    
    Tokenizer::tokenize(message, tokens, delimiters);
    const char prefix = tokens.front().front();
    std::string remoteId = tokens.at(1);
    std::string timestamp = tokens.at(2);
    
    InviteEventData* newInvitation = new InviteEventData();
    
    switch(prefix)
    {
        case PREFIX_INVITE:
        {
            if(_remoteUserId.length()==0)
            {
                newInvitation->what = InviteEventData::Type::Recv;
                newInvitation->data = remoteId;
                _remoteUserId = remoteId;
                _timestamp = timestamp;
                CallStateHandler::getInstance()->SendXmppMessage(CallStateHandler::Cmd::XmppMessage, newInvitation);
            }
            else
            {
                std::string newMessage = "B:"+_userId+":"+timestamp;
                XmppMainHandler::getInstance()->SendText(remoteId, newMessage);
            }
            break;
        }
        case PREFIX_REFUSE:
        {
            if(_remoteUserId.length()>0 && !_remoteUserId.compare(remoteId))
            {
                newInvitation->what = InviteEventData::Type::Refusal;
                CallStateHandler::getInstance()->SendXmppMessage(CallStateHandler::Cmd::XmppMessage, newInvitation);
                SendEmptyMessage(P2PChannelAllocator::Cmd::Destroy);
            }
            break;
        }
        case PREFIX_BUSY:
        {
            if(_remoteUserId.length()>0 && !_remoteUserId.compare(remoteId))
            {
                newInvitation->what = InviteEventData::Type::Busy;
                CallStateHandler::getInstance()->SendXmppMessage(CallStateHandler::Cmd::XmppMessage, newInvitation);
                SendEmptyMessage(P2PChannelAllocator::Cmd::Destroy);
            }
            break;
        }
        case PREFIX_CANCEL:
        {
            if(_remoteUserId.length()>0 && !_remoteUserId.compare(remoteId))
            {
                newInvitation->what = InviteEventData::Type::Cancel;
                CallStateHandler::getInstance()->SendXmppMessage(CallStateHandler::Cmd::XmppMessage, newInvitation);
                SendEmptyMessage(P2PChannelAllocator::Cmd::Destroy);
            }
            break;
        }
        case PREFIX_MESSAGE:
        {
            if(_remoteUserId.length()>0 && !_remoteUserId.compare(remoteId))
            {
                newInvitation->what = InviteEventData::Type::Message;
            }
            break;
        }
        case PREFIX_ERROR:
        {
            if(_remoteUserId.length()>0)
            {
                newInvitation->what = InviteEventData::Type::Error;
                CallStateHandler::getInstance()->SendXmppMessage(CallStateHandler::Cmd::XmppMessage, newInvitation);
                SendEmptyMessage(P2PChannelAllocator::Cmd::Destroy);
            }
            break;
        }
        default:
            break;
    }
}
