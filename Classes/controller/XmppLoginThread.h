//
//  XmppLoginThread.h
//  Vist
//
//  Created by kerberos on 1/9/15.
//
//

#ifndef __Vist__XmppLoginThread__
#define __Vist__XmppLoginThread__

#include <stdio.h>
#include <string>
#include <iostream> 
#include "webrtc/base/logging.h"
#include "webrtc/base/sigslot.h"
#include "webrtc/base/ssladapter.h"
#include "webrtc/base/thread.h"
#include "webrtc/libjingle/xmpp/xmppthread.h"
#include "webrtc/libjingle/xmpp/xmppauth.h"
#include "webrtc/libjingle/xmpp/xmpppump.h"
#include "controller/XmppMainHandler.h"
#include "util/define.h"

class XmppMainHandler;

class XmppLoginThread : public rtc::Runnable
{
public:
    
    XmppLoginThread();
    ~XmppLoginThread();
    static XmppLoginThread* Instance();
    
    void Init(std::string& loginId, std::string& password, std::string localIp, std::string networktype, std::string provider);
    void Test(const char *id, std::string localIp, std::string networktype, std::string provider);
    void TestCreateChannel(std::string username, std::string remote);
    void Start();
    void Release();
    void Run(rtc::Thread* thread);
    static void* ThreadFunction(void* arg);

private:
    static XmppLoginThread *_xmppLoginThread;
    rtc::Thread* _workerThread;
    rtc::Thread* _mainThread;
    buzz::XmppClientSettings _xcs;
    buzz::XmppPump _pump;
    const char* _id;
    const char* _password;
};


#endif /* defined(__Vist__XmppLoginThread__) */
