//
//  CallStateHandler.cpp
//  Vist
//
//  Created by kerberos on 1/16/15.
//
//

#include "CallStateHandler.h"

CallStateHandler::CallStateHandler()
:_xmppHandler(nullptr),
 _workerThread(nullptr)
{
    _workerThread = new Thread();
    _workerThread->Start();
}

CallStateHandler::~CallStateHandler()
{
    delete _workerThread;
}

CallStateHandler* CallStateHandler::_callStateHandler = nullptr;

CallStateHandler* CallStateHandler::getInstance()
{
    if(_callStateHandler==nullptr)
    {
        _callStateHandler = new CallStateHandler();
        if(_callStateHandler && _callStateHandler->Init())
        {
            return _callStateHandler;
        }
    }
    return _callStateHandler;
}

bool CallStateHandler::Init()
{
    return true;
}

void CallStateHandler::Start()
{
    P2PChannelAllocator::getInstance()->SendEmptyMessage(P2PChannelAllocator::Cmd::GetNetwork);
}

void CallStateHandler::Install(bool offline)
{
    SendMessage(Cmd::Install, offline);
}

void CallStateHandler::OnMessage(Message *msg)
{
    switch ((CallStateHandler::Cmd)msg->message_id)
    {
        case Cmd::Init:
        {
            break;
        }
        case Cmd::Install:
        {
            TypedMessageData<bool> *msgData = (TypedMessageData<bool> *)msg->pdata;
            if(msgData)
            {
                if(msgData->data())
                {
                    UIMessage* message = new UIMessage(UIMessage::Cmd::NotFountNetwork);
                    UIMessageCallback(message);
                }
                else
                {
                    P2PChannelAllocator::getInstance()->SendEmptyMessage(P2PChannelAllocator::Cmd::GetNetwork);
                }
            }
            break;
        }
        case Cmd::NotFoundNetwork:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::NotFountNetwork);
            UIMessageCallback(message);
            break;
        }
        case Cmd::Start:
        {
            if(Device::getMyIPAddress(_localIpAddress, _networkType))
            {
                _networkProvider = Device::getNetworkProvider();
                
                Device::onContactListCallback = CC_CALLBACK_4(CallStateHandler::OnDeviceMessage, this);
                
                SendEmptyMessage(Cmd::CheckIdentity);
                SendEmptyMessage(Cmd::RequestUpnpPort);
            }
//                P2PChannelAllocator::getInstance()->Init(_localIpAddress,_networkType, _networkProvider);
            break;
        }
        case Cmd::CheckIdentity:
        {
            std::string version = UserDefault::getInstance()->getStringForKey("firsttime");
            if(version.length()==0)
            {
                UIMessage* message = new UIMessage(UIMessage::Cmd::Install);
                message->arg1 = (int)UIMessage::Install::Start;
                UIMessageCallback(message);
            }
            else
            {
                UIMessage* message = new UIMessage(UIMessage::Cmd::Install);
                message->arg1 = (int)UIMessage::Install::Skip;
                UIMessageCallback(message);
                _loginId = UserDefault::getInstance()->getStringForKey("loginid");
                _loginPassword = UserDefault::getInstance()->getStringForKey("loginpassword");
                SendEmptyMessage(Cmd::Login);
            }
            break;
        }
        case Cmd::SignUp:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                std::string regID = Device::getRegisterID();
                _signupInfo.store(msgData->data(), msgData->data(), regID);
                
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) && defined(COCOS2D_DEBUG)
                std::string url = std::string(XMPP_HTTP_SIGNUP_URL) + "&username="+_signupInfo.getUserId()+"&password="+_signupInfo.getPassword()+"&os=d&param="+_signupInfo.getRegisterId();
    #endif
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) && !defined(COCOS2D_DEBUG)
                std::string url = std::string(XMPP_HTTP_SIGNUP_URL) + "&username="+_signupInfo.getUserId()+"&password="+_signupInfo.getPassword()+"&os=i&param="+_signupInfo.getRegisterId();
    #endif
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                std::string url = std::string(XMPP_HTTP_SIGNUP_URL) + "&username="+_signupInfo.getUserId()+"&password="+_signupInfo.getPassword()+"&os=a&param="+_signupInfo.getRegisterId();
	#endif	
	#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
				std::string url = std::string(XMPP_HTTP_SIGNUP_URL) + "&username=" + _signupInfo.getUserId() + "&password=" + _signupInfo.getPassword() + "&os=w&param=" + _signupInfo.getRegisterId();
	#endif
                std::string tag = "signup";
                SendHttpRequest(XMPP_SERVER_ADDRESS, XMPP_SERVER_HTTP_PORT, url, tag);
                delete msgData;
            }
            break;
        }
        case Cmd::SignUpSuccess:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::Install);
            message->arg1 = (int)UIMessage::Install::Complete;
            UIMessageCallback(message);
            break;
        }
        case Cmd::SignUpFail:
        {
            break;
        }
        case Cmd::DeleteAccount:
        {
            std::string url = std::string(XMPP_HTTP_DELETE_URL) + "&username="+_loginId;
            std::string tag = "delete";
            SendHttpRequest(XMPP_SERVER_ADDRESS, XMPP_SERVER_HTTP_PORT, url, tag);
            break;
        }
        case Cmd::DeleteAccountSuccess:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::DeleteAccount);
            message->flag = true;
            UIMessageCallback(message);
            break;
        }
        case Cmd::DeleteAccountFail:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::DeleteAccount);
            message->flag = false;
            UIMessageCallback(message);
            break;
        }
        case Cmd::Login:
        {
            XmppLoginThread::Instance()->Init(_loginId, _loginPassword, _localIpAddress, _networkType, Device::getNetworkProvider());
            XmppLoginThread::Instance()->Start();
            break;
        }
        case Cmd::RequestContactList:
        {
            Device::requestContactList();
            break;
        }
        case Cmd::RequestUpnpPort:
        {
            UpnpPortHandler::getInstance()->Request();
            break;
        }
        case Cmd::LoginSuccess:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::Login);
            message->flag = true;
            UIMessageCallback(message);
            P2PChannelAllocator::getInstance()->SetUserID(_loginId);
            SendEmptyMessage(Cmd::CheckOfflineMessage);
            break;
        }
        case Cmd::LoginFail:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::Login);
            message->flag = false;
            UIMessageCallback(message);
            break;
        }
        case Cmd::CheckOfflineMessage:
        {
            std::string message = Device::getOfflineMessage();
            if(message.length()!=0)
            {
                P2PChannelAllocator::getInstance()->SendMessage(P2PChannelAllocator::Cmd::RecvInvitation, message);
            }
            std::list<std::string>* messageList = Device::getHiddenMessageList();
            if(messageList)
            {
                messageList->clear();
                delete messageList;
            }
            break;
        }
        case Cmd::XmppMessage:
        {
            TypedMessageData<InviteEventData*> *msgData = (TypedMessageData<InviteEventData*> *)msg->pdata;
            if(msgData)
            {
                HandleXmppMessage((InviteEventData *)msgData->data());
                delete msgData;
            }
            break;
        }
        case Cmd::Invite:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                std::string username = IdentityManager::makeUserID(msgData->data());
                if(username.length()>0)
                {
                    P2PChannelAllocator::getInstance()->SendMessage(P2PChannelAllocator::Cmd::SendInvitation, username);
                }
                delete msgData;
            }
            break;
        }
        case Cmd::Accept:
        {
            P2PChannelAllocator::getInstance()->SendEmptyMessage(P2PChannelAllocator::Cmd::Allocate);
            UIMessage* message = new UIMessage(UIMessage::Cmd::Invite);
            message->arg1 = (int)InviteEventData::Type::Connecting;
            UIMessageCallback(message);
            break;
        }
        case Cmd::Refusal:
        {
            P2PChannelAllocator::getInstance()->SendEmptyMessage(P2PChannelAllocator::Cmd::SendRefusal);
            break;
        }
        case Cmd::Cancel:
        {
            P2PChannelAllocator::getInstance()->SendEmptyMessage(P2PChannelAllocator::Cmd::SendCancel);
            break;
        }
        case Cmd::Busy:
        {
            P2PChannelAllocator::getInstance()->SendEmptyMessage(P2PChannelAllocator::Cmd::SendBusy);
            break;
        }
        case Cmd::ConnectionSuccess:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                UIMessage* message = new UIMessage(UIMessage::Cmd::Connect);
                message->flag = true;
                message->data = msgData->data();
                UIMessageCallback(message);
                delete msgData;
            }
            break;
        }
        case Cmd::ConnectionFail:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::Connect);
            message->flag = false;
            UIMessageCallback(message);
        	break;
        }
        case Cmd::Disconnect:
        {
            P2PStreamer::getInstance()->SendDisconnectedCommand();
            P2PStreamer::getInstance()->RequestStopEncoding();
            break;
        }
        case Cmd::Disconnected:
        {
            P2PStreamer::getInstance()->RequestStopEncoding();
            UIMessage* message = new UIMessage(UIMessage::Cmd::Disconnect);
            UIMessageCallback(message);
            break;
        }
        case Cmd::Release:
        {
//            P2PChannelAllocator::getInstance()->Release();
            UpnpPortHandler::getInstance()->Release();
            P2PStreamer::getInstance()->Release();
            break;
        }
        case Cmd::Exit:
        {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            Director::getInstance()->end();
            exit(0);
#endif
        	break;
        }
        case Cmd::SendEffectCommand:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                P2PStreamer::getInstance()->SendEffectCommand(msgData->data().c_str());
            }
            delete msgData;
            break;
        }
        case Cmd::SendEmoticonCommand:
        {
            TypedMessageData<int> *msgData = (TypedMessageData<int> *)msg->pdata;
            if(msgData)
            {
                P2PStreamer::getInstance()->SendEmoticonCommand(msgData->data());
            }
            delete msgData;
            break;
        }
        case Cmd::SendStickerCommand:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                P2PStreamer::getInstance()->SendStickerCommand(msgData->data());
            }
            delete msgData;
            break;
        }
        case Cmd::SendRotateCommand:
        {
            TypedMessageData<Vec3> *msgData = (TypedMessageData<Vec3> *)msg->pdata;
            if(msgData)
            {
                P2PStreamer::getInstance()->SendRotateCommand(msgData->data());
            }
            delete msgData;
            break;
        }
        case Cmd::SendLinkURLCommand:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                P2PStreamer::getInstance()->SendURLCommand(msgData->data().c_str());
            }
            delete msgData;
            break;
        }
        case Cmd::DecodeEffect:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                UIMessage* message = new UIMessage(UIMessage::Cmd::DecodeEffect);
                message->data = msgData->data();
                UIMessageCallback(message);
            }
            delete msgData;
            break;
        }
        case Cmd::DecodeEmoticon:
        {
            TypedMessageData<int> *msgData = (TypedMessageData<int> *)msg->pdata;
            if(msgData)
            {
                UIMessage* message = new UIMessage(UIMessage::Cmd::DecodeEmoticon);
                message->arg1 = msgData->data();
                UIMessageCallback(message);
            }
            delete msgData;
            break;
        }
        case Cmd::DecodeSticker:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                UIMessage* message = new UIMessage(UIMessage::Cmd::DecodeSticker);
                message->data = msgData->data();
                UIMessageCallback(message);
            }
            delete msgData;
            break;
        }
        case Cmd::DecodeRotate:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                UIMessage* message = new UIMessage(UIMessage::Cmd::DecodeRotate);
                message->data = msgData->data();
                UIMessageCallback(message);
            }
            delete msgData;
            break;
        }
        case Cmd::EncodingBitrate:
        {
            TypedMessageData<int> *msgData = (TypedMessageData<int> *)msg->pdata;
            if(msgData)
            {
                UIMessage* message = new UIMessage(UIMessage::Cmd::EncodingBitrate);
                message->arg1 = msgData->data();
                UIMessageCallback(message);
            }
            delete msgData;
            break;
        }
        case Cmd::LinkURL:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                UIMessage* message = new UIMessage(UIMessage::Cmd::LinkURL);
                message->data = msgData->data();
                UIMessageCallback(message);
            }
            delete msgData;
            break;
        }
        case Cmd::StartRecording:
        {
            TypedMessageData<std::string> *msgData = (TypedMessageData<std::string> *)msg->pdata;
            if(msgData)
            {
                P2PStreamer::getInstance()->StartRecording(msgData->data());
            }
            delete msgData;
        	break;
        }
        case Cmd::StopRecording:
        {
        	P2PStreamer::getInstance()->StopRecording();
        	break;
        }
        case Cmd::StartRecordingGIF:
        {
            TypedMessageData<const char*> *msgData = (TypedMessageData<const char*> *)msg->pdata;
            if(msgData)
            {
                P2PStreamer::getInstance()->StartRecordingGIF(msgData->data());
            }
            delete msgData;
            break;
        }
        case Cmd::StopRecordingGIF:
        {
            P2PStreamer::getInstance()->StopRecordingGIF();
            break;
        }
    }
}

void CallStateHandler::HandleXmppMessage(InviteEventData* newInvitation)
{
    switch(newInvitation->what)
    {
        case InviteEventData::Type::Recv:
        {
            std::string username = IdentityManager::parseUserID(newInvitation->data);
            Device::searchContact(username.c_str());
            break;
        }
        case InviteEventData::Type::Refusal:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::Invite);
            message->arg1 = (int)InviteEventData::Type::Refusal;
            UIMessageCallback(message);
            break;
        }
        case InviteEventData::Type::Busy:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::Invite);
            message->arg1 = (int)InviteEventData::Type::Busy;
            UIMessageCallback(message);
            break;
        }
        case InviteEventData::Type::Cancel:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::Invite);
            message->arg1 = (int)InviteEventData::Type::Cancel;
            UIMessageCallback(message);
            break;
        }
        case InviteEventData::Type::Message:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::Invite);
            message->arg1 = (int)InviteEventData::Type::Message;
            UIMessageCallback(message);
            break;
        }
        case InviteEventData::Type::Error:
        {
            UIMessage* message = new UIMessage(UIMessage::Cmd::Invite);
            message->arg1 = (int)InviteEventData::Type::Error;
            UIMessageCallback(message);
            break;
        }
        default:
            break;
    }
	delete newInvitation;
}

void CallStateHandler::OnDeviceMessage(Device::Res response, void* data, unsigned char* photo, int length)
{
    switch (response)
    {
        case Device::Res::CheckContactList:
        {
            std::vector<std::string> *msgData = (std::vector<std::string> *)data;
            if(msgData==nullptr)
            {
                CCLOG("choice item is nullptr");
                break;
            }
            /*
            else if(msgData->size()==2) // A fist item of the list is a person's name.
            {
                SendMessage(CallStateHandler::Cmd::Invite, msgData->back());
                
                UIMessage* message = new UIMessage(UIMessage::Cmd::Invite);
                message->arg1 = (int)InviteEventData::Type::Send;
                cocos2d::Image *imf=nullptr;
                if(photo)
                {
                    imf =new cocos2d::Image();
                    imf->retain();
                    imf->initWithImageData(photo, length);
                    delete photo;
                }
                
                message->extra = new ContactInfo(msgData->front(), imf);
                UIMessageCallback(message);
                delete msgData;
            }
            */
            else if(msgData->size()>1)
            {
                UIMessage* message1 = new UIMessage(UIMessage::Cmd::ContactList);
                message1->extra = new ContactInfo(msgData->front(), photo, length);
                message1->bundle = msgData;
                UIMessageCallback(message1);
            }
            break;
        }
            
        case Device::Res::SearchContact:
        {
            std::vector<std::string> *msgData = (std::vector<std::string> *)data;
            if(msgData!=nullptr)
            {
                UIMessage* message = new UIMessage(UIMessage::Cmd::Invite);
                message->arg1 = (int)InviteEventData::Type::Recv;
                message->extra = new ContactInfo(msgData->front(), photo, length);
                UIMessageCallback(message);
                delete msgData;
                break;
            }
        }
    }
}

void CallStateHandler::SendHttpRequest(std::string host, int port, std::string path, std::string tag)
{
    std::string url = host+":"+std::to_string(port)+path;

    network::HttpRequest* request = new (std::nothrow) network::HttpRequest();
    request->setUrl(url.c_str());
    request->setRequestType(network::HttpRequest::Type::GET);
    request->setResponseCallback(CC_CALLBACK_2(CallStateHandler::OnHttpResponse, this));
    request->setTag(tag.c_str());
    
    network::HttpClient::getInstance()->send(request);
    request->release();
}

void CallStateHandler::OnHttpResponse(network::HttpClient *sender, network::HttpResponse *response)
{
    
    std::string result_str;
    long statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %ld", statusCode);
    CCLOG("response code: %ld", statusCode);
    
    if (!response->isSucceed())
    {
        CCLOG("http error buffer: %s", result_str.c_str());
        return;
    }
    if(response->getHttpRequest()->getTag()==nullptr)
    {
        CCLOG("getHttpRequest()->getTag() is nullptr");
    }
    std::string tag = response->getHttpRequest()->getTag();
    if(!tag.find("signup"))
    {
        std::vector<char> *buffer = response->getResponseData();
        std::string result_str(buffer->begin(),buffer->end());
        if(!result_str.find("<result>ok</result>"))
        {
            CCLOG("success to sign up...");
            UserDefault::getInstance()->setStringForKey("loginid", _signupInfo.getUserId());
            UserDefault::getInstance()->setStringForKey("loginpassword", _signupInfo.getPassword());
            UserDefault::getInstance()->setStringForKey("registerid", _signupInfo.getRegisterId());
            _loginId = _signupInfo.getUserId();
            _loginPassword = _signupInfo.getPassword();
            SendEmptyMessage(Cmd::SignUpSuccess);
        }
        else if(!result_str.find("<error>"))
        {
            CCLOG("fail to sign up>>>");
            CCLOG("%s", result_str.c_str());
            SendEmptyMessage(Cmd::SignUpFail);
        }
        
    }
    else if(!tag.find("delete"))
    {
        std::vector<char> *buffer = response->getResponseData();
        std::string result_str(buffer->begin(),buffer->end());
        if(!result_str.find("<result>ok</result>"))
        {
            CCLOG("success to delete account");
            UserDefault::getInstance()->setStringForKey("firsttime", "");
            UserDefault::getInstance()->setStringForKey("loginid", "");
            UserDefault::getInstance()->setStringForKey("loginpassword", "");
            UserDefault::getInstance()->setStringForKey("registerid", "");
            SendEmptyMessage(Cmd::DeleteAccountSuccess);
        }
        else if(!result_str.find("<error>"))
        {
            CCLOG("fail to delete account");
            CCLOG("%s", result_str.c_str());
            SendEmptyMessage(Cmd::DeleteAccountFail);
        }
    }
    return;
}

void CallStateHandler::SendEmptyMessage(CallStateHandler::Cmd cmd)
{
    _workerThread->Post(this, (unsigned int)cmd);
}

void CallStateHandler::SendMessage(CallStateHandler::Cmd cmd, bool userData)
{
    TypedMessageData<bool> *msgData = new TypedMessageData<bool>(userData);
    _workerThread->Post(this, (unsigned int)cmd, msgData);
}

void CallStateHandler::SendMessage(CallStateHandler::Cmd cmd, int userData)
{
    TypedMessageData<int> *msgData = new TypedMessageData<int>(userData);
    _workerThread->Post(this, (unsigned int)cmd, msgData);
}

void CallStateHandler::SendMessage(CallStateHandler::Cmd cmd, const char* userData)
{
    TypedMessageData<const char*> *msgData = new TypedMessageData<const char*>(userData);
    _workerThread->Post(this, (unsigned int)cmd, msgData);
}

void CallStateHandler::SendMessage(CallStateHandler::Cmd cmd, std::string userData)
{
    TypedMessageData<std::string> *msgData = new TypedMessageData<std::string>(userData);
    _workerThread->Post(this, (unsigned int)cmd, msgData);
}

void CallStateHandler::SendMessage(CallStateHandler::Cmd cmd, std::vector<std::string>* bundle)
{
    TypedMessageData<std::vector<std::string>*> *msgData = new TypedMessageData<std::vector<std::string>*>(bundle);
    _workerThread->Post(this, (unsigned int)cmd, msgData);
}

void CallStateHandler::SendMessage(CallStateHandler::Cmd cmd, Vec3 userData)
{
    TypedMessageData<Vec3> *msgData = new TypedMessageData<Vec3>(userData);
    _workerThread->Post(this, (unsigned int)cmd, msgData);
}

void CallStateHandler::SendXmppMessage(CallStateHandler::Cmd cmd, InviteEventData* data)
{
    TypedMessageData<InviteEventData*> *msgData = new TypedMessageData<InviteEventData*>(data);
    _workerThread->Post(this, (unsigned int)cmd, msgData);
}


void CallStateHandler::SendEmptyMessageDelayed(CallStateHandler::Cmd cmd, int delay)
{
    _workerThread->PostDelayed(delay, this, (unsigned int)cmd);
}
