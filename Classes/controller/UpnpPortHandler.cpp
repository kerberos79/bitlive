//
//  UpnpPortHandler.cpp
//  Vista
//
//  Created by kerberos on 2/2/15.
//
//

#include "UpnpPortHandler.h"

UpnpPortHandler::UpnpPortHandler()
:_workerThread(nullptr),
supportUpnp(false),
mappedPort(0)
{
    _workerThread = new Thread();
    _workerThread->Start();
}

UpnpPortHandler::~UpnpPortHandler()
{
}

UpnpPortHandler* UpnpPortHandler::mUpnpPortHandler = nullptr;

UpnpPortHandler* UpnpPortHandler::getInstance()
{
    if(mUpnpPortHandler==nullptr)
    {
        mUpnpPortHandler = new UpnpPortHandler();
    }
    return mUpnpPortHandler;
}


void UpnpPortHandler::OnMessage(Message *msg)
{
    switch ((UpnpPortHandler::Cmd)msg->message_id)
    {
        case Cmd::Start:
        {
            RequestMapping();
            break;
        }
        case Cmd::Release:
        {
            RemoveMapping();
            break;
        }
    }
}

void UpnpPortHandler::Request()
{
    supportUpnp = false;
    internalPort = "7002";
    protocolType = "TCP";
    
     _workerThread->Post(this, (unsigned int)Cmd::Start);
}

void UpnpPortHandler::Release()
{
    RemoveMapping();
//    _workerThread->Post(this, (unsigned int)Cmd::Release);
}

void getRequestPort(const char* ip, char* requestPort)
{
    std::string temp = ip;
    
    std::vector<std::string> tokens;
    const std::string delimiters = ".";
    
    Tokenizer::tokenize(temp, tokens, delimiters);
    std::string lastNumber = tokens.at(3);
    int port = 7000 + std::stoi( lastNumber );
    sprintf(requestPort, "%d", port);
}

bool UpnpPortHandler::IsSupport() {
    criticalsection.Enter();
    if(supportUpnp)
    {
        criticalsection.Leave();
        return true;
    }
    criticalsection.Leave();
    return false;
}

SocketAddress& UpnpPortHandler::GetAddress()
{
    return externalAddress;
}

bool UpnpPortHandler::GetUpnpInfo(std::string& ip, std::string& port) {
    criticalsection.Enter();
    if(supportUpnp)
    {
        ip = mappedAddr;
        port = externalPort;
        criticalsection.Leave();
        return true;
    }
    criticalsection.Leave();
    return false;
}

bool UpnpPortHandler::Compare(std::string& dest_ip)
{
    criticalsection.Enter();
    if(supportUpnp)
    {
        if(!dest_ip.compare(mappedAddr))
            criticalsection.Leave();
        return false;
    }
    criticalsection.Leave();
    return true;
}


void UpnpPortHandler::RequestMapping()
{

	struct UPNPDev * devlist = 0;
	int i;
	const char * rootdescurl = 0;
	const char * multicastif = 0;
	const char * minissdpdpath = 0;
	int error = 0;
	int ipv6 = 0;
	const char * description = 0;
	if ((devlist = upnpDiscover(2000, multicastif, minissdpdpath,
		0/*sameport*/, ipv6, &error)) != 0)
	{
		struct UPNPDev * device;
		if (devlist)
		{
			CCLOG("List of UPNP devices found on the network :");
			for (device = devlist; device; device = device->pNext)
			{
				CCLOG(" desc: %s st: %s",
					device->descURL, device->st);
			}
		}
		else
		{
			CCLOG("upnpDiscover() error code=%d", error);
		}

		i = 1;
		if ((rootdescurl && UPNP_GetIGDFromUrl(rootdescurl, &urls, &data, lanaddr, sizeof(lanaddr)))
			|| (i = UPNP_GetValidIGD(devlist, &urls, &data, lanaddr, sizeof(lanaddr))))
		{
			switch (i) {
			case 1:
				CCLOG("Found valid IGD : %s", urls.controlURL);
				break;
			case 2:
				CCLOG("Found a (not connected?) IGD : %s", urls.controlURL);
				CCLOG("Trying to continue anyway");
				break;
			case 3:
				CCLOG("UPnP device found. Is it an IGD ? : %s", urls.controlURL);
				CCLOG("Trying to continue anyway");
				break;
			default:
				CCLOG("Found device (igd ?) : %s", urls.controlURL);
				CCLOG("Trying to continue anyway");
			}
			CCLOG("Local LAN ip address : %s", lanaddr);

			criticalsection.Enter();
			supportUpnp = true;
			criticalsection.Leave();
		}
		else
		{
			CCLOG("No valid UPNP Internet Gateway Device found.");
		}
		freeUPNPDevlist(devlist); devlist = 0;
	}
	else
	{
		CCLOG("No IGD UPnP Device found on the network !");
	}
}

rtc::SocketAddress& UpnpPortHandler::GetExternalAddress(int internal_port)
{
	int r;
	char internal_port_str[10];
	sprintf(internal_port_str, "%d", internal_port);
	getRequestPort(lanaddr, externalPort);
	while ((r = SetRedirectAndTest(&urls, &data,
		lanaddr, internal_port_str,
		externalPort,
		"TCP",
		"0",
		lanaddr,
		mappedAddr)) == 718) {
		int value = atoi(externalPort);
		if (value>32000) {
			r = UPNPCOMMAND_UNKNOWN_ERROR;
			break;
		}
		sprintf(externalPort, "%d", value + 256);
	}
	if (r == UPNPCOMMAND_SUCCESS) {
        externalAddress.SetIP(mappedAddr);
        mappedPort = atoi(externalPort);
        externalAddress.SetPort(mappedPort);
		CCLOG("UPNPCOMMAND_SUCCESS : %s:%s >> %s", lanaddr, internal_port_str, externalAddress.ToString().c_str());
	}
	else {
		FreeUPNPUrls(&urls);
	}

	return externalAddress;
}

void UpnpPortHandler::RemoveMapping() {
    criticalsection.Enter();
    if(supportUpnp && mappedPort>0) {
        RemoveRedirect(&urls, &data, externalPort, "TCP");
        CCLOG("RemoveRedirect :%s:%s",mappedAddr,externalPort);
        FreeUPNPUrls(&urls);
        mappedPort = 0;
    }
    criticalsection.Leave();
}
