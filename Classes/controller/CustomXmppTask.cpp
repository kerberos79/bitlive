#include "CustomXmppTask.h"

using namespace cricket;

extern const buzz::StaticQName QN_GINGLE_P2P_CANDIDATE;
extern const buzz::StaticQName QN_GINGLE_P2P_TRANSPORT;

namespace cricket {
CustomXmppTask::CustomXmppTask(buzz::XmppTaskParentInterface* parent) :
		XmppTask(parent, buzz::XmppEngine::HL_ALL) {
}

CustomXmppTask::~CustomXmppTask() {

}

bool CustomXmppTask::HandleStanza(const buzz::XmlElement* stanza) {
	LOG(LS_INFO)
			<< "CustomXmppTask::HandleStanza(const buzz::XmlElement* stanza)";
	if (stanza->Name() == buzz::QN_MESSAGE) {
        if (stanza->FirstNamed(QN_GINGLE_P2P_CANDIDATE)) {
			LOG(LS_INFO) << "got a Candidate request. \n";
			SignalHandleCandidate(stanza);
		}
		else if(stanza->FirstNamed(QN_GINGLE_P2P_TRANSPORT)) {
			LOG(LS_INFO) << "got a Candidate request. \n";
			SignalHandleTransport(stanza);
		}
		else {
			LOG(LS_INFO) << "got a message. \n";
			LOG(INFO)<<stanza->Str();
			SignalHandleChat(stanza);
		}
	}
	else {
		LOG(LS_INFO) << "got a message. \n";
		LOG(INFO)<<stanza->Str();
	}

	return true;
}

int CustomXmppTask::ProcessStart() {
	return 0;
}
}


