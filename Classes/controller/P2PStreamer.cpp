//
//  P2PStreamer.cpp
//  Vista
//
//  Created by kerberos on 2/27/15.
//
//

#include "P2PStreamer.h"

P2PStreamer::P2PStreamer()
:P2PStreamEngine() ,
_voiceDecoder(nullptr),
_vpDecoder(nullptr) ,
_previousState(EncState::Idle) ,
_encodingState(EncState::Idle) ,
_voiceRecordState(EncState::Idle),
_videoDelegate(nullptr),
_audioDelegate(nullptr),
_createdCodec(false)
{
    _voiceDecoder = new VoiceDecoder();
    _voiceEncoder = new VoiceEncoder();
    _vpDecoder = new VPDecoder();
    _vpEncoder = new VPEncoder();
    _videoEncodingBitrate = 1100;//300, 2000;
    _audioDelegate = new AudioInputDeletegate();
    _videoDelegate = new VideoInputDeletegate();
    
}

P2PStreamer::~P2PStreamer()
{
    if(_createdCodec)
        ReleaseCodec();
    if(_voiceDecoder)
    {
        delete _voiceDecoder;
    }
    if(_voiceEncoder)
        delete _voiceEncoder;
    if(_vpDecoder)
        delete _vpDecoder;
    if(_vpEncoder)
        delete _vpEncoder;
    if(_videoDelegate)
        delete _videoDelegate;
    if(_audioDelegate)
        delete _audioDelegate;
    if(_bitrateMonitor)
        delete _bitrateMonitor;
    stream_.clear();
}

P2PStreamer* P2PStreamer::mP2PStreamer = nullptr;

P2PStreamer* P2PStreamer::getInstance()
{
    if(mP2PStreamer==nullptr)
    {
        mP2PStreamer = new P2PStreamer();
    }
    return mP2PStreamer;
}

void P2PStreamer::InitScreen(MainScene *mainScene, int previewWidth, int previewHeight)
{
    _decodeScreen = mainScene->getDecodeScreen();
    _previewScreen = mainScene->getPreviewScreen();
    _previewWidth = previewWidth;
    _previewHeight = previewHeight;
    _previewScreen->previewEncodingDelegate = CC_CALLBACK_3(P2PStreamer::EncodePreviewFrame, this);
    
    _videoDelegate->func = CC_CALLBACK_3(PreviewScreen::updateYV12Frame, _previewScreen);
    _videoDelegate->faceDetect = CC_CALLBACK_2(MainScene::OnFaceDetectCallback, mainScene);
    _videoDelegate->completeRecordCallback = CC_CALLBACK_1(MainScene::OnCompleteRecordCallback, mainScene);
    _videoDelegate->getVideoFrameRGBA = CC_CALLBACK_1(PreviewScreen::getVideoFrameRGBA, _previewScreen);
    _videoDelegate->getVideoFrameBGRA = CC_CALLBACK_1(PreviewScreen::getVideoFrameBGRA, _previewScreen);
    _videoDelegate->getVideoFrameNV21 = CC_CALLBACK_2(PreviewScreen::getVideoFrameNV21, _previewScreen);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    _previewScreen->setEncodingFrameRate(14);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    _previewScreen->setEncodingFrameRate(18);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	_previewScreen->setEncodingFrameRate(20);
#endif
    Device::startCameraPreview(_videoDelegate, previewWidth);
}

void P2PStreamer::InitAudio()
{
    Device::createAudioIO(VOICE_SAMPLE_RATE, VOICE_CHANNELS, VOICE_DURATION);
}

void P2PStreamer::Start(cricket::Connection *conn)
{
    init();
    conn_ = conn;
    _getConnectCommand = _getConnectedCommand = false;
    conn_->SignalReadPacket.connect(this, &P2PStreamer::OnReadPacket);
    
    P2PStreamer::getInstance()->InitAudio();
    CreateCodec();
    _bitrateMonitor = BitrateMonitor::create();
}

void P2PStreamer::RequestStopEncoding()
{
    _decodeScreen->unscheduleUpdate();
    _voiceCommandLock.Enter();
    if(_voiceRecordState == EncState::Idle)
    {
        _voiceCommandLock.Leave();
        RequestEncoder(EncReqType::Stop);
    }
    else
    {
        _voiceRecordState = EncState::Stop;
        _voiceCommandLock.Leave();
    }
}

void P2PStreamer::StartRecording(std::string filename)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    _audioDelegate->func = CC_CALLBACK_1(P2PStreamer::EncodeVoiceFrame, this);
    Device::startVoiceRecorderEx(_audioDelegate);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    _audioDelegate->func = CC_CALLBACK_1(P2PStreamer::EncodeVoiceFrame, this);
    Device::startVoiceRecorder(_audioDelegate);
#endif
    Device::startVideoRecord(filename.c_str());
    _voiceCommandLock.Enter();
    _voiceRecordState = EncState::Recording;
    _voiceCommandLock.Leave();
    RequestEncoder(EncReqType::StartRecord);
}

void P2PStreamer::StopRecording()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    _voiceCommandLock.Enter();
    _voiceRecordState = EncState::StopRecording;
    _voiceCommandLock.Leave();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    _voiceCommandLock.Enter();
    if(_voiceRecordState == EncState::Recording)
    {
        _voiceRecordState = EncState::StopRecording;
        _voiceCommandLock.Leave();
    }
    else
    {
        _voiceCommandLock.Leave();
        RequestEncoder(EncReqType::StopRecord);
    }
#endif
}

void P2PStreamer::StartRecordingGIF(const char* filename)
{
    GifEncoder::getInstance()->create(filename, _previewWidth, _previewHeight, 20);
    GifEncoder::getInstance()->setVideoInputDeletegate(_videoDelegate);
    RequestEncoder(EncReqType::StartRecordGIF);
}

void P2PStreamer::StopRecordingGIF()
{
    RequestEncoder(EncReqType::StopRecordGIF);
}

void P2PStreamer::Release()
{
    RequestEncoder(EncReqType::Release);
    Device::releaseVoiceRecorder();
}

void P2PStreamer::OnReadPacket(cricket::Connection *connection,
                               const char *data, size_t len, const rtc::PacketTime& time) {
    unsigned char ret;
    ret = onRecvPacket((unsigned char*)data, len, &stream_);
    switch(ret) {
        case STREAM_COMMAND:
        {
            ParseCommand((const char*)stream_.buffer);
            break;
        }
        case STREAM_OPUS:
        {
            _voiceDecoder->decodeStream(&stream_);
            break;
        }
        case STREAM_MP3:
        {
            break;
        }
        case STREAM_ARTWORK:
        {
            break;
        }
        case STREAM_VP8:
        {
            if(_vpDecoder->decodeFrame(&stream_)==0) {
                if(_bitrateMonitor->add(time.timestamp))
                {
                    RequestBitrateCommand(_bitrateMonitor->_requestBitrate);
                }
            }
            else {
                requestFrame(gold_recv_helper);
            }
            break;
        }
        case STREAM_GOLD:
        {
            if(_vpDecoder->decodeGFrame(&stream_)==0) {
                _bitrateMonitor->start(time.timestamp);
            }
            else
                requestFrame(gold_recv_helper);
            break;
        }
        case STREAM_RECOVERY:
        {
            if(stream_.buffer[stream_.length-2] == STREAM_GOLD) {
                if(stream_.buffer[stream_.length-3]==REQUEST_GOLDFRAME) {
                    RequestEncoder(EncReqType::RequestKeyFrame, stream_.buffer[1], stream_.buffer[0]);
                }
                else if(stream_.buffer[stream_.length-3]==REQUEST_FRAME_COMPLETE) {
                    RequestEncoder(EncReqType::CompleteKeyFrame, stream_.buffer[0]);
                }
            }
            break;
        }
        case SOCKET_CLOSED:
//        case STREAM_SKIP:
        default:
            break;
    }
}

void P2PStreamer::CreateCodec()
{
    if(_createdCodec)
        ReleaseCodec();
    _vpDecoder->create();
    _decodeScreen->updateDecodeFrame = CC_CALLBACK_1(VPDecoder::udpateDecodeFrame, _vpDecoder);
    _decodeScreen->scheduleUpdate();
	_voiceDecoder->create(VOICE_BUFFER_SIZE, VOICE_SAMPLE_RATE, VOICE_CHANNELS);
    _voiceEncoder->create(VOICE_DURATION, VOICE_SAMPLE_RATE, VOICE_CHANNELS);
    _createdCodec = true;
}

void P2PStreamer::ReleaseCodec()
{
    _vpDecoder->release();
    _voiceDecoder->release();
    _createdCodec = false;
}

bool P2PStreamer::GetIntValueFromJSON(std::string &str, const char* param, int* value)
{
    mJsonDoc.Parse<0>(str.c_str());
    if (mJsonDoc.HasParseError())
    {
        CCLOG("GetParseError %s\n", mJsonDoc.GetParseError());
        return false;
    }
    *value = DICTOOL->getIntValue_json(mJsonDoc, param);
    return true;
}

const char* P2PStreamer::GetStringValueFromJSON(std::string &str, const char* param)
{
    mJsonDoc.Parse<0>(str.c_str());
    if (mJsonDoc.HasParseError())
    {
        CCLOG("GetParseError %s\n", mJsonDoc.GetParseError());
        return nullptr;
    }
    return DICTOOL->getStringValue_json(mJsonDoc, param);
}

void P2PStreamer::ParseCommand(std::string str)
{
    size_t pos = 0;
    char command;
    std::string data;
    if((pos = str.find('{')) <= str.length())
    {
        command = str.front();
        data = str.substr(1, str.length());
        
        switch(command)
        {
            case COMMAND_CONNECT:
            {
                if(!_getConnectCommand)
                {
                    int bitrate;
                    bool result = GetIntValueFromJSON(data, "bitrate", &bitrate);
                    _bitrateMonitor->setBitrte(bitrate);
                    CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::ConnectionSuccess, data);
                    _getConnectCommand = true;
                }
                SendConnectedCommand();
                break;
            }
            case COMMAND_CONNECTED:
            {
                if(!_getConnectedCommand)
                {
                    _audioDelegate->func = CC_CALLBACK_1(P2PStreamer::EncodeVoiceFrame, this);
                    Device::startVoiceRecorder(_audioDelegate);
                    _voiceCommandLock.Enter();
                    _voiceRecordState = EncState::Start;
                    _voiceCommandLock.Leave();
                    RequestEncoder(EncReqType::Start);
                    SignalConnectionCompleted();
                    _getConnectedCommand = true;
                }
                break;
            }
            case COMMAND_DISCONNECTED:
            {
                CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Disconnected);
                break;
            }
            case COMMAND_STOP :
            {
                break;
            }
            case COMMAND_EFFECT:
            {
                std::string type = GetStringValueFromJSON(data, "type");
                CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::DecodeEffect, type);
                break;
            }
            case COMMAND_EMOTICON:
            {
                int index;
                bool result = GetIntValueFromJSON(data, "index", &index);
                if(result)
                {
                    CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::DecodeEmoticon, index);
                }
                break;
            }
            case COMMAND_STICKER:
            {
                std::string param = GetStringValueFromJSON(data, "param");
                CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::DecodeSticker, param);
                break;
            }
            case COMMAND_ROTATE:
            {
                std::string degree = GetStringValueFromJSON(data, "degree");
                CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::DecodeRotate, degree);
                break;
            }
            case COMMAND_CHAT:
            {
                break;
            }
            case COMMAND_URL:
            {
                std::string url = GetStringValueFromJSON(data, "url");
                CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::LinkURL, url);
                break;
            }
            case COMMAND_BITRATE:
            {
                int rate;
                bool result = GetIntValueFromJSON(data, "rate", &rate);
                if(result)
                {
                    _bitrateMonitor->setBitrte(rate);
                }
                break;
            }
            case COMMAND_REQUEST_BITRATE:
            {
                int rate;
                bool result = GetIntValueFromJSON(data, "rate", &rate);
                if(result)
                {
                    SendBitrateCommand(rate);
                    CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::EncodingBitrate, rate);
                }
                break;
            }
        }
    }
}

void P2PStreamer::SendConnectCommand()
{
    const char* effect = _previewScreen->getEffect();
    unsigned int bitrate = _previewScreen->getBitrate();
    
    sprintf(_commandBuffer, "%c{\"effect\":\"%s\", \"bitrate\":%d}", COMMAND_CONNECT, effect, bitrate);
    sendCommand(_commandBuffer);
}

void P2PStreamer::SendConnectedCommand()
{
    char data[] = " {}";
    data[0] = COMMAND_CONNECTED;
    sendCommand(data);
}

void P2PStreamer::SendDisconnectedCommand()
{
    char data[] = " {}";
    data[0] = COMMAND_DISCONNECTED;
    sendCommand(data);
}

void P2PStreamer::SendStopCommand()
{
    char data[] = " {}";
    data[0] = COMMAND_STOP;
    sendCommand(data);
}

void P2PStreamer::SendEffectCommand(const char* type)
{
    sprintf(_commandBuffer, "%c{\"type\":\"%s\"}", COMMAND_EFFECT, type);
    sendCommand(_commandBuffer);
}

void P2PStreamer::SendEmoticonCommand(int index)
{
    sprintf(_commandBuffer, "%c{\"index\":%d}", COMMAND_EMOTICON, index);
    sendCommand(_commandBuffer);
}

void P2PStreamer::SendStickerCommand(std::string param)
{
    sprintf(_commandBuffer, "%c{\"param\":\"%s\"}", COMMAND_STICKER, param.c_str());
    sendCommand(_commandBuffer);
}

void P2PStreamer::SendRotateCommand(Vec3 degree)
{
    sprintf(_commandBuffer, "%c{\"degree\":\"%d,%d,%d\"}", COMMAND_ROTATE, (int)degree.x, (int)degree.y, (int)degree.z);
    sendCommand(_commandBuffer);
}

void P2PStreamer::SendChatCommand(const char* msg)
{
	char* data = (char*)malloc(strlen(msg) + 10);
    sprintf(data, "%c{\"msg\":\"%s\"}", COMMAND_CHAT, msg);
    sendCommand(data);
	delete data;
}

void P2PStreamer::SendURLCommand(const char* url)
{
    char* data = (char*)malloc(strlen(url) + 16);
    sprintf(data, "%c{\"url\":\"%s\"}", COMMAND_URL, url);
    sendCommand(data);
    delete data;
}

void P2PStreamer::SendBitrateCommand(int bitrate)
{
    char* data = (char*)malloc(16);
    sprintf(data, "%c{\"rate\":%d}", COMMAND_BITRATE, bitrate);
    sendCommand(data);
    delete data;
}

void P2PStreamer::RequestBitrateCommand(int bitrate)
{
    char* data = (char*)malloc(16);
    sprintf(data, "%c{\"rate\":%d}", COMMAND_REQUEST_BITRATE, bitrate);
    sendCommand(data);
    delete data;
}

int P2PStreamer::EncodeVoiceFrame(short* buffer)
{
    EncState state;
    _voiceCommandLock.Enter();
    state = _voiceRecordState;
    _voiceCommandLock.Leave();
    
    switch(state)
    {
        case EncState::Idle:
        {
            break;
        }
        case EncState::Start:
        {
            if(_voiceEncoder->encodeFrame((const short*) buffer) > 0) {
                sendSinglePacket(STREAM_OPUS,
                                 (unsigned char *) _voiceEncoder->encOutputBuffer,
                                 _voiceEncoder->encOutputSize);
            }
            break;
        }
        case EncState::Stop:
        {
            _voiceEncoder->release();
            RequestEncoder(EncReqType::Stop);
            _voiceRecordState = EncState::Idle;
            return 1;
        }
        case EncState::Recording:
        {
        	return 100;
        }
        case EncState::StopRecording:
        {
            _voiceRecordState = EncState::Idle;
            RequestEncoder(EncReqType::StopRecord);
        	return 101;
        }
    }
    return 0;
}

int P2PStreamer::EncodePreviewFrame(unsigned char *y, unsigned char *u, unsigned char *v)
{
    HandleEncodeRequest();
    
    switch(_encodingState)
    {
        case EncState::Idle:
        {
            break;
        }
        case EncState::Start:
        {
            _startedStream = true;
            _sendLastPacketCount = 0;
            _encodingFrameCount = 0;
            
            if( (_vpEncoder->create(_previewWidth, _previewHeight, _videoEncodingBitrate))<0)
            {
                CCLOG("fail to VpEncoder");
                _previousState = EncState::Idle;
                _encodingState = EncState::Idle;
                break;
            }
            if(!_vpEncoder->encodeKeyFrame(y, u, v)) {
                if (_vpEncoder->encOutputSize > 0) {
                    sendFramePacket(gold_send_helper, (unsigned char *) _vpEncoder->encOutputBuffer,
                                    _vpEncoder->encOutputSize, (unsigned char)(_encodingFrameCount & 0xff), (unsigned char)(_encodingLevel & 0xff));
                }
            }
            _previousState = EncState::Encoding;
            _encodingState = EncState::SendLastPacket;
            break;
        }
        case EncState::Encoding:
        {
            if(!_vpEncoder->encodeFrame(y, u, v)) {
                if (_vpEncoder->encOutputSize > 0) {
                    sendMultiPacket(STREAM_VP8,
                                    (unsigned char *) _vpEncoder->encOutputBuffer,
                                    _vpEncoder->encOutputSize);
                }
            }
            break;
        }
        case EncState::Keyframe:
        {
            if(!_vpEncoder->encodeKeyFrame(y, u, v)) {
                if (_vpEncoder->encOutputSize > 0) {
                    sendFramePacket(gold_send_helper, (unsigned char *) _vpEncoder->encOutputBuffer,
                                    _vpEncoder->encOutputSize, (unsigned char)(_encodingFrameCount & 0xff), (unsigned char)(_encodingLevel & 0xff));
                }
            }
            _previousState = EncState::Encoding;
            _encodingState = EncState::SendLastPacket;
            break;
        }
        case EncState::Resampling:
        {
            // TODO : resampling
            _previousState = EncState::Encoding;
            _encodingState = EncState::SendLastPacket;
            break;
        }
        case EncState::SendLastPacket:
        {
            {
                sendLastFramePacket(gold_send_helper);
                CCLOG("SendLastPacket %d", _sendLastPacketCount++);
            }
            break;
        }
        case EncState::FlipCamera:
        {
            _encodingState = _previousState;
            UIMessage* message = new UIMessage(UIMessage::Cmd::FlippedCamera);
            CallStateHandler::getInstance()->UIMessageCallback(message);
            break;
        }
        case EncState::Stop:
        {
            _vpEncoder->release();
            _previousState = EncState::Idle;
            _encodingState = EncState::Idle;
            _startedStream = false;
            P2PChannelAllocator::getInstance()->SendEmptyMessage(P2PChannelAllocator::Cmd::Destroy);
            break;
        }
        case EncState::Release:
        {
            _vpEncoder->release();
            _previousState = EncState::Idle;
            _encodingState = EncState::Idle;
            _startedStream = false;
            CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Exit);
            return 1;
        }
        case EncState::Recording:
        {
        	return 100;
        }
        case EncState::StopRecording:
        {
            _encodingState = EncState::Idle;
        	return 101;
        }
        case EncState::RecordingGIF:
        {
            GifEncoder::getInstance()->encode();
            break;
        }
        case EncState::StopRecordingGIF:
        {
            GifEncoder::getInstance()->close();
            _encodingState = EncState::Idle;
            break;
        }
    }
    return 0;
}

void P2PStreamer::HandleEncodeRequest()
{
    _videoCommandLock.Enter();
    while(!_videoCommandQueue.empty())
    {
        EncReq* newReq = _videoCommandQueue.front();
        switch(newReq->what)
        {
            case EncReqType::Start:
            {
                _encodingState = EncState::Start;
                break;
            }
            case EncReqType::Pause:
            {
                _previousState = _encodingState;
                _encodingState = EncState::Idle;
                break;
            }
            case EncReqType::Resume:
            {
                _previousState = _encodingState;
                _encodingState = EncState::Encoding;
                break;
            }
            case EncReqType::RequestKeyFrame:
            {
                _encodingFrameCount = newReq->arg1;
//                requestLevel = newReq->arg2;
                _previousState = _encodingState;
                _encodingState = EncState::Keyframe;
                break;
            }
            case EncReqType::CompleteKeyFrame:
            {
                _sendLastPacketCount = 0;
                _encodingState = _previousState;
                break;
            }
            case EncReqType::Resampling:
            {
                int newFramerate = newReq->arg1;
                _previousState = _encodingState;
                _encodingState = EncState::Resampling;
                break;
            }
            case EncReqType::ChangeLevel:
            {
                SetEncodingLevel((unsigned char)newReq->arg1);
                _previousState = _encodingState;
                _encodingState = EncState::Resampling;
                break;
            }
            case EncReqType::FlipCamera:
            {
                _previousState = _encodingState;
                _encodingState = EncState::FlipCamera;
                break;
            }
            case EncReqType::Stop:
            {
                while(!_videoCommandQueue.empty())
                {
                    EncReq* newReq = _videoCommandQueue.front();
                    _videoCommandQueue.pop_front();
                    delete newReq;
                }
                _encodingState = EncState::Stop;
                _videoCommandLock.Leave();
                return;
            }
            case EncReqType::Release:
            {
                while(!_videoCommandQueue.empty())
                {
                    EncReq* newReq = _videoCommandQueue.front();
                    _videoCommandQueue.pop_front();
                    delete newReq;
                }
                _encodingState = EncState::Release;
                _videoCommandLock.Leave();
                return;
            }
            case EncReqType::StartRecord:
            {
                _encodingState = EncState::Recording;
                break;
            }
            case EncReqType::StopRecord:
            {
                if(_encodingState == EncState::Recording)
                    _encodingState = EncState::StopRecording;
                break;
            }
            case EncReqType::StartRecordGIF:
            {
                _encodingState = EncState::RecordingGIF;
                break;
            }
            case EncReqType::StopRecordGIF:
            {
                _encodingState = EncState::StopRecordingGIF;
                break;
            }
        }
        _videoCommandQueue.pop_front();
        delete newReq;
    }
    _videoCommandLock.Leave();
}

void P2PStreamer::RequestEncoder(P2PStreamer::EncReqType what, int arg1, int arg2)
{
    _videoCommandLock.Enter();
    EncReq* newReq = new EncReq(what, arg1, arg2);
    _videoCommandQueue.push_back(newReq);
    _videoCommandLock.Leave();
}

void P2PStreamer::SetEncodingLevel(unsigned char level)
{
    _encodingLevel = level;
}

void P2PStreamer::Test1()
{
}