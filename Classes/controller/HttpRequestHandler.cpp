
//
//  HttpRequestHandler.cpp
//  Vista
//
//  Created by kerberos on 2/4/15.
//
//

#include "HttpRequestHandler.h"

HttpRequestHandler::HttpRequestHandler() {
    mRequest = new rtc::AsyncHttpRequest("http");
    mRequest->SignalWorkDone.connect(this,
                                    &HttpRequestHandler::OnRequestDone);
}

HttpRequestHandler::~HttpRequestHandler() {
    if(mRequest)
        delete mRequest;
}
 
HttpRequestHandler* HttpRequestHandler::mHttpRequestHandler = nullptr;

HttpRequestHandler* HttpRequestHandler::getInstance()
{
    if(mHttpRequestHandler==nullptr)
    {
        mHttpRequestHandler = new HttpRequestHandler();
    }
    return mHttpRequestHandler;
}

void HttpRequestHandler::SendGetRequest(const std::string& host, int port,
                      const std::string& path, const std::string& tag) {
    this->mTag = tag;
    mRequest->request().verb = rtc::HV_GET;
    mRequest->set_host(host);
    mRequest->set_port(port);
    mRequest->request().path = path;
    mRequest->response().document.reset(new rtc::MemoryStream());
    mRequest->Start();
}

void HttpRequestHandler::OnRequestDone(rtc::SignalThread* thread) {
    SignalHttpResponse(mTag, mRequest->response());
}