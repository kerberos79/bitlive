//
//  P2PStreamer.h
//  Vista
//
//  Created by kerberos on 2/27/15.
//
//

#ifndef __Vista__P2PStreamer__
#define __Vista__P2PStreamer__

#include <stdio.h>
#include <list>

#include "cocos2d.h"
#include "cocostudio/DictionaryHelper.h"
#include "webrtc/base/messagehandler.h"
#include "webrtc/base/thread.h"
#include "webrtc/base/criticalsection.h"
#include "controller/XmppMainHandler.h"
#include "stream/P2PStreamEngine.h"
#include "codec/VoiceDecoder.h"
#include "codec/VoiceEncoder.h"
#include "codec/VPDecoder.h"
#include "codec/VPEncoder.h"
#include "view/DecodeScreen.h"
#include "view/PreviewScreen.h"
#include "view/MainScene.h"
#include "util/BitrateMonitor.h"
#include "gif/GifEncoder.h"

USING_NS_CC;
using namespace cocostudio;
using namespace rtc;

#define VOICE_BITRATE_      128000
#define VOICE_DURATION      0.06
#define VOICE_SAMPLE_RATE   16000
#define VOICE_CHANNELS      1
#define VOICE_BUFFER_SIZE   8000

#define COMMAND_CONNECT         'A'
#define COMMAND_CONNECTED       'B'
#define COMMAND_DISCONNECTED    'C'
#define COMMAND_STOP            'D'
#define COMMAND_EFFECT          'E'
#define COMMAND_ROTATE          'F'
#define COMMAND_CHAT            'G'
#define COMMAND_URL             'H'
#define COMMAND_EMOTICON        'I'
#define COMMAND_STICKER         'J'
#define COMMAND_BITRATE         'K'
#define COMMAND_REQUEST_BITRATE 'L'

class PreviewScreen;
class DecodeScreen;
class MainScene;

class P2PStreamer : public P2PStreamEngine, public sigslot::has_slots<>{
public:
    
    enum class EncReqType : unsigned int
    {
        Start,
        Pause,
        Resume,
        RequestKeyFrame,
        CompleteKeyFrame,
        Resampling,
        ChangeLevel,
        FlipCamera,
        Stop,
        Release,
        StartRecord,
        StopRecord,
        StartRecordGIF,
        StopRecordGIF,
    };
    
    enum class EncState : unsigned int
    {
        Idle,
        Start,
        Encoding,
        Recording,
        StopRecording,
        RecordingGIF,
        StopRecordingGIF,
        Keyframe,
        Resampling,
        SendLastPacket,
        FlipCamera,
		Stop,
        Release,
    };
    
    class EncReq
    {
    public:
        EncReqType  what;
        int         arg1;
        int         arg2;
        EncReq(EncReqType a, int b, int c)
        {
            this->what = a;
            this->arg1 = b;
            this->arg2 = c;
        }
        ~EncReq(){}
    };
    
    P2PStreamer();
    ~P2PStreamer();
    static P2PStreamer* getInstance();
    void Start(cricket::Connection *conn);
    void Release();
    void RequestStopEncoding();
    int _videoEncodingBitrate;
    
    void InitScreen(MainScene *mainScene, int previewWidth, int previewHeight);
    void InitAudio();
    void SendConnectCommand();
    void SendConnectedCommand();
    void SendDisconnectedCommand();
    void SendStopCommand();
    void SendEffectCommand(const char* type);
    void SendEmoticonCommand(int index);
    void SendStickerCommand(std::string data);
    void SendRotateCommand(Vec3 degree);
    void SendChatCommand(const char* msg);
    void SendURLCommand(const char* url);
    void SendBitrateCommand(int bitrate);
    void RequestBitrateCommand(int bitrate);
    void Test1();
    void Test2();
    void RequestEncoder(P2PStreamer::EncReqType what, int arg1=0, int arg2=0);
    void StartRecording(std::string filename);
    void StopRecording();
    void StartRecordingGIF(const char* filename);
    void StopRecordingGIF();

    sigslot::signal0<> SignalConnectionCompleted;
private:
    
    void OnReadPacket(cricket::Connection *connection, const char *data, size_t len, const rtc::PacketTime& time);
    void CreateCodec();
    void ReleaseCodec();
    
    // parse & send Command
    bool GetIntValueFromJSON(std::string &str, const char* param, int* value);
    const char* GetStringValueFromJSON(std::string &str, const char* param);
    void ParseCommand(std::string command);
    
    // encoding
    int EncodeVoiceFrame(short* buffer);
    int EncodePreviewFrame(unsigned char *y, unsigned char *u, unsigned char *v);
    void HandleEncodeRequest();
    void SetEncodingLevel(unsigned char level);
    
    static P2PStreamer* mP2PStreamer;
    VoiceDecoder* _voiceDecoder;
    VoiceEncoder* _voiceEncoder;
    VPDecoder* _vpDecoder;
    VPEncoder* _vpEncoder;
    DecodeScreen* _decodeScreen;
    PreviewScreen* _previewScreen;
    BitrateMonitor* _bitrateMonitor;
    int _previewWidth, _previewHeight;
    rapidjson::Document mJsonDoc;
    
    VideoInputDeletegate *_videoDelegate;
    AudioInputDeletegate *_audioDelegate;
    
    unsigned char _encodingFrameCount;
    unsigned char _encodingLevel;
    int _sendLastPacketCount;
    
    P2PStreamer::EncState   _encodingState;
    P2PStreamer::EncState   _previousState;
    std::list<EncReq*>      _videoCommandQueue;
    rtc::CriticalSection    _videoCommandLock;
    P2PStreamer::EncState   _voiceRecordState;
    rtc::CriticalSection    _voiceCommandLock;
    Stream stream_;
    bool                    _createdCodec;
    bool                    _startedStream;
    bool                    _getConnectCommand, _getConnectedCommand;
    char                    _commandBuffer[200];
};

#endif /* defined(__Vista__P2PStreamer__) */
