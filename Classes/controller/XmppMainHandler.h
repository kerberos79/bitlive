#ifndef __P2PCLIENT_MANAGER_H__
#define __P2PCLIENT_MANAGER_H__

#include <map>
#include <string>
#include <vector>
#include <ctype.h>
#include "cocos2d.h"
#include "util/Tokenizer.h"
#include "webrtc/base/helpers.h"
#include "webrtc/base/criticalsection.h"
#include "webrtc/base/timeutils.h"
#include "webrtc/libjingle/session/sessionmanager.h"
#include "webrtc/libjingle/session/parsing.h"
#include "webrtc/libjingle/xmpp/xmppclient.h"
#include "webrtc/p2p/base/session.h"
#include "webrtc/p2p/base/candidate.h"
#include "controller/CustomXmppTask.h"
#include "controller/CallStateHandler.h"
#include "controller/P2PChannelAllocator.h"
#include "datatype/InviteEventData.h"

#ifdef FEATURE_P2P
#include "P2PChannel.h"
#include "StunInfo.h"
#include "P2PStreamEngine.h"
#include "../Shader.h"
#endif

using namespace cricket;

const int PING_DELAY = 300000;

class XmppMainHandler: public rtc::MessageHandler, public sigslot::has_slots<> {
public:
    enum class Res : unsigned int
    {
        Init = 0,
        Start,
        CheckIdentity,
        Login,
		Message,
        Error,
    };

    enum class Cmd : unsigned int
    {
        MSG_PING,
		MSG_QUIT,
    };
        
	XmppMainHandler();
	~XmppMainHandler();
    static XmppMainHandler* getInstance();
    void create(buzz::XmppClient* xmpp_client, std::string localIp);
	void SendP2PConnect(const std::string& to);
    /*
    P2PStreamEngine* createP2PStreamEngine() {
        return new P2PStreamEngine(channel->getCurrentSocket());
    }
    */
    void SendEmptyMessage(XmppMainHandler::Cmd cmd);
    void SendEmptyMessageDelayed(XmppMainHandler::Cmd cmd, int delay);
    void SendMessage(XmppMainHandler::Cmd cmd, const cricket::Candidate* candidate);
    void SendText(const std::string& to, const std::string& message);
    void sendFailConnection(const std::string& remote);
	void SendData(const char* data);
	void SendData(const char* data, int len) ;
	void Quit();
    void SendChat(const std::string& to, const std::string msg);
    void OnCandidate(const std::string& to, const cricket::Candidate* c);
	void Input(const char * data, int len);
        
	void HandleChat(const buzz::XmlElement* elem);
	virtual void OnMessage(rtc::Message *msg);

	buzz::XmppClient* getXmppClient() const {
		return xmpp_client_;
	}

	void setXmppClient(buzz::XmppClient* xmppClient) {
		xmpp_client_ = xmppClient;
	}

    void setNetworkInfo(const std::string &type, const std::string &provider);
    void setUserID(const std::string & userid);
    std::string & getUserID();
    void setDomain(const std::string domain);
    void RequestReallocation(std::string& remoteUser);
    void RequestRelayPort();
    
    sigslot::signal2<XmppMainHandler::Res, void*> SignalXmppMessage;
private:
    void InitMedia();
    void HandleCandidate(const buzz::XmlElement* elem);
    bool ParseAddress(const buzz::XmlElement* elem,
                      const buzz::QName& address_name, const buzz::QName& port_name,
                      rtc::SocketAddress* address, ParseError* error);
    void HandleTransport(const buzz::XmlElement* elem);
    void SendPing();
    buzz::XmlElement* MakeIq(const std::string& type, const buzz::Jid& to, const std::string& id);
	void OnStateChange(buzz::XmppEngine::State state);
	static const std::string strerror(buzz::XmppEngine::Error err);

    static XmppMainHandler* mXmppMainHandler;
	buzz::XmppClient* xmpp_client_;
	rtc::Thread* worker_thread_;
	CustomXmppTask* xmpp_task_;
	bool allow_local_ips_;
	std::string name;
	std::string _network_type;
	std::string _network_provider;
	std::string user_id;
    std::string domain_name;

	bool incoming_call_;
	uint32 start_time_;
    std::string	_localIp;
    rtc::CriticalSection criticalsection_;
    
};

#endif
