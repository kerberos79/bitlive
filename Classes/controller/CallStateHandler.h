//
//  CallStateHandler.h
//  Vist
//
//  Created by kerberos on 1/16/15.
//
//

#ifndef __Vist__CallStateHandler__
#define __Vist__CallStateHandler__

#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>

#include "cocos2d.h"
#include "HttpClient.h"
#include "HttpResponse.h"
#include "controller/XmppLoginThread.h"
#include "controller/XmppMainHandler.h"
#include "controller/UpnpPortHandler.h"
#include "controller/HttpRequestHandler.h"
#include "controller/P2PStreamer.h"
#include "datatype/SignupInfo.h"
#include "datatype/InviteEventData.h"
#include "datatype/UIMessage.h"
#include "webrtc/base/messagehandler.h"
#include "webrtc/base/sigslot.h"
#include "webrtc/base/thread.h"
#include "util/define.h"
#include "util/IdentityManager.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "util/AndroidMP3Decoder.h"
#endif

USING_NS_CC;
using namespace rtc;

class XmppMainHandler;

class CallStateHandler: public MessageHandler, public sigslot::has_slots<> {
public:
    enum class Cmd : unsigned int
    {
        Start = 0,
        Init,
        Install,
        NotFoundNetwork,
        CheckIdentity,
        SignUp,
        SignUpSuccess,
        SignUpFail,
        DeleteAccount,
        DeleteAccountSuccess,
        DeleteAccountFail,
        Login,
        RequestContactList,
        DestoryStunPort,
        RequestUpnpPort,
		LoginSuccess,
        LoginFail,
        CheckOfflineMessage,
        XmppMessage,
        Invite,
        Refusal,
        Cancel,
        Accept,
        Busy,
        ConnectionSuccess,
		ConnectionFail,
        Disconnect,
        Disconnected,
        Release,
        SendEffectCommand,
        SendEmoticonCommand,
        SendStickerCommand,
        SendRotateCommand,
        SendLinkURLCommand,
        DecodeEffect,
        DecodeEmoticon,
        DecodeSticker,
        DecodeRotate,
        EncodingBitrate,
        LinkURL,
        StartRecording,
        StopRecording,
        StartRecordingGIF,
        StopRecordingGIF,
		Exit,
    };
    CallStateHandler();
    ~CallStateHandler();
    
    static CallStateHandler* getInstance();
    bool Init();
    void Start();
    void Install(bool offline);
    virtual void OnMessage(Message *msg);
    void SendEmptyMessage(CallStateHandler::Cmd cmd);
    void SendMessage(CallStateHandler::Cmd cmd, bool userData);
    void SendMessage(CallStateHandler::Cmd cmd, int userData);
    void SendMessage(CallStateHandler::Cmd cmd, const char* userData);
    void SendMessage(CallStateHandler::Cmd cmd, std::string userData);
    void SendMessage(CallStateHandler::Cmd cmd, std::vector<std::string>* bundle);
    void SendMessage(CallStateHandler::Cmd cmd, Vec3 userData);
    void SendXmppMessage(CallStateHandler::Cmd cmd, InviteEventData* data);
    void SendEmptyMessageDelayed(CallStateHandler::Cmd Cmd, int delay);
    void SendMessageDelayed(Message *msg, int delay);
    void SendHttpRequest(std::string host, int port, std::string path, std::string tag);
    std::string& GetLoginID()
    {
        return _loginId;
    }
    std::string& GetLoginPassword()
    {
        return _loginPassword;
    }
        
    std::function<void(std::string)> SignalOfflineMessage;
    std::function<void(UIMessage*)> UIMessageCallback;
private:
    void HandleXmppMessage(InviteEventData* eventData);
    void OnStunResponse(bool ready);
    void OnHttpResponse(network::HttpClient *sender, network::HttpResponse *response);
    void OnDeviceMessage(Device::Res response, void* data, unsigned char* photo, int length);

    static CallStateHandler* _callStateHandler;
    XmppMainHandler* _xmppHandler;
    Thread* _workerThread;
        
    std::string _loginId;
    std::string _loginPassword;
    std::string _localIpAddress;
    std::string _networkType;
    std::string _networkProvider;
        
    SignupInfo _signupInfo;
    std::string remote_username;
};
#endif /* defined(__Vist__CallStateHandler__) */
