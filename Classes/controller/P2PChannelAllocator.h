//
//  P2PChannelAllocator.h
//  Vista
//
//  Created by kerberos on 4/24/15.
//
//

#ifndef __Vista__P2PChannelAllocator__
#define __Vista__P2PChannelAllocator__

#include <stdio.h>
#include <list>
#include "cocos2d.h"
#include "webrtc/base/fakenetwork.h"
#include "webrtc/base/firewallsocketserver.h"
#include "webrtc/base/natserver.h"
#include "webrtc/base/natsocketfactory.h"
#include "webrtc/base/physicalsocketserver.h"
#include "webrtc/base/proxyserver.h"
#include "webrtc/base/socketaddress.h"
#include "webrtc/base/thread.h"
#include "webrtc/base/criticalsection.h"
#include "webrtc/base/virtualsocketserver.h"
#include "webrtc/p2p/client/basicportallocator.h"
#include "webrtc/p2p/base/basicpacketsocketfactory.h"
#include "webrtc/p2p/base/p2ptransportchannel.h"
#include "webrtc/p2p/base/port.h"
#include "webrtc/p2p/base/tcpport.h"
#include "webrtc/p2p/base/udpport.h"
#include "webrtc/p2p/base/stunport.h"
#include "webrtc/p2p/base/turnport.h"
#include "webrtc/p2p/base/relayport.h"
#include "controller/XmppMainHandler.h"
#include "controller/UpnpPortHandler.h"
#include "util/define.h"

static const rtc::SocketAddress StunAddr(STUN_SERVER_ADDRESS, 3478);
static const rtc::SocketAddress RelayUdpAddr(XMPP_SERVER_ADDRESS, 3478);
static const cricket::ProtocolAddress RelayProtoAddr(RelayUdpAddr, cricket::ProtocolType::PROTO_UDP);

#define PREFIX_INVITE   'I'
#define PREFIX_REFUSE   'R'
#define PREFIX_BUSY     'B'
#define PREFIX_CANCEL   'C'
#define PREFIX_MESSAGE  'M'
#define PREFIX_ERROR    'E'

const int P2P_STUN_TIMEOUT = 5000;
const int MIN_PORT = 7002;
const int MAX_PORT = 8000;
const int tcp_server_port = 7002;

const char MAGIC_COOKIES[] = { '\x72', '\xC6', '\x4B', '\xC6' };
class P2PChannelAllocator : public rtc::MessageHandler, public sigslot::has_slots<>{
public:
    
    enum class Cmd : unsigned int
    {
        GetNetwork,
        Allocate,
        Reallocate,
        Candidate,
        Transport,
        RelayResponse,
        Timeout,
        PortRemove,
        TCPPing,
        Ping,
        SendConnection,
        Destroy,
        SendInvitation,
        RecvInvitation,
        SendCancel,
        SendRefusal,
        SendBusy
    };
    
    enum class Protocol : unsigned int
    {
        Tcp,
        Stun,
        Ipv6,
    };
    
    P2PChannelAllocator();
    ~P2PChannelAllocator();
    static P2PChannelAllocator* getInstance();
    
    void Init(std::string &localIp, std::string &network_type, std::string &provider);
    void SetUserID(std::string userid);
    void Release();
    void SendEmptyMessage(P2PChannelAllocator::Cmd cmd);
    void SendEmptyMessageDelayed(P2PChannelAllocator::Cmd cmd, int delay);
    void SendMessage(P2PChannelAllocator::Cmd cmd, cricket::Connection* conn);
    void SendMessage(P2PChannelAllocator::Cmd cmd, cricket::Port* port);
    void SendMessage(P2PChannelAllocator::Cmd cmd, std::string userData);
    void SendMessageDelayed(P2PChannelAllocator::Cmd cmd, cricket::Connection* conn, int delay);
    void SendRemoteCandidate(cricket::Candidate* candidate);
    void SendRelayPortResponse(cricket::Candidate* candidate);
private:
    virtual void OnMessage(rtc::Message *msg);
    cricket::Port* CreateTCPPorts(bool allow_listen);
    cricket::Port* CreateUDPPorts();
    cricket::Port* CreateStunPorts();
    cricket::Port* CreateTurnPorts();
    void OnAllocate();
    void OnReallocate(std::string& remoteUser);
    bool SearchRemoteCandidate(cricket::Candidate* candidate);
    cricket::Port* SearchPortFromCandidate(cricket::Candidate& candidate);
    void OnCandidate(cricket::Candidate* candidate);
    void OnCandidateRelay(cricket::Candidate* candidate);
    void OnCandidateRelayResponse(cricket::Candidate* candidate);
    void OnTimeout();
    void OnNetworksChanged();
    void OnCandidateReady(cricket::Port* port, const cricket::Candidate& candidate);
    void SendNewCandidate(cricket::Candidate& candidate);
    void OnConnectionCreated(cricket::Port* port, cricket::Connection *conn);
    void OnPortError(cricket::Port* port);
    void OnPortComplete(cricket::Port* port);
    void OnPortDestroyed(cricket::PortInterface* port);
    void OnConnectionDestroyed(cricket::Connection *conn);
    void OnWaitTcpPing(cricket::Connection *connection, const char *data, size_t len,
                      const rtc::PacketTime& time);
    void OnConnectionStateChange(cricket::Connection* conn);
    void DestroyChannels() ;
    void DestroyPort(cricket::Port* port);
    void OnConnectionCompleted();
    void OnReadPacket(cricket::Connection *connection, const char *data, size_t len, const rtc::PacketTime& time);
    void OnSendInvitation(std::string remote);
    void OnRecvInvitation(std::string message);
    void OnSendCancel();
    void OnSendRefusal();
    void OnSendBusy();
    
    static P2PChannelAllocator* mP2PChannelAllocator;
    rtc::NetworkManager* network_manager_;
    rtc::Network* network_;
    rtc::PacketSocketFactory* socket_factory_;
    rtc::Thread* _workerThread;
    rtc::CriticalSection criticalsection_;
    std::list<cricket::Candidate*> newRemoteCandidates;
    std::list<cricket::Candidate*> remoteRelayCandidates;
    std::list<cricket::Port*> ports_;
    std::vector<cricket::Connection*> connections_;
    bool isIpv6Address;
    bool isPublicAddress;
    cricket::Port* tcpServerPort_;
    cricket::ServerAddresses stun_servers;
    bool connection_successed;
    bool _disconnected;
    std::string _remoteUserId;
    std::string _userId;
    std::string _timestamp;
    
    char test_buffer[20];
    int test_count;
    std::string thisipaddress;
    cricket::TurnPort* _turnport;
};

#endif /* defined(__Vista__P2PChannelAllocator__) */
