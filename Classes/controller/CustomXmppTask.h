#ifndef __CUSTOM_XMPPTASK_H_
#define __CUSTOM_XMPPTASK_H_

#include <string>
#include "webrtc/base/sigslot.h"
#include "webrtc/libjingle/xmpp/xmppengine.h"
#include "webrtc/libjingle/xmpp/constants.h"
#include "webrtc/libjingle/session/constants.h"
#include "webrtc/libjingle/xmpp/xmpptask.h"
#include "webrtc/base/helpers.h"
#include "webrtc/base/logging.h"
namespace cricket {

class CustomXmppTask: public buzz::XmppTask
{
public:
	CustomXmppTask(buzz::XmppTaskParentInterface* parent);
	~CustomXmppTask();

	virtual bool HandleStanza(const buzz::XmlElement* stanza);
	virtual int ProcessStart();
	sigslot::signal1<const buzz::XmlElement*> SignalHandleCandidate;
	sigslot::signal1<const buzz::XmlElement*> SignalHandleTransport;
	sigslot::signal1<const buzz::XmlElement*> SignalHandleChat;
};

}
#endif
