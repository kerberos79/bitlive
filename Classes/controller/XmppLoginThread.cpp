//
//  XmppLoginThread.cpp
//  Vist
//
//  Created by kerberos on 1/9/15.
//
//

#include "XmppLoginThread.h"


XmppLoginThread* XmppLoginThread::_xmppLoginThread = nullptr;

XmppLoginThread::XmppLoginThread()
:_mainThread(nullptr),_id(nullptr)
{
    _workerThread = new rtc::Thread();
}

XmppLoginThread::~XmppLoginThread()
{
    
}

XmppLoginThread* XmppLoginThread::Instance()
{
    if(_xmppLoginThread==nullptr)
    {
        _xmppLoginThread = new XmppLoginThread();
    }
    
    return _xmppLoginThread;
}

void XmppLoginThread::Release()
{
    if(_xmppLoginThread)
    {
        delete _xmppLoginThread;
        _xmppLoginThread = nullptr;
    }
}

void XmppLoginThread::Init(std::string& loginId, std::string& password, std::string localIp, std::string networktype, std::string provider)
{
    _id = loginId.c_str();
    _password = password.c_str();
    buzz::Jid jid = buzz::Jid(_id);
    rtc::InsecureCryptStringImpl pass;
    pass.password() = _password;
    _xcs.set_user(jid.domain());
    _xcs.set_host(XMPP_DOMAIN_NAME);
    _xcs.set_use_tls(buzz::TLS_DISABLED);
    _xcs.set_pass(rtc::CryptString(pass));
    _xcs.set_server(rtc::SocketAddress(XMPP_SERVER_ADDRESS, XMPP_SERVER_PORT));
    _xcs.set_allow_plain(false);
    
    XmppMainHandler::getInstance()->create(_pump.client(), localIp);
    
    // initialized callback function
    XmppMainHandler::getInstance()->setDomain(XMPP_DOMAIN_NAME);
    XmppMainHandler::getInstance()->setUserID(_id);
    XmppMainHandler::getInstance()->setNetworkInfo(networktype, provider);
}

void XmppLoginThread::Start()
{
    _workerThread->Start(this);
}

void XmppLoginThread::Run(Thread* thread)
{
    _mainThread = new rtc::Thread();
    rtc::ThreadManager::Instance()->SetCurrentThread(_mainThread);
   	_pump.DoLogin(_xcs, new buzz::XmppSocket(buzz::TLS_DISABLED), new XmppAuth());
    
    // This is blocked by quit.
    _mainThread->Run();
   	_pump.DoDisconnect();
    
    Release();
}


