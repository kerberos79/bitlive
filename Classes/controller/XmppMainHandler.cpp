#include "XmppMainHandler.h"

const buzz::StaticQName QN_TIMESTAMP = { NS_EMPTY, "tms" };
const buzz::StaticQName QN_RESULT = { NS_EMPTY, "rs" };

const int P2P_TRY_TIMEOUT = 5000;  // 10sec

std::string GetWord(const std::vector<std::string>& words, size_t index,
                    const std::string& def) {
    if (words.size() > index) {
        return words[index];
    } else {
        return def;
    }
}

int GetInt(const std::vector<std::string>& words, size_t index, int def) {
    int val;
    if (words.size() > index && rtc::FromString(words[index], &val)) {
        return val;
    } else {
        return def;
    }
}

template <typename T> std::string tostr(const T& t) {
    std::   ostringstream os;
    os<<t;
    return os.str();
}

XmppMainHandler::XmppMainHandler() :
worker_thread_(NULL),
incoming_call_(false)
{
}

XmppMainHandler::~XmppMainHandler() {

	delete worker_thread_;
}

XmppMainHandler* XmppMainHandler::mXmppMainHandler = nullptr;

XmppMainHandler* XmppMainHandler::getInstance()
{
    if(mXmppMainHandler==nullptr)
    {
    	mXmppMainHandler = new XmppMainHandler();
    }
    return mXmppMainHandler;
}

void XmppMainHandler::create(buzz::XmppClient* xmpp_client, std::string localIp)
{
    xmpp_client_ = xmpp_client;
    xmpp_client_->SignalStateChange.connect(this, &XmppMainHandler::OnStateChange);
	_localIp = localIp;

}

void XmppMainHandler::SendP2PConnect(const std::string& to) {

	SendChat(to, 0);
}

void XmppMainHandler::OnCandidate(const std::string& to, const cricket::Candidate* candidate) {
    buzz::XmlElement* elem = new buzz::XmlElement(QN_GINGLE_P2P_CANDIDATE);
    
    elem->SetAttr(QN_ADDRESS, candidate->address().ipaddr().ToString());
    elem->SetAttr(QN_PORT, candidate->address().PortAsString());
    elem->SetAttr(QN_PREFERENCE, tostr(candidate->preference()));
    elem->SetAttr(QN_USERNAME, candidate->username());
    elem->SetAttr(QN_PROTOCOL, candidate->protocol());
    elem->SetAttr(QN_GENERATION, candidate->generation_str());
    if (candidate->password().size() > 0)
        elem->SetAttr(QN_PASSWORD, candidate->password());
    if (candidate->type().size() > 0)
        elem->SetAttr(buzz::QN_TYPE, candidate->type());
    if (candidate->network_name().size() > 0)
        elem->SetAttr(QN_NETWORK, candidate->network_name());
    
    buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
    stanza->AddAttr(buzz::QN_TO, to+"@"+domain_name);
    stanza->AddAttr(buzz::QN_ID, rtc::CreateRandomString(16));
    stanza->AddAttr(buzz::QN_TYPE, "chat");
    stanza->AddElement(elem);
    
    LOG(WARNING)<<"[SendCandidate]"<<stanza->Str();
    xmpp_client_->SendStanza(stanza);
    delete candidate;
    delete stanza;
}

void XmppMainHandler::HandleCandidate(const buzz::XmlElement* stanza) {
    std::string remoteUser;
    const buzz::XmlElement* error = stanza->FirstNamed(buzz::QN_ERROR);
    if(error!=NULL) {
        CCLOG("HandleCandidate has an error");
        const std::string& from = stanza->Attr(buzz::QN_FROM);
        int pos = from.find("@");
        remoteUser = from.substr(0,pos);
        CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::ConnectionFail, remoteUser);
//        jni_callback->OnFailedConnection(remoteuser.c_str());
        return;
    }
    const buzz::XmlElement* elem = stanza->FirstNamed(QN_GINGLE_P2P_CANDIDATE);
    const buzz::XmlElement* chatElement = stanza->FirstNamed(buzz::QN_MESSAGE);
    
    const std::string& from = stanza->Attr(buzz::QN_FROM);
    int pos = from.find("@");
    remoteUser = from.substr(0,pos);
    
    cricket::ParseError parseError;
    rtc::SocketAddress address;
    if (ParseAddress(elem, QN_ADDRESS, QN_PORT, &address, &parseError))
    {
        cricket::Candidate* candidate = new cricket::Candidate();
        
        candidate->set_address(address);
        candidate->set_username(elem->Attr(QN_USERNAME));
        candidate->set_protocol(elem->Attr(QN_PROTOCOL));
        candidate->set_generation_str(elem->Attr(QN_GENERATION));
        if (elem->HasAttr(QN_PASSWORD))
            candidate->set_password(elem->Attr(QN_PASSWORD));
        if (elem->HasAttr(buzz::QN_TYPE))
            candidate->set_type(elem->Attr(buzz::QN_TYPE));
        if (elem->HasAttr(QN_NETWORK))
            candidate->set_network_name(elem->Attr(QN_NETWORK));
        
        LOG(WARNING) << "[HandleCandidate]"<<stanza->Str();
       P2PChannelAllocator::getInstance()->SendRemoteCandidate(candidate);
    }
}

bool XmppMainHandler::ParseAddress(const buzz::XmlElement* elem,
                                   const buzz::QName& address_name, const buzz::QName& port_name,
                                   rtc::SocketAddress* address, ParseError* error) {
    if (!elem->HasAttr(address_name)) {
        LOG(LS_INFO)<<"address_name not have";
        return cricket::BadParse("address does not have " + address_name.LocalPart(),
                        error);
    }
    
    if (!elem->HasAttr(port_name)) {
        LOG(LS_INFO)<<"port_name not have";
        return cricket::BadParse("address does not have " + port_name.LocalPart(), error);
    }
    address->SetIP(elem->Attr(address_name));
    std::istringstream ist(elem->Attr(port_name));
    int port = 0;
    ist >> port;
    address->SetPort(port);
    
    return true;
}

void XmppMainHandler::RequestRelayPort() {
    buzz::XmlElement* elem = new buzz::XmlElement(QN_GINGLE_P2P_TRANSPORT);
    elem->SetAttr(buzz::QN_TYPE, "allocate");
    buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
    stanza->AddAttr(buzz::QN_TO, std::string(RELAY_PEER_NAME)+"@"+domain_name);
    stanza->AddAttr(buzz::QN_ID, rtc::CreateRandomString(16));
    stanza->AddAttr(buzz::QN_TYPE, "chat");
    stanza->AddElement(elem);
    xmpp_client_->SendStanza(stanza);
    LOG(WARNING) << "[RequestRelayPort]"<<stanza->Str();
    delete stanza;
}

void XmppMainHandler::RequestReallocation(std::string& remoteUser) {
    buzz::XmlElement* elem = new buzz::XmlElement(QN_GINGLE_P2P_TRANSPORT);
    elem->SetAttr(buzz::QN_TYPE, "reallocate");
    buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
    stanza->AddAttr(buzz::QN_TO, remoteUser+"@"+domain_name);
    stanza->AddAttr(buzz::QN_ID, rtc::CreateRandomString(16));
    stanza->AddAttr(buzz::QN_TYPE, "chat");
    stanza->AddElement(elem);
    xmpp_client_->SendStanza(stanza);
    LOG(WARNING) << "[RequestReallocation]"<<stanza->Str();
    delete stanza;
}

void XmppMainHandler::HandleTransport(const buzz::XmlElement* stanza) {
    std::string remoteUser;
    const buzz::XmlElement* error = stanza->FirstNamed(buzz::QN_ERROR);
    if(error!=NULL) {
        CCLOG("HandleTransport has an error");
        const std::string& from = stanza->Attr(buzz::QN_FROM);
        int pos = from.find("@");
        remoteUser = from.substr(0,pos);
        CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::ConnectionFail, remoteUser);
        //        jni_callback->OnFailedConnection(remoteuser.c_str());
        return;
    }
    const buzz::XmlElement* elem = stanza->FirstNamed(QN_GINGLE_P2P_TRANSPORT);
    const buzz::XmlElement* chatElement = stanza->FirstNamed(buzz::QN_MESSAGE);
    
    const std::string& from = stanza->Attr(buzz::QN_FROM);
    int pos = from.find("@");
    const std::string remote_username = from.substr(0,pos);
    
    cricket::ParseError parseError;
    rtc::SocketAddress address;
    const std::string& type = elem->Attr(buzz::QN_TYPE);
    LOG(WARNING) << "[HandleTransport]"<<stanza->Str();
    if(!type.compare("reallocate"))
    {
        P2PChannelAllocator::getInstance()->SendMessage(P2PChannelAllocator::Cmd::Reallocate, remote_username);
    }
    else
    {
        if (ParseAddress(elem, QN_ADDRESS, QN_PORT, &address, &parseError))
        {
            cricket::Candidate* candidate = new cricket::Candidate();
            
            candidate->set_address(address);
            candidate->set_username(elem->Attr(QN_USERNAME));
            candidate->set_protocol(elem->Attr(QN_PROTOCOL));
            candidate->set_generation_str(elem->Attr(QN_GENERATION));
            if (elem->HasAttr(QN_PASSWORD))
                candidate->set_password(elem->Attr(QN_PASSWORD));
            if (elem->HasAttr(buzz::QN_TYPE))
                candidate->set_type(elem->Attr(buzz::QN_TYPE));
            if (elem->HasAttr(QN_NETWORK))
                candidate->set_network_name(elem->Attr(QN_NETWORK));
            
            P2PChannelAllocator::getInstance()->SendRelayPortResponse(candidate);
        }
    }
}

void XmppMainHandler::SendPing()
{
    buzz::XmlElement* stanza = MakeIq(buzz::STR_GET, buzz::Jid(buzz::STR_EMPTY), user_id);
    stanza->AddElement(new buzz::XmlElement(buzz::QN_PING));
    xmpp_client_->SendStanza(stanza);
    delete stanza;
}

buzz::XmlElement* XmppMainHandler::MakeIq(const std::string& type,
       const buzz::Jid& to,
       const std::string& id) {
    buzz::XmlElement* result = new buzz::XmlElement(buzz::QN_IQ);
    if (!type.empty())
        result->AddAttr(buzz::QN_TYPE, type);
    if (!to.IsEmpty())
        result->AddAttr(buzz::QN_TO, to.Str());
    if (!id.empty())
        result->AddAttr(buzz::QN_ID, id);
    return result;
}

void XmppMainHandler::SendText(const std::string& to, const std::string& message)
{
    CCLOG("%s", domain_name.c_str());
    buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
    stanza->AddAttr(buzz::QN_TO, to +"@" + domain_name);
    stanza->AddAttr(buzz::QN_TYPE, "chat");
    buzz::XmlElement* body = new buzz::XmlElement(buzz::QN_BODY);
    body->SetBodyText(message);
    stanza->AddElement(body);
    xmpp_client_->SendStanza(stanza);
    LOG(INFO)<<stanza->Str();
    delete stanza;
}

void XmppMainHandler::sendFailConnection(const std::string& remote) {
    buzz::XmlElement* elem = new buzz::XmlElement(QN_GINGLE_P2P_TRANSPORT);
    elem->SetAttr(QN_RESULT, "false");
    buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
    stanza->AddAttr(buzz::QN_TO, remote+"@"+domain_name);
    stanza->AddAttr(buzz::QN_ID, rtc::CreateRandomString(16));
    stanza->AddAttr(buzz::QN_TYPE, "chat");
    stanza->AddElement(elem);
    xmpp_client_->SendStanza(stanza);
    LOG(INFO)<<">>"<<stanza->Str();
    delete stanza;
}

void XmppMainHandler::SendData(const char* data) {
	LOG(LS_INFO)<<"XmppMainHandler::SendData!!";
	//const char* data = str.c_str();
	int len = static_cast<int>(strlen(data));
//	this->channel->SendPacket(data, len);
}

void XmppMainHandler::SendData(const char* data, int len) {
	LOG(LS_INFO)<<"XmppMainHandler::SendData!!";
//	this->channel->SendPacket(data, len);
}

void XmppMainHandler::Quit() {
    
    if (rtc::Thread::Current()!=NULL) {
		rtc::Thread::Current()->Quit();
	}
}

void XmppMainHandler::setUserID(const std::string & userid) {
    this->user_id = userid;
}

std::string & XmppMainHandler::getUserID() {
    return this->user_id;
}

void XmppMainHandler::setDomain(const std::string domain) {
    this->domain_name = domain;
}

void XmppMainHandler::setNetworkInfo(const std::string &type, const std::string &provider) {
    this->_network_type = type;
    this->_network_provider = provider;
}


void XmppMainHandler::OnStateChange(buzz::XmppEngine::State state) {
	switch (state) {
	case buzz::XmppEngine::STATE_START:
		CCLOG("connecting...");
		break;
	case buzz::XmppEngine::STATE_OPENING:
		CCLOG("logging in...");
		break;
    case buzz::XmppEngine::STATE_OPEN:
        CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::LoginSuccess);
	    InitMedia();
        SendEmptyMessageDelayed(Cmd::MSG_PING, PING_DELAY);
		break;

        case buzz::XmppEngine::STATE_CLOSED:
        CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::LoginFail);
		buzz::XmppEngine::Error error = xmpp_client_->GetError(NULL);
            
        // TODO: logout callback
		CCLOG("quit %d", __LINE__);
		Quit();
		break;
    }
}

void XmppMainHandler::InitMedia() {
	//init status.
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_PRESENCE);
	stanza->AddElement(new buzz::XmlElement(buzz::QN_STATUS));
	getXmppClient()->SendStanza(stanza);

	worker_thread_ = new rtc::Thread();
        worker_thread_->Start();
    
	xmpp_task_ = new CustomXmppTask(xmpp_client_);
	xmpp_task_->SignalHandleCandidate.connect(this,
			&XmppMainHandler::HandleCandidate);
	xmpp_task_->SignalHandleTransport.connect(this,
			&XmppMainHandler::HandleTransport);
	xmpp_task_->SignalHandleChat.connect(this, &XmppMainHandler::HandleChat);
	xmpp_client_->AddXmppTask(xmpp_task_, buzz::XmppEngine::HL_ALL);
	delete stanza;
    
}

void XmppMainHandler::OnMessage(rtc::Message *msg) {
    switch ((XmppMainHandler::Cmd)msg->message_id)
    {
        case Cmd::MSG_PING:
        {
            SendPing();
            SendEmptyMessageDelayed(Cmd::MSG_PING, PING_DELAY);
            break;
        }
        case Cmd::MSG_QUIT:
        {
            break;
        }
	}
}

void XmppMainHandler::Input(const char * data, int len) {

}

void XmppMainHandler::SendChat(const std::string& remote, const std::string msg) {
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
	stanza->AddAttr(buzz::QN_TO, remote);
	stanza->AddAttr(buzz::QN_ID, rtc::CreateRandomString(16));
	stanza->AddAttr(buzz::QN_TYPE, "chat");
	buzz::XmlElement* body = new buzz::XmlElement(buzz::QN_BODY);
	body->AddAttr(buzz::QN_TYPE, "message");
	body->SetBodyText("M:"+user_id+":"+msg);
	stanza->AddElement(body);
	xmpp_client_->SendStanza(stanza);

	delete stanza;
}

void XmppMainHandler::HandleChat(const buzz::XmlElement* stanza) {
	const buzz::XmlElement* error = stanza->FirstNamed(buzz::QN_ERROR);
	if(error!=NULL) {
        // offline message has error BUT we ignore this error message beacause server send a push noti!!!
        /*
		CCLOG("HandleCandidate has an error");
        LOG(INFO)<<stanza->Str();
        const std::string& from = stanza->Attr(buzz::QN_FROM);

        InviteEventData* newInvitation = new InviteEventData();
        newInvitation->setFrom(from);
        newInvitation->store(from, "");
        newInvitation->setEventType(InviteEventData::Type::Error);
        CallStateHandler::getInstance()->SendXmppMessage(CallStateHandler::Cmd::XmppMessage, newInvitation);
        */
		return;
	}
    const buzz::XmlElement* elem = stanza->FirstElement();
    P2PChannelAllocator::getInstance()->SendMessage(P2PChannelAllocator::Cmd::RecvInvitation, elem->BodyText());

	LOG(INFO)<<"<<"<<stanza->Str();
}

const std::string XmppMainHandler::strerror(buzz::XmppEngine::Error err) {
	switch (err) {
	case buzz::XmppEngine::ERROR_NONE:
		return "";
	case buzz::XmppEngine::ERROR_XML:
		return "Malformed XML or encoding error";
	case buzz::XmppEngine::ERROR_STREAM:
		return "XMPP stream error";
	case buzz::XmppEngine::ERROR_VERSION:
		return "XMPP version error";
	case buzz::XmppEngine::ERROR_UNAUTHORIZED:
		return "User is not authorized (Check your username and password)";
	case buzz::XmppEngine::ERROR_TLS:
		return "TLS could not be negotiated";
	case buzz::XmppEngine::ERROR_AUTH:
		return "Authentication could not be negotiated";
	case buzz::XmppEngine::ERROR_BIND:
		return "Resource or session binding could not be negotiated";
	case buzz::XmppEngine::ERROR_CONNECTION_CLOSED:
		return "Connection closed by output handler.";
	case buzz::XmppEngine::ERROR_DOCUMENT_CLOSED:
		return "Closed by </stream:stream>";
	case buzz::XmppEngine::ERROR_SOCKET:
		return "Socket error";
	default:
		return "Unknown error";
	}
}

void XmppMainHandler::SendEmptyMessage(XmppMainHandler::Cmd cmd)
{
    worker_thread_->Post(this, (int)cmd);
}

void XmppMainHandler::SendEmptyMessageDelayed(XmppMainHandler::Cmd cmd, int delay)
{
    worker_thread_->PostDelayed(delay, this, (int)cmd);
}

void XmppMainHandler::SendMessage(XmppMainHandler::Cmd cmd, const cricket::Candidate* candidate)
{
    TypedMessageData<const cricket::Candidate*> *msgData = new TypedMessageData<const cricket::Candidate*>(candidate);
    worker_thread_->Post(this, (unsigned int)cmd, msgData);
}
