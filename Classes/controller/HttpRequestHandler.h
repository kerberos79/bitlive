//
//  HttpRequestHandler.h
//  Vista
//
//  Created by kerberos on 2/4/15.
//
//

#ifndef __Vista__HttpRequestHandler__
#define __Vista__HttpRequestHandler__

#include <stdio.h>
#include "webrtc/base/asynchttprequest.h"
#include "webrtc/base/httpserver.h"
#include "webrtc/base/socketstream.h"
#include "webrtc/base/thread.h"
#include "webrtc/base/sigslot.h"
#include "cocos2d.h"

class HttpRequestHandler : public sigslot::has_slots<> {
public:
    HttpRequestHandler();
    ~HttpRequestHandler();
    static HttpRequestHandler* getInstance();
    void SendGetRequest(const std::string& host, int port,
                          const std::string& path, const std::string& tag);
    sigslot::signal2<std::string&, rtc::HttpResponseData&> SignalHttpResponse;
    
private:
    void OnRequestDone(rtc::SignalThread* thread);
    
    static HttpRequestHandler* mHttpRequestHandler;
    rtc::AsyncHttpRequest* mRequest;
    std::string mTag;
};
#endif /* defined(__Vista__HttpRequestHandler__) */
