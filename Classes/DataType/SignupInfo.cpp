//
//  SignupInfo.cpp
//  Vista
//
//  Created by kerberos on 2/10/15.
//
//

#include "SignupInfo.h"

SignupInfo::SignupInfo()
{
}

SignupInfo::~SignupInfo()
{
}

void SignupInfo::store(std::string& userId, std::string& password, const char* registerId)
{
    _userId = userId;
    _password = password;
    _registerId = registerId;
}

void SignupInfo::store(std::string& userId, std::string& password, std::string& registerId)
{
    _userId = userId;
    _password = password;
    _registerId = registerId;
}

std::string& SignupInfo::getUserId()
{
    return _userId;
}

std::string& SignupInfo::getPassword()
{
    return _password;
}

std::string& SignupInfo::getRegisterId()
{
    return _registerId;
}