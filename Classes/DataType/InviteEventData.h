//
//  InviteEventData.h
//  Vista
//
//  Created by kerberos on 2/13/15.
//
//

#ifndef __Vista__InviteEventData__
#define __Vista__InviteEventData__

#include <string>
class InviteEventData {
public:
    enum class Type : int
    {
        Send,
        Recv,
        Cancel,
        Refusal,
        Busy,
        Connecting,
		Message,
        Error,
        Connect
    };
    InviteEventData();
    ~InviteEventData();
    InviteEventData::Type what;
    std::string data;
};

#endif /* defined(__Vista__InviteEventData__) */
