//
//  SignupInfo.h
//  Vista
//
//  Created by kerberos on 2/10/15.
//
//

#ifndef __Vista__SignupInfo__
#define __Vista__SignupInfo__

#include <stdio.h>
#include <string>
class SignupInfo {
public:
    SignupInfo();
    ~SignupInfo();
    void store(std::string& userId, std::string& password, const char* registerId);
    void store(std::string& userId, std::string& password, std::string& registerId);
    std::string& getUserId();
    std::string& getPassword();
    std::string& getRegisterId();
    
    std::string _userId;
    std::string _password;
    std::string _registerId;
private:
};
#endif /* defined(__Vista__SignupInfo__) */
