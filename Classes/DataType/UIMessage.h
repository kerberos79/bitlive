//
//  UIMessage.h
//  Vista
//
//  Created by kerberos on 2/27/15.
//
//

#ifndef __Vista__UIMessage__
#define __Vista__UIMessage__

#include <stdio.h>
#include <vector>
#include <string>

class UIMessage {
public:
    enum class Cmd : unsigned int
    {
        Install,
        ContactList,
        Login,
        Invite,
        Connect,
        Disconnect,
        NotFountNetwork,
        FlippedCamera,
        DecodeEffect,
        DecodeEmoticon,
        DecodeSticker,
        DecodeRotate,
        EncodingBitrate,
        LinkURL,
        DeleteAccount,
        Join,
        FaceDetect,
        FinishCaptureGIF,
        FinishRecord,
    };
    
    
    enum class Install : int
    {
        Start,
        Skip,
        Complete
    };
    
    enum class Alert
    {
        NotSupported = 0,
        Captured,
        IOSCaptured,
        OnBusy
    };
    
    enum class Toast
    {
        LinkURL,
        Call
    };
    
    enum class Record : int
    {
        MP4 = 0,
        GIF
    };

    
    UIMessage(Cmd cmd):bundle(nullptr), extra(nullptr)
    {
        what = cmd;
    }
    ~UIMessage()
    {
        if(bundle!=nullptr && !bundle->empty())
        {
            bundle->clear();
            delete bundle;
        }
    }
    
    UIMessage::Cmd what;
    bool flag;
    int arg1;
    int arg2;
    int arg3;
    int arg4;
    std::string data;
    std::vector<std::string>* bundle;
    void* extra;
private:
};

#endif /* defined(__Vista__UIMessage__) */
