//
//  Procedure.h
//  BitLive
//
//  Created by kerberos on 6/29/15.
//
//

#ifndef BitLive_Procedure_h
#define BitLive_Procedure_h


enum class MainState : unsigned int
{
    Idle = 0,
    Install,
    Enter,
    Offline,
    Main,
    Calling,
    Recording,
    Exit,
    Release,
};

#endif
