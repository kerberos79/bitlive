#include"GFrameRecvHelper.h"

GFrameRecvHelper::GFrameRecvHelper(unsigned char streamid, int maxpacketsize):
request_gframe_count(0),
frameCount(0),
sumFrameDelay(0),
totalReadBytes(0),
request_gframe(false),
frame_number(0xff),
isComplete(false),
packet_count(0),
sequence_number(0),
total_packet_number(0),
frame_length(0)
{
	stream_id = streamid;
	max_packet_size = maxpacketsize;
    request_buffer = new unsigned char[max_packet_size];
	receive_check_list = new bool[max_packet_size] ;
	memset(receive_check_list, 0, sizeof(bool)*max_packet_size);
	read_buffer = new unsigned char[MAX_FRAME_VIDEO];
	
}

GFrameRecvHelper::~GFrameRecvHelper() {
	delete read_buffer;
	delete request_buffer;
	delete receive_check_list;
}

int GFrameRecvHelper::insert(unsigned char* buffer, int length) {
	unsigned char* offset = buffer + length;
	bool recovery_bit = (*(--offset) & 0x80)? true : false;
	unsigned char framenum = *(--offset);
	unsigned char totalpacketnum = *(--offset);
	unsigned char packet_number = *(--offset);
	int payload_length = length - GOLD_HEADER_SIZE;
	if(frame_number!=framenum) {
		total_packet_number = totalpacketnum;
		frame_number = framenum;
		frame_length = 0;
		packet_count = 0;
		sequence_number = 0;
		isComplete = false;
		memset(receive_check_list, 0, sizeof(bool)*total_packet_number);
	}

	if(receive_check_list[packet_number] == false) {
		memcpy(read_buffer+packet_number*GOLD_PAYLOAD_SIZE, buffer, payload_length);
		receive_check_list[packet_number] = true;
		frame_length += payload_length;
		packet_count++;
	}
	if(recovery_bit) {
		if(total_packet_number == (packet_number+1)) {
			unsigned char* temp = request_buffer;
			for(int i=0;i<total_packet_number;i++) {
				if(receive_check_list[i]==false)
					*(temp++) = i;
			}
			if(temp!=request_buffer) {
				*(temp++) = frame_number;
				*(temp++) = REQUEST_FRAME_PACKET;
				*(temp++) = stream_id;
				*(temp++) = STREAM_RECOVERY;
                request_buffer_length = temp - request_buffer;
				return PACKET_LOSS;
			}
		}
		return PACKET_END;
	}

	if(sequence_number!=packet_number) {
		unsigned char* temp = request_buffer;

		while( sequence_number < packet_number )
			*(temp++) = sequence_number++;

		*(temp++) = frame_number;
		*(temp++) = REQUEST_FRAME_PACKET;
		*(temp++) = stream_id;
		*(temp++) = STREAM_RECOVERY;
		request_buffer_length = temp - request_buffer;
        sequence_number = packet_number+1;
		return PACKET_LOSS;
	}
	else {
		sequence_number++;
	}
	if(total_packet_number == (packet_number+1))
		return PACKET_END;
	return PACKET_SKIP;
}

bool GFrameRecvHelper::isCompleted() {
	if(isComplete)
		return true;
	if(total_packet_number==packet_count) {
		request_buffer[0] = frame_number;
		request_buffer[1] = REQUEST_FRAME_COMPLETE;
		request_buffer[2] = stream_id;
		request_buffer[3] = STREAM_RECOVERY;
		request_buffer_length = 4;
		encodingLevel = read_buffer[--frame_length];
		isComplete = true;
		return true;
	}
	return false;
}

void GFrameRecvHelper::createRequestGFrame() {
	request_gframe_buffer[0] = request_level;
	request_gframe_buffer[1] = frame_number + 1;
	request_gframe_buffer[2] = REQUEST_GOLDFRAME;
	request_gframe_buffer[3] = stream_id;
    request_gframe_buffer[4] = STREAM_RECOVERY;
}

