//
//  DecryptHelper.cpp
//  BitLive
//
//  Created by keros on 9/17/15.
//
//

#include "DecryptHelper.h"

DecryptHelper::DecryptHelper()
:_key{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15},
_iv{1,2,3,4,5,6,7,8}
{
    ERR_load_crypto_strings();
}

DecryptHelper::~DecryptHelper()
{
    
    ERR_free_strings();
}

bool DecryptHelper::init()
{
    unsigned long err = 0;
    
    EVP_CIPHER_CTX_init(&_ctx);
    
    if(EVP_DecryptInit_ex(&_ctx, EVP_bf_cbc(), NULL, _key, _iv) != 1) {
        err = ERR_get_error();
        CCLOG("ERR: EVP_DecryptInit() - %s\n", ERR_error_string (err, NULL));
        return false;
    }
    
    return true;
}

int DecryptHelper::decrypt(unsigned char* plainText, unsigned char* cipherText, unsigned int cipherTextLen)
{
    unsigned long err = 0;
    
    init();
    
    if(EVP_DecryptUpdate(&_ctx, plainText, &_outLen, cipherText, cipherTextLen) != 1) {
        err = ERR_get_error();
        CCLOG("ERR : EVP_DecryptUpdate() - %s\n", ERR_error_string(err, NULL));
        return false;
    }
    if(EVP_DecryptFinal_ex(&_ctx, plainText + _outLen, &_tmpLen) != 1) {
        err = ERR_get_error();
        CCLOG("ERR : EVP_DecryptUpdate() - %s\n", ERR_error_string(err, NULL));
        return false;
    }
    EVP_CIPHER_CTX_cleanup(&_ctx);
    return _outLen+_tmpLen;
}

