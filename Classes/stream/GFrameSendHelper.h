#ifndef __GFRAME_SEND_HELPER_H_
#define __GFRAME_SEND_HELPER_H_

#include <time.h>
#include "cocos2d.h"
#include "webrtc/base/criticalsection.h"
#include "util/define.h"
#include "stream/StreamPacket.h"

class GFrameSendHelper {
public:
    rtc::CriticalSection criticalsection;
	int total_packet_count;
	unsigned char stream_id;
	unsigned char frame_number;
	unsigned char count;
	bool request_frame, complete_frame;
private:
	StreamPacket* packet[MAX_KEYFRAME_NUM];

public:
	GFrameSendHelper();
	~GFrameSendHelper();
	unsigned char* getBuffer(int index);
	int getLength(int index);
	StreamPacket* getPacket(int index);
	StreamPacket* getLastPacket();
	StreamPacket* getRecoveryPacket(int index);
	void setFrameLength(int length);
	StreamPacket* setPacket(unsigned char* src_buffer, int size);
	void setLevel(unsigned char* src_buffer, int size, unsigned char framedelay);
};

#endif
