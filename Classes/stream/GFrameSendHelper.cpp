#include"GFrameSendHelper.h"

GFrameSendHelper::GFrameSendHelper() {
    for(int i=0;i<MAX_KEYFRAME_NUM;i++) {
    	packet[i] = new StreamPacket(MAX_PACKET_VIDEO);
    }
    frame_number = 0;
    total_packet_count = 0;
    request_frame = complete_frame = false;
}

GFrameSendHelper::~GFrameSendHelper() {
    for(int i=0;i<MAX_KEYFRAME_NUM;i++) {
		if(packet[i]) {
			delete packet[i];
			packet[i] = NULL;
		}
    }
}

unsigned char* GFrameSendHelper::getBuffer(int index) {
	return packet[index]->data;
}
int GFrameSendHelper::getLength(int index) {
	return packet[index]->length;
}

StreamPacket* GFrameSendHelper::getPacket(int index) {
	return packet[index];
}

StreamPacket* GFrameSendHelper::getLastPacket() {
	if(packet[count]) {
		return packet[count]->checkFlag(true);
	}
	return NULL;
}

StreamPacket* GFrameSendHelper::getRecoveryPacket(int index) {
	if(packet[index]) {
		return packet[index]->checkFlag(true);
	}
	return NULL;
}

StreamPacket* GFrameSendHelper::setPacket(unsigned char* src_buffer, int size) {
	return packet[count]->copy(src_buffer, size, stream_id, frame_number, total_packet_count, count);
}

void GFrameSendHelper::setLevel(unsigned char* src_buffer, int size, unsigned char level) {
	src_buffer[size] = level;
}

