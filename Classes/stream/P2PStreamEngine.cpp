#include "P2PStreamEngine.h"

P2PStreamEngine::P2PStreamEngine()
:gold_recv_helper(nullptr),
gold_send_helper(nullptr),
artwork_recv_helper(nullptr),
artwork_send_helper(nullptr)
{
	write_video_packet = new StreamPacket(MAX_PACKET_VIDEO);
    rpacket = new unsigned char[MAX_PACKET_SIZE];
    video_frame_buffer = new unsigned char[MAX_FRAME_VIDEO];
    voice_encode_buffer = new unsigned char[MAX_PACKET_VOICE];
    voice_decode_buffer = new unsigned char[MAX_PACKET_VOICE];
    packet_count = 0;
    read_offset = 0;

	gold_send_helper = new GFrameSendHelper();
	artwork_send_helper = new GFrameSendHelper();
    wpacket_number = 0;
}

P2PStreamEngine::~P2PStreamEngine() {

    if(rpacket)
    	delete rpacket;
    if(write_video_packet)
    	delete write_video_packet;
    if(video_frame_buffer)
        delete video_frame_buffer;
    if(voice_encode_buffer)
        delete voice_encode_buffer;
    if(voice_decode_buffer)
        delete voice_decode_buffer;

}
void P2PStreamEngine::init()
{
    release();
    gold_recv_helper = new GFrameRecvHelper(STREAM_GOLD, MAX_KEYFRAME_NUM);
    artwork_recv_helper = new GFrameRecvHelper(STREAM_ARTWORK, MAX_ARTWORK_FRAME_NUM);
}

void P2PStreamEngine::release()
{
    if(gold_recv_helper) {
        delete gold_recv_helper;
        gold_recv_helper = NULL;
    }
    if(artwork_recv_helper) {
        delete artwork_recv_helper;
        artwork_recv_helper = NULL;
    }
}

int P2PStreamEngine::onRecvPacket(unsigned char* current_offset, size_t read_bytes, Stream *stream) {
	bool end_bit;
	unsigned char *temp;
	unsigned char rstream_id;
	unsigned char rpacket_number;
    
    temp = current_offset + read_bytes -1;
    rstream_id = *(temp) & 0x3F;
    
    switch(rstream_id) {
    case STREAM_VP8:
    {
#ifdef LOSS_SKIP
        if(gold_recv_helper->request_gframe) {
            if(gold_recv_helper->request_gframe==2) {
                if(requestFrame(gold_recv_helper)<0)
                    return SOCKET_CLOSED;
                gold_recv_helper->request_gframe = 1;
            }
            else
                gold_recv_helper->request_gframe++;
            return STREAM_SKIP;
        }
        rpacket_number = *(temp - 1);
#else
        rpacket_number = *(temp - 1);
        if(gold_recv_helper->request_gframe) {
            if(requestFrame(gold_recv_helper)<0)
                return UDP_SOCKET_CLOSED;
            start_bit = *(temp)&0x40;
            if(start_bit) {
                packet_count=rpacket_number;
            }
        }
#endif
        if(packet_count==rpacket_number) {
            memcpy(video_frame_buffer+read_offset, current_offset, read_bytes - VIDEO_HEADER_SIZE);
            read_offset += read_bytes - VIDEO_HEADER_SIZE;
            packet_count++;
        }
        else {	// detect a packet loss
            
            if(requestFrame(gold_recv_helper)<0)
                return SOCKET_CLOSED;
            gold_recv_helper->request_gframe = 1;
            read_offset = 0;
        }

        end_bit = *(temp)&0x80;
        if(end_bit) {
            stream->setBuffer(video_frame_buffer , read_offset);
            read_offset = 0;
            return rstream_id;
        }
        break;
    }
    case STREAM_GOLD:
    {
        int ret;
        gold_recv_helper->request_gframe = 0;
        if( (ret = gold_recv_helper->insert(current_offset, read_bytes)) == PACKET_END) {
            if(gold_recv_helper->isCompleted()) {
                if(sendGframeComplete(gold_recv_helper)<0)
                    return SOCKET_CLOSED;
                stream->setBuffer(gold_recv_helper->read_buffer , gold_recv_helper->frame_length);
                packet_count = 0;
                return rstream_id;
            }
            else {
                if(requestRecovery(gold_recv_helper)<0)
                    return SOCKET_CLOSED;
            }
        }
        else if(ret == PACKET_LOSS){
            if(requestRecovery(gold_recv_helper)<0)
                return SOCKET_CLOSED;
        }
        break;
    }
    case STREAM_RECOVERY:
    {
        unsigned char streamid = *(temp-1);
        unsigned char request = *(temp-2);
        if( request == REQUEST_FRAME_PACKET){
            unsigned char framenumber = *(temp-3);
            if(streamid==STREAM_GOLD) {
                if(sendFrameRecoveryPacket(gold_send_helper, current_offset, read_bytes - RECOVERY_HEADER_SIZE, framenumber)<0)
                    return SOCKET_CLOSED;
            }
        }
        else if(request==REQUEST_GOLDFRAME) {
            gold_send_helper->criticalsection.Enter();
            if(!gold_send_helper->request_frame && gold_send_helper->frame_number != current_offset[1]) {
                gold_send_helper->request_frame = true;
                stream->setBuffer(current_offset, read_bytes);
                gold_send_helper->criticalsection.Leave();
                return rstream_id;
            }
            gold_send_helper->criticalsection.Leave();
        }
        else if(request==REQUEST_FRAME_COMPLETE) {
            gold_send_helper->criticalsection.Enter();
            if( !gold_send_helper->complete_frame && gold_send_helper->frame_number == current_offset[0]) {
                gold_send_helper->complete_frame = true;
                stream->setBuffer(current_offset, read_bytes);
                gold_send_helper->criticalsection.Leave();
                return rstream_id;
            }
            gold_send_helper->criticalsection.Leave();
        }
        break;
    }
    case STREAM_ARTWORK:
    {
        break;
    }
    case STREAM_COMMAND:
    {
        if( (*current_offset)>10) {
            *(current_offset+read_bytes - SINGLE_HEADER_SIZE) = 0;
            stream->setBuffer(current_offset, read_bytes);
            return rstream_id;
        }
        break;
    }
    case STREAM_OPUS:
    case STREAM_MP3:
    {
        stream->setBuffer(current_offset, read_bytes - SINGLE_HEADER_SIZE);
        return rstream_id;
    }
    case STREAM_ERROR:
    {
        CCLOG("STREAM_ERROR %x, length : %d", rstream_id, read_bytes);
        break;
    }
    default:
        CCLOG("receive a packet with wrong stream id %x, length : %d", rstream_id, read_bytes);
        break;
    }

	return STREAM_SKIP;
}

int P2PStreamEngine::requestFrame(GFrameRecvHelper *helper) {
    helper->createRequestGFrame();
    CCLOG(">>request G %d", gold_recv_helper->request_gframe_buffer[1]);
    gold_send_helper->criticalsection.Enter();
    conn_->Send(helper->request_gframe_buffer, helper->request_gframe_buffer_length, options);
    gold_send_helper->criticalsection.Leave();
	return 1;
}

int P2PStreamEngine::requestRecovery(GFrameRecvHelper *helper) {
    gold_send_helper->criticalsection.Enter();
    conn_->Send(helper->request_buffer, helper->request_buffer_length, options);
    gold_send_helper->criticalsection.Leave();
	return 1;
}

int P2PStreamEngine::sendGframeComplete(GFrameRecvHelper *helper) {
    gold_send_helper->criticalsection.Enter();
    conn_->Send(helper->request_buffer, helper->request_buffer_length, options);
    gold_send_helper->criticalsection.Leave();
	return 1;
}

int P2PStreamEngine::sendCommand(const char *command) {
    int size = strlen(command);
	strcpy((char*)command_buffer, command);
    command_buffer[size] = STREAM_COMMAND;
    gold_send_helper->criticalsection.Enter();
    if(conn_)
        conn_->Send(command_buffer, size+1, options);
    gold_send_helper->criticalsection.Leave();
	return 1;
}

void P2PStreamEngine::sendSinglePacket(unsigned char stream_id, unsigned char *buffer, int size) {
    buffer[size] = stream_id;
    
    gold_send_helper->criticalsection.Enter();
    conn_->Send(buffer, size+1, options);
    gold_send_helper->criticalsection.Leave();
}

void P2PStreamEngine::sendMultiPacket(unsigned char stream_id, unsigned char *buffer, int size) {
	unsigned char *header;
    
    gold_send_helper->criticalsection.Enter();
	while(size>VIDEO_PAYLOAD_SIZE) {
		header = buffer + VIDEO_PAYLOAD_SIZE;
		memcpy(backup[stream_id], header, VIDEO_BACKUP_SIZE);

		*(header) = wpacket_number++;
        *(header+1) = stream_id;
        conn_->Send(buffer, VIDEO_PAYLOAD_SIZE+VIDEO_HEADER_SIZE, options);
		memcpy(header, backup[stream_id], VIDEO_BACKUP_SIZE);
		size -= VIDEO_PAYLOAD_SIZE;
		buffer += VIDEO_PAYLOAD_SIZE;
	}

	header = buffer + size;
	*(header) = wpacket_number++;
    *(header+1) = stream_id | 0x80;
    conn_->Send(buffer, size+VIDEO_HEADER_SIZE, options);
    gold_send_helper->criticalsection.Leave();
}

int P2PStreamEngine::sendFramePacket(GFrameSendHelper *helper,unsigned char *buffer, int size, unsigned char framecount, unsigned char level) {
	StreamPacket *packet;
	helper->setLevel(buffer, size++, level);
	helper->request_frame = false;
	helper->complete_frame = false;
	helper->count = 0;
	helper->stream_id = STREAM_GOLD;
	helper->frame_number = framecount;
	helper->total_packet_count = size/GOLD_PAYLOAD_SIZE;
	if(size%GOLD_PAYLOAD_SIZE)
		helper->total_packet_count++;
    
    helper->criticalsection.Enter();
	while(size>GOLD_PAYLOAD_SIZE) {
        packet = helper->setPacket(buffer, GOLD_PAYLOAD_SIZE);
        
        conn_->Send(packet->data, packet->length, options);
		size -= GOLD_PAYLOAD_SIZE;
		buffer += GOLD_PAYLOAD_SIZE;
		helper->count++;
	}

    packet = helper->setPacket(buffer, size);
    
    conn_->Send(packet->data, packet->length, options);
    wpacket_number = 0;
    helper->criticalsection.Leave();
	return size;
}

int P2PStreamEngine::sendResampling(GFrameSendHelper *helper,unsigned char *buffer, int size, unsigned char framecount, unsigned char level) {
	StreamPacket *packet;
    
    helper->criticalsection.Enter();
	helper->setLevel(buffer, size++, level);
	helper->request_frame = false;
	helper->complete_frame = false;
	helper->count = 0;
	helper->stream_id = STREAM_GOLD;
	helper->frame_number = framecount;
	helper->total_packet_count = size/GOLD_PAYLOAD_SIZE;
	if(size%GOLD_PAYLOAD_SIZE)
		helper->total_packet_count++;

	while(size>GOLD_PAYLOAD_SIZE) {
		packet = helper->setPacket(buffer, GOLD_PAYLOAD_SIZE);
        conn_->Send(packet->data, packet->length, options);
		size -= GOLD_PAYLOAD_SIZE;
		buffer += GOLD_PAYLOAD_SIZE;
		helper->count++;
	}

	packet = helper->setPacket(buffer, size);
    conn_->Send(packet->data, packet->length, options);
    wpacket_number = 0;
    helper->criticalsection.Leave();
	return size;
}

int P2PStreamEngine::sendLastFramePacket(GFrameSendHelper *helper) {
    StreamPacket *packet;
    helper->criticalsection.Enter();
    if(helper==NULL) {
        helper->criticalsection.Leave();
		return 0;
	}
    if( (packet = helper->getLastPacket())==NULL) {
        helper->criticalsection.Leave();
		return 0;
	}
    conn_->Send(packet->data, packet->length, options);
    helper->criticalsection.Leave();
	return 1;
}


int P2PStreamEngine::sendFrameRecoveryPacket(GFrameSendHelper *helper, unsigned char *buffer, int count, unsigned char framenumber) {
    helper->criticalsection.Enter();
    if(helper->frame_number!=framenumber) {
        helper->criticalsection.Leave();
		return 0;
	}
	StreamPacket *packet;
	while(count>0) {
		packet = helper->getRecoveryPacket(*(buffer++));
        conn_->Send(packet->data, packet->length, options);
		count--;
    }
    helper->criticalsection.Leave();

	return 1;
}
