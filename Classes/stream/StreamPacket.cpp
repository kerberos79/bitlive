#include "StreamPacket.h"

StreamPacket::StreamPacket() {
	data = new unsigned char[MAX_PACKET_SIZE];
}

StreamPacket::StreamPacket(int buffersize) {
	data = new unsigned char[buffersize];
}

StreamPacket::StreamPacket(unsigned char* buffer, int buffersize) {
    data = new unsigned char[buffersize];
    memcpy(data, buffer, buffersize);
    length = buffersize;
}

StreamPacket::~StreamPacket() {
	if(data)
	delete data;
}

StreamPacket* StreamPacket::copy(unsigned char* buffer, int payload_length,
					unsigned char streamid, unsigned char packetnum) {
	memcpy(data, buffer, payload_length);
	data[payload_length] = packetnum;
	data[payload_length+1] = streamid;
	length = payload_length + 2;
	return this;
}

StreamPacket* StreamPacket::copyWithFlag(unsigned char* buffer, int payload_length,
					unsigned char streamid, unsigned char packetnum, unsigned char flag) {
	memcpy(data, buffer, payload_length);
	data[payload_length] = packetnum;
	data[payload_length+1] = streamid | flag;
	length = payload_length + 2;
	return this;
}

StreamPacket* StreamPacket::copy(unsigned char* buffer, int payload_length, unsigned char streamid,
					unsigned char framecount, unsigned char totalpacketnum, unsigned char packetnum) {
	memcpy(data, buffer, payload_length);
	data[payload_length] = packetnum;
	data[payload_length+1] = totalpacketnum;
	data[payload_length+2] = framecount;
	data[payload_length+3] = streamid;
	length = payload_length + 4;
	return this;
}

StreamPacket* StreamPacket::copyWithFlag(unsigned char* buffer, int payload_length, unsigned char streamid,
					unsigned char framecount, unsigned char totalpacketnum, unsigned char packetnum) {
	memcpy(data, buffer, payload_length);
	data[payload_length] = packetnum;
	data[payload_length+1] = totalpacketnum;
	data[payload_length+2] = framecount;
	data[payload_length+3] = streamid | 0x80;
	length = payload_length + 4;
	return this;
}

StreamPacket* StreamPacket::checkFlag(bool flag) {
	if(flag)
		data[length-1] |= 0x80;
	else
		data[length-1] &= 0x7F;
	return this;
}

