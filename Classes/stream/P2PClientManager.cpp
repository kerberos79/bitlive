#include "P2PClientManager.h"

const char LN_ALLOCATE[] = "allocate";
const buzz::StaticQName QN_GINGLE_P2P_ALLOCATE = { NS_GINGLE_P2P, LN_ALLOCATE };
const buzz::StaticQName QN_PRIVATEIP = { cricket::NS_EMPTY, "p0" };
const buzz::StaticQName QN_PRIVATEPORT = { cricket::NS_EMPTY, "p0t" };
const buzz::StaticQName QN_PUBLICIP = { cricket::NS_EMPTY, "p1" };
const buzz::StaticQName QN_PUBLICPORT = { cricket::NS_EMPTY, "p1t" };
const buzz::StaticQName QN_NETWORK_TYPE = { cricket::NS_EMPTY, "net" };
const buzz::StaticQName QN_NETWORK_PROVIDER = { cricket::NS_EMPTY, "ntp" };
const buzz::StaticQName QN_ORDER = { cricket::NS_EMPTY, "or" };
const buzz::StaticQName QN_PROTO = { cricket::NS_EMPTY, "pro" };
const buzz::StaticQName QN_REMOTEIP = { cricket::NS_EMPTY, "ip" };
const buzz::StaticQName QN_REMOTEPORT = { cricket::NS_EMPTY, "pt" };
const buzz::StaticQName QN_TOKENID = { cricket::NS_EMPTY, "tkn" };
const buzz::StaticQName QN_RESULT = { cricket::NS_EMPTY, "rs" };
const buzz::StaticQName QN_GUEST = { cricket::NS_EMPTY, "gu" };

const int P2P_TRY_TIMEOUT = 5000;  // 10sec
namespace {

const char* DescribeStatus(buzz::Status::Show show, const std::string& desc) {
	switch (show) {
	case buzz::Status::SHOW_XA:
		return desc.c_str();
	case buzz::Status::SHOW_ONLINE:
		return "online";
	case buzz::Status::SHOW_AWAY:
		return "away";
	case buzz::Status::SHOW_DND:
		return "do not disturb";
	case buzz::Status::SHOW_CHAT:
		return "ready to chat";
	default:
		return "offline";
	}
}

std::string GetWord(const std::vector<std::string>& words, size_t index,
		const std::string& def) {
	if (words.size() > index) {
		return words[index];
	} else {
		return def;
	}
}

int GetInt(const std::vector<std::string>& words, size_t index, int def) {
	int val;
	if (words.size() > index && talk_base::FromString(words[index], &val)) {
		return val;
	} else {
		return def;
	}
}

}

int P2PClientManager::CreateHostChannel(const char* remote, const char* token) {

	setInvitation(remote, token);
	worker_thread_->PostDelayed(200, this, MSG_CREATECHANNEL);
	pthread_mutex_lock(&lock);
	pthread_cond_wait(&condition, &lock);
	pthread_mutex_unlock(&lock);

	if(channel==NULL || channel->IsEmptySocket()) {
		return 0;
	}
	channel->SetConnected(false);
	if(channel->IsTcp()) {
		LOGX("channel->WaitConnect()");
		if(!channel->WaitConnect()) {
			LOGX("fail to channel->waitConnect()");
			return 0;
		}
		LOGX("channel->Connected()");
		worker_thread_->Post(this, MSG_P2P_CONNECTED);
		if(!channel->WaitAccept()) {
			LOGX("fail to channel->WaitAccept()");
			return 0;
		}
		channel->SendSync();
		if(!channel->WaitSync()) {
			LOGX("fail to channel->WaitSync()");
			return 0;
		}
		worker_thread_->Post(this, MSG_P2P_ACCEPCTED);
		LOGX("success to channel->Accepted()");
	}
	else {
		LOGX("channel->WaitConnect()");
		if(!channel->WaitConnect()) {
			LOGX("fail to channel->waitConnect()");
			requestRelayPort(to);
			pthread_mutex_lock(&lock);
			pthread_cond_wait(&condition, &lock);
			pthread_mutex_unlock(&lock);
		}
		channel->SendSync();
		if(!channel->WaitSync()) {
			LOGX("fail to channel->WaitSync()");
			return 0;
		}
		worker_thread_->Post(this, MSG_P2P_ACCEPCTED);
	}
	if(jni_callback)
		jni_callback->OnConnected();
	else
		return 0;
    return 1;
}

int P2PClientManager::CreateGuestChannel() {
	if(channel==NULL || channel->IsEmptySocket()) {
		return 0;
	}
	channel->SetConnected(false);
	if(channel->IsTcp()) {
		LOGX("channel->WaitConnect()");
		if(!channel->WaitConnect()) {
			LOGX("fail to channel->waitConnect()");
			return 0;
		}
		LOGX("channel->Connected()");
		worker_thread_->Post(this, MSG_P2P_CONNECTED);

		if(!channel->WaitAccept()) {
			LOGX("fail to channel->WaitAccept()");
			return 0;
		}

		channel->SendSync();
		if(!channel->WaitSync()) {
			LOGX("fail to channel->WaitSync()");
			return 0;
		}
		worker_thread_->Post(this, MSG_P2P_ACCEPCTED);
		LOGX("success to channel->accepted()");
	}
	else {
		LOGX("channel->WaitConnect()");
		if(!channel->WaitConnect()) {
			LOGX("fail to channel->waitConnect()");
			pthread_mutex_lock(&lock);
			pthread_cond_wait(&condition, &lock);
			pthread_mutex_unlock(&lock);
		}
		LOGX("channel->Connected()");
		channel->SendSync();
		if(!channel->WaitSync()) {
			LOGX("fail to channel->WaitSync()");
			return 0;
		}
		worker_thread_->Post(this, MSG_P2P_ACCEPCTED);
	}
	if(jni_callback)
		jni_callback->OnConnected();
	else
		return 0;
    return 1;
}

int P2PClientManager::ReqeustUpnp() {
}

int P2PClientManager::ReqeustNatpmp() {
	channel->NatpmpRequestMapping(7000, 7000, 2, 3600);
	return 1;
}

void P2PClientManager::CloseChannel() {
    pthread_cond_signal(&condition);
	if(channel)
		channel->CloseSocket();
}

void P2PClientManager::ReleaseChannel() {
	if(channel)
		channel->ReleaseSocket();
}

void P2PClientManager::SendP2PConnect(const std::string& to) {

	SendChat(to, 0);
}

void P2PClientManager::SendData(const char* data) {
	LOG(LS_INFO)<<"P2PClientManager::SendData!!";
	//const char* data = str.c_str();
	int len = static_cast<int>(strlen(data));
//	this->channel->SendPacket(data, len);
}

void P2PClientManager::SendData(const char* data, int len) {
	LOG(LS_INFO)<<"P2PClientManager::SendData!!";
//	this->channel->SendPacket(data, len);
}

void P2PClientManager::ParseLine(const std::string& line) {
	std::vector<std::string> words;
	int start = -1;
	int state = 0;
	for (int index = 0; index <= static_cast<int>(line.size()); ++index) {
		if (state == 0) {
			if (!isspace(line[index])) {
				start = index;
				state = 1;
			}
		} else {
			ASSERT(state == 1);
			ASSERT(start >= 0);
			if (isspace(line[index])) {
				std::string word(line, start, index - start);
				words.push_back(word);
				start = -1;
				state = 0;
			}
		}
	}

	// Global commands
	const std::string& command = GetWord(words, 0, "");
	if (command == "quit") {
		LOGX("quit %d", __LINE__);
		Quit();
	} else if (command == "call") {
		std::string to = GetWord(words, 1, "");
		MakeCallTo(to);
	} else if (command == "send") {
		buzz::Jid jid(words[1]);
		SendChat(words[1], words[2]);
	} else if (command == "p2p") {
		to = words[1];
		worker_thread_->PostDelayed(100, this, MSG_CREATECHANNEL);
	} else if (command == "go") {
		std::string word =  words[1];
		const char* data = word.c_str();
		int len = static_cast<int>(strlen(data));
//		channel->SendPacket(data, len);
	} else if (command == "stop") {

	}
}

void P2PClientManager::Quit() {

	if(channel) {
	    pthread_cond_signal(&condition);
		channel->CloseSocket();
		LOGX("channel->CloseSocket()");
	}
	if(jni_callback) {
		delete jni_callback;
		jni_callback = NULL;
	}
	if (channel_is_not_work_ && talk_base::Thread::Current()!=NULL) {
		talk_base::Thread::Current()->Quit();
	}
}

P2PClientManager::P2PClientManager(buzz::XmppClient* xmpp_client, const char *ipaddress) :
		xmpp_client_(xmpp_client), worker_thread_(NULL), incoming_call_(false) {
    xmpp_client_->SignalStateChange.connect(this, &P2PClientManager::OnStateChange);
	// LOGX("P2PClientManager clear");
	ready_candidates_.clear();
	remote_candidates_.clear();
    channel_is_not_work_ = true;
	env = NULL;
	local_ip = ipaddress;
	channel = new P2PChannel();
    pthread_mutexattr_t mAttr;
    pthread_mutexattr_settype(&mAttr, PTHREAD_MUTEX_RECURSIVE_NP);
    pthread_mutex_init(&lock, &mAttr);
    pthread_mutex_init(&remote_user_lock, &mAttr);
    pthread_cond_init(&condition, &mAttr);
}

P2PClientManager::~P2PClientManager() {
	if(channel) {
		delete channel;
		channel = NULL;
	}
	if(jni_callback) {
		delete jni_callback;
		jni_callback = NULL;
	}

	delete worker_thread_;
    pthread_cond_signal(&condition);
    pthread_cond_destroy(&condition);
	pthread_mutex_destroy (&lock);
	pthread_mutex_destroy (&remote_user_lock);
}

void P2PClientManager::OnStateChange(buzz::XmppEngine::State state) {
	switch (state) {
	case buzz::XmppEngine::STATE_START:
		LOGX("connecting...");
		break;
	case buzz::XmppEngine::STATE_OPENING:
		LOGX("logging in...");
		break;
	case buzz::XmppEngine::STATE_OPEN:
		jni_callback->OnLoggedIn();
	    InitMedia();
		break;

	case buzz::XmppEngine::STATE_CLOSED:
		buzz::XmppEngine::Error error = xmpp_client_->GetError(NULL);

		jni_callback->OnLogOut(strerror(error).c_str());
		LOGX("quit %d", __LINE__);
		Quit();
		break;
	}
}

void P2PClientManager::InitMedia() {
	//init status.
	SetAvailable(getXmppClient()->jid(), &my_status_);
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_PRESENCE);
	stanza->AddElement(new buzz::XmlElement(buzz::QN_STATUS));
	stanza->AddText(my_status_.status(), 1);
	getXmppClient()->SendStanza(stanza);

	worker_thread_ = new talk_base::Thread();
	// The worker thread must be started here since initialization of
	// the ChannelManager will generate messages that need to be
	// dispatched by it.
	worker_thread_->Start();
	// TODO: It looks like we are leaking many objects. E.g.
	// |network_manager_| is never deleted.
	xmpp_task_ = new CustomXmppTask(xmpp_client_);
	xmpp_task_->SignalHandleCandidate.connect(this,
			&P2PClientManager::HandleCandidate);
	xmpp_task_->SignalHandleTransport.connect(this,
			&P2PClientManager::HandleTransport);
	xmpp_task_->SignalHandleChat.connect(this, &P2PClientManager::HandleChat);
	xmpp_client_->AddXmppTask(xmpp_task_, buzz::XmppEngine::HL_ALL);
	delete stanza;

//	channel->UpnpRequestMapping(local_ip, "7002", "7002", "TCP");
}

void P2PClientManager::OnMessage(talk_base::Message *msg) {
	switch (msg->message_id) {
	case (MSG_CREATECHANNEL): {
		LOG(LS_INFO)<<"CREATE-CHANNEL!!";
	    if(channel->Allocate(local_ip))
	    {
		    start_time_ = talk_base::Time();
		    pthread_cond_signal(&condition);
			worker_thread_->PostDelayed(1000,this, MSG_P2P_CONNECT);
	    }
		WriteCandidate(to, channel->local_info);
	    break;
	}
	case (MSG_DESTROYCHANNEL): {
		if(channel) {
			channel->CloseSocket();
		}
		break;
	}
	case MSG_P2P_CANDIDATE :
	{
	    start_time_ = talk_base::Time();
	    if(channel->IceCandidate(network_type, network_provider, local_ip))
	    {
	    }
	    else
	    {
			requestConnection(to, channel->trans_info);
	    }
		worker_thread_->PostDelayed(400,this, MSG_P2P_CONNECT);
	    jni_callback->OnStartGuestStreamReceiver();
		break;
	}
	case MSG_P2P_REQUEST_CONNECTION :
	{
	    start_time_ = talk_base::Time();
		channel->ReleaseSocket();
		channel->CreateClient();
	    pthread_cond_signal(&condition);
		worker_thread_->PostDelayed(400,this, MSG_P2P_CONNECT);
		break;
	}
	case MSG_P2P_CONNECT :
	{
	    pthread_cond_signal(&condition);

		if(talk_base::TimeSince(start_time_) <= P2P_TRY_TIMEOUT) {
			channel->SendConnect();
			if(!channel->IsTcp())
				worker_thread_->PostDelayed(400, this, MSG_P2P_CONNECT);
			LOGX("MSG_P2P_CONNECT");
		}
		else
			worker_thread_->Post(this, MSG_P2P_TIMEOUT);

		break;
	}
	case MSG_P2P_CONNECTED :
	{
		worker_thread_->Clear(this, MSG_P2P_CONNECT);
		channel->SendAccept();
//		worker_thread_->PostDelayed(200, this, MSG_P2P_CONNECTED);
		LOGX("MSG_P2P_CONNECTED");
		break;
	}
	case MSG_P2P_ACCEPCTED :
	{
		worker_thread_->Clear(this, MSG_P2P_CONNECT);
		channel->SetConnected(true);
		LOGX("MSG_P2P_ACCEPCTED");
		break;
	}
	case MSG_P2P_TIMEOUT :
	{
		LOGX("MSG_P2P_TIMEOUT");
		worker_thread_->Clear(this, MSG_P2P_CONNECT);
		if(!channel->IsConnected()) {
//			jni_callback->OnTimeout();
			channel->CloseSocket();
		}
		break;
	}
	case MSG_FAIL_CONNECTION:
	{
		channel->ReleaseSocket();
	    pthread_cond_signal(&condition);
		break;
	}
	}
}

void P2PClientManager::Input(const char * data, int len) {

}

void P2PClientManager::OnReadPacket(TransportChannel* channel,
		const char* data, size_t len) {
	std::string word = data;
	LOG(LS_INFO) << "OnReadPacket :"<< len;
}

void P2PClientManager::SendInvitation(const std::string& remote, const std::string& tokenId) {
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
	stanza->AddAttr(buzz::QN_TO, remote +"@" + domain_name);
	stanza->AddAttr(buzz::QN_ID, tokenId);
	stanza->AddAttr(buzz::QN_TYPE, "chat");
	buzz::XmlElement* body = new buzz::XmlElement(buzz::QN_BODY);
	std::string text = "I:"+tokenId+":"+user_id;
	body->SetBodyText(text);
	stanza->AddElement(body);
	xmpp_client_->SendStanza(stanza);

	setInvitation(remote, tokenId);
	LOG(INFO)<<stanza->Str();
	delete stanza;
}

void P2PClientManager::SendCancelation(const std::string& remote, const std::string& tokenId) {
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
	stanza->AddAttr(buzz::QN_TO, remote +"@"+ domain_name);
	stanza->AddAttr(buzz::QN_ID, talk_base::CreateRandomString(16));
	stanza->AddAttr(buzz::QN_TYPE, "chat");
	buzz::XmlElement* body = new buzz::XmlElement(buzz::QN_BODY);
	std::string text = "C:"+tokenId+":"+user_id;
	body->SetBodyText(text);
	stanza->AddElement(body);
	xmpp_client_->SendStanza(stanza);

	if(compareInvitation(remote, tokenId))
		setInvitation("", "");
	LOG(INFO)<<stanza->Str();
	delete stanza;
}

void P2PClientManager::SendRefusal(const std::string& remote, const std::string& tokenId) {
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
	stanza->AddAttr(buzz::QN_TO, remote +"@"+ domain_name);
	stanza->AddAttr(buzz::QN_ID, talk_base::CreateRandomString(16));
	stanza->AddAttr(buzz::QN_TYPE, "chat");
	buzz::XmlElement* body = new buzz::XmlElement(buzz::QN_BODY);
	std::string text = "R:"+tokenId+":"+user_id;
	body->SetBodyText(text);
	stanza->AddElement(body);
	xmpp_client_->SendStanza(stanza);

	LOG(INFO)<<stanza->Str();
	delete stanza;
}

void P2PClientManager::SendBusy(const std::string& remote, const std::string& tokenId) {
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
	stanza->AddAttr(buzz::QN_TO, remote +"@"+ domain_name);
	stanza->AddAttr(buzz::QN_ID, talk_base::CreateRandomString(16));
	stanza->AddAttr(buzz::QN_TYPE, "chat");
	buzz::XmlElement* body = new buzz::XmlElement(buzz::QN_BODY);
	std::string text = "B:"+tokenId+":"+user_id;
	body->SetBodyText(text);
	stanza->AddElement(body);
	xmpp_client_->SendStanza(stanza);

	LOG(INFO)<<stanza->Str();
	delete stanza;
}

void P2PClientManager::SendChat(const std::string& remote, const std::string msg) {
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
	stanza->AddAttr(buzz::QN_TO, remote);
	stanza->AddAttr(buzz::QN_ID, talk_base::CreateRandomString(16));
	stanza->AddAttr(buzz::QN_TYPE, "chat");
	buzz::XmlElement* body = new buzz::XmlElement(buzz::QN_BODY);
	body->AddAttr(buzz::QN_TYPE, "message");
	body->SetBodyText("M:"+user_id+":"+msg);
	stanza->AddElement(body);
	xmpp_client_->SendStanza(stanza);

	delete stanza;
}

void P2PClientManager::HandleChat(const buzz::XmlElement* stanza) {
	const buzz::XmlElement* error = stanza->FirstNamed(buzz::QN_ERROR);
	if(error!=NULL) {
		LOGX("HandleCandidate has an error");
		LOG(INFO)<<stanza->Str();
		return;
	}
	const buzz::XmlElement* elem = stanza->FirstElement();
	const std::string& from = stanza->Attr(buzz::QN_FROM);
	const std::string& type = stanza->Attr(buzz::QN_TYPE);
	jni_callback->OnMessage(elem->BodyText().c_str());
	LOG(INFO)<<"<<"<<stanza->Str();
}

void P2PClientManager::WriteCandidate(const std::string& remote, StunInfo *info) {
	buzz::XmlElement* elem = new buzz::XmlElement(QN_GINGLE_P2P_CANDIDATE);
	elem->SetAttr(buzz::QN_TYPE, info->type);
	elem->SetAttr(QN_PRIVATEIP, info->private_ip);
	elem->SetAttr(QN_PRIVATEPORT, info->private_port);
	elem->SetAttr(QN_PUBLICIP, info->public_ip);
	elem->SetAttr(QN_PUBLICPORT, info->public_port);
	elem->SetAttr(QN_ORDER, info->order);
	elem->SetAttr(QN_TOKENID, token_id);
	elem->SetAttr(QN_NETWORK_TYPE, network_type);
	elem->SetAttr(QN_NETWORK_PROVIDER, network_provider);
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
	stanza->AddAttr(buzz::QN_TO, remote+"@"+domain_name);
	stanza->AddAttr(buzz::QN_ID, talk_base::CreateRandomString(16));
	stanza->AddAttr(buzz::QN_TYPE, "chat");
	stanza->AddElement(elem);
	xmpp_client_->SendStanza(stanza);
	LOG(INFO)<<">>"<<stanza->Str();
	delete stanza;
}

void P2PClientManager::HandleCandidate(const buzz::XmlElement* stanza) {
	const buzz::XmlElement* error = stanza->FirstNamed(buzz::QN_ERROR);
	if(error!=NULL) {
		LOGX("HandleCandidate has an error");
		const std::string& from = stanza->Attr(buzz::QN_FROM);
		int pos = from.find("@");
		const std::string& remoteuser = from.substr(0,pos);
		jni_callback->OnFailedConnection(remoteuser.c_str());
		return;
	}
	const buzz::XmlElement* elem = stanza->FirstNamed(QN_GINGLE_P2P_CANDIDATE);
	const buzz::XmlElement* chatElement = stanza->FirstNamed(buzz::QN_MESSAGE);

	const std::string& from = stanza->Attr(buzz::QN_FROM);
	int pos = from.find("@");
	const std::string& remoteuser = from.substr(0,pos);
	const std::string& new_token_id = elem->Attr(QN_TOKENID);

	LOG(INFO)<<"<<"<<stanza->Str();
	if( compareInvitation(remoteuser, new_token_id)) {
		channel->remote_info->type = elem->Attr(buzz::QN_TYPE);
		channel->remote_info->order = elem->Attr(QN_ORDER);
		channel->remote_info->private_ip = elem->Attr(QN_PRIVATEIP);
		channel->remote_info->private_port = elem->Attr(QN_PRIVATEPORT);
		channel->remote_info->public_ip = elem->Attr(QN_PUBLICIP);
		channel->remote_info->public_port = elem->Attr(QN_PUBLICPORT);
		channel->remote_info->network_type = elem->Attr(QN_NETWORK_TYPE);
		channel->remote_info->network_provider = elem->Attr(QN_NETWORK_PROVIDER);

		worker_thread_->Post(this, MSG_P2P_CANDIDATE);
	}
	else {
		sendFailConnection(remoteuser);
	}
}

void P2PClientManager::sendFailConnection(const std::string& remote) {
	buzz::XmlElement* elem = new buzz::XmlElement(QN_GINGLE_P2P_TRANSPORT);
	elem->SetAttr(QN_RESULT, "false");
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
	stanza->AddAttr(buzz::QN_TO, remote+"@"+domain_name);
	stanza->AddAttr(buzz::QN_ID, talk_base::CreateRandomString(16));
	stanza->AddAttr(buzz::QN_TYPE, "chat");
	stanza->AddElement(elem);
	xmpp_client_->SendStanza(stanza);
	LOG(INFO)<<">>"<<stanza->Str();
	delete stanza;
}

void P2PClientManager::requestConnection(const std::string& remote, TransInfo *info) {
	buzz::XmlElement* elem = new buzz::XmlElement(QN_GINGLE_P2P_TRANSPORT);
	elem->SetAttr(QN_RESULT, "true");
	elem->SetAttr(QN_REMOTEIP, info->ip);
	elem->SetAttr(QN_REMOTEPORT, info->port);
	elem->SetAttr(QN_ORDER, info->order);
	elem->SetAttr(QN_PROTO, info->protocol);
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
	stanza->AddAttr(buzz::QN_TO, remote+"@"+domain_name);
	stanza->AddAttr(buzz::QN_ID, talk_base::CreateRandomString(16));
	stanza->AddAttr(buzz::QN_TYPE, "chat");
	stanza->AddElement(elem);
	xmpp_client_->SendStanza(stanza);
	LOG(INFO)<<">>"<<stanza->Str();
	delete stanza;
}

void P2PClientManager::HandleTransport(const buzz::XmlElement* stanza) {
	const buzz::XmlElement* elem = stanza->FirstNamed(QN_GINGLE_P2P_TRANSPORT);
	const buzz::XmlElement* chatElement = stanza->FirstNamed(buzz::QN_MESSAGE);

	const std::string& from = stanza->Attr(buzz::QN_FROM);
	int pos = from.find("/");
	const std::string& remoteuser = from.substr(0,pos);
	const std::string& result = elem->Attr(QN_RESULT);
	if(!result.compare("true")) {
		channel->trans_info->order = elem->Attr(QN_ORDER);
		channel->trans_info->ip = elem->Attr(QN_REMOTEIP);
		channel->trans_info->port = elem->Attr(QN_REMOTEPORT);
		channel->trans_info->protocol = elem->Attr(QN_PROTO);
		worker_thread_->Post(this, MSG_P2P_REQUEST_CONNECTION);
	}
	else {
		worker_thread_->Post(this, MSG_FAIL_CONNECTION);
	}
	LOG(INFO)<<"<<"<<stanza->Str();
}

void P2PClientManager::requestRelayPort(const std::string& remote) {
	buzz::XmlElement* elem = new buzz::XmlElement(QN_GINGLE_P2P_ALLOCATE);
	elem->SetAttr(QN_GUEST, remote+"@"+domain_name);
	buzz::XmlElement* stanza = new buzz::XmlElement(buzz::QN_MESSAGE);
	stanza->AddAttr(buzz::QN_TO, "relay@"+domain_name);
	stanza->AddAttr(buzz::QN_ID, talk_base::CreateRandomString(16));
	stanza->AddAttr(buzz::QN_TYPE, "chat");
	stanza->AddElement(elem);
	xmpp_client_->SendStanza(stanza);
	LOG(INFO)<<">>"<<stanza->Str();
	delete stanza;
}

void P2PClientManager::SetAvailable(const buzz::Jid& jid, buzz::Status* status) {
	status->set_jid(jid);
	status->set_available(true);
	status->set_show(buzz::Status::SHOW_ONLINE);
}

void P2PClientManager::OnRequestSignaling() {
	session_manager_->OnSignalingReady();
}

void P2PClientManager::OnSessionCreate(Session* session, bool initiate) {
	session->set_allow_local_ips(true);
	session->set_current_protocol(PROTOCOL_HYBRID);
}

void P2PClientManager::MakeCallTo(const std::string& name) {
	//buzz::Jid callto_jid(name);
	//if (!call_) {
	   // call_ = molo_client_->CreateCall();
	   // session_ = call_->InitiateSession(callto_jid);
	//}
}

const std::string P2PClientManager::strerror(buzz::XmppEngine::Error err) {
	switch (err) {
	case buzz::XmppEngine::ERROR_NONE:
		return "";
	case buzz::XmppEngine::ERROR_XML:
		return "Malformed XML or encoding error";
	case buzz::XmppEngine::ERROR_STREAM:
		return "XMPP stream error";
	case buzz::XmppEngine::ERROR_VERSION:
		return "XMPP version error";
	case buzz::XmppEngine::ERROR_UNAUTHORIZED:
		return "User is not authorized (Check your username and password)";
	case buzz::XmppEngine::ERROR_TLS:
		return "TLS could not be negotiated";
	case buzz::XmppEngine::ERROR_AUTH:
		return "Authentication could not be negotiated";
	case buzz::XmppEngine::ERROR_BIND:
		return "Resource or session binding could not be negotiated";
	case buzz::XmppEngine::ERROR_CONNECTION_CLOSED:
		return "Connection closed by output handler.";
	case buzz::XmppEngine::ERROR_DOCUMENT_CLOSED:
		return "Closed by </stream:stream>";
	case buzz::XmppEngine::ERROR_SOCKET:
		return "Socket error";
	default:
		return "Unknown error";
	}
}
