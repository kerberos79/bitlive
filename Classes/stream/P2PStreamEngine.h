#ifndef __P2PSTREAM_ENGINE_H__
#define __P2PSTREAM_ENGINE_H__
#include "cocos2d.h"
#include <string.h>
#include <stdio.h>
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM == CC_PLATFORM_OSX)
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <malloc/malloc.h>
#include <netdb.h>
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/udp.h>
#include <malloc.h>
#include <netdb.h>
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include <malloc.h>
#endif
#include <stdlib.h>
#include <list>
#include "webrtc/p2p/base/port.h"
#include "util/define.h"
#include "stream/Stream.h"
#include "stream/StreamPacket.h"
#include "stream/GFrameSendHelper.h"
#include "stream/GFrameRecvHelper.h"

#define LOSS_SKIP

class P2PStreamEngine {
public :
	int rsocket;
	int wsocket;

    // recieve
    unsigned char *video_frame_buffer;
    unsigned char *rpacket;
    int 		  read_offset;
	int 		  rpacket_length;
	unsigned char packet_count;

    // send
    struct sockaddr_in serveraddr;
	StreamPacket* epacket;
	StreamPacket* write_video_packet;
	GFrameSendHelper *gold_send_helper;
	GFrameRecvHelper *gold_recv_helper;
	GFrameSendHelper *artwork_send_helper;
	GFrameRecvHelper *artwork_recv_helper;
	unsigned char wpacket_number;
	cricket::Connection* conn_;
	unsigned char command_buffer[256];
    unsigned char backup[MAX_STREAM_NUM][VIDEO_BACKUP_SIZE];
    unsigned char *voice_encode_buffer;
    unsigned char *voice_decode_buffer;
    rtc::PacketOptions options;

	P2PStreamEngine();
	~P2PStreamEngine();
    void init();
    void release();
	int recvPacket(Stream *stream);
    int onRecvPacket(unsigned char* current_offset, size_t len, Stream *stream);
	////////
	int requestFrame(GFrameRecvHelper *helper);
	int requestRecovery(GFrameRecvHelper *helper);
	int sendGframeComplete(GFrameRecvHelper *helper);
    int sendCommand(const char *command);
	void sendSinglePacket(unsigned char stream_id, unsigned char *buffer, int size);
	void sendMultiPacket(unsigned char stream_id, unsigned char *buffer, int size);
	int sendFramePacket(GFrameSendHelper *helper, unsigned char *buffer, int size, unsigned char frame_count, unsigned char level);
	int sendResampling(GFrameSendHelper *helper, unsigned char *buffer, int size, unsigned char frame_count, unsigned char level);
	int sendLastFramePacket(GFrameSendHelper *helper);
	int sendFrameRecoveryPacket(GFrameSendHelper *helper, unsigned char *buffer, int count, unsigned char framenumber);
};
#endif
