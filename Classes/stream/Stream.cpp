#include "Stream.h"
Stream::Stream() :
buffer(nullptr),
length(0)
{
}

Stream::Stream(int capacity):
length(0)
{
	buffer = new unsigned char[capacity];
}


Stream::~Stream() {
	if(buffer)
		delete buffer;
}

void Stream::setBuffer(unsigned char *src_buffer, int src_length) {
	buffer = src_buffer;
	length = src_length;
}

void Stream::clone(Stream* stream)
{
	length = stream->length;
	memcpy(buffer, stream->buffer, length);
}

void Stream::clear(){
	buffer = nullptr;
}
