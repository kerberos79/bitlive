//
//  DecryptHelper.h
//  BitLive
//
//  Created by keros on 9/17/15.
//
//

#ifndef __BitLive__DecryptHelper__
#define __BitLive__DecryptHelper__

#include <stdio.h>
#include <openssl/aes.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include "cocos2d.h"
class DecryptHelper {
public:
    
    DecryptHelper();
    ~DecryptHelper();
    bool init();
    int decrypt(unsigned char* plainText, unsigned char* cipherText, unsigned int cipherTextLen);
private:
    EVP_CIPHER_CTX _ctx;
    unsigned char _key[16];
    unsigned char _iv[8];
    int _outLen, _tmpLen;
};
#endif /* defined(__BitLive__DecryptHelper__) */
