//
//  EncryptHelper.cpp
//  BitLive
//
//  Created by keros on 9/17/15.
//
//

#include "EncryptHelper.h"

EncryptHelper::EncryptHelper()
:_key{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15},
_iv{1,2,3,4,5,6,7,8}
{
    ERR_load_crypto_strings();
}

EncryptHelper::~EncryptHelper()
{
    ERR_free_strings();
}

bool EncryptHelper::init()
{
    unsigned long err = 0;
    
    EVP_CIPHER_CTX_init(&_ctx);
    
    if(EVP_EncryptInit_ex(&_ctx, EVP_bf_cbc(), NULL, _key, _iv) != 1) {
        err = ERR_get_error();
        CCLOG("ERR : EVP_Encrypt() - %s\n", ERR_error_string(err, NULL));
        return false;
    }
    
    return true;
}

int EncryptHelper::encrypt(unsigned char* cipherText, unsigned char* plainText, unsigned int plainTextLen)
{
    unsigned long err = 0;
    
    init();
    
    if(EVP_EncryptUpdate(&_ctx, cipherText, &_outLen, plainText, plainTextLen) != 1) {
        err = ERR_get_error();
        CCLOG("ERR : EVP_EncryptUpdate() - %s\n", ERR_error_string(err, NULL));
        return false;
    }
    if(EVP_EncryptFinal_ex(&_ctx, cipherText + _outLen, &_tmpLen) != 1) {
        err = ERR_get_error();
        CCLOG("ERR : EVP_EncryptFinal_ex() - %s\n", ERR_error_string(err, NULL));
        return false;
    }
    EVP_CIPHER_CTX_cleanup(&_ctx);
    return (_outLen + _tmpLen);
}
