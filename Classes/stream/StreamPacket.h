#ifndef __STREAM_PACKET_H__
#define __STREAM_PACKET_H__
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include "cocos2d.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include <malloc/malloc.h>
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <malloc.h>
#endif
#include "util/define.h"
class StreamPacket {
public:
	unsigned char *data;
	int length;

	StreamPacket();
	StreamPacket(int buffersize);
    StreamPacket(unsigned char* buffer, int buffersize);
    ~StreamPacket();
    StreamPacket* copy(unsigned char* buffer, int payload_length, unsigned char streamid,
    		unsigned char packetnum);
    StreamPacket* copyWithFlag(unsigned char* buffer, int payload_length, unsigned char streamid,
    		unsigned char packetnum, unsigned char flag);
    StreamPacket* copyWithFlag(unsigned char* buffer, int payload_length, unsigned char streamid,
    		unsigned char framecount, unsigned char totalpacketnum, unsigned char packetnum);
    StreamPacket* copy(unsigned char* buffer, int payload_length, unsigned char streamid,
    		unsigned char framecount, unsigned char totalpacketnum, unsigned char packetnum);
    StreamPacket* checkFlag(bool flag);
};



#endif
