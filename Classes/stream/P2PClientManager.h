#ifndef __P2PCLIENT_MANAGER_H__
#define __P2PCLIENT_MANAGER_H__

#include <map>
#include <string>
#include <vector>
#include <jni.h>
#include <ctype.h>
#include <pthread.h>
#include <GLES2/gl2.h>

#include <ns_turn_utils.h>
#include "talk/base/helpers.h"
#include "talk/p2p/base/session.h"
#include "talk/p2p/base/sessionmanager.h"
#include "talk/p2p/base/candidate.h"
#include "talk/p2p/base/parsing.h"
#include "talk/p2p/client/sessionmanagertask.h"
#include "talk/p2p/client/basicportallocator.h"
#include "talk/xmpp/xmppclient.h"
#include "talk/examples/call/status.h"
#include "../CustomXmppTask.h"
#include "../JNICallback.h"
#include "../util.h"
#include "P2PChannel.h"
#include "StunInfo.h"
#include "P2PStreamEngine.h"
#include "../Shader.h"

using namespace cricket;


enum {
	MSG_CREATECHANNEL = 1,
	MSG_DESTROYCHANNEL,
	MSG_P2P_CANDIDATE,
	MSG_P2P_REQUEST_CONNECTION,
	MSG_P2P_CONNECT,
	MSG_P2P_CONNECTED,
	MSG_P2P_ACCEPCTED,
	MSG_P2P_TIMEOUT,
	MSG_FAIL_CONNECTION,
	MSG_QUIT,
};

class P2PClientManager: public talk_base::MessageHandler, public sigslot::has_slots<> {
public:
	P2PClientManager(buzz::XmppClient* xmpp_client, const char *ipaddress);
	~P2PClientManager();

	int CreateHostChannel(const char* remote, const char* token);
	int CreateGuestChannel();
	int ReqeustUpnp();
	int ReqeustNatpmp();
	void CloseChannel();
	void ReleaseChannel();
	void SendP2PConnect(const std::string& to);
	void SendInvitation(const std::string& remote, const std::string& tokenId);
	void SendRefusal(const std::string& remote, const std::string& tokenId);
	void SendCancelation(const std::string& to, const std::string& tokenId);
	void SendBusy(const std::string& remote, const std::string& tokenId);
	void WriteCandidate(const std::string& to, StunInfo *info);
	void sendFailConnection(const std::string& remote);
	void requestConnection(const std::string& to, TransInfo *info);
	void requestRelayPort(const std::string& remote);
	void SendData(const char* data);
	void SendData(const char* data, int len) ;
	void ParseLine(const std::string &str);
	void Quit();
	void OnRequestSignaling();
	void OnSessionCreate(Session* session, bool initiate);
    void MakeCallTo(const std::string& name);
    void SendChat(const std::string& to, const std::string msg);
	void Input(const char * data, int len);

	void HandleCandidate(const buzz::XmlElement* elem);
	void HandleTransport(const buzz::XmlElement* elem);
	void HandleChat(const buzz::XmlElement* elem);
	virtual void OnMessage(talk_base::Message *msg);

	buzz::XmppClient* getXmppClient() const {
		return xmpp_client_;
	}

	void setXmppClient(buzz::XmppClient* xmppClient) {
		xmpp_client_ = xmppClient;
	}

	std::string getRemoteUser() const {
	  return this->to;
	}

	void setInvitation(std::string remote, std::string token){
		pthread_mutex_lock(&lock);
		this->to = remote;
		this->token_id = token;
		LOG(INFO)<<"setInvitation to :"<<this->to<<", token : "+this->token_id<<"\n";
		pthread_mutex_unlock(&lock);
	}

	// return true if user_id and token_id is same.
	bool compareInvitation(const std::string &remote_user_id, const std::string &token) {
		bool result = true;
		pthread_mutex_lock(&lock);
		LOG(INFO)<<"new is remote_user_id:"<<remote_user_id<<", token:"<<token<<"\n";
		LOG(INFO)<<"old is user_id:"<<to<<", token_id:"<<token_id<<"\n";
		if(remote_user_id.compare(to) || token.compare(token_id)) {
			LOG(INFO)<<"compareInvitation is FALSE";
			result = false;
		}
		else {
			LOG(INFO)<<"compareInvitation is TRUE";
		}
		pthread_mutex_unlock(&lock);
		return result;
	}

	void setCallback(JNICallback *jni_callback) {
		this->jni_callback = jni_callback;
	}

	void setNetworkInfo(const char *type, const char *provider) {
		network_type = type;
		network_provider = provider;
	}

	P2PStreamEngine* createP2PStreamEngine() {
		return new P2PStreamEngine(channel->getCurrentSocket());
	}

	Shader* createShader() {
		return new Shader();
	}

	void setUserID(const char* userid) {
		user_id = userid;
	}

	void setDomain(const char* domain) {
		domain_name = domain;
	}
	void setUpnp(Upnp* _upnp) {		channel->setUpnp(_upnp);	}
private:
    void InitMedia();
	void OnStateChange(buzz::XmppEngine::State state);
	void SetAvailable(const buzz::Jid& jid, buzz::Status* status);
	void OnReadPacket(TransportChannel* channel, const char* data,
			size_t len);
	static const std::string strerror(buzz::XmppEngine::Error err);

	JNIEnv* env;
	JavaVM* jvm;
	buzz::XmppClient* xmpp_client_;
	talk_base::Thread* worker_thread_;
	talk_base::NetworkManager* network_manager_;
	PortAllocator* port_allocator_;
	SessionManager* session_manager_;
	SessionManagerTask* session_manager_task_;
	CustomXmppTask* xmpp_task_;
	bool allow_local_ips_;
	bool channel_is_not_work_;
	std::string name;
	std::string to;
	std::string token_id;
	std::string network_type;
	std::string network_provider;
	std::string user_id;
	std::string domain_name;


	bool incoming_call_;
	Session *session_;
	buzz::Status my_status_;
	std::vector<Candidate> ready_candidates_;
	std::vector<Candidate> remote_candidates_;
	JNICallback *jni_callback;
	uint32 start_time_;
	const char*	local_ip;
	P2PChannel *channel = NULL;
    pthread_cond_t condition;
    pthread_mutex_t lock;
    pthread_mutex_t remote_user_lock;
};

#endif
