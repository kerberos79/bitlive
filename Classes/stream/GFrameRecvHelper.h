#ifndef __GFRAME_RECV_HELPER_H_
#define __GFRAME_RECV_HELPER_H_
#include <time.h>
#include "cocos2d.h"
#include "util/define.h"
#include "stream/StreamPacket.h"

class GFrameRecvHelper {
public:
	unsigned char request_gframe_buffer[9]; // add last 4 bytes for backup storage
	unsigned char request_gframe_buffer_length = 5;
	unsigned char *request_buffer;
	unsigned char request_buffer_length;
	unsigned char *read_buffer;
	unsigned char frame_number;
	int frame_length;
	bool isComplete;
	int request_gframe;
	int request_level;
	int encodingLevel;
	int	totalReadBytes;
private:
	unsigned char stream_id;
	int max_packet_size;
	bool *receive_check_list;
	unsigned char total_packet_number;
	unsigned char packet_count;
	unsigned char sequence_number;
    int request_gframe_count;

	// for managing a delay between received video frames
	int	bytePerSeconds, frameDelay, sumFrameDelay, frameCount;

public:
	GFrameRecvHelper(unsigned char streamid, int max_packet_size);
	~GFrameRecvHelper();
	int insert(unsigned char* buffer, int length);
	bool isCompleted();
	void createRequestGFrame();
};

#endif
