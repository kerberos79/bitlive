#ifndef __STREAM_H__
#define __STREAM_H__

#include <memory>

class Stream {
public:
	unsigned char* buffer;
	int length;

	Stream();
	Stream(int capacity);
    ~Stream();
    void setBuffer(unsigned char *src_buffer, int src_length);
	void clone(Stream* stream);
    void clear();
};

#endif
