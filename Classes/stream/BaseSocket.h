#ifndef __BASIC_SOCKET_H__
#define __BASIC_SOCKET_H__
#include "util/define.h"
#include "stream/P2PSync.h"

class BaseSocket {
protected:
public:
	BaseSocket() {}
	virtual ~BaseSocket() {}
    virtual int Destroy(){ return 0; }
	virtual bool IsHost(){ return 0; }
	virtual bool IsTcp(){ return 0; }
	virtual bool WaitConnect(){ return 0; }
	virtual bool WaitAccept(){ return 0; }
	virtual int SendConnect(){ return 0; }
	virtual int SendAccept(){ return 0; }
	virtual bool WaitSync(){ return 0; }
	virtual int SendSync(){ return 0; }
	virtual P2PSync* GetSync(){ return 0; }
	virtual int Read(unsigned char *buffer){ return 0; }
	virtual void Write(unsigned char *buffer, int len){}
	virtual void WriteAndRecover(unsigned char *buffer, int len){}
};
#endif
