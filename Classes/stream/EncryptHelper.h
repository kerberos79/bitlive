//
//  EncryptHelper.h
//  BitLive
//
//  Created by keros on 9/17/15.
//
//

#ifndef __BitLive__EncryptHelper__
#define __BitLive__EncryptHelper__

#include <stdio.h>
#include <openssl/aes.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include "cocos2d.h"
class EncryptHelper {
public:
    
    EncryptHelper();
    ~EncryptHelper();
    bool init();
    int encrypt(unsigned char* cipherText, unsigned char* plainText, unsigned int plainTextLen);
private:
    
    EVP_CIPHER_CTX _ctx;
    unsigned char _key[16];
    unsigned char _iv[8];
    int _outLen, _tmpLen;
};
#endif /* defined(__BitLive__EncryptHelper__) */
