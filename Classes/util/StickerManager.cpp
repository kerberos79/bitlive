//
//  StickerManager.cpp
//  BitLive
//
//  Created by kerberos on 8/20/15.
//
//

#include "StickerManager.h"

StickerInfo::StickerInfo(const char *name, Vec2 anchor, bool flip, int count, float duration, int blend, bool reverse)
:_sprite(nullptr), _action(nullptr)
 {
	_name = name;
	_anchor = anchor;
	_flip = flip;
    _blend = blend;
    _reverse = reverse;
	_count = count;
	_duration = duration;
}

Sprite* StickerInfo::getSprite()
{
    if(_sprite==nullptr)
    {
        _sprite = Sprite::create(_name+".png");
        _sprite->setAnchorPoint(_anchor);
        _sprite->retain();
        switch(_blend)
        {
            case 1: // blend 1 image
            {
                BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
                _sprite->setBlendFunc(blendFunc3);
                break;
            }
            case 2: // mix 2 image
            {
                auto blendSprite = Sprite::create(_name+"_1.png");
                blendSprite->setAnchorPoint(Vec2::ZERO);
                BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
                blendSprite->setBlendFunc(blendFunc3);
                _sprite->addChild(blendSprite);
                break;
            }
            case 3: // mix multiple image
            {
                auto blendSprite = Sprite::create(_name+"_1.png");
                blendSprite->setAnchorPoint(Vec2::ZERO);
                BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
                blendSprite->setBlendFunc(blendFunc3);
                _sprite->addChild(blendSprite);
                char spriteStr[20];
                Vector<SpriteFrame*> frameAnimation;
                for(int i=1;i<_count;i++)
                {
                    sprintf(spriteStr, "%s_%d.png", _name.c_str(), i);
                    auto spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,_sprite->getContentSize().width,_sprite->getContentSize().height));
                    frameAnimation.pushBack(spriteFrame);
                }
                if(_reverse)
                {
                    for(int i=_count-1;i>1;i--)
                    {
                        sprintf(spriteStr, "%s_%d.png", _name.c_str(), i);
                        auto spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,_sprite->getContentSize().width,_sprite->getContentSize().height));
                        frameAnimation.pushBack(spriteFrame);
                    }
                }
                auto animation = Animation::createWithSpriteFrames(frameAnimation, _duration);
                _action = RepeatForever::create(Animate::create(animation));
                _action->retain();
            }
            default:
            {
                char spriteStr[20];
                Vector<SpriteFrame*> frameAnimation;
                
                auto spriteFrame = SpriteFrame::create(_name+".png" , Rect(0,0,_sprite->getContentSize().width,_sprite->getContentSize().height));
                frameAnimation.pushBack(spriteFrame);
                
                for(int i=1;i<_count;i++)
                {
                    sprintf(spriteStr, "%s_%d.png", _name.c_str(), i);
                    spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,_sprite->getContentSize().width,_sprite->getContentSize().height));
                    frameAnimation.pushBack(spriteFrame);
                }
                if(_reverse)
                {
                    for(int i=_count-1;i>0;i--)
                    {
                        sprintf(spriteStr, "%s_%d.png", _name.c_str(), i);
                        auto spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,_sprite->getContentSize().width,_sprite->getContentSize().height));
                        frameAnimation.pushBack(spriteFrame);
                    }
                    spriteFrame = SpriteFrame::create(_name+".png" , Rect(0,0,_sprite->getContentSize().width,_sprite->getContentSize().height));
                    frameAnimation.pushBack(spriteFrame);
                }
                auto animation = Animation::createWithSpriteFrames(frameAnimation, _duration);
                _action = RepeatForever::create(Animate::create(animation));
                _action->retain();
                break;
            }
        }
    }
    return _sprite;
}

StickerManager::StickerManager()
:_currentTag(-1)
{
    
}

StickerManager::~StickerManager()
{
    
}

StickerManager* StickerManager::create(MainScene* mainScene, std::string sticker_list, const Size& size)
{
    StickerManager *manager = new (std::nothrow) StickerManager();
    if( manager && manager->init(mainScene, sticker_list, size))
    {
        return manager;
    }
    CC_SAFE_DELETE(manager);
    return nullptr;
}

bool StickerManager::init(MainScene* mainScene, std::string& sticker_list, const Size& size)
{
    mMainScene = mainScene;
    _size = size;
    
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile(sticker_list);
    doc.Parse<0>(contentStr.c_str());
    if (_doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    int tag, count, blend;
    float anchorX, anchorY, duration;
    bool flip, reverse;
    const char *sprite;
    StickerInfo *info;
    int listCount = DICTOOL->getArrayCount_json(doc, "list");
    for(int i=0;i<listCount; i++)
    {
        const rapidjson::Value& item = DICTOOL->getDictionaryFromArray_json(doc, "list", i);
        tag = DICTOOL->getIntValue_json(item, "tag");
        sprite = DICTOOL->getStringValue_json(item, "sprite");
        anchorX = DICTOOL->getFloatValue_json(item, "anchor_x");
        anchorY = DICTOOL->getFloatValue_json(item, "anchor_y");
        flip = DICTOOL->getBooleanValue_json(item, "flip", false);
        blend = DICTOOL->getIntValue_json(item, "blend", 0);
        reverse = DICTOOL->getBooleanValue_json(item, "reverse", false);
        count = DICTOOL->getIntValue_json(item, "count");
        if(count>1)
        	duration = DICTOOL->getFloatValue_json(item, "duration");
        info = new StickerInfo(sprite, Vec2(anchorX, anchorY), flip, count, duration, blend, reverse);
        std::pair<int, StickerInfo*> map_item(tag, info);
        _map.insert(map_item);
    }
    return true;
}

int StickerManager::getCurrentTag()
{
    return _currentTag;
}

StickerInfo* StickerManager::getPreviewItem(int tag)
{
    if(_currentTag == tag)
    {
        _currentTag = -1;
        _currentSticker = nullptr;
        return nullptr;
    }
    std::unordered_map<int, StickerInfo*>::const_iterator got = _map.find(tag);
    if ( got == _map.end() )
    {
        CCLOG("not found");
        return nullptr;
    }
    StickerInfo *info = (StickerInfo*)got->second;
    _currentTag = tag;
    return info;
}

StickerInfo* StickerManager::getDecodeItem(int tag)
{
    if(_currentTag == tag)
    {
        return _currentInfo;
    }
    
    std::unordered_map<int, StickerInfo*>::const_iterator got = _map.find(tag);
    if ( got == _map.end() )
    {
        CCLOG("not found");
        return nullptr;
    }
    _currentInfo = (StickerInfo*)got->second;
    _currentTag = tag;
    return _currentInfo;
}
