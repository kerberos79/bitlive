//
//  EmoticonManager.cpp
//  BitLive
//
//  Created by keros on 8/1/15.
//
//

#include "EmoticonManager.h"


EmoticonInfo::EmoticonInfo(bool flip)
:_isLastFrame(false), _action1(nullptr), _action2(nullptr), _action3(nullptr)
{
    _flip = flip;
}

void EmoticonInfo::addAction(const rapidjson::Value& actions, const Size& size)
{
    char spriteStr[30];
    const char *spriteName, *sound;
    float startX, startY, endX, endY, delay;
    bool reverse, fadeout;
    _type = DICTOOL->getIntValue_json(actions, "type");
    spriteName = DICTOOL->getStringValue_json(actions, "sprite");
    sprintf(spriteStr, "%s.png", spriteName);
    _sprite = Sprite::create(spriteStr);
    if(_sprite)
    {
        _sprite->retain();
        _repeat = DICTOOL->getIntValue_json(actions, "repeat") * 2 -1;
        _duration = DICTOOL->getFloatValue_json(actions, "duration");
        startX = DICTOOL->getFloatValue_json(actions, "startX");
        startY = DICTOOL->getFloatValue_json(actions, "startY");
        _startScale = DICTOOL->getFloatValue_json(actions, "startScale");
        _startPos = Vec2(size.width * startX, size.height * startY);
        sound = DICTOOL->getStringValue_json(actions, "sound");
        if(sound)
            _sound = sound;
        
        switch(_type)
        {
            case 1: // scale
            {
                _endScale = DICTOOL->getFloatValue_json(actions, "endScale");
                reverse = DICTOOL->getBooleanValue_json(actions, "reverse", false);
                float delayBefore = DICTOOL->getFloatValue_json(actions, "delay1");
                float delayAfter = DICTOOL->getFloatValue_json(actions, "delay2");
                if(reverse)
                {
                    auto scale1 = ScaleTo::create(_duration, _endScale);
                    auto scale2 = ScaleTo::create(_duration, _startScale);
                    _action1 = Sequence::create(DelayTime::create(delayBefore), scale1, scale2,
                                               CallFuncN::create( CC_CALLBACK_1(EmoticonInfo::afterAnimated, this)),
                                               nullptr);
                }
                else
                {
                    auto scale1 = ScaleTo::create(_duration, _endScale);
                    _action1 = Sequence::create(DelayTime::create(delayBefore), scale1, DelayTime::create(delayAfter),
                                               CallFuncN::create( CC_CALLBACK_1(EmoticonInfo::afterAnimated, this)),
                                               nullptr);
                }
                _action1->retain();
                
                fadeout = DICTOOL->getBooleanValue_json(actions, "fadeout", false);
                if(fadeout)
                {
                    float fadeDuration = DICTOOL->getFloatValue_json(actions, "fadeDuration");
                    _action2 = Sequence::create(DelayTime::create(delayBefore), FadeOut::create(fadeDuration),
                                                nullptr);
                    _action2->retain();
                }
                break;
            }
            case 2: // move
            {
                endX = DICTOOL->getFloatValue_json(actions, "endX");
                endY = DICTOOL->getFloatValue_json(actions, "endY");
                _endPos = Vec2(size.width * endX, size.height * endY);
                delay = DICTOOL->getFloatValue_json(actions, "delay");
                if(delay)
                {
                _action1 = Sequence::create(MoveTo::create(_duration, _endPos),
                                               DelayTime::create(delay),
                                               CallFuncN::create( CC_CALLBACK_1(EmoticonInfo::afterAnimated, this)),
                                               nullptr);
                }
                else
                {
                    _action1 = Sequence::create(MoveTo::create(_duration, _endPos),
                                               CallFuncN::create( CC_CALLBACK_1(EmoticonInfo::afterAnimated, this)),
                                               nullptr);
                }
                _action1->retain();
                break;
            }
            case 3: // frame
            {
                int frameCount = DICTOOL->getIntValue_json(actions, "count");
                
                if(frameCount>1)
                {
                    auto spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,_sprite->getContentSize().width,_sprite->getContentSize().height));
                    _frameAnim.pushBack(spriteFrame);
                }
                
                for(int i=1;i<frameCount;i++)
                {
                    sprintf(spriteStr, "%s_%d.png", spriteName, i);
                    auto spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,_sprite->getContentSize().width,_sprite->getContentSize().height));
                    _frameAnim.pushBack(spriteFrame);
                }
                auto animation = Animation::createWithSpriteFrames(_frameAnim, _duration / frameCount);
                _action1 = Sequence::create(Animate::create(animation),
                                               CallFuncN::create( CC_CALLBACK_1(EmoticonInfo::afterAnimated, this)),
                                               nullptr);
                _action1->retain();
                
                bool move = DICTOOL->getBooleanValue_json(actions, "move");
                if(move)
                {
                    endX = DICTOOL->getFloatValue_json(actions, "endX");
                    endY = DICTOOL->getFloatValue_json(actions, "endY");
                    _endPos = Vec2(size.width * endX, size.height * endY);
                    delay = DICTOOL->getFloatValue_json(actions, "delay");
                    if(delay)
                    {
                        _action2 = Sequence::create(DelayTime::create(delay),
                                                    MoveTo::create(_duration, _endPos),
                                                    nullptr);
                    }
                    else
                    {
                        _action2 = Sequence::create(MoveTo::create(_duration, _endPos),
                                                    nullptr);
                    }
                    _action2->retain();
                }
                bool blend = DICTOOL->getBooleanValue_json(actions, "blend");
                if(blend)
                {
                    BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
                    _sprite->setBlendFunc(blendFunc3);
                }
                break;
            }
            case 4: // fly
            {
                const char *direction = DICTOOL->getStringValue_json(actions, "direction");
                delay = DICTOOL->getFloatValue_json(actions, "delay");
                _startPos.x = -_sprite->getContentSize().width*0.5f;
                _endPos = Vec2(size.width * 0.5f, _startPos.y);
                auto flyInAction =EaseExponentialInOut::create(MoveTo::create(_duration, _endPos));
                _endPos = Vec2(size.width + _sprite->getContentSize().width*0.5f, _startPos.y);
                auto flyOutAction =EaseExponentialInOut::create(MoveTo::create(_duration, _endPos));

                _action1 = Sequence::create(flyInAction,
                                           DelayTime::create(delay),
                                           flyOutAction,
                                           CallFuncN::create( CC_CALLBACK_1(EmoticonInfo::afterAnimated, this)),
                                           nullptr);
                _action1->retain();
                break;
            }
            case 5: // move & scale
            {
                
                endX = DICTOOL->getFloatValue_json(actions, "endX");
                endY = DICTOOL->getFloatValue_json(actions, "endY");
                _endPos = Vec2(size.width * endX, size.height * endY);
                delay = DICTOOL->getFloatValue_json(actions, "delay");
                if(delay)
                {
                    _action2 = Sequence::create(MoveTo::create(_duration, _endPos),
                                                DelayTime::create(delay),
                                                CallFuncN::create( CC_CALLBACK_1(EmoticonInfo::afterAnimated, this)),
                                                nullptr);
                }
                else
                {
                    _action2 = Sequence::create(MoveTo::create(_duration, _endPos),
                                                CallFuncN::create( CC_CALLBACK_1(EmoticonInfo::afterAnimated, this)),
                                                nullptr);
                }
                _action2->retain();
                
                _endScale = DICTOOL->getFloatValue_json(actions, "endScale");
                _action1 = Sequence::create(ScaleTo::create(_duration, _endScale),
                                            nullptr);
                _action1->retain();
                
                fadeout = DICTOOL->getBooleanValue_json(actions, "fadeout", false);
                if(fadeout)
                {
                    float fadeDuration = DICTOOL->getFloatValue_json(actions, "fadeDuration");
                    _action3 = FadeOut::create(fadeDuration);
                    _action3->retain();
                }
                break;
            }
        }
    }
}

void EmoticonInfo::runAction(Node* parent, bool isPreviewScreen)
{
    _repeatCount = 1;
    parent->addChild(_sprite, ZORDER_EMOTICON);
    _sprite->setPosition(Vec2(_startPos.x , _startPos.y) );
    _sprite->setOpacity(255);
    _sprite->setScale(_startScale);
    if(_action1)
        _sprite->runAction(_action1);
    if(_action2)
        _sprite->runAction(_action2);
    if(_action3)
        _sprite->runAction(_action3);
    if(_sound.length()>0)
    {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        if(isPreviewScreen)
        {
            Device::playSoundWithMixing(_sound);
        }
        else
        {
            Device::playSound(_sound);
        }
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        if(isPreviewScreen)
            AndroidMP3Decoder::getInstance()->decodeFile(_sound);
        else
            _soundID = cocos2d::experimental::AudioEngine::play2d(_sound, false, 0.6f);
#endif
    }
}

void EmoticonInfo::removeAction(Node* parent, bool isPreviewScreen)
{
    parent->removeChild(_sprite);
    if(_action1)
        _sprite->stopAction(_action1);
    if(_action2)
        _sprite->stopAction(_action2);
    if(_action3)
        _sprite->stopAction(_action3);
    parent->removeChild(_sprite);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if(!isPreviewScreen)
        cocos2d::experimental::AudioEngine::stop(_soundID);
#endif

}

void EmoticonInfo::afterAnimated(Node* sender)
{
    if(_repeatCount<_repeat)
    {
        if(_action1)
            _sprite->stopAction(_action1);
        if(_action2)
            _sprite->stopAction(_action2);
        if(_action3)
            _sprite->stopAction(_action3);
        _sprite->setPosition(Vec2(_startPos.x , _startPos.y));
        _sprite->setScale(_startScale);
        _sprite->runAction(_action1);
        _repeatCount++;
    }
    else
    {
        if(_isLastFrame)
            animateDone();
    }
}

void EmoticonInfo::visit()
{
    _sprite->visit();
}

EmoticonManager::EmoticonManager()
:_isPreviewScreen(false)
{
    
}

EmoticonManager::~EmoticonManager()
{
    
}

EmoticonManager* EmoticonManager::create(MainScene* mainScene, std::string emoticon_list, const Size& size)
{
    EmoticonManager *manager = new (std::nothrow) EmoticonManager();
    if( manager && manager->init(mainScene, emoticon_list, size))
    {
        return manager;
    }
    CC_SAFE_DELETE(manager);
    return nullptr;
}

bool EmoticonManager::init(MainScene* mainScene, std::string& emoticon_list, const Size& size)
{
    mMainScene = mainScene;
    _size = size;
    
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile(emoticon_list);
    doc.Parse<0>(contentStr.c_str());
    if (_doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    int listCount = DICTOOL->getArrayCount_json(doc, "list");
    int tag, actionCount;
    bool flip;
    EmoticonInfo *info;
    for(int i=0;i<listCount; i++)
    {
        const rapidjson::Value& item = DICTOOL->getDictionaryFromArray_json(doc, "list", i);
        tag = DICTOOL->getIntValue_json(item, "tag");
        flip = DICTOOL->getBooleanValue_json(item, "flip");
        
        actionCount = DICTOOL->getArrayCount_json(item, "actions");
        std::vector<EmoticonInfo *> *infoList = new std::vector<EmoticonInfo *>();
        for(int j=0;j<actionCount; j++)
        {
            const rapidjson::Value& actions = DICTOOL->getDictionaryFromArray_json(item, "actions", j);
            info = new EmoticonInfo(flip);
            info->animateDone = CC_CALLBACK_0(EmoticonManager::afterAnimated, this);
            info->addAction(actions, _size);
            infoList->push_back(info);
        }
        info->_isLastFrame = true;
        std::pair<int, std::vector<EmoticonInfo*>*> map_item(tag, infoList);
        _map.insert(map_item);
    }
    _currentInfoList = nullptr;
    return true;
}

void EmoticonManager::setParent(Node *parent, bool isPreviewScreen)
{
    _parent = parent;
    _isPreviewScreen = isPreviewScreen;
}

bool EmoticonManager::addSprite(int tag)
{
    if(_currentInfoList!=nullptr)
    {
        EmoticonInfo *oldEmoticon;
        for (std::vector<EmoticonInfo*>::iterator itor = _currentInfoList->begin(); itor != _currentInfoList->end(); ++itor)
        {
            oldEmoticon = (EmoticonInfo*)(*itor);
            oldEmoticon->removeAction(_parent, _isPreviewScreen);
        }
    }
    
    std::unordered_map<int, std::vector<EmoticonInfo*>*>::const_iterator got = _map.find(tag);
    if ( got == _map.end() )
    {
        CCLOG("not found");
        return false;
    }
    _currentInfoList = (std::vector<EmoticonInfo*>*)got->second;
    
    EmoticonInfo *newEmoticon;
    for (std::vector<EmoticonInfo*>::iterator itor = _currentInfoList->begin(); itor != _currentInfoList->end(); ++itor)
    {
        newEmoticon = (EmoticonInfo*)(*itor);
        newEmoticon->runAction(_parent, _isPreviewScreen);
    }
    if(newEmoticon==nullptr)
        return false;
    return false;
}

void EmoticonManager::afterAnimated()
{
    for (std::vector<EmoticonInfo*>::iterator itor = _currentInfoList->begin(); itor != _currentInfoList->end(); ++itor)
    {
        EmoticonInfo *currentEmoticon = (EmoticonInfo*)(*itor);
        currentEmoticon->removeAction(_parent, _isPreviewScreen);
    }
    _currentInfoList = nullptr;
}

void EmoticonManager::visit()
{
    if(_currentInfoList==nullptr)
        return;
    for (std::vector<EmoticonInfo*>::iterator itor = _currentInfoList->begin(); itor != _currentInfoList->end(); ++itor)
    {
        EmoticonInfo *currentEmoticon = (EmoticonInfo*)(*itor);
        currentEmoticon->visit();
    }
}
