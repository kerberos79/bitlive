//
//  HexString.cpp
//  BitLive
//
//  Created by kerberos on 7/3/15.
//
//

#include "HexString.h"

Color4B HexString::ConvertColor4B(std::string str)
{
    unsigned int nHexValue;
    
    std::stringstream convert(str);
    convert >> std::hex >> nHexValue;
    int a = nHexValue & 0xff;
    int b = (nHexValue &0xff00)>>8;
    int g = (nHexValue &0xff0000)>>16;
    int r = (nHexValue &0xff000000)>>24;
    return Color4B(r,g,b,a);
}

Color4F HexString::ConvertColor4F(std::string str)
{
    unsigned int nHexValue;
    
    std::stringstream convert(str);
    convert >> std::hex >> nHexValue;
    float a = (float)(nHexValue & 0xff) / 255.0;
    float b = (float)((nHexValue &0xff00)>>8) / 255.0;
    float g = (float)((nHexValue &0xff0000)>>16) / 255.0;
    float r = (float)((nHexValue &0xff000000)>>24) / 255.0;
    return Color4F(r,g,b,a);
}

Color3B HexString::ConvertColor3B(std::string str)
{
    unsigned int nHexValue;
    
    std::stringstream convert(str);
    convert >> std::hex >> nHexValue;
    int b = nHexValue & 0xff;
    int g = (nHexValue &0xff00)>>8;
    int r = (nHexValue &0xff0000)>>16;
    return Color3B(r,g,b);
}