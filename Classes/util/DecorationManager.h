//
//  DecorationManager.h
//  BitLive
//
//  Created by kerberos on 8/20/15.
//
//

#ifndef __BitLive__DecorationManager__
#define __BitLive__DecorationManager__


#include <stdio.h>
#include <string>
#include <vector>
#include <unordered_map>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/DictionaryHelper.h"
#include "view/MainScene.h"
#include "view/PreviewScreen.h"
#include "view/DecodeScreen.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocostudio;

class MainScene;
class PreviewScreen;
class DecodeScreen;

class DecorationManager
{
public:
    DecorationManager();
    ~DecorationManager();
    static DecorationManager* create(Node* layer, const Size& size);
    void setItem(int tag);
    void updateFaceRect(Vec2& rect, float scale, float angle);
    void afterHeartAnimated(Node* sender);
private:
    bool init(Node* layer, const Size& size);
    void loadHulk();
    void loadIron();
    void loadHeart();
    void loadMagazine();
    void loadBokeh1();
    void loadBokeh2();
    void loadBokeh3();
    void loadBokeh4();
    void loadEvent1();
    void loadEvent2();
    void loadSpace();
    void loadSpartan();
    void loadJoker();
    void loadNeo();
    MainScene *mMainScene;
    rapidjson::Document _doc;
    Node *_layer;
    Node *_faceLayer;
    Vector<Node*> _spriteList;
    Vector<Action*> _actionList1;
    Vector<Action*> _actionList2;
    Size _size;
    int _currentTag;
};

#endif /* defined(__BitLive__DecorationManager__) */
