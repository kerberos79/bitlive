//
//  StickerManager.h
//  BitLive
//
//  Created by kerberos on 8/20/15.
//
//

#ifndef __BitLive__StickerManager__
#define __BitLive__StickerManager__


#include <stdio.h>
#include <string>
#include <vector>
#include <unordered_map>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/DictionaryHelper.h"
#include "view/MainScene.h"
#include "view/PreviewScreen.h"
#include "view/DecodeScreen.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocostudio;

class MainScene;
class PreviewScreen;
class DecodeScreen;

class StickerInfo
{
public:
	StickerInfo(const char *name, Vec2 anchor, bool flip, int count, float duration, int blend, bool reverse);
	~StickerInfo();
    Sprite* getSprite();
    std::string _name;
    Sprite *_sprite;
    Vec2 _anchor;
    bool _flip;
    int _blend;
    bool _reverse;
    int _count;
    float _duration;
    Action* _action;
};

class StickerManager
{
public:
    StickerManager();
    ~StickerManager();
    static StickerManager* create(MainScene* mainScene, std::string sticker_list, const Size& size);
    StickerInfo* getPreviewItem(int tag);
    StickerInfo* getDecodeItem(int tag);
    int getCurrentTag();
private:
    bool init(MainScene* mainScene, std::string& sticker_list, const Size& size);
    MainScene *mMainScene;
    std::unordered_map<int, StickerInfo*> _map;
    rapidjson::Document _doc;
    Sprite *_currentSticker;
    Size    _size;
    int     _currentTag;
    StickerInfo* _currentInfo;
};

#endif /* defined(__BitLive__StickerManager__) */
