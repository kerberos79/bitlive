//
//  FaceDetectHelper.hpp
//  BitLive
//
//  Created by keros on 10/5/15.
//
//

#ifndef __BitLive__FaceDetectHelper__
#define __BitLive__FaceDetectHelper__

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;
using namespace ui;

class FaceDetectHelper
{
public:
    
    static const int Emoticon;
    static const int Sticker;
    static const int GLProgram;
    
    FaceDetectHelper();
    ~FaceDetectHelper();
    void enable(int flag);
    void disable(int flag);
private:
    int _flag;
};

#endif /* FaceDetectHelper_hpp */
