//
//  HexString.h
//  BitLive
//
//  Created by kerberos on 7/3/15.
//
//

#ifndef __BitLive__HexString__
#define __BitLive__HexString__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class HexString
{
public:
    static Color4B ConvertColor4B(std::string str);
    static Color4F ConvertColor4F(std::string str);
    static Color3B ConvertColor3B(std::string str);
};
#endif /* defined(__BitLive__HexString__) */
