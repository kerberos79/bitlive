#ifndef __Bitlive_AndroidMP3Decoder_H__
#define __Bitlive_AndroidMP3Decoder_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <malloc.h>
#include <pthread.h>
#include <assert.h>
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include <list>
#include "cocos2d.h"
#include "webrtc/base/criticalsection.h"

USING_NS_CC;
using namespace std;
// aux effect on the output mix, used by the buffer queue player
static const SLEnvironmentalReverbSettings reverbSettings =
        SL_I3DL2_ENVIRONMENT_PRESET_STONECORRIDOR;
#ifndef WAV_HEADER_BYTESIZE
#define WAV_HEADER_BYTESIZE 44
#endif
#ifndef PCM_BUFFER_NUM
#define PCM_BUFFER_NUM 6
#endif
#ifndef PCM_BUFFER_BYTESIZE
#define PCM_BUFFER_BYTESIZE 960*2
#endif

#ifndef PCM_BUFFER_SHORTSIZE
#define PCM_BUFFER_SHORTSIZE 960
#endif

#ifndef SHORT_MAX
#define SHORT_MAX 32767
#endif
#ifndef SHORT_MIN
#define SHORT_MIN -32768
#endif

class AndroidMP3Decoder
{
public:
    AndroidMP3Decoder();
	~AndroidMP3Decoder();
	static AndroidMP3Decoder* getInstance();
	void setVolume(int level);
	void decodeFile(std::string filename);
	void playCallback(SLAndroidSimpleBufferQueueItf bq, void *context);
	void mixPcmBuffer(short* src, int size);
private:
	void createEngine();
	void createBufferQueueAudioPlayer(unsigned int samplerate, int channels, bool callback);
	void play();
	void pause();
	void resume();
	void stop();
	void copyToMixingBufferList(unsigned char* buffer, int byte_size);
	void shutdown();

	unsigned char *pcmBuffer;
	unsigned char *mixBuffer;
    int volume;
    bool isPlayed;
    bool hasMixedSound;
    int mixCount;
    // engine interfaces
    SLObjectItf engineObject = NULL;
    SLEngineItf engineEngine = NULL;

    // output mix interfaces
    SLObjectItf outputMixObject = NULL;
    SLEnvironmentalReverbItf outputMixEnvironmentalReverb = NULL;

    // buffer queue player interfaces
    SLObjectItf bqPlayerObject = NULL;
    SLPlayItf bqPlayerPlay = NULL;
    SLAndroidSimpleBufferQueueItf bqPlayerBufferQueue = NULL;
    SLEffectSendItf bqPlayerEffectSend = NULL;
    SLMuteSoloItf bqPlayerMuteSolo = NULL;
    SLVolumeItf bqPlayerVolume = NULL;

    int currentStreamIndex;

    static AndroidMP3Decoder* sAndroidMP3Decoder;
    static std::string writablePath;
    AAsset* asset;
    int totalByteSize, readByteSize;
    rtc::CriticalSection criticalsection_;
};
#endif
