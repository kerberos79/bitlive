//
//  IdentityManager.h
//  BitLive
//
//  Created by keros on 6/9/15.
//
//

#ifndef __BitLive__IdentityManager__
#define __BitLive__IdentityManager__

#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include "cocos2d.h"
#include "util/Tokenizer.h"

class IdentityManager
{
public:
    static std::string hasCountryCode(std::string username);
    static std::string makeUserID(std::string username);
    static std::string parseUserID(std::string userID);
    static void setCountryCode(std::string code);
    
private:
    
    static std::string user_id;
    static std::string result;
};

#endif /* defined(__BitLive__IdentityManager__) */
