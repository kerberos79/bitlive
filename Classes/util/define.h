#ifndef __DEFINE_H__
#define __DEFINE_H__

//   Address
#define XMPP_HTTP_SERVER_URL            "http://52.68.201.252:9090"
#define XMPP_HTTP_USERSERVICE_SECRET    "u78kTiA4"
#define XMPP_HTTP_PUSH_SECRET           "2lW5ul4Q"
#define XMPP_HTTP_SIGNUP_URL            "/plugins/userService/userservice?type=add&secret=u78kTiA4"
#define XMPP_HTTP_DELETE_URL            "/plugins/userService/userservice?type=delete&secret=u78kTiA4"
#define HTTP_SIGNP_HOST                 "52.68.201.252"


#define STUN_SERVER_ADDRESS "40.74.138.83"
#define XMPP_SERVER_ADDRESS "52.68.201.252"
#define XMPP_SERVER_HTTP_PORT	9090
#define XMPP_SERVER_HTTPS_PORT	9091
#define XMPP_SERVER_PORT	 5222
#define XMPP_SERVER_TLS_PORT	 5223
#define XMPP_DOMAIN_NAME 	"cksoft.com"

#define RELAY_PEER_NAME     "relay"

#define SERVER_PORT 6000

// Stream Protocol
#define STREAM_COMMAND  	0x0
#define	STREAM_OPUS 		0x1
#define	STREAM_MP3 			0x2
#define	STREAM_VP8 			0x3
#define	STREAM_AAC 			0x4
#define	STREAM_ERROR		0x5
#define	MAX_STREAM_NUM 		0x5
#define	STREAM_ARTWORK 		0x32 // jpeg for audio album artwork
#define	STREAM_GOLD 		0x33 // gold frame for VP3
#define	STREAM_RECOVERY 	0x3F
#define	STREAM_SKIP         0xFF
#define	SOCKET_CLOSED       0xFF

#define REQUEST_GOLDFRAME		0x01
#define REQUEST_FRAME_PACKET	0x02
#define REQUEST_FRAME_COMPLETE	0x03

#define	MAX_PACKET_SIZE		4098
#define	MAX_PACKET_COMMAND 	256
#define	MAX_PACKET_VOICE 	512
#define	MAX_PACKET_MP3	 	4098
#define	MAX_PACKET_VIDEO 	1400
#define	MAX_PACKET_IMAGE 	1400

#define TCP_HEAD_SIZE 		3
#define TCP_TAIL_SIZE 		1
#define TCP_EXTRA_SIZE 		4
#define TCP_PREFIX			0xF3

// Special command id
#define SOCKET_CONNECT		0x04
#define SOCKET_ACCEPT		0x06
#define SYNC_TIME			0x08

#define	MAX_FRAME_VIDEO 	300000

#define MAX_SOCKET_BUFFER_SIZE	65535
#define	MAX_KEYFRAME_NUM		512
#define	MAX_ARTWORK_FRAME_NUM	20

#define SINGLE_HEADER_SIZE 		1
#define GOLD_PAYLOAD_SIZE 		1352
#define GOLD_HEADER_SIZE 		4
#define RECOVERY_HEADER_SIZE 	4

#define PACKET_NOT_FINISH 	-1
#define PACKET_PARSE_ERROR 	-2
#define SOCKET_DISCONNECT	-3
#define NO_PACKET 			-4

#define VIDEO_PAYLOAD_SIZE			1352
#define VIDEO_HEADER_SIZE			2
#define VIDEO_BACKUP_SIZE			VIDEO_HEADER_SIZE + TCP_EXTRA_SIZE
#define WAIT_PACKET_DELAY_NS 			2000000
#define SEND_SINGLE_PACKET_DELAY_NS 	3000000
#define SEND_VIDEO_PACKET_DELAY_NS 		3000000
#define SEND_VIDEO_PACKET_END_DELAY_NS 	10000000

#define PREFIX_START_PACKET  0x80
#define PREFIX_SINGLE_PACKET 0xC0


#define ERROR_CORRECTION_BIT	0x80
#define FRAME_END_BIT			0x80

#define PACKET_END			1
#define PACKET_SKIP			0
#define PACKET_LOSS			-1

#endif
