//
//  DecorationManager.cpp
//  BitLive
//
//  Created by keros on 10/8/15.
//
//

#include "DecorationManager.h"


DecorationManager::DecorationManager()
:_currentTag(0)
{
    
}

DecorationManager::~DecorationManager()
{
    
}

DecorationManager* DecorationManager::create(Node* layer, const Size& size)
{
    DecorationManager *manager = new (std::nothrow) DecorationManager();
    if( manager && manager->init(layer, size))
    {
        return manager;
    }
    CC_SAFE_DELETE(manager);
    return nullptr;
}

bool DecorationManager::init(Node* layer, const Size& size)
{
    _layer = layer;
    _size = size;
    return true;
}

void DecorationManager::setItem(int tag)
{
    if(_currentTag == tag)
        return;
    
    if(_currentTag>0)
    {
        Vector<Node*>::iterator iterEnd = _spriteList.end();
        for(Vector<Node*>::iterator iterPos = _spriteList.begin();
            iterPos != iterEnd;
            ++iterPos )
        {
            (*iterPos)->stopAllActions();
            _layer->removeChild(*iterPos);
        }
        _spriteList.clear();
    }
    switch(tag)
    {
        case 0:
        {
            break;
        }
        case 1:
        {
            loadHulk();
            break;
        }
        case 2:
        {
            loadIron();
            break;
        }
        case 3:
        {
            loadHeart();
            break;
        }
        case 5:
        {
            loadMagazine();
            break;
        }
        case 10:
        {
            loadBokeh1();
            break;
        }
        case 11:
        {
            loadBokeh2();
            break;
        }
        case 12:
        {
            loadSpace();
            break;
        }
        case 13:
        {
            loadSpartan();
            break;
        }
        case 14:
        {
            loadJoker();
            break;
        }
        case 15:
        {
            loadBokeh3();
            break;
        }
        case 16:
        {
            loadNeo();
            break;
        }
        case 17:
        {
            loadEvent1();
            break;
        }
        case 18:
        {
            loadEvent2();
            break;
        }
        case 20:
        {
            loadBokeh4();
            break;
        }
        default:
        {
            break;
        }
    }
    _currentTag = tag;
}

void DecorationManager::updateFaceRect(Vec2& rect, float scale, float angle)
{
    switch(_currentTag)
    {
        case 2:
        {
            Vec2 newRect = rect - Vec2(_size.width*0.5f, _size.height*0.5f);
            Vec2 test = newRect*1.6 + Vec2(_size.width*0.5f, _size.height*0.5f);
            _faceLayer->setPosition(test);
            _faceLayer->setScale(scale*1.6);
            _faceLayer->setRotation(angle);
            break;
        }
        case 12:
        {
            Vec2 newRect = rect - Vec2(_size.width*0.5f, _size.height*0.5f);
            Vec2 test = newRect + Vec2(_size.width*0.5f, _size.height*0.5f);
            _faceLayer->setPosition(test);
            _faceLayer->setScale(scale*1.3);
            _faceLayer->setRotation(angle);
            break;
        }
        case 13:
        case 14:
        case 16:
        {
            _faceLayer->setPosition(rect);
            _faceLayer->setScale(scale);
            _faceLayer->setRotation(angle);
            break;
        }
        default:
        {
            break;
        }
    }
}

void DecorationManager::loadHulk()
{
    auto sprite = Sprite::create("deco01_1.png");
    sprite->setAnchorPoint(Vec2::ZERO);
    sprite->setPosition(Vec2::ZERO);
    
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
}

void DecorationManager::loadIron()
{
    BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
    char spriteStr[20];
    _faceLayer = Node::create();
    _faceLayer->setAnchorPoint(Vec2(0.5, 0.5));
    _faceLayer->setPosition(_size/2);
    _faceLayer->setContentSize(Size(220.0, 50.0));
    _layer->addChild(_faceLayer);
    _spriteList.pushBack(_faceLayer);
    
    auto sprite = Sprite::create("deco02_10.png");
    sprite->setAnchorPoint(Vec2(0.5, 0.5));
    sprite->setPosition(Vec2(0, 40));
    sprite->setBlendFunc(blendFunc3);
    auto action3 = RepeatForever::create(Sequence::create( RotateTo::create(0.4, 20),
                                                          DelayTime::create(2.0),
                                                          RotateTo::create(1.4, 0),
                                                          nullptr));
    sprite->runAction(action3);
    _faceLayer->addChild(sprite);
    
    sprite = Sprite::create("deco02_1.png");
    sprite->setAnchorPoint(Vec2(0.8, 0.9));
    sprite->setPosition(Vec2(0, -60));
    sprite->setBlendFunc(blendFunc3);
    _faceLayer->addChild(sprite);
    
    sprite = Sprite::create("deco02_2.png");
    sprite->setAnchorPoint(Vec2(0.1, 0.5));
    sprite->setPosition(Vec2(220.0,  0));
    sprite->setBlendFunc(blendFunc3);
    _faceLayer->addChild(sprite);
    
    sprite = Sprite::create("deco02_3.png");
    sprite->setAnchorPoint(Vec2(0.5, 0.5));
    sprite->setPosition(Vec2(40.0, -320));
    sprite->setBlendFunc(blendFunc3);
    _faceLayer->addChild(sprite);
}
static float rotation[]={150.0, 10.0, 80.0, 100.0, 40.0, 50.0, 100.0, 40.0, 50.0, 60.0, 40.0, 20.0};
static float distancefactor[]={180.0, 170.0, 160.0, 180.0, 170.0, 170.0, 190.0, 160.0, 170.0, 210.0, 180.0, 170.0};
static float scalefactor[]={1.0, 0.8, 1.0, 0.6, 0.8, 1.0, 0.6, 0.8, 1.0, 0.6, 1.2, 0.8};
static float delayfactor[]={0.0, 0.2, 0.5, 0.4, 0.5, 0.1, 0.5, 0.2, 0.1, 0.6, 0.2, 0.5};
void DecorationManager::loadHeart()
{
    Vec2 startPoint = Vec2(_size.width*0.5f, _size.height*0.5f);
    Vec2 nomVector = Vec2(1.0,1.0);
    nomVector.normalize();
    Vec2 midPoint = nomVector * distancefactor[0] + startPoint;
    ccBezierConfig bezier;
    bezier.controlPoint_1 = Vec2(50, 50);
    bezier.controlPoint_2 = Vec2(-100, 100);
    bezier.endPosition = Vec2(-600, 100);
    float delay = 0.0;
    _actionList1.clear();
    _actionList2.clear();
    for(int i=0;i<12;i++)
    {
        auto sprite1 = Sprite::create("deco03.png");
        sprite1->setTag(i);
        sprite1->setAnchorPoint(Vec2(0.5, 0.5));
        sprite1->setPosition(startPoint);
        sprite1->setScale(scalefactor[i]);
        sprite1->setOpacity(0);
        
        auto visibleAction1 = EaseExponentialOut::create(MoveTo::create(1.6f, midPoint));
        auto action1 = Sequence::create(DelayTime::create(delay), visibleAction1,
                                         BezierBy::create(5, bezier),
                                         CallFuncN::create( CC_CALLBACK_1(DecorationManager::afterHeartAnimated, this)),
                                         nullptr);
        sprite1->runAction(action1);
        auto action2 = Sequence::create( DelayTime::create(delay),  FadeIn::create(1.0), nullptr);
        sprite1->runAction(action2);
        
        _layer->addChild(sprite1);
        _spriteList.pushBack(sprite1);
        _actionList1.pushBack(action1);
        _actionList2.pushBack(action2);
        
        delay+=delayfactor[i];
        float f = rotation[i];
        nomVector.normalize();
        nomVector.rotate(Vec2::ZERO, f);
        midPoint = (nomVector * distancefactor[i] + startPoint);
        bezier.controlPoint_1.rotate(Vec2::ZERO, f);
        bezier.controlPoint_2.rotate(Vec2::ZERO, f);
        bezier.endPosition.rotate(Vec2::ZERO, f);
    }
}


void DecorationManager::afterHeartAnimated(Node* sender)
{
    sender->stopAllActions();
    sender->setPosition(Vec2(_size.width*0.5f, _size.height*0.5f));
    sender->setOpacity(0);
    sender->runAction(_actionList1.at(sender->getTag()));
    sender->runAction(_actionList2.at(sender->getTag()));
}

void DecorationManager::loadMagazine()
{
    auto sprite = Sprite::create("deco04.png");
    sprite->setAnchorPoint(Vec2::ZERO);
    sprite->setPosition(Vec2::ZERO);
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
}

void DecorationManager::loadBokeh1()
{
    BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
    char spriteStr[16];
    Vector<SpriteFrame*> frameAnim1;
    auto sprite = Sprite::create("deco10.jpg");
    sprite->setAnchorPoint(Vec2::ZERO);
    sprite->setPosition(Vec2::ZERO);
    sprite->setBlendFunc(blendFunc3);
    auto spriteFrame = SpriteFrame::create("deco10.jpg" , Rect(0,0,sprite->getContentSize().width,sprite->getContentSize().height));
    frameAnim1.pushBack(spriteFrame);
    for(int i=1;i<48;i++)
    {
        sprintf(spriteStr, "deco10_%d.jpg", i);
        auto spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,sprite->getContentSize().width,sprite->getContentSize().height));
        frameAnim1.pushBack(spriteFrame);
    }
    auto animation = Animation::createWithSpriteFrames(frameAnim1, 0.2);
    auto action = RepeatForever::create(Animate::create(animation));
    sprite->runAction(action);
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
    
    Vector<SpriteFrame*> frameAnim2;
    sprite = Sprite::create("deco10.jpg");
    sprite->setAnchorPoint(Vec2(0.5, 0.5));
    sprite->setRotation(180);
    sprite->setPosition(Vec2(sprite->getContentSize().width/2, 640.0 - sprite->getContentSize().height/2));
    sprite->setBlendFunc(blendFunc3);
    spriteFrame = SpriteFrame::create("deco10.jpg" , Rect(0,0,sprite->getContentSize().width,sprite->getContentSize().height));
    frameAnim2.pushBack(spriteFrame);
    for(int i=1;i<48;i++)
    {
        sprintf(spriteStr, "deco10_%d.jpg", i);
        auto spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,sprite->getContentSize().width,sprite->getContentSize().height));
        frameAnim2.pushBack(spriteFrame);
    }
    animation = Animation::createWithSpriteFrames(frameAnim2, 0.2);
    action = RepeatForever::create(Animate::create(animation));
    sprite->runAction(action);
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
}
void DecorationManager::loadBokeh2()
{
    BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
    char spriteStr[16];
    Vector<SpriteFrame*> frameAnim;
    auto sprite = Sprite::create("deco11.jpg");
    sprite->setAnchorPoint(Vec2::ZERO);
    sprite->setPosition(Vec2::ZERO);
    sprite->setBlendFunc(blendFunc3);
    auto spriteFrame = SpriteFrame::create("deco11.jpg" , Rect(0,0,sprite->getContentSize().width,sprite->getContentSize().height));
    frameAnim.pushBack(spriteFrame);
    for(int i=1;i<40;i++)
    {
        sprintf(spriteStr, "deco11_%d.jpg", i);
        auto spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,sprite->getContentSize().width,sprite->getContentSize().height));
        frameAnim.pushBack(spriteFrame);
    }
    auto animation = Animation::createWithSpriteFrames(frameAnim, 0.2);
    auto action = RepeatForever::create(Animate::create(animation));
    sprite->runAction(action);
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
}

void DecorationManager::loadBokeh3()
{
    BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
    char spriteStr[16];
    Vector<SpriteFrame*> frameAnim;
    auto sprite = Sprite::create("deco15.jpg");
    sprite->setAnchorPoint(Vec2::ZERO);
    sprite->setPosition(Vec2::ZERO);
    sprite->setBlendFunc(blendFunc3);
    auto spriteFrame = SpriteFrame::create("deco15.jpg" , Rect(0,0,sprite->getContentSize().width,sprite->getContentSize().height));
    frameAnim.pushBack(spriteFrame);
    for(int i=1;i<81;i++)
    {
        sprintf(spriteStr, "deco15_%d.jpg", i);
        auto spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,sprite->getContentSize().width,sprite->getContentSize().height));
        frameAnim.pushBack(spriteFrame);
    }
    for(int i=80;i>=1;i--)
    {
        sprintf(spriteStr, "deco15_%d.jpg", i);
        auto spriteFrame = SpriteFrame::create(spriteStr , Rect(0,0,sprite->getContentSize().width,sprite->getContentSize().height));
        frameAnim.pushBack(spriteFrame);
    }
    spriteFrame = SpriteFrame::create("deco15.jpg" , Rect(0,0,sprite->getContentSize().width,sprite->getContentSize().height));
    frameAnim.pushBack(spriteFrame);
    auto animation = Animation::createWithSpriteFrames(frameAnim, 0.2);
    auto action = RepeatForever::create(Animate::create(animation));
    sprite->runAction(action);
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
}

void DecorationManager::loadBokeh4()
{
    BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
    char spriteStr[16];
    Vector<SpriteFrame*> frameAnim;
    auto sprite = Sprite::create("deco20.jpg");
    sprite->setAnchorPoint(Vec2::ZERO);
    sprite->setPosition(Vec2::ZERO);
    sprite->setBlendFunc(blendFunc3);
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
}

void DecorationManager::loadSpace()
{
    BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
    _faceLayer = Node::create();
    _faceLayer->setAnchorPoint(Vec2(0.5, 0.5));
    _faceLayer->setPosition(_size/2);
    _faceLayer->setContentSize(Size(0.0, 0.0));
    _layer->addChild(_faceLayer);
    _spriteList.pushBack(_faceLayer);
    
    auto sprite = Sprite::create("deco12.jpg");
    sprite->setAnchorPoint(Vec2(0.5, 0.59));
    sprite->setPosition(Vec2(-20.0, 100.0));
    sprite->setBlendFunc(blendFunc3);
    _faceLayer->addChild(sprite);
}

void DecorationManager::loadSpartan()
{
    _faceLayer = Node::create();
    _faceLayer->setAnchorPoint(Vec2(0.5, 0.5));
    _faceLayer->setPosition(_size/2);
    _faceLayer->setContentSize(Size(0.0, 0.0));
    _layer->addChild(_faceLayer);
    _spriteList.pushBack(_faceLayer);
    
    auto sprite = Sprite::create("deco13.png");
    sprite->setAnchorPoint(Vec2(0.5, 0.36));
    sprite->setPosition(Vec2(0.0, 50.0));
    _faceLayer->addChild(sprite);
}

void DecorationManager::loadJoker()
{
    _faceLayer = Node::create();
    _faceLayer->setAnchorPoint(Vec2(0.5, 0.5));
    _faceLayer->setPosition(_size/2);
    _faceLayer->setContentSize(Size(100.0, 100.0));
    _layer->addChild(_faceLayer);
    _spriteList.pushBack(_faceLayer);
    
    auto sprite = Sprite::create("deco14.png");
    sprite->setAnchorPoint(Vec2(0.5, 0.6));
    sprite->setPosition(Vec2(50.0, 50.0));
    _faceLayer->addChild(sprite);
}

void DecorationManager::loadNeo()
{
    _faceLayer = Node::create();
    _faceLayer->setAnchorPoint(Vec2(0.5, 0.5));
    _faceLayer->setPosition(_size/2);
    _faceLayer->setContentSize(Size(100.0, 100.0));
    _layer->addChild(_faceLayer);
    _spriteList.pushBack(_faceLayer);
    
    auto sprite = Sprite::create("deco16.png");
    sprite->setAnchorPoint(Vec2(0.5, 0.3));
    sprite->setPosition(Vec2(50.0, 50.0));
    _faceLayer->addChild(sprite);
}

void DecorationManager::loadEvent1()
{
    auto sprite = Sprite::create("deco17.png");
    sprite->setAnchorPoint(Vec2::ZERO);
    sprite->setPosition(Vec2::ZERO);
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
    
    sprite = Sprite::create("deco17_1.png");
    sprite->setAnchorPoint(Vec2(0, 1.0));
    sprite->setPosition(Vec2(0, _size.height));
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
}

void DecorationManager::loadEvent2()
{
    auto sprite = Sprite::create("deco18.png");
    sprite->setAnchorPoint(Vec2(0, 1.0));
    sprite->setPosition(Vec2(0, _size.height));
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
    
    BlendFunc blendFunc3 = { GL_SRC_ALPHA, GL_ONE };
    sprite = Sprite::create("deco18_1.jpg");
    sprite->setAnchorPoint(Vec2::ZERO);
    sprite->setPosition(Vec2::ZERO);
    sprite->setBlendFunc(blendFunc3);
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
    sprite->runAction(RepeatForever::create(Sequence::create(FadeOut::create(2.0), FadeIn::create(2.0), nullptr)));
    
    sprite = Sprite::create("deco18_2.jpg");
    sprite->setAnchorPoint(Vec2::ZERO);
    sprite->setPosition(Vec2::ZERO);
    sprite->setOpacity(0);
    sprite->setBlendFunc(blendFunc3);
    _layer->addChild(sprite);
    _spriteList.pushBack(sprite);
    sprite->runAction(RepeatForever::create(Sequence::create(FadeIn::create(2.0), FadeOut::create(2.0), nullptr)));
    
}
