//
//  EmoticonManager.h
//  BitLive
//
//  Created by keros on 8/1/15.
//
//

#ifndef __BitLive__EmoticonManager__
#define __BitLive__EmoticonManager__

#include <stdio.h>
#include <string>
#include <vector>
#include <unordered_map>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/DictionaryHelper.h"
#include "view/MainScene.h"
#include "view/PreviewScreen.h"
#include "view/DecodeScreen.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "util/AndroidMP3Decoder.h"
#endif


USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocostudio;

class MainScene;
class PreviewScreen;
class DecodeScreen;

class EmoticonInfo
{
public:
    EmoticonInfo(bool flip);
    void addAction(const rapidjson::Value& actions, const Size& size);
    void runAction(Node* parent, bool isPreviewScreen);
    void removeAction(Node* parent, bool isPreviewScreen);
    void visit();
    std::function<void()> animateDone;
    bool _isLastFrame;
    bool    _flip;
private:
    void afterAnimated(Node* sender);
    int _type;
    Vec2 _startPos;
    Vec2 _endPos;
    float _duration;
    float _startScale, _endScale;
    Vector<SpriteFrame*> _frameAnim;
    Action *_action1, *_action2, *_action3;
    Sprite* _sprite;
    int     _repeat;
    int     _repeatCount;
    int     _soundID;
    std::string _sound;
};

class EmoticonManager
{
public:
    EmoticonManager();
    ~EmoticonManager();
    static EmoticonManager* create(MainScene* mainScene, std::string emoticon_list, const Size& size);
    bool addSprite(int tag);
    void setRotation3D(bool flipcamera);
    void setParent(Node* parent, bool isPreviewScreen = false);
    void afterAnimated();
    void visit();
private:
    bool init(MainScene* mainScene, std::string& emoticon_list, const Size& size);
    MainScene *mMainScene;
    Node *_parent;
    std::unordered_map<int, std::vector<EmoticonInfo*>*> _map;
    std::vector<EmoticonInfo*>* _currentInfoList;
    rapidjson::Document _doc;
    Size    _size;
    float   _scale;
    bool _isPreviewScreen;
};

#endif /* defined(__BitLive__EmoticonManager__) */
