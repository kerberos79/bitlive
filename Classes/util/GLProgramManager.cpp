//
//  GLProgramManager.cpp
//  BitLive
//
//  Created by keros on 6/10/15.
//
//

#include "GLProgramManager.h"


int GLProgramManager::BiteralBlurRadius;
uint32 GLProgramManager::oldtime;

GLProgramManager::GLProgramManager()
{
    
}

GLProgramManager::~GLProgramManager()
{
    for(auto iter = _map.begin(); iter != _map.end(); ++iter)
    {
        if(iter->second->_program1)
            iter->second->_program1->release();
        if(iter->second->_program2)
            iter->second->_program2->release();
        delete iter->second;
    }
}
void GLProgramManager::initializeDevice()
{
    if(Device::getDeviceLevel()>1)
        BiteralBlurRadius = 9;
    else
        BiteralBlurRadius = 6;
}

void GLProgramManager::add(const char *name, const char *fsh1, const char *fsh2, const char *lookup)
{
    GLProgramInfo *info;
    GLProgram *glprogram1, *glprogram2;
    glprogram1 = GLProgram::createWithFilenames("multiTexture.vsh", fsh1);
    glprogram1->retain();
    if(fsh2)
    {
        glprogram2 = GLProgram::createWithFilenames("multiTexture.vsh", fsh2);
        glprogram2->retain();
    }
    else
        glprogram2 = nullptr;
    
    if(lookup)
    {
        auto lookup_texture = Sprite::create(lookup);
        lookup_texture->retain();
        info = new GLProgramInfo();
        info->set(glprogram1, glprogram2, lookup_texture->getTexture());
    }
    else
    {
        info = new GLProgramInfo();
        info->set(glprogram1, glprogram2, nullptr);
    }
    info->set(fsh1, fsh2, lookup);
    std::pair<std::string, GLProgramInfo*> map_item(name, info);
    _map.insert(map_item);
}

bool GLProgramManager::loadAndAdd(std::string program_file)
{
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile(program_file);
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    int listCount = DICTOOL->getArrayCount_json(doc, "program");
    const char *name, *vsh1, *vsh2, *fsh1, *fsh2, *lookup;
    GLProgram *glprogram1, *glprogram2;
    GLProgramInfo *info;
    for(int i=0;i<listCount; i++)
    {
        const rapidjson::Value& item = DICTOOL->getDictionaryFromArray_json(doc, "program", i);
        name = DICTOOL->getStringValue_json(item, "name");
        fsh1 = DICTOOL->getStringValue_json(item, "fsh1");
        fsh2 = DICTOOL->getStringValue_json(item, "fsh2");
        lookup = DICTOOL->getStringValue_json(item, "lookup");
        
        glprogram1 = GLProgram::createWithFilenames("multiTexture.vsh", fsh1);
        glprogram1->retain();
        if(fsh2)
        {
            glprogram2 = GLProgram::createWithFilenames("multiTexture.vsh", fsh2);
            glprogram2->retain();
        }
        else
            glprogram2 = nullptr;
        
        if(lookup)
        {
            auto lookup_texture = Sprite::create(lookup);
            lookup_texture->retain();
            info = new GLProgramInfo();
            info->set(glprogram1, glprogram2, lookup_texture->getTexture());
        }
        else
        {
            info = new GLProgramInfo();
            info->set(glprogram1, glprogram2, nullptr);
        }
        info->set(fsh1, fsh2, lookup);
        std::pair<std::string, GLProgramInfo*> map_item(name, info);
        _map.insert(map_item);
    }
    return true;
}

bool GLProgramManager::load(std::string program_file)
{
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile(program_file);
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    int listCount = DICTOOL->getArrayCount_json(doc, "program");
    const char *name, *fsh1, *fsh2, *lookup;
    bool face, time_sec;
    int decoration;
    GLProgramInfo *info;
    for(int i=0;i<listCount; i++)
    {
        const rapidjson::Value& item = DICTOOL->getDictionaryFromArray_json(doc, "program", i);
        name = DICTOOL->getStringValue_json(item, "name");
        fsh1 = DICTOOL->getStringValue_json(item, "fsh1");
        fsh2 = DICTOOL->getStringValue_json(item, "fsh2");
        lookup = DICTOOL->getStringValue_json(item, "lookup");
        face = DICTOOL->getBooleanValue_json(item, "face", false);
        time_sec = DICTOOL->getBooleanValue_json(item, "time", false);
        decoration = DICTOOL->getIntValue_json(item, "deco", 0);
        
        info = new GLProgramInfo();
        info->set(fsh1, fsh2, lookup, face, time_sec, decoration);
        std::pair<std::string, GLProgramInfo*> map_item(name, info);
        _map.insert(map_item);
    }
    return true;
}

GLProgramInfo* GLProgramManager::getGLProgramInfo(std::string program)
{
    std::unordered_map<std::string, GLProgramInfo*>::const_iterator got = _map.find(program);
    if ( got == _map.end() )
    {
        CCLOG("not found");
        got = _map.find("Origin");
    }
    GLProgramInfo *info = (GLProgramInfo*)got->second;
    if(info->_program1 == nullptr)
    {
        GLProgram *glprogram1, *glprogram2;
        glprogram1 = GLProgram::createWithFilenames("multiTexture.vsh", info->_fsh1);
        glprogram1->retain();
        if(info->_fsh2.length()>0)
        {
            glprogram2 = GLProgram::createWithFilenames("multiTexture.vsh", info->_fsh2);
            glprogram2->retain();
        }
        else
            glprogram2 = nullptr;
        
        if(info->_lookupfile.length()>0)
        {
            auto lookup_texture = Sprite::create(info->_lookupfile);
            lookup_texture->retain();
            info->set(glprogram1, glprogram2, lookup_texture->getTexture());
        }
        else
        {
            info->set(glprogram1, glprogram2, nullptr);
        }

    }
    return info;
}