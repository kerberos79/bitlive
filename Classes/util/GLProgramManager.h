//
//  GLProgramManager.h
//  BitLive
//
//  Created by keros on 6/10/15.
//
//

#ifndef __BitLive__GLProgramManager__
#define __BitLive__GLProgramManager__

#include <stdio.h>
#include <string>
#include <vector>
#include <unordered_map>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "webrtc/base/timeutils.h"
#include "ui/CocosGUI.h"
#include "cocostudio/DictionaryHelper.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocostudio;

class GLProgramInfo
{
public:
    GLProgramInfo()
    :_program1(nullptr),_program2(nullptr),_lookup(nullptr)
    {
    }
    void set(GLProgram* program1, GLProgram* program2, Texture2D* lookup)
    {
        _program1 = program1;
        _program2 = program2;
        _lookup = lookup;
    }
    
    void set(const char *fsh1, const char *fsh2, const char *lookupfile, bool face = false, bool time_sec = false, int decoration = 0)
    {
        if(fsh1)
            _fsh1 = fsh1;
        if(fsh2)
            _fsh2 = fsh2;
        if(lookupfile)
            _lookupfile = lookupfile;
        _face = face;
        _time = time_sec;
        _decoration = decoration;
    }
    GLProgram* _program1;
    GLProgram* _program2;
    Texture2D* _lookup;
    std::string _fsh1;
    std::string _fsh2;
    std::string _lookupfile;
    bool _face, _time;
    int _decoration;
};

class GLProgramManager
{
public:
    GLProgramManager();
    ~GLProgramManager();
    
    static int BiteralBlurRadius;
    static void initializeDevice();
    void add(const char *name, const char *fsh1, const char *fsh2=nullptr, const char *lookup=nullptr);
    bool load(std::string program_file);
    bool loadAndAdd(std::string program_file);
    GLProgramInfo* getGLProgramInfo(std::string program);
    
private:
    static uint32 oldtime;
    bool init();
    std::unordered_map<std::string, GLProgramInfo*> _map;
    rapidjson::Document _doc;
    int _listCount, _currentCount;
    const char *_name, *_vsh1, *_vsh2, *_fsh1, *_fsh2, *_lookup;
    GLProgram *_glprogram1, *_glprogram2;
    Sprite *_lookupTexture;
};
#endif /* defined(__BitLive__GLProgramManager__) */
