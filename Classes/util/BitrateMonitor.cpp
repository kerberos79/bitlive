//
//  BitrateMonitor.cpp
//  BitLive
//
//  Created by kerberos on 8/3/15.
//
//

#include "BitrateMonitor.h"

BitrateMonitor::BitrateMonitor()
{
    
}

BitrateMonitor::~BitrateMonitor()
{
    
}

BitrateMonitor* BitrateMonitor::create()
{
    BitrateMonitor *monitor = new BitrateMonitor();
    if(monitor)
    {
        return monitor;
    }
    
    return nullptr;
}

void BitrateMonitor::setBitrte(int bitrate)
{
    _bitrate = bitrate;
}

void BitrateMonitor::start(long time)
{
    _starttime = time;
    _count = 0;
}

bool BitrateMonitor::add(long time)
{
    _count++;
    unsigned int bitrate;
    unsigned int duration = time - _starttime;
    if( duration > 2000000 )
    {
        bitrate = duration / _count;
        _starttime = time;
        _count = 0;
    }
    else
        return false;
    CCLOG("du:%d, bit:%d, count:%d", duration, bitrate, _count);
    if(bitrate < 150000)
    {
        _requestBitrate = 1;
    }
    else if(bitrate < 300000)
    {
        _requestBitrate = 2;
    }
    else
    {
        _requestBitrate = 4;
    }
    
    if(_requestBitrate != _bitrate)
    {
        return true;
    }
    
    return false;
}