#include "AndroidMP3Decoder.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

AndroidMP3Decoder* AndroidMP3Decoder::sAndroidMP3Decoder = nullptr;
std::string AndroidMP3Decoder::writablePath;
AndroidMP3Decoder *decoderIF;
// this callback handler is called every time a buffer finishes playing
void bqPlayerCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
    assert(bq == bqPlayerBufferQueue);
    assert(NULL == context);
    decoderIF->playCallback(bq, context);
}

AndroidMP3Decoder* AndroidMP3Decoder::getInstance()
{
    if (sAndroidMP3Decoder == nullptr)
    {
    	sAndroidMP3Decoder = new AndroidMP3Decoder();
    }
    return sAndroidMP3Decoder;
}

AndroidMP3Decoder::AndroidMP3Decoder()
:isPlayed(false), hasMixedSound(false), asset(nullptr)
{
	pcmBuffer = new unsigned char[PCM_BUFFER_BYTESIZE];
	mixBuffer = new unsigned char[PCM_BUFFER_BYTESIZE];
	int volumeLevel = 50;
	volume = -1 * (volumeLevel - 100) * (volumeLevel - 100) /2;
	createEngine();
    decoderIF = this;
}

AndroidMP3Decoder::~AndroidMP3Decoder() {
	shutdown();

    if(pcmBuffer!=NULL) {
    	delete pcmBuffer;
    	pcmBuffer = NULL;
    }
    if(mixBuffer!=NULL) {
    	delete mixBuffer;
    	mixBuffer = NULL;
    }
}

// create the engine and output mix objects
void AndroidMP3Decoder::createEngine()
{
    SLresult result;

	CCLOG("AndroidMP3Decoder createEngine");
    // create engine
    result = slCreateEngine(&engineObject, 0, NULL, 0, NULL, NULL);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the engine
    result = (*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the engine interface, which is needed in order to create other objects
    result = (*engineObject)->GetInterface(engineObject, SL_IID_ENGINE, &engineEngine);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // create output mix, with environmental reverb specified as a non-required interface
    const SLInterfaceID ids[1] = {SL_IID_ENVIRONMENTALREVERB};
    const SLboolean req[1] = {SL_BOOLEAN_FALSE};
    result = (*engineEngine)->CreateOutputMix(engineEngine, &outputMixObject, 1, ids, req);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the output mix
    result = (*outputMixObject)->Realize(outputMixObject, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the environmental reverb interface
    // this could fail if the environmental reverb effect is not available,
    // either because the feature is not present, excessive CPU load, or
    // the required MODIFY_AUDIO_SETTINGS permission was not requested and granted
    result = (*outputMixObject)->GetInterface(outputMixObject, SL_IID_ENVIRONMENTALREVERB,
            &outputMixEnvironmentalReverb);
    if (SL_RESULT_SUCCESS == result) {
        result = (*outputMixEnvironmentalReverb)->SetEnvironmentalReverbProperties(
                outputMixEnvironmentalReverb, &reverbSettings);
        (void)result;
    }
    // ignore unsuccessful result codes for environmental reverb, as it is optional for this example

}

// create buffer queue audio player
void AndroidMP3Decoder::createBufferQueueAudioPlayer(unsigned int newSamplerate, int newChannels, bool callback)
{
	CCLOG("AndroidMP3Decoder createBufferQueueAudioPlayer");
    SLresult result;
    SLuint32 channelMask;
    if(newChannels==1) {
    	channelMask = SL_SPEAKER_FRONT_CENTER;
    }
    else if(newChannels==2) {
    	channelMask = SL_SPEAKER_FRONT_LEFT|SL_SPEAKER_FRONT_RIGHT;
    }

    // configure audio source
    SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
    SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, (unsigned int)newChannels, (SLuint32)newSamplerate*1000/*SL_SAMPLINGRATE_44_1*/,
        SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
        channelMask, SL_BYTEORDER_LITTLEENDIAN};
    SLDataSource audioSrc = {&loc_bufq, &format_pcm};

    // configure audio sink
    SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, outputMixObject};
    SLDataSink audioSnk = {&loc_outmix, NULL};

    // create audio player
    const SLInterfaceID ids[3] = {SL_IID_BUFFERQUEUE, SL_IID_EFFECTSEND,
            /*SL_IID_MUTESOLO,*/ SL_IID_VOLUME};
    const SLboolean req[3] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE,
            /*SL_BOOLEAN_TRUE,*/ SL_BOOLEAN_TRUE};
    result = (*engineEngine)->CreateAudioPlayer(engineEngine, &bqPlayerObject, &audioSrc, &audioSnk,
            3, ids, req);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the player
    result = (*bqPlayerObject)->Realize(bqPlayerObject, SL_BOOLEAN_FALSE);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the play interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_PLAY, &bqPlayerPlay);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the buffer queue interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_BUFFERQUEUE,
            &bqPlayerBufferQueue);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

		// register callback on the buffer queue
		result = (*bqPlayerBufferQueue)->RegisterCallback(bqPlayerBufferQueue, bqPlayerCallback, NULL);
		assert(SL_RESULT_SUCCESS == result);
		(void)result;

    // get the effect send interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_EFFECTSEND,
            &bqPlayerEffectSend);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    // get the volume interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_VOLUME, &bqPlayerVolume);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    result = (*bqPlayerVolume)->SetVolumeLevel(bqPlayerVolume, volume);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

}

void AndroidMP3Decoder::play() {
	if(bqPlayerPlay==NULL)
		return;
    SLresult result;
    result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
}

void AndroidMP3Decoder::pause() {
	if(bqPlayerPlay==NULL)
		return;
    SLresult result;
    result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PAUSED);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
}

void AndroidMP3Decoder::resume() {
	if(bqPlayerPlay==NULL)
		return;
    SLresult result;
    result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
}

void AndroidMP3Decoder::stop() {
	CCLOG("AndroidMP3Decoder stop");
	if(bqPlayerPlay==NULL)
		return;
    SLresult result;
    result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_STOPPED);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

    result = (*bqPlayerBufferQueue)->Clear(bqPlayerBufferQueue);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;

}

void AndroidMP3Decoder::setVolume(int level) {
	volume = -1 * (level - 100) * (level - 100) /2;
	if(bqPlayerObject==NULL)
		return;
    SLresult result;
    result = (*bqPlayerVolume)->SetVolumeLevel(bqPlayerVolume, volume);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
}

void AndroidMP3Decoder::decodeFile(std::string filename)
{
    int err;
    SLresult result;

    if(!isPlayed)
    {
		createBufferQueueAudioPlayer(16000, 1, true);
		isPlayed = true;
    }

    if(asset!=nullptr)
    	AAsset_close(asset);
    if((asset = AAssetManager_open(cocos2d::FileUtilsAndroid::getAssetManager(), filename.c_str(), AASSET_MODE_UNKNOWN))==nullptr)
    {
    	CCLOG("error : AAssetManager_open %s", filename.c_str());
    	return;
    }
    totalByteSize = AAsset_getLength(asset);
    if((err = AAsset_seek(asset, WAV_HEADER_BYTESIZE+PCM_BUFFER_BYTESIZE, SEEK_SET)) == -1)
    {
    	CCLOG("error : AAsset_seek");
    	return;
    }
	int bytes = AAsset_read(asset, pcmBuffer, PCM_BUFFER_BYTESIZE);
	play();
	copyToMixingBufferList(pcmBuffer, bytes);
	criticalsection_.Enter();
	hasMixedSound = true;
	criticalsection_.Leave();
	result = (*bqPlayerBufferQueue)->Enqueue(bqPlayerBufferQueue, pcmBuffer, bytes);
}
// this callback handler is called every time a buffer finishes playing
void AndroidMP3Decoder::playCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
	int err;
    assert(bq == bqPlayerBufferQueue);
    assert(NULL == context);
	int bytes = AAsset_read(asset, pcmBuffer, PCM_BUFFER_BYTESIZE);
	if(bytes<PCM_BUFFER_BYTESIZE)
	{
    	AAsset_close(asset);
    	asset = nullptr;
		stop();
		criticalsection_.Enter();
		hasMixedSound = false;
		criticalsection_.Leave();
	}
	else
	{
		copyToMixingBufferList(pcmBuffer, bytes);
		(*bqPlayerBufferQueue)->Enqueue(bqPlayerBufferQueue, pcmBuffer, bytes);
	}
}

void AndroidMP3Decoder::shutdown()
{
    // destroy buffer queue audio player object, and invalidate all associated interfaces
    if (bqPlayerObject != NULL) {
        (*bqPlayerObject)->Destroy(bqPlayerObject);
        bqPlayerObject = NULL;
        bqPlayerPlay = NULL;
        bqPlayerBufferQueue = NULL;
        bqPlayerEffectSend = NULL;
        bqPlayerMuteSolo = NULL;
        bqPlayerVolume = NULL;
    }

    // destroy output mix object, and invalidate all associated interfaces
    if (outputMixObject != NULL) {
        (*outputMixObject)->Destroy(outputMixObject);
        outputMixObject = NULL;
        outputMixEnvironmentalReverb = NULL;
    }

    // destroy engine object, and invalidate all associated interfaces
    if (engineObject != NULL) {
        (*engineObject)->Destroy(engineObject);
        engineObject = NULL;
        engineEngine = NULL;
    }

}

void AndroidMP3Decoder::copyToMixingBufferList(unsigned char* buffer, int byte_size)
{
	criticalsection_.Enter();
	memcpy(mixBuffer, buffer, byte_size);
	mixCount++;
	criticalsection_.Leave();
}

void AndroidMP3Decoder::mixPcmBuffer(short* src, int size)
{
	criticalsection_.Enter();
	if(hasMixedSound)
	{
		int mixed;
		short* src_ch = src;
		short* mix_ch = (short*)mixBuffer;

//		memcpy(src_ch, mixBuffer, PCM_BUFFER_BYTESIZE);

		for(int i=0;i<PCM_BUFFER_SHORTSIZE;i++)
		{
			mixed = *src_ch + *mix_ch++;
			if (mixed > SHORT_MAX) mixed = SHORT_MAX;
			else if (mixed < SHORT_MIN) mixed = SHORT_MIN;
			*src_ch++ = (short)mixed;
		}

	}
	criticalsection_.Leave();
}

