//
//  IdentityManager.cpp
//  BitLive
//
//  Created by keros on 6/9/15.
//
//

#include "IdentityManager.h"

std::string IdentityManager::user_id = "";
std::string IdentityManager::result = "";

std::string IdentityManager::hasCountryCode(std::string username)
{
    std::string::size_type offset = 0;
    offset = username.find( "@gmail.com", offset );
    if(std::string::npos != offset)
    {
        return "";
    }
    if(username.front() == '+')
    {
        user_id = username.substr(1, username.length());
        std::vector<std::string> tokens;
        const std::string delimiters = "-";
        Tokenizer::tokenize(user_id, tokens, delimiters);
        return tokens.front();
    }
    return "";
}

std::string IdentityManager::makeUserID(std::string username)
{
    std::string::size_type offset = 0;
    offset = username.find( "@gmail.com", offset );
    if(std::string::npos != offset)
    {
        user_id = username;
        user_id.replace( offset, 10, ".gmail" );
        return user_id;
    }
    
    std::string::iterator dash_pos = std::remove(username.begin(), username.end(), '-');
    username.erase(dash_pos, username.end());
    if(username.front() == '+')
    {
        user_id = username.substr(1, username.length());
    }
    else if(username.front() == '0')
    {
        user_id = cocos2d::Device::getCountryCode() + username.substr(1, username.length());
    }
    else
        user_id = username;
    return user_id;
}

std::string IdentityManager::parseUserID(std::string userID)
{
    
    std::string::size_type offset = 0;
    offset = userID.find( ".gmail", offset );
    if(std::string::npos != offset)
    {
        userID.replace( offset, 10, "@gmail.com" );
        return userID;
    }
    
    return userID;
}
