//
//  FaceDetectHelper.cpp
//  BitLive
//
//  Created by keros on 10/5/15.
//
//

#include "FaceDetectHelper.h"

const int FaceDetectHelper::Emoticon = 0x01;
const int FaceDetectHelper::Sticker = 0x02;
const int FaceDetectHelper::GLProgram = 0x04;

FaceDetectHelper::FaceDetectHelper()
:_flag(0)
{
    
}

FaceDetectHelper::~FaceDetectHelper()
{
    
}

void FaceDetectHelper::enable(int flag)
{
    _flag |= flag;
    
    Device::enableFaceDetect(true);
}

void FaceDetectHelper::disable(int flag)
{
    _flag ^= flag;
    
    if(!_flag)
        Device::enableFaceDetect(false);
}