//
//  Tokenizer.h
//  BitLive
//
//  Created by keros on 6/12/15.
//
//

#ifndef __BitLive__Tokenizer__
#define __BitLive__Tokenizer__

#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>

class Tokenizer
{
public:
    static void tokenize(const std::string& str,
               std::vector<std::string>& tokens,
                         const std::string& delimiters);
    
};
#endif /* defined(__BitLive__Tokenizer__) */
