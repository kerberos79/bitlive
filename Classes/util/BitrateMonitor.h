//
//  BitrateMonitor.h
//  BitLive
//
//  Created by kerberos on 8/3/15.
//
//

#ifndef __BitLive__BitrateMonitor__
#define __BitLive__BitrateMonitor__

#include <stdio.h>
#include "cocos2d.h"

class BitrateMonitor
{
public:
    BitrateMonitor();
    ~BitrateMonitor();
    static BitrateMonitor* create();
    void setBitrte(int bitrate);
    void start(long time);
    bool add(long time);
    unsigned int _requestBitrate;
private:
    long _starttime;
    int _count;
    unsigned int _bitrate;
};

#endif /* defined(__BitLive__BitrateMonitor__) */
