//
//  EffectMenu.h
//  BitLive
//
//  Created by keros on 6/14/15.
//
//

#ifndef __BitLive__EffectMenu__
#define __BitLive__EffectMenu__

#include <stdio.h>
#include <string>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "extra/Strings.h"
#include "datatype/UIMessage.h"
#include "view/SliderEx.h"
#include "view/ToggleButton.h"
#include "view/MainScene.h"
#include "view/PageViewEx.h"
#include "view/LayerColorExtension.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;

class EffectMenuItem
{
public:
    EffectMenuItem():_scrollView(nullptr)
    {
        
    }
    ~EffectMenuItem()
    {
        
    }
    
    std::vector<std::string> _list;
    ui::ScrollView*          _scrollView;
};

class EffectMenu : public LayerColor, public LayerColorExtension
{
public:
    static Color4B TitleBackgroundColor;
    static Color4B BodyBackgroundColor;
    static Color3B TitleFontColor;
    static Color3B BodyFontColor;
    static std::string FontType;
    static int FontSize;
    static int TitleHeight;
    static int Height;
    static int ItemWidth;
    static int ItemHeight;
    static std::string CloseButton;
    static std::string PreviousButtonNormal;
    static std::string PreviousButtonSelect;
    static std::string OriginButtonNormal;
    static std::string OriginButtonSelect;
    static std::string ExposureSprite;
    static std::string ExposureSliderTrack;
    static std::string ExposureSliderThumb;
    static std::string ExposureSliderProgress;
    
    ~EffectMenu();
    static EffectMenu* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    void setSelectedButton(ToggleButton* button);
    virtual void show(bool isShow);
    virtual cocos2d::Size& getSize();
    virtual Color4B& getBackgroundColor();
    virtual void setBackgroundColor(Color4B& color);
private:
    bool init(MainScene* mainscene);
    void addSubView(int tag);
    void menuRootCallback(Ref* pSender);
    void menuSubCallback(Ref* pSender);
    void menuNoEffectCallback(Ref* pSender);
    void menuPreviousCallback(Ref* pSender);
    void menuCloseCallback(Ref* pSender);
    void afterInvisibleAction(Node* sender);
    
    MainScene* mMainScene;
    PageViewEx* mPageView;
    ToggleButton* mSelectedButton;
    ToggleButton* mPreviousButton;
    Button* mOriginButton;
    SliderEx* mExposureSlider;
    Size mVisibleSize;
    std::string mSelectedEffect;
    std::vector<EffectMenuItem*> _menuList;
    ui::ScrollView *_rootScrollView, *_subScrollView;
    float scrollViewPos, scrollViewHeight;
};
#endif /* defined(__BitLive__EffectMenu__) */
