//
//  ContactDialog.cpp
//  BitLive
//
//  Created by keros on 7/15/15.
//
//

#include "ContactDialog.h"


Color4B ContactDialog::BackgroundColor;
Color3B ContactDialog::TitleFontColor;
Color3B ContactDialog::FontColor;
int ContactDialog::FontSize = 0;
int ContactDialog::Height;
int ContactDialog::ItemHeight = 0;
std::string ContactDialog::ItemButtonNormalTop;
std::string ContactDialog::ItemButtonSelectTop;
std::string ContactDialog::ItemButtonNormal;
std::string ContactDialog::ItemButtonSelect;
std::string ContactDialog::CancelButtonNormal;
std::string ContactDialog::CancelButtonSelect;

ContactDialog::~ContactDialog()
{
}

ContactDialog* ContactDialog::create(MainScene *mainscene, std::vector<std::string>* list)
{
    ContactDialog * dialog = new (std::nothrow) ContactDialog();
    if( dialog && dialog->init(mainscene, list))
    {
        dialog->setPosition(Vec2::ZERO);
        dialog->autorelease();
        return dialog;
    }
    CC_SAFE_DELETE(dialog);
    return nullptr;
}


void ContactDialog::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& contactDialog = DICTOOL->getSubDictionary_json(doc, "contact_dialog");
    ContactDialog::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(contactDialog, "background_color"));
    ContactDialog::TitleFontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(contactDialog, "title_font_color"));
    ContactDialog::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(contactDialog, "font_color"));
    ContactDialog::FontSize = DICTOOL->getIntValue_json(contactDialog, "font_size") * MainScene::LayoutScale;
    ContactDialog::Height = DICTOOL->getIntValue_json(contactDialog, "height") * MainScene::LayoutScale;
    ContactDialog::ItemHeight = DICTOOL->getIntValue_json(contactDialog, "item_height") * MainScene::LayoutScale;
    ContactDialog::ItemButtonNormalTop = DICTOOL->getStringValue_json(contactDialog, "item_button_normal_top");
    ContactDialog::ItemButtonSelectTop = DICTOOL->getStringValue_json(contactDialog, "item_button_select_top");
    ContactDialog::ItemButtonNormal = DICTOOL->getStringValue_json(contactDialog, "item_button_normal");
    ContactDialog::ItemButtonSelect = DICTOOL->getStringValue_json(contactDialog, "item_button_select");
    ContactDialog::CancelButtonNormal = DICTOOL->getStringValue_json(contactDialog, "cancel_button_normal");
    ContactDialog::CancelButtonSelect = DICTOOL->getStringValue_json(contactDialog, "cancel_button_select");
}

bool ContactDialog::init(MainScene *mainscene, std::vector<std::string>* list)
{
    mMainScene = mainscene;
    
    _CustomListener = EventListenerTouchOneByOne::create();
    _CustomListener->retain();
    _CustomListener->setSwallowTouches(true);
    _CustomListener->onTouchBegan = CC_CALLBACK_2(ContactDialog::onTouchBegan, this);
    _CustomListener->onTouchMoved = CC_CALLBACK_2(ContactDialog::onTouchMoved, this);
    _CustomListener->onTouchEnded = CC_CALLBACK_2(ContactDialog::onTouchEnded, this);
    _CustomListener->onTouchCancelled = CC_CALLBACK_2(ContactDialog::onTouchCancelled, this);
    
    _touchListener = _CustomListener;
    
    if(!initWithColor(BackgroundColor, visibleSize.width, visibleSize.height))
        return false;
    this->show(true);
    this->scheduleUpdate();
    
    std::vector<std::string>::iterator itor = list->begin();
    for (; itor != list->end(); ++itor)
    {
        _list.push_back(*itor);
    }
    return true;
}

void ContactDialog::update(float dt)
{
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("invite.json");
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return;
    }
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    ssize_t count = _list.size();
    
    mContactLayer = LayerColor::create();
    mContactLayer->setAnchorPoint(Vec2(0.5, 0));
    
    int layerHeight = 0;
    int i=0;
    Button* button;
    std::vector<std::string>::iterator itor = _list.begin();
    ++itor;
    for (; itor != _list.end(); ++itor, ++i)
    {
        if(layerHeight==0)
        {
            button = Button::create(ContactDialog::ItemButtonNormalTop, ContactDialog::ItemButtonSelectTop);
            layerHeight = button->getContentSize().height * count;
        }
        else
        {
            button = Button::create(ContactDialog::ItemButtonNormal, ContactDialog::ItemButtonSelect);
        }
        button->setScale9Enabled(true);
        button->setTitleText(*itor);
        button->setTitleFontSize(ContactDialog::FontSize);
        button->setTitleColor(ContactDialog::FontColor);
        button->setPosition(Vec2(button->getContentSize().width * 0.5f,
                                 layerHeight - (button->getContentSize().height*(float(i)+0.5f))));
        button->addClickEventListener(CC_CALLBACK_1(MainScene::choiceItemCallback, mMainScene));
        
        mContactLayer->addChild(button);
    }
    button = Button::create(ContactDialog::ItemButtonNormal, ContactDialog::ItemButtonSelect);
    button->setScale9Enabled(true);
    button->setTitleText(DICTOOL->getStringValue_json(doc, "cancel"));
    button->setTitleFontSize(ContactDialog::FontSize);
    button->setTitleColor(ContactDialog::FontColor);
    button->setPosition(Vec2(button->getContentSize().width * 0.5f,
                             layerHeight - (button->getContentSize().height*(float(i)+0.5f))));
    button->addClickEventListener(CC_CALLBACK_1(ContactDialog::OnCancelButtonClick, this));
    mContactLayer->addChild(button);
    
    int pos = (visibleSize.width - button->getContentSize().width) * 0.5f;
    
    _startPos = Vec2(pos, -layerHeight);
    _endPos = Vec2(pos, 0);
    mContactLayer->setPosition(_startPos);
    mContactLayer->setContentSize(Size(button->getContentSize().width, layerHeight));
    this->addChild(mContactLayer);
    this->unscheduleUpdate();
    
    _visibleAction = Sequence::create(DelayTime::create(0.4f),
                                      EaseExponentialOut::create(MoveTo::create(0.4f, _endPos)),
                                      nullptr);
    ;
    _visibleAction->retain();
    
    _invisibleAction = Sequence::create(EaseExponentialOut::create(MoveTo::create(0.4f, _startPos)),
                                        CallFuncN::create( CC_CALLBACK_1(ContactDialog::afterInvisibleAction, this)),
                                        nullptr);
    _invisibleAction->retain();
    mContactLayer->runAction(_visibleAction);
}

bool ContactDialog::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    return true;
}

void ContactDialog::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
}

void ContactDialog::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
}

void ContactDialog::onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
}

void ContactDialog::menuSelectCallback(Ref* pSender)
{
    
}

void ContactDialog::OnCancelButtonClick(Ref* pSender)
{
    mContactLayer->runAction(_invisibleAction);
}

void ContactDialog::show(bool isShow)
{
    if(isShow)
    {
        _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener, this);
    }
    else
    {
        _eventDispatcher->removeEventListener(_touchListener);
        
        mContactLayer->runAction(_invisibleAction);
    }
    this->setVisible(isShow);
}

void ContactDialog::afterInvisibleAction(Node* sender)
{
    _visibleAction->release();
    _invisibleAction->release();
    mMainScene->removeChild(this);
}