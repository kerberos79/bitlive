//
//  BlurredLayer.h
//  BitLive
//
//  Created by keros on 7/22/15.
//
//

#ifndef __BitLive__BlurredLayer__
#define __BitLive__BlurredLayer__

#include <stdio.h>
#include "cocos2d.h"
#include "view/MainScene.h"
#include "platform/CCDevice.h"
#include "util/GLProgramManager.h"

USING_NS_CC;

class MainScene;

class BlurredLayer : public RenderTexture
{
public:
    ~BlurredLayer();
    static BlurredLayer * create(MainScene *mainscene, int width, int height, int degree);
    bool init(MainScene *mainscene, int width, int height, int degree, Texture2D::PixelFormat eFormat);
    virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags);
private:
    MainScene *mMainScene;
    RenderTexture* rtX;
    Sprite* stepX;
};
#endif /* defined(__BitLive__BlurredLayer__) */
