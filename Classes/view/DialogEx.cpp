//
//  DialogEx.cpp
//  AnicameraMAC
//
//  Created by kerberos on 12/9/14.
//
//

#include "DialogEx.h"


Color4B DialogEx::BackgroundColor;
Color3B DialogEx::FontColor;
int DialogEx::TitleFontSize = 0;
int DialogEx::BodyFontSize = 0;
int DialogEx::MarginTop = 0;
int DialogEx::MarginBottom = 0;
std::string DialogEx::OkButtonNormal;
std::string DialogEx::OkButtonSelect;
std::string DialogEx::CancelButtonNormal;
std::string DialogEx::CancelButtonSelect;

DialogEx::~DialogEx()
{
}

DialogEx * DialogEx::create(const std::string title, const std::string body, DialogEx::Type type)
{
    DialogEx * dialog = new (std::nothrow) DialogEx();
    if( dialog && dialog->init(title, body, type))
    {
        dialog->setPosition(Vec2::ZERO);
        dialog->autorelease();
        return dialog;
    }
    CC_SAFE_DELETE(dialog);
    return nullptr;
}

bool DialogEx::init(const std::string title, const std::string body, DialogEx::Type type)
{
    Size visibleSize = Director::getInstance()->getVisibleSize();
    int width = visibleSize.width;
    int height = DialogEx::MarginBottom;
    // save button
    _okButton = Button::create();
    _okButton->loadTextures(DialogEx::OkButtonNormal, DialogEx::OkButtonSelect, "");
    _okButton->setPosition(Vec2(width/2, height+_okButton->getContentSize().height/2));
    _okButton->setTouchEnabled(true);
    
    height += _okButton->getContentSize().height;
    // Add the title
    _titleText = Text::create(title, "Helvetica", DialogEx::TitleFontSize);
    
    // add messege
    _bodyText = RichText::create();
    _bodyText->ignoreContentAdaptWithSize(false);
    _bodyText->setContentSize(Size(width * 0.8, _titleText->getContentSize().height*3));
    
    RichElementText* re3 = RichElementText::create(3,DialogEx::FontColor, 255, body, "Helvetica", DialogEx::BodyFontSize);
    _bodyText->pushBackElement(re3);
    _bodyText->setPosition(Vec2(width / 2, height + _bodyText->getContentSize().height*0.6));
    height += _bodyText->getContentSize().height*1.2;
    
    _titleText->setColor(DialogEx::FontColor);
    _titleText->setPosition(Vec2(width / 2, height + _titleText->getContentSize().height*0.6));
    height += _titleText->getContentSize().height*1.2 + DialogEx::MarginTop;
    
    
    auto layout = LayerColor::create(DialogEx::BackgroundColor, width, height);
    layout->setPosition(Vec2(0, (visibleSize.height - height)/2));
    layout->addChild(_titleText);
    layout->addChild(_bodyText);
    layout->addChild(_okButton);
    
    if(!initWithColor(Color4B(0x0, 0, 0, 0x0), Director::getInstance()->getVisibleSize().width, Director::getInstance()->getVisibleSize().height))
        return false;
    
    this->addChild(layout);
    
    _CustomListener = EventListenerTouchOneByOne::create();
    _CustomListener->retain();
    _CustomListener->setSwallowTouches(true);
    _CustomListener->onTouchBegan = CC_CALLBACK_2(DialogEx::onTouchBegan, this);
    _CustomListener->onTouchMoved = CC_CALLBACK_2(DialogEx::onTouchMoved, this);
    _CustomListener->onTouchEnded = CC_CALLBACK_2(DialogEx::onTouchEnded, this);
    _CustomListener->onTouchCancelled = CC_CALLBACK_2(DialogEx::onTouchCancelled, this);
    
    _touchListener = _CustomListener;
    return true;
}

bool DialogEx::addOkButtonClickListener(std::function<void(Ref*)> callback)
{
    if(_okButton==nullptr)
        return false;
    _okButton->addClickEventListener(callback);
    return true;
}

void DialogEx::setTitle(const std::string title)
{
    _titleText->setString(title);
}

void DialogEx::setBody(const std::string body)
{
    _bodyText->removeElement(0);
    RichElementText* re3 = RichElementText::create(3, Color3B(0x59, 0xc6, 0xce), 255, body, "Helvetica", DialogEx::BodyFontSize);
    _bodyText->pushBackElement(re3);
    
}

bool DialogEx::onTouchBegan(Touch *touch, Event *event)
{
    return true;
}

void DialogEx::onTouchMoved(Touch *touch, Event *event)
{
}

void DialogEx::onTouchEnded(Touch *touch, Event *event)
{
}

void DialogEx::onTouchCancelled(Touch* touch, Event* unused_event)
{
}
void DialogEx::show(bool isShow)
{
    if(isShow)
    {
        _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener, this);
    }
    else
    {
        _eventDispatcher->removeEventListener(_touchListener);
    }
    this->setVisible(isShow);
}
