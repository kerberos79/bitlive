//
//  ExitMenu.cpp
//  BitLive
//
//  Created by keros on 6/14/15.
//
//

#include "ExitMenu.h"

Color4B ExitMenu::BackgroundColor;
Color3B ExitMenu::FontColor;
int ExitMenu::FontSize;
int ExitMenu::Height;
std::string ExitMenu::OkButtonNormal;
std::string ExitMenu::OkButtonSelect;
std::string ExitMenu::CancelButtonNormal;
std::string ExitMenu::CancelButtonSelect;

ExitMenu::~ExitMenu()
{
}

ExitMenu * ExitMenu::create(MainScene* mainscene)
{
    ExitMenu * menu = new (std::nothrow) ExitMenu();
    if( menu && menu->init(mainscene))
    {
        menu->setPosition(Vec2::ZERO);
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void ExitMenu::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& exitMenu = DICTOOL->getSubDictionary_json(doc, "exit_menu");
    ExitMenu::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(exitMenu, "background_color"));
    ExitMenu::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(exitMenu, "font_color"));
    ExitMenu::FontSize = DICTOOL->getIntValue_json(exitMenu, "font_size") * MainScene::LayoutScale;
    ExitMenu::Height = DICTOOL->getIntValue_json(exitMenu, "height") * MainScene::LayoutScale;
    ExitMenu::OkButtonNormal = DICTOOL->getStringValue_json(exitMenu, "ok_button_normal");
    ExitMenu::OkButtonSelect = DICTOOL->getStringValue_json(exitMenu, "ok_button_select");
    ExitMenu::CancelButtonNormal = DICTOOL->getStringValue_json(exitMenu, "cancel_button_normal");
    ExitMenu::CancelButtonSelect = DICTOOL->getStringValue_json(exitMenu, "cancel_button_select");
}

bool ExitMenu::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("submenu.json");
    rapidjson::Document doc;
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    auto _title = Text::create(DICTOOL->getStringValue_json(doc, "submenu_exit"), Strings::getFontType(), ExitMenu::FontSize);
    _title->setColor(ExitMenu::FontColor);
    _title->setPosition(Vec2(visibleSize.width * 0.5f, ExitMenu::Height * 0.8f));
    
    // ok button
    auto _okButton = Button::create();
    _okButton->loadTextures(ExitMenu::OkButtonNormal, "", "");
    _okButton->setPosition(Vec2(visibleSize.width * 0.3f, ExitMenu::Height * 0.4f));
    _okButton->addClickEventListener(CC_CALLBACK_1(ExitMenu::OnOkButtonClick,this));
    
    // cancel button
    auto _cancelButton = Button::create();
    _cancelButton->loadTextures(ExitMenu::CancelButtonNormal, "", "");
    _cancelButton->setPosition(Vec2(visibleSize.width * 0.7f, ExitMenu::Height * 0.4f));
    _cancelButton->addClickEventListener(CC_CALLBACK_1(ExitMenu::OnCancelButtonClick,this));
    
    if(!initWithColor(ExitMenu::BackgroundColor, visibleSize.width, ExitMenu::Height))
        return false;
    this->addChild(_title);
    this->addChild(_okButton);
    this->addChild(_cancelButton);
    _startPos = Vec2(0, -ExitMenu::Height);
    _endPos = Vec2::ZERO;
    _visibleAction = EaseExponentialOut::create(MoveTo::create(0.4f, _endPos));
    _visibleAction->retain();
    
    _invisibleAction = Sequence::create(EaseExponentialOut::create(MoveTo::create(0.4f, _startPos)),
                                        CallFuncN::create( CC_CALLBACK_1(ExitMenu::afterInvisibleAction, this)),
                                        nullptr);
    _invisibleAction->retain();
    return true;
}

void ExitMenu::OnOkButtonClick(Ref* pSender)
{
    mMainScene->OnExit();
    mMainScene->toggleMenuCallback(this);
}

void ExitMenu::OnCancelButtonClick(Ref* pSender)
{
    mMainScene->toggleMenuCallback(this);
}

void ExitMenu::show(bool isShow)
{
    if(isShow)
    {
        this->stopAllActions();
        this->setVisible(true);
        this->setPosition(_startPos);
        this->runAction(_visibleAction);
    }
    else
    {
        this->runAction(_invisibleAction);
    }
}

void ExitMenu::afterInvisibleAction(Node* sender)
{
    this->setVisible(false);
}

cocos2d::Size& ExitMenu::getSize()
{
    return (cocos2d::Size&)this->getContentSize();
}

Color4B& ExitMenu::getBackgroundColor()
{
    return ExitMenu::BackgroundColor;
}

void ExitMenu::setBackgroundColor(Color4B& color)
{
    _displayedColor.r = _realColor.r = color.r;
    _displayedColor.g = _realColor.g = color.g;
    _displayedColor.b = _realColor.b = color.b;
    _displayedOpacity = _realOpacity = color.a;
    
    updateColor();
}
