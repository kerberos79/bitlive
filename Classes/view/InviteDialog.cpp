//
//  InviteDialog.cpp
//  Vista
//
//  Created by kerberos on 2/11/15.
//
//

#include "InviteDialog.h"

Color4B InviteDialog::BackgroundColor1;
Color4B InviteDialog::BackgroundColor2;
Color3B InviteDialog::FontColor;
int InviteDialog::TitleFontSize;
int InviteDialog::BodyFontSize;
int InviteDialog::Height;
std::string InviteDialog::DefaultPhotoImage;
std::string InviteDialog::DefaultPhotoStencil;
std::string InviteDialog::CallButtonNormal;
std::string InviteDialog::CallButtonSelect;
std::string InviteDialog::OkButtonNormal;
std::string InviteDialog::OkButtonSelect;
std::string InviteDialog::CancelButtonNormal;
std::string InviteDialog::CancelButtonSelect;

InviteDialog::~InviteDialog()
{
    if(_contactInfo)
        delete _contactInfo;
}

InviteDialog * InviteDialog::create(MainScene *mainscene)
{
    InviteDialog * dialog = new (std::nothrow) InviteDialog();
    if( dialog && dialog->init(mainscene))
    {
        dialog->setPosition(Vec2::ZERO);
        dialog->autorelease();
        return dialog;
    }
    CC_SAFE_DELETE(dialog);
    return nullptr;
}

void InviteDialog::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& inviteDialog = DICTOOL->getSubDictionary_json(doc, "invite_dialog");
    InviteDialog::BackgroundColor1 = HexString::ConvertColor4B(DICTOOL->getStringValue_json(inviteDialog, "background_color1"));
    InviteDialog::BackgroundColor2 = HexString::ConvertColor4B(DICTOOL->getStringValue_json(inviteDialog, "background_color2"));
    InviteDialog::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(inviteDialog, "font_color"));
    InviteDialog::TitleFontSize = DICTOOL->getIntValue_json(inviteDialog, "title_font_size") * MainScene::LayoutScale;
    InviteDialog::BodyFontSize = DICTOOL->getIntValue_json(inviteDialog, "text_font_size") * MainScene::LayoutScale;
    InviteDialog::Height = DICTOOL->getIntValue_json(inviteDialog, "height") * MainScene::LayoutScale;
    InviteDialog::DefaultPhotoImage = DICTOOL->getStringValue_json(inviteDialog, "default_photo_image");
    InviteDialog::DefaultPhotoStencil = DICTOOL->getStringValue_json(inviteDialog, "default_photo_stencil");
    InviteDialog::CallButtonNormal = DICTOOL->getStringValue_json(inviteDialog, "call_button_normal");
    InviteDialog::CallButtonSelect = DICTOOL->getStringValue_json(inviteDialog, "call_button_select");
    InviteDialog::OkButtonNormal = DICTOOL->getStringValue_json(inviteDialog, "ok_button_normal");
    InviteDialog::OkButtonSelect = DICTOOL->getStringValue_json(inviteDialog, "ok_button_select");
    InviteDialog::CancelButtonNormal = DICTOOL->getStringValue_json(inviteDialog, "cancel_button_normal");
    InviteDialog::CancelButtonSelect = DICTOOL->getStringValue_json(inviteDialog, "cancel_button_select");
}

bool InviteDialog::init(MainScene *mainscene)
{
    mMainScene = mainscene;
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("invite.json");
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    _receiveString = DICTOOL->getStringValue_json(doc, "receive");
    _sendString = DICTOOL->getStringValue_json(doc, "send");
    _refusalString = DICTOOL->getStringValue_json(doc, "refusal");
    _cancelString = DICTOOL->getStringValue_json(doc, "cancel");
    _busyString = DICTOOL->getStringValue_json(doc, "busy");
    _notFoundUserString = DICTOOL->getStringValue_json(doc, "not_found_userid");
    _connectingString = DICTOOL->getStringValue_json(doc, "connecting");
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    _photo = Sprite::create(InviteDialog::DefaultPhotoImage);
    _photo->setPosition(Vec2(_photo->getContentSize().width * 0.5f, _photo->getContentSize().height*0.5f));

    auto stencil = Sprite::create(InviteDialog::DefaultPhotoStencil);
    stencil->setPosition(_photo->getContentSize() * 0.5f);
    
    auto clippedNode = ClippingNode::create();
    clippedNode->setPosition(Vec2((visibleSize.width - _photo->getContentSize().width)* 0.5f,
                                  InviteDialog::Height - _photo->getContentSize().height* 0.5f));
    clippedNode->setAlphaThreshold(0.5f);
    clippedNode->setStencil(stencil);
    clippedNode->addChild(_photo);

    // add title
    _titleText = LabelTTF::create(_receiveString, Strings::getFontType(), InviteDialog::TitleFontSize);
    _titleText->setColor(InviteDialog::FontColor);
    _titleText->setPosition(Vec2(visibleSize.width * 0.5f, InviteDialog::Height * 0.65f));
    
    // add messege
    _bodyText = LabelTTF::create(_receiveString, Strings::getFontType(), InviteDialog::BodyFontSize);
    _bodyText->setColor(InviteDialog::FontColor);
    _bodyText->setPosition(Vec2(visibleSize.width * 0.5f, InviteDialog::Height * 0.2f));
    _progressBar = InfiniteProgressBar::create();
    _progressBar->setPosition(Vec2((visibleSize.width - _progressBar->getContentSize().width) * 0.5f,
                                   (InviteDialog::Height- _progressBar->getContentSize().height) * 0.2f ));
    
    // call button
    _callButton = Button::create();
    _callButton->loadTextures(InviteDialog::CallButtonNormal, InviteDialog::CallButtonSelect, "");
    
    float x = (visibleSize.width - _callButton->getContentSize().width * 2 )/3.0f + _callButton->getContentSize().width * 0.5f;
    _centerPositionX = visibleSize.width * 0.5f;
    _rightPositionX = visibleSize.width -x;
    _callButton->setPosition(Vec2(x, InviteDialog::Height * 0.44f));
    _callButton->addClickEventListener(CC_CALLBACK_1(InviteDialog::OnCallButtonClick,this));
    
	// ok button
	_okButton = Button::create();
	_okButton->loadTextures(InviteDialog::OkButtonNormal, InviteDialog::OkButtonSelect, "");
	_okButton->setPosition(Vec2(_centerPositionX, InviteDialog::Height * 0.44f));
    
    _okButton->addClickEventListener(CC_CALLBACK_1(InviteDialog::OnOkButtonClick,this));
    
    // cancel button
	_cancelButton = Button::create();
    _cancelButton->loadTextures(InviteDialog::CancelButtonNormal, InviteDialog::CancelButtonSelect, "");
    _cancelButton->setPosition(Vec2(_rightPositionX, InviteDialog::Height * 0.44f));
    
    _cancelButton->addClickEventListener(CC_CALLBACK_1(InviteDialog::OnCancelButtonClick,this));
    
    // ok button
    auto closeButton = Button::create();
    closeButton->loadTextures("btn_close_white.png", "", "");
    closeButton->setPosition(Vec2(visibleSize.width - closeButton->getContentSize().width * 0.5f,
                                InviteDialog::Height - closeButton->getContentSize().height * 0.5f));
    
    closeButton->addClickEventListener(CC_CALLBACK_1(InviteDialog::OnCloseButtonClick,this));
    
    if(!initWithColor(InviteDialog::BackgroundColor1, InviteDialog::BackgroundColor2))
        return false;
    this->setContentSize(Size(visibleSize.width, InviteDialog::Height));
    this->addChild(_bodyText);
    this->addChild(_progressBar);
    this->addChild(_callButton);
    this->addChild(_okButton);
    this->addChild(closeButton);
    this->addChild(_cancelButton);
    this->addChild(clippedNode);
    this->addChild(_titleText);
    _contactInfo = nullptr;
    
    _startPos = Vec2(0, -InviteDialog::Height*1.3f);
    _endPos = Vec2::ZERO;
    _visibleAction = EaseExponentialOut::create(MoveTo::create(0.4f, _endPos));
    _visibleAction->retain();
    
    _invisibleAction = Sequence::create(EaseExponentialOut::create(MoveTo::create(0.4f, _startPos)),
                                        CallFuncN::create( CC_CALLBACK_1(InviteDialog::afterInvisibleAction, this)),
                                        nullptr);
    _invisibleAction->retain();
    return true;
}

void InviteDialog::show(InviteEventData::Type type, ContactInfo* info)
{
    _type = type;
    switch(_type)
    {
        case InviteEventData::Type::Recv:
        {
            if(info)
            {
                _titleText->setString(info->_name);
                setContactInfo(info);
                this->scheduleUpdate();
            }
            _bodyText->setVisible(false);
            _callButton->setVisible(true);
            _okButton->setVisible(false);
            _cancelButton->setPosition(Vec2(_rightPositionX, getContentSize().height * 0.44f));
            _cancelButton->setVisible(true);
            _progressBar->startAction();
            _progressBar->setVisible(true);
            
            this->stopAllActions();
            this->setVisible(true);
            this->setPosition(_startPos);
            this->runAction(_visibleAction);
            playSound();
            break;
        }
        case InviteEventData::Type::Send:
        {
            if(info)
            {
                _titleText->setString(info->_name);
                setContactInfo(info);
                this->scheduleUpdate();
            }
            _bodyText->setVisible(false);
            _callButton->setVisible(false);
            _okButton->setVisible(false);
            _cancelButton->setPosition(Vec2(_centerPositionX, getContentSize().height * 0.44f));
            _cancelButton->setVisible(true);
            _progressBar->startAction();
            _progressBar->setVisible(true);
            
            this->stopAllActions();
            this->setVisible(true);
            this->setPosition(_startPos);
            this->runAction(_visibleAction);
            playSound();
            break;
        }
        case InviteEventData::Type::Refusal:
        {
            _bodyText->setString(_refusalString);
            _bodyText->setVisible(true);
            _callButton->setVisible(false);
            _okButton->setVisible(true);
            _cancelButton->setVisible(false);
            _progressBar->removeAction();
            _progressBar->setVisible(false);
            this->setVisible(true);
            stopSound();
            break;
        }
        case InviteEventData::Type::Cancel:
        {
            _bodyText->setString(_cancelString);
            _bodyText->setVisible(true);
            _callButton->setVisible(false);
            _okButton->setVisible(true);
            _cancelButton->setVisible(false);
            _progressBar->removeAction();
            _progressBar->setVisible(false);
            this->setVisible(true);
            stopSound();
            break;
        }
        case InviteEventData::Type::Busy:
        {
            _bodyText->setString(_busyString);
            _bodyText->setVisible(true);
            _callButton->setVisible(false);
            _okButton->setVisible(true);
            _cancelButton->setVisible(false);
            _progressBar->removeAction();
            _progressBar->setVisible(false);
            this->setVisible(true);
            stopSound();
            break;
        }
        case InviteEventData::Type::Connecting:
        {
            _bodyText->setString(_connectingString);
            _bodyText->setVisible(false);
            _callButton->setVisible(false);
            _okButton->setVisible(false);
            _cancelButton->setPosition(Vec2(_centerPositionX, getContentSize().height * 0.44f));
            _cancelButton->setVisible(true);
            this->setVisible(true);
            break;
        }
        case InviteEventData::Type::Error:
        {
            _bodyText->setString(_notFoundUserString);
            _bodyText->setVisible(true);
            _callButton->setVisible(false);
            _okButton->setVisible(true);
            _cancelButton->setVisible(false);
            _progressBar->removeAction();
            _progressBar->setVisible(false);
            this->setVisible(true);
            stopSound();
            break;
        }
        case InviteEventData::Type::Message:
        {
            break;
        }
        case InviteEventData::Type::Connect: 
        {
            _progressBar->removeAction();
            _progressBar->setVisible(false);
            stopSound();

            this->stopAllActions();
            this->setVisible(true);
            this->setPosition(_endPos);
            this->runAction(_invisibleAction);
            return;
        }
    }
}

void InviteDialog::afterInvisibleAction(Node* sender)
{
    this->setVisible(false);
}

void InviteDialog::OnCallButtonClick(Ref* pSender)
{
    CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Accept);
}

void InviteDialog::OnOkButtonClick(Ref* pSender)
{
    stopSound();
    mMainScene->toggleMenuCallback(this);
}

void InviteDialog::OnCloseButtonClick(Ref* pSender)
{
    mMainScene->showToast(UIMessage::Toast::Call, _contactInfo->_name);
    mMainScene->toggleMenuCallback(this);
}

void InviteDialog::OnCancelButtonClick(Ref* pSender)
{
    switch(_type)
    {
        case InviteEventData::Type::Send:
        {
            CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Cancel);
            stopSound();
            break;
        }
        case InviteEventData::Type::Recv:
        {
            CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Refusal);
            stopSound();
            break;
        }
        case InviteEventData::Type::Refusal:
        {
            break;
        }
        case InviteEventData::Type::Cancel:
        {
            CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Cancel);
            stopSound();
            break;
        }
        case InviteEventData::Type::Busy:
        {
            stopSound();
            break;
        }
        case InviteEventData::Type::Connecting:
        {
            CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Cancel);
            stopSound();
            break;
        }
        case InviteEventData::Type::Message:
        {
            break;
        }
        case InviteEventData::Type::Error:
        {
            break;
        }
    }
    if(_progressBar->isVisible())
    {
        _progressBar->removeAction();
        _progressBar->setVisible(false);
    }
    mMainScene->toggleMenuCallback(this);
}

void InviteDialog::playSound()
{
    _currentRingerMode = Device::getRingerMode();
    switch(_currentRingerMode)
    {
        case 0: // silent
        {
            break;
        }
        case 1: // vibrate
        {
//            Device::vibrate(true);
            break;
        }
        case 2: // normal
        {
            _callSoundID = cocos2d::experimental::AudioEngine::play2d("call.mp3", true, 1.0f);
            break;
        }
    }
}

void InviteDialog::stopSound()
{
    switch(_currentRingerMode)
    {
        case 0: // silent
        {
            break;
        }
        case 1: // vibrate
        {
//            Device::vibrate(false);
            break;
        }
        case 2: // normal
        {
            cocos2d::experimental::AudioEngine::stop(_callSoundID);
            break;
        }
    }
}

void InviteDialog::show(bool isShow)
{
    if(isShow)
    {
        this->stopAllActions();
        this->setVisible(true);
        this->setPosition(_startPos);
        this->runAction(_visibleAction);
    }
    else
    {
        if(_type!= InviteEventData::Type::Send && _type!=InviteEventData::Type::Recv)
        {
            if(_progressBar->isVisible())
            {
                _progressBar->removeAction();
                _progressBar->setVisible(false);
            }
            stopSound();
        }
        this->runAction(_invisibleAction);
    }
}

cocos2d::Size& InviteDialog::getSize()
{
    return (cocos2d::Size&)this->getContentSize();
}

Color4B& InviteDialog::getBackgroundColor()
{
    return InviteDialog::BackgroundColor1;
}

void InviteDialog::setBackgroundColor(Color4B& color)
{
    _displayedColor.r = _realColor.r = color.r;
    _displayedColor.g = _realColor.g = color.g;
    _displayedColor.b = _realColor.b = color.b;
    _displayedOpacity = _realOpacity = color.a;
    
    updateColor();
}

void InviteDialog::update(float dt)
{
    CCLOG("update");
    addPicture(_contactInfo);
    this->unscheduleUpdate();
}

void InviteDialog::addPicture(ContactInfo *info)
{
    cocos2d::Image *image=nullptr;
    if(info->_image)
    {
        image =new cocos2d::Image();
        image->initWithImageData(info->_image, info->_size);
        if(image)
        {
            Texture2D* pTexture = new Texture2D();
            pTexture->initWithImage(image);
            _photo->setTexture(pTexture);
        }
    }
    else
    {
        _photo->setTexture(InviteDialog::DefaultPhotoImage);
    }
}

void InviteDialog::setContactInfo(ContactInfo* info)
{
    if(_contactInfo!=nullptr && !_contactInfo->_name.compare(info->_name))
        return;
    if(_contactInfo)
        delete _contactInfo;
    _contactInfo = info;
}

ContactInfo* InviteDialog::getContactInfo()
{
    return _contactInfo;
}
