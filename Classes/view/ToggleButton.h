//
//  ToggleButton.h
//  AnicameraMAC
//
//  Created by kerberos on 11/8/14.
//
//

#ifndef __AnicameraMAC__ToggleButton__
#define __AnicameraMAC__ToggleButton__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class ToggleButton : public Button
{
public:
    
    virtual ~ToggleButton();
    
    static ToggleButton* create();
    bool init();
    virtual void onPressStateChangedToNormal();
    virtual void onPressStateChangedToPressed();
    void setTitleColor(const Color3B& colorNormal, const Color3B& colorSelect);
    void onNormal();
    void onPressed();
    void toggle();
    bool isPressed();
    virtual void setEnabled(bool enabled);
private:
    bool mInitialized;
    bool mIsPressed;
    Color3B mColorNormal;
    Color3B mColorSelect;
};

#endif /* defined(__AnicameraMAC__ToggleButton__) */
