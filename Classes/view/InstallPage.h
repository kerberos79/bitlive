//
//  InstallPage.h
//  Vista
//
//  Created by kerberos on 1/23/15.
//
//

#ifndef __Vista__InstallPage__
#define __Vista__InstallPage__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Cocos2dxProfile.h"
#include "controller/CallStateHandler.h"
#include "extra/Strings.h"
#include "util/IdentityManager.h"
#include "view/MainScene.h"
#include "view/UntouchablePageView.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;
#ifdef COCOS2D_DEBUG
class InstallPage : public LayerColor, public cocos2d::ui::EditBoxDelegate
#else
class InstallPage : public LayerColor
#endif
{
public:
    static Color4B BackgroundColor;
    static Color3B FontColor;
    static int TitleFontSize;
    static int TextFontSize;
    static int GuideFontSize;
    static std::string PhoneLoginButtonNormal;
    static std::string GoogleLoginButtonNormal;
    static std::string PreviousButtonNormal;
    static std::string ForwardButtonNormal;
    static std::string SigninButtonNormal;
    static std::string SkipButtonNormal;
    static std::string LoadingNormal;
    static std::string CountryCodeBoxBackground;
    static Size CountryCodeBoxSize;
    static InstallPage* create(MainScene *mainscene);
    static void loadLayout(rapidjson::Document &doc);
    void pageviewCallback(Ref* pSender, PageView::EventType type);
    void menuPhoneNumberLoginback(Ref* pSender);
    void menuGoogleLoginback(Ref* pSender);
    void menuSigninCallback(Ref* pSender);
    void menuStartCallback(Ref* pSender);
    void menuDebugCallback(Ref* pSender);
    void menuSkipCallback(Ref* pSender);
    void agreement(std::string userid);
    void complete();
    
#ifdef COCOS2D_DEBUG
    virtual void editBoxEditingDidBegin(cocos2d::ui::EditBox* editBox);
    virtual void editBoxEditingDidEnd(cocos2d::ui::EditBox* editBox);
    virtual void editBoxTextChanged(cocos2d::ui::EditBox* editBox, const std::string& text);
    virtual void editBoxReturn(cocos2d::ui::EditBox* editBox);
#endif
private:
    virtual void update(float dt);
    bool initWithProfile(MainScene *mainscene);
    bool init(MainScene* mainscene);
    
    MainScene* mMainScene;
    UntouchablePageView* pageView;
    std::string labelID;
    std::string userID;
    std::string countryCode;
    std::string homepageUrl;
    
    Layout*     page1;
    Layout*     page2;
    Sprite*     loadingSprite;
    Button*     prevButton;
    Button*     nextButton;
    Button*     phoneNumberLoginButton;
    Button*     googleLoginButton;
    
    LabelTTF*      labelUserID;
    LabelTTF*      labelCountryCode;
    RichText*   agreementText;
    Button*     signinButton;
    Button*     startButton;
    cocos2d::ui::EditBox* countryCodeEditBox;

    RepeatForever*  loadingForever;
    
    float      _time;
    cocos2d::ui::EditBox* debugEditBox;
};

#endif /* defined(__Vista__InstallPage__) */
