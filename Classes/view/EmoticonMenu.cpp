//
//  EmoticonMenu.cpp
//  BitLive
//
//  Created by keros on 7/31/15.
//
//

#include "EmoticonMenu.h"

Color4B EmoticonMenu::BackgroundColor;
Color3B EmoticonMenu::FontColorNormal;
Color3B EmoticonMenu::FontColorSelect;
std::string EmoticonMenu::FontType;
int EmoticonMenu::FontSize;
int EmoticonMenu::Height;
int EmoticonMenu::ItemWidth;
int EmoticonMenu::ItemHeight;
std::string EmoticonMenu::CloseButton;
std::string EmoticonMenu::RoundButtonNormal;
std::string EmoticonMenu::RoundButtonSelect;
std::string EmoticonMenu::TabButtonNormal;
std::string EmoticonMenu::TabButtonSelect;

EmoticonMenu::~EmoticonMenu()
{
}

EmoticonMenu * EmoticonMenu::create(MainScene* mainscene)
{
    EmoticonMenu * menu = new (std::nothrow) EmoticonMenu();
    if( menu && menu->init(mainscene))
    {
        menu->setPosition(Vec2::ZERO);
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void EmoticonMenu::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& emoticonMenu = DICTOOL->getSubDictionary_json(doc, "emoticon_menu");
    EmoticonMenu::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(emoticonMenu, "background_color"));
    EmoticonMenu::FontColorNormal = HexString::ConvertColor3B(DICTOOL->getStringValue_json(emoticonMenu, "font_color_normal"));
    EmoticonMenu::FontColorSelect = HexString::ConvertColor3B(DICTOOL->getStringValue_json(emoticonMenu, "font_color_select"));
    EmoticonMenu::FontSize = DICTOOL->getIntValue_json(emoticonMenu, "font_size") * MainScene::LayoutScale;
    EmoticonMenu::Height = DICTOOL->getIntValue_json(emoticonMenu, "height") * MainScene::LayoutScale;
    EmoticonMenu::ItemWidth = DICTOOL->getIntValue_json(emoticonMenu, "item_width") * MainScene::LayoutScale;
    EmoticonMenu::ItemHeight = DICTOOL->getIntValue_json(emoticonMenu, "item_height") * MainScene::LayoutScale;
    EmoticonMenu::CloseButton = DICTOOL->getStringValue_json(emoticonMenu, "close_button");
    EmoticonMenu::RoundButtonNormal = DICTOOL->getStringValue_json(emoticonMenu, "round_button_normal");
    EmoticonMenu::RoundButtonSelect = DICTOOL->getStringValue_json(emoticonMenu, "round_button_select");
    EmoticonMenu::TabButtonNormal = DICTOOL->getStringValue_json(emoticonMenu, "tab_button_normal");
    EmoticonMenu::TabButtonSelect = DICTOOL->getStringValue_json(emoticonMenu, "tab_button_select");
}

bool EmoticonMenu::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    // Read content from file
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("emoticon_menu_item_list.json");
    
    rapidjson::Document doc;
    
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    
    rapidjson::Document labelDoc;
    std::string labelStr = FileUtils::getInstance()->getStringFromFile("menu.json");
    labelDoc.Parse<0>(labelStr.c_str());
    if (labelDoc.HasParseError())
    {
        CCLOG("GetParseError %s\n", labelDoc.GetParseError());
        return false;
    }
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    if(!initWithColor(EmoticonMenu::BackgroundColor, visibleSize.width, EmoticonMenu::Height))
        return false;
    
    _emoticonTabButton = ToggleButton::create();
    _emoticonTabButton->loadTextures(EmoticonMenu::TabButtonNormal, EmoticonMenu::TabButtonSelect, "");
    _emoticonTabButton->setPosition(Vec2(_emoticonTabButton->getContentSize().width * 0.5f, EmoticonMenu::Height - _emoticonTabButton->getContentSize().height*0.5f));
    _emoticonTabButton->setTitleText(DICTOOL->getStringValue_json(labelDoc, "emoticon"));
    _emoticonTabButton->setTitleColor(FontColorNormal, FontColorNormal);
    _emoticonTabButton->setTitleFontSize(FontSize);
    _emoticonTabButton->setTitleFontName(Strings::getFontType());
    _emoticonTabButton->setTouchEnabled(true);
    _emoticonTabButton->addClickEventListener(CC_CALLBACK_1(EmoticonMenu::menuEmoticonTabCallback,this));
    _emoticonTabButton->onPressed();
    _currentTabButton = _emoticonTabButton;
    
    _stickerTabButton = ToggleButton::create();
    _stickerTabButton->loadTextures(EmoticonMenu::TabButtonNormal, EmoticonMenu::TabButtonSelect, "");
    _stickerTabButton->setPosition(Vec2(_stickerTabButton->getContentSize().width * 1.5f, EmoticonMenu::Height - _stickerTabButton->getContentSize().height*0.5f));
    _stickerTabButton->setTitleText(DICTOOL->getStringValue_json(labelDoc, "sticker"));
    _stickerTabButton->setTitleColor(FontColorNormal, FontColorNormal);
    _stickerTabButton->setTitleFontSize(FontSize);
    _stickerTabButton->setTitleFontName(Strings::getFontType());
    _stickerTabButton->setTouchEnabled(true);
    _stickerTabButton->addClickEventListener(CC_CALLBACK_1(EmoticonMenu::menuStickerTabCallback,this));
    _stickerTabButton->onNormal();
    
    auto noStickerTabButton = Button::create();
    noStickerTabButton->loadTextures(EmoticonMenu::RoundButtonNormal, EmoticonMenu::RoundButtonSelect, "");
    noStickerTabButton->setPosition(Vec2(_stickerTabButton->getContentSize().width * 2.5f, EmoticonMenu::Height - noStickerTabButton->getContentSize().height*0.5f));
    noStickerTabButton->setTitleText(DICTOOL->getStringValue_json(labelDoc, "no_sticker"));
    noStickerTabButton->setTitleColor(FontColorNormal);
    noStickerTabButton->setTitleFontSize(FontSize);
    noStickerTabButton->setTitleFontName(Strings::getFontType());
    noStickerTabButton->setTouchEnabled(true);
    noStickerTabButton->addClickEventListener(CC_CALLBACK_1(EmoticonMenu::menuNoStickerTabCallback,this));
    
    auto closeButton = Button::create();
    closeButton->setAnchorPoint(Vec2(0, 0));
    closeButton->loadTextures(EmoticonMenu::CloseButton, "", "");
    closeButton->setPosition(Vec2(visibleSize.width - closeButton->getContentSize().width, EmoticonMenu::Height - closeButton->getContentSize().height));
    closeButton->setTouchEnabled(true);
    closeButton->addClickEventListener(CC_CALLBACK_1(EmoticonMenu::menuCloseCallback,this));
    
    float scrollViewHeight = EmoticonMenu::Height - _emoticonTabButton->getContentSize().height;
    int listCount = DICTOOL->getArrayCount_json(doc, "list");
    float scrollViewInnerHeight = EmoticonMenu::ItemHeight *  ((listCount-1)/5 + 1);
    
    _emoticonScrollView = ui::ScrollView::create();
    _emoticonScrollView->setContentSize(Size(visibleSize.width, scrollViewHeight));
    _emoticonScrollView->setBackGroundColorType(ui::ScrollView::BackGroundColorType::SOLID);
    _emoticonScrollView->setBackGroundColor(Color3B::WHITE);
    _emoticonScrollView->setPosition(Vec2::ZERO);
    _emoticonScrollView->setDirection(ListView::Direction::VERTICAL);
    _emoticonScrollView->setBounceEnabled(false);
    Button* button;
    int tag;
    const char* icon;
    float posX, posY;
    posY = -0.5f;
    for(int i=0;i<listCount; i++)
    {
        if(i%5==0)
        {
            posX = 0.1f;
            posY += 1.0f;
        }
        else
        {
            posX += 0.2f;
        }
        const rapidjson::Value& item = DICTOOL->getDictionaryFromArray_json(doc, "list", i);
        tag = DICTOOL->getIntValue_json(item, "tag");
        icon = DICTOOL->getStringValue_json(item, "icon");
        
        button = Button::create();
        button->setTag(tag);
        button->setScale(MainScene::LayoutScale);
        button->loadTextures(icon, "", "");
        button->setPosition(Vec2(visibleSize.width* posX,
                                 scrollViewInnerHeight - EmoticonMenu::ItemHeight * posY));
        button->setTouchEnabled(true);
        button->addClickEventListener(CC_CALLBACK_1(MainScene::menuSetEmoticonCallback,mMainScene));
        button->setPressedActionEnabled(false);
        _emoticonScrollView->addChild(button);
    }
    _emoticonScrollView->setInnerContainerSize(Size(visibleSize.width, scrollViewInnerHeight));
    
    if(!initWithColor(EmoticonMenu::BackgroundColor, visibleSize.width, EmoticonMenu::Height))
        return false;
    this->addChild(_emoticonScrollView, 0);
    this->addChild(_emoticonTabButton, 0);
    this->addChild(_stickerTabButton, 0);
    this->addChild(noStickerTabButton, 0);
    this->addChild(closeButton, 1);
    _startPos = Vec2(0, -EmoticonMenu::Height);
    _endPos = Vec2::ZERO;
    _visibleAction = EaseExponentialOut::create(MoveTo::create(0.4f, _endPos));
    _visibleAction->retain();
    
    _invisibleAction = Sequence::create(EaseExponentialOut::create(MoveTo::create(0.4f, _startPos)),
                                        CallFuncN::create( CC_CALLBACK_1(EmoticonMenu::afterInvisibleAction, this)),
                                        nullptr);
    _invisibleAction->retain();
    _stickerScrollView = nullptr;
    _selectedStickerButton = nullptr;
    return true;
}

void EmoticonMenu::showStickerMenu()
{
    if(_stickerScrollView==nullptr)
    {
        // Read content from file
        std::string contentStr = FileUtils::getInstance()->getStringFromFile("sticker_menu_item_list.json");
        
        rapidjson::Document doc;
        
        doc.Parse<0>(contentStr.c_str());
        if (doc.HasParseError())
        {
            CCLOG("GetParseError %s\n", doc.GetParseError());
            return;
        }
        Size visibleSize = Director::getInstance()->getVisibleSize();
        int listCount = DICTOOL->getArrayCount_json(doc, "list");
        float scrollViewPos = _emoticonScrollView->getPosition().y;
        float scrollViewHeight = _emoticonScrollView->getContentSize().height;
        float scrollViewInnerHeight = EmoticonMenu::ItemHeight *  ((listCount-1)/5 + 1);
        
        _stickerScrollView = ui::ScrollView::create();
        _stickerScrollView->setContentSize(Size(visibleSize.width, scrollViewHeight));
        _stickerScrollView->setBackGroundColorType(ui::ScrollView::BackGroundColorType::SOLID);
        _stickerScrollView->setBackGroundColor(Color3B::WHITE);
        _stickerScrollView->setPosition(Vec2::ZERO);
        _stickerScrollView->setDirection(ListView::Direction::VERTICAL);
        _stickerScrollView->setBounceEnabled(false);
        Button* button;
        int tag;
        const char* icon;
        float posX, posY;
        posY = -0.5f;
        for(int i=0, j=0;i<listCount; i++)
        {
            if(j==0)
            {
                posX = 0.1f;
                posY += 1.0f;
            }
            else
            {
                posX += 0.2f;
            }
            const rapidjson::Value& item = DICTOOL->getDictionaryFromArray_json(doc, "list", i);
            tag = DICTOOL->getIntValue_json(item, "tag");
            icon = DICTOOL->getStringValue_json(item, "icon");
            button = Button::create();
            button->setTag(tag);
            button->setScale(MainScene::LayoutScale);
            button->loadTextures(icon, "", "");
            button->setPosition(Vec2(visibleSize.width* posX,
                                     scrollViewInnerHeight - EmoticonMenu::ItemHeight * posY));
            button->setTouchEnabled(true);
            button->addClickEventListener(CC_CALLBACK_1(EmoticonMenu::menuSetStickerCallback,this));
            button->setPressedActionEnabled(false);
            _stickerScrollView->addChild(button);
            if(j==4)
                j=0;
            else
                j++;
        }
        _stickerScrollView->setInnerContainerSize(Size(visibleSize.width, scrollViewInnerHeight));
        
        this->addChild(_stickerScrollView, 0);
    }
    _stickerScrollView->setVisible(true);
}

void EmoticonMenu::menuEmoticonTabCallback(Ref* pSender)
{
    if(_currentTabButton==_emoticonTabButton)
        return;
    _currentTabButton->toggle();
    _currentTabButton = _emoticonTabButton;
    _currentTabButton->toggle();
    _emoticonScrollView->setVisible(true);
    if(_stickerScrollView)
        _stickerScrollView->setVisible(false);
}

void EmoticonMenu::menuStickerTabCallback(Ref* pSender)
{
    if(_currentTabButton==_stickerTabButton)
        return;
    _currentTabButton->toggle();
    _currentTabButton = _stickerTabButton;
    _currentTabButton->toggle();
    _emoticonScrollView->setVisible(false);
    showStickerMenu();
}

void EmoticonMenu::menuNoStickerTabCallback(Ref* pSender)
{
    if(_selectedStickerButton!=nullptr)
        mMainScene->menuSetStickerCallback(_selectedStickerButton);
}

void EmoticonMenu::menuSetStickerCallback(Ref* pSender)
{
    _selectedStickerButton = (Button*)pSender;
    mMainScene->menuSetStickerCallback(pSender);
}

void EmoticonMenu::menuCloseCallback(Ref* pSender)
{
    mMainScene->toggleMenuCallback(this);
}

void EmoticonMenu::show(bool isShow)
{
    if(isShow)
    {
        this->stopAllActions();
        this->setVisible(true);
        this->setPosition(_startPos);
        this->runAction(_visibleAction);
    }
    else
    {
        this->runAction(_invisibleAction);
    }
}

void EmoticonMenu::afterInvisibleAction(Node* sender)
{
    this->setVisible(false);
}

cocos2d::Size& EmoticonMenu::getSize()
{
    return (cocos2d::Size&)this->getContentSize();
}

Color4B& EmoticonMenu::getBackgroundColor()
{
    return EmoticonMenu::BackgroundColor;
}

void EmoticonMenu::setBackgroundColor(Color4B& color)
{
    _displayedColor.r = _realColor.r = color.r;
    _displayedColor.g = _realColor.g = color.g;
    _displayedColor.b = _realColor.b = color.b;
    _displayedOpacity = _realOpacity = color.a;
    
    updateColor();
}
