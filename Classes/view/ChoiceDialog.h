//
//  ChoiceDialog.h
//  Vista
//
//  Created by kerberos on 1/22/15.
//
//

#ifndef __Vista__ChoiceDialog__
#define __Vista__ChoiceDialog__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Cocos2dxProfile.h"
#include "controller/CallStateHandler.h"
#include "extra/Strings.h"
#include "util/IdentityManager.h"
#include "view/MainScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;
class ChoiceDialog : public LayerColor
{
public:
    enum class Type
    {
        POSITIVE,
        NEGATIVE,
        BOTH
    };
    ~ChoiceDialog();
    static Color4B BackgroundColor;
    static Color3B FontColor;
    static int FontSize;
    static int ItemHeight;
    static std::string OkButtonNormal;
    static std::string OkButtonSelect;
    static ChoiceDialog* create(MainScene *mainscene, std::vector<std::string>* list, std::function<void(Ref*)> callback);
    static void loadLayout(rapidjson::Document &doc);
    void show(bool isShow);
    std::string& getUsername()
    {
        return _username;
    }
    
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* unused_event);
private:
    bool init(MainScene *mainscene, std::vector<std::string>* list, std::function<void(Ref*)> callback);
    MainScene* mMainScene;
    ListView* listView;
    EventListenerTouchOneByOne *_CustomListener;
    std::string _username;
};
#endif /* defined(__Vista__ChoiceDialog__) */
