//
//  MainMenu.cpp
//  BitLive
//
//  Created by keros on 6/11/15.
//
//

#include "MainMenu.h"
#include "CCDomainFactory.h"
#include <iostream>


Color4B MainMenu::BackgroundColor1;
Color4B MainMenu::BackgroundColor2;
Color3B MainMenu::FontColor;
int MainMenu::FontSize = 0;
int MainMenu::Height = 0;
int MainMenu::SubMenuHeight = 0;
int MainMenu::RecTimeout = 0;
std::string MainMenu::ContactButtonNormal;
std::string MainMenu::WebButtonNormal;
std::string MainMenu::EffectButtonNormal;
std::string MainMenu::EmoticonButtonNormal;
std::string MainMenu::SettingButtonNormal;
std::string MainMenu::FlipButtonNormal;
std::string MainMenu::ExitButtonNormal;
std::string MainMenu::ExitButtonDisable;
std::string MainMenu::HangupButtonNormal;
std::string MainMenu::CallButtonNormal;
std::string MainMenu::CallButtonSelect;
std::string MainMenu::HomeButtonNormal;
std::string MainMenu::HomeButtonSelect;
std::string MainMenu::HomeButtonDisable;
std::string MainMenu::FormatButtonNormal;
std::string MainMenu::FormatButtonSelect;
std::string MainMenu::RecordButtonNormal;
std::string MainMenu::RecordButtonSelect;
std::string MainMenu::RecordButtonDisable;
std::string MainMenu::CaptureButtonNormal;
std::string MainMenu::CaptureButtonDisable;

MainMenu::~MainMenu()
{
}

MainMenu * MainMenu::create(MainScene* mainscene)
{
    MainMenu * menu = new (std::nothrow) MainMenu();
    if( menu && menu->init(mainscene))
    {
        menu->setPosition(Vec2::ZERO);
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void MainMenu::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& mainMenu = DICTOOL->getSubDictionary_json(doc, "main_menu");
    MainMenu::BackgroundColor1 = HexString::ConvertColor4B(DICTOOL->getStringValue_json(mainMenu, "background_color1"));
    MainMenu::BackgroundColor2 = HexString::ConvertColor4B(DICTOOL->getStringValue_json(mainMenu, "background_color2"));
    MainMenu::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(mainMenu, "font_color"));
    MainMenu::FontSize = DICTOOL->getIntValue_json(mainMenu, "font_size") * MainScene::LayoutScale;
    MainMenu::Height = DICTOOL->getIntValue_json(mainMenu, "height") * MainScene::LayoutScale;
    MainMenu::SubMenuHeight = DICTOOL->getIntValue_json(mainMenu, "submenu_height") * MainScene::LayoutScale;
    MainMenu::ContactButtonNormal = DICTOOL->getStringValue_json(mainMenu, "contact_button_normal");
    MainMenu::WebButtonNormal = DICTOOL->getStringValue_json(mainMenu, "web_button_normal");
    MainMenu::EffectButtonNormal = DICTOOL->getStringValue_json(mainMenu, "effect_button_normal");
    MainMenu::EmoticonButtonNormal = DICTOOL->getStringValue_json(mainMenu, "emoticon_button_normal");
    MainMenu::SettingButtonNormal = DICTOOL->getStringValue_json(mainMenu, "setting_button_normal");
    MainMenu::FlipButtonNormal = DICTOOL->getStringValue_json(mainMenu, "flip_button_normal");
    MainMenu::ExitButtonNormal = DICTOOL->getStringValue_json(mainMenu, "exit_button_normal");
    MainMenu::ExitButtonDisable = DICTOOL->getStringValue_json(mainMenu, "exit_button_disable");
    MainMenu::HangupButtonNormal = DICTOOL->getStringValue_json(mainMenu, "hangup_button_normal");
    MainMenu::CallButtonNormal = DICTOOL->getStringValue_json(mainMenu, "call_button_normal");
    MainMenu::CallButtonSelect = DICTOOL->getStringValue_json(mainMenu, "call_button_select");
    MainMenu::HomeButtonNormal = DICTOOL->getStringValue_json(mainMenu, "home_button_normal");
    MainMenu::HomeButtonSelect = DICTOOL->getStringValue_json(mainMenu, "home_button_select");
    MainMenu::HomeButtonDisable = DICTOOL->getStringValue_json(mainMenu, "home_button_disable");
    MainMenu::FormatButtonNormal = DICTOOL->getStringValue_json(mainMenu, "format_button_normal");
    MainMenu::FormatButtonSelect = DICTOOL->getStringValue_json(mainMenu, "format_button_select");
    MainMenu::RecordButtonNormal = DICTOOL->getStringValue_json(mainMenu, "record_button_normal");
    MainMenu::RecordButtonSelect = DICTOOL->getStringValue_json(mainMenu, "record_button_select");
    MainMenu::RecordButtonDisable = DICTOOL->getStringValue_json(mainMenu, "record_button_disable");
    MainMenu::CaptureButtonNormal = DICTOOL->getStringValue_json(mainMenu, "capture_button_normal");
    MainMenu::CaptureButtonDisable = DICTOOL->getStringValue_json(mainMenu, "capture_button_disable");
    
    MainMenu::RecTimeout = UserDefault::getInstance()->getIntegerForKey("record_timeout", 15);
}

bool MainMenu::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    if(MainMenu::Height <= MainScene::SubMenuHeight)
    {
        createLayoutWithLabel();
    }
    else if((MainMenu::Height*0.7f) > MainScene::SubMenuHeight)
    {
        MainMenu::Height = MainMenu::Height * 0.7f;
        createLayout();
    }
    else
    {
        MainMenu::Height = MainScene::SubMenuHeight;
        createLayout();
    }
    float posSubMenu = MainMenu::Height * 0.8f;
    // rotate button
    auto flipButton = Button::create();
    flipButton->loadTextures(FlipButtonNormal, "", "");
    flipButton->setPosition(Vec2(visibleSize.width*0.18f, posSubMenu));
    flipButton->setTouchEnabled(true);
    flipButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuRotateCallback,mMainScene));
    
    // setting button
    mCaptureButton = ToggleButton::create();
    mCaptureButton->loadTextures(MainMenu::CaptureButtonNormal, MainMenu::CaptureButtonNormal, MainMenu::CaptureButtonDisable);
    mCaptureButton->setPosition(Vec2(visibleSize.width*0.34f, posSubMenu));
    mCaptureButton->setTouchEnabled(true);
    mCaptureButton->addClickEventListener(CC_CALLBACK_1(MainMenu::menuCaptureCallback,this));
    
    mHomeButton = ToggleButton::create();
    mHomeButton->loadTextures(MainMenu::HomeButtonNormal, MainMenu::HomeButtonSelect, MainMenu::HomeButtonDisable);
    mHomeButton->setPosition(Vec2(visibleSize.width*0.5f, posSubMenu));
    mHomeButton->setTouchEnabled(true);
    mHomeButton->addClickEventListener(CC_CALLBACK_1(MainMenu::menuHomeCallback,this));
    
    mRecordButton = ToggleButton::create();
    mRecordButton->loadTextures(MainMenu::RecordButtonNormal, MainMenu::RecordButtonSelect, MainMenu::RecordButtonDisable);
    mRecordButton->setPosition(Vec2(visibleSize.width*0.66f, posSubMenu));
    mRecordButton->setTouchEnabled(true);
    mRecordButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuRecordCallback,mMainScene));
    
    mExitButton = Button::create();
    mExitButton->loadTextures(ExitButtonNormal, "", ExitButtonDisable);
    mExitButton->setPosition(Vec2(visibleSize.width*0.82f, posSubMenu));
    mExitButton->setTouchEnabled(true);
    mExitButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuExitCallback,mMainScene));
    
    // end
    if(!initWithColor(MainMenu::BackgroundColor1, MainMenu::BackgroundColor2))
        return false;
    this->setContentSize(Size(visibleSize.width, MainMenu::Height));
    
    this->addChild(flipButton);
    this->addChild(mHomeButton);
    this->addChild(mExitButton);
    this->addChild(mRecordButton);
    this->addChild(mCaptureButton);
    
    this->addChild(mMainLayout);
    this->addChild(mCallLayout);
    this->addChild(mRecordLayout);
    
    _isHiding = false;
    _startPos = Vec2(0, -MainMenu::Height);
    _endPos = Vec2::ZERO;
    _visibleAction = Sequence::create(EaseExponentialOut::create(MoveTo::create(0.4f, _endPos)),
                                        CallFuncN::create( CC_CALLBACK_1(MainMenu::afterVisibleAction, this)),
                                        nullptr);
    _visibleAction->retain();
    
    _invisibleAction = Sequence::create(EaseExponentialOut::create(MoveTo::create(0.4f, _startPos)),
                                        CallFuncN::create( CC_CALLBACK_1(MainMenu::afterInvisibleAction, this)),
                                        nullptr);
    _invisibleAction->retain();
    
    _visibleHomeAction = EaseExponentialOut::create(MoveTo::create(0.8f, Vec2::ZERO));
    _visibleHomeAction->retain();
    
    _invisibleHomeAction = EaseExponentialOut::create(MoveTo::create(0.8f, Vec2(-visibleSize.width, 0.0)));
    _invisibleHomeAction->retain();
    
    _visibleRecordAction = EaseExponentialOut::create(MoveTo::create(0.8f, Vec2::ZERO));
    _visibleRecordAction->retain();
    
    _invisibleRecordAction = EaseExponentialOut::create(MoveTo::create(0.8f, Vec2(visibleSize.width, 0.0)));
    _invisibleRecordAction->retain();
    _isRecording = false;
    return true;
}

void MainMenu::createLayout()
{
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("menu.json");
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return;
    }
    Size visibleSize = Director::getInstance()->getVisibleSize();
    float bottomMenuHeight = MainMenu::Height - MainMenu::SubMenuHeight;
    float posBottomMenuButton = bottomMenuHeight * 0.6f;
    float posBottomMenuLabel = bottomMenuHeight * 0.2f;
    float leftMargin = visibleSize.width * 0.05f;
    float layoutwidth = visibleSize.width * 0.9f;
    {
        // contact button
        auto contactButton = Button::create();
        contactButton->loadTextures("btn_contact_normal_2.png", "", "");
        contactButton->setPosition(Vec2(leftMargin + layoutwidth *1.0f/8.0f, posBottomMenuButton));
        contactButton->setTouchEnabled(true);
        contactButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuContractCallback,mMainScene));;;;;
        
        auto contactLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "contact"), Strings::getFontType(), FontSize*0.9);
        contactLabel->setColor(FontColor);
        contactLabel->setPosition(Vec2(contactButton->getPosition().x, posBottomMenuLabel));
        
        // effect button
        auto effectButton = Button::create();
        effectButton->loadTextures("btn_effect_normal_2.png", "", "");
        effectButton->setPosition(Vec2(leftMargin + layoutwidth *3.0f/8.0f, posBottomMenuButton));
        effectButton->setTouchEnabled(true);
        effectButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEffectCallback,mMainScene));
        auto effectLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "effect"), Strings::getFontType(), FontSize*0.9);
        effectLabel->setColor(FontColor);
        effectLabel->setPosition(Vec2(effectButton->getPosition().x, posBottomMenuLabel));
        
        auto emoticonButton = Button::create();
        emoticonButton->loadTextures("btn_emoticon_normal_2.png", "", "");
        emoticonButton->setPosition(Vec2(leftMargin + layoutwidth *5.0f/8.0f, posBottomMenuButton));
        emoticonButton->setTouchEnabled(true);
        emoticonButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEmoticonCallback,mMainScene));
        
        auto emoticonLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "emoticon"), Strings::getFontType(), FontSize*0.9);
        emoticonLabel->setColor(FontColor);
        emoticonLabel->setPosition(Vec2(emoticonButton->getPosition().x, posBottomMenuLabel));
        
        // exit button
        auto settingButton = Button::create();
        settingButton->loadTextures("btn_setting_normal_2.png", "", "");
        settingButton->setPosition(Vec2(leftMargin + layoutwidth*7.0f/8.0f, posBottomMenuButton));
        settingButton->setTouchEnabled(true);
        settingButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuSettingCallback,mMainScene));
        
        auto settingLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "setting"), Strings::getFontType(), FontSize*0.9);
        settingLabel->setColor(FontColor);
        settingLabel->setPosition(Vec2(settingButton->getPosition().x, posBottomMenuLabel));
        
        mMainLayout = Layout::create();
        mMainLayout->retain();
        mMainLayout->setContentSize(Size(visibleSize.width, bottomMenuHeight));
        mMainLayout->setPosition(Vec2::ZERO);
        
        mMainLayout->addChild(contactLabel, 1);
        mMainLayout->addChild(effectLabel, 1);
        mMainLayout->addChild(emoticonLabel, 1);
        mMainLayout->addChild(settingLabel, 1);
        
        mMainLayout->addChild(contactButton, 1);
        mMainLayout->addChild(emoticonButton, 1);
        mMainLayout->addChild(effectButton, 1);
        mMainLayout->addChild(settingButton, 1);
    }
    {
        auto webButton = Button::create();
        webButton->loadTextures("btn_web_normal_2.png", "", "");
        webButton->setPosition(Vec2(leftMargin + layoutwidth *1.0f/8.0f, posBottomMenuButton));
        webButton->setTouchEnabled(true);
        webButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuWebCallback,mMainScene));
        auto webLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "web"), Strings::getFontType(), FontSize);
        webLabel->setColor(FontColor);
        webLabel->setPosition(Vec2(webButton->getPosition().x, posBottomMenuLabel));
        
        // effect button
        auto effectButton = Button::create();
        effectButton->loadTextures("btn_effect_normal_2.png", "", "");
        effectButton->setPosition(Vec2(leftMargin + layoutwidth *3.0f/8.0f, posBottomMenuButton));
        effectButton->setTouchEnabled(true);
        effectButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEffectCallback,mMainScene));
        
        auto effectLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "effect"), Strings::getFontType(), FontSize*0.9);
        effectLabel->setColor(FontColor);
        effectLabel->setPosition(Vec2(effectButton->getPosition().x, posBottomMenuLabel));
        
        auto emoticonButton = Button::create();
        emoticonButton->loadTextures("btn_emoticon_normal_2.png", "", "");
        emoticonButton->setPosition(Vec2(leftMargin + layoutwidth *5.0f/8.0f, posBottomMenuButton));
        emoticonButton->setTouchEnabled(true);
        emoticonButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEmoticonCallback,mMainScene));
        
        auto emoticonLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "emoticon"), Strings::getFontType(), FontSize*0.9);
        emoticonLabel->setColor(FontColor);
        emoticonLabel->setPosition(Vec2(emoticonButton->getPosition().x, posBottomMenuLabel));
        
        auto hangupButton = Button::create();
        hangupButton->loadTextures("btn_hangup_normal_2.png", "", "");
        hangupButton->setPosition(Vec2(leftMargin + layoutwidth *7.0f/8.0f, posBottomMenuButton));
        hangupButton->setTouchEnabled(true);
        hangupButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuHangupCallback,mMainScene));
        auto hangupLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "hangup"), Strings::getFontType(), FontSize);
        hangupLabel->setColor(FontColor);
        hangupLabel->setPosition(Vec2(hangupButton->getPosition().x, posBottomMenuLabel));
        
        mCallLayout = Layout::create();
        mCallLayout->retain();
        mCallLayout->setContentSize(Size(visibleSize.width, bottomMenuHeight));
        mCallLayout->setPosition(Vec2::ZERO);
        
        mCallLayout->addChild(webLabel, 1);
        mCallLayout->addChild(effectLabel, 1);
        mCallLayout->addChild(emoticonLabel, 1);
        mCallLayout->addChild(hangupLabel, 1);
        
        mCallLayout->addChild(webButton, 1);
        mCallLayout->addChild(effectButton, 1);
        mCallLayout->addChild(emoticonButton, 1);
        mCallLayout->addChild(hangupButton, 1);
    }
    {
        mFormatButton = ToggleButton::create();
        mFormatButton->loadTextures("btn_format_normal_2.png", "btn_format_select_2.png", "");
        mFormatButton->setPosition(Vec2(leftMargin + layoutwidth *1.0f/8.0f, posBottomMenuButton));
        mFormatButton->setTouchEnabled(true);
        mFormatButton->addClickEventListener(CC_CALLBACK_1(MainMenu::menuFormatCallback, this));
        
        int currentFormat = UserDefault::getInstance()->getIntegerForKey("record_type", (int)UIMessage::Record::MP4);
        if(currentFormat == (int)UIMessage::Record::MP4)
            mFormatButton->onNormal();
        else
            mFormatButton->onPressed();
        auto formatLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "format"), Strings::getFontType(), FontSize*0.9);
        formatLabel->setColor(FontColor);
        formatLabel->setPosition(Vec2(mFormatButton->getPosition().x, posBottomMenuLabel));
        
        auto effectButton = Button::create();
        effectButton->loadTextures("btn_effect_normal_2.png", "", "");
        effectButton->setPosition(Vec2(leftMargin + layoutwidth *3.0f/8.0f, posBottomMenuButton));
        effectButton->setTouchEnabled(true);
        effectButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEffectCallback,mMainScene));
        auto effectLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "effect"), Strings::getFontType(), FontSize*0.9);
        effectLabel->setColor(FontColor);
        effectLabel->setPosition(Vec2(effectButton->getPosition().x, posBottomMenuLabel));
        
        auto emoticonButton = Button::create();
        emoticonButton->loadTextures("btn_emoticon_normal_2.png", "", "");
        emoticonButton->setPosition(Vec2(leftMargin + layoutwidth *5.0f/8.0f, posBottomMenuButton));
        emoticonButton->setTouchEnabled(true);
        emoticonButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEmoticonCallback,mMainScene));
        
        auto emoticonLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "emoticon"), Strings::getFontType(), FontSize*0.9);
        emoticonLabel->setColor(FontColor);
        emoticonLabel->setPosition(Vec2(emoticonButton->getPosition().x, posBottomMenuLabel));
        
        mRecordSettingButton = Button::create();
        mRecordSettingButton->loadTextures("btn_setting_normal_2.png", "", "");
        mRecordSettingButton->setPosition(Vec2(leftMargin + layoutwidth *7.0f/8.0f, posBottomMenuButton));
        mRecordSettingButton->setTouchEnabled(true);
        mRecordSettingButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuRecordSettingCallback,mMainScene));
        
        auto settingLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "setting"), Strings::getFontType(), FontSize*0.9);
        settingLabel->setColor(FontColor);
        settingLabel->setPosition(Vec2(mRecordSettingButton->getPosition().x, posBottomMenuLabel));
        
        mRecordLayout = Layout::create();
        mRecordLayout->retain();
        mRecordLayout->setContentSize(Size(visibleSize.width, bottomMenuHeight));
        mRecordLayout->setPosition(Vec2(visibleSize.width, 0.0));
        
        mRecordLayout->addChild(formatLabel, 1);
        mRecordLayout->addChild(effectLabel, 1);
        mRecordLayout->addChild(emoticonLabel, 1);
        mRecordLayout->addChild(settingLabel, 1);
        
        mRecordLayout->addChild(mFormatButton);
        mRecordLayout->addChild(effectButton);
        mRecordLayout->addChild(emoticonButton);
        mRecordLayout->addChild(mRecordSettingButton);
    }
}

void MainMenu::createLayoutWithLabel()
{
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("menu.json");
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return;
    }
    Size visibleSize = Director::getInstance()->getVisibleSize();
    float bottomMenuHeight = MainMenu::Height - MainMenu::SubMenuHeight;
    float posBottomMenuButton = bottomMenuHeight * 0.6f;
    float posBottomMenuLabel = bottomMenuHeight * 0.25f;
    float leftMargin = visibleSize.width * 0.05f;
    float layoutwidth = visibleSize.width * 0.9f;
    {
        // contact button
        auto contactButton = Button::create();
        contactButton->loadTextures(ContactButtonNormal, "", "");
        contactButton->setPosition(Vec2(leftMargin + layoutwidth *1.0f/8.0f, posBottomMenuButton));
        contactButton->setTouchEnabled(true);
        contactButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuContractCallback,mMainScene));
        
        auto contactLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "contact"), Strings::getFontType(), FontSize);
        contactLabel->setColor(FontColor);
        contactLabel->setPosition(Vec2(contactButton->getPosition().x, posBottomMenuLabel));
        
        // effect button
        auto effectButton = Button::create();
        effectButton->loadTextures(EffectButtonNormal, "", "");
        effectButton->setPosition(Vec2(leftMargin + layoutwidth *3.0f/8.0f, posBottomMenuButton));
        effectButton->setTouchEnabled(true);
        effectButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEffectCallback,mMainScene));
        auto effectLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "effect"), Strings::getFontType(), FontSize);
        effectLabel->setColor(FontColor);
        effectLabel->setPosition(Vec2(effectButton->getPosition().x, posBottomMenuLabel));
        
        auto emoticonButton = Button::create();
        emoticonButton->loadTextures(EmoticonButtonNormal, "", "");
        emoticonButton->setPosition(Vec2(leftMargin + layoutwidth *5.0f/8.0f, posBottomMenuButton));
        emoticonButton->setTouchEnabled(true);
        emoticonButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEmoticonCallback,mMainScene));
        
        auto emoticonLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "emoticon"), Strings::getFontType(), FontSize);
        emoticonLabel->setColor(FontColor);
        emoticonLabel->setPosition(Vec2(emoticonButton->getPosition().x, posBottomMenuLabel));
        
        // exit button
        auto settingButton = Button::create();
        settingButton->loadTextures(SettingButtonNormal, "", "");
        settingButton->setPosition(Vec2(leftMargin + layoutwidth*7.0f/8.0f, posBottomMenuButton));
        settingButton->setTouchEnabled(true);
        settingButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuSettingCallback,mMainScene));
        auto settingLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "setting"), Strings::getFontType(), FontSize);
        settingLabel->setColor(FontColor);
        settingLabel->setPosition(Vec2(settingButton->getPosition().x, posBottomMenuLabel));
        mMainLayout = Layout::create();
        mMainLayout->retain();
        mMainLayout->setContentSize(Size(visibleSize.width, bottomMenuHeight));
        mMainLayout->setPosition(Vec2::ZERO);
        
        mMainLayout->addChild(contactLabel, 1);
        mMainLayout->addChild(effectLabel, 1);
        mMainLayout->addChild(emoticonLabel, 1);
        mMainLayout->addChild(settingLabel, 1);
        
        mMainLayout->addChild(contactButton, 1);
        mMainLayout->addChild(emoticonButton, 1);
        mMainLayout->addChild(effectButton, 1);
        mMainLayout->addChild(settingButton, 1);
    }
    {
        auto webButton = Button::create();
        webButton->loadTextures(WebButtonNormal, "", "");
        webButton->setPosition(Vec2(leftMargin + layoutwidth *1.0f/8.0f, posBottomMenuButton));
        webButton->setTouchEnabled(true);
        webButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuWebCallback,mMainScene));
        
        auto webLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "web"), Strings::getFontType(), FontSize);
        webLabel->setColor(FontColor);
        webLabel->setPosition(Vec2(leftMargin + layoutwidth*1.0f/8.0f, posBottomMenuLabel));
        
        // effect button
        auto effectButton = Button::create();
        effectButton->loadTextures(EffectButtonNormal, "", "");
        effectButton->setPosition(Vec2(leftMargin + layoutwidth *3.0f/8.0f, posBottomMenuButton));
        effectButton->setTouchEnabled(true);
        effectButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEffectCallback,mMainScene));
        auto effectLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "effect"), Strings::getFontType(), FontSize);
        effectLabel->setColor(FontColor);
        effectLabel->setPosition(Vec2(leftMargin + layoutwidth*3.0f/8.0f, posBottomMenuLabel));
        
        auto emoticonButton = Button::create();
        emoticonButton->loadTextures(EmoticonButtonNormal, "", "");
        emoticonButton->setPosition(Vec2(leftMargin + layoutwidth *5.0f/8.0f, posBottomMenuButton));
        emoticonButton->setTouchEnabled(true);
        emoticonButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEmoticonCallback,mMainScene));
        
        auto emoticonLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "emoticon"), Strings::getFontType(), FontSize);
        emoticonLabel->setColor(FontColor);
        emoticonLabel->setPosition(Vec2(leftMargin + layoutwidth*5.0f/8.0f, posBottomMenuLabel));
        
        auto hangupButton = Button::create();
        hangupButton->loadTextures(HangupButtonNormal, "", "");
        hangupButton->setPosition(Vec2(leftMargin + layoutwidth *7.0f/8.0f, posBottomMenuButton));
        hangupButton->setTouchEnabled(true);
        hangupButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuHangupCallback,mMainScene));
        
        
        auto hangupLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "hangup"), Strings::getFontType(), FontSize);
        hangupLabel->setColor(FontColor);
        hangupLabel->setPosition(Vec2(leftMargin + layoutwidth*7.0f/8.0f, posBottomMenuLabel));
        
        mCallLayout = Layout::create();
        mCallLayout->retain();
        mCallLayout->setContentSize(Size(visibleSize.width, bottomMenuHeight));
        mCallLayout->setPosition(Vec2::ZERO);
        mCallLayout->addChild(webLabel, 1);
        mCallLayout->addChild(effectLabel, 1);
        mCallLayout->addChild(emoticonLabel, 1);
        mCallLayout->addChild(hangupLabel, 1);
        mCallLayout->addChild(webButton, 1);
        mCallLayout->addChild(effectButton, 1);
        mCallLayout->addChild(emoticonButton, 1);
        mCallLayout->addChild(hangupButton, 1);
    }
    {
        mFormatButton = ToggleButton::create();
        mFormatButton->loadTextures(FormatButtonNormal, FormatButtonSelect, "");
        mFormatButton->setPosition(Vec2(leftMargin + layoutwidth *1.0f/8.0f, posBottomMenuButton));
        mFormatButton->setTouchEnabled(true);
        mFormatButton->addClickEventListener(CC_CALLBACK_1(MainMenu::menuFormatCallback, this));
        int currentFormat = UserDefault::getInstance()->getIntegerForKey("record_type", (int)UIMessage::Record::MP4);
        if(currentFormat == (int)UIMessage::Record::MP4)
            mFormatButton->onNormal();
        else
            mFormatButton->onPressed();
        auto formatLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "format"), Strings::getFontType(), FontSize);
        formatLabel->setColor(FontColor);
        formatLabel->setPosition(Vec2(mFormatButton->getPosition().x, posBottomMenuLabel));
        
        auto effectButton = Button::create();
        effectButton->loadTextures(EffectButtonNormal, "", "");
        effectButton->setPosition(Vec2(leftMargin + layoutwidth *3.0f/8.0f, posBottomMenuButton));
        effectButton->setTouchEnabled(true);
        effectButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEffectCallback,mMainScene));
        auto effectLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "effect"), Strings::getFontType(), FontSize);
        effectLabel->setColor(FontColor);
        effectLabel->setPosition(Vec2(effectButton->getPosition().x, posBottomMenuLabel));
        
        auto emoticonButton = Button::create();
        emoticonButton->loadTextures(EmoticonButtonNormal, "", "");
        emoticonButton->setPosition(Vec2(leftMargin + layoutwidth *5.0f/8.0f, posBottomMenuButton));
        emoticonButton->setTouchEnabled(true);
        emoticonButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuEmoticonCallback,mMainScene));
        
        auto emoticonLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "emoticon"), Strings::getFontType(), FontSize);
        emoticonLabel->setColor(FontColor);
        emoticonLabel->setPosition(Vec2(emoticonButton->getPosition().x, posBottomMenuLabel));
        
        // exit button
        mRecordSettingButton = Button::create();
        mRecordSettingButton->loadTextures(SettingButtonNormal, "", "");
        mRecordSettingButton->setPosition(Vec2(leftMargin + layoutwidth *7.0f/8.0f, posBottomMenuButton));
        mRecordSettingButton->setTouchEnabled(true);
        mRecordSettingButton->addClickEventListener(CC_CALLBACK_1(MainScene::menuRecordSettingCallback,mMainScene));
        auto settingLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "setting"), Strings::getFontType(), FontSize);
        settingLabel->setColor(FontColor);
        settingLabel->setPosition(Vec2(mRecordSettingButton->getPosition().x, posBottomMenuLabel));
        
        mRecordLayout = Layout::create();
        mRecordLayout->retain();
        mRecordLayout->setContentSize(Size(visibleSize.width, bottomMenuHeight));
        mRecordLayout->setPosition(Vec2(visibleSize.width, 0.0));
        mRecordLayout->addChild(mFormatButton);
        mRecordLayout->addChild(effectButton);
        mRecordLayout->addChild(emoticonButton);
        mRecordLayout->addChild(mRecordSettingButton);
        mRecordLayout->addChild(formatLabel);
        mRecordLayout->addChild(effectLabel);
        mRecordLayout->addChild(emoticonLabel);
        mRecordLayout->addChild(settingLabel);
    }
}

void MainMenu::onTouchCallback(Ref* pSender, Widget::TouchEventType eventType)
{
    CCLOG("MainMenu::onTouchCallback");
}
void MainMenu::setState(MainState state)
{
    switch(state)
    {
        case MainState::Idle:
        {
            this->setVisible(false);
            break;
        }
        case MainState::Install:
        {
            this->setVisible(false);
            break;
        }
        case MainState::Enter:
        {
            mHomeButton->onPressed();
            mHomeButton->setEnabled(false);
            mRecordButton->onNormal();
            mRecordButton->setEnabled(true);
            mMainLayout->setVisible(true);
            mCallLayout->setVisible(false);
            {
                mMainLayout->runAction(_visibleHomeAction);
                mRecordLayout->runAction(_invisibleRecordAction);
            }
            show(true);
            break;
        }
        case MainState::Main:
        {
            if(currentState==MainState::Main)
                break;
            mHomeButton->setEnabled(false);
            mHomeButton->onPressed();
            mRecordButton->setEnabled(true);
            mExitButton->setEnabled(true);
            if(currentState==MainState::Recording)
            {
                mMainLayout->runAction(_visibleHomeAction);
                mRecordLayout->runAction(_invisibleRecordAction);
            }
            else
            {
                mMainLayout->setVisible(true);
                mRecordLayout->setVisible(false);
            }
            mCallLayout->setVisible(false);
            mRecordButton->onNormal();
            show(true);
            break;
        }
        case MainState::Calling:
        {
            mHomeButton->setEnabled(false);
            mHomeButton->onPressed();
            mRecordButton->setEnabled(false);
            mExitButton->setEnabled(false);
            if(currentState==MainState::Recording)
            {
                mRecordLayout->runAction(_invisibleRecordAction);
            }
            mMainLayout->setVisible(false);
            mCallLayout->setVisible(true);
            mRecordButton->onNormal();
            show(true);
            break;
        }
        case MainState::Recording:
        {
            mHomeButton->onNormal();
            if(mMainScene->isOfflineMode())
                mHomeButton->setEnabled(false);
            else
                mHomeButton->setEnabled(true);
            mRecordButton->setEnabled(false);
            mCallLayout->setVisible(false);
            mRecordLayout->setVisible(true);
            
            mMainLayout->runAction(_invisibleHomeAction);
            mRecordLayout->runAction(_visibleRecordAction);
            mRecordButton->onPressed();
            show(true);
            break;
        }
        case MainState::Exit:
        {
            this->setVisible(false);
            break;
        }
    }
    currentState = state;
}

void MainMenu::show(bool isShow)
{
    if(isShow)
    {
        if(this->isVisible())
            return;
        this->stopAllActions();
        this->setVisible(true);
        this->setPosition(_startPos);
        this->runAction(_visibleAction);
    }
    else
    {
        if(_isHiding)
            return;
        _isHiding = true;
        this->runAction(_invisibleAction);
    }
}

void MainMenu::afterVisibleAction(Node* sender)
{
}


void MainMenu::afterInvisibleAction(Node* sender)
{
    this->setVisible(false);
    _isHiding = false;
}

void MainMenu::menuCaptureCallback(Ref* pSender)
{
    mMainScene->menuCaptureCallback(pSender);
}

void MainMenu::menuHomeCallback(Ref* pSender)
{
    mMainScene->ChangeState(MainState::Main);
}

void MainMenu::menuRecordCallback(Ref* pSender)
{
    if(_isRecording)
    {
        mMainScene->OnEnableRecording(false);
        stopRecordAnimation();
        _isRecording = false;
    }
    else
    {
        mMainScene->OnEnableRecording(true);
        startRecordAnimation();
        _isRecording = true;
    }
}

void MainMenu::menuFormatCallback(Ref* pSender)
{
    if(_isRecording)
        return;
    ToggleButton* button = (ToggleButton*)pSender;
    if(button->isPressed())
    {
        mMainScene->ChangeRecordType(UIMessage::Record::MP4);
        UserDefault::getInstance()->setIntegerForKey("record_type", (int)UIMessage::Record::MP4);
        button->onNormal();
    }
    else
    {
        mMainScene->ChangeRecordType(UIMessage::Record::GIF);
        UserDefault::getInstance()->setIntegerForKey("record_type", (int)UIMessage::Record::GIF);
        button->onPressed();
    }
}

void MainMenu::stopRecording()
{
    if(_isRecording)
    {
        stopRecordAnimation();
        _isRecording = false;
    }
}

void MainMenu::startRecordAnimation()
{
    mRecordSettingButton->setEnabled(false);
    mCaptureButton->setEnabled(false);
    mHomeButton->setEnabled(false);
}

void MainMenu::stopRecordAnimation()
{
    mRecordSettingButton->setEnabled(true);
    mCaptureButton->setEnabled(true);
    if(mMainScene->isOfflineMode())
        mHomeButton->setEnabled(false);
    else
        mHomeButton->setEnabled(true);
}

cocos2d::Size& MainMenu::getSize()
{
    return (cocos2d::Size&)this->getContentSize();
}

Color4B& MainMenu::getBackgroundColor()
{
    return MainMenu::BackgroundColor1;
}

void MainMenu::setBackgroundColor(Color4B& color)
{
    _displayedColor.r = _realColor.r = color.r;
    _displayedColor.g = _realColor.g = color.g;
    _displayedColor.b = _realColor.b = color.b;
    _displayedOpacity = _realOpacity = color.a;
    
    updateColor();
}

void MainMenu::setTimeout(int timeout)
{
    MainMenu::RecTimeout = timeout;
}
