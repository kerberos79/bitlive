//
//  PageViewEx.h
//  BitLive
//
//  Created by keros on 6/25/15.
//
//

#ifndef __BitLive__UntouchablePageView__
#define __BitLive__UntouchablePageView__

#include <stdio.h>

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;

class UntouchablePageView : public PageView
{
public:
    UntouchablePageView();
    virtual ~UntouchablePageView();
    static UntouchablePageView* create();
    
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unusedEvent);
    virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unusedEvent);
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unusedEvent);
    virtual void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unusedEvent);
};
#endif /* defined(__BitLive__PageViewEx__) */
