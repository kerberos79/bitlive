//
//  ActionQueue.cpp
//  Vista
//
//  Created by kerberos on 3/1/15.
//
//

#include "ActionQueue.h"
ActionQueue::ActionQueue(Node* node)
:_node(node),
_busy(false)
{

}

ActionQueue::~ActionQueue()
{
    criticalsection.Enter();
    if(!_actionList.empty())
    {
        _actionList.clear();
    }
    criticalsection.Leave();
}

void ActionQueue::add(ActionInterval* action)
{
    criticalsection.Enter();
    if(_busy || !_actionList.empty())
    {
        _actionList.push_back(action);
        criticalsection.Leave();
        return;
    }
    
    auto sequence = Sequence::create(action,
                                     CallFuncN::create( CC_CALLBACK_0(ActionQueue::afterAction, this)),
                                     nullptr);
    
    _node->runAction(sequence);
    _busy = true;
    criticalsection.Leave();
}

void ActionQueue::afterAction()
{
    criticalsection.Enter();
    if(_actionList.empty())
    {
        _busy = false;
        criticalsection.Leave();
        return;
    }
    
    ActionInterval* action = _actionList.front();
    _actionList.pop_front();
    auto sequence = Sequence::create(action,
                                         CallFuncN::create( CC_CALLBACK_0(ActionQueue::afterAction, this)),
                                         nullptr);
    _node->runAction(sequence);
    criticalsection.Leave();
}
