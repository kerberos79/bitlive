//
//  Alert.h
//  BitLive
//
//  Created by keros on 8/27/15.
//
//

#ifndef __BitLive__Alert__
#define __BitLive__Alert__


#include <stdio.h>
#include <list>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Cocos2dxProfile.h"
#include "view/MainScene.h"
#include "view/LayerColorExtension.h"
#include "util/HexString.h"
#include "datatype/UIMessage.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;

class MainScene;
class Alert : public LayerColor
{
public:
    
    ~Alert();
    static Color4B BackgroundColor;
    static Color3B FontColor;
    static int FontSize;
    static int Height;
    
    static Alert* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    void show(std::string message, UIMessage::Alert type);
    void afterAction(Node* sender);
private:
    bool init(MainScene* mainscene);
    virtual void update(float dt);
    MainScene* mMainScene;
    LabelTTF *_titleText;
    Sequence *_visibleAction, *_textAction;
    std::string _message;
};
#endif /* defined(__BitLive__Alert__) */
