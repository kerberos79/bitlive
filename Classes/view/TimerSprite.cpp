//
//  TimerSprite.cpp
//  AnicameraMAC
//
//  Created by kerberos on 11/25/14.
//
//

#include "TimerSprite.h"

TimerSprite* TimerSprite::create(const std::string& filename, const std::function<void(Node*)> &callback)
{
    TimerSprite *sprite = new (std::nothrow) TimerSprite();
    if (sprite && sprite->initWithFile(filename))
    {
        sprite->_func = callback;
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

void TimerSprite::addTexture(int index, const std::string& filename)
{
    extraTextures[index] = Director::getInstance()->getTextureCache()->addImage(filename);
}

void TimerSprite::startAnimation()
{
    setTexture(extraTextures[_timerLimit-1]);
    setScale(1.0f, 1.0f);
    auto action = Sequence::create(ScaleTo::create(1.0f, 0.0f),
                                   CallFuncN::create( CC_CALLBACK_1(TimerSprite::afterAnimated, this, _timerLimit-2)),
                                   nullptr);
    runAction(action);
}

void TimerSprite::afterAnimated(Node* sender, int count)
{
    if(count>0)
    {
        setTexture(extraTextures[count]);
        setScale(1.0f, 1.0f);
        auto action = Sequence::create(ScaleTo::create(1.0f, 0.0f),
                                       CallFuncN::create( CC_CALLBACK_1(TimerSprite::afterAnimated, this, count-1)),
                                       nullptr);
        runAction(action);
    }
    else
    {
        setTexture(extraTextures[count]);
        setScale(1.0f, 1.0f);
        auto action = Sequence::create(ScaleTo::create(1.0f, 0.0f),
                                       CallFuncN::create( _func ),
                                       nullptr);
        runAction(action);
    }
}

void TimerSprite::setTimerLimit(int value)
{
    _timerLimit = value;
}