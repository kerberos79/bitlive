//
//  UntouchablePageView.cpp
//  BitLive
//
//  Created by keros on 6/25/15.
//
//

#include "UntouchablePageView.h"

UntouchablePageView::UntouchablePageView()
:PageView()
{
    
}

UntouchablePageView::~UntouchablePageView()
{
    _pageViewEventListener = nullptr;
    _pageViewEventSelector = nullptr;
}

UntouchablePageView* UntouchablePageView::create()
{
    UntouchablePageView* widget = new (std::nothrow) UntouchablePageView();
    if (widget && widget->init())
    {
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

bool UntouchablePageView::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unusedEvent)
{
    return true;
}

void UntouchablePageView::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unusedEvent)
{
}

void UntouchablePageView::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unusedEvent)
{
}

void UntouchablePageView::onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unusedEvent)
{
}