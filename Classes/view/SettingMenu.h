//
//  SettingMenu.h
//  BitLive
//
//  Created by keros on 6/14/15.
//
//

#ifndef __BitLive__SettingMenu__
#define __BitLive__SettingMenu__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "extra/Strings.h"
#include "datatype/UIMessage.h"
#include "view/SliderEx.h"
#include "view/ToggleButton.h"
#include "view/MainScene.h"
#include "view/LayerColorExtension.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;

class SettingMenu : public LayerColor, public cocos2d::ui::EditBoxDelegate, public LayerColorExtension
{
public:
    static Color4B TitleBackgroundColor;
    static Color4B BodyBackgroundColor;
    static Color3B TitleFontColor;
    static Color3B BodyFontColor;
    static int TitleFontSize;
    static int BodyFontSize;
    static int Height;
    static int TitleHeight;
    static std::string CloseButton;
    static std::string CountryImage;
    static std::string SignoutImage;
    static std::string JoinImage;
    static std::string LicenseImage;
    static std::string SignoutButtonNormal;
    static Color3B SignoutButtonFontColor;
    static int SignoutButtonFontSize;
    static std::string CountryCodeBoxBackground;
    static Size CountryCodeBoxSize;
    
    ~SettingMenu();
    static SettingMenu* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    virtual void editBoxEditingDidBegin(cocos2d::ui::EditBox* editBox);
    virtual void editBoxEditingDidEnd(cocos2d::ui::EditBox* editBox);
    virtual void editBoxTextChanged(cocos2d::ui::EditBox* editBox, const std::string& text);
    virtual void editBoxReturn(cocos2d::ui::EditBox* editBox);
    virtual void show(bool isShow);
    virtual cocos2d::Size& getSize();
    virtual Color4B& getBackgroundColor();
    virtual void setBackgroundColor(Color4B& color);
private:
    bool init(MainScene* mainscene);
    void menuSignoutCallback(Ref* pSender);
    void menuJoinCallback(Ref* pSender);
    void menuLicenseCallback(Ref* pSender);
    void OnSignoutButtonClick(Ref* pSender);
    void menuCloseCallback(Ref* pSender);
    void afterInvisibleAction(Node* sender);
    MainScene* mMainScene;
    Button* _signoutButton;
    cocos2d::ui::EditBox* countryCodeEditBox;
    float signoutButtonHeight;
};
#endif /* defined(__BitLive__SettingMenu__) */
