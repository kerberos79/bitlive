//
//  DialogEx.h
//  AnicameraMAC
//
//  Created by kerberos on 12/9/14.
//
//

#ifndef __AnicameraMAC__DialogEx__
#define __AnicameraMAC__DialogEx__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class DialogEx : public LayerColor
{
public:
    enum class Type
    {
        POSITIVE,
        NEGATIVE,
        BOTH
    };
    ~DialogEx();
    static Color4B BackgroundColor;
    static Color3B FontColor;
    static int TitleFontSize;
    static int BodyFontSize;
    static int MarginTop;
    static int MarginBottom;
    static std::string OkButtonNormal;
    static std::string OkButtonSelect;
    static std::string CancelButtonNormal;
    static std::string CancelButtonSelect;
    static DialogEx* create(const std::string title, const std::string body, DialogEx::Type type);
    bool init(const std::string title, const std::string body, DialogEx::Type type);
    bool addOkButtonClickListener(std::function<void(Ref*)> callback);
    void setTitle(const std::string title);
    void setBody(const std::string body);
    void show(bool isShow);
    
    virtual bool onTouchBegan(Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchMoved(Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchEnded(Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchCancelled(Touch* touch, cocos2d::Event* unused_event);
private:
    Text *_titleText;
    RichText *_bodyText;
    Button *_okButton;
    EventListenerTouchOneByOne *_CustomListener;
};
#endif /* defined(__AnicameraMAC__DialogEx__) */
