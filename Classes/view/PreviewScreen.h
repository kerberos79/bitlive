//
//  PreviewScreen.h
//  265Player
//
//  Created by kerberos on 10/16/14.
//
//

#ifndef ___65Player__PreviewScreen__
#define ___65Player__PreviewScreen__

#include <stdio.h>
#include "cocos2d.h"
#include "webrtc/base/criticalsection.h"
#include "webrtc/base/timeutils.h"
#include "platform/CCDevice.h"
#include "math/CCGeometry.h"
#include "view/YUVSprite.h"
#include "view/MainScene.h"
#include "util/GLProgramManager.h"
#include "util/EmoticonManager.h"
#include "util/StickerManager.h"
#include "util/DecorationManager.h"
#include "util/FaceDetectHelper.h"

USING_NS_CC;
class MainScene;
class EmoticonManager;
class StickerManager;
class DecorationManager;

class PreviewScreen : public RenderTexture
{
public:
    ~PreviewScreen();
    static PreviewScreen * create(MainScene *mainscene, int dst_width, int dst_height);
    bool init(MainScene *mainscene, int org_width, int dst_height, Texture2D::PixelFormat eFormat);
    void fitOnScreen(Size& parentLayerSize);
    void stretch(float ratio);
    int updateNV12Frame(unsigned char* y, unsigned char* uv);
    int updateYV12Frame(unsigned char* src_y, unsigned char* src_u, unsigned char* src_v);
    void updateFaceRect(int leftX, int leftY, int rightX, int rightY);
    void getVideoFrameNV21(unsigned char* dst_y, unsigned char* dst_uv);
    void getVideoFrameRGBA(unsigned char* dst_rgba);
    void getVideoFrameBGRA(unsigned char* dst_bgra);
    void flipCameraPreview();
    void setEncodingFrameRate(int value);
    void enterCallScreen();
    void enterSmallScreen();
    void enterFullScreen();
    bool isSmallScreen();
    void calculateGaussianWeights();
    void backupLastEffect();
    void initGLProgram();
    void setProgram(std::string program);
    void setBlurEffect(bool onoff);
    cocos2d::Size getScaledSize();
    float getCurrentRatio();
    Vec2 getCurrentPos();
    bool isProhibitEmoticon();
    
    bool isFront()
    {
        bool result = _isFrontCamera;
        return result;
    }
    
    int getFullScreenHeight()
    {
        return _fullScreenSize.height;
    }
    const char* getCaptureFileName()
    {
        return _filename;
    }
    
    Vec3 getDegree();
    const char* getEffect();
    unsigned int getBitrate();
    void setBitrate(unsigned int bitrate);
    void addEmoticonLayer();
    void setEmoticon(int tag);
    void setSticker(int tag);
    void setDecoration(int tag);
    int getSmallScreenSector();
    void setSmallScreenSector(int sector);
    void captureJpeg();
    void captureGif(bool enable);
    void enableRecord(bool enable);
    void setWaterMark(bool enable);
    
    std::function<int(unsigned char *y, unsigned char *u, unsigned char *v)> previewEncodingDelegate;
private:
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event  *event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event  *event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event  *event);
    virtual void update(float dt);
    virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags);
    void record();
    void selectSector(Vec2 p);
    void afterEnterCall(Node* sender);
    void afterCaptureFirstFrame();
    int rotate_value;
    int _isFrontCamera;
    float _fullScreenRatio, _smallScreenRatio, _ratioEx;

    MainScene *mMainScene;
    YUVSprite *mYUVSprite;
    CustomCommand _customCommand;
    
    int           _width, _height;
    int           _bytesWidth, _halfWidth, _halfHeight;
    cocos2d::Size _parentSreenSize;
    cocos2d::Size _fullScreenSize;
    cocos2d::Size _smallScreenSize;
    Vec2          _fullScreenPosition;
    Vec2          _smallScreenPosition[4];
    int           _smallScreenSector;
    
    unsigned char *_frameBufferY;
    unsigned char *_frameBufferU;
    unsigned char *_frameBufferV;
    
    long _frameInterval;
    uint32 _oldTime, _currentTime;
    uint32           _touchOldTime;
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    struct timespec _restInterval;
#endif
    unsigned int _frameSkipCount, _currentBitrate;
    rtc::CriticalSection criticalsection;
    rtc::CriticalSection criticalsection2;
    RenderTexture* rtX;
    Sprite* stepX;

    FaceDetectHelper* _faceDetectHelper;
    GLProgramManager _glProgramManager;
    GLProgramInfo* _currentProgramInfo;
    bool             _twoPassFilter;
    bool             _updatedFrame;
    bool             _smallScreen;
    bool             _touchOnScreen;
    std::string      _lastEffect;
    DrawNode*        _outline;
    EmoticonManager* _emoticonManager;
    Node*           _emoticonLayer;
    StickerManager*  _stickerManager;
    Sprite*          _sticker;
    Node*            _stickerLayer;
    Sprite*          _watermark;
    DecorationManager*  _decorationManager;
    Sprite*          _decoration;
    Vec2 _newLeftEye, _newRightEye, _midPoint;
    float _stickerScale, _stickerAngle;
    
    bool             _startRecord;
    unsigned char*   _recordRGBABuffer;
    unsigned char*   _recordBufferU;
    unsigned char*   _recordBufferV;
    CustomCommand _recordCommand;
    
    char _filename[200];
};


#endif /* defined(___65Player__PreviewScreen__) */
