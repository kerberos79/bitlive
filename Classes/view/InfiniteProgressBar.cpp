//
//  InfiniteProgressBar.cpp
//  BitLive
//
//  Created by keros on 6/15/15.
//
//

#include "InfiniteProgressBar.h"

InfiniteProgressBar::~InfiniteProgressBar()
{
    
}

InfiniteProgressBar *InfiniteProgressBar::create()
{
    InfiniteProgressBar *ret = new (std::nothrow) InfiniteProgressBar();
    if (ret && ret->init())
    {
        ret->autorelease();
        return ret;
    }
    else
    {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}

bool InfiniteProgressBar::init()
{
    
    auto progressbar = Sprite::create("infinite_progressbar.png");
    progressbar->setPosition(progressbar->getContentSize() * 0.5f);
    
    _progressBarTrack = Sprite::create("infinite_progressbar_track.png");
    _progressBarTrack->setPosition(_progressBarTrack->getContentSize()*0.5f);
    
    auto stencil = Sprite::create("infinite_progressbar_stencil.png");
    stencil->setPosition(_progressBarTrack->getContentSize() * 0.5f);
    
    auto clippedNode = ClippingNode::create();
    clippedNode->setPosition(Vec2((progressbar->getContentSize().width - _progressBarTrack->getContentSize().width) * 0.5f,
                                  (progressbar->getContentSize().height - _progressBarTrack->getContentSize().height) * 0.5f));
    clippedNode->setAlphaThreshold(0.5f);
    clippedNode->setStencil(stencil);
    clippedNode->addChild(_progressBarTrack);
    
    if(!initWithColor(Color4B(0,0,0,0), progressbar->getContentSize().width,  progressbar->getContentSize().height))
        return false;

    this->addChild(clippedNode);
    this->addChild(progressbar);
    return true;
}

void InfiniteProgressBar::startAction()
{
    _progressBarTrack->setPosition(Vec2(-_progressBarTrack->getContentSize().width*0.5f,
                                        _progressBarTrack->getContentSize().height*0.5f));
    _progressBarTrack->runAction(Sequence::create(
                     MoveTo::create(3, Vec2(_progressBarTrack->getContentSize().width,
                                            _progressBarTrack->getContentSize().height*0.5f)),
                     CallFuncN::create( CC_CALLBACK_0(InfiniteProgressBar::onRepeat, this)),
                                                  nullptr));
}

void InfiniteProgressBar::onRepeat()
{
    _progressBarTrack->setPosition(Vec2(-_progressBarTrack->getContentSize().width*0.5f,
                                        _progressBarTrack->getContentSize().height*0.5f));
    _progressBarTrack->runAction(Sequence::create(
                       MoveTo::create(3, Vec2(_progressBarTrack->getContentSize().width,
                                              _progressBarTrack->getContentSize().height*0.5f)),
                       CallFuncN::create( CC_CALLBACK_0(InfiniteProgressBar::onRepeat, this)),
                       nullptr));
}

void InfiniteProgressBar::removeAction()
{
    _progressBarTrack->stopAllActions();
}
