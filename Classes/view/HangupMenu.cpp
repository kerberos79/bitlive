//
//  HangupMenu.cpp
//  BitLive
//
//  Created by keros on 6/14/15.
//
//

#include "HangupMenu.h"

Color4B HangupMenu::BackgroundColor;
Color3B HangupMenu::FontColor;
int HangupMenu::FontSize;
int HangupMenu::Height;
std::string HangupMenu::OkButtonNormal;
std::string HangupMenu::OkButtonSelect;
std::string HangupMenu::CancelButtonNormal;
std::string HangupMenu::CancelButtonSelect;

HangupMenu::~HangupMenu()
{
}

HangupMenu * HangupMenu::create(MainScene* mainscene)
{
    HangupMenu * menu = new (std::nothrow) HangupMenu();
    if( menu && menu->init(mainscene))
    {
        menu->setPosition(Vec2::ZERO);
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void HangupMenu::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& hangupMenu = DICTOOL->getSubDictionary_json(doc, "hangup_menu");
    HangupMenu::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(hangupMenu, "background_color"));
    HangupMenu::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(hangupMenu, "font_color"));
    HangupMenu::FontSize = DICTOOL->getIntValue_json(hangupMenu, "font_size") * MainScene::LayoutScale;
    HangupMenu::Height = DICTOOL->getIntValue_json(hangupMenu, "height") * MainScene::LayoutScale;
    HangupMenu::OkButtonNormal = DICTOOL->getStringValue_json(hangupMenu, "ok_button_normal");
    HangupMenu::OkButtonSelect = DICTOOL->getStringValue_json(hangupMenu, "ok_button_select");
    HangupMenu::CancelButtonNormal = DICTOOL->getStringValue_json(hangupMenu, "cancel_button_normal");
    HangupMenu::CancelButtonSelect = DICTOOL->getStringValue_json(hangupMenu, "cancel_button_select");
}

bool HangupMenu::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("submenu.json");
    rapidjson::Document doc;
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    auto _title = Text::create(DICTOOL->getStringValue_json(doc, "submenu_hangup"), Strings::getFontType(), HangupMenu::FontSize);
    _title->setColor(HangupMenu::FontColor);
    _title->setPosition(Vec2(visibleSize.width * 0.5f, ExitMenu::Height * 0.8f));
    
    
    _leftPosition = Vec2(visibleSize.width * 0.3f, HangupMenu::Height * 0.4f);
    _rightPosition = Vec2(visibleSize.width * 0.7f, HangupMenu::Height * 0.4f);
    // ok button
    _okButton = Button::create();
    _okButton->loadTextures(HangupMenu::OkButtonNormal, "", "");
    _okButton->setPosition(_leftPosition);
    
    _okButton->addClickEventListener(CC_CALLBACK_1(HangupMenu::OnOkButtonClick,this));
    
    // cancel button
    _cancelButton = Button::create();
    _cancelButton->loadTextures(HangupMenu::CancelButtonNormal, "", "");
    _cancelButton->setPosition(_rightPosition);
    
    _cancelButton->addClickEventListener(CC_CALLBACK_1(HangupMenu::OnCancelButtonClick,this));
    
    if(!initWithColor(HangupMenu::BackgroundColor, visibleSize.width, HangupMenu::Height))
        return false;
    this->addChild(_title);
    this->addChild(_okButton);
    this->addChild(_cancelButton);
    _currentState = HangupMenu::State::Disconnect;
    _startPos = Vec2(0, -HangupMenu::Height);
    _endPos = Vec2::ZERO;
    _visibleAction = EaseExponentialOut::create(MoveTo::create(0.4f, _endPos));
    _visibleAction->retain();
    
    _invisibleAction = Sequence::create(EaseExponentialOut::create(MoveTo::create(0.4f, _startPos)),
                                        CallFuncN::create( CC_CALLBACK_1(HangupMenu::afterInvisibleAction, this)),
                                        nullptr);
    _invisibleAction->retain();
    return true;
}

void HangupMenu::OnOkButtonClick(Ref* pSender)
{
    switch (_currentState)
    {
        case State::Disconnect:
        {
            mMainScene->OnHangup();
            break;
        }
        case State::Disconnected:
        {
            mMainScene->toggleMenuCallback(this);
            break;
        }
    }
}

void HangupMenu::OnCancelButtonClick(Ref* pSender)
{
    mMainScene->toggleMenuCallback(this);
}

void HangupMenu::OnDisconnect()
{
    _currentState = HangupMenu::State::Disconnect;
    _okButton->setPosition(_leftPosition);
    _cancelButton->setVisible(false);
}

void HangupMenu::OnDisconnected()
{
    _currentState = HangupMenu::State::Disconnected;
    _okButton->setPosition(_rightPosition);
    _cancelButton->setVisible(false);
}

void HangupMenu::show(bool isShow)
{
    if(isShow)
    {
        this->stopAllActions();
        this->setVisible(true);
        this->setPosition(_startPos);
        this->runAction(_visibleAction);
    }
    else
    {
        this->runAction(_invisibleAction);
    }
}

void HangupMenu::afterInvisibleAction(Node* sender)
{
    this->setVisible(false);
}

cocos2d::Size& HangupMenu::getSize()
{
    return (cocos2d::Size&)this->getContentSize();
}

Color4B& HangupMenu::getBackgroundColor()
{
    return HangupMenu::BackgroundColor;
}

void HangupMenu::setBackgroundColor(Color4B& color)
{
    _displayedColor.r = _realColor.r = color.r;
    _displayedColor.g = _realColor.g = color.g;
    _displayedColor.b = _realColor.b = color.b;
    _displayedOpacity = _realOpacity = color.a;
    
    updateColor();
}
