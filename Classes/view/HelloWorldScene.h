#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/DictionaryHelper.h"
#include "PreviewScreen.h"
#include "ToggleButton.h"
#include "EmoticonMenuButton.h"
#include "TimerSprite.h"
#include "SliderEx.h"
#include "DialogEx.h"
#include "controller/XmppLoginThread.h"
#include "controller/CallStateHandler.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocostudio;

#define   DEGREE0   0
#define   DEGREE90  90
#define   DEGREE180 180
#define   DEGREE270 -90

#define   MOVE_MENU_DURATION 0.2
#define   ROTATE_BUTTON_DURATION 0.5

#define   POSITION_LEFTBOTTOM  1
#define   POSITION_CENTERBOTTOM 2
#define   POSITION_RIGHTBOTTOM 3
#define   POSITION_LEFTMIDDLE 4
#define   POSITION_CENTER 5
#define   POSITION_RIGHTMIDDLE 6
#define   POSITION_LEFTTOP 7
#define   POSITION_CENTERTOP 8
#define   POSITION_RIGHTTOP 9

#define   ZORDER_PREVIEW_SCREEN     0
#define   ZORDER_EMOTICON_LAYER     1
#define   ZORDER_SLIDEMENU_LAYER    2
#define   ZORDER_MAINMENU_LAYER     3
#define   ZORDER_CAPTURE_SCREEN     10
#define   ZORDER_CAPTUREMENU_LAYER  11
#define   ZORDER_DIALGO_LAYER       100

#define   ACTION_SLIDE_MOVE_DELAY       0.2

class HelloWorld : public cocos2d::Layer, public sigslot::has_slots<> 
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    PreviewScreen* getPreviewScreen()
    {
    	return mPreviewScreen;
    }

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    bool loadLayoutFromJson();
    void createTopMenu();
    void createBottomMenu();
    void createCaptureMenu();
    void createRightMenu();
    void createPreviewScreen();
    void createLeftMenu();
    void createOptionMenu();
    void createEmoticonSprite();
    void createTimerSprite();
    void createDialog(const char *title, const char *body);
    void initCustomEvent();
    void showDialog(const char *body);
    void hideDialog();

    
    // a selector callback
    void menuFlipCallback(cocos2d::Ref* pSender);
    void menuCaptureCallback(cocos2d::Ref* pSender);
    void menuCloseCallback(cocos2d::Ref* pSender);
    void menuCancelCallback(cocos2d::Ref* pSender);
    void menuSaveCallback(cocos2d::Ref* pSender);
    void menuShareCallback(cocos2d::Ref* pSender);
    void menuRightCallback(Ref* pSender, Widget::TouchEventType eventType);
    void menuLeftCallback(Ref* pSender);
    void menuEmoticonCallback(cocos2d::Ref* pSender);
    void menuShotCallback(cocos2d::Ref* pSender);
    void menuTimerCallback(cocos2d::Ref* pSender);
    void menuControllerCallback(cocos2d::Ref* pSender);
    void menuEffectCallback(cocos2d::Ref* pSender);
    void menuTimerOptionCallback(Ref* pSender, Widget::TouchEventType eventType);
    void toggleTopMenuButton(ToggleButton* button);
    void rotateButton(int newRotation);
    void onAcceleration(Acceleration *acc, Event *event);
    void onCaptureScreen();
    
    void OnXmppLoginCallback(buzz::XmppEngine::State state);
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

    ~HelloWorld();
private:
    Size mSideMenuSize;
    int mOptionMenuHeight;
    int mBottomMenuHeight;
    int mTopMenuHeight;
    int mBottomHeight;
    int mDialogTitleFontSize;
    int mDialogTextFontSize;
    int mDialogMarginTop;
    int mDialogMarginBottom;
    bool mTransparentBottomMenu;
    Layer *mLeftPanel;
    Layer *mRightPanel;
    Layer *mOptionPanel;
    LayerColor *mTopPanel;
    LayerColor *mBottomPanel;
    LayerColor *mCapturePanel;
    Sprite *mCaptureSprite;
    Sprite *mEmoticon;
    Button *mCaptureButton;
    ToggleButton *mTimerToggleButton;
    ToggleButton *mFlashButton;
    TimerSprite *mTimerSprite;
    PreviewScreen *mPreviewScreen;
    SliderEx *mExposureSlider;
    DialogEx *mDialog;
    std::vector<Button *> mLeftButtonList;
    std::vector<Button *> mRightButtonList;
    std::vector<Button *> mTopButtonList;
    std::vector<Button *> mBottomButtonList;
    std::vector<Button *> mCaptureButtonList;
    int mCurrentDirection;
    ToggleButton *toggledButtonOnRightMenu;
    EmoticonMenuButton *mSelectedButtonOnLeftMenu;
    ToggleButton *toggledButtonOnTimerOptionMenu;
    std::string _filename;
    static const int captureScreenTag = 9999;
    static const int emoticonSpriteTag = 10000;
    static const int timerSpriteTag = 10001;
    static const int dialogTag = 8888;
    GLubyte* mCaptureBuffer;
    Texture2D* mCaptureTexture;
    Size mCaptureSize;
    std::string mOutputFile;
    CallStateHandler* _callStateHandler;
};

#endif // __HELLOWORLD_SCENE_H__
