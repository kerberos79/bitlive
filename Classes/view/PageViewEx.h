//
//  PageViewEx.h
//  BitLive
//
//  Created by keros on 6/25/15.
//
//

#ifndef __BitLive__PageViewEx__
#define __BitLive__PageViewEx__

#include <stdio.h>

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;

class PageViewEx : public PageView
{
public:
    PageViewEx();
    virtual ~PageViewEx();
    static PageViewEx* create();
    void setTouchThresold(float thresold);
    
protected:
    virtual void handleMoveLogic(Touch *touch);
    
    float positiveTouchThresold, negativeTouchThresold;
};
#endif /* defined(__BitLive__PageViewEx__) */
