//
//  WebBrowser.h
//  BitLive
//
//  Created by kerberos on 7/24/15.
//
//

#ifndef __BitLive__WebBrowser__
#define __BitLive__WebBrowser__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "extra/Strings.h"
#include "util/IdentityManager.h"
#include "view/MainScene.h"
#include "view/ToggleButton.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;
class WebBrowser : public LayerColor, public cocos2d::ui::EditBoxDelegate
{
public:
    ~WebBrowser();
    static Color4B MenuColor;
    static Color3B FontColor;
    static int FontSize;
    static int MenuHeight;
    static int LeftMargin;
    static std::string UrlBox;
    static WebBrowser* create(MainScene *mainscene);
    static void loadLayout(rapidjson::Document &doc);
    void show(bool isShow);
    void show(std::string url);
    virtual void update(float dt);
    virtual void editBoxEditingDidBegin(cocos2d::ui::EditBox* editBox);
    virtual void editBoxEditingDidEnd(cocos2d::ui::EditBox* editBox);
    virtual void editBoxTextChanged(cocos2d::ui::EditBox* editBox, const std::string& text);
    virtual void editBoxReturn(cocos2d::ui::EditBox* editBox);
    
private:
    bool init(MainScene *mainscene);
    void menuLeftCallback(Ref* pSender);
    void menuRightCallback(Ref* pSender);
    void menuReloadCallback(Ref* pSender);
    void menuExitCallback(Ref* pSender);
    void onSaveAsHomepageCallback(std::string url);
    void onSendLinkCallback(std::string url);
    void onCloseCallback();
    void OnCancelButtonClick(Ref* pSender);
    bool OnShouldStartLoading(experimental::ui::WebView *sender, std::string url);
    void OnDidFinishLoading(experimental::ui::WebView *sender, std::string url);
    MainScene* mMainScene;
    experimental::ui::WebView* mWebview;
    LayerColor* mUrlLayout;
    cocos2d::ui::EditBox* mUrlEditBox;
    Label *_titleText;
    Size visibleSize;
    bool mMenuVisible;
    ToggleButton* mReloadButton;
    std::string mUrl;
    
};
#endif /* defined(__BitLive__WebBrowser__) */
