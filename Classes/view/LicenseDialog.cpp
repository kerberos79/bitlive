//
//  LicenseDialog.cpp
//  AnicameraMAC
//
//  Created by kerberos on 12/9/14.
//
//

#include "LicenseDialog.h"


Color4B LicenseDialog::BackgroundColor;
Color3B LicenseDialog::FontColor;
int LicenseDialog::TitleFontSize = 0;
int LicenseDialog::BodyFontSize = 0;
int LicenseDialog::MarginTop = 0;
int LicenseDialog::MarginBottom = 0;
std::string LicenseDialog::OkButtonNormal;
std::string LicenseDialog::OkButtonSelect;

LicenseDialog::~LicenseDialog()
{
}

LicenseDialog * LicenseDialog::create(MainScene* mainscene)
{
    LicenseDialog * dialog = new (std::nothrow) LicenseDialog();
    if( dialog && dialog->init(mainscene))
    {
        dialog->setPosition(Vec2::ZERO);
        dialog->autorelease();
        return dialog;
    }
    CC_SAFE_DELETE(dialog);
    return nullptr;
}

void LicenseDialog::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& LicenseDialog = DICTOOL->getSubDictionary_json(doc, "edit_dialog");
    LicenseDialog::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(LicenseDialog, "background_color"));
    LicenseDialog::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(LicenseDialog, "font_color"));
    LicenseDialog::TitleFontSize = DICTOOL->getIntValue_json(LicenseDialog, "title_font_size") * MainScene::LayoutScale;
    LicenseDialog::BodyFontSize = DICTOOL->getIntValue_json(LicenseDialog, "body_font_size") * MainScene::LayoutScale;
    LicenseDialog::MarginTop = DICTOOL->getIntValue_json(LicenseDialog, "margin_top") * MainScene::LayoutScale;
    LicenseDialog::MarginBottom = DICTOOL->getIntValue_json(LicenseDialog, "margin_bottom") * MainScene::LayoutScale;
    LicenseDialog::OkButtonNormal = DICTOOL->getStringValue_json(LicenseDialog, "ok_button_normal");
    LicenseDialog::OkButtonSelect = DICTOOL->getStringValue_json(LicenseDialog, "ok_button_select");
}

bool LicenseDialog::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    _titleText = Text::create("license", Strings::getFontType(), LicenseDialog::TitleFontSize);
    _titleText->setColor(LicenseDialog::FontColor);
    _titleText->setPosition(Vec2(visibleSize.width * 0.5f,
                                visibleSize.height - (LicenseDialog::MarginTop + _titleText->getContentSize().height)*0.5f));
    _okButton = Button::create();
    _okButton->loadTextures(LicenseDialog::OkButtonNormal, LicenseDialog::OkButtonSelect, "");
    _okButton->setPosition(Vec2(visibleSize.width * 0.5f,
                                (LicenseDialog::MarginBottom + _okButton->getContentSize().height)*0.5f));
    _okButton->addClickEventListener(CC_CALLBACK_1(LicenseDialog::OnOkButtonClick,this));
    
    int height = visibleSize.height - (LicenseDialog::MarginTop + LicenseDialog::MarginBottom + _titleText->getContentSize().height + _okButton->getContentSize().height);
    // add messege
    
    
    _listView = ListView::create();
    _listView->setContentSize(Size(visibleSize.width * 0.95f, height));
    _listView->setPosition(Vec2(visibleSize.width * 0.025f, LicenseDialog::MarginBottom+_okButton->getContentSize().height));
    _listView->setDirection(ListView::Direction::VERTICAL);
    _listView->setBounceEnabled(false);
    _listView->pushBackDefaultItem();

    _bodyText = RichText::create();
    _bodyText->ignoreContentAdaptWithSize(true);
    _bodyText->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.5f));
    auto re3 = RichElementText::create(3,LicenseDialog::FontColor, 255, FileUtils::getInstance()->getStringFromFile("license.txt"), Strings::getFontType(), LicenseDialog::BodyFontSize);
    _bodyText->pushBackElement(re3);
    _bodyText->formatText();
    _listView->addChild(_bodyText);

    if(!initWithColor(LicenseDialog::BackgroundColor, visibleSize.width, visibleSize.height))
        return false;
    
    this->setPosition(Vec2::ZERO);
    this->addChild(_titleText);
    this->addChild(_listView);
    this->addChild(_okButton);
    return true;
}

void LicenseDialog::OnOkButtonClick(Ref* pSender)
{
    this->setVisible(false);
}
