//
//  InstallPage.cpp
//  Vista
//
//  Created by kerberos on 1/23/15.
//
//

#include "InstallPage.h"
#include "CCSoomlaEventDispatcher.h"
#include "CCDomainFactory.h"

Color4B InstallPage::BackgroundColor;
Color3B InstallPage::FontColor;
int InstallPage::TitleFontSize;
int InstallPage::TextFontSize;
int InstallPage::GuideFontSize;
std::string InstallPage::PhoneLoginButtonNormal;
std::string InstallPage::GoogleLoginButtonNormal;
std::string InstallPage::PreviousButtonNormal;
std::string InstallPage::ForwardButtonNormal;
std::string InstallPage::SigninButtonNormal;
std::string InstallPage::SkipButtonNormal;
std::string InstallPage::LoadingNormal;
std::string InstallPage::CountryCodeBoxBackground;
Size InstallPage::CountryCodeBoxSize;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* PhoneNumberEvent = "PhoneNumberEvent";
static const char* kClientId = "1067915222659-3f2iod4all0h4nt25oeaa8oq12to6h5l.apps.googleusercontent.com";
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//static const char* kClientId = "1067915222659-ehm1j0feeaq6lvg9lpliep56dai7m5jk.apps.googleusercontent.com"; // debug.keystore
static const char* kClientId = "1067915222659-lf0kubqketo14ebfgjh2iftengcpsvi2.apps.googleusercontent.com"; // bitlive.keystore
#else
static const char* kClientId = "";
#endif

InstallPage* InstallPage::create(MainScene *mainscene)
{
    
    InstallPage * page = new (std::nothrow) InstallPage();
    if( page && page->initWithProfile(mainscene))
    {
        page->setPosition(Vec2::ZERO);
        page->autorelease();
        return page;
    }
    CC_SAFE_DELETE(page);
    return nullptr;
}

void InstallPage::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& installDialog = DICTOOL->getSubDictionary_json(doc, "install_dialog");
    InstallPage::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(installDialog, "background_color"));
    InstallPage::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(installDialog, "font_color"));
    InstallPage::TitleFontSize = DICTOOL->getIntValue_json(installDialog, "title_font_size") * MainScene::LayoutScale;
    InstallPage::TextFontSize = DICTOOL->getIntValue_json(installDialog, "text_font_size") * MainScene::LayoutScale;
    InstallPage::GuideFontSize = DICTOOL->getIntValue_json(installDialog, "guide_font_size") * MainScene::LayoutScale;
    InstallPage::PhoneLoginButtonNormal = DICTOOL->getStringValue_json(installDialog, "phone_login_button_normal");
    InstallPage::GoogleLoginButtonNormal = DICTOOL->getStringValue_json(installDialog, "google_login_button_normal");
    InstallPage::PreviousButtonNormal = DICTOOL->getStringValue_json(installDialog, "previous_button_normal");
    InstallPage::ForwardButtonNormal = DICTOOL->getStringValue_json(installDialog, "forward_button_normal");
    InstallPage::SigninButtonNormal = DICTOOL->getStringValue_json(installDialog, "signin_button_normal");
    InstallPage::SkipButtonNormal = DICTOOL->getStringValue_json(installDialog, "skip_button_normal");
    InstallPage::LoadingNormal = DICTOOL->getStringValue_json(installDialog, "loading_normal");
    InstallPage::CountryCodeBoxBackground = DICTOOL->getStringValue_json(installDialog, "country_code_box_background");
    InstallPage::CountryCodeBoxSize = Size(DICTOOL->getIntValue_json(installDialog, "country_code_box_width") * MainScene::LayoutScale,
                                           DICTOOL->getIntValue_json(installDialog, "country_code_box_height") * MainScene::LayoutScale);
}

bool InstallPage::initWithProfile(MainScene *mainscene)
{
    soomla::CCSoomla::initialize("customSecret");
    
    __Dictionary *profileParams = __Dictionary::create();
    __Dictionary *twitterParams = __Dictionary::create();
    twitterParams->setObject(__String::create("T8NMGwKq6USYCjZox86Bxd2RJ"), "consumerKey");
    twitterParams->setObject(__String::create("TO88J9QjIJlSGfuyfza9Ox2OgTsLc1rjcmaJknlIMuUw0T1I30"), "consumerSecret");
    
    profileParams->setObject(twitterParams, soomla::CCUserProfileUtils::providerEnumToString(soomla::TWITTER)->getCString());
    
    __Dictionary *googleParams = __Dictionary::create();
    googleParams->setObject(__String::create(kClientId), "clientId");
    
    profileParams->setObject(googleParams, soomla::CCUserProfileUtils::providerEnumToString(soomla::GOOGLE)->getCString());
    
    soomla::CCSoomlaProfile::initialize(profileParams);

    return init(mainscene);
}

bool InstallPage::init(MainScene *mainscene)
{
    mMainScene = mainscene;
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("install.json");
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    homepageUrl = DICTOOL->getStringValue_json(doc, "homepage_url");
    UserDefault::getInstance()->setStringForKey("homepage", homepageUrl);

    Size visibleSize = Director::getInstance()->getVisibleSize();
    pageView = UntouchablePageView::create();
    pageView->setContentSize(visibleSize);
    pageView->addEventListener(CC_CALLBACK_2(InstallPage::pageviewCallback,this));
    
////// page 0 //////
    Layout* page0 = Layout::create();
    page0->setContentSize(visibleSize);
    page0->setPosition(Vec2::ZERO);

    Text* title0 = Text::create(DICTOOL->getStringValue_json(doc, "install_welcome"), Strings::getFontType(), TitleFontSize);
    title0->setColor(FontColor);
    title0->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.75f));
    
    auto label0 = RichText::create();
    label0->ignoreContentAdaptWithSize(false);
    label0->setContentSize(Size(visibleSize.width * 0.5, visibleSize.height* 0.2f));
    
    RichElementText* re2 = RichElementText::create(3,FontColor, 255, DICTOOL->getStringValue_json(doc, "install_choose_auth_type"), Strings::getFontType(), TextFontSize);
    label0->pushBackElement(re2);
    label0->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height *0.45f));
    phoneNumberLoginButton = Button::create();
    phoneNumberLoginButton->loadTextures(PhoneLoginButtonNormal, "", "");
    phoneNumberLoginButton->setTitleText(DICTOOL->getStringValue_json(doc, "install_phone_login_label"));
    phoneNumberLoginButton->setTitleFontName(Strings::getFontType());
    phoneNumberLoginButton->setTitleColor(FontColor);
    phoneNumberLoginButton->setTitleFontSize(TextFontSize);
    phoneNumberLoginButton->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.4f) );
    phoneNumberLoginButton->setTouchEnabled(true);
    phoneNumberLoginButton->addClickEventListener(CC_CALLBACK_1(InstallPage::menuPhoneNumberLoginback,this));
    
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    googleLoginButton = Button::create();
    googleLoginButton->loadTextures(GoogleLoginButtonNormal, "", "");
    googleLoginButton->setTitleText(DICTOOL->getStringValue_json(doc, "install_google_login_label"));
    googleLoginButton->setTitleFontName(Strings::getFontType());
    googleLoginButton->setTitleColor(FontColor);
    googleLoginButton->setTitleFontSize(TextFontSize);
    googleLoginButton->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.3f));
    googleLoginButton->setTouchEnabled(true);
    googleLoginButton->addClickEventListener(CC_CALLBACK_1(InstallPage::menuGoogleLoginback,this));
#endif
    
    auto skipButton = Button::create();
    skipButton->loadTextures(SkipButtonNormal, "", "");
    skipButton->setTitleText(DICTOOL->getStringValue_json(doc, "skip_label"));
    skipButton->setTitleFontName(Strings::getFontType());
    skipButton->setTitleColor(FontColor);
    skipButton->setTitleFontSize(TextFontSize*0.8f);
    skipButton->setPosition(Vec2(visibleSize.width * 0.85f, visibleSize.height * 0.08f));
    skipButton->setTouchEnabled(true);
    skipButton->addClickEventListener(CC_CALLBACK_1(InstallPage::menuSkipCallback,this));
    
#ifdef COCOS2D_DEBUG
    debugEditBox = cocos2d::ui::EditBox::create(Size(600, 180), cocos2d::ui::Scale9Sprite::create("edit_box_background.png"));
    
    debugEditBox->setInputMode(cocos2d::ui::EditBox::InputMode::URL);
    debugEditBox->setInputFlag(cocos2d::ui::EditBox::InputFlag::SENSITIVE);
    debugEditBox->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.2f));
    debugEditBox->setFontName(Strings::getFontType());
    debugEditBox->setFontSize(TextFontSize);
    debugEditBox->setFontColor(FontColor);
    debugEditBox->setMaxLength(15);
    debugEditBox->setReturnType(cocos2d::ui::EditBox::KeyboardReturnType::DONE);
    debugEditBox->setDelegate(this);
    debugEditBox->setReturnType(cocos2d::ui::EditBox::KeyboardReturnType::DONE);

    auto debugButton = Button::create();
    debugButton->loadTextures("btn_ok_normal.png", "", "");
    debugButton->setPosition(Vec2(visibleSize.width * 0.85f, visibleSize.height * 0.2f));
    debugButton->setTouchEnabled(true);
    debugButton->addClickEventListener(CC_CALLBACK_1(InstallPage::menuDebugCallback,this));
    page0->addChild(debugEditBox);
    page0->addChild(debugButton);
#endif
    page0->addChild(title0);
    page0->addChild(label0);
    page0->addChild(phoneNumberLoginButton);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    page0->addChild(googleLoginButton);
#endif
    page0->addChild(skipButton);
    
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(EventListenerCustom::create("CustomPhoneNumber",  [this](EventCustom* event){
        if(event->getUserData()!=nullptr)
        {
            userID = (const char*)event->getUserData();
            agreement(userID);
        }
        else
        {
        }
    }), this);
    
    std::function<void(EventCustom *)> handleLoginCanceled = [this](EventCustom *event) {
        CCLOG("handleLoginCanceled");
        agreement("");
    };
    
    std::function<void(EventCustom *)> handleLoginFinished = [this](EventCustom *event) {
        __Dictionary *eventData = (__Dictionary *)event->getUserData();
        
        CCLOG("handleLoginFinished");
        soomla::CCUserProfile *userProfile = (soomla::CCUserProfile *)(eventData->objectForKey(soomla::CCProfileConsts::DICT_ELEMENT_USER_PROFILE));
        
        soomla::CCError *profileError = nullptr;
        soomla::CCSoomlaProfile::getInstance()->logout(soomla::GOOGLE, &profileError);
        if(profileError)
            CCLOG("Logout error : %s", profileError->getInfo());
        else
            CCLOG("trying to logout");
        userID = userProfile->getEmail()->getCString();
        agreement(userID);
    };
    
    std::function<void(EventCustom *)> handleLogoutFinished = [this](EventCustom *event) {
        CCLOG("handleLogoutFinished : logged out");
    };
    
    std::function<void(EventCustom *)> handleProfileUpdatedFinished = [this](EventCustom *event) {
        __Dictionary *eventData = (__Dictionary *)event->getUserData();
        soomla::CCUserProfile *userProfile = (soomla::CCUserProfile *)(eventData->objectForKey(soomla::CCProfileConsts::DICT_ELEMENT_USER_PROFILE));
        CCLOG("handleProfileUpdatedFinished : %s", userProfile->getEmail()->getCString());
    };
    
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(EventListenerCustom::create(soomla::CCProfileConsts::EVENT_LOGIN_CANCELLED, handleLoginCanceled), this);
    
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(EventListenerCustom::create(soomla::CCProfileConsts::EVENT_LOGIN_FINISHED, handleLoginFinished), this);
    
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(EventListenerCustom::create(soomla::CCProfileConsts::EVENT_LOGOUT_FINISHED, handleLogoutFinished), this);
    
    getEventDispatcher()->addEventListenerWithSceneGraphPriority(EventListenerCustom::create(soomla::CCProfileConsts::EVENT_USER_PROFILE_UPDATED, handleProfileUpdatedFinished), this);
    pageView->insertPage(page0,0);
    
////// page 1 //////
    page1 = Layout::create();
    page1->setContentSize(visibleSize);
    page1->setPosition(Vec2::ZERO);
    
    labelID = DICTOOL->getStringValue_json(doc, "label_id");
    labelUserID = LabelTTF::create(labelID, Strings::getFontType(), TextFontSize);
    labelUserID->setColor(FontColor);
    labelUserID->setAnchorPoint(Vec2::ZERO);
    labelUserID->setPosition(Vec2(visibleSize.width * 0.2f, visibleSize.height * 0.8f));
    
    labelCountryCode = LabelTTF::create(DICTOOL->getStringValue_json(doc, "label_country_code"), Strings::getFontType(), TextFontSize);
    labelCountryCode->setColor(FontColor);
    labelCountryCode->setAnchorPoint(Vec2::ZERO);
    labelCountryCode->setPosition(Vec2(visibleSize.width * 0.2f, visibleSize.height * 0.7f));
    
    countryCodeEditBox = cocos2d::ui::EditBox::create(InstallPage::CountryCodeBoxSize,
                                                      cocos2d::ui::Scale9Sprite::create(InstallPage::CountryCodeBoxBackground));
    countryCodeEditBox->setInputMode(cocos2d::ui::EditBox::InputMode::PHONE_NUMBER);
    countryCodeEditBox->setAnchorPoint(Vec2::ZERO);
    countryCodeEditBox->setPosition(Vec2(visibleSize.width * 0.22f + labelCountryCode->getContentSize().width, labelCountryCode->getPosition().y + labelCountryCode->getContentSize().height * 0.5f - countryCodeEditBox->getContentSize().height * 0.5f));
    countryCodeEditBox->setFontName(Strings::getFontType());
    countryCodeEditBox->setFontSize(TextFontSize);
    countryCodeEditBox->setFontColor(Color3B::WHITE);
    countryCodeEditBox->setMaxLength(4);
    countryCodeEditBox->setReturnType(cocos2d::ui::EditBox::KeyboardReturnType::DONE);
    countryCodeEditBox->setText(Device::getCountryCodeLocale().c_str());
    
    // add messege
    agreementText = RichText::create();
    agreementText->ignoreContentAdaptWithSize(false);
    agreementText->setContentSize(Size(visibleSize.width * 0.7, visibleSize.height* 0.3f));
    
    RichElementText* re3 = RichElementText::create(3,FontColor, 255, DICTOOL->getStringValue_json(doc, "agreement_text"), Strings::getFontType(), TextFontSize);
    agreementText->pushBackElement(re3);
    agreementText->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.45f));
    
    signinButton = Button::create();
    signinButton->loadTextures(SigninButtonNormal, "", "");
    signinButton->setTitleText(DICTOOL->getStringValue_json(doc, "signin_label"));
    signinButton->setTitleColor(FontColor);
    signinButton->setTitleFontName(Strings::getFontType());
    signinButton->setTitleFontSize(TextFontSize);
    signinButton->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.3f) );
    signinButton->setTouchEnabled(true);
    signinButton->addClickEventListener(CC_CALLBACK_1(InstallPage::menuSigninCallback,this));
    
    page1->addChild(labelUserID);
    page1->addChild(labelCountryCode);
    page1->addChild(countryCodeEditBox);
    page1->addChild(agreementText);
    page1->addChild(signinButton);
    
    pageView->insertPage(page1,1);
////// page 2 //////
    page2 = Layout::create();
    page2->setContentSize(visibleSize);
    page2->setPosition(Vec2::ZERO);
    
    auto logo = Sprite::create("logo.png");
    logo->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height - (visibleSize.height - MainScene::BottomMenuHeight) * 0.5f) );
    
    startButton = Button::create();
    startButton->loadTextures(SigninButtonNormal, "", "");
    startButton->setTitleText(DICTOOL->getStringValue_json(doc, "start_label"));
    startButton->setTitleColor(FontColor);
    startButton->setTitleFontName(Strings::getFontType());
    startButton->setTitleFontSize(TextFontSize);
    startButton->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.3f) );
    startButton->setTouchEnabled(true);
    startButton->addClickEventListener(CC_CALLBACK_1(InstallPage::menuStartCallback,this));
    page2->addChild(logo);
    page2->addChild(startButton);
    
    pageView->insertPage(page2,2);
    
    // end
     if(!initWithColor(BackgroundColor, visibleSize.width, visibleSize.height))
     return false;
    
    loadingSprite = Sprite::create(LoadingNormal);
    loadingSprite->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height * 0.2f));
    loadingSprite->setVisible(false);
    
    this->addChild(pageView);
    this->addChild(loadingSprite);
    
    loadingForever = RepeatForever::create(RotateBy::create(3, 360));
    loadingForever->retain();
    
    scheduleUpdate();
    return true;
}

inline float wave(float x)
{
    return 0.5+0.5*sin(x*2.0);
}

void InstallPage::update(float dt)
{
    _time+=dt/2;
    
    float r = wave(_time*1.0);
    float g = wave(_time*0.25);
    float b = wave(1.0);
    
    _displayedColor.r = _realColor.r = r * 200.0;
    _displayedColor.g = _realColor.g = g * 200.0;
    _displayedColor.b = _realColor.b = b * 200.0;
    _displayedOpacity = _realOpacity = 255;
    
    updateColor();
}

void InstallPage::pageviewCallback(Ref* pSender, PageView::EventType type)
{
}

void InstallPage::agreement(std::string userid)
{
    phoneNumberLoginButton->setEnabled(true);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    googleLoginButton->setEnabled(true);
#endif
    loadingSprite->stopAllActions();
    loadingSprite->setVisible(false);
    if(userid.length() != 0)
    {
        labelUserID->setString(labelID+userid);
        labelCountryCode->setVisible(true);
        countryCodeEditBox->setVisible(true);
        
        pageView->scrollToPage(1);
    }
}

void InstallPage::complete()
{
    UserDefault::getInstance()->setStringForKey("firsttime", "1001");
    signinButton->setEnabled(true);
    loadingSprite->stopAllActions();
    loadingSprite->setVisible(false);
    pageView->scrollToPage(2);
}

void InstallPage::menuPhoneNumberLoginback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    Device::getPhoneNumberWithFablic("CustomPhoneNumber");
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    userID = Device::getPhoneNumber();
    CCLOG("user id : %s", userID.c_str());
    agreement(userID);
#endif
    
}

void InstallPage::menuGoogleLoginback(Ref* pSender)
{
    soomla::CCError *profileError = nullptr;
    soomla::CCSoomlaProfile::getInstance()->login(soomla::GOOGLE, &profileError);
    if(profileError)
        CCLOG("Login error : %s", profileError->getInfo());
    else
        CCLOG("trying to login");
}

void InstallPage::menuSigninCallback(Ref* pSender)
{
    std::string user_id;
    if(userID.length()!=0)
    {
        countryCode = countryCodeEditBox->getText();
        if(countryCode.length()==0)
        {
            return;
        }
        Device::setCountryCode(countryCodeEditBox->getText());
        user_id = IdentityManager::makeUserID(userID);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        Device::requestContactAccess();
        Device::requestMicAccess();
#endif
        CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::SignUp, user_id);
        
        UserDefault::getInstance()->setIntegerForKey("record_type", (int)UIMessage::Record::MP4);
        UserDefault::getInstance()->setIntegerForKey("record_timeout", 15);
        ((Button*)pSender)->setEnabled(false);
        loadingSprite->runAction(loadingForever);
        loadingSprite->setVisible(true);
    }
}

void InstallPage::menuStartCallback(Ref* pSender)
{
    this->removeFromParent();
    mMainScene->removeInstallPage();
    CallStateHandler::getInstance()->Install(mMainScene->isOfflineMode());
}

void InstallPage::menuSkipCallback(Ref* pSender)
{
    UserDefault::getInstance()->setStringForKey("firsttime", "offline");
    mMainScene->setOfflineMode(true);
    pageView->scrollToPage(2);
}

#ifdef COCOS2D_DEBUG
void InstallPage::editBoxEditingDidBegin(cocos2d::ui::EditBox* editBox)
{
    log("editBox %p DidBegin !", editBox);
}

void InstallPage::editBoxEditingDidEnd(cocos2d::ui::EditBox* editBox)
{
    log("editBox %p DidEnd !", editBox);
}

void InstallPage::editBoxTextChanged(cocos2d::ui::EditBox* editBox, const std::string& text)
{
    log("editBox %p TextChanged, text: %s ", editBox, text.c_str());
}

void InstallPage::editBoxReturn(ui::EditBox* editBox)
{
    
}

void InstallPage::menuDebugCallback(Ref* pSender)
{
    std::string input_text =debugEditBox->getText();
    userID = input_text+"@gmail.com";
    agreement(userID);
}
#endif
