//
//  ExitMenu.h
//  BitLive
//
//  Created by keros on 6/14/15.
//
//

#ifndef __BitLive__ExitMenu__
#define __BitLive__ExitMenu__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "extra/Strings.h"
#include "datatype/UIMessage.h"
#include "view/SliderEx.h"
#include "view/ToggleButton.h"
#include "view/MainScene.h"
#include "view/LayerColorExtension.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;

class ExitMenu : public LayerColor, public LayerColorExtension
{
public:
    static Color4B BackgroundColor;
    static Color3B FontColor;
    static int FontSize;
    static int Height;
    static std::string OkButtonNormal;
    static std::string OkButtonSelect;
    static std::string CancelButtonNormal;
    static std::string CancelButtonSelect;
    
    ~ExitMenu();
    static ExitMenu* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    virtual void show(bool isShow);
    virtual cocos2d::Size& getSize();
    virtual Color4B& getBackgroundColor();
    virtual void setBackgroundColor(Color4B& color);
private:
    bool init(MainScene* mainscene);
    void OnOkButtonClick(Ref* pSender);
    void OnCancelButtonClick(Ref* pSender);
    void afterInvisibleAction(Node* sender);
    MainScene* mMainScene;
    ToggleButton* mSelectedButton;
};
#endif /* defined(__BitLive__ExitMenu__) */
