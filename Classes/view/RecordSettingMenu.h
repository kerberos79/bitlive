//
//  RecordSettingMenu.h
//  BitLive
//
//  Created by keros on 9/21/15.
//
//

#ifndef __BitLive__RecordSettingMenu__
#define __BitLive__RecordSettingMenu__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "extra/Strings.h"
#include "datatype/UIMessage.h"
#include "view/SliderEx.h"
#include "view/ToggleButton.h"
#include "view/MainScene.h"
#include "view/LayerColorExtension.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;

class RecordSettingMenu : public LayerColor, public LayerColorExtension
{
public:
    static Color4B TitleBackgroundColor;
    static Color4B BodyBackgroundColor;
    static Color3B TitleFontColor;
    static Color3B BodyFontColor;
    static int TitleFontSize;
    static int BodyFontSize;
    static int Height;
    static int TitleHeight;
    static Color3B ButtonFontColor;
    static int ButtonFontSize;
    static std::string CloseButton;
    static std::string JoinImage;
    static std::string TimerImage;
    static std::string RadioButtonNormal;
    static std::string RadioButtonSelect;
    
    ~RecordSettingMenu();
    static RecordSettingMenu* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    virtual void show(bool isShow);
    virtual cocos2d::Size& getSize();
    virtual Color4B& getBackgroundColor();
    virtual void setBackgroundColor(Color4B& color);
private:
    bool init(MainScene* mainscene);
    void menuCloseCallback(Ref* pSender);
    void menuJoinCallback(Ref* pSender);
    void menuTimeoutButton1Callback(Ref* pSender);
    void menuTimeoutButton2Callback(Ref* pSender);
    void menuTimeoutButton3Callback(Ref* pSender);
    void afterInvisibleAction(Node* sender);
    MainScene* mMainScene;
    ToggleButton* mTimeoutButton1;
    ToggleButton* mTimeoutButton2;
    ToggleButton* mTimeoutButton3;
};
#endif /* defined(__BitLive__RecordSettingMenu__) */
