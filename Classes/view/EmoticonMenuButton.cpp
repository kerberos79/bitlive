//
//  EmoticonMenuButton.cpp
//  AnicameraMAC
//
//  Created by kerberos on 11/17/14.
//
//

#include "EmoticonMenuButton.h"


EmoticonMenuButton::~EmoticonMenuButton()
{
    
}

EmoticonMenuButton* EmoticonMenuButton::create()
{
    EmoticonMenuButton* widget = new (std::nothrow) EmoticonMenuButton();
    if (widget && widget->init())
    {
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

bool EmoticonMenuButton::init()
{
    if (Widget::init())
    {
        return true;
    }
    return false;
}

void EmoticonMenuButton::setResource(const std::string &resource)
{
    _resource = resource;
}

std::string& EmoticonMenuButton::getResource()
{
    return _resource;
}

void EmoticonMenuButton::setResourcePosition(const float x, const float y)
{
    _resourcePos.x = x;
    _resourcePos.y = y;
}

Vec2 EmoticonMenuButton::getResourcePosition()
{
    return _resourcePos;
}