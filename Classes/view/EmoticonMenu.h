//
//  EmoticonMenu.h
//  BitLive
//
//  Created by keros on 7/31/15.
//
//

#ifndef __BitLive__EmoticonMenu__
#define __BitLive__EmoticonMenu__

#include <stdio.h>
#include <string>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "extra/Strings.h"
#include "datatype/UIMessage.h"
#include "view/SliderEx.h"
#include "view/ToggleButton.h"
#include "view/MainScene.h"
#include "view/PageViewEx.h"
#include "view/LayerColorExtension.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;

class EmoticonMenu : public LayerColor, public LayerColorExtension
{
public:
    static Color4B BackgroundColor;
    static Color3B FontColorNormal;
    static Color3B FontColorSelect;
    static std::string FontType;
    static int FontSize;
    static int Height;
    static int ItemWidth;
    static int ItemHeight;
    static std::string CloseButton;
    static std::string RoundButtonNormal;
    static std::string RoundButtonSelect;
    static std::string TabButtonNormal;
    static std::string TabButtonSelect;
    
    ~EmoticonMenu();
    static EmoticonMenu* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    virtual void show(bool isShow);
    virtual cocos2d::Size& getSize();
    virtual Color4B& getBackgroundColor();
    virtual void setBackgroundColor(Color4B& color);
private:
    bool init(MainScene* mainscene);
    void menuEmoticonTabCallback(Ref* pSender);
    void menuStickerTabCallback(Ref* pSender);
    void menuNoStickerTabCallback(Ref* pSender);
    void menuSetStickerCallback(Ref* pSender);
    void menuItemCallback(Ref* pSender);
    void menuCloseCallback(Ref* pSender);
    void afterInvisibleAction(Node* sender);
    void showStickerMenu();
    
    MainScene* mMainScene;
    ui::ScrollView *_emoticonScrollView, *_stickerScrollView;
    ToggleButton *_emoticonTabButton, *_stickerTabButton;
    ToggleButton *_currentTabButton;
    Button *_selectedStickerButton;
};

#endif /* defined(__BitLive__EmoticonMenu__) */
