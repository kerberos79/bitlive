//
//  MainScene.cpp
//  Vist
//
//  Created by kerberos on 1/17/15.
//
//

#include "MainScene.h"

Color4B MainScene::BackgroundColor;
int MainScene::SmallPreviewSize;
int MainScene::BottomMenuHeight;
int MainScene::SubMenuHeight;
int MainScene::CameraWidth;
int MainScene::CameraHeight;
int MainScene::PreviewWidth;
int MainScene::PreviewHeight;
int MainScene::OriginDecodeWidth;
int MainScene::OriginDecodeHeight;
float MainScene::LayoutScale;

Scene* MainScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MainScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

MainScene::~MainScene()
{
    
}

bool MainScene::loadLayoutFromJson()
{
    rapidjson::Document doc;
    mLayerSize = Director::getInstance()->getVisibleSize();
    // calcuate a layout size for slide menu
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("layout.json");
    
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    
    if(mLayerSize.width==1536 && mLayerSize.height==2048)
        MainScene::LayoutScale = 1.0f;
    else if(mLayerSize.width==768 && mLayerSize.height==1024)
        MainScene::LayoutScale = 720.0f/1080.0f;
    else
        MainScene::LayoutScale = mLayerSize.width / 1080.0f;
    
    MainScene::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(doc, "background_color"));
    MainScene::SmallPreviewSize = DICTOOL->getIntValue_json(doc, "small_preview_size") * MainScene::LayoutScale;
    MainScene::BottomMenuHeight = DICTOOL->getIntValue_json(doc, "bottom_height") * MainScene::LayoutScale;
    int previewSize = DICTOOL->getIntValue_json(doc, "preview_size");
    if(previewSize == 480)
    {
        OriginDecodeWidth = 544;
        OriginDecodeHeight = 640;
        CameraWidth = 640;
        CameraHeight = 480;
        PreviewWidth = 480;
        PreviewHeight = 640;
    }
    else
    {
        OriginDecodeWidth = 1280 + 128;
        OriginDecodeHeight = 720 + 240;
        CameraWidth = 1280;
        CameraHeight = 720;
        PreviewWidth = 1280;
        PreviewHeight = 720;
    }
    
    HangupMenu::loadLayout(doc);
    ExitMenu::loadLayout(doc);
    SettingMenu::loadLayout(doc);
    RecordSettingMenu::loadLayout(doc);
    MainMenu::loadLayout(doc);
    EffectMenu::loadLayout(doc);
    EmoticonMenu::loadLayout(doc);
    InviteDialog::loadLayout(doc);
    WarningDialog::loadLayout(doc);
    ContactDialog::loadLayout(doc);
    LicenseDialog::loadLayout(doc);
    Toast::loadLayout(doc);
    Alert::loadLayout(doc);
    CaptureMenu::loadLayout(doc);
    InstallPage::loadLayout(doc);
    WebBrowser::loadLayout(doc);
    ViewFinder::loadLayout(doc);
    return true;
}

// on "init" you need to initialize your instance
bool MainScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    if(!loadLayoutFromJson())
        return false;
    
    mSelectedLayer = nullptr;
    mWebBrowser = nullptr;
    mEffectMenu = nullptr;
    mEmoticonMenu = nullptr;
    mSettingMenu = nullptr;
    mRecordSettingMenu = nullptr;
    mHangupMenu = nullptr;
    mExitMenu = nullptr;
    mInviteDialog = nullptr;
    mWarningDialog = nullptr;
    mLicenseDialog = nullptr;
    mOfflineMode = false;
    
    createPreviewScreen();
    createDecodeScreen();
    P2PStreamer::getInstance()->InitScreen(this, PreviewWidth, PreviewHeight);
    
    createSubMenu();
    createContactDialog();
    createToast();
    createCustomEvent();
    ChangeState(MainState::Idle);
    
    CallStateHandler::getInstance()->UIMessageCallback = CC_CALLBACK_1(MainScene::sendUIMessage, this);
    CallStateHandler::getInstance()->Init();
    
    std::string firsttime = UserDefault::getInstance()->getStringForKey("firsttime");
    if(firsttime.length()!=0)
    {
        if(firsttime.compare("offline"))
        {
            CallStateHandler::getInstance()->Start();
        }
        else
        {
            setOfflineMode(true);
            ChangeState(MainState::Offline);
        }
        mInstallScreen = nullptr;
    }
    else
    {
        createInstallPage();
        ChangeState(MainState::Install);
    }
    Device::setKeepScreenOn(true);
    
    mCurrentRecordType = (UIMessage::Record)UserDefault::getInstance()->getIntegerForKey("record_type", 0);
    /*
    Device::setAccelerometerEnabled(true);
    auto listener = EventListenerAcceleration::create(CC_CALLBACK_2(MainScene::onAcceleration, this));
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
     */
    if ( !LayerColor::initWithColor(MainScene::BackgroundColor)) {
        return false;
    }
    return true;
}

void MainScene::onAcceleration(Acceleration *acc, cocos2d::Event *event)
{
    if(acc->y<-0.8)
    {
        if(mCurrentDirection!=DEGREE0)
        {
            rotateButton(DEGREE0);
            mCurrentDirection = DEGREE0;
        }
    }
    else if(acc->y<0.02 && acc->y>-0.02)
    {
        if(acc->x>0.8)
        {
            if(mCurrentDirection!=DEGREE270)
            {
                rotateButton(DEGREE270);
                mCurrentDirection = DEGREE270;
            }
        }
        else if(acc->x<-0.08)
        {
            if(mCurrentDirection!=DEGREE90)
            {
                rotateButton(DEGREE90);
                mCurrentDirection = DEGREE90;
            }
        }
    }
    
}

void MainScene::createCustomEvent()
{
    auto listener = EventListenerCustom::create("Event", [=](EventCustom* event){
        
        UIMessage* userData = static_cast<UIMessage*>(event->getUserData());
        switch(userData->what)
        {
            case UIMessage::Cmd::Install:
            {
                if(userData->arg1 == (int)UIMessage::Install::Start)
                    ChangeState(MainState::Install);
                else if(userData->arg1 == (int)UIMessage::Install::Skip)
                {
//                    ChangeState(MainState::Enter);
                }
                else if(userData->arg1 == (int)UIMessage::Install::Complete)
                {
                    if(mInstallScreen)
                        mInstallScreen->complete();
                }
                break;
            }
            case UIMessage::Cmd::ContactList:
            {
                if(userData->bundle)
                {
                    std::vector<std::string>* list = userData->bundle;
                    if(list->size()>2)
                    {
                        updateContactDialog(userData->bundle);
                        
                        updateInviteDialog(InviteEventData::Type::Send, (ContactInfo*)userData->extra);
                    }
                    else
                    {
                        CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::Invite, list->at(1));
                        
                        showInviteDialog(InviteEventData::Type::Send, (ContactInfo*)userData->extra);
                    }
                }
                break;
            }
            case UIMessage::Cmd::Login:
            {
                if(userData->flag)
                {
                    ChangeState(MainState::Enter);
                }
                else
                {
                    showWarningDialog(UIMessage::Cmd::Login);
                }
                break;
            }
            case UIMessage::Cmd::Invite:
            {
                switch((InviteEventData::Type)userData->arg1)
                {
                    case InviteEventData::Type::Send:
                    {
                        showInviteDialog(InviteEventData::Type::Send, (ContactInfo*)userData->extra);
                        break;
                    }
                    case InviteEventData::Type::Recv:
                    {
                        if(mMainMenu->isRecording())
                        {
                            ContactInfo* info = (ContactInfo*)userData->extra;
                            showToast(UIMessage::Toast::Call, info->_name);
                            
                            if(mInviteDialog)
                                mInviteDialog->setContactInfo((ContactInfo*)userData->extra);
                        }
                        else
                        {
                            ChangeState(MainState::Main);
                            showInviteDialog(InviteEventData::Type::Recv, (ContactInfo*)userData->extra);
                        }
                        break;
                    }
                    case InviteEventData::Type::Refusal:
                    {
                        showInviteDialog(InviteEventData::Type::Refusal);
                        break;
                    }
                    case InviteEventData::Type::Cancel:
                    {
                        showInviteDialog(InviteEventData::Type::Cancel);
                        break;
                    }
                    case InviteEventData::Type::Busy:
                    {
                        showInviteDialog(InviteEventData::Type::Busy);
                        break;
                    }
                    case InviteEventData::Type::Connecting:
                    {
                        showInviteDialog(InviteEventData::Type::Connecting);
                        break;
                    }
                    case InviteEventData::Type::Error:
                    {
                        showInviteDialog(InviteEventData::Type::Error);
                        break;
                    }
                    case InviteEventData::Type::Message:
                    {
                        break;
                    }
                }
                break;
            }
            case UIMessage::Cmd::Connect:
            {
                showInviteDialog(InviteEventData::Type::Connect, nullptr);
                if(userData->data.length()>0)
                {
                    rapidjson::Document doc;
                    doc.Parse<0>(userData->data.c_str());
                    if (doc.HasParseError())
                    {
                        CCLOG("GetParseError %s\n", doc.GetParseError());
                        break;
                    }
                    const char* effect = DICTOOL->getStringValue_json(doc, "effect");
                    onSetDecodeEffect(effect);
                    ChangeState(MainState::Calling);
                    CCLOG("UIMessage::Cmd::Connect true");
                }
                else
                {
                    showWarningDialog(UIMessage::Cmd::Connect);
                }
                break;
            }
            case UIMessage::Cmd::Disconnect:
            {
                ChangeState(MainState::Main);
                break;
            }
            case UIMessage::Cmd::NotFountNetwork:
            {
                setOfflineMode(true);
                ChangeState(MainState::Offline);
                break;
            }
            case UIMessage::Cmd::FlippedCamera:
            {
                mPreviewScreen->flipCameraPreview();
                if(mCurrentState==MainState::Calling)
                    CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::SendRotateCommand, mPreviewScreen->getDegree());
                break;
            }
            case UIMessage::Cmd::DecodeEffect:
            {
                onSetDecodeEffect(userData->data);
//                mDecodeScreen->setGLProgram(userData->data);
                break;
            }
            case UIMessage::Cmd::DecodeEmoticon:
            {
                mDecodeScreen->setEmoticon(userData->arg1);
                break;
            }
            case UIMessage::Cmd::DecodeSticker:
            {
                mDecodeScreen->setSticker(userData->data);
                break;
            }
            case UIMessage::Cmd::DecodeRotate:
            {
                mDecodeScreen->rotate(userData->data);
                break;
            }
            case UIMessage::Cmd::EncodingBitrate:
            {
                mPreviewScreen->setBitrate(userData->arg1);
                break;
            }
            case UIMessage::Cmd::LinkURL:
            {
                showToast(UIMessage::Toast::LinkURL, userData->data);
                break;
            }
            case UIMessage::Cmd::DeleteAccount:
            {
                if(userData->flag)
                {
                    ChangeState(MainState::Exit);
                    showWarningDialog(UIMessage::Cmd::DeleteAccount);
                }
                else
                {
                }
                break;
            }
            case UIMessage::Cmd::FaceDetect:
            {
                mPreviewScreen->updateFaceRect(userData->arg1, userData->arg2, userData->arg3, userData->arg4);
                break;
            }
            case UIMessage::Cmd::FinishCaptureGIF:
            {
                if(mCaptureMenu)
                    mCaptureMenu->updateGif(mPreviewScreen->getCaptureFileName(), PreviewWidth, PreviewHeight);
                break;
            }
            case UIMessage::Cmd::FinishRecord:
            {
                if(mCaptureMenu)
                    mCaptureMenu->updateMP4(userData->flag);
                break;
            }
        }
        delete userData;
        
    });
    _eventDispatcher->addEventListenerWithFixedPriority(listener, 1);
}

void MainScene::sendUIMessage(UIMessage* message)
{
    EventCustom event("Event");
    event.setUserData((void*)message);
    _eventDispatcher->dispatchEvent(&event);
}

void MainScene::createDecodeScreen()
{
    mDecodeScreen = DecodeScreen::create(this, OriginDecodeWidth, OriginDecodeHeight, PreviewWidth, PreviewHeight);
    mDecodeScreen->fitOnScreen(mLayerSize);
    mDecodeScreen->addEmoticonLayer();
    mDecodeScreen->initGLProgram();
    mDecodeScreen->setVisible(false);
    this->addChild(mDecodeScreen, ZORDER_DECODE_SCREEN);
}

void MainScene::createPreviewScreen()
{
    mPreviewScreen = PreviewScreen::create(this, PreviewWidth, PreviewHeight);
    mPreviewScreen->fitOnScreen(mLayerSize);
    mPreviewScreen->addEmoticonLayer();
    mPreviewScreen->initGLProgram();
    this->addChild(mPreviewScreen, ZORDER_PREVIEW_SCREEN);
    
    MainScene::SubMenuHeight = mLayerSize.height - mPreviewScreen->getFullScreenHeight();
}

void MainScene::createSubMenu()
{
    mMainMenu = MainMenu::create(this);
    mMainMenu->setVisible(false);
    mMainMenu->setPosition(Vec2::ZERO);
    this->addChild(mMainMenu, ZORDER_MAINMENU_LAYER);
    
    mExitMenu = ExitMenu::create(this);
    mExitMenu->setVisible(false);
    this->addChild(mExitMenu, ZORDER_SUBMENU_LAYER);
    
    mHangupMenu = HangupMenu::create(this);
    mHangupMenu->setVisible(false);
    this->addChild(mHangupMenu, ZORDER_SUBMENU_LAYER);
    
    mEffectMenu = EffectMenu::create(this);
    mEffectMenu->setVisible(false);
    this->addChild(mEffectMenu, ZORDER_SUBMENU_LAYER);
    
    mEmoticonMenu = EmoticonMenu::create(this);
    mEmoticonMenu->setVisible(false);
    this->addChild(mEmoticonMenu, ZORDER_SUBMENU_LAYER);
    
    mViewFinder = ViewFinder::create(this);
    mViewFinder->setVisible(false);
    this->addChild(mViewFinder, ZORDER_VIEW_FINDER);
}

void MainScene::ChangeState(MainState state)
{
    switch(state)
    {
        case MainState::Idle:
        {
            if(mInstallScreen)
                mInstallScreen->setVisible(false);
            mPreviewScreen->setBlurEffect(true);
            mMainMenu->setState(MainState::Idle);
            mPreviewScreen->setVisible(true);
            break;
        }
        case MainState::Install:
        {
            if(mInstallScreen)
                mInstallScreen->setVisible(true);
            mPreviewScreen->setBlurEffect(true);
            mMainMenu->setState(MainState::Install);
            mPreviewScreen->setVisible(true);
            mViewFinder->setVisible(false);
            break;
        }
        case MainState::Enter:
        {
            mMainMenu->setState(MainState::Enter);
            mPreviewScreen->setBlurEffect(false);
            mPreviewScreen->setLocalZOrder(ZORDER_PREVIEW_SCREEN);
            mPreviewScreen->enterFullScreen();
            mPreviewScreen->setVisible(true);
            mDecodeScreen->setVisible(false);
            mViewFinder->setVisible(false);
            break;
        }
        case MainState::Offline:
        {
            mMainMenu->setState(MainState::Recording);
            mPreviewScreen->setBlurEffect(false);
            mPreviewScreen->setLocalZOrder(ZORDER_PREVIEW_SCREEN);
            mPreviewScreen->enterFullScreen();
            mPreviewScreen->setWaterMark(true);
            mPreviewScreen->setVisible(true);
            mDecodeScreen->setVisible(false);
            mViewFinder->setVisible(true);
            break;
        }
        case MainState::Main:
        {
            if(mSelectedLayer)
            {
                mSelectedLayer->show(false);
                mSelectedLayer = nullptr;
            }
            mMainMenu->setState(MainState::Main);
            mPreviewScreen->setLocalZOrder(ZORDER_PREVIEW_SCREEN);
            mPreviewScreen->enterFullScreen();
            mPreviewScreen->setWaterMark(false);
            mPreviewScreen->setVisible(true);
            mDecodeScreen->setVisible(false);
            mViewFinder->setVisible(false);
            break;
        }
        case MainState::Calling:
        {
            mMainMenu->setState(MainState::Calling);
            mPreviewScreen->setLocalZOrder(ZORDER_PREVIEW_SCREEN);
            mPreviewScreen->enterCallScreen();
            mDecodeScreen->setLocalZOrder(ZORDER_DECODE_SCREEN);
            mDecodeScreen->enterFullScreen();
            mDecodeScreen->setVisible(true);
            break;
        }
        case MainState::Recording:
        {
            mMainMenu->setState(MainState::Recording);
            mViewFinder->setVisible(true);
            mPreviewScreen->setWaterMark(true);
            break;
        }
        case MainState::Exit:
        {
            if(mSelectedLayer)
                mSelectedLayer->show(false);
            mPreviewScreen->setBlurEffect(true);
            mMainMenu->setState(MainState::Exit);
            break;
        }
        case MainState::Release:
        {
            if(mSelectedLayer)
                mSelectedLayer->show(false);
            
            auto background = LayerColor::create(Color4B::BLACK, mLayerSize.width, mLayerSize.height);
            background->setPosition(Vec2::ZERO);
            auto logo = Sprite::create("logo.png");
            logo->setPosition(mLayerSize/2);
            background->addChild(logo);
            this->addChild(background, ZORDER_LOGO);
            mMainMenu->setState(MainState::Exit);
            break;
        }
    }
    mCurrentState = state;
}

bool MainScene::isCallingState()
{
    return (mCurrentState==MainState::Calling);
}

bool MainScene::isRecordingState()
{
	if(mCaptureMenu!=nullptr && mCaptureMenu->isVisible())
		return false;
    return mMainMenu->isRecordingState();
}

void MainScene::createInstallPage()
{
    if(mInstallScreen == nullptr)
    {
        mInstallScreen = InstallPage::create(this);
        mInstallScreen->setVisible(false);
        this->addChild(mInstallScreen, ZORDER_INSTALL_LAYER);
    }
}

void MainScene::showInstallPage()
{
    if(mInstallScreen)
        mInstallScreen->setVisible(true);
}

void MainScene::removeInstallPage()
{
    if(mInstallScreen) {
        mInstallScreen = nullptr;
    }
}

void MainScene::updateInviteDialog(InviteEventData::Type type, ContactInfo* info)
{
    if(mInviteDialog==nullptr)
    {
        mInviteDialog = InviteDialog::create(this);
        mInviteDialog->setVisible(false);
        this->addChild(mInviteDialog, ZORDER_SUBMENU_LAYER);
    }
    mInviteDialog->setContactInfo(info);
}

void MainScene::showInviteDialog(InviteEventData::Type type, ContactInfo* info)
{
    if(mInviteDialog==nullptr)
    {
        mInviteDialog = InviteDialog::create(this);
        this->addChild(mInviteDialog, ZORDER_SUBMENU_LAYER);
    }
    mInviteDialog->show(type, info);
    mMainMenu->show(false);
    
    if(mSelectedLayer != nullptr && mSelectedLayer != mInviteDialog)
        mSelectedLayer->show(false);
    
    mSelectedLayer = mInviteDialog;
}

bool MainScene::showInviteDialog()
{
    if(mMainMenu->isRecording())
        return false;
    mMainMenu->setState(MainState::Main);
    toggleMenuCallback(mInviteDialog);
    return true;
}

void MainScene::hideInviteDialog(InviteEventData::Type type)
{
	if(mInviteDialog)
    {
        mInviteDialog->show(false);
        mMainMenu->show(true);
    }
    
    mSelectedLayer = nullptr;
}

void MainScene::createContactDialog()
{
    mInviteDialog = InviteDialog::create(this);
    mInviteDialog->setVisible(false);
    this->addChild(mInviteDialog, ZORDER_SUBMENU_LAYER);
    
    mWarningDialog = WarningDialog::create(this, UIMessage::Cmd::NotFountNetwork);
    mWarningDialog->setVisible(false);
    this->addChild(mWarningDialog, ZORDER_DIALOG_LAYER, dialogTag);
}

void MainScene::updateContactDialog(std::vector<std::string>* list)
{
    mContactDialog = ContactDialog::create(this, list);
    mContactDialog->setVisible(true);
    this->addChild(mContactDialog, ZORDER_DIALOG_LAYER, dialogTag);
}

void MainScene::choiceItemCallback(Ref* pSender)
{
    Button* button = (Button*)pSender;
    std::string userID = button->getTitleText();
    
    if(userID.length()==0)
        return;

    CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::Invite, userID);
    mContactDialog->show(false);
    
    showInviteDialog(InviteEventData::Type::Send, mInviteDialog->getContactInfo());
}

void MainScene::menuRotateCallback(Ref* pSender)
{
    Device::flipCameraPreview();
    P2PStreamer::getInstance()->RequestEncoder(P2PStreamer::EncReqType::FlipCamera);
}

void MainScene::toggleMenuCallback(LayerColorExtension* layer)
{
    if(mSelectedLayer==nullptr)
    {
        layer->show(true);
        mSelectedLayer = layer;
        mMainMenu->show(false);
    }
    else if(mSelectedLayer == layer)
    {
        mSelectedLayer->show(false);
        mSelectedLayer = nullptr;
        mMainMenu->show(true);
    }
    else
    {
        mSelectedLayer->show(false);
        mSelectedLayer = layer;
        mSelectedLayer->show(true);
        mMainMenu->show(false);
    }
}

void MainScene::toggleMainMenu(cocos2d::Point& p)
{
    if(mMainMenu->isVisible())
    {
        if(!mToast->_isToastHidden)
        {
            if(p.y > (mLayerSize.height - mToast->getContentSize().height))
            {
                return;
            }
            
        }
        if(mSelectedLayer)
        {
            if(p.y > (mSelectedLayer->getSize().height + mMainMenu->getContentSize().height))
            {
                mSelectedLayer->show(false);
                mMainMenu->setVisible(false);
            }
        }
        else
        {
            if(p.y > mMainMenu->getContentSize().height)
            {
                mMainMenu->setVisible(false);
            }
        }
    }
    else
    {
        if(!mToast->_isToastHidden)
        {
            if(p.y > (mLayerSize.height - mToast->getContentSize().height))
            {
                return;
            }
            
        }
        if(mSelectedLayer)
        {
            mSelectedLayer->show(true);
        }
        else
        {
        }
        mMainMenu->setVisible(true);
    }
}

void MainScene::menuEffectCallback(Ref* pSender)
{
    toggleMenuCallback(mEffectMenu);
}

void MainScene::menuEmoticonCallback(Ref* pSender)
{
    toggleMenuCallback(mEmoticonMenu);
}

void MainScene::menuSettingCallback(Ref* pSender)
{
    if(mSettingMenu==nullptr)
    {
        mSettingMenu = SettingMenu::create(this);
        this->addChild(mSettingMenu, ZORDER_SUBMENU_LAYER);
    }
    toggleMenuCallback(mSettingMenu);
}

void MainScene::menuRecordSettingCallback(Ref* pSender)
{
    if(mRecordSettingMenu==nullptr)
    {
        mRecordSettingMenu = RecordSettingMenu::create(this);
        this->addChild(mRecordSettingMenu, ZORDER_SUBMENU_LAYER);
    }
    toggleMenuCallback(mRecordSettingMenu);
}

void MainScene::menuContractCallback(Ref* pSender)
{
    if(mSelectedLayer!=nullptr)
    {
        mSelectedLayer->show(false);
        mSelectedLayer = nullptr;
        mMainMenu->show(true);
    }
    CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::RequestContactList);
}

void MainScene::menuCallCallback(Ref* pSender)
{
    toggleMenuCallback(mInviteDialog);
}

void MainScene::menuWebCallback(Ref* pSender)
{
    createWebBrowser();
    mWebBrowser->show(true);
}

void MainScene::menuExitCallback(Ref* pSender)
{
    toggleMenuCallback(mExitMenu);
}

void MainScene::menuWarningCallback(Ref* pSender)
{
    CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Release);
}

void MainScene::menuSendLinkURLCallback(std::string url)
{
    CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::SendLinkURLCommand, url);
}

void MainScene::menuCaptureCallback(Ref* pSender)
{
    mPreviewScreen->captureJpeg();
}

void MainScene::menuRecordCallback(Ref* pSender)
{
    ChangeState(MainState::Recording);
}

bool MainScene::isVideoPlayerPlaying()
{
    if(mCaptureMenu==nullptr || !mCaptureMenu->isVisible())
        return false;
    return mCaptureMenu->isPlaying();
}

void MainScene::OnEnableRecording(bool enable)
{
    switch(mCurrentRecordType)
    {
        case UIMessage::Record::MP4:
        {
            if(enable)
            {
                mPreviewScreen->enableRecord(true);
            }
            else
            {
                CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::StopRecording);
                mPreviewScreen->enableRecord(false);
                
                if(mCaptureMenu==nullptr)
                {
                    mCaptureMenu = CaptureMenu::create(this);
                    mCaptureMenu->setPosition(Vec2::ZERO);
                    this->addChild(mCaptureMenu, ZORDER_DIALOG_LAYER);
                }
                mCaptureMenu->updateMediaType(CaptureMenu::MediaType::Movie);
                toggleMenuCallback(mCaptureMenu);
            }
            break;
        }
        case UIMessage::Record::GIF:
        {
            if(enable)
            {
                mPreviewScreen->captureGif(true);
                CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::StartRecordingGIF, (const char*)mPreviewScreen->getCaptureFileName());
            }
            else
            {
                CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::StopRecordingGIF);
                mPreviewScreen->captureGif(false);
                if(mCaptureMenu==nullptr)
                {
                    mCaptureMenu = CaptureMenu::create(this);
                    mCaptureMenu->setPosition(Vec2::ZERO);
                    this->addChild(mCaptureMenu, ZORDER_DIALOG_LAYER);
                }
                mCaptureMenu->updateMediaType(CaptureMenu::MediaType::Gif);
                toggleMenuCallback(mCaptureMenu);
            }
            break;
        }
    }
    
    mViewFinder->start(enable);
}

void MainScene::afterRecordFirstFrame(std::string filename)
{
    CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::StartRecording, filename);
    if(mCaptureMenu==nullptr)
    {
        mCaptureMenu = CaptureMenu::create(this);
        mCaptureMenu->setPosition(Vec2::ZERO);
        mCaptureMenu->setVisible(false);
        this->addChild(mCaptureMenu, ZORDER_DIALOG_LAYER);
    }
    mCaptureMenu->updateMediaType(CaptureMenu::MediaType::Movie);
    mCaptureMenu->setImage(mPreviewScreen->_captureImage, filename, mPreviewScreen->getScaledSize());
}

void MainScene::OnExit()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    ChangeState(MainState::Release);
#endif
    CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Release);
    if(mPreviewScreen)
        mPreviewScreen->backupLastEffect();
}

void MainScene::menuHangupCallback(Ref* pSender)
{
    toggleMenuCallback(mHangupMenu);
}

void MainScene::OnHangup()
{
    ChangeState(MainState::Main);
    CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::Disconnect);
}

void MainScene::OnSignout()
{
    CallStateHandler::getInstance()->SendEmptyMessage(CallStateHandler::Cmd::DeleteAccount);
}

void MainScene::OnJoin()
{
    setOfflineMode(false);
    UserDefault::getInstance()->setStringForKey("firsttime", "");
    createInstallPage();
    ChangeState(MainState::Install);
//    showWarningDialog(UIMessage::Cmd::Join);
}

void MainScene::OnChangeScreenLayout()
{
    if(mPreviewScreen->isSmallScreen())
    {
        mPreviewScreen->enterFullScreen();
        mPreviewScreen->setLocalZOrder(ZORDER_DECODE_SCREEN);
        
        mDecodeScreen->setSmallScreenSector(mPreviewScreen->getSmallScreenSector());
        mDecodeScreen->enterSmallScreen();
        mDecodeScreen->setLocalZOrder(ZORDER_PREVIEW_SCREEN);
    }
    else
    {
        
        mDecodeScreen->enterFullScreen();
        mDecodeScreen->setLocalZOrder(ZORDER_DECODE_SCREEN);
        
        mPreviewScreen->setSmallScreenSector(mDecodeScreen->getSmallScreenSector());
        mPreviewScreen->enterSmallScreen();
        mPreviewScreen->setLocalZOrder(ZORDER_PREVIEW_SCREEN);
    }
}

void MainScene::menuSetEffectCallback(std::string program)
{
    mPreviewScreen->setProgram(program);
    if(mCurrentState==MainState::Calling)
        CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::SendEffectCommand, program);
}

void MainScene::showWarningDialog(UIMessage::Cmd type)
{
    if(mWarningDialog==nullptr)
    {
        mWarningDialog = WarningDialog::create(this, UIMessage::Cmd::NotFountNetwork);
        this->addChild(mWarningDialog, ZORDER_DIALOG_LAYER, dialogTag);
    }
    mWarningDialog->show(type);
}

void MainScene::showAlert(std::string message, UIMessage::Alert type)
{
    if(mAlert == nullptr)
    {
        mAlert = Alert::create(this);
        this->addChild(mAlert, ZORDER_TOAST);
    }
    mAlert->show(message, type);
}

void MainScene::createLicenseDialog()
{
    if(mLicenseDialog == nullptr)
    {
        mLicenseDialog = LicenseDialog::create(this);
        mLicenseDialog->setVisible(false);
        this->addChild(mLicenseDialog, ZORDER_DIALOG_LAYER, dialogTag);
    }
}

void MainScene::OnLicenseDialogView()
{
    createLicenseDialog();
    mLicenseDialog->setVisible(true);
}

void MainScene::createWebBrowser()
{
    if(mWebBrowser==nullptr)
    {
        mWebBrowser = WebBrowser::create(this);
        mWebBrowser->setVisible(false);
        this->addChild(mWebBrowser, ZORDER_DIALOG_LAYER);
    }
}

void MainScene::showWebBrowser(std::string url)
{
    createWebBrowser();
    mWebBrowser->show(url);
}

void MainScene::createToast()
{
    mToast = Toast::create(this);
    this->addChild(mToast, ZORDER_TOAST);
}

void MainScene::showToast(UIMessage::Toast type, std::string message)
{
    mToast->show(type, message);
}

void MainScene::menuSetEmoticonCallback(Ref* pSender)
{
    Button *button = (Button *)pSender;
    
    mPreviewScreen->setEmoticon(button->getTag());
    
    if(mCurrentState==MainState::Calling)
        CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::SendEmoticonCommand, button->getTag());
}


void MainScene::menuSetStickerCallback(Ref* pSender)
{
    Button *button = (Button *)pSender;
    
    mPreviewScreen->setSticker(button->getTag());
}

void MainScene::update(float dt)
{
    mDecodeScreen->setProgram(mDecodeScreen->getReservedProgram());
    this->unscheduleUpdate();
}

void MainScene::onSetDecodeEffect(std::string name)
{
    mDecodeScreen->reserveProgram(name);
    this->scheduleUpdate();
}

void MainScene::rotateButton(int newRotation)
{
/*
    if(mBottomPanel->isVisible())
    {
        for(auto& item : mBottomButtonList)
        {
            auto action = RotateTo::create(ROTATE_BUTTON_DURATION , newRotation);
            item->runAction( Sequence::create(action, nullptr, nullptr));
        }
    }
    else
    {
        for(auto& item : mBottomButtonList)
        {
            item->setRotation(newRotation);
        }
    }
*/
}

void MainScene::afterCaptureCallback(RenderTexture* texture, const std::string& filename)
{
    if(mCaptureMenu==nullptr)
    {
        mCaptureMenu = CaptureMenu::create(this);
        mCaptureMenu->setPosition(Vec2::ZERO);
        this->addChild(mCaptureMenu, ZORDER_DIALOG_LAYER);
    }
    mCaptureMenu->updateMediaType(CaptureMenu::MediaType::Jpeg);
    mCaptureMenu->setImage(mPreviewScreen->_captureImage, filename, mPreviewScreen->getScaledSize());
    toggleMenuCallback(mCaptureMenu);
}

void MainScene::OnFaceDetectCallback(Vec2& left, Vec2& right)
{
    UIMessage* message = new UIMessage(UIMessage::Cmd::FaceDetect);
    message->arg1 = left.x;
    message->arg2 = left.y;
    message->arg3 = right.x;
    message->arg4 = right.y;
    sendUIMessage(message);
}

void MainScene::OnCompleteRecordCallback(bool result)
{
    UIMessage* message = new UIMessage(UIMessage::Cmd::FinishRecord);
    message->flag = result;
    sendUIMessage(message);
}

void MainScene::ChangeRecordType(UIMessage::Record type)
{
    mCurrentRecordType = type;
}

void MainScene::ChangeRecordTimeout(int timeout)
{
    mMainMenu->setTimeout(timeout);
}

void MainScene::setOfflineMode(bool enable)
{
    mOfflineMode = enable;
    
}

void MainScene::resetViewFinder()
{
    mViewFinder->resetCount();
}
