//
//  Toast.h
//  BitLive
//
//  Created by kerberos on 7/28/15.
//
//

#ifndef __BitLive__Toast__
#define __BitLive__Toast__

#include <stdio.h>
#include <list>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Cocos2dxProfile.h"
#include "view/MainScene.h"
#include "view/LayerColorExtension.h"
#include "datatype/UIMessage.h"
#include "util/HexString.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;

class MainScene;
class ToastMessage
{
public:
    ToastMessage(UIMessage::Toast type, std::string message)
    {
        _type = type;
        _message = message;
    }
    
    UIMessage::Toast _type;
    std::string _message;
};

class Toast : public LayerColor
{
public:
    ~Toast();
    static Color4B BackgroundColor;
    static Color3B FontColor;
    static int FontSize;
    static int FontHeight;
    static int Margin;
    static int Height;
    static std::string LinkSprite;
    static std::string CallSprite;
    static std::string CancelButtonNormal;
    
    static Toast* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    void show(UIMessage::Toast type, std::string message);
    void hide();
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    bool    _isToastHidden;
private:
    bool init(MainScene* mainscene);
    void menuCloseCallback(Ref* pSender);
    virtual void update(float dt);
    MainScene* mMainScene;
    LabelTTF *_titleText;
    Sprite* _iconLink;
    Sprite* _iconCall;
    UIMessage::Toast _type;
    std::list<ToastMessage*> _messageList;
    Vec2    _showPosition, _hidePosition;
    Size    _visibleSize;
    EventListenerTouchOneByOne *_CustomListener;
};

#endif /* defined(__BitLive__Toast__) */
