//
//  ActionQueue.h
//  Vista
//
//  Created by kerberos on 3/1/15.
//
//

#ifndef __Vista__ActionQueue__
#define __Vista__ActionQueue__

#include <stdio.h>
#include <list>
#include "cocos2d.h"
#include "webrtc/base/criticalsection.h"

USING_NS_CC;
class ActionQueue {
public:
    ActionQueue(Node* node);
    ~ActionQueue();
    void add(ActionInterval* action);
    void afterAction();
private:
    std::list<ActionInterval*> _actionList;
    Node* _node;
    rtc::CriticalSection criticalsection;
    bool _busy;
};
#endif /* defined(__Vista__ActionQueue__) */
