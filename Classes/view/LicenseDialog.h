//
//  LicenseDialog.h
//  AnicameraMAC
//
//  Created by kerberos on 12/9/14.
//
//

#ifndef __AnicameraMAC__LicenseDialog__
#define __AnicameraMAC__LicenseDialog__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "view/MainScene.h"
#include "extra/Strings.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;
class LicenseDialog : public LayerColor
{
public:
    ~LicenseDialog();
    static Color4B BackgroundColor;
    static Color3B FontColor;
    static int TitleFontSize;
    static int BodyFontSize;
    static int MarginTop;
    static int MarginBottom;
    static std::string OkButtonNormal;
    static std::string OkButtonSelect;
    static LicenseDialog* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    void OnOkButtonClick(Ref* pSender);
    void setTitle(const std::string& title);
    void setBody(const std::string& body);
    
private:
    bool init(MainScene* mainscene);
    MainScene* mMainScene;
    Text*       _titleText;
    Button*     _okButton;
    RichText*   _bodyText;
    ListView*   _listView;
    EventListenerTouchOneByOne *_CustomListener;
};
#endif /* defined(__AnicameraMAC__LicenseDialog__) */
