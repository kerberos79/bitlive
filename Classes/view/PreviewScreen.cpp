//
//  PreviewScreen.cpp
//  265Player
//
//  Created by kerberos on 10/16/14.
//
//

#include "PreviewScreen.h"
#include <math.h>
#include "libyuv.h"

static void neon_BGRA_to_RGBA(unsigned char *src, int width, int height) {
#if !defined(__aarch64__)
    int numPixels = width * height;
    int iterations = numPixels / 8;
    asm volatile ("1:                        \n"
                 "vld4.8    {d0-d3}, [%0]   \n"
                 "vswp.8    d0, d2          \n" //swap Red and Blue
                 "vst4.8    {d0-d3}, [%0]!  \n"
                 "subs      %1, %1, #1      \n"
                 "bne       1b              \n"
                 :
                 : "r"(src), "r"(iterations)
                 : "memory"
                 );
#else
    int numPixels = width * height * 4;
    unsigned char temp;
    for (int i = 0; i < numPixels; i += 4) {
        temp = src[i];
        src[i] = src[i + 2];
        src[i + 2] = temp;
    }
#endif
}


PreviewScreen::~PreviewScreen()
{
    if(_emoticonManager)
        delete _emoticonManager;
    if(_stickerManager)
        delete _stickerManager;
    if(_decorationManager)
        delete _decorationManager;
    
    if(_recordRGBABuffer)
        delete _recordRGBABuffer;
    if(_recordBufferU)
        delete _recordBufferU;
    if(_faceDetectHelper)
        delete _faceDetectHelper;
}

PreviewScreen * PreviewScreen::create(MainScene *mainscene, int dst_width, int dst_height)
{
    PreviewScreen *ret = new (std::nothrow) PreviewScreen();
    
    if(ret && ret->init(mainscene, dst_width, dst_height, Texture2D::PixelFormat::RGBA8888))
    {
        ret->autorelease();
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return nullptr;
}

bool PreviewScreen::init(MainScene *mainscene, int dst_width, int dst_height, Texture2D::PixelFormat eFormat)
{
    mMainScene = mainscene;
    this->setContentSize(Size(dst_width, dst_height));
    mYUVSprite = YUVSprite::create(dst_width, dst_height, dst_width, dst_height);
    mYUVSprite->retain();
    mYUVSprite->setAnchorPoint(Vec2(0.5, 0.5));
    mYUVSprite->setFlippedX(true);
    mYUVSprite->setPosition(Vec2(dst_width*0.5f, dst_height*0.5f));
    mYUVSprite->setScale(1.02);
    rtX = RenderTexture::create(dst_width, dst_height);
    rtX->retain();
    rtX->setAnchorPoint(Vec2::ZERO);
    rtX->setPosition(Vec2::ZERO);
    
    stepX = Sprite::createWithTexture(rtX->getSprite()->getTexture());
    stepX->retain();
    stepX->setAnchorPoint(Vec2::ZERO);
    stepX->setPosition(Vec2::ZERO);
    stepX->setFlippedY(true);
    
    _outline = DrawNode::create();
    _outline->retain();
    _outline->drawLine(Vec2(2,2), Vec2(2, dst_height-2), Color4F::WHITE);
    _outline->drawLine(Vec2(2.0,dst_height-2), Vec2(dst_width-2, dst_height-2), Color4F::WHITE);
    _outline->drawLine(Vec2(dst_width-2,2), Vec2(dst_width-2, dst_height-2), Color4F::WHITE);
    _outline->drawLine(Vec2(2,2), Vec2(dst_width-12, 2), Color4F::WHITE);
    
    _updatedFrame = false;
    _isFrontCamera = true;
    _sticker = nullptr;
    _frameSkipCount = 0;
    _frameInterval = 1000000 * (1000.0 / 20.0); // 100ms
    _currentBitrate = 1;
    
    
    _recordRGBABuffer = new unsigned char[dst_width * dst_height * 4];
    _recordBufferU = new unsigned char[dst_width * dst_height/2];
    _recordBufferV = _recordBufferU + dst_width * dst_height/4;
    _startRecord = false;
    
    _width = dst_width;
    _height = dst_height;
    _bytesWidth = dst_width * 4;
    _halfWidth = dst_width / 2;
    _halfHeight = dst_height / 2;
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    _restInterval.tv_sec = 0;
    _restInterval.tv_nsec = 0;
#endif
    _oldTime = _touchOldTime = rtc::Time();
    return initWithWidthAndHeight(dst_width, dst_height, eFormat, 0);
}

void PreviewScreen::calculateGaussianWeights()
{
    const int points = 24;
    float weights[64];
    float dx = 1.0f/float(points-1);
    float sigma = 1.0f/3.0f;
    float norm = 1.0f/(sqrtf(2.0f*M_PI)*sigma*points);
    float divsigma2 = 0.5f/(sigma*sigma);
    weights[0] = 1.0f;
    for (int i = 1; i < points; i++)
    {
        float x = float(i)*dx;
        weights[i] = norm*expf(-x*x*divsigma2);
        weights[0] -= 2.0f*weights[i];
    }
    CCLOG("0, %f", weights[0]);
    
    for (int i = points-1; i >0 ; i--)
    {
        CCLOG("     yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-%f)).r*%f;", 0.004f*i, weights[i]);
    }
    for (int i = 1; i <points ; i++)
    {
        CCLOG("     yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+%f)).r*%f;", 0.004f*i, weights[i]);
    }
    
    CCLOG("====rgb=================");
     for (int i = points-1; i >0 ; i--)
     {
     CCLOG("     yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+%f, v_texCoord.y)).r*%f;", 0.004f*i, weights[i]);
     }
     for (int i = 1; i <points ; i++)
     {
     CCLOG("     yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-%f, v_texCoord.y)).r*%f;", 0.004f*i, weights[i]);
     }
     
}

void PreviewScreen::initGLProgram()
{
    _glProgramManager.load("program.json");
    _glProgramManager.add("Origin", "original.fsh");
    _glProgramManager.add("Blur", "yuv_vertical_blur.fsh", "rgb_horizontal_blur.fsh");
    this->setProgram("Origin");

}

void PreviewScreen::setProgram(std::string program)
{
    _currentProgramInfo = _glProgramManager.getGLProgramInfo(program);
    
    _twoPassFilter = false;
    if(_currentProgramInfo!=nullptr)
    {
        mYUVSprite->setGLProgram(_currentProgramInfo, this->getContentSize());
        if(_currentProgramInfo->_program2)
        {
            stepX->setGLProgram(_currentProgramInfo->_program2);
            auto glprogramstate = stepX->getGLProgramState();
            glprogramstate->setUniformFloat("imageWidth", this->getContentSize().width);
            glprogramstate->setUniformFloat("imageHeight", this->getContentSize().height);
            glprogramstate->setUniformInt("radius", GLProgramManager::BiteralBlurRadius);
            _twoPassFilter = true;
        }
        if(_currentProgramInfo->_face)
        {
            _faceDetectHelper->enable(FaceDetectHelper::GLProgram);
        }
        else
        {
            _faceDetectHelper->disable(FaceDetectHelper::GLProgram);
        }
        setDecoration(_currentProgramInfo->_decoration);
        _lastEffect = program;
    }
}
void PreviewScreen::setBlurEffect(bool onoff)
{
    if(onoff)
    {
        _currentProgramInfo = _glProgramManager.getGLProgramInfo("Blur");
        if(_currentProgramInfo!=nullptr)
        {
            mYUVSprite->setGLProgram(_currentProgramInfo->_program1, this->getContentSize());
            if(_currentProgramInfo->_program2)
            {
                stepX->setGLProgram(_currentProgramInfo->_program2);
                _twoPassFilter = true;
            }
        }
    }
    else
    {
        this->scheduleUpdate();
    }
}

void PreviewScreen::update(float dt)
{
    std::string lastEffect = UserDefault::getInstance()->getStringForKey("lasteffect");
    if(lastEffect.length()==0)
        lastEffect = "Origin";
    this->setProgram(lastEffect);
    this->unscheduleUpdate();
}

int PreviewScreen::updateYV12Frame(unsigned char* src_y, unsigned char* src_u, unsigned char* src_v)
{
    int ret = 0;
    int i = 0;
    criticalsection.Enter();
    _frameBufferY = src_y;
    _frameBufferU = src_u;
    _frameBufferV = src_v;
    if((_frameSkipCount++)& _currentBitrate)
    {
        ret = previewEncodingDelegate(_frameBufferY, _frameBufferU, _frameBufferV);
    }
    _updatedFrame = true;
    long frameInterval = _frameInterval;
    criticalsection.Leave();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    _currentTime = rtc::Time();
    long interval = (_currentTime - _oldTime)*1000000;
    _restInterval.tv_nsec = frameInterval - interval;
    
    if(_restInterval.tv_nsec>10000000)
    {
        nanosleep(&_restInterval, NULL);
    }
    _oldTime = _currentTime;
#endif
    return ret;
}
void PreviewScreen::fitOnScreen(Size& parentLayerSize)
{
    _parentSreenSize = parentLayerSize;
	_fullScreenRatio = (float)parentLayerSize.width / (float)_contentSize.width;
	_fullScreenSize.width = parentLayerSize.width;
    _fullScreenSize.height = _contentSize.height * _fullScreenRatio;
    _fullScreenPosition = Vec2(_fullScreenSize.width * 0.5f, parentLayerSize.height - _fullScreenSize.height * 0.5f);
    
    _smallScreenSize.width = MainScene::SmallPreviewSize * 0.9f;
    _smallScreenSize.height = (_contentSize.height / _contentSize.width) * _smallScreenSize.width;
    _smallScreenRatio = _smallScreenSize.width / _contentSize.width;
    _smallScreenPosition[0].x = MainScene::SmallPreviewSize * 0.5f;
    _smallScreenPosition[0].y = _fullScreenPosition.y + _fullScreenSize.height * 0.5f - _smallScreenSize.height * 0.55f;
    _smallScreenPosition[1].x = _fullScreenSize.width - MainScene::SmallPreviewSize * 0.5f;
    _smallScreenPosition[1].y = _fullScreenPosition.y + _fullScreenSize.height * 0.5f - _smallScreenSize.height * 0.55f;
    _smallScreenPosition[2].x = _fullScreenSize.width - MainScene::SmallPreviewSize * 0.5f;
    _smallScreenPosition[2].y = _fullScreenPosition.y - _fullScreenSize.height * 0.5f + _smallScreenSize.height * 0.55f;
    _smallScreenPosition[3].x = MainScene::SmallPreviewSize * 0.5f;
    _smallScreenPosition[3].y = _fullScreenPosition.y - _fullScreenSize.height * 0.5f + _smallScreenSize.height * 0.55f;
    _smallScreenSector = 1;
    
    _stickerScale = 0.7;
    _midPoint = _contentSize/2;
    _faceDetectHelper = new FaceDetectHelper();

    this->setAutoDraw(true);
    auto eventListener = EventListenerTouchOneByOne::create();
    eventListener->setSwallowTouches(true);
    eventListener->onTouchBegan = CC_CALLBACK_2(PreviewScreen::onTouchBegan, this);
    eventListener->onTouchMoved = CC_CALLBACK_2(PreviewScreen::onTouchMoved, this);
    eventListener->onTouchEnded = CC_CALLBACK_2(PreviewScreen::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(eventListener, this);

    enterFullScreen();
}

bool PreviewScreen::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if(_smallScreen)
    {
        float distance = this->getPosition().distance(touch->getLocation());
        if(distance < _smallScreenSize.height)
        {
            _touchOnScreen = true;
        }
        else
        {
            _touchOnScreen = false;
        }
    }
    else
    {
        _touchOnScreen = true;
    }
    return _touchOnScreen;
}

void PreviewScreen::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if(_smallScreen)
    {
        if(_touchOnScreen)
        {
            setPosition(this->getPosition().x + touch->getDelta().x,
                        this->getPosition().y + touch->getDelta().y);
        }
    }
}

void PreviewScreen::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if(_smallScreen)
    {
        if(_touchOnScreen)
        {
            uint32 time = rtc::Time();
            if( (time - _touchOldTime) < 300)
            {
                mMainScene->OnChangeScreenLayout();
            }
            else
            {
                selectSector(touch->getLocation());
                _touchOldTime = time;
            }
        }
    }
    else
    {
        if(mMainScene->isRecordingState())
        {
            if(touch->getLocation().y > _fullScreenPosition.y * 0.54)
                mMainScene->getMainMenu()->menuRecordCallback(this);
             
        }
    }
}

int PreviewScreen::getSmallScreenSector()
{
    return _smallScreenSector;
}

void PreviewScreen::setSmallScreenSector(int sector)
{
    _smallScreenSector = sector;
}

void PreviewScreen::selectSector(Vec2 p)
{
    float duration;
    const float standard = _fullScreenPosition.distance(Vec2(0,0));
    if(p.x < _fullScreenPosition.x && p.y < _fullScreenPosition.y)
    {
        _smallScreenSector = 3;
    }
    else if(p.x < _fullScreenPosition.x && p.y >= _fullScreenPosition.y)
    {
        _smallScreenSector = 0;
    }
    else if(p.x >= _fullScreenPosition.x && p.y < _fullScreenPosition.y)
    {
        _smallScreenSector = 2;
    }
    else
    {
        _smallScreenSector = 1;
    }
    duration = p.distance(_smallScreenPosition[_smallScreenSector]) / standard;
    auto moveAction = MoveTo::create(0.3 * duration, _smallScreenPosition[_smallScreenSector]);
    auto action = Sequence::create(moveAction,
                                   nullptr);
    this->runAction(action);
}

void PreviewScreen::stretch(float ratio)
{
    this->setScale(_fullScreenRatio * (0.2*ratio+1.0) , _fullScreenRatio);
}

float PreviewScreen::getCurrentRatio()
{
    return (_smallScreen)? _smallScreenRatio : _fullScreenRatio;
}

Vec2 PreviewScreen::getCurrentPos()
{
    return (_smallScreen)? _smallScreenPosition[_smallScreenSector] : _fullScreenPosition;
}

Size PreviewScreen::getScaledSize()
{
    return _fullScreenSize;
}

void PreviewScreen::enterSmallScreen()
{
    _smallScreen = true;
    this->setScale(_smallScreenRatio, _smallScreenRatio);
    this->setPosition(_smallScreenPosition[_smallScreenSector]);
    this->addChild(_outline, ZORDER_OUTLINE);
}

void PreviewScreen::enterFullScreen()
{
    _smallScreen = false;
    this->setScale(_fullScreenRatio, _fullScreenRatio);
    this->setPosition(_fullScreenPosition);
    this->removeChild(_outline);
}

bool PreviewScreen::isSmallScreen()
{
    return _smallScreen;
}

void PreviewScreen::enterCallScreen()
{
    const Vec2 center = _parentSreenSize/2;
    const float standard = center.distance(Vec2(0,0));
    _smallScreenSector = 1;
    float duration = 0.3 * this->getPosition().distance(_smallScreenPosition[_smallScreenSector]) / standard;
    auto scaleAction = Sequence::create(DelayTime::create(0.5),
                                        ScaleTo::create(duration, _smallScreenRatio),
                                        nullptr);
    this->runAction(scaleAction);
    auto moveAction = Sequence::create(DelayTime::create(0.5),
                                    MoveTo::create(duration, _smallScreenPosition[_smallScreenSector]),
                                    CallFuncN::create( CC_CALLBACK_1(PreviewScreen::afterEnterCall, this)),
                                    nullptr);
    this->runAction(moveAction);
}

void PreviewScreen::afterEnterCall(Node* sender)
{
    _smallScreen = true;
    this->removeChild(_outline);
}

void PreviewScreen::flipCameraPreview()
{
    _isFrontCamera = !_isFrontCamera;
    mYUVSprite->setFlippedX(_isFrontCamera);
}

Vec3 PreviewScreen::getDegree()
{
    Vec3 degree;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if(_isFrontCamera)
        degree = Vec3(0,0,90);
    else
        degree = Vec3(0,0,90);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if(_isFrontCamera)
        degree = Vec3(0,0,270);
    else
        degree = Vec3(180,180,270);
#endif
    return degree;
}

const char* PreviewScreen::getEffect()
{
    return _lastEffect.c_str();
}


unsigned int PreviewScreen::getBitrate()
{
    return _currentBitrate;
}

void PreviewScreen::setBitrate(unsigned int bitrate)
{
	_currentBitrate = bitrate;
}

void PreviewScreen::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
    criticalsection.Enter();
    if(_updatedFrame)
    {
        mYUVSprite->updateDirect(_frameBufferY, _frameBufferU, _frameBufferV);
        if(_currentProgramInfo->_face)
        {
            mYUVSprite->updateFaceDetect(_midPoint, _stickerScale);
        }
        else if(_currentProgramInfo->_time)
        {
            mYUVSprite->updateTime();
        }
        _updatedFrame = false;
        if(_twoPassFilter)
        {
            rtX->begin();
            mYUVSprite->visit();
            rtX->end();
            
            begin();
            stepX->setTexture(rtX->getSprite()->getTexture());
            stepX->visit();
            
            sortAllChildren();
            for(const auto &child: _children)
            {
                if (child != _sprite)
                    child->visit();
            }
            end();
        }
        else
        {
            begin();
            mYUVSprite->visit();
            
            sortAllChildren();
            for(const auto &child: _children)
            {
                if (child != _sprite)
                    child->visit();
            }
            end();
        }
    }
    criticalsection.Leave();
    
    if(_startRecord)
        record();
}

void PreviewScreen::updateFaceRect(int leftX, int leftY, int rightX, int rightY)
{
    criticalsection.Enter();
    if(_isFrontCamera)
    {
        int leftX2 = _contentSize.width - leftX;
        int rightX2 =  _contentSize.width - rightX;
        if(_newLeftEye.distance(Vec2(leftX2, leftY))<=5 && _newRightEye.distance(Vec2(rightX2, rightY))<=5)
        {
            criticalsection.Leave();
            return;
        }
        _newLeftEye.x = leftX2;
        _newLeftEye.y = leftY;
        _newRightEye.x = rightX2;
        _newRightEye.y = rightY;
        _midPoint = (_newLeftEye + _newRightEye)/2;
        _stickerAngle = atan2(_newRightEye.x - _midPoint.x, _newRightEye.y - _midPoint.y) * (180 / M_PI) - 90;
    }
    else
    {
        if(_newLeftEye.distance(Vec2(leftX, leftY))<=5 && _newRightEye.distance(Vec2(rightX, rightY))<=5)
        {
            criticalsection.Leave();
            return;
        }
        _newLeftEye.x = leftX;
        _newLeftEye.y = leftY;
        _newRightEye.x = rightX;
        _newRightEye.y = rightY;
        _midPoint = (_newLeftEye + _newRightEye)/2;
        _stickerAngle = atan2(_newRightEye.x - _midPoint.x, _newRightEye.y - _midPoint.y) * (180 / M_PI) +90;
    }
    _stickerScale = _newLeftEye.distance(_newRightEye) / 200.0;
    if(_sticker)
    {
        _stickerLayer->setPosition(_midPoint);
        _stickerLayer->setScale(_stickerScale);
        _stickerLayer->setRotation(_stickerAngle);
    }
    _decorationManager->updateFaceRect(_midPoint, _stickerScale, _stickerAngle);
    criticalsection.Leave();
    if(mMainScene->isCallingState())
    {
        std::ostringstream os;
        if(_isFrontCamera)

        os <<_stickerManager->getCurrentTag()<<','<<leftX<<','<<leftY
        <<','<<rightX<<','<<rightY;
        CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::SendStickerCommand, os.str());
    }
}

void PreviewScreen::setEncodingFrameRate(int value)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    Device::setFrameRateCamera(value);
#endif
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    criticalsection.Enter();
    _frameInterval = 1000000 * (1000.0 / (float)value);
    criticalsection.Leave();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	Device::setFrameRateCamera(value);
#endif
}

void PreviewScreen::addEmoticonLayer()
{
    _emoticonLayer = Node::create();
    _emoticonLayer->retain();
    _emoticonLayer->setAnchorPoint(Vec2(0.5, 0.5));
    _emoticonLayer->setPosition(this->getContentSize()/2);
    _emoticonLayer->setContentSize(this->getContentSize());
    this->addChild(_emoticonLayer, ZORDER_EMOTICON);
    
    _stickerLayer = Node::create();
    _stickerLayer->retain();
    _stickerLayer->setAnchorPoint(Vec2(0.5, 0.5));
    _stickerLayer->setPosition(this->getContentSize()/2);
    _stickerLayer->setContentSize(Size(0,0));
    this->addChild(_stickerLayer, ZORDER_STICKER);
    
    
    _emoticonManager = EmoticonManager::create(mMainScene, "emoticon.json", this->getContentSize());
    _emoticonManager->setParent(_emoticonLayer, true);
    
    _stickerManager = StickerManager::create(mMainScene, "sticker.json", this->getContentSize());
    _decorationManager = DecorationManager::create(_emoticonLayer, this->getContentSize());
}

void PreviewScreen::setSticker(int tag)
{
    StickerInfo* stickerInfo = _stickerManager->getPreviewItem(tag);
    if(stickerInfo == nullptr)
    {
        _faceDetectHelper->disable(FaceDetectHelper::Sticker);
        criticalsection.Enter();
        if(_sticker)
        {
            _stickerLayer->removeChild(_sticker);
            _sticker = nullptr;
        }
        criticalsection.Leave();
        if(mMainScene->isCallingState())
        {
            std::string param("");
            CallStateHandler::getInstance()->SendMessage(CallStateHandler::Cmd::SendStickerCommand, param);
        }
    }
    else
    {
        _faceDetectHelper->enable(FaceDetectHelper::Sticker);
        criticalsection.Enter();
        _stickerLayer->removeChild(_sticker);
        _sticker = stickerInfo->getSprite();
        _stickerLayer->setPosition(_midPoint);
        _stickerLayer->setScale(_stickerScale);
        _stickerLayer->setRotation(_stickerAngle);
        if(stickerInfo->_flip)
            _sticker->setFlippedX(_isFrontCamera);
        if(stickerInfo->_action)
            _sticker->runAction(stickerInfo->_action);
        _stickerLayer->addChild(_sticker, ZORDER_STICKER);
        criticalsection.Leave();
    }
}
void PreviewScreen::setDecoration(int tag)
{
    criticalsection.Enter();
    _decorationManager->setItem(tag);
    _decorationManager->updateFaceRect(_midPoint, _stickerScale, _stickerAngle);
    criticalsection.Leave();
    _emoticonLayer->setRotation3D(Vec3(0.0, 0.0, 0.0));
}

bool PreviewScreen::isProhibitEmoticon()
{
    return _currentProgramInfo->_decoration;
}

void PreviewScreen::setEmoticon(int tag)
{
	if(_emoticonManager->addSprite(tag))
    {
        if(_isFrontCamera)
        {
            Vec3 rotation = Vec3(0.0, 180.0, 0.0);
            _emoticonLayer->setRotation3D(rotation);
        }
    }
    else
    {
        Vec3 rotation = Vec3(0.0, 0.0, 0.0);
        _emoticonLayer->setRotation3D(rotation);
    }
}

void PreviewScreen::backupLastEffect()
{
    UserDefault::getInstance()->setStringForKey("lasteffect", _lastEffect);
}

void PreviewScreen::captureJpeg()
{
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    sprintf(_filename, "%02d%02d%02d%02d%02d%02d.jpg", (now->tm_year -100), now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    sprintf(_filename, "Pictures/%02d%02d%02d%02d%02d%02d.jpg", (now->tm_year -100), now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
#endif
    
    saveToFile(_filename, Image::Format::JPG, false, CC_CALLBACK_2(MainScene::afterCaptureCallback, mMainScene));
}

void PreviewScreen::captureGif(bool enable)
{
    _startRecord = enable;
    if(enable)
    {
        time_t t = time(0);   // get time now
        struct tm * now = localtime( & t );
        sprintf(_filename, "%s%02d%02d%02d%02d%02d%02d.gif", Device::getPhotoStorageDirectory().c_str(), (now->tm_year -100), now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
    }
}

void PreviewScreen::enableRecord(bool enable)
{
    _startRecord = enable;
    
    if(enable)
    {
        _saveToFileCommand.init(_globalZOrder);
        _saveToFileCommand.func = CC_CALLBACK_0(PreviewScreen::afterCaptureFirstFrame, this);
        
        Director::getInstance()->getRenderer()->addCommand(&_saveToFileCommand);
    }
}

void PreviewScreen::afterCaptureFirstFrame()
{
    CCLOG("afterCaptureFirstFrame");
    if(_captureImage)
        CC_SAFE_DELETE(_captureImage);
    _captureImage = newImage(true);
    
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    sprintf(_filename, "%s%02d%02d%02d%02d%02d%02d.mp4", Device::getPhotoStorageDirectory().c_str(), (now->tm_year -100), now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
    mMainScene->afterRecordFirstFrame(_filename);
}

void PreviewScreen::getVideoFrameRGBA(unsigned char* dst_rgba)
{
    criticalsection2.Enter();
    libyuv::ARGBCopy(_recordRGBABuffer, _bytesWidth,
                     dst_rgba, _bytesWidth,
                     _width, -_height);
    criticalsection2.Leave();
    neon_BGRA_to_RGBA(dst_rgba, _width, _height);
}

void PreviewScreen::getVideoFrameBGRA(unsigned char* dst_bgra)
{
    criticalsection2.Enter();
    libyuv::ARGBCopy(_recordRGBABuffer, _bytesWidth,
                     dst_bgra, _bytesWidth,
                     _width, -_height);
    criticalsection2.Leave();
}

void PreviewScreen::getVideoFrameNV21(unsigned char* dst_y, unsigned char* dst_uv)
{
    unsigned char *src_u = _recordBufferU;
    unsigned char *src_v = _recordBufferV;
    criticalsection2.Enter();
    libyuv::ARGBToI420(_recordRGBABuffer, _bytesWidth,
                       dst_y, _width,
                       _recordBufferU, _halfWidth,
                       _recordBufferV, _halfWidth,
                       _width, -_height);
    criticalsection2.Leave();
    
    for (int y = 0; y < _halfHeight; ++y) {
        // Merge a row of U and V into a row of UV.
        libyuv::MergeUVRow_Any_NEON(src_v, src_u, dst_uv, _halfWidth);
        src_u += _halfWidth;
        src_v += _halfWidth;
        dst_uv += _width;
    }
}

void PreviewScreen::record()
{
    criticalsection2.Enter();
    do
    {
        
        glGetIntegerv(GL_FRAMEBUFFER_BINDING, &_oldFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, _FBO);
        
        // TODO: move this to configration, so we don't check it every time
        /*  Certain Qualcomm Andreno gpu's will retain data in memory after a frame buffer switch which corrupts the render to the texture. The solution is to clear the frame buffer before rendering to the texture. However, calling glClear has the unintended result of clearing the current texture. Create a temporary texture to overcome this. At the end of RenderTexture::begin(), switch the attached texture to the second one, call glClear, and then switch back to the original texture. This solution is unnecessary for other devices as they don't have the same issue with switching frame buffers.
         */
        if (Configuration::getInstance()->checkForGLExtension("GL_QCOM"))
        {
            // -- bind a temporary texture so we can clear the render buffer without losing our texture
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _textureCopy->getName(), 0);
            CHECK_GL_ERROR_DEBUG();
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _texture->getName(), 0);
        }
        glPixelStorei(GL_PACK_ALIGNMENT, 1);
        glReadPixels(0,0, _contentSize.width, _contentSize.height,GL_RGBA,GL_UNSIGNED_BYTE, _recordRGBABuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, _oldFBO);
        

    } while (0);
    criticalsection2.Leave();
}

void PreviewScreen::setWaterMark(bool enable)
{
#ifndef COCOS2D_DEBUG
    if(enable)
    {
    }
    else
    {
    }
#endif
}
