//
//  SettingMenu.cpp
//  BitLive
//
//  Created by keros on 6/14/15.
//
//

#include "SettingMenu.h"

Color4B SettingMenu::TitleBackgroundColor;
Color4B SettingMenu::BodyBackgroundColor;
Color3B SettingMenu::TitleFontColor;
Color3B SettingMenu::BodyFontColor;
int SettingMenu::TitleFontSize;
int SettingMenu::BodyFontSize;
int SettingMenu::Height;
int SettingMenu::TitleHeight;
std::string SettingMenu::CloseButton;
std::string SettingMenu::CountryImage;
std::string SettingMenu::SignoutImage;
std::string SettingMenu::JoinImage;
std::string SettingMenu::LicenseImage;
std::string SettingMenu::SignoutButtonNormal;
Color3B SettingMenu::SignoutButtonFontColor;
int SettingMenu::SignoutButtonFontSize;
std::string SettingMenu::CountryCodeBoxBackground;
Size SettingMenu::CountryCodeBoxSize;

SettingMenu::~SettingMenu()
{
}

SettingMenu * SettingMenu::create(MainScene* mainscene)
{
    SettingMenu * menu = new (std::nothrow) SettingMenu();
    if( menu && menu->init(mainscene))
    {
        menu->setPosition(Vec2::ZERO);
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void SettingMenu::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& settingMenu = DICTOOL->getSubDictionary_json(doc, "setting_menu");
    SettingMenu::TitleBackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(settingMenu, "title_background_color"));
    SettingMenu::BodyBackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(settingMenu, "body_background_color"));
    SettingMenu::TitleFontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(settingMenu, "title_font_color"));
    SettingMenu::BodyFontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(settingMenu, "body_font_color"));
    SettingMenu::TitleFontSize = DICTOOL->getIntValue_json(settingMenu, "title_font_size") * MainScene::LayoutScale;
    SettingMenu::BodyFontSize = DICTOOL->getIntValue_json(settingMenu, "body_font_size") * MainScene::LayoutScale;
    SettingMenu::Height = DICTOOL->getIntValue_json(settingMenu, "height") * MainScene::LayoutScale;
    SettingMenu::TitleHeight = DICTOOL->getIntValue_json(settingMenu, "title_height") * MainScene::LayoutScale;
    SettingMenu::CloseButton = DICTOOL->getStringValue_json(settingMenu, "close_button");
    SettingMenu::CountryImage = DICTOOL->getStringValue_json(settingMenu, "country_image");
    SettingMenu::SignoutImage = DICTOOL->getStringValue_json(settingMenu, "signout_image");
    SettingMenu::JoinImage = DICTOOL->getStringValue_json(settingMenu, "join_image");
    SettingMenu::LicenseImage = DICTOOL->getStringValue_json(settingMenu, "license_image");
    SettingMenu::SignoutButtonNormal = DICTOOL->getStringValue_json(settingMenu, "signout_button_normal");
    SettingMenu::SignoutButtonFontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(settingMenu, "signout_button_font_color"));
    SettingMenu::SignoutButtonFontSize = DICTOOL->getIntValue_json(settingMenu, "signout_button_font_size") * MainScene::LayoutScale;
    SettingMenu::CountryCodeBoxBackground = DICTOOL->getStringValue_json(settingMenu, "country_code_box_background");
    SettingMenu::CountryCodeBoxSize = Size(DICTOOL->getIntValue_json(settingMenu, "country_code_box_width") * MainScene::LayoutScale,
                                           DICTOOL->getIntValue_json(settingMenu, "country_code_box_height") * MainScene::LayoutScale);
}

bool SettingMenu::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    // Read content from file
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("setting.json");
    
    rapidjson::Document doc;
    
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    auto titleLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "title"), Strings::getFontType(), TitleFontSize);
    titleLabel->setColor(TitleFontColor);
    titleLabel->setAnchorPoint(Vec2(0.0, 0.5));
    titleLabel->setPosition(Vec2(visibleSize.width*0.06f, SettingMenu::TitleHeight*0.5f));
    
    auto closeButton = Button::create();
    closeButton->setAnchorPoint(Vec2(1.0, 0.5));
    closeButton->loadTextures(SettingMenu::CloseButton, "", "");
    closeButton->setPosition(Vec2(visibleSize.width, SettingMenu::TitleHeight * 0.5f));
    closeButton->setTouchEnabled(true);
    closeButton->addClickEventListener(CC_CALLBACK_1(SettingMenu::menuCloseCallback,this));
    
    float settingMenuHeight = SettingMenu::Height - SettingMenu::TitleHeight;
    auto titleLayer = LayerColor::create(SettingMenu::TitleBackgroundColor, visibleSize.width, SettingMenu::TitleHeight);
    titleLayer->setPosition(0, settingMenuHeight);
    titleLayer->addChild(closeButton);
    titleLayer->addChild(titleLabel);
    
    auto countrySprite = Sprite::create(SettingMenu::CountryImage);
    countrySprite->setAnchorPoint(Vec2::ZERO);
    countrySprite->setPosition(Vec2(visibleSize.width * 0.06f, settingMenuHeight*0.7f));
    auto countrycode = Text::create(DICTOOL->getStringValue_json(doc, "countrycode"), Strings::getFontType(), BodyFontSize);
    countrycode->setColor(BodyFontColor);
    countrycode->setAnchorPoint(Vec2::ZERO);
    countrycode->setPosition(Vec2(visibleSize.width * 0.14f,settingMenuHeight*0.7f));
    
    countryCodeEditBox = cocos2d::ui::EditBox::create(SettingMenu::CountryCodeBoxSize,
                                                      cocos2d::ui::Scale9Sprite::create(SettingMenu::CountryCodeBoxBackground));
    countryCodeEditBox->setInputMode(cocos2d::ui::EditBox::InputMode::PHONE_NUMBER);
    countryCodeEditBox->setAnchorPoint(Vec2::ZERO);
    countryCodeEditBox->setPosition(Vec2(visibleSize.width * 0.4f, countrycode->getPosition().y + countrycode->getContentSize().height * 0.5f - countryCodeEditBox->getContentSize().height * 0.5f));
    countryCodeEditBox->setFontName(Strings::getFontType());
    countryCodeEditBox->setFontSize(BodyFontSize);
    countryCodeEditBox->setFontColor(Color3B::WHITE);
    countryCodeEditBox->setMaxLength(4);
    countryCodeEditBox->setReturnType(cocos2d::ui::EditBox::KeyboardReturnType::DONE);
    countryCodeEditBox->setDelegate(this);
    countryCodeEditBox->setText(Device::getCountryCode().c_str());
    
    auto itemMenu = Menu::create();
    itemMenu->setContentSize(Size(visibleSize.width, SettingMenu::Height));
    itemMenu->setPosition(Vec2::ZERO);
    
    auto signoutSprite = Sprite::create(SettingMenu::SignoutImage);
    signoutSprite->setAnchorPoint(Vec2::ZERO);
    signoutSprite->setPosition(Vec2(visibleSize.width * 0.06f,settingMenuHeight*0.4f));
    auto signoutLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "signout"), Strings::getFontType(), BodyFontSize);
    signoutLabel->setColor(BodyFontColor);
    auto signoutMenu = MenuItemLabel::create(signoutLabel, CC_CALLBACK_1(SettingMenu::menuSignoutCallback, this));
    signoutMenu->setAnchorPoint(Vec2::ZERO);
    signoutMenu->setPosition(Vec2(visibleSize.width * 0.14f, settingMenuHeight*0.4f));
    
    _signoutButton = Button::create();
    _signoutButton->loadTextures(SettingMenu::SignoutButtonNormal, "", "");
    _signoutButton->setAnchorPoint(Vec2::ZERO);
    _signoutButton->setTitleText(DICTOOL->getStringValue_json(doc, "signout_button"));
    _signoutButton->setTitleColor(SettingMenu::SignoutButtonFontColor);
    _signoutButton->setTitleFontSize(SettingMenu::SignoutButtonFontSize);
    signoutButtonHeight = signoutMenu->getPosition().y + signoutMenu->getContentSize().height * 0.5f - _signoutButton->getContentSize().height * 0.5f;
    _signoutButton->setPosition(Vec2(visibleSize.width, signoutButtonHeight));
    _signoutButton->setTouchEnabled(true);
    _signoutButton->addClickEventListener(CC_CALLBACK_1(SettingMenu::OnSignoutButtonClick,this));
    
    auto joinSprite = Sprite::create(SettingMenu::JoinImage);
    joinSprite->setAnchorPoint(Vec2::ZERO);
    joinSprite->setPosition(Vec2(visibleSize.width * 0.06f,settingMenuHeight*0.4f));
    auto joinLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "joinin"), Strings::getFontType(), BodyFontSize);
    joinLabel->setColor(BodyFontColor);
    auto joinMenu = MenuItemLabel::create(joinLabel, CC_CALLBACK_1(SettingMenu::menuJoinCallback, this));
    joinMenu->setAnchorPoint(Vec2::ZERO);
    joinMenu->setPosition(Vec2(visibleSize.width * 0.14f, settingMenuHeight*0.4f));
    
    auto licenseSprite = Sprite::create(SettingMenu::LicenseImage);
    licenseSprite->setAnchorPoint(Vec2::ZERO);
    licenseSprite->setPosition(Vec2(visibleSize.width * 0.06f, settingMenuHeight*0.1f));
    auto licenseLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "opensource"), Strings::getFontType(), BodyFontSize);
    licenseLabel->setColor(BodyFontColor);
    auto licenseMenu = MenuItemLabel::create(licenseLabel, CC_CALLBACK_1(SettingMenu::menuLicenseCallback, this));
    licenseMenu->setAnchorPoint(Vec2::ZERO);
    licenseMenu->setPosition(Vec2(visibleSize.width * 0.14f, settingMenuHeight*0.1f));
    
    if(!initWithColor(SettingMenu::BodyBackgroundColor, visibleSize.width, SettingMenu::Height))
        return false;
    this->addChild(titleLayer);
    this->addChild(countrySprite);
    this->addChild(countryCodeEditBox);
    this->addChild(countrycode);
    this->addChild(licenseSprite);
    
    std::string firsttime = UserDefault::getInstance()->getStringForKey("firsttime");
    if(firsttime.compare("offline"))
    {
        itemMenu->addChild(signoutMenu);
        itemMenu->addChild(licenseMenu);
        this->addChild(signoutSprite);
        this->addChild(_signoutButton);
        this->addChild(itemMenu);
    }
    else
    {
        itemMenu->addChild(joinMenu);
        itemMenu->addChild(licenseMenu);
        this->addChild(joinSprite);
        this->addChild(itemMenu);
    }
    _startPos = Vec2(0, -SettingMenu::Height);
    _endPos = Vec2::ZERO;
    _visibleAction = EaseExponentialOut::create(MoveTo::create(0.4f, _endPos));
    _visibleAction->retain();
    
    _invisibleAction = Sequence::create(EaseExponentialOut::create(MoveTo::create(0.4f, _startPos)),
                                        CallFuncN::create( CC_CALLBACK_1(SettingMenu::afterInvisibleAction, this)),
                                        nullptr);
    _invisibleAction->retain();
    return true;
}

void SettingMenu::menuCloseCallback(Ref* pSender)
{
    mMainScene->toggleMenuCallback(this);
}

void SettingMenu::menuSignoutCallback(Ref* pSender)
{
    Size visibleSize = Director::getInstance()->getVisibleSize();
    auto actionTo = MoveTo::create(0.5, Vec2(visibleSize.width - _signoutButton->getContentSize().width * 1.1f , signoutButtonHeight));
    _signoutButton->runAction(actionTo);
}

void SettingMenu::menuJoinCallback(Ref* pSender)
{
    mMainScene->toggleMenuCallback(this);
    mMainScene->OnJoin();
}

void SettingMenu::OnSignoutButtonClick(Ref* pSender)
{
    mMainScene->OnSignout();
}

void SettingMenu::menuLicenseCallback(Ref* pSender)
{
    mMainScene->OnLicenseDialogView();
}

void SettingMenu::editBoxEditingDidBegin(cocos2d::ui::EditBox* editBox)
{
}

void SettingMenu::editBoxEditingDidEnd(cocos2d::ui::EditBox* editBox)
{
}

void SettingMenu::editBoxTextChanged(cocos2d::ui::EditBox* editBox, const std::string& text)
{
}

void SettingMenu::editBoxReturn(ui::EditBox* editBox)
{
    std::string countryCode = countryCodeEditBox->getText();
    if(countryCode.length()==0)
    {
        return;
    }
    Device::setCountryCode(countryCodeEditBox->getText());
}

void SettingMenu::show(bool isShow)
{
    countryCodeEditBox->setText(Device::getCountryCode().c_str());
    if(isShow)
    {
        this->stopAllActions();
        this->setVisible(true);
        this->setPosition(_startPos);
        this->runAction(_visibleAction);        
    }
    else
    {
        this->runAction(_invisibleAction);
    }
}

void SettingMenu::afterInvisibleAction(Node* sender)
{
    this->setVisible(false);
}

cocos2d::Size& SettingMenu::getSize()
{
    return (cocos2d::Size&)this->getContentSize();
}

Color4B& SettingMenu::getBackgroundColor()
{
    return SettingMenu::TitleBackgroundColor;
}

void SettingMenu::setBackgroundColor(Color4B& color)
{
    _displayedColor.r = _realColor.r = color.r;
    _displayedColor.g = _realColor.g = color.g;
    _displayedColor.b = _realColor.b = color.b;
    _displayedOpacity = _realOpacity = color.a;
    
    updateColor();
}
