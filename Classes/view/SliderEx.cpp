//
//  SliderEx.cpp
//  AnicameraMAC
//
//  Created by kerberos on 11/24/14.
//
//

#include "SliderEx.h"


SliderEx* SliderEx::create(){
    auto ret = new (std::nothrow) SliderEx();
    if (ret && ret->init())
    {
        ret->_callback = nullptr;
        ret->loadBarTexture("slider_track.png");
        ret->loadSlidBallTextures("slider_thumb.png", "slider_thumb.png", "");
        ret->loadProgressBarTexture("slider_progress.png");
        
        ret->autorelease();
        
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return ret;
}

void SliderEx::setCallBack(const ccSliderExCallback& callback){
    _callback = callback;
}

void SliderEx::setRatio(float ratio) {
    if (ratio > 1.0f){
        ratio = 1.0f;
    }
    else if (ratio < 0.0f){
        ratio = 0.0f;
    }
    
    _ratio = ratio;
    _percent = 100 * _ratio;
    
    float dis = _barLength * _ratio;
    _slidBallRenderer->setPosition(Vec2(dis, _contentSize.height / 2.0f));
    if (_scale9Enabled){
        _progressBarRenderer->setPreferredSize(Size(dis,_progressBarTextureSize.height));
    }
    else
    {
        auto spriteRenderer = _progressBarRenderer->getSprite();
        
        if (nullptr != spriteRenderer) {
            Rect rect = spriteRenderer->getTextureRect();
            rect.size.width = _progressBarTextureSize.width * _ratio;
            spriteRenderer->setTextureRect(rect, spriteRenderer->isTextureRectRotated(), rect.size);
        }
    }
}

bool SliderEx::onTouchBegan(Touch *touch, Event *unusedEvent){
    auto ret = Slider::onTouchBegan(touch, unusedEvent);
    if(ret && _callback){
        _touchEvent = TouchEvent::DOWN;
        Vec2 nsp = convertToNodeSpace(_touchBeganPosition);
        _ratio = nsp.x / _barLength;
        if(_ratio < 0.0f)
            _ratio = 0.0f;
        else if(_ratio > 1.0f)
            _ratio = 1.0f;
        _callback(this,_ratio,_touchEvent);
    }
    return ret;
}

void SliderEx::onTouchMoved(Touch *touch, Event *unusedEvent){
    _touchEvent = TouchEvent::MOVE;
    Slider::onTouchMoved(touch, unusedEvent);
    Vec2 nsp = convertToNodeSpace(_touchMovePosition);
    _ratio = nsp.x / _barLength;
    if(_ratio < 0.0f)
        _ratio = 0.0f;
    else if(_ratio > 1.0f)
        _ratio = 1.0f;
    if(_callback){
        _callback(this,_ratio,_touchEvent);
    }
}

void SliderEx::onTouchEnded(Touch *touch, Event *unusedEvent){
    _touchEvent = TouchEvent::UP;
    Slider::onTouchEnded(touch, unusedEvent);
    Vec2 nsp = convertToNodeSpace(_touchEndPosition);
    _ratio = nsp.x / _barLength;
    if(_ratio < 0.0f)
        _ratio = 0.0f;
    else if(_ratio > 1.0f)
        _ratio = 1.0f;
    if(_callback){
        _callback(this,_ratio,_touchEvent);
    }
}

void SliderEx::onTouchCancelled(Touch *touch, Event *unusedEvent){
    _touchEvent = TouchEvent::CANCEL;
    Slider::onTouchCancelled(touch, unusedEvent);
    
    if(_callback){
        _callback(this,_ratio,_touchEvent);
    }
}