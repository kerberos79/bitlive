//
//  ChoiceDialog.cpp
//  Vista
//
//  Created by kerberos on 1/22/15.
//
//

#include "ChoiceDialog.h"


Color4B ChoiceDialog::BackgroundColor;
Color3B ChoiceDialog::FontColor;
int ChoiceDialog::FontSize = 0;
int ChoiceDialog::ItemHeight = 0;

ChoiceDialog::~ChoiceDialog()
{
}

ChoiceDialog * ChoiceDialog::create(MainScene *mainscene, std::vector<std::string>* list, std::function<void(Ref*)> callback)
{
    ChoiceDialog * dialog = new (std::nothrow) ChoiceDialog();
    if( dialog && dialog->init(mainscene, list, callback))
    {
        dialog->setPosition(Vec2::ZERO);
        dialog->autorelease();
        return dialog;
    }
    CC_SAFE_DELETE(dialog);
    return nullptr;
}


void ChoiceDialog::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& choiceDialog = DICTOOL->getSubDictionary_json(doc, "choice_dialog");
    ChoiceDialog::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(choiceDialog, "background_color"));
    ChoiceDialog::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(choiceDialog, "font_color"));
    ChoiceDialog::FontSize = DICTOOL->getIntValue_json(choiceDialog, "font_size") * MainScene::LayoutScale;
    ChoiceDialog::ItemHeight = DICTOOL->getIntValue_json(choiceDialog, "item_height") * MainScene::LayoutScale;
}

bool ChoiceDialog::init(MainScene *mainscene, std::vector<std::string>* list, std::function<void(Ref*)> callback)
{
    mMainScene = mainscene;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    ssize_t count = list->size();
    float height = (float)(ChoiceDialog::ItemHeight * count);
    Vec2 position = Vec2(visibleSize.width * 0.5f, (visibleSize.height + height) * 0.5f);
    // add custom item
    int i=0;
    for (std::vector<std::string>::iterator itor = list->begin(); itor != list->end(); ++itor, ++i)
    {
        if(i==0)
        {
            _username =*itor;
        }
        else
        {
            position.y -= ChoiceDialog::ItemHeight;
            Button* custom_button = Button::create("cell_normal.png", "cell_select.png");
            custom_button->setScale9Enabled(true);
            custom_button->setTitleText(itor->c_str());
            custom_button->setTitleFontSize(ChoiceDialog::FontSize);
            custom_button->setTitleColor(ChoiceDialog::FontColor);
            custom_button->setPosition(position);
            custom_button->addClickEventListener(callback);
            
            this->addChild(custom_button);
        }
    }
    
    if(!initWithColor(Color4B(0x0, 0, 0, 0x0), visibleSize.width, visibleSize.height))
        return false;
    
    _CustomListener = EventListenerTouchOneByOne::create();
    _CustomListener->retain();
    _CustomListener->setSwallowTouches(true);
    _CustomListener->onTouchBegan = CC_CALLBACK_2(ChoiceDialog::onTouchBegan, this);
    _CustomListener->onTouchMoved = CC_CALLBACK_2(ChoiceDialog::onTouchMoved, this);
    _CustomListener->onTouchEnded = CC_CALLBACK_2(ChoiceDialog::onTouchEnded, this);
    _CustomListener->onTouchCancelled = CC_CALLBACK_2(ChoiceDialog::onTouchCancelled, this);
    
    _touchListener = _CustomListener;
    return true;
}

bool ChoiceDialog::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    return true;
}

void ChoiceDialog::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
}

void ChoiceDialog::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
}

void ChoiceDialog::onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
}
void ChoiceDialog::show(bool isShow)
{
    if(isShow)
    {
        _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener, this);
    }
    else
    {
        _eventDispatcher->removeEventListener(_touchListener);
    }
    this->setVisible(isShow);
}
