//
//  CaptureMenu.h
//  BitLive
//
//  Created by keros on 8/31/15.
//
//

#ifndef __BitLive__CaptureMenu__
#define __BitLive__CaptureMenu__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Cocos2dxProfile.h"
#include "view/MainScene.h"
#include "view/LayerColorExtension.h"
#include "datatype/MainState.h"
#include "util/HexString.h"
#include "gif/CacheGif.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocos2d::experimental::ui;

class MainScene;
class CaptureMenu : public LayerColor, public LayerColorExtension
{
public:
    enum class MediaType
    {
        None = 0,
        Jpeg,
        Gif,
        Movie,
    };
    ~CaptureMenu();
    static Color4B BackgroundColor1;
    static Color4B BackgroundColor2;
    static Color3B FontColor;
    static int FontSize;
    static int Height;
    static std::string PlayButtonNormal;
    static std::string SaveButtonNormal;
    static std::string ShareButtonNormal;
    static std::string CancelButtonNormal;
    static std::string LoadingNormal;
    
    static CaptureMenu* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    virtual void show(bool isShow);
    virtual cocos2d::Size& getSize();
    virtual Color4B& getBackgroundColor();
    virtual void setBackgroundColor(Color4B& color);
    void setImage(Image* image, std::string filename, cocos2d::Size scaled_size =  Size(0,0));
    void updateGif(std::string filename, int width, int height);
    void updateMP4(bool complete);
    void updateMediaType(MediaType type);
    bool isPlaying();
private:
    bool init(MainScene* mainscene);
    void OnPlayButtonClick(Ref* pSender);
    void OnSaveButtonClick(Ref* pSender);
    void OnShareButtonClick(Ref* pSender);
    void OnCancelButtonClick(Ref* pSender);
    void videoEventCallback(Ref* sender, VideoPlayer::EventType eventType);
    void afterInvisibleAction(Node* sender);
    void update(float dt);
    
    MainScene* mMainScene;
    Button *mPlayButton;
    Button *mSaveButton;
    Button *mShareButton;
    Button *mCancelButton;
    Label*  mSaveText;
    Label*  mShareText;
    Label*  mCancelText;
    Sprite* mLoadingSprite;
    LayerGradient*  mMainLayout;
    Action *_visibleAction;
    MainState currentState;
    bool    _isHiding;
    VideoPlayer* mVideoPlayer;
    MediaType mFileType;
    VideoPlayer::EventType mCurrentVideoState;
    Image*  mCapturedImage;
    std::string mCaptureName;
    Sprite* mCaptureSprite;
    CacheGif* mCaptureGif;
    Texture2D* mCaptureTexture;
    Size mVisibleSize;
    Size mScaledImageSize;
    float mScale;
    bool _isSaved;
    float mLoadingCount;
};

#endif /* defined(__BitLive__CaptureMenu__) */
