//
//  InfiniteProgressBar.h
//  BitLive
//
//  Created by keros on 6/15/15.
//
//

#ifndef __BitLive__InfiniteProgressBar__
#define __BitLive__InfiniteProgressBar__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "extra/Strings.h"
#include "datatype/UIMessage.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;

class InfiniteProgressBar : public LayerColor
{
public:
    ~InfiniteProgressBar();
    static InfiniteProgressBar *create();
    void startAction();
    void removeAction();
private:
    
    bool init();
    void onRepeat();
    
    Sprite*     _progressBarTrack;
    
};
#endif /* defined(__BitLive__InfiniteProgressBar__) */
