//
//  EmoticonMenuButton.h
//  AnicameraMAC
//
//  Created by kerberos on 11/17/14.
//
//

#ifndef __AnicameraMAC__EmoticonMenuButton__
#define __AnicameraMAC__EmoticonMenuButton__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class EmoticonMenuButton : public Button
{
public:
    
    virtual ~EmoticonMenuButton();
    
    static EmoticonMenuButton* create();
    bool init();
    void setResource(const std::string &resouce);
    std::string& getResource();
    void setResourcePosition(const float x, const float y);
    Vec2 getResourcePosition();
private:
    std::string _resource;
    Vec2 _resourcePos;
};

#endif /* defined(__AnicameraMAC__EmoticonMenuButton__) */
