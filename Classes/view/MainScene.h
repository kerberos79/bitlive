//
//  MainScene.h
//  Vist
//
//  Created by kerberos on 1/17/15.
//
//

#ifndef __Vist__MainScene__
#define __Vist__MainScene__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/DictionaryHelper.h"
#include "DecodeScreen.h"
#include "PreviewScreen.h"
#include "ToggleButton.h"
#include "EmoticonMenuButton.h"
#include "TimerSprite.h"
#include "SliderEx.h"
#include "ContactDialog.h"
#include "InviteDialog.h"
#include "InstallPage.h"
#include "view/MainMenu.h"
#include "view/WarningDialog.h"
#include "view/Alert.h"
#include "view/EffectMenu.h"
#include "view/EmoticonMenu.h"
#include "view/SettingMenu.h"
#include "view/RecordSettingMenu.h"
#include "view/CaptureMenu.h"
#include "view/ExitMenu.h"
#include "view/HangupMenu.h"
#include "view/LicenseDialog.h"
#include "view/WebBrowser.h"
#include "view/Toast.h"
#include "view/ViewFinder.h"
#include "view/LayerColorExtension.h"
#include "controller/XmppLoginThread.h"
#include "controller/CallStateHandler.h"
#include "datatype/InviteEventData.h"
#include "datatype/UIMessage.h"
#include "datatype/MainState.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocostudio;

#define   DEGREE0   0
#define   DEGREE90  90
#define   DEGREE180 180
#define   DEGREE270 270

#define   MOVE_MENU_DURATION 0.2
#define   ROTATE_BUTTON_DURATION 0.5

#define   ZORDER_DECODE_SCREEN              1
#define   ZORDER_PREVIEW_SCREEN             10
#define   ZORDER_VIEW_FINDER                90
#define   ZORDER_TOPMENU_LAYER              100
#define   ZORDER_MAINMENU_LAYER             101
#define   ZORDER_SUBMENU_LAYER              102
#define   ZORDER_INSTALL_LAYER              110
#define   ZORDER_DIALOG_LAYER               111
#define   ZORDER_TOAST                      200
#define   ZORDER_LOGO                       300

#define ZORDER_STICKER          1
#define ZORDER_EMOTICON         2
#define ZORDER_DECORATION       3
#define ZORDER_OUTLINE          4
#define ZORDER_WATERMARK        5

#define   ACTION_SLIDE_MOVE_DELAY       0.2

class MainMenu;
class CallMenu;
class EffectMenu;
class EmoticonMenu;
class SettingMenu;
class RecordSettingMenu;
class CaptureMenu;
class ExitMenu;
class HangupMenu;
class InstallPage;
class InviteDialog;
class ContactDialog;
class WarningDialog;
class LicenseDialog;
class PreviewScreen;
class DecodeScreen;
class WebBrowser;
class Toast;
class Alert;
class ViewFinder;

class MainScene : public cocos2d::LayerColor
{
public:
        
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    // implement the "static create()" method manually
    CREATE_FUNC(MainScene);
    
    static Color4B BackgroundColor;
    static int SmallPreviewSize;
    static int BottomMenuHeight;
    static int SubMenuHeight;
    static int CameraWidth;
    static int CameraHeight;
    static int PreviewWidth;
    static int PreviewHeight;
    static int OriginDecodeWidth;
    static int OriginDecodeHeight;
    static float LayoutScale;
        
    ~MainScene();
    virtual bool init();
    void onAcceleration(Acceleration *acc, cocos2d::Event *event);
    void OnLoginCallback(bool success);
    void OnMultipleContactsCallback(std::vector<std::string>*);
    void OnInstallCallback(bool show);
    void OnInviteCallback(InviteEventData::Type type, std::string& username);
    void OnFaceDetectCallback(Vec2& left, Vec2& right);
    void OnCompleteRecordCallback(bool result);
    void sendUIMessage(UIMessage* message);
        
    void toggleMenuCallback(LayerColorExtension* layer);
    void toggleMainMenu(cocos2d::Point& p);
    void menuRotateCallback(Ref* pSender);
    void menuEffectCallback(Ref* pSender);
    void menuEmoticonCallback(Ref* pSender);
    void menuSettingCallback(Ref* pSender);
    void menuRecordSettingCallback(Ref* pSender);
    void menuContractCallback(Ref* pSender);
    void menuCallCallback(Ref* pSender);
    void menuWebCallback(Ref* pSender);
    void menuExitCallback(Ref* pSender);
    void menuHangupCallback(Ref* pSender);
    void menuWarningCallback(Ref* pSender);
    void menuSetEffectCallback(std::string program);
    void menuSetEmoticonCallback(Ref* pSender);
    void menuSetStickerCallback(Ref* pSender);
    void menuSendLinkURLCallback(std::string url);
    void menuCaptureCallback(Ref* pSender);
    void menuRecordCallback(Ref* pSender);
    void showWebBrowser(std::string url);
    void choiceItemCallback(Ref* pSender);
    void OnExit();
    void OnHangup();
    void OnSignout();
    void OnJoin();
    void OnLicenseDialogView();
    void OnChangeScreenLayout();
    void OnEnableRecording(bool enable);
    void showToast(UIMessage::Toast type, std::string message);
    void ChangeState(MainState state);
    void ChangeRecordType(UIMessage::Record type);
    void ChangeRecordTimeout(int timeout);
    bool isCallingState();
    bool isRecordingState();
    DecodeScreen* getDecodeScreen() {
        return mDecodeScreen;
    }
    PreviewScreen* getPreviewScreen() {
        return mPreviewScreen;
    }
    MainMenu* getMainMenu() {
        return mMainMenu;
    }
    bool isOfflineMode()
    {
        return mOfflineMode;
    }
    void setOfflineMode(bool enable);
    void afterCaptureCallback(RenderTexture*, const std::string&);
    void afterRecordFirstFrame(std::string filename);
    void showAlert(std::string message, UIMessage::Alert type);
    bool isVideoPlayerPlaying();
    bool showInviteDialog();
    void resetViewFinder();
    void removeInstallPage();
private:
    virtual void update(float dt);
    void createCustomEvent();
    bool loadLayoutFromJson();
    void createDecodeScreen();
    void createPreviewScreen();
    void createSubMenu();
    void createContactDialog();
    void updateContactDialog(std::vector<std::string>*);
    void createInstallPage();
    void showInstallPage();
    void updateInviteDialog(InviteEventData::Type type, ContactInfo* info);
    void showInviteDialog(InviteEventData::Type type, ContactInfo* info=nullptr);
    void hideInviteDialog(InviteEventData::Type type);
    void showWarningDialog(UIMessage::Cmd type);
    void createLicenseDialog();
    void createWebBrowser();
    void createToast();
    
    void rotateButton(int newRotation);
    void onSetDecodeEffect(std::string name);
        
    std::vector<Button *> mEffectButtonList;
    InstallPage* mInstallScreen;
    DecodeScreen *mDecodeScreen;
    PreviewScreen *mPreviewScreen;
    ToggleButton *toggledButtonOnRightMenu;
    LicenseDialog *mLicenseDialog;
    WarningDialog *mWarningDialog;
    Alert *mAlert;
    ContactDialog* mContactDialog;
    InviteDialog* mInviteDialog;
    EffectMenu *mEffectMenu;
    EmoticonMenu *mEmoticonMenu;
    SettingMenu *mSettingMenu;
    RecordSettingMenu *mRecordSettingMenu;
    CaptureMenu   *mCaptureMenu;
    WebBrowser *mWebBrowser;
    ExitMenu   *mExitMenu;
    HangupMenu *mHangupMenu;
    MainMenu   *mMainMenu;
    Toast      *mToast;
    ViewFinder *mViewFinder;
    LayerColorExtension *mSelectedLayer;
    Layer      *mLogoLayer;
    Sprite     *mLogoSprite;
    int mCurrentDirection;
    experimental::ui::WebView* webview;
    // layout
    Size mLayerSize;
    MainState mCurrentState;
    UIMessage::Record mCurrentRecordType;
    bool mOfflineMode;
    static const int dialogTag = 8888;
};
#endif /* defined(__Vist__MainScene__) */
