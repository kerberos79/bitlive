//
//  MainMenu.h
//  BitLive
//
//  Created by keros on 6/11/15.
//
//

#ifndef __BitLive__MainMenu__
#define __BitLive__MainMenu__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Cocos2dxProfile.h"
#include "view/MainScene.h"
#include "view/LayerColorExtension.h"
#include "datatype/MainState.h"
#include "util/HexString.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;

class MainScene;
class MainMenu : public LayerGradient, public LayerColorExtension
{
public:
    ~MainMenu();
    static Color4B BackgroundColor1;
    static Color4B BackgroundColor2;
    static Color3B FontColor;
    static int FontSize;
    static int Height;
    static int SubMenuHeight;
    static int RecTimeout;
    static std::string ContactButtonNormal;
    static std::string WebButtonNormal;
    static std::string EffectButtonNormal;
    static std::string EmoticonButtonNormal;
    static std::string SettingButtonNormal;
    static std::string FlipButtonNormal;
    static std::string CallButtonNormal;
    static std::string CallButtonSelect;
    static std::string HomeButtonNormal;
    static std::string HomeButtonSelect;
    static std::string HomeButtonDisable;
    static std::string FormatButtonNormal;
    static std::string FormatButtonSelect;
    static std::string RecordButtonNormal;
    static std::string RecordButtonSelect;
    static std::string RecordButtonDisable;
    static std::string ExitButtonNormal;
    static std::string ExitButtonDisable;
    static std::string HangupButtonNormal;
    static std::string CaptureButtonNormal;
    static std::string CaptureButtonDisable;
    
    static MainMenu* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    void setState(MainState state);
    virtual void show(bool isShow);
    virtual cocos2d::Size& getSize();
    virtual Color4B& getBackgroundColor();
    virtual void setBackgroundColor(Color4B& color);
    void stopRecording();
    bool isRecordingState()
    {
        return (currentState==MainState::Recording);
    }
    bool isRecording()
    {
        return _isRecording;
    }
    void setTimeout(int timeout);
    void menuRecordCallback(Ref* pSender);
private:
    bool init(MainScene* mainscene);
    void onTouchCallback(Ref* pSender, Widget::TouchEventType eventType);
    void afterVisibleAction(Node* sender);
    void afterInvisibleAction(Node* sender);
    void menuCaptureCallback(Ref* pSender);
    void menuHomeCallback(Ref* pSender);
    void menuFormatCallback(Ref* pSender);
    void startRecordAnimation();
    void stopRecordAnimation();
    void createLayout();
    void createLayoutWithLabel();
    
    MainScene* mMainScene;
    Layout*  mMainLayout;
    Layout*  mCallLayout;
    Layout*  mRecordLayout;
    Button *mContactButton;
    Button *mWebButton;
    Button *mEffectButton;
    Button *mExitButton;
    Button *mHangupButton;
    Button *mRecordSettingButton;
    ToggleButton *mCaptureButton;
    ToggleButton *mRecordButton;
    ToggleButton *mHomeButton;
    ToggleButton *mCurrentTabButton;
    ToggleButton *mFormatButton;
    Action *_visibleHomeAction, *_invisibleHomeAction;
    Action *_visibleRecordAction, *_invisibleRecordAction;
    MainState currentState;
    bool    _isHiding;
    bool    _isRecording;
};
#endif /* defined(__BitLive__MainMenu__) */
