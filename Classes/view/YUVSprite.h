//
//  YUVSprite.h
//  AnicameraMAC
//
//  Created by kerberos on 12/3/14.
//
//

#ifndef __AnicameraMAC__YUVSprite__
#define __AnicameraMAC__YUVSprite__

#include "cocos2d.h"
#include "util/GLProgramManager.h"

USING_NS_CC;
class YUVSprite : public Sprite
{
public:
    ~YUVSprite();
    static YUVSprite* create(int org_width, int org_height, int dst_width, int dst_height);
    bool init(int org_width, int org_height, int dst_width, int dst_height);
    virtual void setGLProgram(GLProgram *glprogram, Size size);
    void setGLProgram(GLProgramInfo* programInfo, cocos2d::Size size);
    void updateDirect( unsigned char *updateBuffer);
    void updateDirect(unsigned char *buf_y, unsigned char *buf_u, unsigned char *buf_v);
    void updateTime();
    void updateFaceDetect(Vec2& facePos, float faceScale);
    virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags);
    
    unsigned char* yuvBuffer;
    
private:
    Texture2D *pTexture0;
    Texture2D *pTexture1;
    Texture2D *pTexture2;
    
    int y_width, y_height;
    int uv_width, uv_height;
    int u_offset, v_offset;
    int y_size, uv_size;
    unsigned char *_initBufferY;
    unsigned char *_initBufferU;
    unsigned char *_initBufferV;
    float seconds;
};
#endif /* defined(__AnicameraMAC__YUVSprite__) */
