//
//  EffectMenu.cpp
//  BitLive
//
//  Created by keros on 6/14/15.
//
//

#include "EffectMenu.h"

Color4B EffectMenu::TitleBackgroundColor;
Color4B EffectMenu::BodyBackgroundColor;
Color3B EffectMenu::TitleFontColor;
Color3B EffectMenu::BodyFontColor;
std::string EffectMenu::FontType;
int EffectMenu::FontSize;
int EffectMenu::TitleHeight;
int EffectMenu::Height;
int EffectMenu::ItemWidth;
int EffectMenu::ItemHeight;
std::string EffectMenu::CloseButton;
std::string EffectMenu::PreviousButtonNormal;
std::string EffectMenu::PreviousButtonSelect;
std::string EffectMenu::OriginButtonNormal;
std::string EffectMenu::OriginButtonSelect;
std::string EffectMenu::ExposureSprite;
std::string EffectMenu::ExposureSliderTrack;
std::string EffectMenu::ExposureSliderThumb;
std::string EffectMenu::ExposureSliderProgress;

EffectMenu::~EffectMenu()
{
}

EffectMenu * EffectMenu::create(MainScene* mainscene)
{
    EffectMenu * menu = new (std::nothrow) EffectMenu();
    if( menu && menu->init(mainscene))
    {
        menu->setPosition(Vec2::ZERO);
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void EffectMenu::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& effectMenu = DICTOOL->getSubDictionary_json(doc, "effect_menu");
    EffectMenu::TitleBackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(effectMenu, "title_background_color"));
    EffectMenu::BodyBackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(effectMenu, "body_background_color"));
    EffectMenu::TitleFontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(effectMenu, "title_font_color"));
    EffectMenu::BodyFontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(effectMenu, "body_font_color"));
    EffectMenu::FontType = DICTOOL->getStringValue_json(effectMenu, "font_type");
    EffectMenu::FontSize = DICTOOL->getIntValue_json(effectMenu, "font_size") * MainScene::LayoutScale;
    EffectMenu::TitleHeight = DICTOOL->getIntValue_json(effectMenu, "title_height") * MainScene::LayoutScale;
    EffectMenu::Height = DICTOOL->getIntValue_json(effectMenu, "height") * MainScene::LayoutScale;
    EffectMenu::ItemWidth = DICTOOL->getIntValue_json(effectMenu, "item_width") * MainScene::LayoutScale;
    EffectMenu::ItemHeight = DICTOOL->getIntValue_json(effectMenu, "item_height") * MainScene::LayoutScale;
    EffectMenu::CloseButton = DICTOOL->getStringValue_json(effectMenu, "close_button");
    EffectMenu::PreviousButtonNormal = DICTOOL->getStringValue_json(effectMenu, "previous_button_normal");
    EffectMenu::PreviousButtonSelect = DICTOOL->getStringValue_json(effectMenu, "previous_button_select");
    EffectMenu::OriginButtonNormal = DICTOOL->getStringValue_json(effectMenu, "origin_button_normal");
    EffectMenu::OriginButtonSelect = DICTOOL->getStringValue_json(effectMenu, "origin_button_select");
    EffectMenu::ExposureSprite = DICTOOL->getStringValue_json(effectMenu, "exposure_sprite");
    EffectMenu::ExposureSliderTrack = DICTOOL->getStringValue_json(effectMenu, "exposure_slider_track");
    EffectMenu::ExposureSliderThumb = DICTOOL->getStringValue_json(effectMenu, "exposure_slider_thumb");
    EffectMenu::ExposureSliderProgress = DICTOOL->getStringValue_json(effectMenu, "exposure_slider_progress");
}

bool EffectMenu::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    // Read content from file
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("filter_menu_item_list.json");
    
    rapidjson::Document doc;
    
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    mVisibleSize = Director::getInstance()->getVisibleSize();
    
    auto closeButton = Button::create();
    closeButton->loadTextures(EffectMenu::CloseButton, "", "");
    closeButton->setAnchorPoint(Vec2(1.0, 0.5));
    closeButton->setPosition(Vec2(mVisibleSize.width, EffectMenu::TitleHeight * 0.5f));
    closeButton->setTouchEnabled(true);
    closeButton->addClickEventListener(CC_CALLBACK_1(EffectMenu::menuCloseCallback,this));
    
    int width;
    mPreviousButton = ToggleButton::create();
    mPreviousButton->loadTextures(EffectMenu::PreviousButtonNormal, EffectMenu::PreviousButtonSelect, "");
    mPreviousButton->setAnchorPoint(Vec2(0, 0.5));
    mPreviousButton->setPosition(Vec2(0, EffectMenu::TitleHeight * 0.5f));
    mPreviousButton->setTouchEnabled(true);
    mPreviousButton->addClickEventListener(CC_CALLBACK_1(EffectMenu::menuPreviousCallback,this));
    mPreviousButton->onNormal();
    
    width = mPreviousButton->getContentSize().width;
    
    mOriginButton = Button::create();
    mOriginButton->loadTextures(EffectMenu::OriginButtonSelect, "", "");
    mOriginButton->setAnchorPoint(Vec2(0, 0.5));
    mOriginButton->setPosition(Vec2(width, EffectMenu::TitleHeight * 0.5f));
    mOriginButton->setTouchEnabled(true);
    mOriginButton->addClickEventListener(CC_CALLBACK_1(EffectMenu::menuNoEffectCallback,this));
    
    width += mOriginButton->getContentSize().width;
    
    auto exposureSprite = Sprite::create(EffectMenu::ExposureSprite);
    exposureSprite->setAnchorPoint(Vec2(0, 0.5));
    exposureSprite->setPosition(Vec2(width , EffectMenu::TitleHeight * 0.5f));
    width += exposureSprite->getContentSize().width;
    
    mExposureSlider = SliderEx::create();
    mExposureSlider->setAnchorPoint(Vec2(0, 0.5));
    mExposureSlider->loadBarTexture(EffectMenu::ExposureSliderTrack);
    mExposureSlider->loadSlidBallTextures(EffectMenu::ExposureSliderThumb, EffectMenu::ExposureSliderThumb, "");
    mExposureSlider->loadProgressBarTexture(EffectMenu::ExposureSliderProgress);
    mExposureSlider->setPosition(Vec2(width , EffectMenu::TitleHeight * 0.5f));
    mExposureSlider->setCallBack([&](SliderEx* sender,float ratio,SliderEx::TouchEvent event){
        switch(event){
            case SliderEx::TouchEvent::MOVE:
                Device::setExposureCamera((int)(ratio * 100));
                break;
            case SliderEx::TouchEvent::DOWN:
            case SliderEx::TouchEvent::UP:
            case SliderEx::TouchEvent::CANCEL:
                break;
        }
    });
    
    scrollViewPos = 0;
    scrollViewHeight = EffectMenu::Height - EffectMenu::TitleHeight;
    auto titleLayer = LayerColor::create(EffectMenu::TitleBackgroundColor, mVisibleSize.width, EffectMenu::TitleHeight);
    titleLayer->setAnchorPoint(Vec2::ZERO);
    titleLayer->setPosition(0, scrollViewHeight);
    titleLayer->addChild(closeButton);
    titleLayer->addChild(mPreviousButton);
    titleLayer->addChild(mOriginButton);
    titleLayer->addChild(exposureSprite);
    titleLayer->addChild(mExposureSlider);
    
    _rootScrollView = ui::ScrollView::create();
    _rootScrollView->setContentSize(Size(mVisibleSize.width, scrollViewHeight));
    _rootScrollView->setBackGroundColorType(ui::ScrollView::BackGroundColorType::SOLID);
    _rootScrollView->setBackGroundColor(Color3B(0xff, 0xff, 0xff));
    _rootScrollView->setPosition(Vec2::ZERO);
    _rootScrollView->setDirection(ListView::Direction::HORIZONTAL);
    _rootScrollView->setBounceEnabled(false);
    
    int listCount = DICTOOL->getArrayCount_json(doc, "list");
    
    int totalWidth = 0;
    Button *button;
    const char *name;
    for(int i=0;i<listCount; i++)
    {
        const rapidjson::Value& list = DICTOOL->getDictionaryFromArray_json(doc, "list", i);
        name = DICTOOL->getStringValue_json(list, "name");
        
        button = Button::create();
        button->loadTextures(std::string(name)+".png", "", "");
        button->setTag(i);
        button->setName(name);
        button->setScale(MainScene::LayoutScale);
        button->setPosition(Vec2(totalWidth + EffectMenu::ItemWidth * 0.53f, scrollViewHeight * 0.6f));
        button->setTouchEnabled(true);
        button->addClickEventListener(CC_CALLBACK_1(EffectMenu::menuRootCallback,this));
        
        auto label = Label::createWithTTF(name, Strings::getFontType(), FontSize);
        label->setColor(BodyFontColor);
        label->setPosition(Vec2(totalWidth + EffectMenu::ItemWidth * 0.53f, scrollViewHeight * 0.15f));
        
        _rootScrollView->addChild(button);
        _rootScrollView->addChild(label);
        totalWidth += EffectMenu::ItemWidth * 1.06f;
        
        int itemCount = DICTOOL->getArrayCount_json(list, "item");
        
        EffectMenuItem *menuItem = new EffectMenuItem();
        for(int j=0;j<itemCount;j++)
        {
            const char* item = DICTOOL->getStringValueFromArray_json(list, "item", j);
            menuItem->_list.push_back(std::string(item));
        }
        
        _menuList.push_back(menuItem);
    }
    _rootScrollView->setInnerContainerSize(Size(totalWidth, scrollViewHeight));
    
    
    if(!initWithColor(EffectMenu::BodyBackgroundColor, mVisibleSize.width, EffectMenu::Height))
        return false;
    this->addChild(titleLayer);
    this->addChild(_rootScrollView);
    _startPos = Vec2(0, -EffectMenu::Height);
    _endPos = Vec2::ZERO;
    _visibleAction = EaseExponentialOut::create(MoveTo::create(0.4f, _endPos));
    _visibleAction->retain();
    
    _invisibleAction = Sequence::create(EaseExponentialOut::create(MoveTo::create(0.4f, _startPos)),
                                CallFuncN::create( CC_CALLBACK_1(EffectMenu::afterInvisibleAction, this)),
                                        nullptr);
    _invisibleAction->retain();
    return true;
}

void EffectMenu::addSubView(int tag)
{
    int totalWidth = 0;;
    Button *button;
    auto scrollView = ui::ScrollView::create();
    scrollView->setContentSize(Size(mVisibleSize.width, scrollViewHeight));
    scrollView->setBackGroundColorType(ui::ScrollView::BackGroundColorType::SOLID);
    scrollView->setBackGroundColor(Color3B(0xff, 0xff, 0xff));
    scrollView->setPosition(Vec2(0, scrollViewPos));
    scrollView->setDirection(ListView::Direction::HORIZONTAL);
    scrollView->setBounceEnabled(false);
    scrollView->setVisible(false);
    
    std::vector<std::string>::iterator iterEnd = _menuList[tag]->_list.end();
    for(std::vector<std::string>::iterator iterPos = _menuList[tag]->_list.begin();
        iterPos != iterEnd;
        ++iterPos)
    {
        std::string item = *iterPos;
        button = Button::create();
        button->setName(item);
        button->loadTextures(std::string(item)+".png", "", "");
        button->setScale(MainScene::LayoutScale);
        button->setPosition(Vec2(totalWidth + EffectMenu::ItemWidth * 0.53f, scrollViewHeight * 0.6f));
        button->setTouchEnabled(true);
        button->addClickEventListener(CC_CALLBACK_1(EffectMenu::menuSubCallback,this));
        
        auto label = Label::createWithTTF(item, Strings::getFontType(), FontSize);
        label->setColor(BodyFontColor);
        label->setPosition(Vec2(totalWidth + EffectMenu::ItemWidth * 0.53f, scrollViewHeight * 0.15f));
        
        scrollView->addChild(button);
        scrollView->addChild(label);
        totalWidth += EffectMenu::ItemWidth * 1.06f;
    }
    scrollView->setInnerContainerSize(Size(totalWidth, scrollViewHeight));
    _menuList[tag]->_scrollView = scrollView;
    this->addChild(scrollView, 0);
}

void EffectMenu::menuCloseCallback(Ref* pSender)
{
    mMainScene->toggleMenuCallback(this);
}

void EffectMenu::menuRootCallback(Ref* pSender)
{
    Button *button = (Button *)pSender;
    
    EffectMenuItem * menuItem = _menuList[button->getTag()];
    if(menuItem->_scrollView == nullptr)
    {
        addSubView(button->getTag());
    }
    _subScrollView = menuItem->_scrollView;
    _subScrollView->setVisible(true);
    _rootScrollView->setVisible(false);
    mPreviousButton->onPressed();
}

void EffectMenu::menuSubCallback(Ref* pSender)
{
    Button *button = (Button *)pSender;
    mMainScene->menuSetEffectCallback(button->getName());
}

void EffectMenu::menuNoEffectCallback(Ref* pSender)
{
    mMainScene->menuSetEffectCallback("Origin");
}

void EffectMenu::menuPreviousCallback(Ref* pSender)
{
    if(_subScrollView)
    {
        _rootScrollView->setVisible(true);
        _subScrollView->setVisible(false);
        _subScrollView = nullptr;
    }
    ((ToggleButton*)pSender)->onNormal();
}

void EffectMenu::show(bool isShow)
{
    if(isShow)
    {
        mExposureSlider->setPercent((int)Device::getExposureCamera());
        this->stopAllActions();
        this->setVisible(true);
        this->setPosition(_startPos);
        this->runAction(_visibleAction);
    }
    else
    {
        this->runAction(_invisibleAction);
    }
}

void EffectMenu::afterInvisibleAction(Node* sender)
{
    this->setVisible(false);
}

void EffectMenu::setSelectedButton(ToggleButton* button)
{
    if(mSelectedButton!=button)
    {
        if(mSelectedButton)
            mSelectedButton->onNormal();
        mSelectedButton = button;
        mSelectedButton->onPressed();
        mSelectedEffect = mSelectedButton->getTitleText();
    }
}

cocos2d::Size& EffectMenu::getSize()
{
    return (cocos2d::Size&)this->getContentSize();
}

Color4B& EffectMenu::getBackgroundColor()
{
    return EffectMenu::TitleBackgroundColor;
}

void EffectMenu::setBackgroundColor(Color4B& color)
{
    _displayedColor.r = _realColor.r = color.r;
    _displayedColor.g = _realColor.g = color.g;
    _displayedColor.b = _realColor.b = color.b;
    _displayedOpacity = _realOpacity = color.a;
    
    updateColor();
}