//
//  ViewFinder.hpp
//  BitLive
//
//  Created by keros on 11/3/15.
//
//

#ifndef __BitLive__ViewFinder__
#define __BitLive__ViewFinder__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Cocos2dxProfile.h"
#include "view/MainScene.h"
#include "view/LayerColorExtension.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocos2d::experimental::ui;

class MainScene;
class ViewFinder : public Layout
{
public:
    static Color3B FontColor;
    static int FontSize;
    static std::string RecImage;
    static std::string GuideImage;
    static std::string CloseButtonNormal;
    
    ~ViewFinder();
    static ViewFinder* create(MainScene* mainscene);
    static void loadLayout(rapidjson::Document &doc);
    virtual void setVisible(bool visible);
    void start(bool enable);
    void resetCount();
    
    virtual bool onTouchBegan(Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchMoved(Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchEnded(Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchCancelled(Touch* touch, cocos2d::Event* unused_event);
private:
    bool init(MainScene* mainscene);
    void update(float dt);
    void onCloseButtonCallback(Ref* pSender);
    
    MainScene* mMainScene;
    Size mVisibleSize;
    Sprite* mRecSprite;
    LayerColor* mGuideLayout;
    Label* mCountText;
    float   mRecordCount;
    char    mRecordCountString[12];
    bool mFirstRecord;
};
#endif /* ViewFinder_h */
