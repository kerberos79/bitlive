//
//  WarningDialog.h
//  BitLive
//
//  Created by keros on 5/16/15.
//
//

#ifndef __BitLive__WarningDialog__
#define __BitLive__WarningDialog__

#include <stdio.h>

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "extra/Strings.h"
#include "datatype/UIMessage.h"
#include "view/MainScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;
class WarningDialog : public LayerColor
{
public:
    ~WarningDialog();
    static Color4B BackgroundColor;
    static Color3B FontColor;
    static int TitleFontSize;
    static int TextFontSize;
    static std::string OkButtonNormal;
    static std::string OkButtonSelect;
    static WarningDialog* create(MainScene *mainscene, UIMessage::Cmd type);
    static void loadLayout(rapidjson::Document &doc);
    bool addOkButtonClickListener(std::function<void(Ref*)> callback);
    void OnOkButtonClick(Ref* pSender);
    void show(bool isShow);
    void show(UIMessage::Cmd type);
    
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* unused_event);
private:
    bool init(MainScene *mainscene, UIMessage::Cmd type);
    MainScene *mMainScene;
    EventListenerTouchOneByOne *_CustomListener;
    LabelTTF *_title;
    LabelTTF *_body;
    Button* _okButton;
    UIMessage::Cmd _currentType;
};
#endif /* defined(__BitLive__WarningDialog__) */
