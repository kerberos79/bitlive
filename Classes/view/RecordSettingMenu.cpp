//
//  RecordSettingMenu.cpp
//  BitLive
//
//  Created by keros on 9/21/15.
//
//

#include "RecordSettingMenu.h"

Color4B RecordSettingMenu::TitleBackgroundColor;
Color4B RecordSettingMenu::BodyBackgroundColor;
Color3B RecordSettingMenu::TitleFontColor;
Color3B RecordSettingMenu::BodyFontColor;
int RecordSettingMenu::TitleFontSize;
int RecordSettingMenu::BodyFontSize;
int RecordSettingMenu::Height;
int RecordSettingMenu::TitleHeight;
Color3B RecordSettingMenu::ButtonFontColor;
int RecordSettingMenu::ButtonFontSize;
std::string RecordSettingMenu::CloseButton;
std::string RecordSettingMenu::JoinImage;
std::string RecordSettingMenu::TimerImage;
std::string RecordSettingMenu::RadioButtonNormal;
std::string RecordSettingMenu::RadioButtonSelect;

RecordSettingMenu::~RecordSettingMenu()
{
}

RecordSettingMenu * RecordSettingMenu::create(MainScene* mainscene)
{
    RecordSettingMenu * menu = new (std::nothrow) RecordSettingMenu();
    if( menu && menu->init(mainscene))
    {
        menu->setPosition(Vec2::ZERO);
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void RecordSettingMenu::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& settingMenu = DICTOOL->getSubDictionary_json(doc, "record_setting_menu");
    RecordSettingMenu::TitleBackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(settingMenu, "title_background_color"));
    RecordSettingMenu::BodyBackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(settingMenu, "body_background_color"));
    RecordSettingMenu::TitleFontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(settingMenu, "title_font_color"));
    RecordSettingMenu::BodyFontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(settingMenu, "body_font_color"));
    RecordSettingMenu::TitleFontSize = DICTOOL->getIntValue_json(settingMenu, "title_font_size") * MainScene::LayoutScale;
    RecordSettingMenu::BodyFontSize = DICTOOL->getIntValue_json(settingMenu, "body_font_size") * MainScene::LayoutScale;
    RecordSettingMenu::Height = DICTOOL->getIntValue_json(settingMenu, "height") * MainScene::LayoutScale;
    RecordSettingMenu::TitleHeight = DICTOOL->getIntValue_json(settingMenu, "title_height") * MainScene::LayoutScale;
    RecordSettingMenu::ButtonFontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(settingMenu, "button_font_color"));
    RecordSettingMenu::ButtonFontSize = DICTOOL->getIntValue_json(settingMenu, "button_font_size") * MainScene::LayoutScale;
    RecordSettingMenu::CloseButton = DICTOOL->getStringValue_json(settingMenu, "close_button");
    RecordSettingMenu::JoinImage = DICTOOL->getStringValue_json(settingMenu, "join_image");
    RecordSettingMenu::TimerImage = DICTOOL->getStringValue_json(settingMenu, "timer_image");
    RecordSettingMenu::RadioButtonNormal = DICTOOL->getStringValue_json(settingMenu, "radio_button_normal");
    RecordSettingMenu::RadioButtonSelect = DICTOOL->getStringValue_json(settingMenu, "radio_button_select");
}

bool RecordSettingMenu::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    // Read content from file
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("setting.json");
    
    rapidjson::Document doc;
    
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    auto titleLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "video_title"), Strings::getFontType(), TitleFontSize);
    titleLabel->setColor(TitleFontColor);
    titleLabel->setAnchorPoint(Vec2(0.0, 0.5));
    titleLabel->setPosition(Vec2(visibleSize.width*0.06f, RecordSettingMenu::TitleHeight*0.5f));
    
    auto closeButton = Button::create();
    closeButton->setAnchorPoint(Vec2(1.0, 0.5));
    closeButton->loadTextures(RecordSettingMenu::CloseButton, "", "");
    closeButton->setPosition(Vec2(visibleSize.width, RecordSettingMenu::TitleHeight*0.5f));
    closeButton->setTouchEnabled(true);
    closeButton->addClickEventListener(CC_CALLBACK_1(RecordSettingMenu::menuCloseCallback,this));
    
    float settingMenuHeight = RecordSettingMenu::Height - RecordSettingMenu::TitleHeight;
    auto titleLayer = LayerColor::create(RecordSettingMenu::TitleBackgroundColor, visibleSize.width, RecordSettingMenu::TitleHeight);
    titleLayer->setPosition(0, settingMenuHeight);
    titleLayer->addChild(closeButton);
    titleLayer->addChild(titleLabel);
    
    auto timerSprite = Sprite::create(RecordSettingMenu::TimerImage);
    timerSprite->setAnchorPoint(Vec2(0, 0.5));
    timerSprite->setPosition(Vec2(visibleSize.width * 0.06f, settingMenuHeight*0.8f));
    auto timerLabel = Text::create(DICTOOL->getStringValue_json(doc, "timeout"), Strings::getFontType(), BodyFontSize);
    timerLabel->setColor(BodyFontColor);
    timerLabel->setAnchorPoint(Vec2(0, 0.5));
    timerLabel->setPosition(Vec2(visibleSize.width * 0.14f,settingMenuHeight*0.8f));
    
    mTimeoutButton1 = ToggleButton::create();
    mTimeoutButton1->loadTextures(RecordSettingMenu::RadioButtonNormal, RecordSettingMenu::RadioButtonSelect, "");
    mTimeoutButton1->setPosition(Vec2(visibleSize.width * 0.2f,settingMenuHeight*0.6f));
    mTimeoutButton1->setTitleText(DICTOOL->getStringValue_json(doc, "timeout1"));
    mTimeoutButton1->setTitleColor(ButtonFontColor, ButtonFontColor);
    mTimeoutButton1->setTitleFontSize(ButtonFontSize);
    mTimeoutButton1->setTitleFontName(Strings::getFontType());
    mTimeoutButton1->addClickEventListener(CC_CALLBACK_1(RecordSettingMenu::menuTimeoutButton1Callback,this));
    mTimeoutButton1->onNormal();
    
    mTimeoutButton2 = ToggleButton::create();
    mTimeoutButton2->loadTextures(RecordSettingMenu::RadioButtonNormal, RecordSettingMenu::RadioButtonSelect, "");
    mTimeoutButton2->setPosition(Vec2(visibleSize.width * 0.5f,settingMenuHeight*0.6f));
    mTimeoutButton2->setTitleText(DICTOOL->getStringValue_json(doc, "timeout2"));
    mTimeoutButton2->setTitleColor(ButtonFontColor, ButtonFontColor);
    mTimeoutButton2->setTitleFontSize(ButtonFontSize);
    mTimeoutButton2->setTitleFontName(Strings::getFontType());
    mTimeoutButton2->addClickEventListener(CC_CALLBACK_1(RecordSettingMenu::menuTimeoutButton2Callback,this));
    mTimeoutButton2->onNormal();
    
    mTimeoutButton3 = ToggleButton::create();
    mTimeoutButton3->loadTextures(RecordSettingMenu::RadioButtonNormal, RecordSettingMenu::RadioButtonSelect, "");
    mTimeoutButton3->setPosition(Vec2(visibleSize.width * 0.8f,settingMenuHeight*0.6f));
    mTimeoutButton3->setTitleText(DICTOOL->getStringValue_json(doc, "timeout3"));
    mTimeoutButton3->setTitleColor(ButtonFontColor, ButtonFontColor);
    mTimeoutButton3->setTitleFontSize(ButtonFontSize);
    mTimeoutButton3->setTitleFontName(Strings::getFontType());
    mTimeoutButton3->addClickEventListener(CC_CALLBACK_1(RecordSettingMenu::menuTimeoutButton3Callback,this));
    mTimeoutButton3->onNormal();
    
    int record_timeout = UserDefault::getInstance()->getIntegerForKey("record_timeout", 15);
    if(record_timeout == 15)
    {
        mTimeoutButton1->onPressed();
    }
    else if(record_timeout == 120)
    {
        mTimeoutButton2->onPressed();
    }
    else
    {
        mTimeoutButton3->onPressed();
    }
    
    auto itemMenu = Menu::create();
    itemMenu->setContentSize(Size(visibleSize.width, SettingMenu::Height));
    itemMenu->setPosition(Vec2::ZERO);
    
    auto joinSprite = Sprite::create(RecordSettingMenu::JoinImage);
    joinSprite->setAnchorPoint(Vec2::ZERO);
    joinSprite->setPosition(Vec2(visibleSize.width * 0.06f,settingMenuHeight*0.3f));
    auto joinLabel = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "joinin"), Strings::getFontType(), BodyFontSize);
    joinLabel->setColor(BodyFontColor);
    auto joinMenu = MenuItemLabel::create(joinLabel, CC_CALLBACK_1(RecordSettingMenu::menuJoinCallback, this));
    joinMenu->setAnchorPoint(Vec2::ZERO);
    joinMenu->setPosition(Vec2(visibleSize.width * 0.14f, settingMenuHeight*0.3f));
    itemMenu->addChild(joinMenu);
    
    if(!initWithColor(RecordSettingMenu::BodyBackgroundColor, visibleSize.width, RecordSettingMenu::Height))
        return false;
    this->addChild(titleLayer);
    this->addChild(timerSprite);
    this->addChild(timerLabel);
    this->addChild(mTimeoutButton1);
    this->addChild(mTimeoutButton2);
    this->addChild(mTimeoutButton3);
    
    std::string firsttime = UserDefault::getInstance()->getStringForKey("firsttime");
    if(mMainScene->isOfflineMode() && !firsttime.compare("offline"))
    {
        this->addChild(joinSprite);
        this->addChild(itemMenu);
    }
    _startPos = Vec2(0, -RecordSettingMenu::Height);
    _endPos = Vec2::ZERO;
    _visibleAction = EaseExponentialOut::create(MoveTo::create(0.4f, _endPos));
    _visibleAction->retain();
    
    _invisibleAction = Sequence::create(EaseExponentialOut::create(MoveTo::create(0.4f, _startPos)),
                                        CallFuncN::create( CC_CALLBACK_1(RecordSettingMenu::afterInvisibleAction, this)),
                                        nullptr);
    _invisibleAction->retain();
    return true;
}

void RecordSettingMenu::menuCloseCallback(Ref* pSender)
{
    mMainScene->toggleMenuCallback(this);
}

void RecordSettingMenu::menuJoinCallback(Ref* pSender)
{
    mMainScene->toggleMenuCallback(this);
    mMainScene->OnJoin();
}

void RecordSettingMenu::menuTimeoutButton1Callback(Ref* pSender)
{
    mTimeoutButton1->onPressed();
    mTimeoutButton2->onNormal();
    mTimeoutButton3->onNormal();
    mMainScene->ChangeRecordTimeout(15);
    UserDefault::getInstance()->setIntegerForKey("record_timeout", 15);
}

void RecordSettingMenu::menuTimeoutButton2Callback(Ref* pSender)
{
    mTimeoutButton1->onNormal();
    mTimeoutButton2->onPressed();
    mTimeoutButton3->onNormal();
    mMainScene->ChangeRecordTimeout(120);
    UserDefault::getInstance()->setIntegerForKey("record_timeout", 120);
}

void RecordSettingMenu::menuTimeoutButton3Callback(Ref* pSender)
{
    mTimeoutButton1->onNormal();
    mTimeoutButton2->onNormal();
    mTimeoutButton3->onPressed();
    mMainScene->ChangeRecordTimeout(1200);
    UserDefault::getInstance()->setIntegerForKey("record_timeout", 1200);
}

void RecordSettingMenu::show(bool isShow)
{
    if(isShow)
    {
        this->stopAllActions();
        this->setVisible(true);
        this->setPosition(_startPos);
        this->runAction(_visibleAction);
    }
    else
    {
        this->runAction(_invisibleAction);
    }
}

void RecordSettingMenu::afterInvisibleAction(Node* sender)
{
    this->setVisible(false);
}

cocos2d::Size& RecordSettingMenu::getSize()
{
    return (cocos2d::Size&)this->getContentSize();
}

Color4B& RecordSettingMenu::getBackgroundColor()
{
    return RecordSettingMenu::TitleBackgroundColor;
}

void RecordSettingMenu::setBackgroundColor(Color4B& color)
{
    _displayedColor.r = _realColor.r = color.r;
    _displayedColor.g = _realColor.g = color.g;
    _displayedColor.b = _realColor.b = color.b;
    _displayedOpacity = _realOpacity = color.a;
    
    updateColor();
}

