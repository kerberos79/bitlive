//
//  Toast.cpp
//  BitLive
//
//  Created by kerberos on 7/28/15.
//
//

#include "Toast.h"

Color4B Toast::BackgroundColor;
Color3B Toast::FontColor;
int Toast::FontSize = 0;
int Toast::FontHeight = 0;
int Toast::Margin = 0;
int Toast::Height = 0;
std::string Toast::LinkSprite;
std::string Toast::CallSprite;
std::string Toast::CancelButtonNormal;

Toast::~Toast()
{
}

Toast * Toast::create(MainScene* mainscene)
{
    Toast * menu = new (std::nothrow) Toast();
    if( menu && menu->init(mainscene))
    {
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void Toast::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& toast = DICTOOL->getSubDictionary_json(doc, "toast");
    Toast::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(toast, "background_color"));
    Toast::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(toast, "font_color"));
    Toast::FontSize = DICTOOL->getIntValue_json(toast, "font_size") * MainScene::LayoutScale;
    Toast::FontHeight = DICTOOL->getIntValue_json(toast, "font_height") * MainScene::LayoutScale;
    Toast::Margin = DICTOOL->getIntValue_json(toast, "margin") * MainScene::LayoutScale;
    Toast::Height = DICTOOL->getIntValue_json(toast, "height") * MainScene::LayoutScale;
    Toast::LinkSprite = DICTOOL->getStringValue_json(toast, "link_sprite");
    Toast::CallSprite = DICTOOL->getStringValue_json(toast, "call_sprite");
    Toast::CancelButtonNormal = DICTOOL->getStringValue_json(toast, "cancel_button_normal");
}

bool Toast::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    
    _visibleSize = Director::getInstance()->getVisibleSize();
    float width = _visibleSize.width - Toast::Margin * 2.0f;
    _iconLink = Sprite::create(Toast::LinkSprite);
    _iconLink->setPosition(Vec2(_iconLink->getContentSize().width * 0.5f, Toast::Height * 0.5f));
    _iconCall = Sprite::create(Toast::CallSprite);
    _iconCall->setPosition(Vec2(_iconCall->getContentSize().width * 0.5f, Toast::Height * 0.5f));
    // cancel button
    auto cancelButton = Button::create();
    cancelButton->retain();
    cancelButton->loadTextures(Toast::CancelButtonNormal, "", "");
    cancelButton->setPosition(Vec2(width - cancelButton->getContentSize().width * 0.5f, Toast::Height*0.5f));
    cancelButton->setTouchEnabled(true);
    cancelButton->addClickEventListener(CC_CALLBACK_1(Toast::menuCloseCallback,this));
    
    // add title
    Size dimensions = Size(width - _iconLink->getContentSize().width - cancelButton->getContentSize().width, Toast::FontHeight);
    _titleText = LabelTTF::create("toast", Strings::getFontType(), Toast::FontSize, dimensions);
    _titleText->setAnchorPoint(Vec2(0.0f, _titleText->getAnchorPoint().y));
    _titleText->setColor(InviteDialog::FontColor);
    _titleText->setPosition(Vec2(_iconLink->getContentSize().width, Toast::Height * 0.5f));
    
    if(!initWithColor(Toast::BackgroundColor, width, Toast::Height))
        return false;
    
    _showPosition = Vec2( (_visibleSize.width - width) * 0.5f, _visibleSize.height - Toast::Height);
    _hidePosition = Vec2( (_visibleSize.width - width) * 0.5f, _visibleSize.height);
    this->addChild(_titleText);
    this->addChild(_iconLink);
    this->addChild(_iconCall);
    this->addChild(cancelButton);
    this->setPosition(_hidePosition);
    _isToastHidden = true;
    
    _CustomListener = EventListenerTouchOneByOne::create();
    _CustomListener->retain();
    _CustomListener->setSwallowTouches(false);
    _CustomListener->onTouchBegan = CC_CALLBACK_2(Toast::onTouchBegan, this);
    _CustomListener->onTouchEnded = CC_CALLBACK_2(Toast::onTouchEnded, this);
    
    _touchListener = _CustomListener;
    
    return true;
}

void Toast::update(float dt)
{
    ToastMessage* message = _messageList.front();
    switch(message->_type)
    {
        case UIMessage::Toast::LinkURL:
            _iconLink->setVisible(true);
            _iconCall->setVisible(false);
            _titleText->setString(message->_message);
            break;
        case UIMessage::Toast::Call:
            _iconLink->setVisible(false);
            _iconCall->setVisible(true);
            _titleText->setString(message->_message);
            break;
    }
    this->unscheduleUpdate();
}

void Toast::show(UIMessage::Toast type, std::string message)
{
    if(_isToastHidden)
    {
        _messageList.push_back(new ToastMessage(type, message));
        this->scheduleUpdate();
        _isToastHidden = false;
        this->runAction(MoveTo::create(0.2, _showPosition));
        _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener, this);
    }
    else
    {
        _messageList.push_back(new ToastMessage(type, message));
    }
}
void Toast::hide()
{
    ToastMessage* hide_message = _messageList.front();
    if(hide_message)
        delete hide_message;
    _messageList.pop_front();
    if(_messageList.empty())
    {
        _isToastHidden = true;
        this->runAction(MoveTo::create(0.2, _hidePosition));
        _eventDispatcher->removeEventListener(_touchListener);
    }
    else
    {
        ToastMessage* message = _messageList.front();
        
        switch(message->_type)
        {
            case UIMessage::Toast::LinkURL:
            _iconLink->setVisible(true);
            _iconCall->setVisible(false);
                _titleText->setString(message->_message);
                break;
            case UIMessage::Toast::Call:
                _iconLink->setVisible(false);
                _iconCall->setVisible(true);
                _titleText->setString(message->_message);
                break;
        }
    }
}

void Toast::menuCloseCallback(Ref* pSender)
{
    ToastMessage* message = _messageList.front();
    switch(message->_type)
    {
        case UIMessage::Toast::LinkURL:
            break;
        case UIMessage::Toast::Call:
            mMainScene->showInviteDialog();
            break;
    }
	hide();
}

bool Toast::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    cocos2d::Point p = touch->getLocation();
    
    if(p.y > (_visibleSize.height - Toast::Height))
    {
        ToastMessage* message = _messageList.front();
        switch(message->_type)
        {
            case UIMessage::Toast::LinkURL:
                mMainScene->showWebBrowser(message->_message);
                break;
            case UIMessage::Toast::Call:
                if(!mMainScene->showInviteDialog())
                    return true;
                break;
        }
        hide();
        return true;
    }
    else
        return false;
}

void Toast::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
}
