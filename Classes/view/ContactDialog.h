//
//  ContactDialog.h
//  BitLive
//
//  Created by keros on 7/15/15.
//
//

#ifndef __BitLive__ContactDialog__
#define __BitLive__ContactDialog__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Cocos2dxProfile.h"
#include "controller/CallStateHandler.h"
#include "extra/Strings.h"
#include "util/IdentityManager.h"
#include "view/MainScene.h"

#define CONTACT_ITEM_MAX  6
USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;
class ContactDialog : public LayerColor
{
public:
    enum class Type
    {
        POSITIVE,
        NEGATIVE,
        BOTH
    };
    ~ContactDialog();
    static Color4B BackgroundColor;
    static Color3B TitleFontColor;
    static Color3B FontColor;
    static int FontSize;
    static int Height;
    static int ItemHeight;
    static std::string ItemButtonNormalTop;
    static std::string ItemButtonSelectTop;
    static std::string ItemButtonNormal;
    static std::string ItemButtonSelect;
    static std::string CancelButtonNormal;
    static std::string CancelButtonSelect;
    static ContactDialog* create(MainScene *mainscene, std::vector<std::string>* list);
    static void loadLayout(rapidjson::Document &doc);
    void show(bool isShow);
    void afterInvisibleAction(Node* sender);
    
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* unused_event);
private:
    bool init(MainScene *mainscene, std::vector<std::string>* list);
    void menuSelectCallback(Ref* pSender);
    void OnCancelButtonClick(Ref* pSender);
    void update(float dt);
    MainScene* mMainScene;
    LayerColor* mContactLayer;
    EventListenerTouchOneByOne *_CustomListener;
    Size visibleSize;
    std::vector<std::string> _list;
    Action *_visibleAction, *_invisibleAction;
    Vec2  _startPos, _endPos;
};

#endif /* defined(__BitLive__ContactDialog__) */
