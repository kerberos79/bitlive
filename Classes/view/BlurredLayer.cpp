//
//  BlurredLayer.cpp
//  BitLive
//
//  Created by keros on 7/22/15.
//
//

#include "BlurredLayer.h"

BlurredLayer * BlurredLayer::create(MainScene *mainscene, int width, int height, int degree)
{
    BlurredLayer *ret = new (std::nothrow) BlurredLayer();
    
    if(ret && ret->init(mainscene, width, height, degree, Texture2D::PixelFormat::RGBA8888))
    {
        ret->autorelease();
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return nullptr;
}

bool BlurredLayer::init(MainScene *mainscene, int width, int height, int degree, Texture2D::PixelFormat eFormat)
{
    mMainScene = mainscene;
    this->setContentSize(Size(width, height));
    rtX = RenderTexture::create(width, height);
    rtX->retain();
    rtX->setAnchorPoint(Vec2::ZERO);
    rtX->setPosition(Vec2::ZERO);
    
    stepX = Sprite::createWithTexture(rtX->getSprite()->getTexture());
    stepX->retain();
    stepX->setAnchorPoint(Vec2::ZERO);
    stepX->setPosition(Vec2::ZERO);
    stepX->setFlippedY(true);
    
    auto glprogram1 = GLProgram::createWithFilenames("multiTexture.vsh", "yuv_vertical_blur.fsh");
    auto glprogram2 = GLProgram::createWithFilenames("multiTexture.vsh", "rgb_horizontal_blur.fsh");
    stepX->setGLProgram(glprogram1);
    this->setGLProgram(glprogram2);
    return initWithWidthAndHeight(width, height, eFormat, 0);
}

void BlurredLayer::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
    rtX->begin();

    // TODO : Draw
    
    rtX->end();

    begin();
    stepX->setTexture(rtX->getSprite()->getTexture());
    stepX->visit();
    end();
}