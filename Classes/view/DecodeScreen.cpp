//
//  DecodeScreen.cpp
//  Vista
//
//  Created by kerberos on 2/24/15.
//
//

#include "DecodeScreen.h"

DecodeScreen::~DecodeScreen()
{
    if(_stickerManager)
        delete _stickerManager;
    if(_decorationManager)
        delete _decorationManager;
}

DecodeScreen * DecodeScreen::create(MainScene *mainscene, int org_width, int org_height, int dst_width, int dst_height)
{
    DecodeScreen *ret = new (std::nothrow) DecodeScreen();
    
    if(ret && ret->init(mainscene, org_width, org_height, dst_width, dst_height, Texture2D::PixelFormat::RGBA8888))
    {
        ret->autorelease();
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return nullptr;
}

bool DecodeScreen::init(MainScene *mainscene, int org_width, int org_height, int dst_width, int dst_height, Texture2D::PixelFormat eFormat)
{
    mMainScene = mainscene;
    this->setContentSize(Size(dst_width, dst_height));
    mYUVSprite = YUVSprite::create(org_width, org_height, org_width, dst_height);
    mYUVSprite->retain();
    mYUVSprite->setScale(1.02);
    mYUVSprite->setPosition(Vec2(org_width * 0.5f, dst_height * 0.5f));
    
    rtX = RenderTexture::create(dst_width, dst_height);
    rtX->retain();
    
    stepX = Sprite::createWithTexture(rtX->getSprite()->getTexture());
    stepX->retain();
    stepX->setPosition(Point(dst_width * 0.5f, dst_height * 0.5f));
    stepX->setFlippedY(true);
    
    _outline = DrawNode::create();
    _outline->retain();
    _outline->drawLine(Vec2(2,2), Vec2(2, dst_height-2), Color4F::WHITE);
    _outline->drawLine(Vec2(2.0,dst_height-2), Vec2(dst_width-2, dst_height-2), Color4F::WHITE);
    _outline->drawLine(Vec2(dst_width-2,2), Vec2(dst_width-2, dst_height-2), Color4F::WHITE);
    _outline->drawLine(Vec2(2,2), Vec2(dst_width-12, 2), Color4F::WHITE);
    
    updatedFrame = false;
    _sticker = nullptr;
    return initWithWidthAndHeight(dst_width, dst_height, eFormat, 0);
}

void DecodeScreen::initGLProgram()
{
    _glProgramManager.load("program.json");
    _glProgramManager.add("Origin", "bb_rgb.fsh", "bb_low.fsh");
    this->setProgram("Origin");
}

void DecodeScreen::reserveProgram(std::string program)
{
    _reservedGlProgram = program;
}

std::string DecodeScreen::getReservedProgram()
{
    return _reservedGlProgram;
}

void DecodeScreen::setProgram(std::string program)
{
    _currentProgramInfo = _glProgramManager.getGLProgramInfo(program);
    
    _twoPassFilter = false;
    if(_currentProgramInfo!=nullptr)
    {
        mYUVSprite->setGLProgram(_currentProgramInfo, this->getContentSize());
        if(_currentProgramInfo->_program2)
        {
            stepX->setGLProgram(_currentProgramInfo->_program2);
            auto glprogramstate = stepX->getGLProgramState();
            glprogramstate->setUniformFloat("imageWidth", this->getContentSize().width);
            glprogramstate->setUniformFloat("imageHeight", this->getContentSize().height);
            glprogramstate->setUniformInt("radius", GLProgramManager::BiteralBlurRadius);
            _twoPassFilter = true;
        }
        setDecoration(_currentProgramInfo->_decoration);
        _twoPassFilter = (_currentProgramInfo->_program2!=nullptr);
    }
}

void DecodeScreen::rotate(std::string degree)
{
	Vec3 rotationSprite;
    std::vector<std::string> tokens;
    const std::string delimiters = ",";
    Tokenizer::tokenize(degree, tokens, delimiters);

    rotationSprite.x = std::stoi(tokens[0]);
    rotationSprite.y = std::stoi(tokens[1]);
    rotationSprite.z = std::stoi(tokens[2]);
}

void DecodeScreen::fitOnScreen(Size& parentLayerSize)
{
    _parentSreenSize = parentLayerSize;
    _fullScreenRatio = (float)parentLayerSize.width / (float)_contentSize.width;
    _fullScreenSize.width = parentLayerSize.width;
    _fullScreenSize.height = _contentSize.height * _fullScreenRatio;
    _fullScreenPosition = Vec2(_fullScreenSize.width * 0.5f, parentLayerSize.height - _fullScreenSize.height * 0.5f);
    
    _smallScreenSize.width = MainScene::SmallPreviewSize * 0.9f;
    _smallScreenSize.height = (_contentSize.height / _contentSize.width) * _smallScreenSize.width;
    _smallScreenRatio = _smallScreenSize.width / _contentSize.width;
    _smallScreenPosition[0].x = MainScene::SmallPreviewSize * 0.5f;
    _smallScreenPosition[0].y = _fullScreenPosition.y + _fullScreenSize.height * 0.5f - _smallScreenSize.height * 0.55f;
    _smallScreenPosition[1].x = _fullScreenSize.width - MainScene::SmallPreviewSize * 0.5f;
    _smallScreenPosition[1].y = _fullScreenPosition.y + _fullScreenSize.height * 0.5f - _smallScreenSize.height * 0.55f;
    _smallScreenPosition[2].x = _fullScreenSize.width - MainScene::SmallPreviewSize * 0.5f;
    _smallScreenPosition[2].y = _fullScreenPosition.y - _fullScreenSize.height * 0.5f + _smallScreenSize.height * 0.55f;
    _smallScreenPosition[3].x = MainScene::SmallPreviewSize * 0.5f;
    _smallScreenPosition[3].y = _fullScreenPosition.y - _fullScreenSize.height * 0.5f + _smallScreenSize.height * 0.55f;
    _smallScreenSector = 1;
    
    enterFullScreen();
    _stickerScale = 0.5;
    _stickerId = -1;
    this->unscheduleUpdate();
    
    auto eventListener = EventListenerTouchOneByOne::create();
    eventListener->setSwallowTouches(true);
    eventListener->onTouchBegan = CC_CALLBACK_2(DecodeScreen::onTouchBegan, this);
    eventListener->onTouchMoved = CC_CALLBACK_2(DecodeScreen::onTouchMoved, this);
    eventListener->onTouchEnded = CC_CALLBACK_2(DecodeScreen::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(eventListener, this);
}

void DecodeScreen::enterSmallScreen()
{
    _smallScreen = true;
    this->setScale(_smallScreenRatio, _smallScreenRatio);
    this->setPosition(_smallScreenPosition[_smallScreenSector]);
    this->addChild(_outline, ZORDER_OUTLINE);
}

void DecodeScreen::enterFullScreen()
{
    _smallScreen = false;
    this->setScale(_fullScreenRatio, _fullScreenRatio);
    this->setPosition(_fullScreenPosition);
    this->removeChild(_outline);
}

bool DecodeScreen::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if(_smallScreen)
    {
        float distance = this->getPosition().distance(touch->getLocation());
        if(distance < _smallScreenSize.height)
        {
            _touchOnScreen = true;
        }
        else
        {
            _touchOnScreen = false;
        }
    }
    else
    {
        _touchOnScreen = false;
    }
    return _touchOnScreen;
}

void DecodeScreen::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if(_smallScreen)
    {
        if(_touchOnScreen)
        {
            setPosition(this->getPosition().x + touch->getDelta().x,
                        this->getPosition().y + touch->getDelta().y);
        }
    }
}

void DecodeScreen::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if(_smallScreen)
    {
        if(_touchOnScreen)
        {
            uint32 time = rtc::Time();
            if( (time - _touchOldTime) < 300)
            {
                mMainScene->OnChangeScreenLayout();
            }
            else
            {
                selectSector(touch->getLocation());
                _touchOldTime = time;
            }
        }
    }
}

int DecodeScreen::getSmallScreenSector()
{
    return _smallScreenSector;
}

void DecodeScreen::setSmallScreenSector(int sector)
{
    _smallScreenSector = sector;
}

void DecodeScreen::selectSector(Vec2 p)
{
    float duration;
    const float standard = _fullScreenPosition.distance(Vec2(0,0));
    if(p.x < _fullScreenPosition.x && p.y < _fullScreenPosition.y)
    {
        _smallScreenSector = 3;
    }
    else if(p.x < _fullScreenPosition.x && p.y >= _fullScreenPosition.y)
    {
        _smallScreenSector = 0;
    }
    else if(p.x >= _fullScreenPosition.x && p.y < _fullScreenPosition.y)
    {
        _smallScreenSector = 2;
    }
    else
    {
        _smallScreenSector = 1;
    }
    duration = p.distance(_smallScreenPosition[_smallScreenSector]) / standard;
    auto moveAction = MoveTo::create(0.3 * duration, _smallScreenPosition[_smallScreenSector]);
    auto action = Sequence::create(moveAction,
                                   nullptr);
    this->runAction(action);
}

Size DecodeScreen::getScaledSize()
{
    return _fullScreenSize;
}

float DecodeScreen::getCurrentRatio()
{
    return (_smallScreen)? _smallScreenRatio : _fullScreenRatio;
}

Vec2 DecodeScreen::getCurrentPos()
{
    return (_smallScreen)? _smallScreenPosition[_smallScreenSector] : _fullScreenPosition;
}

void DecodeScreen::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
    if(updateDecodeFrame(mYUVSprite))
    {
        if(_currentProgramInfo->_face)
        {
            mYUVSprite->updateFaceDetect(_midPoint, _stickerScale);
            _decorationManager->updateFaceRect(_midPoint, _stickerScale, _stickerAngle);
        }
        else if(_currentProgramInfo->_time)
        {
            mYUVSprite->updateTime();
        }
        if(_twoPassFilter)
        {
            rtX->begin();
            mYUVSprite->visit();            
            rtX->end();
            
            begin();
            stepX->setTexture(rtX->getSprite()->getTexture());
            stepX->visit();
            
            sortAllChildren();
            for(const auto &child: _children)
            {
                if (child != _sprite)
                    child->visit();
            }
            end();
        }
        else
        {
            begin();
            mYUVSprite->visit();
            
            sortAllChildren();
            for(const auto &child: _children)
            {
                if (child != _sprite)
                    child->visit();
            }
            end();
        }
    }
}

void DecodeScreen::addEmoticonLayer()
{
    _emoticonLayer = Node::create();
    _emoticonLayer->retain();
    _emoticonLayer->setAnchorPoint(Vec2(0.5, 0.5));
    _emoticonLayer->setPosition(this->getContentSize()/2);
    _emoticonLayer->setContentSize(this->getContentSize());
    this->addChild(_emoticonLayer, ZORDER_EMOTICON);
    
    _emoticonManager = EmoticonManager::create(mMainScene, "emoticon.json", this->getContentSize());
    _emoticonManager->setParent(this);
    
    _stickerManager = StickerManager::create(mMainScene, "sticker.json", this->getContentSize());
    _decorationManager = DecorationManager::create(_emoticonLayer, this->getContentSize());
}

void DecodeScreen::setEmoticon(int tag)
{
    _emoticonManager->addSprite(tag);
}

void DecodeScreen::update(float dt)
{
    if(_stickerId>=0)
    {
        StickerInfo* stickerInfo;
        if((stickerInfo = _stickerManager->getDecodeItem(_stickerId))!=nullptr)
        {
            Sprite* sticker = stickerInfo->getSprite();
            if(_sticker != sticker)
            {
                if(_sticker)
                    this->removeChild(_sticker);
                _sticker = sticker;
                this->addChild(_sticker, ZORDER_STICKER);
                if(stickerInfo->_action)
                    _sticker->runAction(stickerInfo->_action);
            }
            _sticker->setPosition(_midPoint);
            _sticker->setScale(_stickerScale);
            _sticker->setRotation(_stickerAngle);
        }
    }
    this->unscheduleUpdate();
}

void DecodeScreen::setSticker(std::string param)
{
    if(param.length()==0)
    {
        this->removeChild(_sticker);
        _sticker = nullptr;
        return;
    }
    std::vector<std::string> tokens;
    const std::string delimiters = ",";
    Tokenizer::tokenize(param, tokens, delimiters);
    _stickerId = std::stoi(tokens[0]);
    _newLeftEye.x = std::stoi(tokens[1]);
    _newLeftEye.y = std::stoi(tokens[2]);
    _newRightEye.x = std::stoi(tokens[3]);
    _newRightEye.y = std::stoi(tokens[4]);
    _stickerScale = _newLeftEye.distance(_newRightEye) / 200.0;
    _stickerAngle = atan2(_newRightEye.x - _newLeftEye.x, _newRightEye.y - _newLeftEye.y) * (180 / M_PI) +90;
    _midPoint = (_newLeftEye + _newRightEye)/2;
    this->scheduleUpdate();
}

void DecodeScreen::setDecoration(int tag)
{
    criticalsection.Enter();
    _decorationManager->setItem(tag);
    criticalsection.Leave();
}