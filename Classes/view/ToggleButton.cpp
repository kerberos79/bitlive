//
//  ToggleButton.cpp
//  AnicameraMAC
//
//  Created by kerberos on 11/8/14.
//
//

#include "ToggleButton.h"

ToggleButton::~ToggleButton()
{
    
}

ToggleButton* ToggleButton::create()
{
    ToggleButton* widget = new (std::nothrow) ToggleButton();
    if (widget && widget->init())
    {
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

bool ToggleButton::init()
{
    if (Widget::init())
    {
        mInitialized = false;
        mIsPressed = false;
        return true;
    }
    return false;
}

void ToggleButton::onPressStateChangedToNormal()
{
    if(mInitialized)
    {
        return;
    }
    else
    {
        _buttonNormalRenderer->setVisible(true);
        _buttonClickedRenderer->setVisible(false);
        _buttonDisableRenderer->setVisible(false);
        mInitialized = true;
    }
    
}


void ToggleButton::onPressStateChangedToPressed()
{
}

void ToggleButton::setTitleColor(const Color3B& colorNormal, const Color3B& colorSelect)
{
    _titleRenderer->setColor(colorNormal);
    mColorNormal = colorNormal;
    mColorSelect = colorSelect;
}

void ToggleButton::onNormal()
{
    _buttonNormalRenderer->setVisible(true);
    _buttonClickedRenderer->setVisible(false);
    _buttonDisableRenderer->setVisible(false);
    _titleRenderer->setColor(mColorNormal);
    mIsPressed = false;
}

void ToggleButton::onPressed()
{
    _buttonNormalRenderer->setVisible(false);
    _buttonClickedRenderer->setVisible(true);
    _buttonDisableRenderer->setVisible(false);
    _titleRenderer->setColor(mColorSelect);
    mIsPressed = true;
}

void ToggleButton::toggle()
{
    if(mIsPressed)
        onNormal();
    else
        onPressed();
}

bool ToggleButton::isPressed()
{
    return mIsPressed;
}

void ToggleButton::setEnabled(bool enabled)
{
    if(mIsPressed)
    {
        _buttonNormalRenderer->setVisible(false);
        _buttonClickedRenderer->setVisible(true);
        _buttonDisableRenderer->setVisible(false);
        mIsPressed = true;
    }
    else if(enabled)
    {
        _buttonNormalRenderer->setVisible(true);
        _buttonClickedRenderer->setVisible(false);
        _buttonDisableRenderer->setVisible(false);
        mIsPressed = false;
    }
    else
    {
        _buttonNormalRenderer->setVisible(false);
        _buttonClickedRenderer->setVisible(false);
        _buttonDisableRenderer->setVisible(true);
        mIsPressed = false;
    }
    _titleRenderer->setColor(mColorSelect);
    _enabled = enabled;
}
