//
//  SliderEx.h
//  AnicameraMAC
//
//  Created by kerberos on 11/24/14.
//
//

#ifndef __AnicameraMAC__SliderEx__
#define __AnicameraMAC__SliderEx__

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;
class SliderEx : public Slider
{
public:
    enum class TouchEvent
    {
        DOWN,
        MOVE,
        UP,
        CANCEL
    };
    typedef std::function<void(SliderEx*,float,TouchEvent)> ccSliderExCallback;
    
    static SliderEx* create();
    void setCallBack(const ccSliderExCallback& callback);
    void setRatio(float ratio);
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unusedEvent);
    virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unusedEvent);
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unusedEvent);
    virtual void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unusedEvent);
    
private:
    TouchEvent _touchEvent;
    float _ratio;
    ccSliderExCallback _callback;
};
#endif /* defined(__AnicameraMAC__SliderEx__) */
