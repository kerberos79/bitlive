#include "HelloWorldScene.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

HelloWorld::~HelloWorld()
{
    if(mCaptureTexture)
        delete mCaptureTexture;

}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    if(!loadLayoutFromJson())
        return false;
    
    createPreviewScreen();
    createBottomMenu();
    createTopMenu();
    
    
    if(mTransparentBottomMenu)
        mSideMenuSize.height = visibleSize.height - mTopMenuHeight;
    else
        mSideMenuSize.height = mPreviewScreen->getScaledSize().height;

    createCaptureMenu();
    createLeftMenu();
    createRightMenu();
    createTimerSprite();
    createOptionMenu();
    createEmoticonSprite();
    createDialog("alarm", "null");
    
    mPreviewScreen->initGLProgram();

    mCaptureSize.width = mPreviewScreen->getScaledSize().width;
    mCaptureSize.height = mPreviewScreen->getScaledSize().height;
    
    mCaptureTexture = new Texture2D();
    
    Director::getInstance()->setAnimationInterval(1.0/30);
    Device::setAccelerometerEnabled(true);
    auto listener = EventListenerAcceleration::create(CC_CALLBACK_2(HelloWorld::onAcceleration, this));
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    initCustomEvent();
    
    
    _callStateHandler = CallStateHandler::create();
//    _callStateHandler->SetOnXmppLoginCallback(CC_CALLBACK_1(HelloWorld::OnXmppLoginCallback, this));
    _callStateHandler->Start();
    return true;
}

bool HelloWorld::loadLayoutFromJson()
{
    
    // calcuate a layout size for slide menu
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("layout.json");
    
    rapidjson::Document doc;
    
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    
    mSideMenuSize.width = DICTOOL->getIntValue_json(doc, "side_menu_width");
    mTopMenuHeight = DICTOOL->getIntValue_json(doc, "top_menu_height");
    mBottomHeight = DICTOOL->getIntValue_json(doc, "bottom_height");
    mOptionMenuHeight = DICTOOL->getIntValue_json(doc, "option_height");
    
    const rapidjson::Value& dialog = DICTOOL->getSubDictionary_json(doc, "dialog");
    DialogEx::BackgroundColor = Color4B(DICTOOL->getIntValue_json(dialog, "background_color_r"),
                                        DICTOOL->getIntValue_json(dialog, "background_color_g"),
                                        DICTOOL->getIntValue_json(dialog, "background_color_b"),
                                        DICTOOL->getIntValue_json(dialog, "background_color_a"));
    DialogEx::FontColor = Color3B(DICTOOL->getIntValue_json(dialog, "font_color_r"),
                                     DICTOOL->getIntValue_json(dialog, "font_color_g"),
                                     DICTOOL->getIntValue_json(dialog, "font_color_b"));
    DialogEx::TitleFontSize = DICTOOL->getIntValue_json(dialog, "title_font_size");
    DialogEx::TitleFontSize = DICTOOL->getIntValue_json(dialog, "title_font_size");
    DialogEx::BodyFontSize = DICTOOL->getIntValue_json(dialog, "text_font_size");
    DialogEx::MarginTop = DICTOOL->getIntValue_json(dialog, "margin_top");
    DialogEx::MarginBottom = DICTOOL->getIntValue_json(dialog, "margin_bottom");
    DialogEx::OkButtonNormal = DICTOOL->getStringValue_json(dialog, "ok_button_normal");
    DialogEx::OkButtonSelect = DICTOOL->getStringValue_json(dialog, "ok_button_select");
    
    return true;
}

void HelloWorld::onAcceleration(Acceleration *acc, Event *event)
{
    if(acc->y<-0.8)
    {
        if(mCurrentDirection!=DEGREE0)
        {
            rotateButton(DEGREE0);
            mCurrentDirection = DEGREE0;
        }
    }
    else if(acc->y<0.02 && acc->y>-0.02)
    {
        if(acc->x>0.8)
        {
            if(mCurrentDirection!=DEGREE270)
            {
                rotateButton(DEGREE270);
                mCurrentDirection = DEGREE270;
            }            
        }
        else if(acc->x<-0.08)
        {
            if(mCurrentDirection!=DEGREE90)
            {
                rotateButton(DEGREE90);
                mCurrentDirection = DEGREE90;
            }
        }
    }

}

void HelloWorld::createBottomMenu()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();
    int cellWidth = visibleSize.width/5;
    
    // capture button
    mCaptureButton = Button::create();
    if(mBottomMenuHeight<=mBottomHeight)
        mCaptureButton->loadTextures("capture_normal_narrow.png", "capture_select_narrow.png", "");
    else
        mCaptureButton->loadTextures("capture_normal.png", "capture_select.png", "");
    CCLOG("mBottomMenuHeight:%d, mTopMenuHeight: %d", mBottomMenuHeight,mTopMenuHeight);
    mCaptureButton->setPosition(Vec2(cellWidth*5/2, mBottomMenuHeight/2));
    mCaptureButton->setTouchEnabled(true);
    mCaptureButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuCaptureCallback,this));
    
    
    // flip button
    auto flipButton = Button::create();
    flipButton->loadTextures("flip_normal.png", "flip_select.png", "");
    flipButton->setPosition(Vec2(cellWidth*3/2, mBottomMenuHeight/2));
    flipButton->setTouchEnabled(true);
    flipButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuFlipCallback,this));
    
    // setting button
    auto exitButton = Button::create();
    exitButton->loadTextures("exit_normal.png", "exit_select.png", "");
    exitButton->setPosition(Vec2(cellWidth*7/2, mBottomMenuHeight/2));
    exitButton->setTouchEnabled(true);
    exitButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuCloseCallback,this));
    
    int bgcolor = (mTransparentBottomMenu)?0:255;
    auto layout = LayerColor::create(Color4B(0x2f, 0x2f, 0x2f, bgcolor), visibleSize.width, mBottomMenuHeight);
    layout->setPosition(Vec2(0, 0));
    layout->addChild(mCaptureButton);
    layout->addChild(flipButton);
    layout->addChild(exitButton);
    
    this->addChild(layout, ZORDER_MAINMENU_LAYER);

    mBottomButtonList.push_back(flipButton);
    mBottomButtonList.push_back(exitButton);
    mBottomPanel = layout;
}


void HelloWorld::createTopMenu()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();
    int cellWidth = visibleSize.width/5;
    
    int position, bgcolor;
    if(mTransparentBottomMenu)
    {
        bgcolor = 0;
        position = visibleSize.height - mTopMenuHeight;
    }
    else
    {
        bgcolor = 255;
        position = mBottomMenuHeight + mPreviewScreen->getScaledSize().height;
    }
    auto layout = LayerColor::create(Color4B(0x2f, 0x2f, 0x2f, bgcolor), visibleSize.width, mTopMenuHeight);
    
    layout->setPosition(Vec2(0, position));

    // emoticon button
    auto emoticonButton = ToggleButton::create();
    emoticonButton->loadTextures("emoticon_normal.png", "emoticon_select.png", "");
    emoticonButton->setPosition(Vec2(cellWidth/2, mTopMenuHeight/2));
    emoticonButton->setTouchEnabled(true);
    emoticonButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuEmoticonCallback,this));
    layout->addChild(emoticonButton);
    
    // shot button
    mFlashButton = ToggleButton::create();
    mFlashButton->loadTextures("flash_normal.png", "flash_select.png", "");
    mFlashButton->setPosition(Vec2(cellWidth*3/2, mTopMenuHeight/2));
    mFlashButton->setTouchEnabled(true);
    mFlashButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuShotCallback,this));
    layout->addChild(mFlashButton);
    
    // controller button
    auto controllerButton = ToggleButton::create();
    controllerButton->loadTextures("controller_normal.png", "controller_select.png", "");
    controllerButton->setPosition(Vec2(cellWidth*7/2, mTopMenuHeight/2));
    controllerButton->setTouchEnabled(true);
    controllerButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuControllerCallback,this));
    layout->addChild(controllerButton);
    
    // timer button
    mTimerToggleButton = ToggleButton::create();
    mTimerToggleButton->loadTextures("timer_normal.png", "timer_select.png", "");
    mTimerToggleButton->setPosition(Vec2(cellWidth*5/2, mTopMenuHeight/2));
    mTimerToggleButton->setTouchEnabled(true);
    mTimerToggleButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuTimerCallback,this));
    layout->addChild(mTimerToggleButton);
    
    // effect button
    auto effectButton = ToggleButton::create();
    effectButton->loadTextures("effect_normal.png", "effect_select.png", "");
    effectButton->setPosition(Vec2(cellWidth*9/2, mTopMenuHeight/2));
    effectButton->setTouchEnabled(true);
    effectButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuEffectCallback,this));
    layout->addChild(effectButton);
 
    this->addChild(layout, ZORDER_MAINMENU_LAYER);
    
    mTopButtonList.push_back(emoticonButton);
    mTopButtonList.push_back(mFlashButton);
    mTopButtonList.push_back(mTimerToggleButton);
    mTopButtonList.push_back(controllerButton);
    mTopButtonList.push_back(effectButton);
    mTopPanel = layout;
}

void HelloWorld::menuEmoticonCallback(cocos2d::Ref* pSender)
{
    ToggleButton* button = (ToggleButton*)pSender;
    button->toggle();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    if(button->isPressed())
        mLeftPanel->runAction(MoveTo::create(ACTION_SLIDE_MOVE_DELAY, Vec2(0.0,visibleSize.height - (mTopMenuHeight+mLeftPanel->getContentSize().height))));
    else
        mLeftPanel->runAction(MoveTo::create(ACTION_SLIDE_MOVE_DELAY, Vec2(-mLeftPanel->getContentSize().width,visibleSize.height - (mTopMenuHeight+mLeftPanel->getContentSize().height))));
}

void HelloWorld::menuShotCallback(cocos2d::Ref* pSender)
{
    if(Device::turnCameraFlashOnOff())
        ((ToggleButton*)pSender)->onPressed();
    else
        ((ToggleButton*)pSender)->onNormal();
}

void HelloWorld::menuTimerCallback(cocos2d::Ref* pSender)
{
    ((ToggleButton*)pSender)->toggle();
}

void HelloWorld::menuControllerCallback(cocos2d::Ref* pSender)
{
    ToggleButton* button = (ToggleButton*)pSender;
    button->toggle();
    
    if(button->isPressed())
    {
        mOptionPanel->setVisible(true);
        mOptionPanel->runAction(MoveTo::create(ACTION_SLIDE_MOVE_DELAY, Vec2(0.0,mTopPanel->getPosition().y - mOptionMenuHeight)));
    }
    else
    {
        mOptionPanel->setVisible(false);
        mOptionPanel->runAction(MoveTo::create(ACTION_SLIDE_MOVE_DELAY, Vec2(0.0,mTopPanel->getPosition().y)));
    }
}

void HelloWorld::menuEffectCallback(cocos2d::Ref* pSender)
{
    ToggleButton* button = (ToggleButton*)pSender;
    button->toggle();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    if(button->isPressed())
    {
        mRightPanel->runAction(MoveTo::create(ACTION_SLIDE_MOVE_DELAY, Vec2(visibleSize.width - mSideMenuSize.width,visibleSize.height - (mTopMenuHeight+mRightPanel->getContentSize().height))));
    }
    else
    {
        mRightPanel->runAction(MoveTo::create(ACTION_SLIDE_MOVE_DELAY, Vec2(visibleSize.width,visibleSize.height - (mTopMenuHeight+mRightPanel->getContentSize().height))));
    }
}

void HelloWorld::createPreviewScreen()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();
    mPreviewScreen = PreviewScreen::create(640, 480, 640, 480);
    mPreviewScreen->retain();
    mPreviewScreen->fitOnScreen(visibleSize.width, visibleSize.height - mTopMenuHeight);
    
    mBottomMenuHeight = visibleSize.height - (mPreviewScreen->getScaledSize().height + mTopMenuHeight);
    if(mBottomMenuHeight>0)
    {
        mPreviewScreen->setPosition(Vec2(visibleSize.width/2, mBottomMenuHeight + mPreviewScreen->getScaledSize().height/2));
        mTransparentBottomMenu = false;
    }
    else
    {
        mPreviewScreen->setPosition(Vec2(visibleSize.width/2, mPreviewScreen->getScaledSize().height/2));
        mBottomMenuHeight = mBottomHeight;
        mTransparentBottomMenu = true;
    }
    
    this->addChild(mPreviewScreen, ZORDER_PREVIEW_SCREEN);
}

void HelloWorld::createCaptureMenu()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();
    int cellWidth = visibleSize.width/3;
    auto layout = LayerColor::create(Color4B(0x2f, 0x2f, 0x2f, 255), visibleSize.width, mBottomPanel->getContentSize().height);
    
    // cancel button
    auto cancelButton = Button::create();
    cancelButton->loadTextures("cancel_normal.png", "cancel_select.png", "");
    cancelButton->setPosition(Vec2(cellWidth/2, mBottomPanel->getContentSize().height/2));
    cancelButton->setTouchEnabled(true);
    cancelButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuCancelCallback,this));
    layout->addChild(cancelButton);
    
    // save button
    auto saveButton = Button::create();
    saveButton->loadTextures("save_normal.png", "save_select.png", "");
    saveButton->setPosition(Vec2(cellWidth*3/2, mBottomPanel->getContentSize().height/2));
    saveButton->setTouchEnabled(true);
    saveButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuSaveCallback,this));
    layout->addChild(saveButton);
    
    // share button
    auto shareButton = Button::create();
    shareButton->loadTextures("share_normal.png", "share_normal.png", "");
    shareButton->setPosition(Vec2(cellWidth*5/2, mBottomPanel->getContentSize().height/2));
    shareButton->setTouchEnabled(true);
    shareButton->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuShareCallback,this));
    layout->addChild(shareButton);
    
    layout->setPosition(Vec2(0, -layout->getContentSize().height));
    layout->setVisible(false);
    
    this->addChild(layout, ZORDER_CAPTUREMENU_LAYER);
    
    mCaptureButtonList.push_back(cancelButton);
    mCaptureButtonList.push_back(saveButton);
    mCaptureButtonList.push_back(shareButton);
    mCapturePanel = layout;
}

void HelloWorld::menuCancelCallback(cocos2d::Ref* pSender)
{
    this->removeChildByTag(captureScreenTag);
    mCaptureSprite = nullptr;
    
    auto action = MoveTo::create(MOVE_MENU_DURATION , Vec2(0, 0));
    mBottomPanel->runAction( Sequence::create(action, nullptr, nullptr));
    mBottomPanel->setVisible(true);
    
    mCapturePanel->setPosition(Vec2(0, -mCapturePanel->getContentSize().height));
    mCapturePanel->setVisible(false);
    mCaptureButton->setEnabled(true);
    
    mTopPanel->setVisible(true);
    mBottomPanel->setVisible(true);
    mLeftPanel->setVisible(true);
    mRightPanel->setVisible(true);
    if(mOptionPanel->getPosition().y < mTopPanel->getPosition().y)
    {
        mOptionPanel->setVisible(true);
    }
}

void HelloWorld::menuSaveCallback(cocos2d::Ref* pSender)
{
    char temp[30];
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    sprintf(temp, "Anicamera%02d%02d%02d%02d%02d%02d.jpg", (now->tm_year -100), now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
    mOutputFile = Device::getExternalStorageDirectory() + "/"+temp;
}

void HelloWorld::menuShareCallback(cocos2d::Ref* pSender)
{
}

void HelloWorld::createLeftMenu()
{
    
    // Read content from file
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("left_menu_item_list.json");
    
    rapidjson::Document doc;
    
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return;
    }

    int listCount = DICTOOL->getArrayCount_json(doc, "list");
    int id;
    const char* normal_image, *selected_image, *resource;
    float posX, posY;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    auto listView = ListView::create();
    listView->setContentSize(Size(mSideMenuSize.width, mSideMenuSize.height));
    listView->setDirection(ListView::Direction::VERTICAL);
    
    for(int i=0;i<listCount; i++)
    {
        const rapidjson::Value& item = DICTOOL->getDictionaryFromArray_json(doc, "list", i);
        normal_image = DICTOOL->getStringValue_json(item, "normal_image");
        resource = DICTOOL->getStringValue_json(item, "resource");
        posX = DICTOOL->getFloatValue_json(item, "posX");
        posY = DICTOOL->getFloatValue_json(item, "posY");
        
        auto button = EmoticonMenuButton::create();
        button->loadTextures(normal_image, "", "");
        button->setPosition(Vec2(mSideMenuSize.width / 2 - button->getContentSize().width,
                                 button->getContentSize().height * 0.625));
        button->setTouchEnabled(true);
        button->addClickEventListener(CC_CALLBACK_1(HelloWorld::menuLeftCallback,this));
        button->setPressedActionEnabled(false);
        button->setResource(resource);
        button->setResourcePosition(posX, posY);
        listView->addChild(button);
        button->autorelease();
        mLeftButtonList.push_back(button);
    }
    Layer* layer = Layer::create();
    layer->setContentSize(Size(mSideMenuSize.width, mSideMenuSize.height));
    layer->setPosition(Vec2(0.0f, visibleSize.height - (mTopMenuHeight+mSideMenuSize.height)));
    layer->addChild(listView);
    
    this->addChild(layer, ZORDER_SLIDEMENU_LAYER);
    mLeftPanel = layer;
    
    mLeftPanel->runAction(MoveTo::create(ACTION_SLIDE_MOVE_DELAY, Vec2(-mLeftPanel->getContentSize().width,visibleSize.height - (mTopMenuHeight+mSideMenuSize.height))));
}

void HelloWorld::menuLeftCallback(Ref* pSender)
{
    int bottom, left;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    EmoticonMenuButton* button = (EmoticonMenuButton*)pSender;
    
    if(mSelectedButtonOnLeftMenu == nullptr)
    {
        mSelectedButtonOnLeftMenu = button;
        
        mEmoticon->setTexture(button->getResource());
        
        switch(mCurrentDirection)
        {
            case DEGREE90:
            {
                bottom = mBottomMenuHeight + mPreviewScreen->getScaledSize().height * (1.0-button->getResourcePosition().x);
                left = visibleSize.width * button->getResourcePosition().y;
                break;
            }
            case DEGREE270:
            {
                bottom = mBottomMenuHeight + mPreviewScreen->getScaledSize().height * button->getResourcePosition().x;
                left = visibleSize.width * (1.0 - button->getResourcePosition().y);
                break;
            }
            case DEGREE0:
            default:
            {
                bottom = mBottomMenuHeight + mPreviewScreen->getScaledSize().height * button->getResourcePosition().y;
                left = visibleSize.width * button->getResourcePosition().x;
                break;
            }
        }
        mEmoticon->setPosition(Vec2(left, bottom));
        mEmoticon->setRotation(mCurrentDirection);
        this->addChild(mEmoticon, ZORDER_EMOTICON_LAYER);
        CCLOG("menuLeftCallback1");
    }
    else if(mSelectedButtonOnLeftMenu == button)
    {
        this->removeChildByTag(emoticonSpriteTag);
        mSelectedButtonOnLeftMenu = nullptr;
        CCLOG("menuLeftCallback2");
    }
    else
    {
        mSelectedButtonOnLeftMenu = button;
        this->removeChildByTag(emoticonSpriteTag);
        
        mEmoticon->setTexture(button->getResource());
        switch(mCurrentDirection)
        {
            case DEGREE90:
            {
                bottom = mBottomMenuHeight + mPreviewScreen->getScaledSize().height * (1.0-button->getResourcePosition().x);
                left = visibleSize.width * button->getResourcePosition().y;
                break;
            }
            case DEGREE270:
            {
                bottom = mBottomMenuHeight + mPreviewScreen->getScaledSize().height * button->getResourcePosition().x;
                left = visibleSize.width * (1.0 - button->getResourcePosition().y);
                break;
            }
            case DEGREE0:
            default:
            {
                bottom = mBottomMenuHeight + mPreviewScreen->getScaledSize().height * button->getResourcePosition().y;
                left = visibleSize.width * button->getResourcePosition().x;
                break;
            }
        }        mEmoticon->setPosition(Vec2(left, bottom));
        mEmoticon->setRotation(mCurrentDirection);
        this->addChild(mEmoticon, ZORDER_EMOTICON_LAYER);
        CCLOG("menuLeftCallback3");
    }
}


void HelloWorld::createRightMenu()
{
    
    // Read content from file
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("right_menu_item_list.json");
    
    rapidjson::Document doc;
    
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return;
    }
    
    int listCount = DICTOOL->getArrayCount_json(doc, "list");
    int id;
    const char* normal_image, *selected_image, *fsh;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    auto listView = ListView::create();
    listView->setContentSize(Size(mSideMenuSize.width, mSideMenuSize.height));
    listView->setDirection(ListView::Direction::VERTICAL);
    
    
    for(int i=0;i<listCount; i++)
    {
        const rapidjson::Value& item = DICTOOL->getDictionaryFromArray_json(doc, "list", i);
        id = DICTOOL->getIntValue_json(item, "tag");
        normal_image = DICTOOL->getStringValue_json(item, "normal_image");
        selected_image = DICTOOL->getStringValue_json(item, "selected_image");
        fsh = DICTOOL->getStringValue_json(item, "fsh");
        
        auto button = ToggleButton::create();
        button->setTag(id);
        button->loadTextures(normal_image, selected_image, "");
        button->setPosition(Vec2(mSideMenuSize.width / 2 - button->getContentSize().width,
                                 button->getContentSize().height * 0.625));
        button->setTouchEnabled(true);
        button->addTouchEventListener(CC_CALLBACK_2(HelloWorld::menuRightCallback,this));
        button->setPressedActionEnabled(false);
        listView->addChild(button);
        button->autorelease();
        mRightButtonList.push_back(button);
        
        mPreviewScreen->putGLProgram(fsh);
    }
    ((ToggleButton*)mRightButtonList.front())->onPressed();
    toggledButtonOnRightMenu = (ToggleButton*)mRightButtonList.front();
    Layer* layer = Layer::create();
    layer->setPosition(Vec2(visibleSize.width - mSideMenuSize.width,visibleSize.height - (mTopMenuHeight+mSideMenuSize.height)));
    layer->setContentSize(Size(mSideMenuSize.width, mSideMenuSize.height));
    layer->addChild(listView);
    
    this->addChild(layer, ZORDER_SLIDEMENU_LAYER);
    mRightPanel = layer;
    
    mRightPanel->runAction(MoveTo::create(ACTION_SLIDE_MOVE_DELAY, Vec2(visibleSize.width,visibleSize.height - (mTopMenuHeight+mSideMenuSize.height))));
}

void HelloWorld::createEmoticonSprite()
{
    mEmoticon = Sprite::create();
    mEmoticon->retain();
    mEmoticon->setTag(emoticonSpriteTag);
    
    auto listener1 = EventListenerTouchOneByOne::create();
    listener1->setSwallowTouches(true);
    
    
    listener1->onTouchBegan = [](Touch* touch, Event* event){
        return true;
    };
    
    
    listener1->onTouchMoved = [](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        target->setPosition(target->getPosition() + touch->getDelta());
    };
    
    listener1->onTouchEnded = [=](Touch* touch, Event* event){
    };
    
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, mEmoticon);
}

void HelloWorld::createTimerSprite()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();
    mTimerSprite = TimerSprite::create("timer07.png", CC_CALLBACK_1(HelloWorld::menuCaptureCallback, this));
    mTimerSprite->setTag(timerSpriteTag);
    mTimerSprite->setPosition(Vec2(visibleSize.width/2, mBottomMenuHeight + mPreviewScreen->getScaledSize().height/2));
    mTimerSprite->addTexture(6, "timer07.png");
    mTimerSprite->addTexture(5, "timer06.png");
    mTimerSprite->addTexture(4, "timer05.png");
    mTimerSprite->addTexture(3, "timer04.png");
    mTimerSprite->addTexture(2, "timer03.png");
    mTimerSprite->addTexture(1, "timer02.png");
    mTimerSprite->addTexture(0, "timer01.png");
    mTimerSprite->setVisible(false);
    this->addChild(mTimerSprite, ZORDER_SLIDEMENU_LAYER);
}

void HelloWorld::menuFlipCallback(Ref* pSender)
{
}

void HelloWorld::menuCaptureCallback(Ref* pSender)
{
    
    _callStateHandler->SendEmptyMessage(CallStateHandler::Cmd::Login);
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    Director::getInstance()->end();
    exit(0);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    Device::releaseCameraPreview();
#endif
}

void HelloWorld::menuRightCallback(Ref* pSender, Widget::TouchEventType eventType)
{
    if(eventType==Widget::TouchEventType::ENDED)
    {
        ToggleButton* button = (ToggleButton*)pSender;
        button->onPressed();
        
        mPreviewScreen->setGLProgram(button->getTag());
        if(toggledButtonOnRightMenu != nullptr && toggledButtonOnRightMenu != button)
        {
            toggledButtonOnRightMenu->onNormal();
        }
        toggledButtonOnRightMenu = button;
    }
}

void HelloWorld::createOptionMenu()
{
    Size visibleSize = Director::getInstance()->getVisibleSize();
    int cellWidth = visibleSize.width/5;
    auto layer = Layer::create();
    layer->setPosition(Vec2(0, mTopPanel->getPosition().y - mOptionMenuHeight));
    layer->setContentSize(Size(visibleSize.width, mOptionMenuHeight));
    
    auto scaleSprite = Sprite::create("scale_sprite.png");
    scaleSprite->setPosition(Vec2(visibleSize.width * 0.22, layer->getContentSize().height*5/6));
    layer->addChild(scaleSprite);
    
    auto exposureSprite = Sprite::create("exposure_sprite.png");
    exposureSprite->setPosition(Vec2(visibleSize.width * 0.22, layer->getContentSize().height*3/6));
    layer->addChild(exposureSprite);
    
    auto timerSprite = Sprite::create("timer_sprite.png");
    timerSprite->setPosition(Vec2(visibleSize.width * 0.22, layer->getContentSize().height*1/6));
    layer->addChild(timerSprite);
    
    auto scaleSlider = SliderEx::create();
    scaleSlider->setCallBack([&](SliderEx* sender,float ratio,SliderEx::TouchEvent event){
        switch(event){
            case SliderEx::TouchEvent::MOVE:
            {
                mPreviewScreen->stretch(ratio);
                break;
            }
            case SliderEx::TouchEvent::DOWN:
            case SliderEx::TouchEvent::UP:
            case SliderEx::TouchEvent::CANCEL:
                break;
        }
    });
    scaleSlider->setPosition(Vec2(visibleSize.width * 0.78 - scaleSlider->getContentSize().width/2,layer->getContentSize().height*5/6));
    layer->addChild(scaleSlider);
    
    mExposureSlider = SliderEx::create();
    mExposureSlider->setCallBack([&](SliderEx* sender,float ratio,SliderEx::TouchEvent event){
        switch(event){
            case SliderEx::TouchEvent::MOVE:
                Device::setExposureCamera((int)(ratio * 100));
                break;
            case SliderEx::TouchEvent::DOWN:
            case SliderEx::TouchEvent::UP:
            case SliderEx::TouchEvent::CANCEL:
                break;
        }
    });
    mExposureSlider->setPosition(Vec2(visibleSize.width * 0.78 - scaleSlider->getContentSize().width/2,layer->getContentSize().height*3/6));
    mExposureSlider->setPercent(Device::getExposureCamera());
    layer->addChild(mExposureSlider);
    
    auto timer3_button = ToggleButton::create();
    timer3_button->setTag(3);
    timer3_button->loadTextures("timer3s_normal.png", "timer3s_select.png", "");
    timer3_button->setPosition(Vec2(visibleSize.width * 0.4 - timer3_button->getContentSize().width/2,layer->getContentSize().height*1/6));
    timer3_button->setTouchEnabled(true);
    timer3_button->addTouchEventListener(CC_CALLBACK_2(HelloWorld::menuTimerOptionCallback,this));
    timer3_button->setPressedActionEnabled(false);
    layer->addChild(timer3_button);
    
    auto timer5_button = ToggleButton::create();
    timer5_button->setTag(5);
    timer5_button->loadTextures("timer5s_normal.png", "timer5s_select.png", "");
    timer5_button->setPosition(Vec2(visibleSize.width * 0.6 - timer5_button->getContentSize().width/2,layer->getContentSize().height*1/6));
    timer5_button->setTouchEnabled(true);
    timer5_button->addTouchEventListener(CC_CALLBACK_2(HelloWorld::menuTimerOptionCallback,this));
    timer5_button->setPressedActionEnabled(false);
    layer->addChild(timer5_button);
    
    auto timer7_button = ToggleButton::create();
    timer7_button->setTag(7);
    timer7_button->loadTextures("timer7s_normal.png", "timer7s_select.png", "");
    timer7_button->setPosition(Vec2(visibleSize.width * 0.8 - timer7_button->getContentSize().width/2,layer->getContentSize().height*1/6));
    timer7_button->setTouchEnabled(true);
    timer7_button->addTouchEventListener(CC_CALLBACK_2(HelloWorld::menuTimerOptionCallback,this));
    timer7_button->setPressedActionEnabled(false);
    layer->addChild(timer7_button);
    
    timer5_button->onPressed();
    toggledButtonOnTimerOptionMenu = timer5_button;
    mTimerSprite->setTimerLimit(5);
    
    this->addChild(layer, ZORDER_SLIDEMENU_LAYER);
    mOptionPanel = layer;
    mOptionPanel->setVisible(false);
    mOptionPanel->runAction(MoveTo::create(ACTION_SLIDE_MOVE_DELAY, Vec2(0.0,mTopPanel->getPosition().y)));
}

void HelloWorld::menuTimerOptionCallback(Ref* pSender, Widget::TouchEventType eventType)
{
    if(eventType==Widget::TouchEventType::ENDED)
    {
        ToggleButton* button = (ToggleButton*)pSender;
        button->onPressed();
        mTimerSprite->setTimerLimit(button->getTag());
        if(toggledButtonOnTimerOptionMenu != nullptr && toggledButtonOnTimerOptionMenu != button)
        {
            toggledButtonOnTimerOptionMenu->onNormal();
        }
        toggledButtonOnTimerOptionMenu = button;
    }
}

void HelloWorld::rotateButton(int newRotation)
{
    
    if(mLeftPanel->isVisible())
    {
        for(auto& item : mLeftButtonList)
        {
            auto action = RotateTo::create(ROTATE_BUTTON_DURATION , newRotation);
            item->runAction( Sequence::create(action, nullptr, nullptr));
        }
    }
    else
    {
        for(auto& item : mLeftButtonList)
        {
            item->setRotation(newRotation);
        }
    }
    
    if(mRightPanel->isVisible())
    {
        for(auto& item : mRightButtonList)
        {
            auto action = RotateTo::create(ROTATE_BUTTON_DURATION , newRotation);
            item->runAction( Sequence::create(action, nullptr, nullptr));
        }
    }
    else
    {
        for(auto& item : mRightButtonList)
        {
            item->setRotation(newRotation);
        }
    }
    
    if(mBottomPanel->isVisible())
    {
        for(auto& item : mBottomButtonList)
        {
            auto action = RotateTo::create(ROTATE_BUTTON_DURATION , newRotation);
            item->runAction( Sequence::create(action, nullptr, nullptr));
        }
    }
    else
    {
        for(auto& item : mBottomButtonList)
        {
            item->setRotation(newRotation);
        }
    }
    
    if(mTopPanel->isVisible())
    {
        for(auto& item : mTopButtonList)
        {
            auto action = RotateTo::create(ROTATE_BUTTON_DURATION , newRotation);
            item->runAction( Sequence::create(action, nullptr, nullptr));
        }
    }
    else
    {
        for(auto& item : mBottomButtonList)
        {
            item->setRotation(newRotation);
        }
    }
    
    if(mCapturePanel->isVisible())
    {
        for(auto& item : mCaptureButtonList)
        {
            auto action = RotateTo::create(ROTATE_BUTTON_DURATION , newRotation);
            item->runAction( Sequence::create(action, nullptr, nullptr));
        }
    }
    else
    {
        for(auto& item : mCaptureButtonList)
        {
            item->setRotation(newRotation);
        }
    }
    mTimerSprite->setRotation(newRotation);
    
    if(mCaptureSprite!=nullptr)
    {
        auto action = RotateTo::create(ROTATE_BUTTON_DURATION , newRotation);
        mCaptureSprite->runAction( Sequence::create(action, nullptr, nullptr));
    }
    
    if(mSelectedButtonOnLeftMenu!=nullptr)
    {
        auto action = RotateTo::create(ROTATE_BUTTON_DURATION , newRotation);
        mEmoticon->runAction( Sequence::create(action, nullptr, nullptr));
    }
}


void HelloWorld::createDialog(const char *title, const char *body)
{
    if(mDialog==nullptr)
    {
        mDialog = DialogEx::create(title, body, DialogEx::Type::POSITIVE);
        mDialog->addOkButtonClickListener(CC_CALLBACK_0(HelloWorld::hideDialog,this));
        this->addChild(mDialog, ZORDER_DIALGO_LAYER, dialogTag);
    }
    else
    {
        mDialog->setBody(body);
    }
    mDialog->show(false);
}

void HelloWorld::showDialog(const char *body)
{
    mDialog->setBody(body);
    mDialog->show(true);
}

void HelloWorld::hideDialog()
{
    mDialog->show(false);
}

void HelloWorld::initCustomEvent()
{
    auto _listener = EventListenerCustom::create("XmppEvent", [=](EventCustom* event){
        char* msg = static_cast<char*>(event->getUserData());
        showDialog(msg);
    });
    _eventDispatcher->addEventListenerWithFixedPriority(_listener, 1);
}

void HelloWorld::OnXmppLoginCallback(buzz::XmppEngine::State state)
{
    EventCustom event("XmppEvent");
    switch (state) {
        case buzz::XmppEngine::STATE_START:
            CCLOG("OnLoginListener : connecting...");
            break;
        case buzz::XmppEngine::STATE_OPENING:
            CCLOG("OnLoginListener : logging in...");
            break;
        case buzz::XmppEngine::STATE_OPEN:
            CCLOG("OnLoginListener : open...");
            event.setUserData((void*)"open");
            break;
        case buzz::XmppEngine::STATE_CLOSED:
            event.setUserData((void*)"closed");
            _eventDispatcher->dispatchEvent(&event);
            CCLOG("OnLoginListener : quit...");
            break;
    }

}