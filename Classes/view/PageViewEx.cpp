//
//  PageViewEx.cpp
//  BitLive
//
//  Created by keros on 6/25/15.
//
//

#include "PageViewEx.h"

PageViewEx::PageViewEx()
:PageView(),
negativeTouchThresold(0.0f),
positiveTouchThresold(0.0f)
{
    
}

PageViewEx::~PageViewEx()
{
    _pageViewEventListener = nullptr;
    _pageViewEventSelector = nullptr;
}

PageViewEx* PageViewEx::create()
{
    PageViewEx* widget = new (std::nothrow) PageViewEx();
    if (widget && widget->init())
    {
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

void PageViewEx::setTouchThresold(float thresold)
{
    negativeTouchThresold = -thresold;
    positiveTouchThresold = thresold;
}

void PageViewEx::handleMoveLogic(Touch *touch)
{
    Vec2 touchPoint = touch->getLocation();
    
    float offset = 0.0;
    offset = touchPoint.x - touch->getPreviousLocation().x;
    
    if (offset < negativeTouchThresold)
    {
        _touchMoveDirection = TouchDirection::LEFT;
    }
    else if (offset > positiveTouchThresold)
    {
        _touchMoveDirection = TouchDirection::RIGHT;
    }
    scrollPages(offset);
}