//
//  TimerSprite.h
//  AnicameraMAC
//
//  Created by kerberos on 11/25/14.
//
//

#ifndef __AnicameraMAC__TimerSprite__
#define __AnicameraMAC__TimerSprite__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class TimerSprite : public Sprite
{
public:
    
    static TimerSprite* create(const std::string& filename, const std::function<void(Node*)> &callback);
    
    void addTexture(int index, const std::string& filename);
    void startAnimation();
    void afterAnimated(Node* sender, int count);
    void setTimerLimit(int value);
    std::function<void(Node*)> _func;
private:
    Texture2D *extraTextures[7];
    int _timerLimit;
};

#endif /* defined(__AnicameraMAC__TimerSprite__) */
