//
//  ViewFinder.cpp
//  BitLive
//
//  Created by keros on 11/3/15.
//
//

#include "ViewFinder.h"

Color3B ViewFinder::FontColor;
int ViewFinder::FontSize;
std::string ViewFinder::RecImage;
std::string ViewFinder::GuideImage;
std::string ViewFinder::CloseButtonNormal;

ViewFinder::~ViewFinder()
{
    
}

ViewFinder * ViewFinder::create(MainScene* mainscene)
{
    ViewFinder * menu = new (std::nothrow) ViewFinder();
    if( menu && menu->init(mainscene))
    {
        menu->setPosition(Vec2::ZERO);
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void ViewFinder::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& viewFinder = DICTOOL->getSubDictionary_json(doc, "view_finder");
    ViewFinder::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(viewFinder, "font_color"));
    ViewFinder::FontSize = DICTOOL->getIntValue_json(viewFinder, "font_size") * MainScene::LayoutScale;
    ViewFinder::RecImage = DICTOOL->getStringValue_json(viewFinder, "rec_image");
    ViewFinder::GuideImage = DICTOOL->getStringValue_json(viewFinder, "guide_image");
    ViewFinder::CloseButtonNormal = DICTOOL->getStringValue_json(viewFinder, "close_button_normal");
}

bool ViewFinder::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("guide.json");
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    mVisibleSize = Director::getInstance()->getVisibleSize();
    
    mRecSprite = Sprite::create(ViewFinder::RecImage);
    mRecSprite->setPosition(Vec2(mVisibleSize.width * 0.9f,mVisibleSize.height * 0.95f) );
    
    mCountText = Label::createWithTTF("00:00:00:00", Strings::getFontType(), FontSize);
    mCountText->setColor(Color3B::WHITE);
    mCountText->setPosition(Vec2(mVisibleSize.width * 0.5f, mVisibleSize.height * 0.95f));
    if(!Layout::init())
        return false;
    
    this->setContentSize(Size(mVisibleSize.width, mVisibleSize.height - MainScene::SubMenuHeight));
    this->setPosition(Vec2(0, MainScene::SubMenuHeight));
    
    {
        mFirstRecord = UserDefault::getInstance()->getBoolForKey("record_first", true);
        if(mFirstRecord)
        {
        	mGuideLayout = LayerColor::create(Color4B(0,0,0,200), this->getContentSize().width, this->getContentSize().height);
        	mGuideLayout->setName("guide");
        	mGuideLayout->setPosition(Vec2(0, MainScene::SubMenuHeight));
            mGuideLayout->setTouchEnabled(true);
            mGuideLayout->setSwallowsTouches(true);
            
            auto touchSprite = Sprite::create(ViewFinder::GuideImage);
            touchSprite->setPosition(Vec2(mVisibleSize.width * 0.5f,this->getContentSize().height * 0.5f) );
            
            auto guideText = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "record"), Strings::getBrushFontType(), FontSize * 1.2f);
            guideText->setColor(Color3B::WHITE);
            guideText->setPosition(Vec2(this->getContentSize().width * 0.5f, this->getContentSize().height * 0.35f));
            
            auto closeButton = Button::create();
            closeButton->loadTextures(ViewFinder::CloseButtonNormal, "", "");
            closeButton->setTitleColor(Color3B::WHITE);
            closeButton->setTitleFontSize(FontSize * 1.2f);
            closeButton->setTitleFontName(Strings::getBrushFontType());
            closeButton->setTitleText(DICTOOL->getStringValue_json(doc, "close"));
            closeButton->setPosition(Vec2(mVisibleSize.width * 0.9f, this->getContentSize().height*0.1f));
            closeButton->addClickEventListener(CC_CALLBACK_1(ViewFinder::onCloseButtonCallback, this));
            
            mGuideLayout->addChild(touchSprite);
            mGuideLayout->addChild(guideText);
            mGuideLayout->addChild(closeButton);
            this->addChild(mGuideLayout);
            UserDefault::getInstance()->setBoolForKey("record_first", false);
            
            _touchListener = EventListenerTouchOneByOne::create();
            _touchListener->retain();
            _touchListener->setSwallowTouches(true);
            _touchListener->onTouchBegan = CC_CALLBACK_2(ViewFinder::onTouchBegan, this);
            _touchListener->onTouchMoved = CC_CALLBACK_2(ViewFinder::onTouchMoved, this);
            _touchListener->onTouchEnded = CC_CALLBACK_2(ViewFinder::onTouchEnded, this);
            _touchListener->onTouchCancelled = CC_CALLBACK_2(ViewFinder::onTouchCancelled, this);
            _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener, this);
        }
        else
        {
        	mGuideLayout = nullptr;
        }
    }

    this->addChild(mRecSprite);
    this->addChild(mCountText);
    
    mRecordCount = 0;
    return true;
}

void ViewFinder::update(float dt)
{
    
    mRecordCount += dt;
    int millisecond2 = (int)(mRecordCount * 10000.0)%100;
    int millisecond = (int)(mRecordCount * 100.0)%100;
    int second = (int)(mRecordCount)%60;
    int min = (int)(mRecordCount/60.0)%60;
    if((int)mRecordCount >= MainMenu::RecTimeout)
    {
        sprintf(mRecordCountString, "00:%02d:00.00", second);
        mMainScene->getMainMenu()->menuRecordCallback(this);
        resetCount();
        mRecSprite->stopAllActions();
        mRecSprite->setVisible(true);
        this->unscheduleUpdate();
    }
    else
    {
        sprintf(mRecordCountString, "%02d:%02d:%02d.%02d", min, second, millisecond, millisecond2);
    }
    mCountText->setString(std::string(mRecordCountString));
}

void ViewFinder::setVisible(bool visible)
{
    if(visible)
    {
        mCountText->setString("00:00:00.00");
    }
    Layout::setVisible(visible);
}

void ViewFinder::start(bool enable)
{
    if(enable)
    {
        auto blinkAction = Blink::create(1500, 2000);
        mRecSprite->runAction(blinkAction);
        this->scheduleUpdate();
    }
    else
    {
        mRecSprite->stopAllActions();
        mRecSprite->setVisible(true);
        this->unscheduleUpdate();
    }
}

void ViewFinder::onCloseButtonCallback(Ref* pSender)
{
    _eventDispatcher->removeEventListener(_touchListener);
    this->removeChildByName("guide");
}

void ViewFinder::resetCount()
{
    mCountText->setString("00:00:00.00");
    mRecordCount = 0;
}

bool ViewFinder::onTouchBegan(Touch *touch, cocos2d::Event *event)
{
    return true;
}

void ViewFinder::onTouchMoved(Touch *touch, cocos2d::Event *event)
{
}

void ViewFinder::onTouchEnded(Touch *touch, cocos2d::Event *event)
{
}

void ViewFinder::onTouchCancelled(Touch* touch, cocos2d::Event* unused_event)
{
}