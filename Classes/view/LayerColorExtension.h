//
//  LayerColorExtension.h
//  BitLive
//
//  Created by kerberos on 6/29/15.
//
//

#ifndef __BitLive__LayerColorExtension__
#define __BitLive__LayerColorExtension__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Cocos2dxProfile.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class LayerColorExtension
{
public:
    virtual void show(bool isShow) = 0;
    virtual cocos2d::Size& getSize() = 0;
    virtual Color4B& getBackgroundColor() = 0;
    virtual void setBackgroundColor(Color4B& color) = 0;
protected:
    Action *_visibleAction, *_invisibleAction;
    Vec2  _startPos, _endPos;

};
#endif /* defined(__BitLive__LayerColorExtension__) */
