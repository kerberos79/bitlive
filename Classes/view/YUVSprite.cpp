//
//  YUVSprite.cpp
//  AnicameraMAC
//
//  Created by kerberos on 12/3/14.
//
//

#include "YUVSprite.h"

YUVSprite::~YUVSprite()
{
    if(_initBufferY)
        delete _initBufferY;
}

YUVSprite* YUVSprite::create(int org_width, int org_height, int dst_width, int dst_height)
{
    YUVSprite *sprite = new (std::nothrow) YUVSprite();
    if (sprite && sprite->init(org_width, org_height, dst_width, dst_height))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

bool YUVSprite::init(int org_width, int org_height, int dst_width, int dst_height)
{
    y_width = dst_width;
    y_height = dst_height;
    u_offset = org_width * org_height;
    v_offset = u_offset + (u_offset/4);
    uv_width = dst_width/2;
    uv_height = dst_height/2;
    y_size = y_width * y_height;
    uv_size = uv_width * uv_height;
    
    _initBufferY = new unsigned char[y_size +uv_size*2];
    _initBufferU = _initBufferY + y_size;
    _initBufferV = _initBufferU + uv_size;
    yuvBuffer = _initBufferY;
    if(_initBufferY)
        memset(_initBufferY, 0,y_size);
    if(_initBufferU)
        memset(_initBufferU, 127,uv_size);
    if(_initBufferV)
        memset(_initBufferV, 127,uv_size);
    pTexture0 = new Texture2D();
    pTexture0->initWithData(_initBufferY,
                            y_size,
                            Texture2D::PixelFormat::I8,
                            y_width,
                            y_height,
                            Size(y_width, y_height));
    pTexture1 = new Texture2D();
    pTexture1->initWithData(_initBufferU,
                            uv_size,
                            Texture2D::PixelFormat::I8,
                            uv_width,
                            uv_height,
                            Size(uv_width, uv_height));
    pTexture2 = new Texture2D();
    pTexture2->initWithData(_initBufferV,
                            uv_size,
                            Texture2D::PixelFormat::I8,
                            uv_width,
                            uv_height,
                            Size(uv_width, uv_height));
    
    initWithTexture(pTexture0);
    
    this->setContentSize(Size(dst_width, dst_height));
    seconds = 0.0;
    return true;
}

void YUVSprite::setGLProgram(GLProgram *glprogram, Size size)
{
    auto glprogramstate = GLProgramState::getOrCreateWithGLProgram(glprogram);
    setGLProgramState(glprogramstate);
    glprogramstate->setUniformTexture("u_texture", pTexture2);
    glprogramstate->setUniformTexture("v_texture", pTexture1);
    glprogramstate->setUniformFloat("imageWidth", size.width);
    glprogramstate->setUniformFloat("imageHeight", size.height);
}

void YUVSprite::setGLProgram(GLProgramInfo* programInfo, cocos2d::Size size)
{
    auto glprogramstate = GLProgramState::getOrCreateWithGLProgram(programInfo->_program1);
    setGLProgramState(glprogramstate);
    glprogramstate->setUniformTexture("u_texture", pTexture2);
    glprogramstate->setUniformTexture("v_texture", pTexture1);
    glprogramstate->setUniformFloat("imageWidth", size.width);
    glprogramstate->setUniformFloat("imageHeight", size.height);
    if(programInfo->_lookup)
        glprogramstate->setUniformTexture("rgbTexture2", programInfo->_lookup);
}

void YUVSprite::updateTime()
{
    auto glprogramstate = getGLProgramState();
    glprogramstate->setUniformFloat("iGlobalTime", seconds);
    seconds+=0.05;
}

void YUVSprite::updateFaceDetect(Vec2& facePos, float faceScale)
{
    auto glprogramstate = getGLProgramState();
    glprogramstate->setUniformVec2("facePos", facePos);
    glprogramstate->setUniformFloat("faceScale", faceScale);
}

void YUVSprite::updateDirect(unsigned char *updateBuffer)
{
    GL::bindTexture2D(pTexture0->getName());
    glTexSubImage2D(GL_TEXTURE_2D,0,0,0,y_width,y_height,GL_LUMINANCE, GL_UNSIGNED_BYTE,updateBuffer);
    GL::bindTexture2D(pTexture1->getName());
    glTexSubImage2D(GL_TEXTURE_2D,0,0,0,uv_width,uv_height,GL_LUMINANCE, GL_UNSIGNED_BYTE,updateBuffer + u_offset);
    GL::bindTexture2D(pTexture2->getName());
    glTexSubImage2D(GL_TEXTURE_2D,0,0,0,uv_width,uv_height,GL_LUMINANCE, GL_UNSIGNED_BYTE,updateBuffer + v_offset);
}

void YUVSprite::updateDirect(unsigned char *buf_y, unsigned char *buf_u, unsigned char *buf_v)
{
    GL::bindTexture2D(pTexture0->getName());
    glTexSubImage2D(GL_TEXTURE_2D,0,0,0,y_width,y_height,GL_LUMINANCE, GL_UNSIGNED_BYTE, buf_y);
    GL::bindTexture2D(pTexture1->getName());
    glTexSubImage2D(GL_TEXTURE_2D,0,0,0,uv_width,uv_height,GL_LUMINANCE, GL_UNSIGNED_BYTE, buf_u);
    GL::bindTexture2D(pTexture2->getName());
    glTexSubImage2D(GL_TEXTURE_2D,0,0,0,uv_width,uv_height,GL_LUMINANCE, GL_UNSIGNED_BYTE, buf_v);
}
void YUVSprite::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
    // Don't do calculate the culling if the transform was not updated
    _insideBounds = (flags & FLAGS_TRANSFORM_DIRTY) ? renderer->checkVisibility(transform, _contentSize) : _insideBounds;
    
    if(_insideBounds)
    {
        _quadCommand.init(_globalZOrder, _texture->getName(), getGLProgramState(), _blendFunc, &_quad, 1, transform);
        renderer->addCommand(&_quadCommand);
    }
}
