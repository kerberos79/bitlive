//
//  WarningDialog.cpp
//  BitLive
//
//  Created by keros on 5/16/15.
//
//

#include "WarningDialog.h"

Color4B WarningDialog::BackgroundColor;
Color3B WarningDialog::FontColor;
int WarningDialog::TitleFontSize ;
int WarningDialog::TextFontSize;
std::string WarningDialog::OkButtonNormal;
std::string WarningDialog::OkButtonSelect;

WarningDialog::~WarningDialog()
{
}

WarningDialog * WarningDialog::create(MainScene *mainscene, UIMessage::Cmd type)
{
    WarningDialog * dialog = new (std::nothrow) WarningDialog();
    if( dialog && dialog->init(mainscene, type))
    {
        dialog->setPosition(Vec2::ZERO);
        dialog->autorelease();
        return dialog;
    }
    CC_SAFE_DELETE(dialog);
    return nullptr;
}

void WarningDialog::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& warningDialog = DICTOOL->getSubDictionary_json(doc, "warning_dialog");
    WarningDialog::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(warningDialog, "background_color"));
    WarningDialog::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(warningDialog, "font_color"));
    WarningDialog::TitleFontSize = DICTOOL->getIntValue_json(warningDialog, "title_font_size") * MainScene::LayoutScale;
    WarningDialog::TextFontSize = DICTOOL->getIntValue_json(warningDialog, "text_font_size") * MainScene::LayoutScale;
    WarningDialog::OkButtonNormal = DICTOOL->getStringValue_json(warningDialog, "ok_button_normal");
    WarningDialog::OkButtonSelect = DICTOOL->getStringValue_json(warningDialog, "ok_button_select");
}

bool WarningDialog::init(MainScene *mainscene, UIMessage::Cmd type)
{
    mMainScene = mainscene;
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("warning.json");
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }

    Size visibleSize = Director::getInstance()->getVisibleSize();
    std::string title, body;
    if(type == UIMessage::Cmd::NotFountNetwork)
    {
        title = DICTOOL->getStringValue_json(doc, "not_found_network");
        body = DICTOOL->getStringValue_json(doc, "not_found_network_message");
    }
    else if(type == UIMessage::Cmd::Login)
    {
        title = DICTOOL->getStringValue_json(doc, "login_fail");
        body = DICTOOL->getStringValue_json(doc, "login_fail_message");
    }
    // Add the title
    _title = LabelTTF::create(title, Strings::getFontType(), WarningDialog::TitleFontSize);
    _title->setColor(FontColor);
    _title->setPosition(Vec2(visibleSize.width*0.5f, visibleSize.height*0.7));
    
    // add messege
    _body = LabelTTF::create(body, Strings::getFontType(), WarningDialog::TextFontSize);
    _body->setColor(FontColor);
    _body->setPosition(Vec2(visibleSize.width * 0.5f, visibleSize.height *0.55f));
    
    _okButton = Button::create();
    _okButton->loadTextures(WarningDialog::OkButtonNormal, WarningDialog::OkButtonSelect, "");
    _okButton->setPosition(Vec2(visibleSize.width*0.5f, visibleSize.height*0.3f));
    _okButton->setTouchEnabled(true);
    _okButton->addClickEventListener(CC_CALLBACK_1(WarningDialog::OnOkButtonClick,this));
    
    if(!initWithColor(WarningDialog::BackgroundColor, visibleSize.width, visibleSize.height))
        return false;
    
    this->addChild(_title);
    this->addChild(_body);
    this->addChild(_okButton);
    
    _CustomListener = EventListenerTouchOneByOne::create();
    _CustomListener->retain();
    _CustomListener->setSwallowTouches(true);
    _CustomListener->onTouchBegan = CC_CALLBACK_2(WarningDialog::onTouchBegan, this);
    _CustomListener->onTouchMoved = CC_CALLBACK_2(WarningDialog::onTouchMoved, this);
    _CustomListener->onTouchEnded = CC_CALLBACK_2(WarningDialog::onTouchEnded, this);
    _CustomListener->onTouchCancelled = CC_CALLBACK_2(WarningDialog::onTouchCancelled, this);
    
    _touchListener = _CustomListener;
    return true;
}

void WarningDialog::OnOkButtonClick(Ref* pSender)
{
    switch(_currentType)
    {
        case UIMessage::Cmd::NotFountNetwork:
        case UIMessage::Cmd::Login:
        case UIMessage::Cmd::Connect:
        {
            this->show(false);
            mMainScene->setOfflineMode(true);
            mMainScene->ChangeState(MainState::Offline);
            break;
        }
        case UIMessage::Cmd::DeleteAccount:
        {
            mMainScene->OnExit();
            break;
        }
        case UIMessage::Cmd::Join:
        {
            mMainScene->OnExit();
            break;
        }
        default:
            break;
    }
}

bool WarningDialog::addOkButtonClickListener(std::function<void(Ref*)> callback)
{
    _okButton->addClickEventListener(callback);
    return true;
}

bool WarningDialog::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    CCLOG("WarningDialog::onTouchBegan");
    return true;
}

void WarningDialog::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
    CCLOG("WarningDialog::onTouchMoved");
}

void WarningDialog::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
    CCLOG("WarningDialog::onTouchEnded");
}

void WarningDialog::onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* unused_event)
{
    CCLOG("WarningDialog::onTouchCancelled");
}
void WarningDialog::show(bool isShow)
{
    if(isShow)
    {
        _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener, this);
    }
    else
    {
        _eventDispatcher->removeEventListener(_touchListener);
    }
    this->setVisible(isShow);
}

void WarningDialog::show(UIMessage::Cmd type)
{
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("warning.json");
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return;
    }
    
    std::string title, body;
    switch(type)
    {
        case UIMessage::Cmd::NotFountNetwork:
        {
            title = DICTOOL->getStringValue_json(doc, "not_found_network");
            body = DICTOOL->getStringValue_json(doc, "not_found_network_message");
            break;
        }
        case UIMessage::Cmd::Login:
        {
            title = DICTOOL->getStringValue_json(doc, "login_fail");
            body = DICTOOL->getStringValue_json(doc, "login_fail_message");
            break;
        }
        case UIMessage::Cmd::Connect:
        {
            title = DICTOOL->getStringValue_json(doc, "connect_fail_fail");
            body = DICTOOL->getStringValue_json(doc, "connect_fail_fail_message");
            break;
        }
        case UIMessage::Cmd::DeleteAccount:
        {
            title = DICTOOL->getStringValue_json(doc, "delete_account");
            body = DICTOOL->getStringValue_json(doc, "delete_account_message");
            break;
        }
        case UIMessage::Cmd::Join:
        {
            title = DICTOOL->getStringValue_json(doc, "join");
            body = DICTOOL->getStringValue_json(doc, "join_message");
            break;
        }
        default:
            break;
    }
    _currentType = type;
    _title->setString(title);
    _body->setString(body);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener, this);
    this->setVisible(true);
}
