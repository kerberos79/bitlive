//
//  Alert.cpp
//  BitLive
//
//  Created by keros on 8/27/15.
//
//

#include "Alert.h"

Color4B Alert::BackgroundColor;
Color3B Alert::FontColor;
int Alert::FontSize = 0;
int Alert::Height = 0;

Alert::~Alert()
{
}

Alert * Alert::create(MainScene* mainscene)
{
    Alert * menu = new (std::nothrow) Alert();
    if( menu && menu->init(mainscene))
    {
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void Alert::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& alert = DICTOOL->getSubDictionary_json(doc, "alert");
    Alert::BackgroundColor = HexString::ConvertColor4B(DICTOOL->getStringValue_json(alert, "background_color"));
    Alert::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(alert, "font_color"));
    Alert::FontSize = DICTOOL->getIntValue_json(alert, "font_size") * MainScene::LayoutScale;
    Alert::Height = DICTOOL->getIntValue_json(alert, "height") * MainScene::LayoutScale;
}

bool Alert::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    _titleText = LabelTTF::create("Alert", Strings::getFontType(), Alert::FontSize);
    _titleText->setColor(InviteDialog::FontColor);
    _titleText->setPosition(Vec2(visibleSize.width*0.5, Alert::Height * 0.5f));
    _titleText->setOpacity(0);
    
    if(!initWithColor(BackgroundColor, visibleSize.width, Alert::Height))
        return false;
    
    this->setPosition(Vec2(0, visibleSize.height - Alert::Height));
    this->addChild(_titleText);
    _visibleAction = Sequence::create(FadeIn::create(0.4),
                                      DelayTime::create(1),
                                      FadeOut::create(0.4),
                                      CallFuncN::create( CC_CALLBACK_1(Alert::afterAction, this)),
                                      nullptr);
    _visibleAction->retain();
    _textAction = Sequence::create(FadeIn::create(0.4),
                                 DelayTime::create(1),
                                 FadeOut::create(0.4),
                                   nullptr);
    _textAction->retain();
    
    return true;
}

void Alert::update(float dt)
{
    _titleText->setString(_message);
    this->unscheduleUpdate();
}

void Alert::show(std::string message, UIMessage::Alert type)
{
    this->stopAction(_visibleAction);
    this->stopAction(_textAction);
    
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("alert.json");
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return;
    }
    
    std::string title, body;
    switch(type)
    {
        case UIMessage::Alert::NotSupported:
        _message = DICTOOL->getStringValue_json(doc, "not_support");
        break;
        case UIMessage::Alert::Captured:
        _message = DICTOOL->getStringValue_json(doc, "captured");
            break;
        case UIMessage::Alert::IOSCaptured:
            _message = DICTOOL->getStringValue_json(doc, "ios_captured");
            break;
        case UIMessage::Alert::OnBusy:
            _message = DICTOOL->getStringValue_json(doc, "on_busy");
            _message += message;
            break;
    }
    this->setVisible(true);

    this->scheduleUpdate();
    this->runAction(_visibleAction);
    
    _titleText->runAction(_textAction);
}
void Alert::afterAction(Node* sender)
{
    this->setVisible(false);
}
