//
//  CaptureMenu.cpp
//  BitLive
//
//  Created by keros on 8/31/15.
//
//

#include "CaptureMenu.h"
#include "CCDomainFactory.h"

Color4B CaptureMenu::BackgroundColor1;
Color4B CaptureMenu::BackgroundColor2;
Color3B CaptureMenu::FontColor;
int CaptureMenu::FontSize;
int CaptureMenu::Height;
std::string CaptureMenu::PlayButtonNormal;
std::string CaptureMenu::SaveButtonNormal;
std::string CaptureMenu::ShareButtonNormal;
std::string CaptureMenu::CancelButtonNormal;
std::string CaptureMenu::LoadingNormal;

CaptureMenu::~CaptureMenu()
{
}

CaptureMenu * CaptureMenu::create(MainScene* mainscene)
{
    CaptureMenu * menu = new (std::nothrow) CaptureMenu();
    if( menu && menu->init(mainscene))
    {
        menu->setPosition(Vec2::ZERO);
        menu->autorelease();
        return menu;
    }
    CC_SAFE_DELETE(menu);
    return nullptr;
}

void CaptureMenu::loadLayout(rapidjson::Document &doc)
{
    const rapidjson::Value& CaptureMenu = DICTOOL->getSubDictionary_json(doc, "capture_menu");
    CaptureMenu::BackgroundColor1 = HexString::ConvertColor4B(DICTOOL->getStringValue_json(CaptureMenu, "background_color1"));
    CaptureMenu::BackgroundColor2 = HexString::ConvertColor4B(DICTOOL->getStringValue_json(CaptureMenu, "background_color2"));
    CaptureMenu::FontColor = HexString::ConvertColor3B(DICTOOL->getStringValue_json(CaptureMenu, "font_color"));
    CaptureMenu::FontSize = DICTOOL->getIntValue_json(CaptureMenu, "font_size") * MainScene::LayoutScale;
    CaptureMenu::PlayButtonNormal = DICTOOL->getStringValue_json(CaptureMenu, "play_button_normal");
    CaptureMenu::SaveButtonNormal = DICTOOL->getStringValue_json(CaptureMenu, "save_button_normal");
    CaptureMenu::ShareButtonNormal = DICTOOL->getStringValue_json(CaptureMenu, "share_button_normal");
    CaptureMenu::CancelButtonNormal = DICTOOL->getStringValue_json(CaptureMenu, "cancel_button_normal");
    CaptureMenu::LoadingNormal = DICTOOL->getStringValue_json(CaptureMenu, "loading_normal");
}

bool CaptureMenu::init(MainScene* mainscene)
{
    mMainScene = mainscene;
    rapidjson::Document doc;
    std::string contentStr = FileUtils::getInstance()->getStringFromFile("capture.json");
    doc.Parse<0>(contentStr.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %s\n", doc.GetParseError());
        return false;
    }
    CaptureMenu::Height = MainScene::SubMenuHeight;
    mVisibleSize = Director::getInstance()->getVisibleSize();
    float posLabel;
    {
        mPlayButton = Button::create();
        mPlayButton->loadTextures(PlayButtonNormal, "", "");
        mPlayButton->setPosition(Vec2(mVisibleSize.width * 0.5f,
                                      mVisibleSize.height - (mVisibleSize.height - MainScene::SubMenuHeight)/2));
        mPlayButton->setTouchEnabled(true);
        mPlayButton->setVisible(false);
        mPlayButton->addClickEventListener(CC_CALLBACK_1(CaptureMenu::OnPlayButtonClick, this));
        
        mLoadingSprite = Sprite::create(LoadingNormal);
        mLoadingSprite->setPosition(Vec2(mVisibleSize.width * 0.5f,
                                        mVisibleSize.height - (mVisibleSize.height - MainScene::SubMenuHeight)/2));
        mLoadingSprite->setVisible(false);
        
        mSaveButton = Button::create();
        mSaveButton->loadTextures(SaveButtonNormal, "", "");
        mSaveButton->setPosition(Vec2(mVisibleSize.width * 0.75f, CaptureMenu::Height * 0.6f));
        mSaveButton->setTouchEnabled(true);
        mSaveButton->addClickEventListener(CC_CALLBACK_1(CaptureMenu::OnSaveButtonClick, this));
        
        posLabel = (mSaveButton->getPosition().y - mSaveButton->getContentSize().height * 0.5f) * 0.5f;
        mSaveText = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "save"), Strings::getFontType(), FontSize);
        mSaveText->setColor(FontColor);
        mSaveText->setPosition(Vec2(mSaveButton->getPosition().x, posLabel));
        
        mShareButton = Button::create();
        mShareButton->loadTextures(ShareButtonNormal, "", "");
        mShareButton->setPosition(Vec2(mVisibleSize.width * 0.5f, CaptureMenu::Height * 0.6f));
        mShareButton->setTouchEnabled(true);
        mShareButton->addClickEventListener(CC_CALLBACK_1(CaptureMenu::OnShareButtonClick, this));
        
        mShareText = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "share"), Strings::getFontType(), FontSize);
        mShareText->setColor(FontColor);
        mShareText->setPosition(Vec2(mShareButton->getPosition().x, posLabel));
        
        mCancelButton = Button::create();
        mCancelButton->loadTextures(CancelButtonNormal, "", "");
        mCancelButton->setPosition(Vec2(mVisibleSize.width * 0.25f, CaptureMenu::Height * 0.6f));
        mCancelButton->setTouchEnabled(true);
        mCancelButton->addClickEventListener(CC_CALLBACK_1(CaptureMenu::OnCancelButtonClick, this));
        
        mCancelText = Label::createWithTTF(DICTOOL->getStringValue_json(doc, "cancel"), Strings::getFontType(), FontSize);
        mCancelText->setColor(FontColor);
        mCancelText->setPosition(Vec2(mCancelButton->getPosition().x, posLabel));
        
        mMainLayout = LayerGradient::create(CaptureMenu::BackgroundColor1, CaptureMenu::BackgroundColor2);
        mMainLayout->setContentSize(Size(mVisibleSize.width, CaptureMenu::Height));
        mMainLayout->setPosition(Vec2::ZERO);
        
        mMainLayout->addChild(mCancelText, 1);
        mMainLayout->addChild(mShareText, 1);
        mMainLayout->addChild(mSaveText, 1);
        
        mMainLayout->addChild(mPlayButton, 1);
        mMainLayout->addChild(mCancelButton, 1);
        mMainLayout->addChild(mShareButton, 1);
        mMainLayout->addChild(mSaveButton, 1);
        
        mMainLayout->addChild(mLoadingSprite, 2);
    }
    
    // end
    if(!initWithColor(Color4B::BLACK, mVisibleSize.width, mVisibleSize.height))
        return false;
    this->addChild(mMainLayout, 1);
    
    _isHiding = false;
    _startPos = Vec2(mVisibleSize.width * 0.3f, 0);
    _endPos = Vec2::ZERO;
    _visibleAction = EaseExponentialOut::create(MoveTo::create(0.4f, Vec2::ZERO));
    _visibleAction->retain();
    mCaptureGif = nullptr;
    mCaptureTexture = nullptr;
    return true;
}

void CaptureMenu::show(bool isShow)
{
    if(isShow)
    {
        if(mFileType == MediaType::Movie)
        {
            if(mVideoPlayer==nullptr)
            {
                mVideoPlayer = VideoPlayer::create();
                mVideoPlayer->setPosition(Vec2(mVisibleSize.width * 0.5f,
                                               mVisibleSize.height - mScaledImageSize.height * 0.5f));
                mVideoPlayer->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
                mVideoPlayer->setScale(mScale);
                mVideoPlayer->setContentSize(Size(mCapturedImage->getWidth(), mCapturedImage->getHeight()));
                mVideoPlayer->addEventListener(CC_CALLBACK_2(CaptureMenu::videoEventCallback, this));
                this->addChild(mVideoPlayer, 2);
            }
            mLoadingSprite->setVisible(true);
            mLoadingSprite->runAction(RepeatForever::create(RotateBy::create(3, 360)));
        }
        this->setVisible(true);
        this->setPosition(Vec2(_startPos));
        this->runAction(_visibleAction);
    }
    else
    {
        if(mCaptureTexture)
        {
//            delete mCaptureTexture;
            mCaptureTexture = nullptr;
        }
        if(mCaptureGif)
        {
            this->removeChild(mCaptureGif);
            mCaptureGif = nullptr;
        }
        if(mCaptureSprite)
        {
            this->removeChild(mCaptureSprite);
            mCaptureSprite = nullptr;
        }
        this->setVisible(false);
    }
}

void CaptureMenu::afterInvisibleAction(Node* sender)
{
    this->setVisible(false);
    _isHiding = false;
}

void CaptureMenu::setImage(Image* image, std::string filename, cocos2d::Size scaled_size)
{
    mCapturedImage = image;
    mCaptureName = filename;
    if( mFileType == MediaType::Gif)
    {
        return;
    }
    if(mCaptureTexture==nullptr)
        mCaptureTexture  = new Texture2D();
    if(!mCaptureTexture->initWithImage(image))
    {
        CCLOG("fail to initWithImage from capture image");
        return;
    }
    mScale = mVisibleSize.width/mCapturedImage->getWidth();
    mScaledImageSize.width = mCapturedImage->getWidth() * mScale;
    mScaledImageSize.height = mCapturedImage->getHeight() * mScale;
    mCaptureSprite = Sprite::createWithTexture(mCaptureTexture);
    mCaptureSprite->setScale(mScale);
    mCaptureSprite->setPosition(Vec2(scaled_size.width/2, mVisibleSize.height - mScaledImageSize.height*0.5f));
    this->addChild(mCaptureSprite, 0);
    
    _isSaved = false;
    mCurrentVideoState = VideoPlayer::EventType::STOPPED;
}

void CaptureMenu::update(float dt)
{
    switch(mFileType)
    {
        case MediaType::Gif:
        {
            mCaptureGif = CacheGif::create(mCaptureName.c_str());
            if(mCaptureGif)
            {
                mCaptureGif->setScale(mScale);
                mCaptureGif->setPosition(Vec2(mScaledImageSize.width/2, mVisibleSize.height - mScaledImageSize.height*0.5f));
                this->addChild(mCaptureGif, 0);
            }
            this->unscheduleUpdate();
            break;
        }
        case MediaType::Movie:
        {
            mLoadingCount+=dt;
            if(mLoadingCount>1)
            {
                mLoadingSprite->stopAllActions();
                mLoadingSprite->setVisible(false);
                mPlayButton->setVisible(true);
                this->unscheduleUpdate();
            }
            break;
        }
    }
}

void CaptureMenu::updateMediaType(MediaType type)
{
    mFileType = type;
}

void CaptureMenu::updateGif(std::string filename, int width, int height)
{
    mCaptureName = filename;
    if(mFileType == MediaType::Gif)
    {
        mPlayButton->setVisible(false);
    }
    mScale = mVisibleSize.width/width;
    mScaledImageSize.width = width * mScale;
    mScaledImageSize.height = height * mScale;
    
    _isSaved = false;
    mCurrentVideoState = VideoPlayer::EventType::STOPPED;
    this->scheduleUpdate();
}

void CaptureMenu::updateMP4(bool complete)
{
    if(complete)
    {
        mLoadingCount = 0;
        this->scheduleUpdate();
    }
}

void CaptureMenu::OnPlayButtonClick(Ref* pSender)
{
    if(mVideoPlayer!=nullptr)
    {
        mVideoPlayer->setVisible(true);
        mVideoPlayer->setFileName(mCaptureName.c_str());
        mVideoPlayer->play();
    }
}

void CaptureMenu::videoEventCallback(Ref* sender, VideoPlayer::EventType eventType)
{
    mCurrentVideoState = eventType;
    switch (eventType) {
        case VideoPlayer::EventType::PLAYING:
        CCLOG("PLAYING");
        break;
        case VideoPlayer::EventType::PAUSED:
        CCLOG("PAUSED");
        break;
        case VideoPlayer::EventType::STOPPED:
        CCLOG("STOPPED");
        break;
        case VideoPlayer::EventType::COMPLETED:
        CCLOG("COMPLETED");
        mVideoPlayer->setVisible(false);
        break;
        default:
        break;
    }
}

bool CaptureMenu::isPlaying()
{
    return (mCurrentVideoState == VideoPlayer::EventType::PLAYING);
}

void CaptureMenu::OnSaveButtonClick(Ref* pSender)
{
    if(_isSaved)
        return;
    switch(mFileType)
    {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        case MediaType::Jpeg:
        {
            Device::savePhoto(mCaptureName.c_str());
            mMainScene->showAlert("", UIMessage::Alert::IOSCaptured);
            break;
        }
        case MediaType::Gif:
        {
            Device::saveGif(mCaptureName.c_str());
            mMainScene->showAlert("", UIMessage::Alert::IOSCaptured);
            break;
        }
        case MediaType::Movie:
        {
            Device::saveMovie();
            mMainScene->showAlert("", UIMessage::Alert::IOSCaptured);
            break;
        }
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        case MediaType::Jpeg:
        {
            mMainScene->showAlert("", UIMessage::Alert::Captured);
            Device::scanMediaNewFile(mCaptureName.c_str());
            break;
        }
        case MediaType::Gif:
        {
            mMainScene->showAlert("", UIMessage::Alert::Captured);
            Device::scanMediaNewFile(mCaptureName.c_str());
            break;
        }
        case MediaType::Movie:
        {
            mMainScene->showAlert("", UIMessage::Alert::Captured);
            Device::scanMediaNewFile(mCaptureName.c_str());
            break;
        }
#endif
        case MediaType::None:
        default:
        {
            break;
        }
    }
    
    _isSaved = true;
}

void CaptureMenu::OnShareButtonClick(Ref* pSender)
{
    if(mCurrentVideoState == VideoPlayer::EventType::PLAYING)
    {
        mVideoPlayer->stop();
    }
    switch(mFileType)
    {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        case MediaType::Jpeg:
        {
            Device::sharePhoto(mCaptureName.c_str());
            break;
        }
        case MediaType::Gif:
        {
            Device::sharePhoto(mCaptureName.c_str());
            break;
        }
        case MediaType::Movie:
        {
            Device::shareMovie(mCaptureName.c_str());
            break;
        }
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        case MediaType::Jpeg:
        {
            Device::sendActionShareImage(mCaptureName.c_str());
            break;
        }
        case MediaType::Gif:
        {
            Device::sendActionShareImage(mCaptureName.c_str());
            break;
        }
        case MediaType::Movie:
        {
            Device::sendActionShareMovie(mCaptureName.c_str());
            break;
        }
#endif
        case MediaType::None:
        default:
        {
            break;
        }
    }
}

void CaptureMenu::OnCancelButtonClick(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    cocos2d::FileUtils::getInstance()->deleteFile(mCaptureName);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if(!_isSaved)
        cocos2d::FileUtils::getInstance()->deleteFile(mCaptureName);
#endif
    if(mFileType == MediaType::Movie)
    {
        if(mCurrentVideoState == VideoPlayer::EventType::PLAYING)
        {
            mVideoPlayer->stop();
        }
        if(mVideoPlayer)
            mVideoPlayer->setVisible(false);
    }
    if(mLoadingSprite->isVisible())
    {
        mLoadingSprite->setVisible(false);
        mLoadingSprite->stopAllActions();
    }
    if(mPlayButton->isVisible())
        mPlayButton->setVisible(false);
    mMainScene->resetViewFinder();
    mMainScene->toggleMenuCallback(this);
    this->unscheduleUpdate();
}

cocos2d::Size& CaptureMenu::getSize()
{
    return (cocos2d::Size&)this->getContentSize();
}

Color4B& CaptureMenu::getBackgroundColor()
{
    return CaptureMenu::BackgroundColor1;
}

void CaptureMenu::setBackgroundColor(Color4B& color)
{
    _displayedColor.r = _realColor.r = color.r;
    _displayedColor.g = _realColor.g = color.g;
    _displayedColor.b = _realColor.b = color.b;
    _displayedOpacity = _realOpacity = color.a;
    
    updateColor();
}
