//
//  DecodeScreen.h
//  Vista
//
//  Created by kerberos on 2/24/15.
//
//

#ifndef __Vista__DecodeScreen__
#define __Vista__DecodeScreen__

#include <stdio.h>
#include "cocos2d.h"
#include "util/Tokenizer.h"
#include "platform/CCDevice.h"
#include "math/CCGeometry.h"
#include "view/YUVSprite.h"
#include "view/MainScene.h"
#include "util/GLProgramManager.h"
#include "util/EmoticonManager.h"
#include "util/StickerManager.h"
#include "util/DecorationManager.h"

USING_NS_CC;

class MainScene;
class EmoticonManager;
class DecorationManager;

class DecodeScreen : public RenderTexture
{
public:
    ~DecodeScreen();
    static DecodeScreen * create(MainScene *mainscene, int org_width, int org_height, int dst_width, int dst_height);
    bool init(MainScene *mainscene, int org_width, int org_height, int dst_width, int dst_height, Texture2D::PixelFormat eFormat);
    void fitOnScreen(Size& parentLayerSize);
    void setUpdatedFrame();
    void setFrameRate(int value);
    void enterSmallScreen();
    void enterFullScreen();
    
    void initGLProgram();
    void reserveProgram(std::string program);
    std::string getReservedProgram();
    void setProgram(std::string program);
    void rotate(std::string degree);
    cocos2d::Size getScaledSize();
    float getCurrentRatio();
    Vec2 getCurrentPos();
    
    void addEmoticonLayer();
    void setEmoticon(int tag);
    void setSticker(std::string param);
    void setDecoration(int tag);
    int getSmallScreenSector();
    void setSmallScreenSector(int sector);
    std::function<bool(YUVSprite*)> updateDecodeFrame;
private:
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event  *event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event  *event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event  *event);
    virtual void update(float dt);
    virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags);
    void selectSector(Vec2 p);
    int rotate_value;
    float _fullScreenRatio, _smallScreenRatio;
    
    MainScene *mMainScene;
    YUVSprite *mYUVSprite;
    CustomCommand _customCommand;
    
    cocos2d::Size _parentSreenSize;
    cocos2d::Size _fullScreenSize;
    cocos2d::Size _smallScreenSize;
    Vec2          _fullScreenPosition;
    Vec2          _smallScreenPosition[4];
    int           _smallScreenSector;
    
    bool updatedFrame;
    QuadCommand      _quadCommand;
    int    _flipAnimateCount;
    int    _currentDegree;
    
    unsigned char *_frameBuffer;
    RenderTexture* rtX;
    Sprite* stepX;
    
    GLProgramManager _glProgramManager;
    GLProgramInfo* _currentProgramInfo;
    std::string      _reservedGlProgram;
    bool    _twoPassFilter;
    bool    _smallScreen;
    bool    _touchOnScreen;
    uint32  _touchOldTime;
    DrawNode*        _outline;
    EmoticonManager* _emoticonManager;
    Node*           _emoticonLayer;
    StickerManager*  _stickerManager;
    Sprite*          _sticker;
    DecorationManager*  _decorationManager;
    int              _stickerId;
    rtc::CriticalSection criticalsection;
    Vec2 _newLeftEye, _newRightEye, _midPoint;
    float _stickerScale, _stickerAngle;
};


#endif /* defined(__Vista__DecodeScreen__) */
