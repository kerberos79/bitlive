//
//  WebBrowser.cpp
//  BitLive
//
//  Created by kerberos on 7/24/15.
//
//

#include "WebBrowser.h"

Color4B WebBrowser::MenuColor;
Color3B WebBrowser::FontColor;
int WebBrowser::FontSize;
int WebBrowser::MenuHeight;
int WebBrowser::LeftMargin;
std::string WebBrowser::UrlBox;

WebBrowser::~WebBrowser()
{
    
}

WebBrowser* WebBrowser::create(MainScene *mainscene)
{
    WebBrowser * dialog = new (std::nothrow) WebBrowser();
    if( dialog && dialog->init(mainscene))
    {
        dialog->setPosition(Vec2::ZERO);
        dialog->autorelease();
        return dialog;
    }
    CC_SAFE_DELETE(dialog);
    return nullptr;
}


void WebBrowser::loadLayout(rapidjson::Document &doc)
{
    WebBrowser::MenuColor = HexString::ConvertColor4B("f2f2f2FF");
    WebBrowser::FontColor = HexString::ConvertColor3B("000000");
    WebBrowser::FontSize = 40 * MainScene::LayoutScale;
    WebBrowser::MenuHeight = 140 * MainScene::LayoutScale;
    WebBrowser::LeftMargin = 8 * MainScene::LayoutScale;
    WebBrowser::UrlBox = "url_box.png";
}

bool WebBrowser::init(MainScene *mainscene)
{
    mMainScene = mainscene;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    mWebview = experimental::ui::WebView::create();
    mWebview->setContentSize(Size(visibleSize.width, visibleSize.height - (float)WebBrowser::MenuHeight));
    mWebview->setPosition(mWebview->getContentSize()*0.5f);
    mWebview->setOnShouldStartLoading(CC_CALLBACK_2(WebBrowser::OnShouldStartLoading, this));
    mWebview->setOnDidFinishLoading(CC_CALLBACK_2(WebBrowser::OnDidFinishLoading, this));
    
    auto menuLayout = Layout::create();
    menuLayout->setPosition(Vec2::ZERO);
    menuLayout->setContentSize(Size(visibleSize.width, WebBrowser::MenuHeight));
    
    Vec2 position = Vec2(visibleSize.width, 0.0);
    
    // extra button
    auto extraButton = Button::create();
    extraButton->loadTextures("btn_extra_normal.png", "btn_extra_select.png", "");
    position.x -= extraButton->getContentSize().width;
    extraButton->setAnchorPoint(Vec2::ZERO);
    extraButton->setPosition(position);
    extraButton->setTouchEnabled(true);
    extraButton->addClickEventListener(CC_CALLBACK_1(WebBrowser::menuExitCallback,this));
    // right button
    auto forwardButton = Button::create();
    forwardButton->loadTextures("btn_forward_normal.png", "btn_forward_select.png", "");
    position.x -= forwardButton->getContentSize().width;
    forwardButton->setAnchorPoint(Vec2::ZERO);
    forwardButton->setPosition(position);
    forwardButton->setTouchEnabled(true);
    forwardButton->addClickEventListener(CC_CALLBACK_1(WebBrowser::menuRightCallback,this));
    // left button
    auto backButton = Button::create();
    backButton->loadTextures("btn_back_normal.png", "btn_back_select.png", "");
    position.x -= backButton->getContentSize().width;
    backButton->setAnchorPoint(Vec2::ZERO);
    backButton->setPosition(position);
    backButton->setTouchEnabled(true);
    backButton->addClickEventListener(CC_CALLBACK_1(WebBrowser::menuLeftCallback,this));
    
    // reload button
    mReloadButton = ToggleButton::create();
    mReloadButton->loadTextures("btn_reload.png", "btn_stop.png", "");
    position.x -= mReloadButton->getContentSize().width;
    mReloadButton->setAnchorPoint(Vec2::ZERO);
    mReloadButton->setPosition(position);
    mReloadButton->setTouchEnabled(true);
    mReloadButton->addClickEventListener(CC_CALLBACK_1(WebBrowser::menuReloadCallback,this));
    
    position.x -= WebBrowser::LeftMargin;
    auto urlBoxSprite = cocos2d::ui::Scale9Sprite::create(WebBrowser::UrlBox);
    mUrlEditBox = cocos2d::ui::EditBox::create(Size(position.x, WebBrowser::MenuHeight), urlBoxSprite);
    mUrlEditBox->setInputMode(cocos2d::ui::EditBox::InputMode::URL);
    mUrlEditBox->setInputFlag(cocos2d::ui::EditBox::InputFlag::SENSITIVE);
    mUrlEditBox->setAnchorPoint(Vec2::ZERO);
    mUrlEditBox->setPosition(Vec2(WebBrowser::LeftMargin, position.y));
    mUrlEditBox->setFontName(Strings::getFontType());
    mUrlEditBox->setFontSize(WebBrowser::FontSize);
    mUrlEditBox->setFontColor(WebBrowser::FontColor);
    mUrlEditBox->setMaxLength(500);
    mUrlEditBox->setReturnType(cocos2d::ui::EditBox::KeyboardReturnType::GO);
    mUrlEditBox->setDelegate(this);
    
    mUrlLayout = LayerColor::create(WebBrowser::MenuColor, visibleSize.width, mUrlEditBox->getContentSize().height);
    mUrlLayout->setPosition(Vec2(0.0, visibleSize.height - mUrlEditBox->getContentSize().height));
    mUrlLayout->addChild(mUrlEditBox);
    mUrlLayout->addChild(backButton);
    mUrlLayout->addChild(forwardButton);
    mUrlLayout->addChild(mReloadButton);
    mUrlLayout->addChild(extraButton);

    if(!initWithColor(WebBrowser::MenuColor, visibleSize.width, visibleSize.height))
        return false;
    
    this->setContentSize(visibleSize);
    this->addChild(mWebview, 0);
    this->addChild(mUrlLayout, 1);
    
    mMenuVisible = false;
    mWebview->setOnSaveHomepageListener(CC_CALLBACK_1(WebBrowser::onSaveAsHomepageCallback,this));
    mWebview->setOnSendLinkListener(CC_CALLBACK_1(WebBrowser::onSendLinkCallback,this));
    mWebview->setOnCloseListener(CC_CALLBACK_0(WebBrowser::onCloseCallback,this));
    return true;

}

void WebBrowser::update(float dt)
{
    mUrlEditBox->setText(mUrl.c_str());
    this->unscheduleUpdate();
}

bool WebBrowser::OnShouldStartLoading(experimental::ui::WebView *sender, std::string url)
{
    mUrl = url;
    mReloadButton->onPressed();
    this->scheduleUpdate();
    return true;
}

void WebBrowser::OnDidFinishLoading(experimental::ui::WebView *sender, std::string url)
{
    mUrl = url;
    mReloadButton->onNormal();
    this->scheduleUpdate();
}

void WebBrowser::menuLeftCallback(Ref* pSender)
{
    mWebview->goBack();
}

void WebBrowser::menuRightCallback(Ref* pSender)
{
    mWebview->goForward();
}

void WebBrowser::menuReloadCallback(Ref* pSender)
{
    if(mReloadButton->isPressed())
        mWebview->reload();
    else
        mWebview->stopLoading();
}

void WebBrowser::menuExitCallback(Ref* pSender)
{
    mMenuVisible = !mMenuVisible;
    mWebview->showMenu(mMenuVisible);
}

void WebBrowser::onSaveAsHomepageCallback(std::string url)
{
    UserDefault::getInstance()->setStringForKey("homepage", url);
    mMenuVisible = false;
}

void WebBrowser::onSendLinkCallback(std::string url)
{
    mMainScene->menuSendLinkURLCallback(url);
    mMenuVisible = false;
}

void WebBrowser::onCloseCallback()
{
    this->show(false);
    mMenuVisible = false;
}

void WebBrowser::editBoxEditingDidBegin(cocos2d::ui::EditBox* editBox)
{
}

void WebBrowser::editBoxEditingDidEnd(cocos2d::ui::EditBox* editBox)
{
}

void WebBrowser::editBoxTextChanged(cocos2d::ui::EditBox* editBox, const std::string& text)
{
}

void WebBrowser::editBoxReturn(ui::EditBox* editBox)
{
    std::string url = editBox->getText();
    mWebview->loadURL(url);
}

void WebBrowser::show(bool isShow)
{
    std::string url = UserDefault::getInstance()->getStringForKey("homepage", "http://www.naver.com");
    mWebview->loadURL(url);
    mWebview->setVisible(isShow);
    this->setVisible(isShow);
}

void WebBrowser::show(std::string url)
{
    mWebview->loadURL(url);
    mWebview->setVisible(true);
    this->setVisible(true);
}
