//
//  InviteDialog.h
//  Vista
//
//  Created by kerberos on 2/11/15.
//
//

#ifndef __Vista__InviteDialog__
#define __Vista__InviteDialog__

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "audio/include/AudioEngine.h"
#include "view/InfiniteProgressBar.h"
#include "controller/CallStateHandler.h"
#include "extra/Strings.h"
#include "datatype/UIMessage.h"
#include "datatype/InviteEventData.h"
#include "view/MainScene.h"
#include "view/LayerColorExtension.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class MainScene;
class InviteDialog : public LayerGradient, public LayerColorExtension
{
public:
    
    ~InviteDialog();
    static Color4B BackgroundColor1;
    static Color4B BackgroundColor2;
    static Color3B FontColor;
    static int TitleFontSize;
    static int BodyFontSize;
    static int Height;
    static std::string DefaultPhotoImage;
    static std::string DefaultPhotoStencil;
    static std::string CallButtonNormal;
    static std::string CallButtonSelect;
    static std::string OkButtonNormal;
    static std::string OkButtonSelect;
    static std::string CancelButtonNormal;
    static std::string CancelButtonSelect;
    
    static InviteDialog* create(MainScene *mainscene);
    static void loadLayout(rapidjson::Document &doc);
    void OnCallButtonClick(Ref* pSender);
    void OnOkButtonClick(Ref* pSender);
    void OnCloseButtonClick(Ref* pSender);
    void OnCancelButtonClick(Ref* pSender);
    void setContactInfo(ContactInfo* info);
    ContactInfo* getContactInfo();
    void show(InviteEventData::Type type, ContactInfo* info=nullptr);
    InviteEventData::Type getType()
    {
    	return _type;
    }
    virtual void show(bool isShow);
    virtual cocos2d::Size& getSize();
    virtual Color4B& getBackgroundColor();
    virtual void setBackgroundColor(Color4B& color);
private:
    bool init(MainScene *mainscene);
    virtual void update(float dt);
    void playSound();
    void stopSound();
    void addPicture(ContactInfo *info);
    void afterInvisibleAction(Node* sender);
    MainScene *mMainScene;
    LabelTTF *_titleText;
    LabelTTF *_bodyText;
    Button *_callButton;
    Button *_okButton;
    Button *_cancelButton;
    Sprite *_photo;
    ClippingNode* clippedNode;
    InfiniteProgressBar *_progressBar;
    EventListenerTouchOneByOne *_CustomListener;
    InviteEventData::Type _type;
    
    std::string _sendString;
    std::string _receiveString;
    std::string _refusalString;
    std::string _cancelString;
    std::string _busyString;
    std::string _notFoundUserString;
    std::string _connectingString;
    
    float        _centerPositionX;
    float        _rightPositionX;
    int _currentRingerMode;
    int _callSoundID;
    ContactInfo* _contactInfo;
};
#endif /* defined(__Vista__InviteDialog__) */
