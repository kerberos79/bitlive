//
//  GifEncoder.h
//  BitLive
//
//  Created by keros on 9/20/15.
//
//

#ifndef __BitLive__GifEncoder__
#define __BitLive__GifEncoder__

#include <stdio.h>
#include <string>
#include "cocos2d.h"
#include "gif_lib/gif_encode.h"
#include "webrtc/base/criticalsection.h"
#include "webrtc/base/timeutils.h"
#include "webrtc/base/thread.h"
#include "webrtc/base/messagehandler.h"
#include "webrtc/base/sigslot.h"
#include "controller/CallStateHandler.h"

USING_NS_CC;
class GifEncoder : public rtc::MessageHandler, public sigslot::has_slots<>
{
public:
    GifEncoder();
    ~GifEncoder();
    static GifEncoder* getInstance();
    void create(std::string filename, int width, int height, int delay);
    void setVideoInputDeletegate(VideoInputDeletegate* videoDelegate);
    void encode();
    void close();
private:
    void OnMessage(rtc::Message *msg);
    
    static GifEncoder* _encoder;
    rtc::Thread* _workerThread;
    rtc::CriticalSection criticalsection_;
    GifWriter _writer;
    int _width;
    int _height;
    int _delay;
    int _skipcount;
    uint8_t* _imageBuffer;

    VideoInputDeletegate *_videoDelegate;
};
#endif /* defined(__BitLive__GifEncoder__) */
