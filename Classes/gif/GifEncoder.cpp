//
//  GifEncoder.cpp
//  BitLive
//
//  Created by keros on 9/20/15.
//
//

#include "GifEncoder.h"

GifEncoder* GifEncoder::_encoder;

GifEncoder::GifEncoder()
:_imageBuffer(nullptr),
_workerThread(nullptr)
{
    _workerThread = new rtc::Thread();
    _workerThread->Start();
}

GifEncoder::~GifEncoder()
{
    if(_imageBuffer)
        delete _imageBuffer;
}

GifEncoder* GifEncoder::getInstance()
{
    if(_encoder==nullptr)
    {
        _encoder = new GifEncoder();
    }
    return _encoder;
}

void GifEncoder::create(std::string filename, int width, int height, int delay)
{
    if(_imageBuffer==nullptr)
        _imageBuffer = new uint8_t[width*height*4];
    _width = width;
    _height = height;
    _delay = delay;
    _skipcount = 1;
    if(!GifBegin(&_writer, filename.c_str(), width, height, delay, 8, true))
    {
        CCLOG("fail to GifBegin");
    }
}

void GifEncoder::setVideoInputDeletegate(VideoInputDeletegate* videoDelegate)
{
    _videoDelegate = videoDelegate;
}

void GifEncoder::OnMessage(rtc::Message *msg)
{
    switch (msg->message_id)
    {
        case 1:
        {
            if(_videoDelegate)
            {
                _videoDelegate->getVideoFrameBGRA(_imageBuffer);
            }
            if(!GifWriteFrame(&_writer, _imageBuffer, _width, _height, _delay))
            {
                CCLOG("fail to GifWriteFrame");
            }
            break;
        }
        case 0:
        {
            GifEnd(&_writer);
            UIMessage* message = new UIMessage(UIMessage::Cmd::FinishCaptureGIF);
            CallStateHandler::getInstance()->UIMessageCallback(message);
            break;
        }
    }
}

void GifEncoder::encode()
{
    _workerThread->Post(this, 1);
    _skipcount=0;
}

void GifEncoder::close()
{
    _workerThread->Clear(this, 1);
    _workerThread->Post(this, 0);
}