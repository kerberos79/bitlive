#include"VoiceDecoder.h"

VoiceDecoder::VoiceDecoder():
pcmBuffer(nullptr),
isStarted(false)
{
}

VoiceDecoder::~VoiceDecoder() {
}

void VoiceDecoder::release() {
	if(isStarted)
	{
		opus_decoder_destroy(dec);
		Device::stopAudioTrack();
		isStarted = false;
	}
}

void VoiceDecoder::create(int buf_size, int samplerate, int channels) {
	if(isStarted)
		return;
	_bufferSize = buf_size;
	_samplerate = samplerate;
	_channels = channels;

    if(!Device::startAudioTrack(_samplerate, _channels))
    {
    	CCLOG("fail to createVoiceOutputDevice");
    }
	dec = opus_decoder_create(_samplerate, _channels, &decError);

    isStarted = true;
}

void VoiceDecoder::decodeStream(Stream *stream)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)||(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    pcmBuffer = Device::getAudioTrackBuffer();
    if(pcmBuffer!=NULL)
    {
        if( (_size = opus_decode(dec, stream->buffer, stream->length, pcmBuffer, MAX_DECODE_DATA_BYTES, 0))<=0)
        {
            CCLOG("fail to opus_decode");
        }
        Device::writeAudioTrack(pcmBuffer, _size);
    }
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	if ((_size = opus_decode(dec, stream->buffer, stream->length, pcmBuffer, MAX_DECODE_DATA_BYTES, 0))>0)
	{
		Device::writeAudioTrack(_size);
	}
#endif
    else
    {
        CCLOG("Device::getAudioTrackBuffer() is null");
    }
}

