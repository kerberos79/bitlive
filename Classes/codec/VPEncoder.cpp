// Refer to this URL
// http://www.webmproject.org/docs/vp8-sdk/example__twopass__encoder.html

#include <stdlib.h>
#include "VPEncoder.h"

VPEncoder::VPEncoder() {
	isCreated = false;
}

VPEncoder::~VPEncoder() {
	release();
}

// framerate : ex) 1024 (kbps)
int VPEncoder::create(int width, int height, int framerate){

	int res;

	codec = &codecs[CODEC_VP8];
    vpx_img_alloc(&enc_raw_data,
    			  VPX_IMG_FMT_YV12,
                  width, height, 32);
    enc_raw_plane_temp[VPX_PLANE_Y] = enc_raw_data.planes[VPX_PLANE_Y];
    enc_raw_plane_temp[VPX_PLANE_U] = enc_raw_data.planes[VPX_PLANE_U];
    enc_raw_plane_temp[VPX_PLANE_V] = enc_raw_data.planes[VPX_PLANE_V];
	/* Construct Encoder Context */

    compressed_frame_header = new unsigned char[MAX_FRAME_VIDEO];

    res = vpx_codec_enc_config_default(codec->iface(),
                                       &enc_config,
                                       0);
    if(res<0) {
    	CCLOG("Failed to get config: %d\n", res);
    	return -1;
    }
    enc_config.rc_target_bitrate = framerate;
	enc_config.g_w = width;
	enc_config.g_h = height;
	enc_config.g_pass = VPX_RC_ONE_PASS;
	enc_config.g_threads = 2;
	enc_config.kf_max_dist = 12000;
//	enc_config.g_error_resilient = 1;
    enc_config.rc_end_usage = VPX_VBR;
    enc_config.g_timebase.num = 1;
    enc_config.g_timebase.den = 15;
	res = vpx_codec_enc_init(&encoder, codec->iface(), &enc_config, 0);
    if(res<0) {
    	CCLOG("Failed to vpx_codec_enc_init() %d\n", res);
    	return -1;
    }

	res = vpx_codec_control(&encoder, VP8E_SET_CPUUSED, 15);
    if(res<0) {
    	CCLOG("Failed to vpx_codec_control(VP8E_SET_CPUUSED) %d\n", res);
    	return -1;
    }
    
    CCLOG("create VPEncoder : %x", encoder);
    
	frame_count = 0;
	frames_out = 0;
	isCreated = true;
	return 1;
}

int VPEncoder::getEncodeBufferSize() {
	return encOutputSize;
}

int VPEncoder::encodeFrame(unsigned char *y, unsigned char *u, unsigned char *v){
	enc_raw_data.planes[VPX_PLANE_Y] = y;
	enc_raw_data.planes[VPX_PLANE_U] = u;
	enc_raw_data.planes[VPX_PLANE_V] = v;

    compressed_frame.buf = (unsigned char*)(compressed_frame_header);
	if( (ret = vpx_codec_set_cx_data_buf(&encoder, &compressed_frame, 0, 0))<0) {
		CCLOG("error : encodeFrame ret = %d", ret);
		return ret;
	}
	if( (ret = vpx_codec_encode(&encoder, &enc_raw_data, frame_count++, 1, 0 , VPX_DL_REALTIME))<0) {
		CCLOG("error : encodeFrame ret = %d", ret);
		return ret;
	}
	vpx_codec_iter_t iter = NULL;
	enc_pkt = vpx_codec_get_cx_data(&encoder, &iter);

    if(enc_pkt->kind == VPX_CODEC_CX_FRAME_PKT) {
        encOutputBuffer = (unsigned char*)enc_pkt->data.frame.buf;// compressed_frame_header;
        encOutputSize = enc_pkt->data.frame.sz;
//        CCLOG("encodeFrame size : %d [%x %x %x %x]", encOutputSize, encOutputBuffer[0], encOutputBuffer[1], encOutputBuffer[2], encOutputBuffer[3]);
    	got_data = 1;
    }
    else if(enc_pkt->kind == VPX_CODEC_STATS_PKT) {
    	CCLOG("encodeFrame stat_pkt %d", enc_pkt->data.frame.sz);
    	got_data = 0;
    }
    else {
    	CCLOG("encode default");
    	got_data = 0;
    }
	return ret;
}

int VPEncoder::encodeKeyFrame(unsigned char *y, unsigned char *u, unsigned char *v){
	enc_raw_data.planes[VPX_PLANE_Y] = y;
	enc_raw_data.planes[VPX_PLANE_U] = u;
	enc_raw_data.planes[VPX_PLANE_V] = v;

    compressed_frame.buf = (unsigned char*)(compressed_frame_header);
	if( (ret = vpx_codec_set_cx_data_buf(&encoder, &compressed_frame, 0, 0))<0) {
		CCLOG("error : encodeFrame ret = %d", ret);
		return ret;
	}
	if( (ret = vpx_codec_encode(&encoder, &enc_raw_data, frame_count++, 1, VPX_FRAME_IS_KEY, VPX_DL_REALTIME))<0) {
		CCLOG("error : encodeFrame ret = %d", ret);
		return ret;
	}
	vpx_codec_iter_t iter = NULL;
	enc_pkt = vpx_codec_get_cx_data(&encoder, &iter);
    
    if(enc_pkt->kind == VPX_CODEC_CX_FRAME_PKT) {
        encOutputBuffer = (unsigned char*)enc_pkt->data.frame.buf;// compressed_frame_header;
        encOutputSize = enc_pkt->data.frame.sz;
//        CCLOG("encodeFrame size : %d [%x %x %x %x]", encOutputSize, encOutputBuffer[0], encOutputBuffer[1], encOutputBuffer[2], encOutputBuffer[3]);
    	got_data = 1;
    }
    else if(enc_pkt->kind == VPX_CODEC_STATS_PKT) {
    	CCLOG("encodeFrame stat_pkt %d", enc_pkt->data.frame.sz);
    	got_data = 0;
    }
    else {
    	CCLOG("encode default");
    	got_data = 0;
    }
	return ret;
}

void VPEncoder::release(){
	if(isCreated) {
		vpx_codec_destroy(&encoder);

		enc_raw_data.planes[VPX_PLANE_Y] = enc_raw_plane_temp[VPX_PLANE_Y];
		enc_raw_data.planes[VPX_PLANE_U] = enc_raw_plane_temp[VPX_PLANE_U];
		enc_raw_data.planes[VPX_PLANE_V] = enc_raw_plane_temp[VPX_PLANE_V];
		vpx_img_free(&enc_raw_data);
		if(compressed_frame_header!=NULL) {
			delete compressed_frame_header;
			compressed_frame_header = NULL;
		}
		isCreated = false;
	}

}

int VPEncoder::setFramerate(int rate){
	int res;
	release();
    enc_config.rc_target_bitrate = rate;
    frame_count = 0;
	res = vpx_codec_enc_init(&encoder, codec->iface(), &enc_config, 0);
    if(res<0) {
    	CCLOG("Failed to vpx_codec_enc_init() %d\n", res);
    	return -1;
    }

	res = vpx_codec_control(&encoder, VP8E_SET_CPUUSED, 0);
    if(res<0) {
    	CCLOG("Failed to vpx_codec_control(VP8E_SET_CPUUSED) %d\n", res);
    	return -1;
    }

	return 1;
}
