#ifndef __VPENCODE_H__
#define __VPENCODE_H__

extern "C" {
#include <vpx_codec.h>
#include <vpx_encoder.h>
#include <vpx_decoder.h>
#include <vp8.h>
#include <vp8cx.h>
#include <vp8dx.h>
}
#include "cocos2d.h"
#include "util/define.h"
#include "VPCodecCommon.h"

#define NUM_ENCODE_FRAME		16

class VPEncoder {
public :
	unsigned char *encOutputBuffer;
	int encOutputSize;
private:
	bool					  isCreated;
	vpx_codec_ctx_t           encoder;
	struct vpx_codec_enc_cfg  enc_config;
	vpx_image_t            	  enc_raw_data;
	unsigned char*			  enc_raw_plane_temp[4];
	vpx_fixed_buf_t			  compressed_frame;
	unsigned char			  *compressed_frame_header;
	const vpx_codec_cx_pkt_t *enc_pkt;
	long					  frame_count;
	int						  frames_out;
	struct codec_item  		  *codec;
	vpx_codec_iter_t 		  iter;
	int 					  got_data;
	int 					  ret;
public:
	VPEncoder();
	~VPEncoder();

	int create(int width, int height, int framerate);
	int setFramerate(int framerate);
	int getEncodeBufferSize();
	int encodeFrame(unsigned char *y, unsigned char *u, unsigned char *v);
	int encodeKeyFrame(unsigned char *y, unsigned char *u, unsigned char *v);
	void release();

};


#endif
