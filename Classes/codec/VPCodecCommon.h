#ifndef __VPCODEC_COMMON_H__
#define __VPCODEC_COMMON_H__

#define CODEC_VP8 		0
#define CODEC_VP9		1

#define VP8_FOURCC (0x30385056)
#define VP9_FOURCC (0x30395056)
static struct codec_item {
  char const              *name;
  const vpx_codec_iface_t *(*iface)(void);
  const vpx_codec_iface_t *(*dx_iface)(void);
  unsigned int             fourcc;
} codecs[] = {
  {"vp8", &vpx_codec_vp8_cx, &vpx_codec_vp8_dx, VP8_FOURCC},
};

#endif
