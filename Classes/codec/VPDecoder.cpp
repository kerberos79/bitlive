// Refer to this URL
// http://www.webmproject.org/docs/vp8-sdk/example__simple__decoder.html

#include "VPDecoder.h"

VPDecoder::VPDecoder() {
    updatedFrame = false;
}

VPDecoder::~VPDecoder() {
}


int VPDecoder::create(){

	codec = &codecs[CODEC_VP8];
	vpx_codec_err_t ret;
	/* Construct Encoder Context */
//    flags = VPX_CODEC_USE_ERROR_CONCEALMENT;
	if( (ret = vpx_codec_dec_init(&decoder, codec->dx_iface(), NULL, 0)) !=0) {
		CCLOG("fail to vpx_codec_dec_init1 %d", ret);
		return -1;
	}

	CCLOG("vpx_codec_dec_init");
	return 1;
}

int VPDecoder::decodeFrame(Stream *stream){

    criticalsection.Enter();
	vpx_codec_err_t ret;
	if( (ret = vpx_codec_decode(&decoder, stream->buffer, stream->length, NULL, 0))!= 0) {
		CCLOG("fail to vpx_codec_decode ret = %d", ret);
        criticalsection.Leave();
		return ret;
	}
	vpx_codec_iter_t iter = NULL;
	dec_frame_data = vpx_codec_get_frame(&decoder, &iter);
    updatedFrame = true;
    criticalsection.Leave();
	return ret;
}

int VPDecoder::decodeGFrame(Stream *stream){

    criticalsection.Enter();
	vpx_codec_err_t ret;
	if( (ret = vpx_codec_decode(&decoder, stream->buffer, stream->length, NULL, 0))!= 0) {
		CCLOG("fail to vpx_codec_decode ret = %d", ret);
        criticalsection.Leave();
		return ret;
	}
	vpx_codec_iter_t iter = NULL;
	dec_frame_data = vpx_codec_get_frame(&decoder, &iter);
    updatedFrame = true;
    criticalsection.Leave();
	return ret;
}

bool VPDecoder::udpateDecodeFrame(YUVSprite* sprite)
{
    criticalsection.Enter();
    if(!updatedFrame)
    {
        criticalsection.Leave();
        return false;
    }
    sprite->updateDirect((unsigned char*)dec_frame_data->planes[VPX_PLANE_Y],
                         (unsigned char*)dec_frame_data->planes[VPX_PLANE_U],
                         (unsigned char*)dec_frame_data->planes[VPX_PLANE_V]);
    updatedFrame = false;
    criticalsection.Leave();
    return true;
}

void VPDecoder::release(){
    criticalsection.Enter();
    updatedFrame = false;
	vpx_codec_destroy(&decoder);
    criticalsection.Leave();
}
