//
//  OpenALPlayer.cpp
//  Vista
//
//  Created by kerberos on 3/7/15.
//
//

#include "OpenALPlayer.h"

OpenALPlayer::OpenALPlayer(int samplerate)
:_device(nullptr),
_context(nullptr),
_source(0)
{
    Init(samplerate);
}

OpenALPlayer::~OpenALPlayer()
{
    Release();
}

void OpenALPlayer::Init(int samplerate)
{
    ALenum err;
    _frequency = samplerate;
    const ALint context_attribs[] = { ALC_FREQUENCY, samplerate, 0 };
    _device = alcOpenDevice("mic");
    _context = alcCreateContext(_device, context_attribs);
    alcMakeContextCurrent(_context);
    alGenSources(1, &_source );
    
//    alSourcei(_source, AL_BUFFER, 0);
    for(int i=0;i<OPENAL_BUFFER_SIZE;i++)
    {
        alGenBuffers(1,&_buffer);
    }
    
    alSourcePlay(_source);
    err = alGetError();
    if (err != 0)
        printf("Error alSourcePlay!");
}

void OpenALPlayer::Write(unsigned char* buffer, int size)
{
    int buffersProcessed = 0;
    alGetSourcei(_source, AL_BUFFERS_PROCESSED, &buffersProcessed);
    
    // check to see if we have a buffer to deQ
    if (buffersProcessed > 0) {
        alSourceUnqueueBuffers(_source, 1, &_buffer);
        alBufferData(_buffer, AL_FORMAT_MONO16, buffer, size, _frequency);
        alSourceQueueBuffers(_source, 1, &_buffer);
    }
}

void OpenALPlayer::Release()
{
    alDeleteSources(1, &_source);
    
				
    // Release audio buffer
    for(int i=0;i<OPENAL_BUFFER_SIZE;i++)
    {
        alSourceUnqueueBuffers(_source, 1, &_buffer);
        alDeleteBuffers(1, &_buffer);
    }
    
    // Cleaning up
    alcMakeContextCurrent(0);
    alcDestroyContext(_context);
    alcCloseDevice(_device);
}
