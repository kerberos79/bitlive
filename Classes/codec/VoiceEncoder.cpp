#include"VoiceEncoder.h"

VoiceEncoder::VoiceEncoder() {
	encOutputBuffer = new unsigned char[MAX_OUTPUT_BUFFER_BYTES];
}

VoiceEncoder::~VoiceEncoder() {
	if(encOutputBuffer) {
		delete encOutputBuffer;
		encOutputBuffer = 0;
	}
}

void VoiceEncoder::release() {

	if(enc) {
		opus_encoder_destroy(enc);
		enc = 0;
	}
}
int VoiceEncoder::create(double duration, int samplerate, int channels) {
	enc = opus_encoder_create(samplerate, channels, OPUS_APPLICATION_VOIP, &encError);
    if (encError != OPUS_OK)
    {
//    	LOGI("Cannot create encoder: %s\n", opus_strerror(encError));
    	return encError;
    }
    samplesPerFrame = (int)(duration * samplerate);
#if 1
    opus_encoder_ctl(enc, OPUS_SET_COMPLEXITY(10));
    opus_encoder_ctl(enc, OPUS_SET_BITRATE(16000));
    opus_encoder_ctl(enc, OPUS_SET_BANDWIDTH(OPUS_BANDWIDTH_WIDEBAND));
    opus_encoder_ctl(enc, OPUS_SET_VBR(1));
    opus_encoder_ctl(enc, OPUS_SET_VBR_CONSTRAINT(0));
    opus_encoder_ctl(enc, OPUS_SET_INBAND_FEC(0));
    opus_encoder_ctl(enc, OPUS_SET_DTX(0));
    opus_encoder_ctl(enc, OPUS_SET_PACKET_LOSS_PERC(0));
    opus_encoder_ctl(enc, OPUS_SET_LSB_DEPTH(16));
    
    opus_encoder_ctl(enc, OPUS_SET_SIGNAL(OPUS_SIGNAL_VOICE));
#endif
	return encError;
}

int VoiceEncoder::encodeFrame(const opus_int16 *pcm) {
	encOutputSize = opus_encode(enc, pcm, samplesPerFrame, encOutputBuffer, MAX_ENCODE_DATA_BYTES);
	return encOutputSize;
}
