#ifndef __VPDECODE_H__
#define __VPDECODE_H__

#include <stdlib.h>
#include <time.h>
#include "cocos2d.h"
extern "C" {
#include <vpx_codec.h>
#include <vpx_encoder.h>
#include <vpx_decoder.h>
#include <vp8.h>
#include <vp8cx.h>
#include <vp8dx.h>
}
#include "webrtc/base/criticalsection.h"
#include "VPCodecCommon.h"
#include "VPEncoder.h"
#include "stream/Stream.h"
#include "view/YUVSprite.h"

class VPDecoder {
public :
private:
	vpx_codec_ctx_t           decoder;
	vpx_image_t            	  *dec_frame_data;
	struct codec_item  		  *codec;
	unsigned char 			  *pBufferPlaneY;
	unsigned char 			  *pBufferPlaneU;
	unsigned char 			  *pBufferPlaneV;

	int						  decodeFrameSize;
    rtc::CriticalSection      criticalsection;
    bool updatedFrame;
public:
	VPDecoder();
	~VPDecoder();

	int create();
	int decodeFrame(Stream *stream);
	int decodeGFrame(Stream *stream);
    bool udpateDecodeFrame(YUVSprite* sprite);
	void release();
};


#endif
