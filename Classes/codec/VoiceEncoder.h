#ifndef __VOICE_ENCODER_H__
#define __VOICE_ENCODER_H__
#include <opus.h>
#include <opus_defines.h>
#include "cocos2d.h"
#include "util/define.h"

#define MAX_ENCODE_DATA_BYTES sizeof(opus_int16)*960
#define MAX_OUTPUT_BUFFER_BYTES         960*2

class VoiceEncoder {
public :
	unsigned char *encOutputBuffer;
	int encOutputSize;
private :
	OpusEncoder *enc;
    int samplesPerFrame;
	int encError;
    opus_int32 skip;

	int decError;
	int decResult;
public:
	VoiceEncoder();
	~VoiceEncoder();

	int create(double duration, int samplerate, int channels);
    int encodeFrame(const opus_int16 *pcm);
	void writeFrame();
	void release();
};

#endif
