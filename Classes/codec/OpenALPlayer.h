//
//  OpenALPlayer.h
//  Vista
//
//  Created by kerberos on 3/7/15.
//
//

#ifndef __Vista__OpenALPlayer__
#define __Vista__OpenALPlayer__

#include <stdio.h>
#include "cocos2d.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <AL/al.h>
#include <AL/alc.h>
#endif

#define OPENAL_BUFFER_SIZE  3
class OpenALPlayer {
public:
    OpenALPlayer(int samplerate);
    ~OpenALPlayer();
    
    void Init(int samplerate);
    void Release();
    void Write(unsigned char* buffer, int size);
private:
    ALCdevice* _device;
    ALCcontext* _context;
    ALuint _buffer;
    ALuint _source;
    int _frequency;
};
#endif /* defined(__Vista__OpenALPlayer__) */
