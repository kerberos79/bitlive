#ifndef __VOICE_DECODER_H__
#define __VOICE_DECODER_H__
#include <opus.h>
#include <opus_defines.h>
#include "cocos2d.h"
#include "util/define.h"
#include "stream/Stream.h"

using namespace cocos2d;
#define MAX_DECODE_DATA_BYTES 1000
#define BUFFER_QUEUE_SIZE	10
#define STREAM_BUFFER_SIZE	256

class VoiceDecoder{
public :
public:
	VoiceDecoder();
	~VoiceDecoder();

	void create(int buf_size, int samplerate, int channels);
    void decodeStream(Stream *stream);
	void release();

	opus_int16 *pcmBuffer;
private :

    OpusDecoder *dec;
    int decError;
    int decResult;
    int	decodeFrameSize;
    int _bufferSize, _samplerate, _channels;
    int _size;
    bool isStarted;
};

#endif
