
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    vec3 p=v_texCoord.xyz - 0.5;
    
    vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, 0.5+(p.xy*=0.98)).r,
                                    texture2D(u_texture, 0.5+(p.xy*=0.98)).r - 0.5,
                                    texture2D(v_texture, 0.5+(p.xy*=0.98)).r - 0.5);
    vec3 o=rgbColor.rbb;
    for (float i=0.;i<50.;i++)
    {
        p.z+=pow(max(0.0, 0.5-length(rgbColor.rg)), 2.0)*exp(-i*.1);
    }
    gl_FragColor=vec4(o*o+p.z,1);
}

