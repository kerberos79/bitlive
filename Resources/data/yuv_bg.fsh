
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform float iGlobalTime;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
float wave(float x)
{
    return 0.5+0.5*sin(x*2.0);
}

void main() {
//    gl_FragColor = vec4(v_texCoord,0.5-0.5*sin(iGlobalTime),1.0);
    gl_FragColor = vec4(wave(v_texCoord.x+iGlobalTime*2.),wave((v_texCoord.y+iGlobalTime*0.25)),wave(v_texCoord.x*v_texCoord.y),1.0);
}

