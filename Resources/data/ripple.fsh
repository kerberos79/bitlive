
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
float x(float t) {
    t = mod(t, 4.0);
    return abs(t) - abs(t-1.0) - abs(t-2.0) + abs(t-3.0) - 1.0;
}

void main() {
    /*
     vec2 uv = fragCoord.xy / iResolution.xy;
     // uv.x *= iResolution.x / iResolution.y;
     vec2 p = abs(mod(uv*100.0, 1.0));
     vec2 cell = floor(uv*100.0);
     float t = iGlobalTime*7.0 + atan(cell.y+0.01,cell.x)/1.57*4.0+ length(texture2D(iChannel0,uv));
     t *= 1.5;
     vec2 s = vec2(x(t), x(t-1.0))*0.35+0.5;
     float d = max(abs(p.x-s.x), abs(p.y-s.y))* 0.9725;
     fragColor = vec4(smoothstep(0.25, 0.051, d))* texture2D(iChannel0,cell / 100.0).bbra * texture2D(iChannel0,cell / 100.0).rbra * 3.2 ;
     */
    
    vec2 uv = v_texCoord;
    vec2 p = abs(mod(uv*100.0, 1.0));
    vec2 cell = floor(uv*100.0);
    
    vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                    texture2D(u_texture, v_texCoord).r - 0.5,
                                    texture2D(v_texture, v_texCoord).r - 0.5);
    float t = iGlobalTime*7.0 + atan(cell.y+0.01,cell.x)/1.57*4.0+ length(rgbColor);
    t *= 1.5;
    vec2 s = vec2(x(t), x(t-1.0))*0.35+0.5;
    float d = max(abs(p.x-s.x), abs(p.y-s.y))* 0.9725;
    
    vec3 color = YUVtoRGB * vec3(texture2D(CC_Texture0, cell / 100.0).r,
                                 texture2D(u_texture, cell / 100.0).r - 0.5,
                                 texture2D(v_texture, cell / 100.0).r - 0.5);
    
    gl_FragColor = vec4(smoothstep(0.25, 0.051, d))* color.bbr * color.rbr * 3.2, 1.0) ;
}

