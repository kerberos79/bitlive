
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
vec3 b;
float gWT;
float distanceFromCentralColor;
float gW;
float l;
float sum;
const float distanceNormalizationFactor = 16.0;
void main() {
  float stepW1 =4.0/imageWidth;
  float stepNW1 =-4.0/imageWidth;
  float stepH1 =4.0/imageHeight;
  float stepNH1 =-4.0/imageHeight;
  vec3 yuvColor = vec3(texture2D(CC_Texture0, v_texCoord).r,
                       texture2D(u_texture, v_texCoord).r - 0.5,
                       texture2D(v_texture, v_texCoord).r - 0.5);
  gWT = 0.18;
  sum = yuvColor.r * 0.18;
  l =texture2D(CC_Texture0, v_texCoord - vec2(stepW1, stepH1)).r;
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.1 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  l =texture2D(CC_Texture0, v_texCoord - vec2(0.0, stepH1)).r; //1
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.14 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  l =texture2D(CC_Texture0, v_texCoord - vec2(stepW1, 0.0)).r; //2
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.17 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  l =texture2D(CC_Texture0, v_texCoord + vec2(stepW1, 0.0)).r; //3
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.17 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  l =texture2D(CC_Texture0, v_texCoord + vec2(0.0, stepH1)).r; //4
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.14 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  l =texture2D(CC_Texture0, v_texCoord + vec2(-stepW1, stepH1)).r; //5
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.1 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  yuvColor.r = sum / gWT;
  b = YUVtoRGB * yuvColor;
  const float bA = 1.0;
  const float oA=1.0;
  float ra;
  if (2.0 * b.r < bA) {
      ra = 2.0 * b.r * b.r + b.r * (1.0 - bA) + b.r * (1.0 - oA);
  } else {
      ra = oA * bA - 2.0 * (bA - b.r) * (oA - b.r) + b.r * (1.0 - bA) + b.r * (1.0 - oA);
  }
  gl_FragColor = vec4(ra, ra, ra, 1.0);
}