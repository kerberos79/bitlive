
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

float value;
void main() {
    float stepW2 =0.6/imageWidth;
    float stepW1 =0.3/imageWidth;
    float stepNW2 =-0.6/imageWidth;
    float stepNW1 =-0.3/imageWidth;
    float stepH2 =0.6/imageHeight;
    float stepH1 =0.3/imageHeight;
    float stepNH2 =-0.6/imageHeight;
    float stepNH1 =-0.3/imageHeight;
    value = texture2D(CC_Texture0, v_texCoord).r;
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepNW2, stepNH2)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, stepNH2)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(0.0, stepNH2)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepW1, stepNH2)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepW2, stepNH2)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepNW2, stepNH1)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, stepNH1)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(0.0, stepNH1)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepW1, stepNH1)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepW2, stepNH1)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepNW2, 0.0)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, 0.0)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepW1, 0.0)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepW2, 0.0)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepNW2, stepH1)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, stepH1)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(0.0, stepH1)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepW1, stepH1)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepW2, stepH1)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepNW2, stepH2)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, stepH2)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(0.0, stepH2)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepW1, stepH2)).r);
    value = min(value, texture2D(CC_Texture0, v_texCoord+vec2(stepW2, stepH2)).r);
    vec3 yuvColor = vec3(value,
                         texture2D(CC_Texture0, v_texCoord).g - 0.5,
                         texture2D(CC_Texture0, v_texCoord).b - 0.5);
    gl_FragColor = vec4(YUVtoRGB * yuvColor , 1.0);
}