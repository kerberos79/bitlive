
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

float stepW1 =1.0/imageWidth;
float stepNW1 =-1.0/imageWidth;
float stepH1 =1.0/imageHeight;
float stepNH1 =-1.0/imageHeight;
vec3 cC;
float gWT;
float distanceFromCentralColor;
float gW;
float l;
float sum;
const float distanceNormalizationFactor = 10.0;
void main() {
    vec3 yuvColor = vec3(texture2D(CC_Texture0, v_texCoord).r,
                         texture2D(u_texture, v_texCoord).r,
                         texture2D(v_texture, v_texCoord).r);
    gWT = 0.18;
    sum = yuvColor.r * 0.18;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, stepNH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.05 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(0.0, stepNH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.09 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepW1, stepNH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.12 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, 0.0)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.15 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepW1, 0.0)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.15 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, stepH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.12 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(0.0, stepH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.09 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepW1, stepH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.05 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    yuvColor.r = sum / gWT;
    gl_FragColor = vec4(yuvColor , 1.0);
}