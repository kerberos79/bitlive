
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;

void main() {
    vec3 yuvColor = vec3(texture2D(CC_Texture0, v_texCoord).r * 0.093604,
                         texture2D(u_texture, v_texCoord).r,
                         texture2D(v_texture, v_texCoord).r);
    
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.092000)).r*0.000554;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.088000)).r*0.000812;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.084000)).r*0.001171;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.080000)).r*0.001660;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.076000)).r*0.002313;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.072000)).r*0.003168;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.068000)).r*0.004267;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.064000)).r*0.005650;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.060000)).r*0.007355;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.056000)).r*0.009413;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.052000)).r*0.011843;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.048000)).r*0.014650;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.044000)).r*0.017816;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.040000)).r*0.021300;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.036000)).r*0.025037;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.032000)).r*0.028932;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.028000)).r*0.032870;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.024000)).r*0.036713;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.020000)).r*0.040314;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.016000)).r*0.043522;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.012000)).r*0.046192;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.008000)).r*0.048200;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y-0.004000)).r*0.049445;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.004000)).r*0.049445;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.008000)).r*0.048200;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.012000)).r*0.046192;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.016000)).r*0.043522;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.020000)).r*0.040314;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.024000)).r*0.036713;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.028000)).r*0.032870;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.032000)).r*0.028932;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.036000)).r*0.025037;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.040000)).r*0.021300;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.044000)).r*0.017816;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.048000)).r*0.014650;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.052000)).r*0.011843;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.056000)).r*0.009413;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.060000)).r*0.007355;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.064000)).r*0.005650;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.068000)).r*0.004267;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.072000)).r*0.003168;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.076000)).r*0.002313;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.080000)).r*0.001660;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.084000)).r*0.001171;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.088000)).r*0.000812;
      yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y+0.092000)).r*0.000554;
    
    gl_FragColor = vec4(yuvColor,1.0);
}

