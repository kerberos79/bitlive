
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
const vec3 COLOR1 = vec3(1.0, 0.891, 0.733);
vec3 RGBToHSL(vec3 color)
{
	vec3 hsl;
	float fmin = min(min(color.r, color.g), color.b);
	float fmax = max(max(color.r, color.g), color.b);
	float delta = fmax - fmin;
	hsl.z = (fmax + fmin) / 2.0;
	if (delta == 0.0)
	{
		hsl.x = 0.0;
		hsl.y = 0.0;
	}
	else
	{
		if (hsl.z < 0.5)
			hsl.y = delta / (fmax + fmin);
		else
			hsl.y = delta / (2.0 - fmax - fmin);

		float deltaR = (((fmax - color.r) / 6.0) + (delta / 2.0)) / delta;
		float deltaG = (((fmax - color.g) / 6.0) + (delta / 2.0)) / delta;
		float deltaB = (((fmax - color.b) / 6.0) + (delta / 2.0)) / delta;

		if (color.r == fmax )
			hsl.x = deltaB - deltaG;
		else if (color.g == fmax)
			hsl.x = (1.0 / 3.0) + deltaR - deltaB;
		else if (color.b == fmax)
			hsl.x = (2.0 / 3.0) + deltaG - deltaR;

		if (hsl.x < 0.0)
			hsl.x += 1.0;
		else if (hsl.x > 1.0)
			hsl.x -= 1.0;
	}

	return hsl;
}

float HueToRGB(float f1, float f2, float hue)
{
	if (hue < 0.0)
		hue += 1.0;
	else if (hue > 1.0)
		hue -= 1.0;
	float res;
	if ((6.0 * hue) < 1.0)
		res = f1 + (f2 - f1) * 6.0 * hue;
	else if ((2.0 * hue) < 1.0)
		res = f2;
	else if ((3.0 * hue) < 2.0)
		res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;
	else
		res = f1;
	return res;
}
vec3 HSLToRGB(vec3 hsl)
{
	vec3 rgb;

	if (hsl.y == 0.0)
		rgb = vec3(hsl.z); 
	else
	{
		float f2;

		if (hsl.z < 0.5)
			f2 = hsl.z * (1.0 + hsl.y);
		else
			f2 = (hsl.z + hsl.y) - (hsl.y * hsl.z);

		float f1 = 2.0 * hsl.z - f2;

		rgb.r = HueToRGB(f1, f2, hsl.x + (1.0/3.0));
		rgb.g = HueToRGB(f1, f2, hsl.x);
		rgb.b= HueToRGB(f1, f2, hsl.x - (1.0/3.0));
	}

	return rgb;
}

vec3 saturation(vec3 color, float sat) {
	const float lumaR = 0.212671;
	const float lumaG = 0.715160;
	const float lumaB = 0.072169;

	float v = sat + 1.0;
	float i = 1.0 - v;
	float r = i * lumaR;
	float g = i * lumaG;
	float b = i * lumaB;

	mat3 mat = mat3( r + v, r, r, g, g + v, g, b, b, b + v );
	return mat * color;
}

void main() {
  vec3 RGB = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                             texture2D(u_texture, v_texCoord).r - 0.5,
                             texture2D(v_texture, v_texCoord).r - 0.5);
  RGB.r = RGB.r * 0.843 + 0.157;
  RGB.b = RGB.b * 0.882 + 0.118;

  vec3 hsv = RGBToHSL(RGB);
  hsv.y = hsv.y * 0.55;
  RGB = HSLToRGB(hsv);

  RGB = saturation(RGB, 0.7);
  RGB *= COLOR1;
  gl_FragColor = vec4(RGB.rgb, 1.0);
}