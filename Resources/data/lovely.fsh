
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform sampler2D rgbTexture2;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

#define GammaCorrection(color, gamma)		pow(color, vec3(gamma))

/*
 ** Levels correction for output slider
 ** Details: http://mouaif.wordpress.com/2009/01/28/levels-control-shader/
 */
#define LevelsControlInputRange(color, minInput, maxInput) min(max(color - vec3(minInput), vec3(0.0)) / (vec3(maxInput) - vec3(minInput)), vec3(1.0))
#define LevelsControlInput(color, minInput, gamma, maxInput) GammaCorrection(LevelsControlInputRange(color, minInput, maxInput), gamma)

#define LevelsControlOutputRange(color, minOutput, maxOutput) mix(vec3(minOutput), vec3(maxOutput), color)

#define LevelsControl(color, minInput, gamma, maxInput, minOutput, maxOutput) 	LevelsControlOutputRange(LevelsControlInput(color, minInput, gamma, maxInput), minOutput, maxOutput)

void main() {
    lowp vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                         texture2D(u_texture, v_texCoord).r - 0.5,
                                         texture2D(v_texture, v_texCoord).r - 0.5);
    
    if(rgbColor.r < 0.0)
        rgbColor.r = 0.0;
    else if(rgbColor.r > 1.0)
        rgbColor.r = 1.0;
    if(rgbColor.g < 0.0)
        rgbColor.g = 0.0;
    else if(rgbColor.g > 1.0)
        rgbColor.g = 1.0;
    if(rgbColor.b < 0.0)
        rgbColor.b = 0.0;
    else if(rgbColor.b > 1.0)
        rgbColor.b = 1.0;
    lowp vec3 outputColor;
    outputColor.rgb = rgbColor.rgb;
    //1.0/1.3
    lowp vec3 pass1 = LevelsControlInput(outputColor,0.0,1.46923077,0.9254902);
    
    //33/255
    lowp vec3 pass2 = LevelsControlOutputRange(pass1,0.14509804,1.0);
    
    //130/255
    lowp vec3 pass3 = LevelsControlOutputRange(pass1,0.38156863,1.0);
    
    lowp vec3 pass4;
    pass4.r = pass1.r;
    pass4.g = pass2.g;
    pass4.b = pass3.b;
    
    // decrease brightness
    lowp vec3 pass5 = pass4 + vec3(0.06);
    
    // auto contrast
    lowp vec3 pass6 = (pass5 - vec3(0.5)) * 1.53 + vec3(0.5);
    
    pass6 *= texture2D(rgbTexture2, v_texCoord).rgb;
    
    float vignetteAmount;
    float distanceFromCenter = length( v_texCoord - vec2(0.5,0.5) );
    vignetteAmount = 1.0 - distanceFromCenter;
    vignetteAmount = smoothstep(0.1, 1.0, vignetteAmount);
    // vignetting
    pass6 *=  vignetteAmount*1.0;
    gl_FragColor = vec4(pass6,1.0);
}

