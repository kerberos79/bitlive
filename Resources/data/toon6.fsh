// https://www.shadertoy.com/view/MdfGR2
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform sampler2D rgbTexture2;
uniform float iGlobalTime;
const vec2 iResolution = vec2(480, 640);

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    vec2 uv = v_texCoord;
    float tempy = iResolution.y/30.0;
    float tempx = iResolution.x/60.0;
    
    vec4 col = vec4(uv,0.5+0.5*sin(iGlobalTime),1.0);
    float t = iGlobalTime/100.0;
    
    vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, uv).r,
                                    texture2D(u_texture, uv).r - 0.5,
                                    texture2D(v_texture, uv).r - 0.5);
    col.x+=rgbColor.x/2.0;
    col.y+=texture2D(rgbTexture2,uv).x/2.0;
    gl_FragColor = col;
}

