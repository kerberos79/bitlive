
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform sampler2D rgbTexture2;

const mat3 saturate = mat3(
                           1.210300,
                           -0.089700,
                           -0.091000,
                           -0.176100,
                           1.123900,
                           -0.177400,
                           -0.034200,
                           -0.034200,
                           1.265800);
const vec3 rgbPrime = vec3(0.25098, 0.14640522, 0.0);
const vec3 desaturate = vec3(.3, .59, .11);

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                 texture2D(u_texture, v_texCoord).r - 0.5,
                                 texture2D(v_texture, v_texCoord).r - 0.5);
    if(rgbColor.r < 0.0)
        rgbColor.r = 0.0;
    else if(rgbColor.r > 1.0)
        rgbColor.r = 1.0;
    if(rgbColor.g < 0.0)
        rgbColor.g = 0.0;
    else if(rgbColor.g > 1.0)
        rgbColor.g = 1.0;
    if(rgbColor.b < 0.0)
        rgbColor.b = 0.0;
    else if(rgbColor.b > 1.0)
        rgbColor.b = 1.0;
    lowp vec3 outputColor;
    
    vec2 tc = (2.0 * norm) - 1.0;
    float d = dot(tc, tc);
    vec2 lookup = 255.*vec2(d, rgbColor.r);
    lookup.y = 255.*rgbColor.g;
    lookup.y = 255.*rgbColor.b;
    
    vec2 red = 256.*vec2(rgbColor.r, 0.16666);
    vec2 green = 256.*vec2(rgbColor.g, 0.5);
    vec2 blue = 256.*vec2(rgbColor.b, 0.83333);
    rgbColor.r = texture2D(rgbTexture2, red).r;
    rgbColor.g = texture2D(rgbTexture2, green).g;
    rgbColor.b = texture2D(rgbTexture2, blue).b;
    gl_FragColor = vec4(rgbColor.rgb,1.0);
}

