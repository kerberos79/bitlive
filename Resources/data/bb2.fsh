
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

float stepW1 =1.0/imageWidth;
float stepNW1 =-1.0/imageWidth;
float stepH1 =1.0/imageHeight;
float stepNH1 =-1.0/imageHeight;

const float base_a = 1.0;
const float overlay_a = 1.0;

vec3 cC;
float gWT;
float distanceFromCentralColor;
float gW;
float l;
float sum;
const float distanceNormalizationFactor = 10.0;

float overlay_yuv(float base, float overlay)
{
    float value;
    if (2.0 * base < base_a) {
        value = 2.0 * overlay * base + overlay * (1.0 - base_a) + base * (1.0 - overlay_a);
    } else {
        value = overlay_a * base_a - 2.0 * (base_a - base) * (overlay_a - overlay) + overlay * (1.0 - base_a) + base * (1.0 - overlay_a);
    }
    
    return value;
}
void main() {
    vec3 yuvColor = vec3(texture2D(CC_Texture0, v_texCoord).r,
                         texture2D(CC_Texture0, v_texCoord).g - 0.5,
                         texture2D(CC_Texture0, v_texCoord).b - 0.5);
    gWT = 0.18;
    sum = yuvColor.r * 0.18;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, stepNH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.05 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(0.0, stepNH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.09 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepW1, stepNH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.12 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, 0.0)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.15 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepW1, 0.0)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.15 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepNW1, stepH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.12 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(0.0, stepH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.09 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    l =texture2D(CC_Texture0, v_texCoord+vec2(stepW1, stepH1)).r;
    distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
    gW = 0.05 * (1.0 - distanceFromCentralColor);
    gWT += gW;
    sum += l * gW;
    yuvColor.r = sum / gWT;
    gl_FragColor = vec4(YUVtoRGB * yuvColor , 1.0);
}