
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;
const float intensity = 0.8;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

float uScale = 1.0; // For imperfect, isotropic anti-aliasing in
float uYrot = 0.01;  // absence of dFdx() and dFdy() functions
float frequency = 120.0; //  간격

float aastep(float threshold, float value) {
    float afwidth = frequency * (1.0/200.0) / uScale / cos(uYrot);
    return smoothstep(threshold-afwidth, threshold+afwidth, value);
}

void main() {
    float step_w = 1.0/imageWidth;
    float step_h = 1.0/imageHeight;
    float step_w2 = 2.0/imageWidth;
    float step_h2 = 2.0/imageHeight;
    float step_w3 = 3.0/imageWidth;
    float step_h3 = 3.0/imageHeight;
    float step_w4 = 4.0/imageWidth;
    float step_h4 = 4.0/imageHeight;
    vec3 yuvColor = vec3(texture2D(CC_Texture0, v_texCoord).r,
                         texture2D(u_texture, v_texCoord).r - 0.5,
                         texture2D(v_texture, v_texCoord).r - 0.5);
    float bloom = yuvColor.r * 0.32 +
    (texture2D(CC_Texture0, vec2(v_texCoord.x - step_w4, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x + step_w4, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - step_h4)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + step_h4)).r ) * 0.05 +
    (texture2D(CC_Texture0, vec2(v_texCoord.x - step_w3, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x + step_w3, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - step_h3)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + step_h3)).r ) * 0.09 +
    (texture2D(CC_Texture0, vec2(v_texCoord.x - step_w2, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x + step_w2, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - step_h2)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + step_h2)).r )* 0.12 +
    (texture2D(CC_Texture0, vec2(v_texCoord.x - step_w, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x + step_w, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - step_h)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + step_h)).r )* 0.15;
    float mono = bloom * intensity;
    if(mono>1.0)
        mono = 1.0;
    yuvColor.r = mono;
    {
        if(yuvColor.r>0.6) {
            yuvColor.r = 0.8;
        }
        else if(yuvColor.r>0.4) {
            yuvColor.r = 0.6;
        }
        else if(yuvColor.r>0.2)
            yuvColor.r = 0.2;
        else
            yuvColor.r = 0.0;
    }
    vec2 st2 = mat2(0.707, -0.707, 0.707, 0.707) * v_texCoord;
    vec2 nearest = 2.0*fract(frequency * st2) - 1.0;
    float dist = length(nearest);
    
    float radius = sqrt(1.0 - mono); // 원 크기
    vec3 white = vec3(1.0, 1.0, 1.0);
    vec3 black = vec3(0.0, 0.0, 0.0);
    vec3 fragcolor = mix(black, white, step(radius, dist));
    
    if(fragcolor.r>0.0)
    {
        gl_FragColor = vec4(YUVtoRGB * yuvColor, 1.0);
    }
    else
    {
        yuvColor.r = 0.2;
        gl_FragColor = vec4(YUVtoRGB * yuvColor, 1.0);
//        gl_FragColor = vec4(fragcolor , 1.0);
    }
}