
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform sampler2D rgbTexture2;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    lowp vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                 texture2D(u_texture, v_texCoord).r - 0.5,
                                            texture2D(v_texture, v_texCoord).r - 0.5);
    if(rgbColor.r < 0.0)
        rgbColor.r = 0.0;
    else if(rgbColor.r > 1.0)
        rgbColor.r = 1.0;
    if(rgbColor.g < 0.0)
        rgbColor.g = 0.0;
    else if(rgbColor.g > 1.0)
        rgbColor.g = 1.0;
    if(rgbColor.b < 0.0)
        rgbColor.b = 0.0;
    else if(rgbColor.b > 1.0)
        rgbColor.b = 1.0;
    lowp vec4 texel = vec4(rgbColor.rgb, 1.0);
    lowp vec3 pass1 = vec3(texture2D(rgbTexture2, vec2(texel.r, 1.0)).r,texture2D(rgbTexture2, vec2(texel.g, 1.0)).g,texture2D(rgbTexture2, vec2(texel.b, 1.0)).b);
    
    
    //apply vignette
    lowp float d = distance(v_texCoord, vec2(0.5,0.5));
    lowp vec3 pass2 = pass1 * smoothstep(0.6394, 0.06056, d);
    
    
    //blend with soft light
    gl_FragColor = 2.0 * vec4(pass2,1.0) * vec4(pass1,1.0) + vec4(pass1,1.0) * vec4(pass1,1.0) - 2.0 * vec4(pass1,1.0) * vec4(pass1,1.0) * vec4(pass2,1.0);
}

