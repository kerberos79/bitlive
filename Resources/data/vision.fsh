//https://www.shadertoy.com/view/ldSGDm
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float iGlobalTime; // shader playback time (in seconds)
const vec2 iResolution = vec2(480, 640);
const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

const vec3 W = vec3(0.2125, 0.7154, 0.0721);
const float T_bright = 0.9;
const float T_contrast = 1.0;
const float T_saturation = 1.0;
vec3 BrightnessContrastSaturation(vec3 color, float brt, float con, float sat)
{
    vec3 black = vec3(0., 0., 0.);
    vec3 middle = vec3(0.5, 0.5, 0.5);
    float luminance = dot(color, W);
    vec3 gray = vec3(luminance, luminance, luminance);
    
    vec3 brtColor = mix(black, color, brt);
    vec3 conColor = mix(middle, brtColor, con);
    vec3 satColor = mix(gray, conColor, sat);
    return satColor;
}

void main() {
    vec2 uv = v_texCoord;
    
    float distanceFromCenter = length( uv - vec2(0.5,0.5) );
    
    float vignetteAmount;
    
    float lum;
    
    vignetteAmount = 1.0 - distanceFromCenter;
    vignetteAmount = smoothstep(0.1, 1.0, vignetteAmount);
    
    vec3 color = YUVtoRGB * vec3(texture2D(CC_Texture0, uv).r,
                                    0.2,
                                    -0.5);
    color = BrightnessContrastSaturation(color, T_bright, T_contrast, T_saturation);
    // scanlines
    color += 0.1*sin(uv.y*iResolution.y*2.0);
    
    // screen flicker
    color += 0.005 * sin(iGlobalTime*16.0);
    
    // vignetting
    color *=  vignetteAmount*1.0;

    gl_FragColor = vec4(color, 1.0);
}

