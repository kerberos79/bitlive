
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    vec2 new_texCoord = vec2(v_texCoord.x + 0.02, v_texCoord.y);
    lowp vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                         texture2D(u_texture, v_texCoord).r - 0.5,
                                         texture2D(v_texture, v_texCoord).r - 0.5);
    lowp vec3 rgbColor2 = YUVtoRGB * vec3(texture2D(CC_Texture0, new_texCoord).r,
                                         texture2D(u_texture, new_texCoord).r - 0.5,
                                         texture2D(v_texture, new_texCoord).r - 0.5);
    
    lowp vec3 result = vec3(rgbColor.r, rgbColor2.g, rgbColor2.b);
    gl_FragColor = vec4(result.rgb,1.0);
}

