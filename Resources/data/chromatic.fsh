
#ifdef GL_ES
precision highp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float iGlobalTime;

const vec2 iResolution = vec2(480, 640);
const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    vec3 c0 = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                    texture2D(u_texture, v_texCoord).r - 0.5,
                                    texture2D(v_texture, v_texCoord).r - 0.5);
    vec2 fragCoord = v_texCoord * iResolution;
    if (mod(floor(fragCoord.y),2.) > 0.)
    {
        float l = dot(c0.xyz, vec3(.2126, .7152, .0722));
        gl_FragColor = vec4(l * c0.rgb, 1.0);
        return;
    }
    
    float t = pow((((1. + sin(iGlobalTime * 10.) * .5)
                    *  .8 + sin(iGlobalTime * cos(fragCoord.y) * 41415.92653) * .0125)
                   * 1.5 + sin(iGlobalTime * 7.) * .5), 5.);
    
    vec2 new_coord =  fragCoord.xy/(iResolution.xy+vec2(t * .2,.0));
    float r = texture2D(CC_Texture0, new_coord).r + 1.402*(texture2D(v_texture, new_coord).r - 0.5);
    new_coord = fragCoord.xy/(iResolution.xy+vec2(t * .5,.0));
    float g = texture2D(CC_Texture0, new_coord).r - 0.344 * (texture2D(u_texture, new_coord).r - 0.5) -0.714 * (texture2D(v_texture, new_coord).r - 0.5);
    new_coord = fragCoord.xy/(iResolution.xy+vec2(t * .9,.0));
    float b = texture2D(CC_Texture0, new_coord).r + 1.772 * (texture2D(u_texture, new_coord).r - 0.5);
    
    gl_FragColor = vec4(c3.r, c2.g, c1.b, 1.0);
}

