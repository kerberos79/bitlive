#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

const float base_a = 1.0;
const float overlay_a = 1.0;
float step_w = 1.0/imageWidth;
float step_h = 1.0/imageHeight;
float step_w2 = 2.0/imageWidth;
float step_h2 = 2.0/imageHeight;

float overlay_yuv(float base, float overlay)
{
    float value;
    if (2.0 * base < base_a) {
        value = 2.0 * overlay * base + overlay * (1.0 - base_a) + base * (1.0 - overlay_a);
    } else {
        value = overlay_a * base_a - 2.0 * (base_a - base) * (overlay_a - overlay) + overlay * (1.0 - base_a) + base * (1.0 - overlay_a);
    }
    
    return value;
}

void main() {
    vec3 yuvColor = vec3(texture2D(CC_Texture0, v_texCoord).r,
                         texture2D(u_texture, v_texCoord).r - 0.5,
                         texture2D(v_texture, v_texCoord).r - 0.5);
    float h = yuvColor.r - (texture2D(CC_Texture0, v_texCoord + vec2(-step_w, -step_h)).r +
                            texture2D(CC_Texture0, v_texCoord + vec2(step_w, -step_h)).r +
                            texture2D(CC_Texture0, v_texCoord + vec2(-step_w, step_h)).r +
                            texture2D(CC_Texture0, v_texCoord + vec2(step_w, step_h)).r +
                            texture2D(CC_Texture0, v_texCoord + vec2(-step_w2, -step_h2)).r +
                            texture2D(CC_Texture0, v_texCoord + vec2(step_w2, -step_h2)).r +
                            texture2D(CC_Texture0, v_texCoord + vec2(-step_w2, step_h2)).r +
                            texture2D(CC_Texture0, v_texCoord + vec2(step_w2, step_h2)).r)/8.0+0.5;
    
    yuvColor.r = overlay_yuv(yuvColor.r, h);
    vec3 base = YUVtoRGB * yuvColor;
    
    gl_FragColor = vec4( base ,1.0);
}

