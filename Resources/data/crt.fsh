
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;

uniform float iGlobalTime; // shader playback time (in seconds)
const vec2 iResolution = vec2(480, 640);
const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
vec2 curve(vec2 uv)
{
    uv = (uv - 0.5) * 2.0;
    uv *= 1.1;
    uv.x *= 1.0 + pow((abs(uv.y) / 5.0), 2.0);
    uv.y *= 1.0 + pow((abs(uv.x) / 4.0), 2.0);
    uv  = (uv / 2.0) + 0.5;
    uv =  uv *0.92 + 0.04;
    return uv;
}
void main() {
    
    vec2 q = v_texCoord;
    vec2 uv = q;
    uv = curve( uv );
    vec3 col;
    vec2 new_coord;
    float test_x =  sin(0.3*iGlobalTime+uv.y*21.0)*sin(0.7*iGlobalTime+uv.y*29.0)*sin(0.3+0.33*iGlobalTime+uv.y*31.0)*0.0017;
    
     new_coord = vec2(test_x+uv.x+0.001,uv.y+0.001);
     col.r = texture2D(CC_Texture0, new_coord).r + 1.402*(texture2D(v_texture, new_coord).r - 0.5)  + 0.05;
     new_coord = vec2(test_x+uv.x,uv.y-0.002);
     col.g = texture2D(CC_Texture0, new_coord).r -0.344 * (texture2D(u_texture, new_coord).r - 0.5) -0.714*(texture2D(v_texture, new_coord).r - 0.5) + 0.05;
     new_coord = vec2(test_x+uv.x-0.002,uv.y);
     col.b = texture2D(CC_Texture0, new_coord).r +1.772 *(texture2D(u_texture, new_coord).r - 0.5) + 0.05;
     
     new_coord = 0.75*vec2(test_x+0.025, -0.027)+vec2(uv.x+0.001,uv.y+0.001);
     col.r += 0.08 * (texture2D(CC_Texture0, new_coord).r + 1.402*(texture2D(v_texture, new_coord).r - 0.5));
     
     new_coord = 0.75*vec2(test_x-0.022, -0.02)+vec2(uv.x,uv.y-0.002);
     col.g += 0.05*(texture2D(CC_Texture0, new_coord).r -0.344 * (texture2D(u_texture, new_coord).r - 0.5) -0.714*(texture2D(v_texture, new_coord).r - 0.5));
     
     new_coord = 0.75*vec2(test_x-0.02, -0.018)+vec2(uv.x-0.002,uv.y);
     col.b += 0.08*(texture2D(CC_Texture0, new_coord).r +1.772 *(texture2D(u_texture, new_coord).r - 0.5));
    
    col = clamp(col*0.6+0.4*col*col*1.0,0.0,1.0);
    
    float vig = (0.0 + 1.0*16.0*uv.x*uv.y*(1.0-uv.x)*(1.0-uv.y));
    col *= vec3(pow(vig,0.3));
    
    col *= vec3(0.95,1.05,0.95);
    col *= 2.8;
    
    float scans = clamp( 0.35+0.35*sin(3.5*iGlobalTime+uv.y*iResolution.y*1.5), 0.0, 1.0);
    
    float s = pow(scans,1.7);
    col = col*vec3( 0.4+0.7*s) ;
    
    col *= 1.0+0.01*sin(110.0*iGlobalTime);
    if (uv.x < 0.0 || uv.x > 1.0)
        col *= 0.0;
    if (uv.y < 0.0 || uv.y > 1.0)
        col *= 0.0;
    
    col*=1.0-0.65*vec3(clamp((mod(v_texCoord.x, 2.0)-1.0)*2.0,0.0,1.0));
    
    gl_FragColor = vec4(col,1.0);
}

