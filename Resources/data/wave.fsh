
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

uniform float iGlobalTime;
const vec2 iResolution = vec2(480.0, 640.0);

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
vec2 mapping(vec2 p) {
    float org_fov = radians(90.0);
    float focal_length = (-0.5+sin(iGlobalTime * 2.0)) * 0.34;
    
    float theta = length(2.0*p)*org_fov*0.5;
    
    // displacement
    float r = focal_length * sin(theta);
    
    return p + normalize(p)*r;
}

vec2 mapping2(vec2 p) {
    float L = length(p);
    float w = 0.01;
    float r = 6.164 / pow(L*45./w,0.8) - 0.164;
    return p + normalize(p)*r;
}

vec2 mapping3(vec2 p) {
    float focal_length = (-0.5+sin(iGlobalTime * 2.0)) * 0.034;
    
    float theta = 14.0*length(p);
    
    // displacement
    float r = focal_length * sin(theta);
    
    return p + normalize(p)*r;
}
void main()
{
    lowp vec2 xy;
    lowp vec3 rgbColor;
    lowp float aspectRatio = iResolution.y / iResolution.x;
    lowp vec2 coords = v_texCoord * 0.8;
    coords = coords - 0.4;
    coords.x *= aspectRatio;
    xy = mapping3(coords);
   
    if(v_texCoord.x>0.492 && v_texCoord.x<0.508
       && v_texCoord.y>0.49 && v_texCoord.y<0.512)
    {
        rgbColor = vec3(texture2D(CC_Texture0, v_texCoord).r,
                        texture2D(u_texture, v_texCoord).r - 0.5,
                        texture2D(v_texture, v_texCoord).r - 0.5);
        gl_FragColor = vec4(YUVtoRGB*rgbColor, 1.0);
    }
    else
    {
    rgbColor = vec3(texture2D(CC_Texture0, 0.5+xy).r,
                    texture2D(u_texture, 0.5+xy).r - 0.5,
                    texture2D(v_texture, 0.5+xy).r - 0.5);
    gl_FragColor = vec4(YUVtoRGB*rgbColor, 1.0);
    }
}