
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

void main() {
    vec3 yuvColor = vec3(texture2D(CC_Texture0, v_texCoord).r * 0.093604,
                                      texture2D(CC_Texture0, v_texCoord).g - 0.5,
                                      texture2D(CC_Texture0, v_texCoord).b - 0.5);

    
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.092000, v_texCoord.y)).r*0.000554;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.088000, v_texCoord.y)).r*0.000812;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.084000, v_texCoord.y)).r*0.001171;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.080000, v_texCoord.y)).r*0.001660;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.076000, v_texCoord.y)).r*0.002313;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.072000, v_texCoord.y)).r*0.003168;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.068000, v_texCoord.y)).r*0.004267;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.064000, v_texCoord.y)).r*0.005650;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.060000, v_texCoord.y)).r*0.007355;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.056000, v_texCoord.y)).r*0.009413;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.052000, v_texCoord.y)).r*0.011843;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.048000, v_texCoord.y)).r*0.014650;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.044000, v_texCoord.y)).r*0.017816;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.040000, v_texCoord.y)).r*0.021300;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.036000, v_texCoord.y)).r*0.025037;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.032000, v_texCoord.y)).r*0.028932;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.028000, v_texCoord.y)).r*0.032870;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.024000, v_texCoord.y)).r*0.036713;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.020000, v_texCoord.y)).r*0.040314;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.016000, v_texCoord.y)).r*0.043522;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.012000, v_texCoord.y)).r*0.046192;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.008000, v_texCoord.y)).r*0.048200;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x+0.004000, v_texCoord.y)).r*0.049445;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.004000, v_texCoord.y)).r*0.049445;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.008000, v_texCoord.y)).r*0.048200;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.012000, v_texCoord.y)).r*0.046192;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.016000, v_texCoord.y)).r*0.043522;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.020000, v_texCoord.y)).r*0.040314;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.024000, v_texCoord.y)).r*0.036713;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.028000, v_texCoord.y)).r*0.032870;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.032000, v_texCoord.y)).r*0.028932;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.036000, v_texCoord.y)).r*0.025037;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.040000, v_texCoord.y)).r*0.021300;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.044000, v_texCoord.y)).r*0.017816;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.048000, v_texCoord.y)).r*0.014650;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.052000, v_texCoord.y)).r*0.011843;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.056000, v_texCoord.y)).r*0.009413;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.060000, v_texCoord.y)).r*0.007355;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.064000, v_texCoord.y)).r*0.005650;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.068000, v_texCoord.y)).r*0.004267;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.072000, v_texCoord.y)).r*0.003168;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.076000, v_texCoord.y)).r*0.002313;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.080000, v_texCoord.y)).r*0.001660;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.084000, v_texCoord.y)).r*0.001171;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.088000, v_texCoord.y)).r*0.000812;
    yuvColor.r += texture2D(CC_Texture0, vec2(v_texCoord.x-0.092000, v_texCoord.y)).r*0.000554;
    gl_FragColor = vec4(YUVtoRGB * yuvColor,1.0);

}

