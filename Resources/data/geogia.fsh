
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

const float fwp = 0.01;
const float ratio = 0.6;
const float scale = 0.5;
const vec3 COLOR_MULT = vec3( 0.981, 0.862, 0.686 );
const float M_PI = 3.14159;
const float PI_PER_4 = M_PI / 4.0;
vec3 brightness(vec3 color, float br) {
	float scaled = br / 2.0;
	if (scaled < 0.0) {
		return color * (1.0 + scaled);
	} else {
		return color + ((1.0 - color) * scaled);
	}
}
vec3 contrast(vec3 color, float contra) {
	return vec3(min(1.0, ((color.x - 0.5) * (tan((contra + 1.0) * PI_PER_4) ) + 0.5))
				, min(1.0, ((color.y - 0.5) * (tan((contra + 1.0) * PI_PER_4) ) + 0.5))
				,min(1.0, ((color.z - 0.5) * (tan((contra + 1.0) * PI_PER_4) ) + 0.5)));
}
void main() {
  vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
						 texture2D(u_texture, v_texCoord).r - 0.5,
                             texture2D(v_texture, v_texCoord).r - 0.5);
    if(rgbColor.r < 0.0)
        rgbColor.r = 0.0;
    else if(rgbColor.r > 1.0)
        rgbColor.r = 1.0;
    if(rgbColor.g < 0.0)
        rgbColor.g = 0.0;
    else if(rgbColor.g > 1.0)
        rgbColor.g = 1.0;
    if(rgbColor.b < 0.0)
        rgbColor.b = 0.0;
    else if(rgbColor.b > 1.0)
        rgbColor.b = 1.0;

  rgbColor = brightness(rgbColor, 0.4724);
  rgbColor = contrast(rgbColor, 0.3149);

  rgbColor.g = rgbColor.g * 0.87 + 0.13;
  rgbColor.b = rgbColor.b * 0.439 + 0.561;

  rgbColor *= COLOR_MULT;
  gl_FragColor = vec4(rgbColor.rgb, 1.0);
}