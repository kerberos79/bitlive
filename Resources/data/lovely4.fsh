
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform sampler2D rgbTexture2;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    lowp vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                         texture2D(u_texture, v_texCoord).r - 0.5,
                                         texture2D(v_texture, v_texCoord).r - 0.5);
    if(rgbColor.r < 0.0)
        rgbColor.r = 0.0;
    else if(rgbColor.r > 1.0)
        rgbColor.r = 1.0;
    if(rgbColor.g < 0.0)
        rgbColor.g = 0.0;
    else if(rgbColor.g > 1.0)
        rgbColor.g = 1.0;
    if(rgbColor.b < 0.0)
        rgbColor.b = 0.0;
    else if(rgbColor.b > 1.0)
        rgbColor.b = 1.0;
    float blueColor = rgbColor.b * 63.0;
    
    lowp  vec2 quad1;
    quad1.y = floor(floor(blueColor) / 8.0);
    quad1.x = floor(blueColor) - (quad1.y * 8.0);
    
    lowp vec2 quad2;
    quad2.y = floor(ceil(blueColor) / 8.0);
    quad2.x = ceil(blueColor) - (quad2.y * 8.0);
    
    lowp vec2 texPos1;
    texPos1.x = (quad1.x * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * rgbColor.r);
    texPos1.y = (quad1.y * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * rgbColor.g);
    
    lowp vec2 texPos2;
    texPos2.x = (quad2.x * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * rgbColor.r);
    texPos2.y = (quad2.y * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * rgbColor.g);
    
    lowp vec3 newColor1 = texture2D(rgbTexture2, texPos1).rgb;
    lowp vec3 newColor2 = texture2D(rgbTexture2, texPos2).rgb;
    
    lowp vec3 newColor = mix(newColor1, newColor2, fract(blueColor));
    float vignetteAmount;
    float distanceFromCenter = length( v_texCoord - vec2(0.5,0.5) );
    vignetteAmount = 1.0 - distanceFromCenter;
    vignetteAmount = smoothstep(0.1, 1.0, vignetteAmount);
    // vignetting
    newColor *=  vignetteAmount*1.0;
    gl_FragColor = vec4(newColor ,1.0);
}

