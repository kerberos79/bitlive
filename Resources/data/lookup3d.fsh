
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform sampler2D rgbTexture2;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

vec4 sampleAs3DTexture(sampler2D texture, vec3 uv, float width) {
    float sliceSize = 1.0 / width;              // space of 1 slice
    float slicePixelSize = sliceSize / width;           // space of 1 pixel
    float sliceInnerSize = slicePixelSize * (width - 1.0);  // space of width pixels
    float zSlice0 = min(floor(uv.z * width), width - 1.0);
    float zSlice1 = min(zSlice0 + 1.0, width - 1.0);
    float xOffset = slicePixelSize * 0.5 + uv.x * sliceInnerSize;
    float s0 = xOffset + (zSlice0 * sliceSize);
    float s1 = xOffset + (zSlice1 * sliceSize);
    vec4 slice0Color = texture2D(texture, vec2(s0, uv.y));
    vec4 slice1Color = texture2D(texture, vec2(s1, uv.y));
    float zOffset = mod(uv.z * width, 1.0);
    vec4 result = mix(slice0Color, slice1Color, zOffset);
    return result;
}

void main() {
    lowp vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                         texture2D(u_texture, v_texCoord).r - 0.5,
                                         texture2D(v_texture, v_texCoord).r - 0.5);
    if(rgbColor.r < 0.0)
        rgbColor.r = 0.0;
    else if(rgbColor.r > 1.0)
        rgbColor.r = 1.0;
    if(rgbColor.g < 0.0)
        rgbColor.g = 0.0;
    else if(rgbColor.g > 1.0)
        rgbColor.g = 1.0;
    if(rgbColor.b < 0.0)
        rgbColor.b = 0.0;
    else if(rgbColor.b > 1.0)
        rgbColor.b = 1.0;
    vec4 newColor = sampleAs3DTexture(rgbTexture2, rgbColor.rgb, 16.0);
    
    gl_FragColor = newColor;
}

