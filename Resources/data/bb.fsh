#ifdef GL_ES
precision mediump float;
#endif


varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;
uniform int radius;

#define SIGMA 13.0
#define BSIGMA 0.1
float kernel[10];
const float bZ = 0.250664;

float normpdf(float x)
{
    return 0.39894*exp(-0.5*x*x/(SIGMA*SIGMA))/SIGMA;
}

float normpdf3(vec3 v)
{
    return 0.39894*exp(-0.5*dot(v,v)/(BSIGMA*BSIGMA))/BSIGMA;
}
void main() {
    vec3 c = texture2D(CC_Texture0, v_texCoord).rgb;
    //declare stuff
    vec3 final_colour = vec3(0.0);
    float Z = 0.0;
    vec3 cc;
    int kSize = (radius-1)/2;
    for (int j = 0; j <= kSize; ++j)
    {
        kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j));
    }

    float factor;
    //read out the texels
    for (int i=-kSize; i <= kSize; ++i)
    {
        for (int j=-kSize; j <= kSize; ++j)
        {
            cc = texture2D(CC_Texture0, v_texCoord + vec2(float(i)/imageWidth,float(j)/imageHeight)).rgb;
            factor = normpdf3(cc-c)*bZ*kernel[kSize+j]*kernel[kSize+i];
            

            Z += factor;
            final_colour += factor*cc;
        }
    }
    gl_FragColor = vec4(final_colour/Z, 1.0);

}

