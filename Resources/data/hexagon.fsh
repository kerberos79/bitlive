
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
float PI = 3.14159265359;
float TAU = 2.0*PI;
float deg30 = TAU/12.0;

const vec2 iResolution = vec2(480, 640);
float hexDist(vec2 a, vec2 b){
    vec2 p = abs(b-a);
    float s = sin(deg30);
    float c = cos(deg30);
    
    float diagDist = s*p.x + c*p.y;
    return max(diagDist, p.x)/c;
}

vec2 nearestHex(float s, vec2 st){
    float h = sin(deg30)*s;
    float r = cos(deg30)*s;
    float b = s + 2.0*h;
    float a = 2.0*r;
    float m = h/r;
    
    vec2 sect = st/vec2(2.0*r, h+s);
    vec2 sectPxl = mod(st, vec2(2.0*r, h+s));
    
    float aSection = mod(floor(sect.y), 2.0);
    
    vec2 coord = floor(sect);
    if(aSection > 0.0){
        if(sectPxl.y < (h-sectPxl.x*m)){
            coord -= 1.0;
        }
        else if(sectPxl.y < (-h + sectPxl.x*m)){
            coord.y -= 1.0;
        }
        
    }
    else{
        if(sectPxl.x > r){
            if(sectPxl.y < (2.0*h - sectPxl.x * m)){
                coord.y -= 1.0;
            }
        }
        else{
            if(sectPxl.y < (sectPxl.x*m)){
                coord.y -= 1.0;
            }
            else{
                coord.x -= 1.0;
            }
        }
    }
    
    float xoff = mod(coord.y, 2.0)*r;
    return vec2(coord.x*2.0*r-xoff, coord.y*(h+s))+vec2(r*2.0, s);
}

void main() {    
    float s = iResolution.x/80.0;
    lowp vec2 nearest = nearestHex(s, v_texCoord.xy);
    vec3 texel = YUVtoRGB * vec3(texture2D(CC_Texture0, nearest/iResolution.xy, -100.0).r,
                                 texture2D(u_texture, nearest/iResolution.xy, -100.0).r - 0.5,
                                 texture2D(v_texture, nearest/iResolution.xy, -100.0).r - 0.5);
    float dist = hexDist(v_texCoord.xy, nearest);
    
    float luminance = (texel.r + texel.g + texel.b)/3.0;
    //float interiorSize = luminance*s;
    float interiorSize = s;
    float interior = 1.0 - smoothstep(interiorSize-1.0, interiorSize, dist);
    //fragColor = vec4(dist);
    gl_FragColor = vec4(texel.rgb*interior, 1.0);
}

