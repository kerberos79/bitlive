
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform sampler2D rgbTexture2;
uniform float imageWidth;
uniform float imageHeight;
uniform vec2 facePos;
uniform float faceScale;
const lowp float shift = 50.0;
const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

void main() {
    lowp vec2 new_texCoord;
    new_texCoord.x = (v_texCoord.x * 0.6) + 0.2;
    new_texCoord.y = (v_texCoord.y * 0.6) + 0.2;
    lowp vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, new_texCoord).r,
                                         texture2D(u_texture, new_texCoord).r - 0.5,
                                         texture2D(v_texture, new_texCoord).r - 0.5);
    
    if(rgbColor.r < 0.0)
        rgbColor.r = 0.0;
    else if(rgbColor.r > 1.0)
        rgbColor.r = 1.0;
    if(rgbColor.g < 0.0)
        rgbColor.g = 0.0;
    else if(rgbColor.g > 1.0)
        rgbColor.g = 1.0;
    if(rgbColor.b < 0.0)
        rgbColor.b = 0.0;
    else if(rgbColor.b > 1.0)
        rgbColor.b = 1.0;
    float blueColor = rgbColor.b * 63.0;
    
    lowp  vec2 quad1;
    quad1.y = floor(floor(blueColor) / 8.0);
    quad1.x = floor(blueColor) - (quad1.y * 8.0);
    
    lowp vec2 quad2;
    quad2.y = floor(ceil(blueColor) / 8.0);
    quad2.x = ceil(blueColor) - (quad2.y * 8.0);
    
    lowp vec2 texPos1;
    texPos1.x = (quad1.x * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * rgbColor.r);
    texPos1.y = (quad1.y * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * rgbColor.g);
    
    lowp vec2 texPos2;
    texPos2.x = (quad2.x * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * rgbColor.r);
    texPos2.y = (quad2.y * 0.125) + 0.5/512.0 + ((0.125 - 1.0/512.0) * rgbColor.g);
    
    lowp vec3 newColor1 = texture2D(rgbTexture2, texPos1).rgb;
    lowp vec3 newColor2 = texture2D(rgbTexture2, texPos2).rgb;
    
    lowp vec3 newColor = mix(newColor1, newColor2, fract(blueColor));
    //apply vignette
    lowp vec2 m = vec2((imageWidth-facePos.x)/imageWidth, (imageHeight-facePos.y+shift)/imageHeight);
    lowp float radius2 = 1.4 / faceScale;
    lowp float d = distance(m, v_texCoord) * radius2;
    newColor = newColor.rgb * (1.0 - d * d);

    gl_FragColor = vec4(newColor,1.0);
}

