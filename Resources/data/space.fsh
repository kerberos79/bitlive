
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform sampler2D rgbTexture2;
uniform float imageWidth;
uniform float imageHeight;
uniform vec2 facePos;
uniform float faceScale;
const lowp float shift = 50.0;
const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

const vec3 COLOR1 = vec3( 0.299, 0.587, 0.114 );
const vec3 COLOR2 = vec3( 0.984, 0.949, 0.639 );
const vec3 COLOR3 = vec3( 0.909, 0.396, 0.702 );
const vec3 COLOR4 = vec3( 0.035, 0.286, 0.914 );

vec3 overlay(vec3 overlayComponent, vec3 underlayComponent, float alpha) {
    vec3 underlay = underlayComponent * alpha;
    return underlay * (underlay + (2.0 * overlayComponent * (1.0 - underlay)));
}
vec3 screenPixelComponent(vec3 maskPixelComponent, float alpha, vec3 imagePixelComponent) {
    return 1.0 - (1.0 - (maskPixelComponent * alpha)) * (1.0 - imagePixelComponent);
}
void main() {
    lowp vec2 new_texCoord;
    new_texCoord.x = (v_texCoord.x * 0.7) + 0.15;
    new_texCoord.y = (v_texCoord.y * 0.7) + 0.15;
    lowp vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, new_texCoord).r,
                                         texture2D(u_texture, new_texCoord).r - 0.5,
                                         texture2D(v_texture, new_texCoord).r - 0.5);
    
    if(rgbColor.r < 0.0)
        rgbColor.r = 0.0;
    else if(rgbColor.r > 1.0)
        rgbColor.r = 1.0;
    if(rgbColor.g < 0.0)
        rgbColor.g = 0.0;
    else if(rgbColor.g > 1.0)
        rgbColor.g = 1.0;
    if(rgbColor.b < 0.0)
        rgbColor.b = 0.0;
    else if(rgbColor.b > 1.0)
        rgbColor.b = 1.0;
    lowp vec3 outputColor;
    outputColor.rgb = rgbColor.rgb;
    lowp vec3 newColor = vec3(texture2D(rgbTexture2, vec2(rgbColor.r, 1.0)).r,texture2D(rgbTexture2, vec2(rgbColor.g, 1.0)).g,texture2D(rgbTexture2, vec2(rgbColor.b, 1.0)).b);
    //apply vignette
    lowp vec2 m = vec2((imageWidth-facePos.x)/imageWidth, (imageHeight-facePos.y+shift)/imageHeight);
    lowp float radius2 = 1.8 / faceScale;
    lowp float d = distance(m, v_texCoord) * radius2;
    newColor = newColor.rgb * (1.0 - d * d);
    
    gl_FragColor = vec4(newColor,1.0);
}

