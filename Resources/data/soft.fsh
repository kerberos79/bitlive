
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
vec3 saturation(vec3 color, float sat) {
	const float lumaR = 0.212671;
	const float lumaG = 0.715160;
	const float lumaB = 0.072169;

	float v = sat + 1.0;
	float i = 1.0 - v;
	float r = i * lumaR;
	float g = i * lumaG;
	float b = i * lumaB;

	mat3 mat = mat3( r + v, r, r, g, g + v, g, b, b, b + v );
	return mat * color;
}

void main() {
  float sum;
  float center;
  center = texture2D(CC_Texture0, v_texCoord).r;
  sum = center * 0.150342;
  sum += (texture2D(CC_Texture0, v_texCoord+vec2(-4.0/640.0, -4.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(4.0/640.0, 4.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(4.0/640.0, -4.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(-4.0/640.0, 4.0/360.0)).r )* 0.003765;
  sum += (texture2D(CC_Texture0, v_texCoord+vec2(-2.0/640.0, -4.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(2.0/640.0, 4.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(2.0/640.0, -4.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(-2.0/640.0, 4.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(-4.0/640.0, -2.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(4.0/640.0, -2.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(-4.0/640.0, 2.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(4.0/640.0, 2.0/360.0)).r )* 0.015019;
    sum += (texture2D(CC_Texture0, v_texCoord+vec2(0.0, -4.0/360.0)).r +
            texture2D(CC_Texture0, v_texCoord+vec2(0.0, 4.0/360.0)).r +
            texture2D(CC_Texture0, v_texCoord+vec2(-4.0/640.0, 0.0)).r +
            texture2D(CC_Texture0, v_texCoord+vec2(4.0/640.0, 0.0)).r )* 0.023792;
  sum += (texture2D(CC_Texture0, v_texCoord+vec2(-2.0/640.0, -2.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(2.0/640.0, -2.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(-2.0/640.0, 2.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(2.0/640.0, 2.0/360.0)).r )* 0.059912;
  sum += (texture2D(CC_Texture0, v_texCoord+vec2(0.0, -2.0/360.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(-2.0/640.0, 0.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(2.0/640.0, 0.0)).r +
          texture2D(CC_Texture0, v_texCoord+vec2(0.0, 2.0/360.0)).r )* 0.094907;
  vec3 yuvColor = vec3(1.0 - ((1.0 - sum ) * (1.0 - center)),
				   texture2D(u_texture, v_texCoord).r - 0.5,
   			   texture2D(v_texture, v_texCoord).r - 0.5);
  vec3 RGB = YUVtoRGB * yuvColor;
  RGB = saturation(RGB, -0.4);
  gl_FragColor = vec4(RGB.rgb, 1.0);
}