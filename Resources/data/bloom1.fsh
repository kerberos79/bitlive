#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;
const float intensity = 0.35;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    float step_w = 1.0/imageWidth;
    float step_h = 1.0/imageHeight;
    float step_w2 = 2.0/imageWidth;
    float step_h2 = 2.0/imageHeight;
    float step_w3 = 3.0/imageWidth;
    float step_h3 = 3.0/imageHeight;
    float step_w4 = 4.0/imageWidth;
    float step_h4 = 4.0/imageHeight;
    vec3 yuvColor = vec3(texture2D(CC_Texture0, v_texCoord).r,
                         texture2D(u_texture, v_texCoord).r,
                         texture2D(v_texture, v_texCoord).r);
    
    float bloom = yuvColor.r * 0.32 +
    (texture2D(CC_Texture0, vec2(v_texCoord.x - step_w4, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x + step_w4, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - step_h4)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + step_h4)).r ) * 0.05 +
    (texture2D(CC_Texture0, vec2(v_texCoord.x - step_w3, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x + step_w3, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - step_h3)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + step_h3)).r ) * 0.09 +
    (texture2D(CC_Texture0, vec2(v_texCoord.x - step_w2, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x + step_w2, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - step_h2)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + step_h2)).r )* 0.12 +
    (texture2D(CC_Texture0, vec2(v_texCoord.x - step_w, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x + step_w, v_texCoord.y)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - step_h)).r +
     texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + step_h)).r )* 0.15;
    
    yuvColor.r += bloom * intensity;
    gl_FragColor = vec4(yuvColor,1.0);
}

