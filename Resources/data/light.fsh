
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

void main() {
  float step_w = 1.0/imageWidth;
  float step_h = 1.0/imageHeight;
  float step_w2 = 2.0/imageWidth;
  float step_h2 = 2.0/imageHeight;
  float sum;
  float center;
  center = texture2D(CC_Texture0, v_texCoord).r;
  sum = center * 0.150342;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(-step_w2, -step_h2)).r * 0.003765;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(-step_w, -step_h2)).r * 0.015019;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(0.0, -step_h2)).r * 0.023792;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(step_w, -step_h2)).r * 0.015019;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(step_w2, -step_h2)).r * 0.003765;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(-step_w2, -step_h)).r * 0.015019;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(-step_w, -step_h)).r * 0.059912;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(0.0, -step_h)).r * 0.094907;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(step_w, -step_h)).r * 0.059912;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(step_w2, -step_h)).r * 0.015019;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(-step_w2.0, 0.0)).r * 0.023792;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(-step_w, 0.0)).r * 0.094907;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(step_w, 0.0)).r * 0.094907;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(step_w2, 0.0)).r * 0.023792;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(-step_w2, step_h)).r * 0.015019;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(-step_w, step_h)).r * 0.059912;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(0.0, step_h)).r * 0.094907;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(step_w, step_h)).r * 0.059912;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(step_w2, step_h)).r * 0.015019;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(-step_w2, step_h2)).r * 0.003765;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(-step_w, step_h2)).r * 0.015019;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(0.0, step_h2)).r * 0.023792;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(step_w, step_h2)).r * 0.015019;
  sum += texture2D(CC_Texture0, v_texCoord+vec2(step_w2, step_h2)).r * 0.003765;
  vec3 yuvColor = vec3(1.0 - ((1.0 - sum ) * (0.8 - center)),
				   texture2D(u_texture, v_texCoord).r - 0.5,
   			   texture2D(v_texture, v_texCoord).r - 0.5);
  gl_FragColor = vec4(YUVtoRGB * yuvColor, 1.0);
}