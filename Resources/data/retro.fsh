
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
const vec3 COLOR1 = vec3( 0.299, 0.587, 0.114 );
const vec3 COLOR2 = vec3( 0.984, 0.949, 0.639 );
const vec3 COLOR3 = vec3( 0.909, 0.396, 0.702 );
const vec3 COLOR4 = vec3( 0.035, 0.286, 0.914 );

vec3 overlay(vec3 overlayComponent, vec3 underlayComponent, float alpha) {
	vec3 underlay = underlayComponent * alpha;
	return underlay * (underlay + (2.0 * overlayComponent * (1.0 - underlay)));
}
vec3 screenPixelComponent(vec3 maskPixelComponent, float alpha, vec3 imagePixelComponent) {
	return 1.0 - (1.0 - (maskPixelComponent * alpha)) * (1.0 - imagePixelComponent);
}
void main() {
  vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
				   texture2D(u_texture, v_texCoord).r - 0.5,
   			   texture2D(v_texture, v_texCoord).r - 0.5);
    
    if(rgbColor.r < 0.0)
        rgbColor.r = 0.0;
    else if(rgbColor.r > 1.0)
        rgbColor.r = 1.0;
    if(rgbColor.g < 0.0)
        rgbColor.g = 0.0;
    else if(rgbColor.g > 1.0)
        rgbColor.g = 1.0;
    if(rgbColor.b < 0.0)
        rgbColor.b = 0.0;
    else if(rgbColor.b > 1.0)
        rgbColor.b = 1.0;
  float gray = dot(rgbColor, COLOR1);
  vec3 color = COLOR2 * overlay(vec3(gray, gray, gray), rgbColor, 1.0) * 0.588235;
  color = screenPixelComponent(COLOR3, 0.2, color);
  color = screenPixelComponent(COLOR4, 0.168627, color);
  gl_FragColor = vec4(color.rgb, 1.0);
}