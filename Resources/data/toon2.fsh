
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
  vec3 yuvColor = vec3(texture2D(CC_Texture0, v_texCoord).r,
                       texture2D(u_texture, v_texCoord).r - 0.5,
                       texture2D(v_texture, v_texCoord).r - 0.5);
  const float bA = 0.5;
  float basey = yuvColor.r;
  float overlayy = yuvColor.r*1.2;
  float overlaya = 1.5;
  if (2.0 * basey < bA) {
      yuvColor.r = 2.0 * overlayy * basey + overlayy* (1.0 - bA) + basey * (1.0 - overlaya);
  } else {
      yuvColor.r = overlaya * bA	 - 2.0 * (bA - basey) * (overlaya - overlayy) + overlayy * (1.0 - bA) + basey * (1.0 - overlaya);
  }
  if(yuvColor.r>0.6) {
    yuvColor.r = 0.9;
  }
  else if(yuvColor.r>0.4) {
    yuvColor.r = 0.6;
  }
  else if(yuvColor.r>0.2)
    yuvColor.r = 0.4;
  else
    yuvColor.r = 0.0;
  gl_FragColor = vec4(YUVtoRGB * yuvColor,1.0);
}