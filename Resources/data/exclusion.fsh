
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform sampler2D rgbTexture2;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    mediump vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                         texture2D(u_texture, v_texCoord).r - 0.5,
                                         texture2D(v_texture, v_texCoord).r - 0.5);
    
    mediump vec4 overlay = vec4(rgbColor.r, rgbColor.r, rgbColor.r, 1.2);
    mediump vec4 base = vec4(texture2D(rgbTexture2, v_texCoord).rgb, 0.8);
    gl_FragColor = vec4((overlay.rgb * base.a + base.rgb * overlay.a - 2.0 * overlay.rgb * base.rgb) + overlay.rgb * (1.0 - base.a) + base.rgb * (1.0 - overlay.a), base.a);
}

