
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;

const float radius = 1.0;
const float strength = 1.0;
const vec2 center = vec2(0.5,0.5);
const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    /*
     
     radius = iMouse.x/iResolution.x;
     strength = iMouse.y/iResolution.y;
     center = vec2(0.5,0.5);
     
     vec2 coord = fragCoord.xy / iResolution.xy;
     coord -= center;
     float distance = length(coord);
     if (distance < radius) {
     float percent = distance / radius;
     if (strength > 0.0) {
     coord *= mix(1.0, smoothstep(0.0, radius / distance, percent), strength * 0.75);
     } else {
     coord *= mix(1.0, pow(percent, 1.0 + strength * 0.75) * radius / distance, 1.0 - percent);
     }
     }
     coord += center;
     fragColor = vec4( texture2D(iChannel0, coord).xyz, 1.0);
     */
    
    vec2 coord = v_texCoord;
    coord -= center;
    float distance = length(coord);
    if (distance < radius) {
        float percent = distance / radius;
        if (strength > 0.0) {
            coord *= mix(1.0, smoothstep(0.0, radius / distance, percent), strength * 0.75);
        } else {
            coord *= mix(1.0, pow(percent, 1.0 + strength * 0.75) * radius / distance, 1.0 - percent);
        }
    }
    coord += center;
    
    
    vec3 rgbColor = vec3(texture2D(CC_Texture0, coord).r,
                         texture2D(u_texture, coord).r - 0.5,
                         texture2D(v_texture, coord).r - 0.5);
    
    gl_FragColor = vec4(YUVtoRGB * rgbColor,1.0);
}

