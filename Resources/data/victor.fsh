
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

#define GammaCorrection(color, gamma)		pow(color, vec3(gamma))

/*
 ** Levels correction for output slider
 ** Details: http://mouaif.wordpress.com/2009/01/28/levels-control-shader/
 */
#define LevelsControlInputRange(color, minInput, maxInput) min(max(color - vec3(minInput), vec3(0.0)) / (vec3(maxInput) - vec3(minInput)), vec3(1.0))
#define LevelsControlInput(color, minInput, gamma, maxInput) GammaCorrection(LevelsControlInputRange(color, minInput, maxInput), gamma)

#define LevelsControlOutputRange(color, minOutput, maxOutput) mix(vec3(minOutput), vec3(maxOutput), color)

#define LevelsControl(color, minInput, gamma, maxInput, minOutput, maxOutput) 	LevelsControlOutputRange(LevelsControlInput(color, minInput, gamma, maxInput), minOutput, maxOutput)

const mediump vec3 luminanceWeighting = vec3(0.2125, 0.7154, 0.0721); // Values from "Graphics Shaders: Theory and Practice" by Bailey and Cunningham

void main() {
    lowp vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                      texture2D(u_texture, v_texCoord).r - 0.5,
                                      texture2D(v_texture, v_texCoord).r - 0.5);
    if(rgbColor.r < 0.0)
        rgbColor.r = 0.0;
    else if(rgbColor.r > 1.0)
        rgbColor.r = 1.0;
    if(rgbColor.g < 0.0)
        rgbColor.g = 0.0;
    else if(rgbColor.g > 1.0)
        rgbColor.g = 1.0;
    if(rgbColor.b < 0.0)
        rgbColor.b = 0.0;
    else if(rgbColor.b > 1.0)
        rgbColor.b = 1.0;
    //min = 53/255, gamma = 1.16, maxInput = 215/255, minOutput = 0.0, maxOutput = 136/255
    lowp vec3 pass1 = LevelsControlOutputRange(LevelsControlInput(rgbColor,0.20784314,1.16,0.84313725),0.0,0.53333333);
    
    // brightness 150%, contrast 100%
    lowp vec3 pass2 = pass1 + vec3(0.3);
    lowp vec3 pass3 = (pass2 - vec3(0.5)) * 1.8 + vec3(0.5);
    
    // saturation
    lowp float luminance = dot(pass3, luminanceWeighting);
    lowp vec3 greyScaleColor = vec3(luminance);
    lowp vec3 pass4 = mix(greyScaleColor, pass3, 1.1);
    
    //apply vignette
    lowp vec2 m = vec2(0.5, 0.5);
    lowp float d = distance(m, v_texCoord) * 1.0;
    lowp vec3 c = pass4.rgb * (1.0 - d * d);
    
    
    
    
    gl_FragColor = vec4(c,1.0);
}

