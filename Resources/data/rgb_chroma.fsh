
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float iGlobalTime;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
void main() {
    lowp vec2 uv = v_texCoord;
    float d = length(uv - vec2(0.5,0.5));
    
    // blur amount
    float blur = 0.0;
    blur = (1.0 + sin(iGlobalTime*6.0)) * 0.5;
    blur *= 1.0 + sin(iGlobalTime*16.0) * 0.5;
    blur = pow(blur, 3.0);
    blur *= 0.05;
    // reduce blur towards center
    blur *= d;
    
    // final color
    lowp vec3 col;
    col.r = texture2D(CC_Texture0, vec2(uv.x+blur,uv.y)).r + 1.402 * texture2D(v_texture, vec2(uv.x+blur,uv.y)).r - 0.5);
    col.g = texture2D(CC_Texture0, uv).r -0.344*(texture2D(u_texture, v_texCoord).r - 0.5)-0.714*(texture2D(v_texture, v_texCoord).r - 0.5));
    col.b = texture2D(CC_Texture0, vec2(uv.x-blur,uv.y)).r + 1.772 * (texture2D(u_texture, vec2(uv.x-blur,uv.y) ).r - 0.5);
    // scanline
    float scanline = sin(uv.y*800.0)*0.04;
    col -= scanline;
    
    // vignette
    col *= 1.0 - d * 0.5;
    
    gl_FragColor = vec4(col,1.0);
}

