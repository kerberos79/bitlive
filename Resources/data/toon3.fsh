
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;
uniform float imageWidth;
uniform float imageHeight;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);
vec3 cC;
float gWT;
float distanceFromCentralColor;
float gW;
float l;
float sum;
const float distanceNormalizationFactor = 16.0;
void main() {
  float stepW1 =1.0/imageWidth;
  float stepNW1 =-1.0/imageWidth;
  float stepH1 =1.0/imageHeight;
  float stepNH1 =-1.0/imageHeight;
  vec3 yuvColor = vec3(texture2D(CC_Texture0, v_texCoord).r,
	   					texture2D(u_texture, v_texCoord).r - 0.46,
   	   				texture2D(v_texture, v_texCoord).r - 0.46);
  const float threashold = 0.5;
  gWT = 0.18;
  sum = yuvColor.r * 0.18;
  float h, v;
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.1 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  l =texture2D(CC_Texture0, v_texCoord - vec2(stepW1, stepH1)).r;
  h = -l;
  v = -l;
  l =texture2D(CC_Texture0, v_texCoord + vec2(stepW1, stepH1)).r;
  h += l;
  v += l;
  l =texture2D(CC_Texture0, v_texCoord + vec2(stepW1, -stepH1)).r;
  h -= l;
  v += l;
  l =texture2D(CC_Texture0, v_texCoord + vec2(0.0, -stepH1)).r;
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.14 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  h -= 2.0 * l;
  l =texture2D(CC_Texture0,  v_texCoord + vec2(-stepW1 , 0.0)).r;
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.17 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  v -= 2.0 * l;
  l =texture2D(CC_Texture0, v_texCoord + vec2(stepW1, 0.0)).r;
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.17 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  v += 2.0 * l;
  l =texture2D(CC_Texture0, v_texCoord + vec2(0.0, stepH1)).r;
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.14 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  h += 2.0 * l;
  l =texture2D(CC_Texture0, v_texCoord + vec2(-stepW1, stepH1)).r;
  distanceFromCentralColor = min(distance(yuvColor.r, l) * distanceNormalizationFactor, 1.0);
  gW = 0.1 * (1.0 - distanceFromCentralColor);
  gWT += gW;
  sum += l * gW;
  h += l;
  v -= l;
    
  if(yuvColor.r>0.6)
    yuvColor.r = 0.8;
  else if(yuvColor.r>0.35)
    yuvColor.r = 0.6;
  else if(yuvColor.r>0.26)
    yuvColor.r = 0.3;
  else
    yuvColor.r = 0.05;
  cC = YUVtoRGB * yuvColor;
  float thresholdTest = 1.0 - step(threashold, length(vec2(h, v)));
  vec3 b = YUVtoRGB * yuvColor;
  const float bA = 0.5;
  vec4 overlay = vec4(texture2D(CC_Texture0, v_texCoord).rgb, 1.8);
  float ra;
  if (2.0 * b.r < bA) {
      ra = 2.0 * overlay.r * b.r + overlay.r * (1.0 - bA) + b.r * (1.0 - overlay.a);
  } else {
      ra = overlay.a * bA - 2.0 * (bA - b.r) * (overlay.a - overlay.r) + overlay.r * (1.0 - bA) + b.r * (1.0 - overlay.a);
  }
  gl_FragColor = vec4(ra*thresholdTest , ra*thresholdTest, ra*thresholdTest , 1.0);
}