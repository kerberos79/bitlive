
#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform sampler2D v_texture;

const mat3 YUVtoRGB = mat3(1.0, 1.0, 1.0,
                           0, -0.344, 1.772,
                           1.402, -0.714,0);

float SCurve (float value, float amount, float correction) {
    
    float curve = 1.0;
    
    if (value < 0.5)
    {
        
        curve = pow(value, amount) * pow(2.0, amount) * 0.5;
    }
    
    else
    {
        curve = 1.0 - pow(1.0 - value, amount) * pow(2.0, amount) * 0.5;
    }
    
    return pow(curve, correction);
}

void main() {
    /*
     
     vec2 uv = fragCoord.xy / iResolution.xy;
     
     vec4 C = texture2D(iChannel0, uv);
     vec4 A = C;
     
    	// Writing this as a sort of 'note to self'
     
    	// Applies an S curve to the image, you can adjust the steepness
    	// of the curve with the control values SCurve(input, steepness, gamma)
     
    	C = vec4(SCurve(C.r, 4.0, 1.0), SCurve(C.g, 3.7, 0.7), SCurve(C.b, 2.6, 0.6), 1.0);
     
     fragColor = C;
     */
    vec3 rgbColor = YUVtoRGB * vec3(texture2D(CC_Texture0, v_texCoord).r,
                                    texture2D(u_texture, v_texCoord).r - 0.5,
                                    texture2D(v_texture, v_texCoord).r - 0.5);
    
    gl_FragColor = vec4(SCurve(rgbColor.r, 4.0, 1.0), SCurve(rgbColor.g, 3.7, 0.7), SCurve(rgbColor.b, 2.6, 0.6), 1.0);
}

